'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "main.dart.js": "6f29f6ea37d29a8beae31150a613d01e",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"constants.js": "ea793f09e97ce2105d7dc0ccacf16cb3",
"index.html": "7503b81c7ab0ab9d24b1835563e40f6f",
"/": "7503b81c7ab0ab9d24b1835563e40f6f",
"assets/fonts/ProximaNova-Semibold.otf": "f14eee643541cf03a10f26c944cc29f5",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/fonts/ProximaNova-Regular.otf": "bf9f5d50c1b928ff21436517a1a95ad9",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/fluttertoast/assets/toastify.js": "e7006a0a033d834ef9414d48db3be6fc",
"assets/packages/fluttertoast/assets/toastify.css": "a85675050054f179444bc5ad70ffc635",
"assets/FontManifest.json": "4e9dc94e2f49b9d7be57bcdd171eed42",
"assets/images/pending.png": "58e0d22df361c2261616818dd558cef9",
"assets/NOTICES": "f53caa2509f9e359421b68cdd7825ecc",
"assets/assests/awesomestar.png": "0402f0759fa83f0a833370f49c095cf5",
"assets/assests/geniefilter.flr": "ad1b3db4da0167e6fae5f8bc1fd815bb",
"assets/assests/lock.flr": "407f1f61fa01f2c4ddae47f2e34d376a",
"assets/assests/app_logo.png": "0c074955dcf7270e83c7c76b4cbe6ed0",
"assets/assests/flixjini_score.png": "3570b6df0edc18c2320195a76820c0b8",
"assets/assests/filter_by.png": "e29c5f6b0d31fa550711b60cfdbbc60f",
"assets/assests/active_livetv.png": "1a997489a32dfa1303ca76cdc97c9ca6",
"assets/assests/share_dialog.png": "e197ae7269bc354030d9a43e67d05842",
"assets/assests/greenup.png": "efcf68154d9cc0f451a8b773fb3d8d07",
"assets/assests/star.png": "c29493184fff90146fbebb365998be9b",
"assets/assests/trash_icon.png": "84e451533befe25c596d0b2597a3e066",
"assets/assests/discover_stars.png": "f5190ae912dfb0b7e8bbf6de1ac208d0",
"assets/assests/inactive_profile.png": "0bfdbd84b3f82415935e85852c90ade7",
"assets/assests/c_down_black.png": "48b534d25afd013a1a6b72bf11811e55",
"assets/assests/tmdb_logo.png": "9b3f9c24d9fd5f297ae433eb33d93514",
"assets/assests/tutorial_img2.png": "023e73bcc61874890ba08c67a5d30e2d",
"assets/assests/more_loader.gif": "06a1f11653eceb2badb5d5d50fd68886",
"assets/assests/review_empty.png": "07698acb04664b00a75d6258aefa06d4",
"assets/assests/c_up_black.png": "fb88e08364fc8777567349bb9b29ea81",
"assets/assests/quotes.png": "1466ff08a1182585c63ef0cfcac3792e",
"assets/assests/awnom.png": "49cba51104116ffe4d786a12d772e3b7",
"assets/assests/positive_smiley.png": "ea25c95c9b006a90492d59e6b8a670c6",
"assets/assests/added.png": "55c9f7966bf8e13fe29e3e3333cbee5c",
"assets/assests/no_movie_queue.png": "9c75c2aa749daedf01c74e263d80c5c8",
"assets/assests/flixjiniLogo.png": "781e4fa8b9ab8bde324a3f284dfd4517",
"assets/assests/c_more_black.png": "f506accf8d0d341e51fd6abb23d7eadd",
"assets/assests/user_pro.png": "c33669e92fb1b591e8453ed0b284a573",
"assets/assests/rt_score.png": "741157ef1209db4017a5107c72cb87ab",
"assets/assests/colorful_google_logo.png": "436e4bd472f0a0006e6b6af51ff6701c",
"assets/assests/sadsmile.flr": "8c6e106c68f496a7c3bc7d82e799314e",
"assets/assests/share.flr": "791592a38493055129ca6815314e11ee",
"assets/assests/filter.png": "a06cbe5597704d73c36fca0d8c57eb7a",
"assets/assests/new_emojis/flixjini_score.png": "3570b6df0edc18c2320195a76820c0b8",
"assets/assests/new_emojis/neutral_smiley.png": "cc8ff82f2eef4de10d706266132086d1",
"assets/assests/new_emojis/score_section_neutral_like.png": "1124f4cc63bd28ab14afec7c90a8d4b7",
"assets/assests/new_emojis/sad_smiley.png": "aebc1c4986f083e34d2170d0060d5eb9",
"assets/assests/new_emojis/award_logo.png": "7a1d6bd09dc7a9303e7d4e3cbda0197b",
"assets/assests/new_emojis/score_section_like.png": "37b006c960666d82bc04497bcf2026af",
"assets/assests/new_emojis/score_section_dislike.png": "a6f6a475ae33fb2801a25c3de61ab27b",
"assets/assests/new_emojis/box_office_icon.png": "9cc30e9ae8553211fc1a8b9be76fe678",
"assets/assests/new_emojis/play_app.png": "d790927e341f796e3d86423f4dda9e01",
"assets/assests/new_emojis/happy_smiley.png": "f4280eeef27bd6c2344dc60bddba638d",
"assets/assests/mylisttick.png": "df7720f1c6d0d4266ae03729b56b5cee",
"assets/assests/type.png": "27c4e7b829e9bf1cdc07a24240705c54",
"assets/assests/blockbuster.png": "79f6b72664bab6bdee01fa4f5f7aca72",
"assets/assests/password_icon.png": "d4dcc4bee52b48997f1af2b3531f1418",
"assets/assests/play_episode.png": "5078e28a5e1be9eee6d887174cf8fe93",
"assets/assests/hide_password_icon.png": "abdebc6933f3351261e388228a6cf219",
"assets/assests/no_internet.png": "912de834f885e2173b5b5485315f6740",
"assets/assests/list_down.png": "e4b86037ef396aec17b4609ba7d1008e",
"assets/assests/inactive_livetv.png": "4b3bb93760517a7a7cc26e6c8b65b556",
"assets/assests/following_list.png": "b181c40eb67df481642403aaa25c8771",
"assets/assests/genie_inactive.png": "64427a64321221248c5bca280d7e763a",
"assets/assests/seen_dark.png": "053e1bea6196625b4e3d349fc73013ca",
"assets/assests/large_queue.png": "62aaec6f48bbf1c5bb088ef4f5df9e84",
"assets/assests/backarrow.png": "d1c32b2c105f1bcbed2713aa8e090e1f",
"assets/assests/expand_icon.png": "6cba7bd385883f582fe715b37e7bf007",
"assets/assests/trailer.png": "ef3ba3dc3ea23267a994cbb1613fb947",
"assets/assests/active_queue.png": "d75fc320d68fc026e561bd1083c1500f",
"assets/assests/disc_banner.jpg": "dd1fd6e2e1f5613e251d33a33ed46dc9",
"assets/assests/star_filled.png": "ce0f57da4d777e20912ede1fd57913d8",
"assets/assests/email_icon.png": "ac94ed845e2075530692502e6e99e494",
"assets/assests/inactive_fresh.png": "b076162b09b61537ce7a18a0466e8d44",
"assets/assests/share.png": "8b40bbc06e8cc03dc30bcd90f6e31fce",
"assets/assests/genie.flr": "22bc9ec1c123d755fb9ae73132d0e4e7",
"assets/assests/active_fresh.png": "9a17d874a9a44da9bc78152e211ae168",
"assets/assests/save_filter_save_button_icon.png": "97d5f9b21fbb2940e996daba34e2be60",
"assets/assests/c_up.png": "5e3a603cc74d418511220564f8eff2ef",
"assets/assests/share/whats_share.png": "928291a33b01865c31cfee58ddec5abb",
"assets/assests/share/copy_url.png": "9fee4e5fd4f069ecd71f9204a9b22816",
"assets/assests/share/share_img.png": "7b8a4a117d047d90134e5ffda4ce9265",
"assets/assests/share/insta_share.png": "1531174b5790a8f3aa5ee9ef50ad3dc1",
"assets/assests/share/share_url.png": "53f0f296c509971bdfcabd202921a17e",
"assets/assests/share/twitter_share.png": "09a8c2a836fe05e4c65d6f15df75ece7",
"assets/assests/share/link_share.png": "bce23f63178d0e2322b04f309ee33414",
"assets/assests/share/gchat_share.png": "00a2d4f25eca9deced701f7d8047f859",
"assets/assests/share/snap_share.png": "37df4b8e01e77dc5013cecb588385719",
"assets/assests/share/others_share.png": "6962811d9bb7f0ea658fd24e27582789",
"assets/assests/share/fb_share.png": "3935f1ba66170126e39fcf9a95a37162",
"assets/assests/share/hangouts_share.png": "68bf9f31872a02467a78708f5499d07b",
"assets/assests/review_full.png": "b77ec4435db0681c9f589d13e93f3d45",
"assets/assests/movies.png": "2201263042fb41fae49b2a617b11d699",
"assets/assests/flixjinilandingpage.flr": "51495e259176c6be586fb049a19e9e61",
"assets/assests/review_half.png": "3dc537f82534984827f60f567f29fa54",
"assets/assests/remove_red.png": "3f5573cbffe5589a7d5eb2537c192db0",
"assets/assests/right_arrow.png": "2df13f725ba6af18d017498bb2cb0e58",
"assets/assests/play_video.png": "9438f0960281761d1a4e1e40e5f6dbdf",
"assets/assests/season_select.png": "55cbd4208f4c0aa5f25ff62f14cb6d80",
"assets/assests/less.png": "1ccbed7161ff69026211c2f8cd5ac77b",
"assets/assests/New%2520Folder%2520With%2520Items/lock.flr": "407f1f61fa01f2c4ddae47f2e34d376a",
"assets/assests/New%2520Folder%2520With%2520Items/sadsmile.flr": "8c6e106c68f496a7c3bc7d82e799314e",
"assets/assests/show_password_icon.png": "ecdc8a1897b96f11e8f589e416c487ed",
"assets/assests/user_queue.png": "cfc88f1b14449d3c5298cd988d86aab8",
"assets/assests/no_bg.jpg": "38b47689ff417be361f74c556d7d4ea2",
"assets/assests/active_home.png": "3b227c5b79d45e8bd97a7a26b0ea1890",
"assets/assests/dnw.png": "519729d4f81bfe92ac0690df1cbe6eca",
"assets/assests/dislike.png": "685c32d5fee636b06698c1cb2d84ccb2",
"assets/assests/theFlixjiniLogo.png": "0c074955dcf7270e83c7c76b4cbe6ed0",
"assets/assests/c_down.png": "befa3ef099018a2a9f19124ad0a98a6f",
"assets/assests/google.png": "b9ff6256f3b25281bb1b8f9ee547cd9d",
"assets/assests/filter_roulette.png": "4b5f7d9116a6c82f59afe2a6d71e5fb8",
"assets/assests/sort_by.png": "4f87e8e1e69d4b15386425df6816d246",
"assets/assests/box_office.png": "998fdb68e309724ee5071cd87a0ec0fc",
"assets/assests/mylist_dark.png": "08c01508890caee110d00d9104635142",
"assets/assests/iosflixjinilogo.jpg": "ded127843467d09e7f464dee3439a0ba",
"assets/assests/jini_fresh.png": "89fe226bfcb403dab322b1ceffdbdfba",
"assets/assests/drop_down_arrow.png": "bedb66eb5e0c2613eb2137955eb8861a",
"assets/assests/active_search.png": "bb8a29ed5213b9d1a31344c14f4359d6",
"assets/assests/share_pro.png": "6c259d2ef93f7fc0b9eb3b6db4d7020f",
"assets/assests/down_arrow_search.png": "c2b8b165d09f503ba349e88bf7f6808b",
"assets/assests/like.png": "2a6c2c30384d8f69dd223ac714d7667d",
"assets/assests/na.png": "4b83466d5854eef78c23a60a4c03f92f",
"assets/assests/greendown.png": "9c8c5a7a90930a1ff8a4a5377c3f0c76",
"assets/assests/close_grey.png": "b162f8faa7d656d9e559b940cc3e005b",
"assets/assests/komparify.png": "4474fcf271b9f8f92cd30db2e7973cbb",
"assets/assests/tv_shows.png": "84177f9274e001eff85dde718697364f",
"assets/assests/negative_smiley.png": "bf37f9df7a627225d9c73f7608a8f62c",
"assets/assests/active_profile.png": "13e04ed9ab12dd50d2153ec061fc8fac",
"assets/assests/review_like.png": "25af79ac51fb44781dc223e3e3fa4d6d",
"assets/assests/dislike_roulette.png": "7b0f8097579fbdeccfcbf13adaa28b1d",
"assets/assests/blurred_images/7.png": "cb09a1f43502f8a844128d65fce8f771",
"assets/assests/blurred_images/2.png": "661f4c956a09219c51c19c035481c927",
"assets/assests/blurred_images/4.png": "b6dccbc92f2e81ec7fcc69bd8f1d2472",
"assets/assests/blurred_images/6.png": "9407a8859c2801b7b132f4614878175e",
"assets/assests/blurred_images/8.png": "5675f72a5a270ca97577f27dd0a3f1b2",
"assets/assests/blurred_images/5.png": "6e9729ca1abcf6273e67e38f4836a7b9",
"assets/assests/blurred_images/3.png": "2fac4c4972c332fbaa0bacf21f7ee38b",
"assets/assests/blurred_images/1.png": "ff270cc0698db4da66388367706e354b",
"assets/assests/blurred_images/10.png": "c7c4a2da68c71166018ff36df76f20e9",
"assets/assests/blurred_images/9.png": "53055452613333d61114499648d1e5b7",
"assets/assests/close_video.png": "14535b4cbea4f6e37307499f353740e3",
"assets/assests/qr_scan.png": "e915a678b4e2f0efd429d82478998ead",
"assets/assests/inactive_queue.png": "a2325990324c91b64c8e22da656e987e",
"assets/assests/play.png": "c6c03151e3d3fed99923b42f84d5af17",
"assets/assests/feedback_banner.png": "d807094738d15bb33082e20b205daf61",
"assets/assests/inactive_search.png": "d1b75c8ac6bc0f04b6821ce3308e5327",
"assets/assests/tv_ic.png": "ade54adbac0b858ad0443664bf7d633a",
"assets/assests/collapse_icon.png": "57245e052750efe992423514dc96b876",
"assets/assests/up_arrow_search.png": "abef4855bde1c24278b23223d2041d00",
"assets/assests/movie_details_page_test_image.png": "bbc51c259ea75ba05fbf311ef1c3713c",
"assets/assests/rab.png": "2563495ebb182f89c49097169640d9aa",
"assets/assests/indianflag.png": "357484a98281d0968edde07ceb6ec358",
"assets/assests/close_circle.png": "f96f8a57ea97f82c2de575f436d433db",
"assets/assests/review_neutral.png": "59c3f62c16571fa66ba7383534f0a1de",
"assets/assests/like_roulette.png": "aa2aebb4f876bd12ec04469e7918b16a",
"assets/assests/tutorial_img1.png": "f13621a4fe452a977c1223f18b729ed1",
"assets/assests/award.png": "ebf5e9073a93c002cb514fb10ce8b797",
"assets/assests/lock.png": "5d349264a77720a4f05e175b8e09f231",
"assets/assests/source.png": "bb909959530bda42272295a002e706b4",
"assets/assests/backarrow1.png": "e782370a3950b40c813d71230dd8dbd6",
"assets/assests/location_pro.png": "696ba1000f147cb1c61da15120141572",
"assets/assests/next_roulette.png": "23b1fc9d49ba828b33fa7d82e5d48258",
"assets/assests/genie_active.png": "3cc0e576d0f78d2a156f01fa2815c2c2",
"assets/assests/no_movie.png": "082c432e42d9399ac8bc6331c430f665",
"assets/assests/fb_log_in.png": "35c007041679a137d2b0b2a6fa7dc894",
"assets/assests/inactive_home.png": "200593050b708730406d5999dba0a27f",
"assets/assests/page_loader.gif": "bc153c5fb086868324545a378380da6d",
"assets/assests/theaters.png": "9d4f2c41efcbd9d8d6800e9f1c8625c4",
"assets/assests/imdb.png": "90bc1067897f1fea19d4a0f0d76aa21b",
"assets/assests/flixjini_logo.flr": "d6b5e8b331166351f276ceec5490fd6a",
"assets/assests/play_app.png": "d790927e341f796e3d86423f4dda9e01",
"assets/assests/whatsapp_icon.png": "f3fc8b811892dfb4ab3baee94f1edd32",
"assets/assests/app_tour.png": "ce70ae6bdbe2db42263c4b1749857477",
"assets/assests/language.png": "c278b705784e2e1240e55201d033696c",
"assets/assests/star_empty.png": "6e5423db61a34e25815548cd01476c51",
"assets/assests/close_icon.png": "e700d1ca6470967da74824228983ea64",
"assets/assests/awdef.png": "70a807197374f2fa3dc8f49fffb0fda8",
"assets/assests/star.flr": "a655fc1f85c9f50c0a4f209b41cc18c6",
"assets/assests/tutorial_img3.png": "f7aea14cbf155de927341faa26a12c45",
"assets/assests/greenclose.png": "12bb1fa24789fabd8b4f1771c86e0648",
"assets/assests/more.png": "d1f0e9c48a785ee7a443729ece11a28a",
"assets/assests/list_up.png": "78ef0a0cd05cec6b60bc6c4335c6c49a",
"assets/assests/queue_roulette.png": "e34d45361e12598db079cba61a49fa92",
"assets/assests/tutorial_img4.png": "410918260077a468acd18d24032e4c44",
"assets/assests/review_dislike.png": "37b993cb0c7a4c824256915f33d8dab6",
"assets/assests/back_roulette.png": "8d6c8b827ae5da7a9193b6e842383046",
"assets/assests/poster_play_roulette.png": "ae054ba06a8f326765c570b82df10838",
"assets/assests/seen_roulette.png": "cee375ae2a5de63ff584e801b794f7be",
"assets/assests/home_tray_bg.jpg": "a993bff511e13d2a0f7147d9d67136ce",
"assets/static/sort_options_data.json": "c69353b2a787aecd46a24145effbda1d",
"assets/static/filter_options_data.json": "79987365111bc06f9ebdab6e9743fc40",
"assets/AssetManifest.json": "6ade7bd854de5dd403bd1c91dbad958e",
"manifest.json": "5a2882c36b29f65b8b99cdae4a51d817",
"version.json": "250ad50b6746c1d5f72e567f72448480",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
