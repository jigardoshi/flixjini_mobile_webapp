import 'dart:convert';
// import 'dart:io';
import 'dart:ui';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/error_handle_page.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flixjini_webapp/utils/deep_link_apps.dart';
// import 'package:device_apps/device_apps.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
// import 'package:flutter_appavailability/flutter_appavailability.dart';
// import 'package:flutter_launcher_icons/constants.dart';
import 'package:url_launcher/url_launcher.dart' as urlLaunch;
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/constants.dart';

class LiveTvDetailPage extends StatefulWidget {
  LiveTvDetailPage({
    this.moveToPreviousTab,
    this.authToken,
    this.showOriginalPosters,
    this.playSources,
    this.liveTvData,
    this.index,
    this.tagIndex,
    this.deepAppsString,
    this.fromSearch,
    this.urlName,
    Key? key,
  }) : super(key: key);
  final authToken;
  final moveToPreviousTab;
  final showOriginalPosters;
  final playSources;
  final index;
  final liveTvData;
  final tagIndex;
  final deepAppsString;
  final fromSearch;
  final urlName;

  @override
  _LiveTvDetailPageState createState() => new _LiveTvDetailPageState();
}

class _LiveTvDetailPageState extends State<LiveTvDetailPage> {
  List installedApps = [], otherApps = [], epgData = [];
  bool installedAppsFlag = false,
      otherAppsFlag = false,
      epgFetched = false,
      channelDataFetched = false,
      ipError = false;
  Map deepApps = {}, channelData = {};
  var sourceKeys = {
    "hotstar": "in.startv.hotstar",
    "erosnow": "com.erosnow",
    "hooq": "tv.hooq.android",
    "amazon-prime": "com.amazon.avod.thirdpartyclient",
    "netflix": "com.netflix.mediaclient",
    "jio-cinema": "com.jio.media.ondemand",
    "hungama-play": "com.hungama.movies",
    "voot": "com.tv.v18.viola",
    "sonyliv": "com.sonyliv",
    "bigflix": "com.bigflix.Movies",
    "google-play": "com.android.vending",
    "youtube-movies": "com.google.android.youtube",
    "spuul": "com.spuul.android",
    "viu": "com.vuclip.viu",
    "viki": "com.viki.android",
    "yupptv": "com.yupptv.yuppflix",
    "alt-balaji": "com.balaji.alt",
    "sun-nxt": "com.suntv.sunnxt",
    "airtel-xstream": "tv.accedo.airtel.wynk",
    "vodafone-play": "com.vodafone.vodafoneplay",
    "zee-5": "com.graymatrix.did",
    "hoichoi": "com.viewlift.hoichoi",
    "mxplayer": "com.mxtech.videoplayer.ad",
    "shemaroo": "com.saranyu.shemarooworld",
    "tata-sky": "com.ryzmedia.tatasky",
    "aha-video": "ahaflix.tv",
    "tvf": "com.tvf.tvfplay",
    "quibitv": "com.quibi.qlient",
    "tubitv": "com.tubitv",
    "voot-kids": "com.viacom18.vootkids",
    "watcho": "com.watcho",
    "docubay": "com.epic.docubay",
    "nextg-tv": "com.digivive.offdeck",
  };

  launchURL(seed) async {
    String url = seed;
    if (await urlLaunch.canLaunch(url)) {
      await urlLaunch.launch(url);
      printIfDebug("Yes we lunched url");
    } else {
      throw 'Could not launch $url';
    }
  }

  void initState() {
    super.initState();
    deepLinkMap();
    if (widget.fromSearch != null && widget.fromSearch) {
      getchannelData();
    } else {
      channelData =
          widget.liveTvData[widget.tagIndex]["channels"][widget.index];
      channelDataFetched = true;
      findAppsInstalled();
      getEpgData(channelData["offering_name_id"].toString());
    }
  }

  getchannelData() async {
    String url = getAppurl() + widget.urlName.toString().split("?").first + "?";
    url += (widget.authToken != null && widget.authToken.isNotEmpty
        ? "jwt_token=" + widget.authToken + "&"
        : "");
    url = constructHeader(url);
    url = await fetchDefaultParams(url);

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for channels details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug(data);
        setState(() {
          channelData = data;
          channelDataFetched = true;
        });
        findAppsInstalled();
        getEpgData(channelData["offering_name_id"].toString());
      } else {
        setState(() {
          ipError = true;
        });
        printIfDebug(
            'Error getting channels IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      setState(() {
        ipError = true;
      });
      printIfDebug(
          '\n\n channels details page api, exception caught is: $exception');
    }
  }

  getTimeFromString(String? showTime) {
    String newTime = "";
    if (showTime != null && showTime.length > 0) {
      List timeList = showTime.split(":");
      // int hour = int.parse(timeList.elementAt(0));

      String minute = timeList.elementAt(1);
      newTime = timeList.elementAt(0) + ":" + minute;
      // newTime += hour > 11 ? "PM" : "AM";
    }
    return newTime;
  }

  deepLinkMap() async {
    try {
      // String reminderDataFromFRC = await getStringFromFRC('deepLinkTvApps');
      printIfDebug("deep link apps" + widget.deepAppsString);

      setState(() {
        deepApps = jsonDecode(widget.deepAppsString.toString());
      });
    } catch (e) {
      printIfDebug(e.toString());
    }
  }

  findAppsInstalled() async {
    List installedAppsLocal = [], otherAppsLocal = [];

    for (int i = 0; i < channelData["playability"].length; i++) {
      var packageName =
          sourceKeys[channelData["playability"][i]["url_name"]] ?? "";
      printIfDebug(packageName);

      // Application? appsIn =
      //     await DeviceApps.getApp(packageName).catchError((onError) {
      //   printIfDebug(onError);
      // });

      if (true) {
        //appsIn != null) {
        installedAppsLocal.add(channelData["playability"][i]);
      } else {
        otherAppsLocal.add(channelData["playability"][i]);
      }
    }
    setState(() {
      installedApps = installedAppsLocal;
      otherApps = otherAppsLocal;
      otherAppsFlag = true;
      installedAppsFlag = true;
    });
    printIfDebug("installed apps" + installedApps.toString());
    printIfDebug("other apps" + otherApps.toString());
  }

  Widget logoContainer() {
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      height: 120,
      width: screenWidth,
      color: Constants.appColorL1,
      margin: EdgeInsets.only(
        top: 5,
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Image.network(
            channelData["channel_logo"],
            fit: BoxFit.fitWidth,
            width: screenWidth - 100,
          ),
          Container(
            width: screenWidth,
            height: 120,
            child: new ClipRect(
              child: new BackdropFilter(
                filter: new ImageFilter.blur(
                  sigmaX: 10.0,
                  sigmaY: 10.0,
                ),
                child: new Container(
                  width: screenWidth,
                  height: 120,
                  decoration: new BoxDecoration(
                    color: Constants.appColorL1.withOpacity(
                      0.30,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Row(
            children: [
              Container(
                width: 60,
                height: 60,
                margin: EdgeInsets.only(
                  left: 20,
                ),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: Constants.appColorFont,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Image.network(
                  channelData["channel_logo"],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 10,
                ),
                child: Text(
                  channelData["name"],
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    letterSpacing: 1,
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget nowPlaying() {
    return Container();
  }

  Widget yourApps(text, flag) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            left: 20,
            top: 40,
            bottom: 20,
          ),
          child: Text(
            text,
            style: TextStyle(
              color: Constants.appColorFont,
              letterSpacing: 1,
            ),
          ),
        ),
        playSourcesCards(flag),
      ],
    );
  }

  Widget playSourcesCards(bool appFlag) {
    var listApps = appFlag ? installedApps : otherApps;
    return AnimationLimiter(
      child: ListView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: listApps.length,
        itemBuilder: (BuildContext context, int index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            duration: const Duration(milliseconds: 375),
            child: SlideAnimation(
              verticalOffset: 50.0,
              child: FadeInAnimation(
                child: GestureDetector(
                  onTap: () {
                    if (deepApps.containsKey(listApps[index]["url_name"])) {
                      launchURL(listApps[index]["stream_link"]);
                    } else if (appFlag) {
                      launchURL(listApps[index]["stream_link"]);

                      // DeviceApps.openApp(
                      //     sourceKeys[listApps[index]["url_name"]]!);
                    } else {
                      String urlL =
                          "https://play.google.com/store/apps/details?id=" +
                              sourceKeys[listApps[index]["url_name"]]!;
                      printIfDebug(urlL);
                      launchURL(urlL);
                    }
                    // launchURL("https://m.nexgtv.com/vasanth-tv.html");
                    // if (true) {
                    //   // callAndroidLaunchCode(index);

                    // }
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      bottom: 20,
                      right: 20,
                      left: 20,
                    ),
                    padding: EdgeInsets.only(
                      top: 5,
                      bottom: 5,
                    ),
                    decoration: BoxDecoration(
                      color: Constants.appColorL2,
                      borderRadius: new BorderRadius.circular(5.0),
                      border: Border.all(
                        color: appFlag
                            ? Constants.appColorLogo
                            : Constants.appColorL2,
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            color: Colors.transparent,
                            height: 45,
                            width: 45,
                            padding: EdgeInsets.all(5),
                            child: Image.network(
                                "https://c.kmpr.in/assets/streaming-sources/" +
                                    listApps[index]["streaming_source_id"]
                                        .toString() +
                                    ".png"),
                          ),
                        ),
                        Expanded(
                          flex: appFlag ? 6 : 5,
                          child: Container(
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.only(
                              left: 5,
                            ),
                            child: Text(
                              listApps[index]["name"],
                              style: TextStyle(
                                color: Constants.appColorFont,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: appFlag ? 2 : 3,
                          child: true || appFlag
                              ? Container(
                                  height: 20,
                                  width: 20,
                                  padding: EdgeInsets.all(5),
                                  child: Image.asset(
                                    "assests/play.png",
                                    color: Constants.appColorLogo,
                                  ),
                                )
                              : Container(
                                  margin: EdgeInsets.only(
                                    right: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(
                                        0xFF777777,
                                      ),
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20)),
                                  ),
                                  padding: EdgeInsets.only(
                                    top: 10,
                                    left: 5,
                                    right: 5,
                                    bottom: 10,
                                  ),
                                  child: Center(
                                    child: Text(
                                      "SUBSCRIBE",
                                      style: TextStyle(
                                        color: Constants.appColorFont,
                                        fontSize: 8,
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget titleBox(String title) {
    return Text(
      title,
      style: TextStyle(
        color: Constants.appColorFont,
        fontSize: 10,
        letterSpacing: 1,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  Widget tvPage() {
    try {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          logoContainer(),
          nowPlaying(),
          installedApps.length > 0 ? yourApps("YOUR APPS", true) : Container(),
          otherApps.length > 0
              ? yourApps("ALSO AVAILABLE ON", false)
              : Container(),
          Container(
            margin: EdgeInsets.only(
              left: 30,
              right: 30,
              top: 5,
              bottom: 20,
            ),
            height: 1,
            color: Constants.appColorDivider.withOpacity(0.5),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: titleBox("CHANNEL GUIDE"),
          ),
          Container(
            height: 75,
            margin: EdgeInsets.only(left: 20),
            child: epgFetched
                ? ListView.builder(
                    // physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: epgData.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (index == 0) {
                        return Container();
                      } else {
                        return Container(
                          // height: 75,
                          child: Row(
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    epgData[index]["show_name"] ?? "",
                                    style: TextStyle(
                                        color: Constants.appColorFont,
                                        fontSize: 15,
                                        letterSpacing: 1,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Row(
                                      children: [
                                        Text(
                                          getTimeFromString(epgData[index]
                                                      ["start_time"]
                                                  .toString()
                                                  .split(" ")
                                                  .first) ??
                                              "",
                                          style: TextStyle(
                                              color: Constants.appColorFont,
                                              fontSize: 10,
                                              letterSpacing: 1,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          " - " +
                                              getTimeFromString(epgData[index]
                                                      ["end_time"]
                                                  .toString()
                                                  .split(" ")
                                                  .first),
                                          style: TextStyle(
                                              color: Constants.appColorFont,
                                              fontSize: 12,
                                              letterSpacing: 1,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                ),
                                width: 1,
                                color: Constants.appColorL2,
                              ),
                            ],
                          ),
                        );
                      }
                    },
                  )
                : getProgressBar(),
          ),
        ],
      );
    } catch (e) {
      return Container();
    }
  }

  getEpgData(String channelId) async {
    String url =
        Constants.apiUrl + "/tvapp_epg/channel/" + channelId + ".json?";
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    printIfDebug(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for movie details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.epgData = data;
        printIfDebug(data);
        setState(() {
          epgData = this.epgData;
          epgFetched = true;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('\n\ndetails page api, exception caught is: $exception');
    }
  }

  @override
  Widget build(BuildContext context) {
    String message =
        "We could not load this page. This may be because you are not connected to the internet or our server is having issues. Please check your internet connection and be rest assured we will resolve any server issues as quick as we can";
    String imagePath = "assests/no_movie.png";
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.appColorL2,
        iconTheme: new IconThemeData(color: Constants.appColorDivider),
      ),
      backgroundColor: Constants.appColorL1,
      body: ipError
          ? Center(
              child: ErrorHandlePage(
                message: message,
                imagePath: imagePath,
                refresh: getchannelData,
              ),
            )
          : SingleChildScrollView(
              child: otherAppsFlag && installedAppsFlag && channelDataFetched
                  ? tvPage()
                  : Center(
                      child: new CircularProgressIndicator(
                        backgroundColor: Constants.appColorLogo,
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Constants.appColorLogo),
                      ),
                    ),
            ),
    );
  }
}
