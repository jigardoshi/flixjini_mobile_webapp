import 'dart:convert';
import 'dart:ui';
import 'package:flixjini_webapp/livestreaming/flutter_live_player.dart';
import 'package:flixjini_webapp/livetv/live_tv_detail_page.dart';
import 'package:flixjini_webapp/livetv/marquee_widget.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart' as urlLaunch;
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/constants.dart';

class LiveTvPage extends StatefulWidget {
  LiveTvPage({
    this.moveToPreviousTab,
    this.showOriginalPosters,
    Key? key,
  }) : super(key: key);
  final moveToPreviousTab;
  final showOriginalPosters;

  @override
  _LiveTvPageState createState() => new _LiveTvPageState();
}

class _LiveTvPageState extends State<LiveTvPage> {
  var liveTvData;
  bool fetchedLiveTvData = false, showFreeButton = false;
  String authenticationToken = "", deepAppsString = "{}";
  List<ScrollController> scrollControllerList = [];
  Map<int, int> pageCount = {};
  Map<int, bool> fetchDataForList = {};
  int selectedContent = 0, tempSelectedContent = 0;
  List contentTypes = ["All Apps", "Free Apps", "My Apps"],
      contentTypeDesc = [
        "Show Content from all sources. Even those not included in your plan",
        "Show Free Content available across all sources",
        "Show only Content included in your currently active plan"
      ];

  void initState() {
    super.initState();
    deepLinkMap();
    getFreeFlagSP();
  }

  deepLinkMap() async {
    try {
      String reminderDataFromFRC = await getStringFromFRC('noDeepLinkApps');
      printIfDebug("deep link apps" + reminderDataFromFRC);

      setState(() {
        deepAppsString = reminderDataFromFRC;
      });
    } catch (e) {
      printIfDebug(e.toString());
    }
  }

  getFreeFlagSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool flag1 = prefs.getBool('freeflag') ?? false;
    bool flag3 = prefs.getBool('myappsflag') ?? false;
    // bool flag4 = prefs.getBool('partnerflag') ?? true;

    int typeNo = flag1 ? 1 : (flag3 ? 2 : 0);

    setState(() {
      selectedContent = typeNo;
      tempSelectedContent = typeNo;
    });
    checkAuthentication();
  }

  checkAuthentication() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    fetchLiveTvData(authenticationToken);
  }

  Future<void> fetchLiveTvData(String auth) async {
    String url = Constants.apiUrl +
        "/tvapp_channel_with_epg.json?app_type=all_apps&is_mobile=1&jwt_token=" +
        auth +
        "&";
    url = (url + getParamsForContent());
    url = constructHeader(url);
    url = await fetchDefaultParams(url);

    try {
      http.Response response = await http.get(
        Uri.parse(url),
        // headers: {
        //   "Accept": "application/json",
        //   "Access-Control-Allow-Origin": "*"
        // },
        // headers: {
        //   'Content-Type': 'application/json',
        //   'Access-Control-Allow-Headers':
        //       'Origin, Content-Type, Cookie, X-CSRF-TOKEN, Accept, Authorization, X-XSRF-TOKEN, Access-Control-Allow-Origin',
        //   'Access-Control-Expose-Headers': "" 'Authorization, authenticated',
        //   'Access-Control-Allow-Origin': '*',
        //   'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT',
        //   'Access-Control-Allow-Credentials': 'true',
        // },
      );
      printIfDebug(response.headers);

      printIfDebug(
        '\n\njson response status code for movie details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.liveTvData = data;
        printIfDebug(data);
        setState(() {
          liveTvData = this.liveTvData;
          fetchedLiveTvData = true;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('\n\n Live tv page api, exception caught is: $exception');
    }
  }

  Future<void> getMoreChannelsForTag(String urlSuffix, int index) async {
    String url = urlSuffix +
        "?app_type=all_apps&is_mobile=1&jwt_token=" +
        authenticationToken +
        "&";
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for tv extra channels: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        List data = json.decode(responseBody);
        printIfDebug(data);
        List extraChannels = [];

        extraChannels.addAll(liveTvData[index]["channels"]);
        extraChannels.removeLast();
        extraChannels.addAll(data);
        setState(() {
          liveTvData[index]["channels"] = extraChannels;
          // fetchedLiveTvData = true;
          fetchDataForList[index] = false;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('\n\ndetails page api, exception caught is: $exception');
    }
  }

  String getParamsForContent() {
    switch (tempSelectedContent) {
      case 0:
        return "";
      case 1:
        return "free_only=true&";
      case 2:
        return "only_profile_sources=true&";
      default:
        return "";
    }
  }

  AppBar appBarContainer() {
    return AppBar(
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () => widget.moveToPreviousTab(),
        ),
      ),
      elevation: 4.0,
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            // titleExpansion.toUpperCase(),
            "Live Tv",
            style: TextStyle(
              color: Constants.appColorFont,
              fontSize: 18,
            ),
          ),
          GestureDetector(
            onTap: () {
              showSheet();
            },
            child: Container(
              // height: 100,
              // width: 100,
              padding: EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: 15,
                right: 15,
              ),
              decoration: BoxDecoration(
                  color: Constants.appColorL1.withOpacity(0.5),
                  border: Border.all(
                    color: Constants.appColorFont.withOpacity(0.6),
                  ),
                  borderRadius: BorderRadius.circular(30)),

              child: Row(
                children: [
                  Text(
                    contentTypes[tempSelectedContent].toUpperCase(),
                    style: TextStyle(
                      color: Constants.appColorFont,
                      fontSize: 12,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 5,
                    ),
                    child: Image.asset(
                      "assests/drop_down_arrow.png",
                      height: 10,
                      width: 10,
                      color: Constants.appColorLogo,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget cardWithEpg(int index, int i) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 2,
                child: Center(
                  child: Container(
                    width: 40,
                    height: 40,
                    margin: EdgeInsets.only(left: 5),
                    decoration: BoxDecoration(
                      color: Constants.appColorFont,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: EdgeInsets.all(3),
                    child: Image.network(
                      liveTvData[index]["channels"][i]["channel_logo"]
                          .toString()
                          .replaceAll(".png", ".jpg"),
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                            // top: 10,
                            // left: 5,
                            right: 0),
                        child: Text(
                          liveTvData[index]["channels"][i]["name"],
                          style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 10,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.timer,
                              size: 10,
                              color: Constants.appColorLogo,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                left: 3,
                              ),
                              child: Row(
                                children: [
                                  Text(
                                    getTimeFromString(liveTvData[index]
                                                    ["channels"][i]["epg"][0]
                                                ["start_time"]
                                            .toString()
                                            .split(" ")
                                            .first) ??
                                        "",
                                    style: TextStyle(
                                      color: Constants.appColorFont
                                          .withOpacity(0.6),
                                      fontSize: 9,
                                    ),
                                  ),
                                  Text(
                                    " - " +
                                        (liveTvData[index]["channels"][i]["epg"]
                                                    [0]["end_time"] !=
                                                null
                                            ? getTimeFromString(
                                                liveTvData[index]["channels"][i]
                                                        ["epg"][0]["end_time"]
                                                    .toString()
                                                    .split(" ")
                                                    .first)
                                            : ""),
                                    style: TextStyle(
                                      color: Constants.appColorFont
                                          .withOpacity(0.6),
                                      fontSize: 9,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Row(
          children: [
            Container(
              width: 100,
              height: 20,
              margin: EdgeInsets.only(left: 10, right: 5),
              child: MarqueeWidget(
                child: Text(
                  liveTvData[index]["channels"][i]["epg"][0]["show_name"] !=
                          null
                      ? liveTvData[index]["channels"][i]["epg"][0]["show_name"]
                          .toString()
                      : "",
                  style: TextStyle(
                    color: Constants.appColorFont.withOpacity(0.6),
                    fontSize: 12,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  getTimeFromString(String? showTime) {
    String newTime = "";
    if (showTime != null && showTime.length > 0) {
      List timeList = showTime.split(":");
      // int hour = int.parse(timeList.elementAt(0));

      String minute = timeList.elementAt(1);
      newTime = timeList.elementAt(0) + ":" + minute;
      // newTime += hour > 11 ? "PM" : "AM";
    }
    return newTime;
  }

  launchURL(seed) async {
    String url = seed;
    if (await urlLaunch.canLaunch(url)) {
      await urlLaunch.launch(url);
      printIfDebug("Yes we lunched url");
    } else {
      throw 'Could not launch $url';
    }
  }

  tvCard(index, i) {
    return liveTvData[index]["channels"][i].isEmpty
        ? Container(
            child: Center(
              child: CircularProgressIndicator(
                backgroundColor: Constants.appColorLogo,
              ),
            ),
          )
        : GestureDetector(
            onTap: () {
              if (liveTvData[index]["channels"][i]["is_playable"] &&
                  (liveTvData[index]["channels"][i]["playability"][0]
                              ["stream_link"]
                          .toString()
                          .contains(".m3u8") ||
                      liveTvData[index]["channels"][i]["playability"][0]
                              ["url_name"]
                          .toString()
                          .contains("playboxtv-live-tv"))) {
                // Navigator.of(context).push(
                //   new MaterialPageRoute(
                //     builder: (BuildContext context) => FlutterLivePlayer(
                //       url: liveTvData[index]["channels"][i]["playability"][0]
                //           ["stream_link"],
                //       isLive: true,
                //       title: liveTvData[index]["channels"][i]["name"],
                //       currentChannel: i,
                //       currentTag: index,
                //       liveTvData: liveTvData,
                //     ),
                //   ),
                // );
              } else {
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    maintainState: false,
                    builder: (BuildContext context) => LiveTvDetailPage(
                      fromSearch: false,
                      authToken: authenticationToken,
                      moveToPreviousTab: widget.moveToPreviousTab,
                      showOriginalPosters: widget.showOriginalPosters,
                      playSources: liveTvData[index]["channels"],
                      liveTvData: liveTvData,
                      index: i,
                      tagIndex: index,
                      deepAppsString: deepAppsString,
                    ),
                  ),
                );
              }
            },
            child: Container(
              margin: EdgeInsets.only(
                right: 10,
              ),
              decoration: BoxDecoration(
                color: Constants.appColorL2,
                borderRadius: BorderRadius.circular(8),
              ),
              width: 130,
              child: liveTvData[index]["channels"][i]["epg"] != null &&
                      liveTvData[index]["channels"][i]["epg"].length > 0
                  ? cardWithEpg(index, i)
                  : Column(
                      children: [
                        Center(
                          child: Container(
                            width: 50,
                            height: 50,
                            margin: EdgeInsets.only(top: 10),
                            decoration: BoxDecoration(
                              color: Constants.appColorFont,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            padding: EdgeInsets.all(10),
                            child: Image.network(
                              liveTvData[index]["channels"][i]["channel_logo"]
                                  .toString()
                                  .replaceAll(".png", ".jpg"),
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(
                              top: 10,
                            ),
                            child: Text(
                              liveTvData[index]["channels"][i]["name"],
                              style: TextStyle(
                                color: Constants.appColorFont,
                                fontSize: 10,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
          );
  }

  Widget bodyContainer() {
    double screenWidth = MediaQuery.of(context).size.width;

    double screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Container(
          child: AnimationLimiter(
            child: ListView.builder(
              itemCount: liveTvData.length,
              itemBuilder: (BuildContext context, int index) {
                ScrollController eachScroll = new ScrollController();
                eachScroll.addListener(() {
                  if (eachScroll.position.pixels ==
                      eachScroll.position.maxScrollExtent) {
                    if (!fetchDataForList.containsKey(index) ||
                        !fetchDataForList[index]!) {
                      printIfDebug("end " + index.toString());
                      if (pageCount.containsKey(index)) {
                        setState(() {
                          pageCount[index] = (pageCount[index]! + 1);
                        });
                      } else {
                        setState(() {
                          pageCount.putIfAbsent(index, () {
                            return 2;
                          });
                        });
                      }
                      String url = liveTvData[index]["tag_url"]
                              .toString()
                              .replaceAll("https://www.komparify.com",
                                  Constants.apiUrl + "/") +
                          "/" +
                          pageCount[index].toString();

                      if (fetchDataForList.containsKey(index)) {
                        setState(() {
                          fetchDataForList[index] = true;
                        });
                      } else {
                        setState(() {
                          fetchDataForList.putIfAbsent(index, () {
                            return true;
                          });
                        });
                      }
                      setState(() {
                        liveTvData[index]["channels"].add({});
                      });

                      // eachScroll.jumpTo(
                      //   double.parse(liveTvData[index]["channels"].length.toString()),
                      // );
                      getMoreChannelsForTag(url, index);
                    }
                  }
                });
                // scrollControllerList[index] = eachScroll;
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 50.0,
                    child: FadeInAnimation(
                      child: liveTvData[index]["channels"].length > 0
                          ? Container(
                              margin: EdgeInsets.only(
                                top: 10,
                                bottom: 10,
                              ),
                              child: Column(
                                children: [
                                  categoryTitle(index),
                                  Container(
                                    height: 80,
                                    margin: EdgeInsets.only(
                                      top: 10,
                                      bottom: 20,
                                      left: 20,
                                    ),
                                    child: ListView.builder(
                                      controller: eachScroll,
                                      scrollDirection: Axis.horizontal,
                                      itemCount:
                                          liveTvData[index]["channels"].length,
                                      itemBuilder:
                                          (BuildContext context, int i) {
                                        return tvCard(index, i);
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        showFreeButton
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    selectedContent = tempSelectedContent;
                  });
                  showSheet();
                },
                child: Container(
                  color: Constants.appColorL1.withOpacity(0.75),
                  height: screenHeight,
                  width: screenWidth,
                ),
              )
            : Container(),
        showFreeButton
            ? Container(
                alignment: Alignment.center,
                child: freeSheet(),
              )
            : Container(),
      ],
    );
  }

  Widget freeSheet() {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.width;
    List<Widget> typeListWidget = [];
    for (int i = 0; i < contentTypes.length; i++) {
      Widget temp = GestureDetector(
        child: Container(
          padding: EdgeInsets.only(
            // left: 15,
            // right: 15,
            top: 10,
            bottom: 10,
          ),
          width: screenWidth / 2,
          margin: EdgeInsets.only(
            top: 10,
            bottom: 15,
          ),
          // height: 25,
          // width: 100,
          decoration: new BoxDecoration(
            border: new Border.all(
              color: selectedContent == i
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            borderRadius: new BorderRadius.circular(30.0),
            color: selectedContent == i
                ? Constants.appColorLogo
                : Constants.appColorL3,
          ),
          child: Center(
            child: Text(
              contentTypes[i],
              style: TextStyle(
                fontSize: 11,
                color: selectedContent == i
                    ? Constants.appColorL1
                    : Constants.appColorFont,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        onTap: () {
          switch (i) {
            case 0:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(false);
              setFreeFlagSP(false);
              break;
            case 1:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(false);
              setFreeFlagSP(true);
              break;
            case 2:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(true);
              setFreeFlagSP(false);
              break;
            default:
          }
        },
      );
      typeListWidget.add(temp);
    }
    return Container(
      width: screenWidth / 1.2,
      child: GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Constants.appColorL3,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                      bottom: 10,
                    ),
                    child: Text(
                      "Choose Content View",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 20,
                      left: 20,
                      right: 20,
                    ),
                    child: Text(
                      // "Choose what content is displayed below. My Plan will display content only from your currently active plan, PlayboxTV will display content from curated sources.",
                      contentTypeDesc[selectedContent],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Constants.appColorFont.withOpacity(0.75),
                        fontWeight: FontWeight.normal,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 20),

              decoration: new BoxDecoration(
                color: Constants.appColorL2,
              ),
              width: screenWidth / 1.2,

              // height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: typeListWidget,
              ),
            ),
            Container(
              color: Constants.appColorL3,
              width: screenWidth / 1.2,
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(
                        // left: 15,
                        // right: 15,
                        top: 10,
                        bottom: 10,
                      ),
                      width: screenWidth / 2,
                      margin: EdgeInsets.only(
                        // top: 10,
                        bottom: 20,
                      ),
                      // height: 25,
                      // width: 100,
                      decoration: new BoxDecoration(
                        border: new Border.all(color: Constants.appColorLogo),
                        borderRadius: new BorderRadius.circular(30.0),
                        color: Constants.appColorLogo,
                      ),
                      child: Center(
                        child: Text(
                          "Apply",
                          style: TextStyle(
                            fontSize: 11,
                            color: Constants.appColorL1,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      printIfDebug(selectedContent);
                      setState(() {
                        fetchedLiveTvData = false;
                        tempSelectedContent = selectedContent;
                      });
                      switch (selectedContent) {
                        case 0:
                          setMyAppsFlagSP(false);
                          setFreeFlagSP(false);
                          break;
                        case 1:
                          setMyAppsFlagSP(false);
                          setFreeFlagSP(true);
                          break;
                        case 2:
                          setMyAppsFlagSP(true);
                          setFreeFlagSP(false);
                          break;
                        default:
                      }
                      showSheet();
                      fetchLiveTvData(authenticationToken);
                    },
                  ),
                  // GestureDetector(
                  //   child: Container(
                  //     padding: EdgeInsets.only(
                  //       // left: 15,
                  //       // right: 15,
                  //       top: 10,
                  //       bottom: 10,
                  //     ),
                  //     width: screenWidth / 2,
                  //     margin: EdgeInsets.only(
                  //       top: 10,
                  //       bottom: 15,
                  //     ),
                  //     // height: 25,
                  //     // width: 100,
                  //     decoration: new BoxDecoration(
                  //       border: new Border.all(
                  //         color: Constants.appColorL3,
                  //       ),
                  //       borderRadius: new BorderRadius.circular(30.0),
                  //       color: Constants.appColorL3,
                  //     ),
                  //     child: Center(
                  //       child: Text(
                  //         "CANCEL",
                  //         style: TextStyle(
                  //           fontSize: 11,
                  //           color: Constants.appColorFont,
                  //           fontWeight: FontWeight.bold,
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  //   onTap: () {

                  //     showSheet();
                  //   },
                  // ),
                ],
              ),
            ),
          ],
        ),
        onTap: () {
          // showSheet();
        },
      ),
    );
  }

  Widget categoryTitle(i) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 14,
      ),
      child: new GestureDetector(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // insertDividerVertical(),
            Expanded(
              // flex: 6,
              child: Container(
                margin: EdgeInsets.only(
                  left: 10,
                ),
                padding: EdgeInsets.only(
                  top: 5,
                  left: 8,
                ),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(
                      width: 4.0,
                      color: Constants.appColorLogo,
                    ),
                  ),
                ),
                child: Text(
                  liveTvData[i]["tag_name"].toString(),
                  textAlign: TextAlign.left,
                  style: new TextStyle(
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 19.0,
                  ),
                  maxLines: 2,
                ),
              ),
            ),
            // Container(
            //   margin: EdgeInsets.only(
            //     right: 10,
            //   ),
            //   child: Image.asset(
            //     "assests/right_arrow.png",
            //     height: 20,
            //     color: Constants.appColorLogo,
            //   ),
            // ),
          ],
        ),
        onTap: () {},
      ),
    );
  }

  showSheet() {
    if (showFreeButton) {
      setFreeSliderFlag(false);
      setState(() {
        showFreeButton = false;
      });
    } else {
      setFreeSliderFlag(true);

      setState(() {
        showFreeButton = true;
      });
    }
  }

  setFreeFlagSP(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('freeflag', flag);
  }

  setMyAppsFlagSP(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('myappsflag', flag);
  }

  setFreeSliderFlag(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('freesliderflag', flag);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      appBar: appBarContainer(),
      body: fetchedLiveTvData
          ? bodyContainer()
          : Center(
              child: CircularProgressIndicator(
                backgroundColor: Constants.appColorLogo,
              ),
            ),
    );
  }
}
