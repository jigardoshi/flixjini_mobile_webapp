// import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:html_unescape/html_unescape.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flare_flutter/flare_actor.dart';
// import 	'dart:convert';

class MovieProfileUnauthenticated extends StatefulWidget {
  MovieProfileUnauthenticated({
    Key? key,
    this.message,
    this.imagePath,
    this.animationName,
    this.switchCardImages,
    this.fromQueuePage,
  }) : super(key: key);

  final message;
  final imagePath;
  final animationName;
  final switchCardImages;
  final fromQueuePage;

  @override
  _MovieProfileUnauthenticatedState createState() =>
      new _MovieProfileUnauthenticatedState();
}

class _MovieProfileUnauthenticatedState
    extends State<MovieProfileUnauthenticated> {
  double screenWidth = 0.0;
  double screenHeight = 0.0;

  @override
  void initState() {
    super.initState();
  }

  Widget quickStart() {
    return new Container(
      padding: const EdgeInsets.all(20.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new RaisedButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPageNew(type: true)));
            },
            child: new Text(
              "Sign Up",
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Constants.appColorFont),
            ),
          ),
          new RaisedButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPageNew(type: false)));
            },
            child: new Text(
              "Sign In",
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Constants.appColorFont),
            ),
          ),
        ],
      ),
    );
  }

  Widget lockImage() {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        child: new Center(
          child: new FlareActor(widget.imagePath,
              alignment: Alignment.center,
              fit: BoxFit.cover,
              animation: widget.animationName),
        ),
      ),
      flex: 3,
    );
  }

  Widget defaultMessage(message) {
    return new Expanded(
      child: new Container(
        width: screenWidth * 0.8,
        child: new Center(
          child: new Text(
            message,
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: Constants.appColorFont,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            textScaleFactor: 1.0,
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget signIn() {
    return new Expanded(
      child: new Container(
        child: new Center(
          child: new InkWell(
            onTap: () {
              Navigator.of(context).pushReplacement(
                new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPageNew(
                    type: false,
                  ),
                ),
              );
            },
            child: new Container(
              height: 40.0,
              width: screenWidth * 0.50,
              decoration: new BoxDecoration(
                color: Constants.appColorLogo,
                border: new Border.all(color: Constants.appColorLogo),
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child: new Center(
                child: new Text(
                  "SIGN IN",
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 15.0,
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 3,
                ),
              ),
            ),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget signUp() {
    return new Expanded(
      child: new Container(
        child: new Center(
          child: new InkWell(
            onTap: () {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPageNew(type: true)));
            },
            child: new Container(
              height: 40.0,
              width: screenWidth * 0.50,
              decoration: new BoxDecoration(
                color: Constants.appColorLogo,
                border: new Border.all(color: Constants.appColorLogo),
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child: new Center(
                child: new Text(
                  "SIGN UP",
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 15.0,
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 3,
                ),
              ),
            ),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget authenticateYourself() {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: new Container(
              margin: EdgeInsets.only(
                top: 40,
              ),
            
              height: screenHeight * 0.5,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  lockImage(),
                  defaultMessage(widget.message),
                  signIn(),
                  signUp(),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(
                bottom: 10,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: widget.fromQueuePage != true
                        ? callGetContainerForVersion()
                        : Container(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget callGetContainerForVersion() {
    return Container(
      child: GestureDetector(
        onLongPress: () {
          // printIfDebug('\n\nlong press detected');
          // showToast(
          //   "Magic switch is toggled!",
          //   'long',
          //   'bottom',
          //   1,
          //   Constants.appColorLogo,
          // );
          // widget.switchCardImages();
        },
        child: getContainerForVersion(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    return authenticateYourself();
  }
}
