// import 'dart:io';

import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/google_advertising_id.dart';
// import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/index/movie_index_page.dart';
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
import 'package:flixjini_webapp/profile/movie_profile_settings.dart';
import 'package:flixjini_webapp/profile/movie_profile_unauthenticated.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 	'package:streaming_entertainment/authentication/movie_authentication_page.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_profile.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'dart:convert';
import 'package:flixjini_webapp/common/constants.dart';

import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';

class MovieProfilePage extends StatefulWidget {
  MovieProfilePage({
    Key? key,
    this.moveToPreviousTab,
    this.switchCardImages,
    this.showOriginalPosters,
  }) : super(key: key);

  final moveToPreviousTab;
  final switchCardImages;
  final showOriginalPosters;

  @override
  MovieProfilePageState createState() => new MovieProfilePageState();
}

class MovieProfilePageState extends State<MovieProfilePage> {
  String authenticationToken = "", fcmToken = "";
  String stringprofileSettingData = "",
      countryCode = '',
      stringDeviceInformation = "";
  bool authenticationTokenFetched = false;
  var profileSettingData = {};
  bool profileSettingDataReady = false;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  // static final facebookAppEvents = FacebookAppEvents();
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  List alwaysopenList = [];

  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    retriveDeviceInformationStatus();
    getCountryCode();
    authenticationStatus();
    logUserScreen('Flixjini Profile Page', 'MovieProfilePage');
  }

  retriveDeviceInformationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringDeviceInformation =
          (prefs.getString('stringDeviceInformation') ?? "");
      prefs.setString('stringDeviceInformation', stringDeviceInformation);
    });
    getFCMTokenInSP();
  }

  Future<void> getFcmidGaidAndroidid() async {
    var deviceInformation = {
      'android_id': "",
      'device_model': "",
      'device_maker': "",
      'app_name': "Flixjini",
      'app_version': "",
      'jwt_token': "",
    };
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
    });
    deviceInformation["jwt_token"] = authenticationToken;
    try {
      if (true) {
        var result = await getGoogleAdId();
        if (result != null) {
          // printIfDebug(
          //     "\n\nno need for uuid, as the following gaid is obtained: $result");
          // printIfDebug(result);
          deviceInformation["ga_id"] = result;
        } else {
          String result = await checkIfNewUuidNeeded();
          // printIfDebug("\n\nuuid obtained is: $result");
          deviceInformation["ga_id"] = result;
        }
      } 
      // else if (true) {
      //   String result = await checkIfNewUuidNeeded();
      //   // printIfDebug("uuid obtained is: $result");
      //   deviceInformation["ga_id"] = result;
      // } else {
      //   // printIfDebug("Unkown OS");
      // }
    } on PlatformException catch (e) {
      // printIfDebug("Failed to get Android Advertising Id: '${e.message}'.");
      String result = await checkIfNewUuidNeeded();
      // printIfDebug("uuid obtained");
      // printIfDebug(result);
      deviceInformation["ga_id"] = result;
      printIfDebug(e);
    }
    if (true) {
      // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      // deviceInformation["android_id"] = androidInfo.androidId;
      // deviceInformation["device_model"] = androidInfo.model;
      // deviceInformation["device_maker"] = androidInfo.manufacturer;
      // deviceInformation["app_version"] = getVersionCodeForAndroid();
      // deviceInformation["os"] = 'android';
      // deviceInformation["gcm_id"] = fcmToken;
    } else if (true) {
      // IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      // deviceInformation["device_model"] = iosInfo.model;
      // deviceInformation["device_maker"] = "Apple Inc";
      // deviceInformation["app_version"] = iosInfo.utsname.release;
      // deviceInformation["ga_id"] = '';
      // deviceInformation["idfa"] = iosInfo.identifierForVendor;
      // deviceInformation["os"] = 'ios';
      // deviceInformation["gcm_id"] = fcmToken;
    } else {
      printIfDebug("Unkown OS");
    }
    String url = "https://api.flixjini.com/app_track.json";
    // printIfDebug(
    //     "\n\nfor app_track api endpoint, the device info: $deviceInformation");
    bool appTrackSatus = false;
    appTrackSatus =
        checkIfAppTrackRequestsNeedsToBeSent(deviceInformation, prefs);
    bool timeFlag = false;

    String timesp = (prefs.getString('spTime') ?? "");
    var timeNow = new DateTime.now();
    prefs.setString('spTime', timeNow.toString());
    if (timesp.length > 0) {
      var timeSaved = DateTime.parse(timesp);
      if (timeNow.isAfter(timeSaved.add(Duration(days: 3)))) {
        timeFlag = true;
      }
    } else {
      timeFlag = true;
    }
    printIfDebug("app track flag" +
        appTrackSatus.toString() +
        "timeFlag" +
        timeFlag.toString());
    if (appTrackSatus || timeFlag) {
      sendRequestToAppTrack(url, deviceInformation);
    }
  }

  sendRequestToAppTrack(url, deviceInformation) async {
    // printIfDebug('\n\napp track api, url: $url, device info: $deviceInformation');
    try {
      var response = await http.post(url, body: deviceInformation);

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var appTrackResponse = json.decode(responseBody);
        printIfDebug("The response after calling app_track");
        printIfDebug(appTrackResponse);
      } else {
        // printIfDebug('Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }

    if (!mounted) return;
  }

  void getFCMTokenInSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = (prefs.getString('spFCMToken') ?? "");
    setState(() {
      fcmToken = token;
    });

    getFcmidGaidAndroidid();

    // printIfDebug('\n\nstoring in sp, this fcm token: $token');
  }

  checkIfAppTrackRequestsNeedsToBeSent(deviceInformation, prefs) {
    String newDeviceInfo = json.encode(deviceInformation);
    if (newDeviceInfo == stringDeviceInformation) {
      printIfDebug("Perfect Match Don't send apptrack request");
      return false;
    } else {
      printIfDebug("Send apptrack request - Device Information has changed");
      setState(() {
        prefs.setString('stringDeviceInformation', newDeviceInfo);
        stringDeviceInformation = newDeviceInfo;
      });
      return true;
    }
  }

  @override
  void dispose() {
    printIfDebug("Moved out of the profile Page -> Save all changes now");

    if (profileSettingDataReady) {
      printIfDebug("Moved out of the profile Page -> Save all changes now");
      updateUserProfile();
    } else {
      printIfDebug(
          "Moved out of the profile Page -> User hasn't authenticated himself -> No chnages to save");
    }
    super.dispose();
  }

  void getCountryCode() async {
    countryCode = await getStringFromSP('userCountryCode');
  }

  constructUrlWithFilters() {
    try {
      String getUrl = "";
      String url = "";
      for (int index = 0;
          index < profileSettingData["topics"].length;
          index++) {
        if ((profileSettingData["topics"][index]["group"] ==
                "clickableImages") ||
            (profileSettingData["topics"][index]["group"] == "checkBoxes")) {
          printIfDebug("Have caught clickable images");
          printIfDebug(profileSettingData["topics"][index]["heading"]);
          bool flag = true;
          bool selected = false;
          List<String> options = [];
          for (int i = 0;
              i < profileSettingData["topics"][index]["options"].length;
              i++) {
            if (flag) {
              getUrl += profileSettingData["topics"][index]["key"] + "=";
            }
            flag = false;
            selected = true;

            if (profileSettingData["topics"][index]["options"][i]["status"]) {
              printIfDebug(
                  profileSettingData["topics"][index]["options"][i]["name"]);

              options.add(profileSettingData["topics"][index]["options"][i]
                      ["filter_id"]
                  .toString());
            }
          }
          getUrl += options.join(',');
          if (selected) {
            getUrl += "&";
          }
        }
      }
      if (profileSettingData.containsKey("hidden_topics")) {
        printIfDebug("Contains Location");
        for (int i = 0; i < profileSettingData["hidden_topics"].length; i++) {
          if (profileSettingData["hidden_topics"][i]["key"] == "location") {
            if (profileSettingData["hidden_topics"][i]["value"].isNotEmpty) {
              getUrl += profileSettingData["hidden_topics"][i]["key"] +
                  "=" +
                  profileSettingData["hidden_topics"][i]["value"] +
                  "&";
              printIfDebug("Location Added to update url");
              printIfDebug(profileSettingData["hidden_topics"][i]["value"]);
            }
            break;
          }
        }
      } else {
        printIfDebug("Location can't be set");
      }
      url = "https://api.flixjini.com/movies/update_user_profile?" +
          getUrl +
          "jwt_token=" +
          authenticationToken;

      printIfDebug('\n\nfor the update user profile api, complete url: $url');
      return url;
    } catch (e) {
      printIfDebug("profile page update issue" + e.toString());
      return "";
    }
  }

  updateUserProfile() async {
    String url = constructUrlWithFilters();
    printIfDebug("Update User Profile - Server Request url: $url");
    url = await fetchDefaultParams(url);
    printIfDebug('\n\nurl before calling update user profile api: $url');
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        printIfDebug(
          '\n\nupdate user profile response status code: ${response.statusCode}',
        );
      } else {
        printIfDebug(
          '\n\nError getting IP address:\nHttp status ${response.statusCode}',
        );
      }
    } catch (exception) {
      printIfDebug(
        '\n\nFailed getting IP address, with the exception e: $exception',
      );
    }
    if (!mounted) return;
  }

  loadFilterMenu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringprofileSettingData =
          (prefs.getString('stringprofileSettingData') ?? "");
    });
    getSearchFilterMenu(countryCode);
  }

  addClickableStatus(var data) {
    printIfDebug('\n\ndata map has: $data');
    for (int i = 0; i < data["topics"].length; i++) {
      if (data["topics"][i]["group"] == "clickableImages") {
        data["topics"][i]["status"] = true;
        printIfDebug(
            '\n\nnow added the status param: ${data["topics"][i]["status"]}');
      }
    }
    return data;
  }

  setBookNowLocation(locationBasedSources) {
    for (int i = 0; i < profileSettingData["hidden_topics"].length; i++) {
      if (profileSettingData["hidden_topics"][i]["key"] == "location") {
        setState(() {
          profileSettingData["hidden_topics"][i]["value"] =
              locationBasedSources;
        });
        printIfDebug("Updated Location");
        printIfDebug(profileSettingData["hidden_topics"][i]["value"]);
        break;
      }
    }
    saveProfileInCache();
  }

  getSearchFilterMenu(String userCountryCode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String filterMenuOptionsUrl =
        "https://api.flixjini.com/movies/get_user_profile.json?country_code=$userCountryCode&jwt_token=" +
            authenticationToken;
    String rootUrl = filterMenuOptionsUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    printIfDebug(
        '\n\nfor the get user profile api, url before sending request: $url');
    var response = await http.get(Uri.parse(url));
    try {
      printIfDebug("\n\nhey there, Inside try");
      if (response.statusCode == 200) {
        printIfDebug("\n\ngot success response status code: 200");
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug(
            '\n\ndecoded value from the get user profile api: $data'); // profile page breaking point

        printIfDebug('\n\ndata [topics] length: ${data['topics'].length}');
        var topics = data['topics'];
        for (int i = 0; i < topics.length; i++) {
          printIfDebug('\n\ni: $i');
          printIfDebug('\n\ntopics [options]: ${topics[i]['options']}');
        }

        data = addClickableStatus(data);
        printIfDebug("\n\nNeed to convert Data to String");
        String temp = json.encode(data);
        this.profileSettingData = data;
        setState(() {
          profileSettingData = this.profileSettingData;
          prefs.setString('stringprofileSettingData', temp);
          profileSettingDataReady = true;
        });
        setAuthenticationTokenFetched(true);
        checkIfAnySelected();
        printIfDebug(
            "\n\nfrom profile page, value of the variable profile setting data: $profileSettingData");
        printIfDebug(
            '\n\nprofile setting data [topics] [1]: ${profileSettingData["topics"][1]}');

        printIfDebug(
            '\n\nprofile setting data [topics] [2]: ${profileSettingData["topics"][2]}');
      } else {
        printIfDebug(
            '\n\nError getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug(
          '\n\nFailed getting IP address, with the exception e: $exception');
      setAuthenticationTokenFetched(true);
    }
    if (!mounted) return;
  }

  setAuthenticationTokenFetched(bool value) {
    setState(() {
      authenticationTokenFetched = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String alwaysOpenString = prefs.getString('alwaysOpenApp') ?? "[]";

    List alwaysOpenList1 = jsonDecode(alwaysOpenString);

    setState(() {
      alwaysopenList = alwaysOpenList1;
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
    });
    printIfDebug("Value of Login Token");
    printIfDebug('\n\nauth token from sp: $authenticationToken');
    if (authenticationToken.isEmpty) {
      printIfDebug('\n\nauth token is empty');
      setAuthenticationTokenFetched(true);
    } else {
      printIfDebug(
          '\n\nauth token is not empty and the value of auth token is: $authenticationToken');
      loadFilterMenu();
    }
  }

  setLoggedStatusToFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', "");
    });
    printIfDebug("LogIn Status on Profile Page");
    printIfDebug(authenticationToken);
    Navigator.of(context).pushReplacement(new MaterialPageRoute(
        builder: (BuildContext context) => new MovieRouting(
              defaultTab: 2,
            )));
  }

  updateShadeAllOptions(int index, bool value) {
    setState(() {
      profileSettingData["topics"][index]["status"] = value;
    });
  }

  checkIfAnySelected() {
    for (int i = 0; i < profileSettingData["topics"].length; i++) {
      if (profileSettingData["topics"][i]["group"] == "clickableImages") {
        bool flag = true;
        for (int index = 0;
            index < profileSettingData["topics"][i]["options"].length;
            index++) {
          if (profileSettingData["topics"][i]["options"][index]["status"]) {
            flag = false;
            updateShadeAllOptions(i, false);
            printIfDebug("Category Checking");
            printIfDebug(profileSettingData["topics"][i]["heading"]);
            break;
          }
        }
        if (flag) {
          printIfDebug("All Cleared");
          updateShadeAllOptions(i, true);
        }
      }
    }
  }

  uponToogle(int index, int j) {
    printIfDebug("Toogled");
    printIfDebug(profileSettingData["topics"][index]["options"][j]["name"]);
    printIfDebug(profileSettingData["topics"][index]["options"][j]["status"]);
    if (profileSettingData["topics"][index]["options"][j]["status"]) {
      printIfDebug(profileSettingData["topics"][index]["options"][j]["name"]);
      updateShadeAllOptions(index, false);
    }
  }

  void saveProfileInCache() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String temp = json.encode(profileSettingData);
    setState(() {
      prefs.setString('stringprofileSettingData', temp);
    });
  }

  Future<void> _sendAnalyticsEvent(String tagName) async {
    await analytics.logEvent(
      name: tagName.toString().toLowerCase() + '_profile',
      parameters: <String, dynamic>{},
    );
    // facebookAppEvents.logEvent(
    //   name: tagName.toString().toLowerCase() + '_profile',
    //   parameters: {
    //     'page': 'profile',
    //   },
    //   valueToSum: 1,
    // );
    if (tagName.toString().toLowerCase() == "zee5") {
      //platform.invokeMethod('logSpendCreditsEvent');
    }
    if (tagName.toString().toLowerCase() == "hindi") {
      //platform.invokeMethod('logAchieveLevelEvent');
    }
    if (tagName.toString().toLowerCase() == "hotstar") {
      //platform.invokeMethod('logDonateEvent');
    }

    printIfDebug("profile check" + tagName.toString());
  }

  void updateClickableImages(int index, int j) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (profileSettingData["topics"][index]["options"][j]["status"]) {
      setState(() {
        profileSettingData["topics"][index]["options"][j]["status"] = false;
      });
    } else {
      _sendAnalyticsEvent(profileSettingData["topics"][index]["options"][j]
              ["name"]
          .toString()
          .replaceAll(" ", ""));
      setState(() {
        profileSettingData["topics"][index]["options"][j]["status"] = true;
      });
    }

    String temp = json.encode(profileSettingData);
    setState(() {
      prefs.setString('stringprofileSettingData', temp);
    });
    printIfDebug("\n\nUpdated Clickable Image Status for\n");
    printIfDebug(
        'name: ${profileSettingData["topics"][index]["options"][j]["name"]}');
    printIfDebug(
        'status: ${profileSettingData["topics"][index]["options"][j]["status"]}');
  }

  Widget welcomeMessage(message) {
    return new Container(
      width: 100.0,
      child: new Padding(
        padding: const EdgeInsets.all(5.0),
        child: new Text(message,
            textAlign: TextAlign.center,
            style: new TextStyle(fontWeight: FontWeight.bold),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            textScaleFactor: 1.0),
      ),
    );
  }

  Widget displayLoader() {
    return Scaffold(
      appBar: appHeaderBar(),
      backgroundColor: Constants.appColorFont,
      body: new Center(
        child: ShimmerProfile(),
      ),
    );
  }

  Widget checkIfUserAuthorised() {
    if ((authenticationToken.isNotEmpty) && (profileSettingDataReady)) {
      printIfDebug(
          '\n\nbefore navigating to the movie profile settings page, value of profile setting data: $profileSettingData');
      return new MovieProfileSettings(
        profileSettingData: profileSettingData,
        setLoggedStatusToFalse: setLoggedStatusToFalse,
        welcomeMessage: welcomeMessage,
        updateClickableImages: updateClickableImages,
        checkIfAnySelected: checkIfAnySelected,
        setBookNowLocation: setBookNowLocation,
        authToken: authenticationToken,
        getSearchFilterMenu: getSearchFilterMenu,
        switchCardImages: widget.switchCardImages,
        showOriginalPosters: widget.showOriginalPosters,
        fcmToken: fcmToken,
        alwaysopenList: alwaysopenList,
      );
    } else {
      String message = "Please Sign In to Access Profile Setting";
      String imagePath = "assests/lock.flr";
      String animationName = "lock";
      return new MovieProfileUnauthenticated(
        message: message,
        imagePath: imagePath,
        animationName: animationName,
        switchCardImages: widget.switchCardImages,
      );
    }
  }

  checkDataReady() {
    try {
      if (authenticationTokenFetched) {
        return new Scaffold(
          backgroundColor: Constants.appColorL1,
          resizeToAvoidBottomInset: false,
          appBar: appHeaderBar(),
          body: checkIfUserAuthorised(),
        );
      } else {
        return displayLoader();
      }
    } catch (exception) {
      return displayLoader();
    }
  }

  AppBar appHeaderBar() {
    return new AppBar(
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () => widget.moveToPreviousTab(),
        ),
      ),
      title: new Text(
        "Profile",
        overflow: TextOverflow.ellipsis,
        style: new TextStyle(
          fontWeight: FontWeight.normal,
          color: Constants.appColorFont,
        ),
        textAlign: TextAlign.center,
        maxLines: 1,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    printIfDebug('\n\nauth token: ' + authenticationToken);
    return checkDataReady();
  }
}
