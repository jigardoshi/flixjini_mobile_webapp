import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/constants.dart';

class LogoutAlertAlert extends StatefulWidget {
  LogoutAlertAlert({
    this.checkIfGoogleSignOutIsNeeded,
  });
  final checkIfGoogleSignOutIsNeeded;

  @override
  LogoutAlertAlertState createState() => LogoutAlertAlertState();
}

class LogoutAlertAlertState extends State<LogoutAlertAlert> {
  bool cancelFlag = false, logoutFlag = false;

  deleteSPEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userEmail", "");
    prefs.setString("userPhoto", "");
  }

  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;

    return AlertDialog(
      backgroundColor: Constants.appColorL2,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            'Confirm',
            style: TextStyle(
              color: Constants.appColorFont,
            ),
          ),
          Container(
            color: Constants.appColorLogo,
            margin: EdgeInsets.only(
              top: 5,
            ),
            height: 3,
            width: 50,
          ),
        ],
      ),
      content: Container(
        height: 80,
        child: Column(
          children: <Widget>[
            Text(
              'Are you sure you want to log out?',
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 15,
              ),
              textAlign: TextAlign.center,
            ),
            Spacer(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color:
                          !cancelFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                        fontSize: 13,
                        color:
                            cancelFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      cancelFlag = true;
                    });
                    printIfDebug('\n\nuser chose no to logout');
                    Navigator.of(context).pop();
                  },
                ),
                GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 20,
                    ),
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      borderRadius: new BorderRadius.circular(30.0),
                      color:
                          !logoutFlag ? Constants.appColorL2 : Constants.appColorLogo,
                    ),
                    child: Text(
                      'Logout',
                      style: TextStyle(
                        color:
                            logoutFlag ? Constants.appColorL2 : Constants.appColorLogo,
                        fontSize: 11,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      logoutFlag = true;
                    });
                    printIfDebug('\n\nuser chose yes to logout');
                    deleteSPEmail();

                    Future.delayed(const Duration(milliseconds: 500), () {
                      widget.checkIfGoogleSignOutIsNeeded();
                    });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
