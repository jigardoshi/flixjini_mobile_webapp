import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';

import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'dart:convert';
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'dart:async';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:launch_review/launch_review.dart';
import 'package:video_player/video_player.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import 'dart:io' show Platform;
// import 'package:firebase_analytics/firebase_analytics.dart';

class AppTourOverlay extends StatefulWidget {
  AppTourOverlay({
    this.changeAppTourOverlay,
    Key? key,
  }) : super(key: key);
  final changeAppTourOverlay;
  @override
  _AppTourOverlayState createState() => new _AppTourOverlayState();
}

class _AppTourOverlayState extends State<AppTourOverlay> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    _controller = VideoPlayerController.network(
        'https://c.kmpr.in/assets/rocketium_links/Flixjini%20App%20Tour.mp4');
    // ..initialize().then((_) {
    //   // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
    //   setState(() {});
    // });
    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize();
    _controller.play();

    printIfDebug(_controller.toString());
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;

    double screenHeight = MediaQuery.of(context).size.height;

    return Container(
      height: screenHeight,
      width: screenWidth,
      color: Constants.appColorL1.withOpacity(0.9),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  widget.changeAppTourOverlay();
                  _controller.pause();
                },
                child: Container(
                  margin: EdgeInsets.only(
                    right: 10,
                  ),
                  child: Image.asset(
                    "assests/close_video.png",
                    height: 50,
                    width: 50,
                  ),
                ),
              ),
            ],
          ),
          Container(
            decoration: new BoxDecoration(
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(8.0),
                topRight: const Radius.circular(8.0),
                bottomLeft: const Radius.circular(8.0),
                bottomRight: const Radius.circular(8.0),
              ),
              color: Constants.appColorL2,
            ),
            margin: EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            width: screenWidth,
            height: screenWidth / 2,
            child: Container(
              child: _controller.value.isInitialized
                  ? VideoPlayer(_controller)
                  : getProgressBar(),
            ),
          ),
        ],
      ),
    );
  }
}
