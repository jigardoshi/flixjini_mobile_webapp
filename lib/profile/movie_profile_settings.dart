import 'package:flixjini_webapp/profile/appTour_overlay.dart';
import 'package:flixjini_webapp/queue/movie_queue_page.dart';
import 'package:flixjini_webapp/splash/movie_landing_page.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:html_unescape/html_unescape.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/profile/movie_profile_location.dart';
import 'package:flixjini_webapp/profile/logout_alert.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
// import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flixjini_webapp/feedback/feedback_overlay.dart';
// import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
// import 'package:flutter/services.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'dart:convert';
import 'dart:convert';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/constants.dart';

GoogleSignIn _googleSignIn = new GoogleSignIn(
  scopes: <String>[
    'email',
    // 'https://www.googleapis.com/auth/contacts.readonly',
    // 'https://www.googleapis.com/auth/userinfo.profile',
  ],
);

class MovieProfileSettings extends StatefulWidget {
  MovieProfileSettings({
    Key? key,
    this.profileSettingData,
    this.setLoggedStatusToFalse,
    this.welcomeMessage,
    this.updateClickableImages,
    this.checkIfAnySelected,
    this.setBookNowLocation,
    this.authToken,
    this.getSearchFilterMenu,
    this.switchCardImages,
    this.showOriginalPosters,
    this.fcmToken,
    this.alwaysopenList,
  }) : super(key: key);

  final profileSettingData,
      setLoggedStatusToFalse,
      welcomeMessage,
      updateClickableImages,
      checkIfAnySelected,
      setBookNowLocation,
      authToken,
      getSearchFilterMenu,
      switchCardImages,
      showOriginalPosters,
      fcmToken,
      alwaysopenList;

  @override
  _MovieProfileSettingsState createState() => new _MovieProfileSettingsState();
}

class _MovieProfileSettingsState extends State<MovieProfileSettings> {
  bool expandStory = false,
      showFeedback = false,
      lastFeedback = true,
      showAppTour = false,
      shakeSwitch = false;
  String showOriginalImages = 'true';
  late GoogleSignInAccount _currentUser;
  static const platform = const MethodChannel('api.komparify/advertisingid');

  // Cached variables
  String authenticationType = "";
  String stringprofileSettingData = "";
  String stringFilterData = "", userEmail = "", userPhoto = "";
  late String userCountryCode = 'IN', _radioValue;

  List<Widget> listOfWidgets = <Widget>[];
  String userEnteredFilterName = "";
  FocusNode saveFilterFocusNode = new FocusNode();
  List alwaysopenListLocal = [];

  @override
  void initState() {
    super.initState();
    alwaysopenListLocal = widget.alwaysopenList ?? [];
    getCountryCode();
    printIfDebug("Hidden Location Field");
    printIfDebug(widget.profileSettingData["hidden_topics"]);
    authenticationTypeCheck();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      printIfDebug('\n\nprofile settings.dart file, new user: $account');
      if (this.mounted) {
        // prevents memory leak
        setState(() {
          _currentUser = account!;
        });
      }
    });
    tryToSignInSilently();
    checkFeedbackFlag();
    getUserEmail();
  }

  getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      userEmail = prefs.getString("userEmail") ?? "";
      userPhoto = prefs.getString("userPhoto") ?? "";
      shakeSwitch = prefs.getBool("shakeSwitch") ?? true;
    });
  }

  changeShakeSwith(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool("shakeSwitch", value);
  }

  checkFeedbackFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? feedbackFlag = prefs.getString("feedback");
    // printIfDebug("  check////////////" +
    //     feedbackFlag +
    //     "count" +
    //     count.toString());

    if (feedbackFlag != null && feedbackFlag == "submitted") {
      setState(() {
        lastFeedback = true;
      });
    } else {
      lastFeedback = false;
    }
  }

  void getCountryCode() async {
    userCountryCode = await getStringFromSP('userCountryCode');
    if (userCountryCode == null || userCountryCode == '') {
      userCountryCode = 'IN';
    }
    printIfDebug('\n\nuser country code from sp: $userCountryCode');
    setState(() {
      _radioValue = userCountryCode;
    });
  }

  clearAuthenticationType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationType = (prefs.getString('authenticationType') ?? "");
      prefs.setString('stringprofileSettingData', "");
      prefs.setString('stringFilterData', "");
      prefs.setString('authenticationType', "");
    });
    printIfDebug("authenticationToken has been cleared due to logout");
  }

  authenticationTypeCheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationType = (prefs.getString('authenticationType') ?? "");
    });
    printIfDebug("Has the user used Gmail authentication");
    printIfDebug(authenticationType);
  }

  tryToSignInSilently() async {
    var gg = await _googleSignIn.signInSilently();
    printIfDebug("Google Constructor");
    printIfDebug("Current User");
    printIfDebug(_currentUser);
    printIfDebug("Google Sign In Object");
    printIfDebug(gg);
    printIfDebug(_googleSignIn);
  }

  bool checkIfAnyAreSelected(index) {
    bool flag = true;
    for (int i = 0;
        i < widget.profileSettingData["topics"][index]["options"].length;
        i++) {
      if (widget.profileSettingData["topics"][index]["options"][i]["status"]) {
        flag = false;
        printIfDebug("Category Checking");
        printIfDebug(widget.profileSettingData["topics"][index]["heading"]);
        break;
      }
    }
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  basedOnshadeAllOptions(int index, int i, bool selectedSectionStatus) {
    if (selectedSectionStatus) {
      return 0.0;
    } else {
      return widget.profileSettingData["topics"][index]["options"][i]["status"]
          ? 0.0
          : 0.6;
    }
  }

  Future<Null> handleSignOut() async {
    printIfDebug("Handling Sign Out");
    printIfDebug(_googleSignIn);

    await _googleSignIn.disconnect();

    printIfDebug("\n\nSuccessfully, Signed Out");
  }

  Widget googlelogoutButton() {
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(17.0),
      child: new Center(
        child: new InkWell(
          onTap: () {
            printIfDebug("GoogleLogout");
            handleSignOut();
            widget.setLoggedStatusToFalse();
          },
          child: new Container(
            height: 40.0,
            width: screenWidth * 0.50,
            padding: const EdgeInsets.all(3.0),
            decoration: new BoxDecoration(
              color: Constants.appColorLogo,
              border: new Border.all(color: Constants.appColorLogo),
              borderRadius: new BorderRadius.circular(30.0),
            ),
            child: new Center(
              child: new Text(
                "Google Logout",
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Constants.appColorFont,
                  fontSize: 15.0,
                ),
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  checkIfGoogleSignOutIsNeeded() {
    if (authenticationType.isNotEmpty) {
      printIfDebug("User used Gmail authentication");
      handleSignOut(); // sign out from google account
    } else {
      printIfDebug("User has not used Gmail authentication");
    }
    clearAuthenticationType();
    widget.setLoggedStatusToFalse();
    showToast("  Logged Out  ", 'long', 'bottom', 1, Constants.appColorLogo);

    printIfDebug("Logout");
  }

  Future<dynamic> getConfirmationForLogout() {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return LogoutAlertAlert(
          checkIfGoogleSignOutIsNeeded: checkIfGoogleSignOutIsNeeded,
        );
      },
    );
  }

  void switchAppVersion() async {
    widget.switchCardImages();
  }

  void actionForShareButtonOnTap() {
    printIfDebug('\n\nshare button is tapped');
    String shareMessage =
        'Experience the fastest way to discover "WHAT" to Stream, "WHERE" to Stream and "WHY" to Stream, only with Flixjini. Check out the Flixjini app now at\n\n';
    shareMessage += 'https://moviesapp.page.link/download';
    // Share.text("Flixjini", shareMessage, "text/plain");
  }

  Widget displayStreamingCheckboxes(index, i, selectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectionStatus);
    return Container(
      // color: Constants.appColorL3,
      // padding: const EdgeInsets.all(1.0),
      // width: 35.0,
      // height: 35.0,
      child: new Card(
        elevation: 5.0,
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () {
            widget.updateClickableImages(index, i);
            printIfDebug("Send Request to server with User Preferences");
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: getStreamingSourceImage(
                    index,
                    i,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildStreamingScource(int index, int i, bool selectedSectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectedSectionStatus);
    return Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(1.0),
      width: 35.0,
      height: 35.0,
      child: new Card(
        elevation: 0.0,
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () {
            widget.updateClickableImages(index, i);
            printIfDebug("Send Request to server with User Preferences");
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: getStreamingSourceImage(
                    index,
                    i,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getStreamingSourceImage(
    int index,
    int i,
  ) {
    if (showOriginalImages == 'true' || true) {
      return Container(
        color: Constants.appColorL3,
        child: Image(
          image: NetworkImage(
            widget.profileSettingData["topics"][index]["options"][i]["image"],
            // useDiskCache: false,
          ),
          fit: BoxFit.fill,
          gaplessPlayback: true,
        ),
      );
    } else {
      return Container();
      //   color: Constants.appColorL3,
      //   child: Image.asset(
      //     'assests/streaming_sources/' +
      //         getLocalImageFileNumber(
      //           widget.profileSettingData["topics"][index]["options"][i]
      //               ["image"],
      //         ),
      //   ),
      // );
    }
  }

  String getLocalImageFileNumber(String imageUrl) {
    String subStringToRemove =
        'https://komparify.sgp1.cdn.digitaloceanspaces.com/assets/streaming-sources/';
    imageUrl = imageUrl.replaceAll(subStringToRemove, '');

    return imageUrl;
  }

  Widget displayCheckboxes1(index, i, selectedSectionStatus) {
    return Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(10.0),
      child: new InkWell(
        onTap: () {
          widget.updateClickableImages(index, i);
        },
        child: Container(
          padding: const EdgeInsets.all(3.0),
          decoration: new BoxDecoration(
            color:
                // Constants.appColorL2,
                widget.profileSettingData["topics"][index]["options"][i]
                        ["status"]
                    ? Constants.appColorLogo
                    : Constants.appColorL3,
            border: new Border.all(
                color: widget.profileSettingData["topics"][index]["options"][i]
                        ["status"]
                    ? Constants.appColorLogo
                    : Constants.appColorL2,
                width: 2.0),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: new Text(
              widget.profileSettingData["topics"][index]["options"][i]["name"]
                  .toString()
                  .toUpperCase(),
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: widget.profileSettingData["topics"][index]["options"][i]
                        ["status"]
                    ? Constants.appColorL1
                    : Constants.appColorFont,
                letterSpacing: 1.5,
              ),
              textScaleFactor: 0.65,
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
        ),
      ),
    );
  }

  Widget displayCheckboxes(context, i, index) {
    return Card(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 5,
        right: 5,
      ),

      color: Constants.appColorL3,
      // elevation: 5.0,
      child: Container(
        color: Constants.appColorL3,
        // padding: const EdgeInsets.only(
        //   left: 5.0,
        //   right: 5.0,
        //   top: 10,
        //   bottom: 10,
        // ),

        child: new InkWell(
          onTap: () {
            widget.updateClickableImages(index, i);
          },
          child: new Container(
            // margin: EdgeInsets.all(5.0),
            padding: EdgeInsets.all(5.0),

            // width: 100.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: widget.profileSettingData["topics"][index]["options"][i]
                      ["status"]
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            child: new Center(
              child: new Text(
                widget.profileSettingData["topics"][index]["options"][i]["name"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: widget.profileSettingData["topics"][index]["options"]
                          [i]["status"]
                      ? Constants.appColorL1
                      : Constants.appColorFont.withOpacity(0.75),
                ),
                textScaleFactor: 0.5,
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildCheckBoxLanguage(int index, int i, bool selectedSectionStatus) {
    return displayCheckboxes(index, i, selectedSectionStatus);
  }

  checkIfOptionTitleNeeded(int index, int i, bool selectedSectionStatus) {
    if (widget.profileSettingData["topics"][index]["heading"] == "Languagues") {
      return Container();
    } else {
      return buildStreamingScource(index, i, selectedSectionStatus);
    }
  }

  checkIfTitleNeeded(int index) {
    var setting = {};
    if (widget.profileSettingData["topics"][index]["heading"] == "Languagues") {
      setting["numberOfOptionsPerRow"] = 4;
      setting["blockSize"] = 100.0;
      setting["childAspectRatio"] = 0.9;
      setting["crossAxisCount"] = 4;
    } else {
      setting["numberOfOptionsPerRow"] = 5;
      setting["blockSize"] = 75.0;
      setting["childAspectRatio"] = 1.0;
      setting["crossAxisCount"] = 5;
    }
    return setting;
  }

  Widget clickableImagesGrid(int index) {
    bool selectedSectionStatus = checkIfAnyAreSelected(index);

    var orientation = MediaQuery.of(context).orientation;

    return Container(
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 10,
      ),
      child: AnimationLimiter(
        child: GridView.builder(
            addAutomaticKeepAlives: false,
            shrinkWrap: true,
            physics: new NeverScrollableScrollPhysics(),
            itemCount:
                widget.profileSettingData["topics"][index]["options"].length,
            // controller: secondaryFilterController,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              crossAxisCount: (orientation == Orientation.portrait) ? 5 : 12,
            ),
            padding: EdgeInsets.all(
              10,
            ),
            itemBuilder: (BuildContext context1, int i) {
              return AnimationConfiguration.staggeredGrid(
                position: index,
                duration: const Duration(milliseconds: 375),
                columnCount: (orientation == Orientation.portrait) ? 5 : 12,
                child: ScaleAnimation(
                  child: FadeInAnimation(
                    child: displayStreamingCheckboxes(
                        index, i, selectedSectionStatus),
                  ),
                ),
              );
            }),
      ),
    );
  }

  Widget clickableImagesGridNew() {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 10,
      ),
      child: AnimationLimiter(
        child: GridView.builder(
            addAutomaticKeepAlives: false,
            shrinkWrap: true,
            physics: new NeverScrollableScrollPhysics(),
            itemCount: alwaysopenListLocal.length,
            // controller: secondaryFilterController,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              crossAxisCount: (orientation == Orientation.portrait) ? 5 : 12,
            ),
            padding: EdgeInsets.all(
              10,
            ),
            itemBuilder: (BuildContext context1, int i) {
              return AnimationConfiguration.staggeredGrid(
                position: i,
                duration: const Duration(milliseconds: 375),
                columnCount: (orientation == Orientation.portrait) ? 5 : 12,
                child: ScaleAnimation(
                  child: FadeInAnimation(
                    child: displayStreamingCheckboxesNew(
                      i,
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }

  Widget displayStreamingCheckboxesNew(i) {
    printIfDebug("https://c.kmpr.in/assets/streaming-sources/" +
        alwaysopenListLocal[i]["key_name"]
            .toString()
            .toLowerCase()
            .replaceAll("-", "") +
        ".png");
    return Container(
      // color: Constants.appColorL3,
      // padding: const EdgeInsets.all(1.0),
      // width: 35.0,
      // height: 35.0,
      child: new Card(
        elevation: 5.0,
        color: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String alwaysOpenkeysString =
                prefs.getString('alwaysOpenAppKeys') ?? "[]";

            List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);

            alwaysOpenKeysList.removeAt(i);
            setState(() {
              alwaysopenListLocal.removeAt(i);
            });

            setState(() {
              prefs.setString("alwaysOpenApp", jsonEncode(alwaysopenListLocal));
              prefs.setString(
                  "alwaysOpenAppKeys", jsonEncode(alwaysOpenKeysList));
            });
          },
          child: new Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 5, right: 5),
                child: ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL3,
                    padding: const EdgeInsets.all(0.0),
                    child: Container(
                      color: Constants.appColorL3,
                      child: Image(
                        image: NetworkImage(
                          "https://c.kmpr.in/assets/streaming-sources/" +
                              alwaysopenListLocal[i]["key_name"]
                                  .toString()
                                  .toLowerCase()
                                  .replaceAll("-", "") +
                              ".png",
                          // useDiskCache: false,
                        ),
                        fit: BoxFit.fill,
                        gaplessPlayback: true,
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // Container(
                  //   padding: EdgeInsets.all(1),
                  //   decoration: BoxDecoration(
                  //     border: Border.all(color: Constants.appColorFont),
                  //   ),
                  //   child: Icon(
                  //     Icons.close_outlined,
                  //     size: 10,
                  //     color: Constants.appColorFont,
                  //   ),
                  // ),
                  Image.asset(
                    "assests/close_circle.png",
                    height: 15,
                    width: 15,
                    // color: Constants.appColorError,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget clickableCheckboxGrid(int index) {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      // height: 200,
      child: GridView.builder(
          addAutomaticKeepAlives: false,
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          itemCount:
              widget.profileSettingData["topics"][index]["options"].length,
          // scrollDirection: Axis.vertical,
          // controller: secondaryFilterController,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 3,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            crossAxisCount: (orientation == Orientation.portrait) ? 2 : 12,
          ),
          padding: EdgeInsets.all(
            10,
          ),
          itemBuilder: (BuildContext context1, int i) {
            return displayCheckboxes(context1, i, index);
          }),
    );
  }

  displayOptionsGrid(int index) {
    if (widget.profileSettingData["topics"][index]["heading"] == "Languagues") {
      return clickableCheckboxGrid(index);
    } else if (widget.profileSettingData["topics"][index]["group"] ==
        "clickableImages") {
      return clickableImagesGrid(index);
    } else if (widget.profileSettingData["topics"][index]["group"] ==
        "checkBoxes") {
      return clickableCheckboxGrid(index);
    } else {
      return new Container();
    }
  }

  Widget getSubHeading(subheading) {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
        left: 15,
        // bottom: 14,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              top: 15,
              left: 10,
              bottom: 10,
            ),
            child: Text(
              subheading.toString().toUpperCase(),
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 14.0,
                color:
                    // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                    //     ? Constants.appColorLogo
                    //     :
                    Constants.appColorFont,
              ),
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
          ),
          Spacer(),
          subheading.toString().toLowerCase().contains('sources')
              ? containerForResetSources(
                  subheading,
                )
              : Container(),
        ],
      ),
    );
  }

  Widget renderSection(int index) {
    return Container(
      margin: EdgeInsets.only(
        top: 20,
      ),
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(5),
      //   color: Constants.appColorL2,
      // ),
      child: Card(
        elevation: 2.5,
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            getSubHeading(
                widget.profileSettingData["topics"][index]["heading"]),
            displayOptionsGrid(index),
          ],
        ),
      ),
    );
  }

  Widget containerForResetSources(String subheading) {
    return GestureDetector(
      onTap: () {
        printIfDebug('\n\nreset all the sources');
        resetAllTheSources();
      },
      child: Container(
        padding: EdgeInsets.only(
          top: 10,
          right: 10,
          // bottom: 10,
        ),
        child: Text(
          "RESET",
          style: new TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 12.0,
            color:
                // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                //     ? Constants.appColorLogo
                //     :
                Constants.appColorLogo.withOpacity(0.5),
          ),
          textAlign: TextAlign.center,
          maxLines: 2,
        ),
      ),
    );
  }

  void resetAllTheSources() {
    for (int i = 0;
        i < widget.profileSettingData["topics"][0]["options"].length;
        i++) {
      if (widget.profileSettingData['topics'][0]['options'][i]['status'] ==
          true) {
        printIfDebug(
            '\n\nsource name: ${widget.profileSettingData['topics'][0]['options'][i]['name']}, status is true');
        setState(() {
          widget.updateClickableImages(0, i);
        });
      }
    }
  }

  Widget renderLocationSection(int index) {
    return Container(
      // color: Constants.appColorL2,

      margin: EdgeInsets.only(
        top: 30,
      ),
      child: Card(
        elevation: 2.5,
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // new MovieDetailSubheading(
            //     subheading: widget.profileSettingData["hidden_topics"][index]
            //         ["heading"]),
            getSubHeading(
                widget.profileSettingData["hidden_topics"][index]["heading"]),
            // new MovieDetailUnderline(),
            new MovieProfileLocation(
              locationSection: widget.profileSettingData["hidden_topics"]
                  [index],
              setBookNowLocation: widget.setBookNowLocation,
            ),
          ],
        ),
      ),
    );
  }

  Widget locationSection() {
    bool flag = false;
    int index = 0;
    for (index = 0;
        index < widget.profileSettingData["hidden_topics"].length;
        index++) {
      if (widget.profileSettingData["hidden_topics"][index]["key"] ==
          "location") {
        flag = true;
        break;
      }
    }
    if (flag) {
      return renderLocationSection(index);
    } else {
      return new Container();
    }
  }

  Widget feedbackPoster() {
    double screenwidth = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.only(
        top: 20,
        // left: 5,
        // right: 5,
        bottom: 5,
      ),
      width: screenwidth,
      // color: Constants.appColorL2,
      decoration: BoxDecoration(
        color: Constants.appColorL2,
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  showAppTour = true;
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 10,
                    ),
                    child: Text(
                      "Take a tour of all the\n features",
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 12,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    child: Image.asset(
                      "assests/app_tour.png",
                      height: 70,
                      width: 70,
                    ),
                  ),
                  Container(
                    decoration: new BoxDecoration(
                      color: Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 5,
                      bottom: 5,
                    ),
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 10,
                    ),
                    child: Text(
                      'APP TOUR',
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontSize: 11,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 120,
            width: 1,
            color: Constants.appColorL1,
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  showFeedback = true;
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 10,
                    ),
                    child: Text(
                      "Let us know what you think of the app",
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    child: Image.asset(
                      "assests/feedback_banner.png",
                      height: 70,
                      width: 70,
                    ),
                  ),
                  Container(
                    decoration: new BoxDecoration(
                      color: Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 5,
                      bottom: 5,
                    ),
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 10,
                    ),
                    child: Text(
                      'FEEDBACK',
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontSize: 11,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget settingsList() {
    var settings = <Widget>[];
    int index = 0;
    widget.profileSettingData["topics"].forEach((topic) {
      var setting = renderSection(index);
      settings.add(setting);
      index += 1;
    });
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: settings,
    );
  }

  Widget optionsContainer(int option, String title) {
    double screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      child: Container(
        width: screenWidth / 1.5,
        margin: EdgeInsets.only(
          top: 20,
        ),
        padding: EdgeInsets.only(
          // left: 15,
          // right: 15,
          top: 10,
          bottom: 10,
        ),
        // height: 25,
        // width: 100,
        decoration: new BoxDecoration(
          border: new Border.all(
            color: Constants.appColorLogo,
          ),
          borderRadius: new BorderRadius.circular(30.0),
          color: Constants.appColorLogo,
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w900,
              color: Constants.appColorL1,
            ),
          ),
        ),
      ),
      onTap: () {
        switch (option) {
          case 1:
            Navigator.of(context).push(
              new MaterialPageRoute(
                builder: (BuildContext context) => new MovieQueuePage(
                  showOriginalPosters: widget.showOriginalPosters,
                  switchCardImages: widget.switchCardImages,
                ),
              ),
            );
            break;

          default:
        }
      },
    );
  }

  Widget profileUI(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: screenwidth * 1.0,
      child: new SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // containerForCountrySelector(),
            // userEmail.length > 0 ? emailContainer() : Container(),
            emailContainer(),

            optionsContainer(1, 'MY ACTIVITY'),

            Card(
              margin: EdgeInsets.only(
                top: 20,
              ),
              color: Constants.appColorL2,
              elevation: 2.5,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Constants.appColorL2,
                ),
                padding: EdgeInsets.only(
                  left: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Shake to Open Genie",
                        style: TextStyle(
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                    Spacer(),
                    Switch(
                        activeColor: Constants.appColorLogo,
                        activeTrackColor: Constants.appColorLogo,
                        inactiveThumbColor: Constants.appColorFont,
                        inactiveTrackColor: Constants.appColorFont,
                        value: shakeSwitch,
                        onChanged: (value) {
                          setState(() {
                            shakeSwitch = value;
                          });
                          changeShakeSwith(value);
                          if (value) {
                            setState(() {
                              detector!.startListening();
                            });
                          } else {
                            setState(() {
                              detector!.stopListening();
                            });
                          }
                        }),
                  ],
                ),
              ),
            ),
            !lastFeedback ? feedbackPoster() : Container(),
            alwaysopenListLocal.isNotEmpty
                ? Card(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    color: Constants.appColorL2,
                    elevation: 2.5,
                    child: Column(
                      children: [
                        getSubHeading("Always open In App"),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Constants.appColorL2,
                          ),
                          padding: EdgeInsets.only(),
                          child: clickableImagesGridNew(),
                        ),
                      ],
                    ),
                  )
                : Container(),
            settingsList(),
            locationSection(),

            applySave(),
            lastFeedback ? feedbackPoster() : Container(),

            callGetContainerForVersion(context),
          ],
        ),
      ),
    );
  }

  applySave() {
    return Container(
      height: 60,
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border(),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              actionForShareButtonOnTap();
              Map<String, String> eventData = {
                'share': "app_share",
                'content': "app_share",
              };
              //platform.invokeMethod('logCompleteTutorialEvent', eventData);
            },
            child: Card(
              elevation: 5,
              color: Constants.appColorL2,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child: Container(
                decoration: new BoxDecoration(
                  color: Constants.appColorL2,
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                padding: EdgeInsets.only(
                  left: 100,
                  right: 100,
                  top: 15,
                  bottom: 15,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                        right: 5,
                      ),
                      child: Image.asset(
                        "assests/share_pro.png",
                        height: 15,
                        width: 15,
                      ),
                    ),
                    Text(
                      'SHARE',
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> updateUserProfile(String userCountryCode) async {
    String url = await fetchDefaultParams(
        'https://api.flixjini.com/movies/update_user_profile.json?');

    url += '&jwt_token=${widget.authToken}';

    url += '&country_code=$userCountryCode';

    printIfDebug('\n\nfinal url before making a request: $url');

    try {
      var response = await http.post(Uri.parse(url));
      var responseBody = json.decode(response.body);
      printIfDebug('\n\nupdate user profile api, response body: $responseBody');
    } catch (exception) {
      printIfDebug('\n\nupdate user profile api, got exception: $exception');
    }
  }

  Widget getOneOption(String countryCode, String countryName) {
    return Container(
      // color: Constants.appColorL3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Constants.appColorL3,
      ),
      child: Row(
        children: <Widget>[
          Radio(
            activeColor: Constants.appColorLogo,
            value: countryCode,
            groupValue: _radioValue,
            onChanged: (String? value) {
              setState(() {
                _radioValue = value!;
              });
              updateUserProfile(value!);
              if (value == 'IN') {
                widget.getSearchFilterMenu('IN');
              } else {
                widget.getSearchFilterMenu('SG');
              }
            },
          ),
          Text(
            countryName,
            style: TextStyle(
              color: Constants.appColorFont,
            ),
          ),
        ],
      ),
    );
  }

  Widget containerForCountrySelector() {
    return Container(
      padding: EdgeInsets.only(
        // left: 15,
        top: 10,
      ),
      color: Constants.appColorL2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 15,
            ),
            child: profilePageCountry(),
          ), // MovieDetailUnderline(),
          getOneOption('IN', 'IN - India'),
          getOneOption('SG', 'SG - Singapore'),
        ],
      ),
    );
  }

  Widget profilePageCountry() {
    return MovieDetailSubheading(subheading: 'Country');
  }

  Widget callGetContainerForVersion(BuildContext context) {
    return Container(
      child: InkWell(
        onLongPress: () {
          printIfDebug('\n\nlong press detected');
          // showToast(
          //   "Magic switch is toggled!",
          //   'long',
          //   'bottom',
          //   1,
          //   Constants.appColorLogo,
          // );
          // switchAppVersion();
          showToast(
            "Debug info copied to clipboard",
            'long',
            'bottom',
            1,
            Constants.appColorLogo,
          );
          String authToken = widget.authToken ?? "";
          String fcmToken = widget.fcmToken ?? "";
          Clipboard.setData(
            new ClipboardData(text: authToken + " , " + fcmToken),
          );
        },
        child: getContainerForVersion(),
      ),
    );
  }

  changeFeedback(bool flag) {
    setState(() {
      showFeedback = flag;
    });
  }

  Widget showFeedbackOverlay() {
    return FeedbackOverlay(
      changeFeedback: changeFeedback,
      authenticationToken: widget.authToken,
    );
  }

  changeAppTourOverlay() {
    setState(() {
      showAppTour = false;
    });
  }

  Widget showAppTourOverlay() {
    return AppTourOverlay(
      changeAppTourOverlay: changeAppTourOverlay,
    );
  }

  Widget emailContainer() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Constants.appColorL2,
        borderRadius: new BorderRadius.circular(5.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            child: ClipRRect(
              borderRadius: new BorderRadius.circular(50.0),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Constants.appColorL3,
                ),
                child: userPhoto != null && userPhoto.length > 0
                    ? Image.network(
                        userPhoto,
                        fit: BoxFit.fill,
                      )
                    : Image.asset(
                        "assests/user_pro.png",
                        fit: BoxFit.fill,
                      ),
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: GestureDetector(
              onTap: () {
                // printIfDebug("object");
                // String finalUrl = "";
                // String type = "POST";
                // String url = 'https://sandbox.woohoo.in/rest/v3/orders';
                // var urlencoded =
                //     "https%3A%2F%2Fsandbox.woohoo.in%2Frest%2Fv3%2Forders";
                // finalUrl = type + "&" + urlencoded;
                // printIfDebug("A= " + type);
                // printIfDebug("B= " + url);
                // printIfDebug("C= " + urlencoded);
                // printIfDebug("D= " + finalUrl);
                // // String body =
                // //     '{"billing":{"firstname":"Jigar","lastname":"Jigar","email":"jigar@komparify.com","telephone":"9500043410","line1":"560 Anna Salai","line2":"Chennai","city":"Chennai","region":"Tamil Nadu","country":"IN","postcode":"600018","billToThis":false},"address":{"firstname":"Ankit","lastname":"Ankit","email":"ankit@komparify.com","telephone":"9500043410","line1":"560 Anna Salai","line2":"Teynampet","city":"Chennai","region":"Tamil Nadu","country":"IN","postcode":"600018","billToThis":true},"payments":[{"code":"svc","amount":100}],"refno":"123456789","products":[{"sku":"EGCGBAMZ001","price":100,"qty":1,"currency":356}]}';
                // // String body =
                // //     '{"address":{"billToThis":true,"city":"Chennai","country":"IN","email":"ankit@komparify.com","firstname":"Ankit","lastname":"Ankit","line1":"560AnnaSalai","line2":"Teynampet","postcode":"600018","region":"TamilNadu","telephone":"9500043410"},"billing":{"billToThis":false,"city":"Chennai","country":"IN","email":"jigar@komparify.com","firstname":"Jigar","lastname":"Jigar","line1":"560AnnaSalai","line2":"Chennai","postcode":"600018","region":"TamilNadu","telephone":"9500043410"},"payments":[{"amount":100,"code":"svc"}],"products":[{"currency":356,"price":100,"qty":1,"sku":"EGCGBAMZ001"}],"refno":"123456789"}';
                // String body =
                //     '{"address":{"billToThis":true,"city":"Chennai","country":"IN","email":"ankit@komparify.com","firstname":"Ankit","lastname":"Ankit","line1":"560AnnaSalai","line2":"Teynampet","postcode":"600018","region":"TamilNadu","telephone":"9500043410"},"billing":{"city":"Chennai","country":"IN","email":"jigar@komparify.com","firstname":"Jigar","lastname":"Jigar","line1":"560AnnaSalai","line2":"Chennai","postcode":"600018","region":"TamilNadu","telephone":"9500043410"},"payments":[{"amount":100,"code":"svc"}],"products":[{"currency":356,"price":100,"qty":1,"sku":"EGCGBAMZ001"}],"refno":"123456789"}';
                // var bodyJson = {};
                // // bodyJson = json.decode(body);
                // printIfDebug("E= " + body.toString());
                // // var bodyencoded = Uri.encodeFull(body);
                // String bodyencoded =
                //     '%7B%0D%0A%09%22address%22%3A+%7B%0D%0A%09%09%22billToThis%22%3A+true%2C%0D%0A%09%09%22city%22%3A+%22Chennai%22%2C%0D%0A%09%09%22country%22%3A+%22IN%22%2C%0D%0A%09%09%22email%22%3A+%22ankit%40komparify.com%22%2C%0D%0A%09%09%22firstname%22%3A+%22Ankit%22%2C%0D%0A%09%09%22lastname%22%3A+%22Ankit%22%2C%0D%0A%09%09%22line1%22%3A+%22560+Anna+Salai%22%2C%0D%0A%09%09%22line2%22%3A+%22Teynampet%22%2C%0D%0A%09%09%22postcode%22%3A+%22600018%22%2C%0D%0A%09%09%22region%22%3A+%22Tamil+Nadu%22%2C%0D%0A%09%09%22telephone%22%3A+%229500043410%22%0D%0A%09%7D%2C%0D%0A%09%22billing%22%3A+%7B%0D%0A%09%09%22billToThis%22%3A+false%2C%0D%0A%09%09%22city%22%3A+%22Chennai%22%2C%0D%0A%09%09%22country%22%3A+%22IN%22%2C%0D%0A%09%09%22email%22%3A+%22jigar%40komparify.com%22%2C%0D%0A%09%09%22firstname%22%3A+%22Jigar%22%2C%0D%0A%09%09%22lastname%22%3A+%22Jigar%22%2C%0D%0A%09%09%22line1%22%3A+%22560+Anna+Salai%22%2C%0D%0A%09%09%22line2%22%3A+%22Chennai%22%2C%0D%0A%09%09%22postcode%22%3A+%22600018%22%2C%0D%0A%09%09%22region%22%3A+%22Tamil+Nadu%22%2C%0D%0A%09%09%22telephone%22%3A+%229500043410%22%0D%0A%09%7D%2C%0D%0A%09%22payments%22%3A+%5B%7B%0D%0A%09%09%22amount%22%3A+100%2C%0D%0A%09%09%22code%22%3A+%22svc%22%0D%0A%09%7D%5D%2C%0D%0A%09%22products%22%3A+%5B%7B%0D%0A%09%09%22currency%22%3A+356%2C%0D%0A%09%09%22price%22%3A+100%2C%0D%0A%09%09%22qty%22%3A+1%2C%0D%0A%09%09%22sku%22%3A+%22EGCGBAMZ001%22%0D%0A%09%7D%5D%2C%0D%0A%09%22refno%22%3A+%22123456789%22%0D%0A%7D';
                // printIfDebug("F= " + bodyencoded);
                // // printIfDebug(bodyencoded);
                // finalUrl = finalUrl + "&" + bodyencoded;
                // printIfDebug(finalUrl);

                // finalUrl = constructHeader512(finalUrl);
                // printIfDebug("Signature= " + finalUrl);
              },
              child: Container(
                margin: EdgeInsets.only(
                  left: 5,
                  right: 5,
                ),
                child: Text(
                  userEmail,
                  style: TextStyle(
                    color: Constants.appColorFont,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: GestureDetector(
              child: Container(
                margin: EdgeInsets.only(
                  right: 5,
                  left: 5,
                ),
                padding: EdgeInsets.only(
                  // left: 15,
                  // right: 15,
                  top: 5,
                  bottom: 5,
                ),
                height: 25,
                width: 100,
                decoration: new BoxDecoration(
                  border: new Border.all(
                    color: Constants.appColorLogo,
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                  color: Constants.appColorL3,
                ),
                child: Center(
                  child: Text(
                    'LOGOUT',
                    style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                      color: Constants.appColorLogo,
                    ),
                  ),
                ),
              ),
              onTap: () {
                getConfirmationForLogout();
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;

    double screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        profileUI(context),
        showFeedback
            ? Container(
                height: screenHeight,
                width: screenWidth,
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(8.0),
                    topRight: const Radius.circular(8.0),
                  ),
                  color: Constants.appColorL1.withOpacity(
                    0.9,
                  ),
                ),
              )
            : Container(),
        showFeedback ? showFeedbackOverlay() : Container(),
        showAppTour ? showAppTourOverlay() : Container(),
      ],
    );
  }
}
