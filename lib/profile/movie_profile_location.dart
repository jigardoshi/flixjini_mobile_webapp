import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/book_now/movie_detail_book_now_sources.dart';
// import 'package:url_launcher/url_launcher.dart';
// import	'package:streaming_entertainment/detail/movie_detail_webview.dart';
import 'dart:async';
import 'dart:convert';
// import 	'dart:io';
import 'package:http/http.dart' as http;
// import 	'package:streaming_entertainment/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieProfileLocation extends StatefulWidget {
  MovieProfileLocation(
      {Key? key, this.locationSection, this.setBookNowLocation})
      : super(key: key);

  final locationSection;
  final setBookNowLocation;

  @override
  _MovieProfileLocationState createState() => new _MovieProfileLocationState();
}

class _MovieProfileLocationState extends State<MovieProfileLocation> {
  final TextEditingController searchController = new TextEditingController();
  String searchString = "";
  bool waiting = false;
  var autoCompleteOptions;

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  Future delayServerRequest() async {
    if (searchString.length >= 4) {
      if (waiting) {
        printIfDebug("Still Waiting..");
      } else {
        String checkString = searchString;
        printIfDebug("Before Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        new Timer(const Duration(seconds: 1), () {
          printIfDebug("After Timeout");
          printIfDebug("Search (Current) String");
          printIfDebug(searchString);
          printIfDebug("Check (Previous) String");
          printIfDebug(checkString);
          if (searchString != checkString) {
            printIfDebug("Dont Make Server Request");
          } else {
            printIfDebug("Make Server Request : Elastic Search");
            printIfDebug("Keshav");
            String appliedMoviesFilterUrl =
                "https://api.flixjini.com/citysearch/" + searchString + ".json";
            printIfDebug(appliedMoviesFilterUrl);
            sendElasticGetRequest(appliedMoviesFilterUrl);
          }
        });
      }
    }
  }

  sendElasticGetRequest(actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    http.get(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      var fetchedData = response.body;
      var jsonFetchedData = json.decode(fetchedData);
      setState(() {
        autoCompleteOptions = jsonFetchedData;
      });
      printIfDebug("New Data Fetched by Elastic Search for Auto Complete");
      printIfDebug(autoCompleteOptions);
    });
  }

  Widget searchButton() {
    String defaultLocation = "Set Default Location";
    if (widget.locationSection["value"].length > 0) {
      defaultLocation = widget.locationSection["value"];
    }
    return new Container(
      padding: const EdgeInsets.all(2.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: 250.0,
            padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
            child: new Theme(
              data: new ThemeData(
                primaryColor: Constants.appColorFont,
                primaryColorDark: Constants.appColorFont,
                hintColor: Constants.appColorDivider,
              ),
              child: new TextField(
                style: new TextStyle(
                  color: Constants.appColorFont,
                ),
                cursorColor: Constants.appColorFont,
                textInputAction: TextInputAction.go,
                onSubmitted: (value) {
                  printIfDebug("Submitted");
                  printIfDebug(value);
                },
                controller: searchController,
                decoration: new InputDecoration(
                  filled: true,
                  hintStyle: new TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 13,
                  ),
                  hintText: defaultLocation.toUpperCase(),
                  fillColor: Constants.appColorL2,
                ),
                onChanged: (value) {
                  setSearchStringValue(value);
                  delayServerRequest();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.all(2.0),
      child: SizedBox(
        height: 2.5,
        width: 100.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorLogo,
          ),
        ),
      ),
    );
  }

  Widget buildOptions(BuildContext context, int index) {
    return new Container(
      padding: const EdgeInsets.all(5.0),
      child: new GestureDetector(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              height: 40.0,
              width: 100.0,
              color: Constants.appColorL2,
              padding: const EdgeInsets.all(2.0),
              child: new Center(
                child: new Text(
                  autoCompleteOptions[index].toString().toUpperCase(),
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    color: Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ),
            ),
            insertDivider(),
          ],
        ),
        onTap: () {
          searchController.clear();
          printIfDebug("Clear TextField");
          printIfDebug("Selected : " + autoCompleteOptions[index]);
          widget.setBookNowLocation(autoCompleteOptions[index]);
          setState(() {
            autoCompleteOptions.clear();
          });
        },
      ),
    );
  }

  Widget autoCompleteList() {
    if ((autoCompleteOptions == null) || (autoCompleteOptions.isEmpty)) {
      return new Container(
        color: Constants.appColorL2,
      );
    } else {
      printIfDebug(autoCompleteOptions);
      return new Container(
        color: Constants.appColorL2,
        child: new SizedBox.fromSize(
          size: const Size.fromHeight(100.0),
          child: new ListView.builder(
            itemCount: autoCompleteOptions.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(10.0),
            itemBuilder: buildOptions,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            padding: EdgeInsets.only(
              bottom: 20,
            ),
            color: Constants.appColorL2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: 5,
                    top: 10,
                  ),
                  child: Image.asset(
                    "assests/location_pro.png",
                    height: 30,
                    width: 30,
                  ),
                ),
                new Center(
                  child: searchButton(),
                ),
              ],
            ),
          ),
          autoCompleteList(),
        ],
      ),
    );
  }
}
