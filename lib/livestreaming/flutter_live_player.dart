// import 'dart:convert';

// import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:better_player/better_player.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
// import 'package:http/http.dart' as http;
// import 'package:wakelock/wakelock.dart';
// // import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flixjini_webapp/common/constants.dart';

// // import 'flixjini_better_player.dart';

// class FlutterLivePlayer extends StatefulWidget {
//   FlutterLivePlayer({
//     Key? key,
//     this.url,
//     this.isLive,
//     this.title,
//     this.liveTvData,
//     this.currentChannel,
//     this.currentTag,
//   }) : super(key: key);
//   final url;
//   final isLive;
//   final title;
//   final liveTvData;
//   final currentChannel;
//   final currentTag;
//   @override
//   _FlutterLivePlayerState createState() => new _FlutterLivePlayerState();
// }

// class _FlutterLivePlayerState extends State<FlutterLivePlayer> {
//   String selectedTag = "",
//       nowPlayingName = "",
//       nowPlayingTime1 = "",
//       nowPlayingTime2 = "",
//       title = "";
//   late BetterPlayerController? _betterPlayerController;
//   int currentTag = 0, currentChannel = 0;
//   bool channelFetched = false, epgFetched = false, showPage = true;
//   List epgData = [], channelData = [];

//   void initState() {
//     super.initState();
//     try {
//       Wakelock.enable(); // or Wakelock.toggle(on: true);

//       title = widget.liveTvData[widget.currentTag]["channels"]
//           [widget.currentChannel]["name"];
//       currentTag = widget.currentTag;
//       currentChannel = widget.currentChannel;
//       BetterPlayerDataSource betterPlayerDataSource = BetterPlayerDataSource(
//         BetterPlayerDataSourceType.network,
//         widget.url,
//         liveStream: widget.isLive,
//       );
//       _betterPlayerController = BetterPlayerController(
//           BetterPlayerConfiguration(
//             autoPlay: true,
//           ),
//           betterPlayerDataSource: betterPlayerDataSource);
//       getEpgData(widget.liveTvData[widget.currentTag]["channels"]
//               [widget.currentChannel]["offering_name_id"]
//           .toString());
//       getChannelList();
//       nowPlayingName = widget
//                   .liveTvData[widget.currentTag]["channels"]
//                       [widget.currentChannel]["epg"]
//                   .length >
//               0
//           ? widget.liveTvData[widget.currentTag]["channels"]
//               [widget.currentChannel]["epg"][0]["show_name"]
//           : "";
//       nowPlayingTime1 = widget
//                   .liveTvData[widget.currentTag]["channels"]
//                       [widget.currentChannel]["epg"]
//                   .length >
//               0
//           ? widget.liveTvData[widget.currentTag]["channels"]
//                   [widget.currentChannel]["epg"][0]["start_time"]
//               .toString()
//           : "";
//       nowPlayingTime2 = widget
//                   .liveTvData[widget.currentTag]["channels"]
//                       [widget.currentChannel]["epg"]
//                   .length >
//               0
//           ? " - " +
//               widget.liveTvData[widget.currentTag]["channels"]
//                       [widget.currentChannel]["epg"][0]["end_time"]
//                   .toString()
//           : "";
//     } catch (e) {
//       printIfDebug("shakthi error 1  " + e.toString());
//     }
//     _betterPlayerController!.addEventsListener((BetterPlayerEvent) {
//       if (_betterPlayerController!.isPlaying() == true) {
//         Wakelock.enable(); // or Wakelock.toggle(on: true);

//       } else {
//         Wakelock.disable(); // or Wakelock.toggle(on: false);

//       }
//     });
//   }

//   getEpgData(String channelId) async {
//     String url =
//         "https://api.flixjini.com/tvapp_epg/channel/" + channelId + "?";
//     url = constructHeader(url);
//     url = await fetchDefaultParams(url);
//     printIfDebug(url);
//     try {
//       var response = await http.get(Uri.parse(url));
//       printIfDebug(
//         '\n\njson response status code for movie details page: ' +
//             response.statusCode.toString(),
//       );

//       if (response.statusCode == 200) {
//         String responseBody = response.body;
//         var data = json.decode(responseBody);
//         this.epgData = data;
//         printIfDebug(data);
//         setState(() {
//           epgData = this.epgData;
//           epgFetched = true;
//         });
//       } else {
//         printIfDebug(
//             'Error getting IP address:\nHttp status ${response.statusCode}');
//       }
//     } catch (exception) {
//       printIfDebug('\n\ndetails page api, exception caught is: $exception');
//     }
//   }

//   getChannelList() async {
//     String url = "https://api.flixjini.com/tvapp_epg/playable_channel_epg?";
//     url = constructHeader(url);
//     url = await fetchDefaultParams(url);
//     printIfDebug(url);
//     try {
//       var response = await http.get(Uri.parse(url));
//       printIfDebug(
//         '\n\njson response status code for movie details page: ' +
//             response.statusCode.toString(),
//       );

//       if (response.statusCode == 200) {
//         String responseBody = response.body;
//         var data = json.decode(responseBody);
//         this.channelData = data;
//         printIfDebug(data);
//         setState(() {
//           channelData = this.channelData;
//           channelFetched = true;
//         });
//       } else {
//         printIfDebug(
//             'Error getting IP address:\nHttp status ${response.statusCode}');
//       }
//     } catch (exception) {
//       printIfDebug('\n\ndetails page api, exception caught is: $exception');
//     }
//   }

//   setPlayer(String newUrl) {
//     try {
//       try {
//         // _betterPlayerController.pause();
//         // _betterPlayerController.dispose();
//         BetterPlayerDataSource betterPlayerDataSource = BetterPlayerDataSource(
//           BetterPlayerDataSourceType.network,
//           newUrl,
//           liveStream: true,
//         );
//         BetterPlayerController _betterPlayerController1;
//         _betterPlayerController1 = BetterPlayerController(
//             BetterPlayerConfiguration(
//               autoDetectFullscreenDeviceOrientation: true,
//               autoPlay: true,
//               systemOverlaysAfterFullScreen: SystemUiOverlay.values,
//             ),
//             betterPlayerDataSource: betterPlayerDataSource);
//         setState(() {
//           _betterPlayerController = null;
//           _betterPlayerController = _betterPlayerController1;
//         });
//       } catch (e) {
//         printIfDebug("shakthi error 2   " + e.toString());
//       }
//     } catch (e) {
//       printIfDebug("shakthi error 5" + e.toString());
//     }
//     Future.delayed(const Duration(milliseconds: 200), () {
//       setState(() {
//         showPage = true;
//       });
//     });
//   }

//   @override
//   void dispose() {
//     // _betterPlayerController.pause();
//     Wakelock.disable(); // or Wakelock.toggle(on: false);

//     _betterPlayerController!.dispose();
//     super.dispose();
//   }

//   Widget titleBox(String title) {
//     return Text(
//       title,
//       style: TextStyle(
//         color: Constants.appColorFont,
//         fontSize: 10,
//         letterSpacing: 1,
//         fontWeight: FontWeight.w600,
//       ),
//     );
//   }

//   Widget insertDivider(screenWidth) {
//     return Container(
//       margin: EdgeInsets.only(
//         left: 10,
//         right: 20,
//         top: 10,
//         bottom: 15,
//       ),
//       height: 1,
//       color: Constants.appColorL2,
//       width: screenWidth,
//     );
//   }

//   Widget bodyContainerNew() {
//     double screenHeight = MediaQuery.of(context).size.height;
//     double screenWidth = MediaQuery.of(context).size.width;
//     return Container(
//       // height: screenHeight,
//       // width: screenWidth,
//       margin: EdgeInsets.only(
//         // torp: 20,
//         left: 10,
//       ),
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Container(
//             margin: EdgeInsets.only(
//               top: 10,
//               bottom: 10,
//             ),
//             child: Row(
//               children: [
//                 titleBox("NOW PLAYING"),
//                 Icon(
//                   Icons.play_arrow_rounded,
//                   color: Constants.appColorLogo,
//                 ),
//               ],
//             ),
//           ),
//           Text(
//             nowPlayingName,
//             style: TextStyle(
//                 color: Constants.appColorFont,
//                 fontSize: 18,
//                 letterSpacing: 1,
//                 fontWeight: FontWeight.bold),
//           ),
//           Container(
//             margin: EdgeInsets.only(
//               top: 10,
//             ),
//             child: Row(
//               children: [
//                 Text(
//                   nowPlayingTime1,
//                   style: TextStyle(
//                       color: Constants.appColorFont,
//                       fontSize: 12,
//                       letterSpacing: 1,
//                       fontWeight: FontWeight.bold),
//                 ),
//                 Text(
//                   nowPlayingTime2,
//                   style: TextStyle(
//                       color: Constants.appColorFont,
//                       fontSize: 12,
//                       letterSpacing: 1,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ],
//             ),
//           ),
//           insertDivider(screenWidth),
//           titleBox("UP NEXT"),
//           Container(
//             height: 75,
//             child: epgFetched
//                 ? ListView.builder(
//                     // physics: NeverScrollableScrollPhysics(),
//                     shrinkWrap: true,
//                     scrollDirection: Axis.horizontal,
//                     itemCount: epgData.length,
//                     itemBuilder: (BuildContext context, int index) {
//                       if (index == 0) {
//                         return Container();
//                       } else {
//                         return Container(
//                           // height: 75,
//                           child: Row(
//                             children: [
//                               Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   Text(
//                                     epgData[index]["show_name"] ?? "",
//                                     style: TextStyle(
//                                         color: Constants.appColorFont,
//                                         fontSize: 15,
//                                         letterSpacing: 1,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   Container(
//                                     margin: EdgeInsets.only(
//                                       top: 10,
//                                     ),
//                                     child: Row(
//                                       children: [
//                                         Text(
//                                           epgData[index]["start_time"]
//                                                   .toString()
//                                                   .split(" ")
//                                                   .first ??
//                                               "",
//                                           style: TextStyle(
//                                               color: Constants.appColorFont,
//                                               fontSize: 10,
//                                               letterSpacing: 1,
//                                               fontWeight: FontWeight.bold),
//                                         ),
//                                         Text(
//                                           " - " +
//                                                   epgData[index]["end_time"]
//                                                       .toString()
//                                                       .split(" ")
//                                                       .first ??
//                                               "",
//                                           style: TextStyle(
//                                               color: Constants.appColorFont,
//                                               fontSize: 12,
//                                               letterSpacing: 1,
//                                               fontWeight: FontWeight.bold),
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                               Container(
//                                 margin: EdgeInsets.only(
//                                   left: 15,
//                                   right: 15,
//                                 ),
//                                 width: 1,
//                                 color: Constants.appColorL2,
//                               ),
//                             ],
//                           ),
//                         );
//                       }
//                     },
//                   )
//                 : getProgressBar(),
//           ),
//           insertDivider(screenWidth),
//           titleBox(widget.liveTvData[currentTag]["tag_name"]
//                   .toString()
//                   .toUpperCase() +
//               " CHANNELS"),
//           Container(
//             height: 100,
//             margin: EdgeInsets.only(
//               top: 20,
//               // bottom: 10,
//             ),
//             child: channelFetched
//                 ? ListView.builder(
//                     // physics: NeverScrollableScrollPhysics(),
//                     shrinkWrap: true,
//                     scrollDirection: Axis.horizontal,
//                     itemCount: channelData.length,
//                     itemBuilder: (BuildContext context, int index) {
//                       return AnimationConfiguration.staggeredList(
//                         position: index,
//                         duration: const Duration(milliseconds: 375),
//                         child: SlideAnimation(
//                           verticalOffset: 50.0,
//                           child: FadeInAnimation(
//                             child: GestureDetector(
//                               onTap: () {
//                                 if (index != currentChannel) {
//                                   setState(() {
//                                     showPage = false;
//                                     title = channelData[index]["name"] ?? "";
//                                     currentChannel = index;
//                                     epgFetched = false;

//                                     nowPlayingName =
//                                         channelData[index]["epg"].length > 0
//                                             ? channelData[index]["epg"][0]
//                                                 ["show_name"]
//                                             : "";
//                                     nowPlayingTime1 =
//                                         channelData[index]["epg"].length > 0
//                                             ? channelData[index]["epg"][0]
//                                                     ["start_time"]
//                                                 .toString()
//                                             : "";
//                                     nowPlayingTime2 =
//                                         channelData[index]["epg"].length > 0
//                                             ? " - " +
//                                                 channelData[index]["epg"][0]
//                                                         ["end_time"]
//                                                     .toString()
//                                             : "";
//                                   });
//                                   getEpgData(channelData[index]["channel_id"]
//                                       .toString());
//                                   setPlayer(channelData[index]["playability"][0]
//                                       ["stream_link"]);
//                                 }
//                               },
//                               child: Container(
//                                 width: currentChannel == index ? 60 : 45,
//                                 margin: EdgeInsets.only(left: 15, right: 15),
//                                 child: Column(
//                                   children: [
//                                     Image.network(
//                                       channelData[index]["channel_logo"]
//                                           .toString()
//                                           .replaceAll(".png", ".jpg"),
//                                       height:
//                                           title == channelData[index]["name"]
//                                               ? 60
//                                               : 45,
//                                       width: title == channelData[index]["name"]
//                                           ? 60
//                                           : 45,
//                                     ),
//                                     currentChannel == index
//                                         ? Container(
//                                             padding: EdgeInsets.only(
//                                               // left: 5,
//                                               // right: 5,
//                                               top: 5,
//                                             ),
//                                             child: Text(
//                                               channelData[index]["name"] ?? "",
//                                               style: TextStyle(
//                                                   color: Constants.appColorFont,
//                                                   fontSize: 10,
//                                                   fontWeight: FontWeight.w600),
//                                               textAlign: TextAlign.center,
//                                             ),
//                                           )
//                                         : Container(),
//                                   ],
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       );
//                     },
//                   )
//                 : getProgressBar(),
//           ),
//           insertDivider(screenWidth),
//         ],
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     // setPlayer();
//     try {
//       try {
//         return Scaffold(
//           appBar: AppBar(
//             backgroundColor: Constants.appColorL2,
//             iconTheme: IconThemeData(color: Constants.appColorDivider),
//             title: GestureDetector(
//               onTap: () {
//                 // _betterPlayerController.enterFullScreen();
//               },
//               child: Text(
//                 title ?? "",
//               ),
//             ),
//           ),
//           backgroundColor: Constants.appColorL1,
//           body:
//               // FlixjiniBetterPlayer(
//               //   url: widget.url,
//               //   liveTv: widget.isLive,
//               // ),
//               showPage
//                   ? Column(
//                       children: [
//                         Container(
//                           child: AspectRatio(
//                             aspectRatio: 16 / 9,
//                             child: BetterPlayer(
//                               controller: _betterPlayerController!,
//                             ),
//                           ),
//                         ),
//                         !_betterPlayerController!.isFullScreen
//                             ? bodyContainerNew()
//                             : Container(),
//                       ],
//                     )
//                   : getProgressBar(),
//         );
//       } catch (e) {
//         printIfDebug("shakthi error 3   " + e.toString());
//       }
//     } catch (e) {
//       printIfDebug("shakthi error 4   " + e.toString());
//     }
//     return getProgressBar();
//   }
// }
