// import 'package:flutter/material.dart';
// // import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:better_player/better_player.dart';

// class FlixjiniBetterPlayer extends StatefulWidget {
//   FlixjiniBetterPlayer({Key? key, this.url, this.liveTv}) : super(key: key);
//   final liveTv;
//   final String? url;

//   @override
//   _FlixjiniBetterPlayerState createState() => _FlixjiniBetterPlayerState();
// }

// class _FlixjiniBetterPlayerState extends State<FlixjiniBetterPlayer> {
//   late BetterPlayerController _betterPlayerController;

//   @override
//   void initState() {
//     super.initState();
//     BetterPlayerDataSource betterPlayerDataSource = BetterPlayerDataSource(
//       BetterPlayerDataSourceType.network,
//       widget.url!,
//       liveStream: widget.liveTv,
//     );
//     _betterPlayerController = BetterPlayerController(
//         BetterPlayerConfiguration(),
//         betterPlayerDataSource: betterPlayerDataSource);
//     _betterPlayerController.play();
//   }

//   @override
//   void dispose() {
//     _betterPlayerController.pause();
//     _betterPlayerController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: AspectRatio(
//         aspectRatio: 16 / 9,
//         child: BetterPlayer(
//           controller: _betterPlayerController,
//         ),
//       ),
//     );
//   }
// }
