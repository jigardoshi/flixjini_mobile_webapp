import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/authentication/movie_authentication_social_networks.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'dart:async';
// import 'package:quiver/async.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import  'package:google_sign_in/google_sign_in.dart';
// import  'package:flutter/services.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:device_info/device_info.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart' as FlixjiniUtils;
// import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flutter/cupertino.dart';
// import 'package:connectivity/connectivity.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:flixjini_webapp/common/constants.dart';

import 'dart:io' show Platform;

class ResetPassword extends StatefulWidget {
  ResetPassword({
    this.type,
    this.fromTutorialPage,
    this.resetPassword,
    Key? key,
  }) : super(key: key);

  final bool? type, fromTutorialPage;
  final resetPassword;

  @override
  ResetPasswordState createState() => new ResetPasswordState();
}

class ResetPasswordState extends State<ResetPassword> {
  bool newUser = true;
  bool blurFlag = false, loading = false, routeFlag = false;
  String authenticationToken = "";
  String setEmail = "";
  String setPassword = "", deviceMake = "";
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _autoValidate = false;
  String password = "";
  String repeatPassword = "";
  String email = "";
  String errorMessage = "", pinText = "";
  bool receivedOTP = false, showPassword = false;
  FocusNode _focusNodeEmail = new FocusNode();
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  int timerNumber = 30;
  TextEditingController pinController = TextEditingController();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    assignLoginOrSignUp();
    // checkConnectivity();
    // fetchMobileMake();

    logUserScreen('Flixjini Authentication Page', 'ResetPassword');
  }

  fetchMobileMake() async {
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    // AndroidDeviceInfo androidInfo;
    // androidInfo = await deviceInfo.androidInfo;
    // setState(() {
    //   deviceMake = androidInfo.manufacturer;
    // });
    // printIfDebug("maker/////////////" + deviceMake);

    // return deviceManu;
  }

  // void checkConnectivity() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.mobile) {
  //     printIfDebug('\n\nconnected to a mobile network.');
  //   } else if (connectivityResult == ConnectivityResult.wifi) {
  //     printIfDebug('\n\nconnected to a wifi network.');
  //   } else {
  //     FlixjiniUtils.showCupertinoDialog(context, 'No Internet',
  //         "Please make sure that you've proper Internet Connection");
  //   }
  // }

  assignLoginOrSignUp() {
    if (widget.type == null) {
      setState(() {
        newUser = true;
      });
    } else {
      setState(() {
        newUser = widget.type!;
      });
    }
  }

  setBlurFlag(bool value) {
    setState(() {
      blurFlag = value;
    });
  }

  sendEvent(String method) {
    Map<String, String> eventData = {
      'method': method,
    };
    //platform.invokeMethod('logCompleteRegistrationEvent', eventData);
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      printIfDebug("User is already Logged in -> Move to Index Page");
      Navigator.of(context).push(
        new MaterialPageRoute(
            builder: (BuildContext context) => new MovieRouting(defaultTab: 2)),
      );
    } else {
      printIfDebug("User isn't logged in -> Stay in Sign Up Page");
    }
  }

  void setUserAuthDetails(email, password) {
    printIfDebug("GG");
    setEmail = email;
    setPassword = password;
    setState(() {
      newUser = false;
    });
    printIfDebug(setEmail);
    printIfDebug(setPassword);
  }

  toggleOptions(bool value) {
    setState(() {
      newUser = value;
    });
  }

  Widget containerForSocialNetworksSignIn() {
    return new Container(
      child: new MovieAuthenticationSocialNetworks(
        newUser: newUser,
        setBlurFlag: setBlurFlag,
        fromTutorialPage: widget.fromTutorialPage!,
        deviceMake: deviceMake,
      ),
    );
  }

  Widget signUpOrLogin() {
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth * 0.96,
      height: 300,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          logInUI(),
          Positioned(
            bottom: 30,
            child: signInButton(),
          ),
        ],
      ),
    );
  }

  Widget errorMessageUI() {
    return new Container(
      padding: const EdgeInsets.only(left: 3.0, right: 3.0),
      margin: EdgeInsets.only(
        bottom: 5,
      ),
      child: new Text(
        errorMessage != null ? errorMessage : "",
        textAlign: TextAlign.justify,
        style: new TextStyle(
            fontWeight: FontWeight.bold, color: Constants.appColorError),
        textScaleFactor: 1.0,
        overflow: TextOverflow.clip,
        maxLines: 2,
      ),
    );
  }

  Widget titleText() {
    return Container(
      padding: EdgeInsets.all(
        15,
      ),
      child: Text(
        "LOGIN WITH OTP",
        style: TextStyle(
          color: Constants.appColorFont,
          fontSize: 18,
        ),
      ),
    );
  }

  Widget logInUI() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          titleText(),
          formUI(),
        ],
      ),
    );
  }

  Color getInputTextColor(_focusNode) {
    return _focusNode.hasFocus
        ? Constants.appColorLogo
        : Constants.appColorDivider;
  }

  String? validateEmail(String? value) {
    Pattern? pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value!))
      return 'Enter Valid Email';
    else
      return null;
  }

  Widget formUI() {
    final theme = Theme.of(context);
    // var showIcon;
    // if (!showPassword) {
    //   showIcon = Image.asset(
    //     'assests/show_password_icon.png',
    //     height: 5,
    //     width: 5,
    //     color: Constants.appColorLogo,
    //   );
    // } else {
    //   showIcon = Image.asset(
    //     'assests/hide_password_icon.png',
    //     height: 5,
    //     width: 5,
    //     color: Constants.appColorDivider,
    //   );
    // }
    return new Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: new Card(
        color: Constants.appColorL2,
        elevation: 10,
        child: Container(
          padding: const EdgeInsets.only(
            left: 40,
            right: 40,
          ),
          width: 300.0,
          height: 200,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              !receivedOTP
                  ? Container(
                      margin: EdgeInsets.only(
                        top: 40,
                        bottom: 20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          getIconContainer('assests/email_icon.png'),
                          Container(
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(
                              "Email Address",
                              style: TextStyle(
                                color: Constants.appColorFont,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(
                        top: 40,
                        bottom: 20,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 20,
                            width: 20,
                            child: Icon(
                              Icons.lock,
                              color: Constants.appColorFont,
                              size: 20,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(
                              "Enter OTP Received",
                              style: TextStyle(
                                color: Constants.appColorFont,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
              !receivedOTP
                  ? Theme(
                      data: theme.copyWith(
                        primaryColor: Constants.appColorFont.withOpacity(0.7),
                        hintColor: Constants.appColorFont,
                        cursorColor: Constants.appColorFont,
                        errorColor: Constants.appColorError,
                      ),
                      child: new TextFormField(
                        style: new TextStyle(
                          color: Constants.appColorFont.withOpacity(0.7),
                        ),
                        focusNode: _focusNodeEmail,
                        // decoration: new InputDecoration(
                        //   icon: getIconContainer('assests/email_icon.png'),
                        //   labelText: "Email Address",
                        // ),
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        validator: validateEmail,
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.only(
                        left: 20,
                      ),
                      child: Theme(
                        data: theme.copyWith(
                          primaryColor: Constants.appColorFont.withOpacity(0.7),
                          hintColor: Constants.appColorFont,
                          cursorColor: Constants.appColorFont,
                          errorColor: Constants.appColorError,
                          textSelectionColor: Constants.appColorFont,
                        ),
                        child: PinCodeTextField(
                          maxLength: 6,
                          controller: pinController,
                          onDone: (String text) {
                            setState(() {
                              pinText = text;
                            });
                          },
                          defaultBorderColor: Constants.appColorFont,
                          hasTextBorderColor: Constants.appColorFont,
                          pinTextStyle: TextStyle(
                            color: Constants.appColorFont,
                          ),
                          pinBoxWidth: 25,
                          highlightColor: Constants.appColorFont,
                          pinBoxDecoration: ProvidedPinBoxDecoration
                              .underlinedPinBoxDecoration,
                        ),
                      ),
                    ),
              new SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  toggleShowPassword() {
    setState(() {
      showPassword = showPassword ? false : true;
    });
  }

  Future validateInputs() async {
    if (_formKey.currentState!.validate()) {
    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  sendEmailReset() async {
    String url = Constants.apiUrl +
        "/flixjini_auth/tv_login.json?&email=" +
        emailController.text +
        "&";
    url = constructHeader(url);

    url = await fetchDefaultParams(url);
    http.post(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      printIfDebug("Response body: ${response.body}");
      String jsonString = response.body;
      var jsonResponse = json.decode(jsonString);
      printIfDebug("seen json: " + jsonResponse.toString());
      if (response.statusCode == 200) {
        showToast("OTP Sent", 'long', 'bottom', 1, Constants.appColorLogo);
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    });
  }

  sendOTP() async {
    String url = Constants.apiUrl +
        "/flixjini_auth/tv_login.json?&email=" +
        emailController.text +
        "&otp=" +
        pinText +
        "&";
    url = constructHeader(url);

    url = await fetchDefaultParams(url);
    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
          loading = false;
        });
        showToast(
            "There has been an error, you can go back and skip login and try reseting the password later",
            'long',
            'bottom',
            1,
            Constants.appColorLogo);
      }
    });
    http.post(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      printIfDebug("Response body: ${response.body}");
      String jsonString = response.body;
      var jsonResponse = json.decode(jsonString);
      printIfDebug("seen json: " + jsonResponse.toString());
      if (response.statusCode == 200) {
        if (!routeFlag && jsonResponse["logged_in"]) {
          showToast("  Logged In Successfully", 'long', 'bottom', 1,
              Constants.appColorLogo);

          authoriseUser(jsonResponse["jwt_token"]);
        } else {
          if (!routeFlag) {
            setState(() {
              loading = false;

              routeFlag = true;
            });

            showToast(jsonResponse["message"], 'long', 'bottom', 1,
                Constants.appColorError);
            printIfDebug("Non sucessesful response from server");
          }
        }
      }
    });
  }

  authoriseUser(token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sendEvent("reset");
    setState(() {
      prefs.setString('authenticationToken', token);
      prefs.setString("userEmail", emailController.text);

      routeFlag = true;
    });
    Navigator.of(context).pushReplacement(new MaterialPageRoute(
        builder: (BuildContext context) => new CountrySelectorPage(
              authToken: token,
            )));
  }

  Widget signInButton() {
    return new GestureDetector(
      onTap: () {
        printIfDebug(pinText);
        if (pinText.length == 6 && receivedOTP) {
          setState(() {
            loading = true;
            routeFlag = false;
          });
          sendOTP();
        } else if (!receivedOTP) {
          validateInputs();

          if (_formKey.currentState!.validate()) {
            setState(() {
              receivedOTP = true;
            });
            sendEmailReset();
          }
        }
      },
      child: new Container(
        decoration: BoxDecoration(
          color: Constants.appColorLogo,
          borderRadius: BorderRadius.circular(20),
        ),
        height: 40.0,
        width: 150,
        padding: const EdgeInsets.all(5.0),
        child: new Center(
          child: new Text(
            "SUBMIT",
            textAlign: TextAlign.center,
            style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: Constants.appColorL1,
            ),
            textScaleFactor: 1.2,
            overflow: TextOverflow.clip,
            maxLines: 2,
          ),
        ),
      ),
    );
  }

  Widget brandUI() {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      margin: true ? EdgeInsets.only(top: 40) : EdgeInsets.all(0),
      padding: EdgeInsets.all(
        true ? 30 : 20,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: screenWidth * 0.7, // 200,
            height: screenHeight * .15, // 100,

            child: new Image.asset(
              "assests/app_logo.png",
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }

  Widget blurScreen() {
    if (blurFlag) {
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      return new Positioned(
        child: new Container(
          width: screenWidth,
          height: screenHeight,
          color: Constants.appColorL1.withOpacity(0.6),
          child: new Center(
            child: new CircularProgressIndicator(
              backgroundColor: Constants.appColorLogo,
              valueColor:
                  new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
            ),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  void startTimer() {
    // CountdownTimer countDownTimer = new CountdownTimer(
    //   new Duration(seconds: timerNumber),
    //   new Duration(seconds: 1),
    // );
    Timer.periodic(
      Duration(seconds: 1),
      (Timer t) {
        // printIfDebug('hi!');
        if (timerNumber > 0) {
          setState(() {
            timerNumber = timerNumber - 1;
          });
        } else {
          t.cancel();
        }
      },
    );

    // var sub = countDownTimer.listen(null);
    // sub.onData((duration) {
    //   setState(() {
    //     timerNumber = _start - duration.elapsed.inSeconds;
    //   });
    // });

    // sub.onDone(() {
    //   printIfDebug("Done");
    //   sub.cancel();
    // });
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double height = MediaQuery.of(context).size.height;
    if (receivedOTP && timerNumber == 30) {
      startTimer();
    }
    return loading
        ? getProgressBar()
        : Scaffold(
            backgroundColor: Constants.appColorL1,
            body: new Container(
              padding: const EdgeInsets.all(0.0),
              child: new SingleChildScrollView(
                child: new ConstrainedBox(
                  constraints: new BoxConstraints(
                    minHeight: height,
                    maxHeight: height,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      brandUI(),
                      // flex/: 5,

                      signUpOrLogin(),
                      receivedOTP
                          ? Container(
                              padding: EdgeInsets.only(
                                top: 20,
                                left: 40,
                                right: 40,
                                bottom: 20,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "An email has been sent to " +
                                        emailController.text.toString() +
                                        " with subject 'Login OTP for Flixjini' Check Spam and Trash",
                                    style: TextStyle(
                                      color: Constants.appColorFont,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: 40,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "Didn't Recieve OTP? ",
                                          style: TextStyle(
                                            color: Constants.appColorFont,
                                          ),
                                        ),
                                        timerNumber == 0
                                            ? GestureDetector(
                                                onTap: () {
                                                  sendEmailReset();
                                                },
                                                child: Text(
                                                  "Resend",
                                                  style: TextStyle(
                                                    color:
                                                        Constants.appColorLogo,
                                                  ),
                                                ),
                                              )
                                            : Text(
                                                "Resend",
                                                style: TextStyle(
                                                  color:
                                                      Constants.appColorDivider,
                                                ),
                                              ),
                                        timerNumber == 0
                                            ? Container()
                                            : Text(
                                                "(" +
                                                    timerNumber.toString() +
                                                    ")",
                                                style: TextStyle(
                                                  color:
                                                      Constants.appColorDivider,
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                      // flex: 8,
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
