import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:io';
import 'dart:ui';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
// import 'package:package_info/package_info.dart';
// import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'movie_authentication_login_new.dart';

class MovieAuthenticationPageNew extends StatefulWidget {
  MovieAuthenticationPageNew({
    this.type,
    this.fromTutorialPage,
    Key? key,
  }) : super(key: key);

  final bool? type, fromTutorialPage;

  @override
  MovieAuthenticationPageNewState createState() =>
      new MovieAuthenticationPageNewState();
}

class MovieAuthenticationPageNewState
    extends State<MovieAuthenticationPageNew> {
  bool newUser = true;
  bool blurFlag = false, existingError = false;
  String authenticationToken = "";
  String setEmail = "";
  String setPassword = "", deviceMake = "";

  @override
  void initState() {
    super.initState();
    assignLoginOrSignUp();
    // checkConnectivity();
    // fetchMobileMake();
    logUserScreen('Flixjini Authentication Page', 'MovieAuthenticationPage');
  }

  fetchMobileMake() async {
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    // AndroidDeviceInfo androidInfo;
    // androidInfo = await deviceInfo.androidInfo;
    // setState(() {
    //   deviceMake = androidInfo.manufacturer;
    // });
    printIfDebug("maker/////////////" + deviceMake);

    // return deviceManu;
  }

  assignLoginOrSignUp() {
    if (widget.type == null) {
      setState(() {
        newUser = true;
      });
    } else {
      setState(() {
        newUser = widget.type!;
      });
    }
  }

  setBlurFlag(bool value) {
    setState(() {
      blurFlag = value;
    });
  }

  toggleOptions(bool value) {
    setState(() {
      newUser = value;
    });
  }

  Widget brandUI() {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      // margin: true ? EdgeInsets.only(top: 40) : EdgeInsets.all(0),
      padding: EdgeInsets.all(
        true ? 30 : 0,
      ),
      margin: EdgeInsets.only(
        bottom: 40,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: screenWidth * 0.7, // 200,
            height: screenHeight * .15, // 100,

            child: new Image.asset(
              "assests/app_logo.png",
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }

  Widget blurScreen() {
    if (blurFlag) {
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      return new Positioned(
        child: new Container(
          width: screenWidth,
          height: screenHeight,
          color: Constants.appColorL1.withOpacity(0.6),
          child: new Center(
            child: new CircularProgressIndicator(
              backgroundColor: Constants.appColorLogo,
              valueColor:
                  new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
            ),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double height = MediaQuery.of(context).size.height;

    return new Scaffold(
      backgroundColor: Constants.appColorL1,
      body: new Container(
        padding: const EdgeInsets.all(0.0),
        child: new SingleChildScrollView(
          child: new ConstrainedBox(
            constraints: new BoxConstraints(
              minHeight: height,
              maxHeight: height,
            ),
            child: new Stack(
              children: <Widget>[
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    brandUI(),
                    MovieAuthenticationLoginNew(
                      setEmail: setEmail,
                      setPassword: setPassword,
                      setBlurFlag: setBlurFlag,
                      existingError: existingError,
                    ),
                  ],
                ),
                blurScreen(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
