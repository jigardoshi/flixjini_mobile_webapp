import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'dart:convert';
import 'package:flixjini_webapp/common/constants.dart';

import 'dart:io';
import 'dart:ui';
// import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/authentication/movie_authentication_signup.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_login.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_social_networks.dart';
// import 'package:flixjini_webapp/authentication/reset_password.dart';

import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import  'package:google_sign_in/google_sign_in.dart';
// import  'package:flutter/services.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
// import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:device_info/device_info.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart' as FlixjiniUtils;
// import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flutter/cupertino.dart';
// import 'package:connectivity/connectivity.dart';

class MovieAuthenticationPage extends StatefulWidget {
  MovieAuthenticationPage({
    this.type,
    this.fromTutorialPage,
    Key? key,
  }) : super(key: key);

  final bool? type, fromTutorialPage;

  @override
  MovieAuthenticationPageState createState() =>
      new MovieAuthenticationPageState();
}

class MovieAuthenticationPageState extends State<MovieAuthenticationPage> {
  bool newUser = true;
  bool blurFlag = false, existingError = false;
  String authenticationToken = "";
  String setEmail = "";
  String setPassword = "", deviceMake = "";

  @override
  void initState() {
    super.initState();
    assignLoginOrSignUp();
    // checkConnectivity();
    // fetchMobileMake();
    logUserScreen('Flixjini Authentication Page', 'MovieAuthenticationPage');
  }

  fetchMobileMake() async {
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    // AndroidDeviceInfo androidInfo;
    // androidInfo = await deviceInfo.androidInfo;
    // setState(() {
    //   deviceMake = androidInfo.manufacturer;
    // });
    printIfDebug("maker/////////////" + deviceMake);

    // return deviceManu;
  }

  // void checkConnectivity() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.mobile) {
  //     printIfDebug('\n\nconnected to a mobile network.');
  //   } else if (connectivityResult == ConnectivityResult.wifi) {
  //     printIfDebug('\n\nconnected to a wifi network.');
  //   } else {
  //     FlixjiniUtils.showCupertinoDialog(context, 'No Internet',
  //         "Please make sure that you've proper Internet Connection");
  //   }
  // }

  assignLoginOrSignUp() {
    if (widget.type == null) {
      setState(() {
        newUser = true;
      });
    } else {
      setState(() {
        newUser = widget.type!;
      });
    }
  }

  setBlurFlag(bool value) {
    setState(() {
      blurFlag = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      printIfDebug("User is already Logged in -> Move to Index Page");
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieRouting(
                defaultTab: 2,
              )));
    } else {
      printIfDebug("User isn't logged in -> Stay in Sign Up Page");
    }
  }

  void setUserAuthDetails(email, password) {
    printIfDebug("GG");
    setEmail = email;
    setPassword = password;

    setState(() {
      newUser = false;
      existingError = true;
    });
    printIfDebug(setEmail);
    printIfDebug(setPassword);
  }

  toggleOptions(bool value) {
    setState(() {
      newUser = value;
    });
  }

  Widget containerForSocialNetworksSignIn() {
    return new Container(
      child: new MovieAuthenticationSocialNetworks(
        newUser: newUser,
        setBlurFlag: setBlurFlag,
        fromTutorialPage: widget.fromTutorialPage,
        deviceMake: deviceMake,
      ),
    );
  }

  Widget signUpOrLogin() {
    if (newUser == true) {
      return new MovieAuthenticationSignup(
        setUserAuthDetails: setUserAuthDetails,
        setBlurFlag: setBlurFlag,
        fromTutorialPage: widget.fromTutorialPage ?? false,
      );
    } else {
      return new MovieAuthenticationLogin(
        setEmail: setEmail,
        setPassword: setPassword,
        setBlurFlag: setBlurFlag,
        fromTutorialPage: widget.fromTutorialPage ?? false,
        existingError: existingError,
      );
      // return Container();

    }
  }

  Widget signUpAndLoginButtons() {
    return new Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Constants.appColorLogo,
      ),
      margin: EdgeInsets.only(
        top: true ? 5.0 : 20,
        left: 30,
        right: 30,
        bottom: true ? 5.0 : 20,
      ),
      padding: EdgeInsets.all(5),
      height: true ? 40 : 40,
      width: 250,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Expanded(
            child: new GestureDetector(
              onTap: () {
                printIfDebug("New user, so sign up");
                toggleOptions(true);
              },
              child: new Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: newUser
                      ? Constants.appColorFont.withOpacity(0.3)
                      : Constants.appColorLogo,
                ),
                child: new Center(
                  child: new Text(
                    "New",
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      color: newUser
                          ? Constants.appColorL1
                          : Constants.appColorFont,
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    textScaleFactor: 1.2,
                  ),
                ),
              ),
            ),
            flex: 1,
          ),
          new Expanded(
            child: new GestureDetector(
              onTap: () {
                printIfDebug("Existing user, so sign in");
                toggleOptions(false);
              },
              child: new Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    color: newUser
                        ? Constants.appColorLogo
                        : Constants.appColorFont.withOpacity(0.3)),
                child: new Center(
                  child: new Text(
                    "Existing",
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      color: newUser
                          ? Constants.appColorFont
                          : Constants.appColorL1,
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    textScaleFactor: 1.2,
                  ),
                ),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  Widget authenticationUI() {
    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          signUpAndLoginButtons(),
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              signUpOrLogin(),
            ],
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: containerForSocialNetworksSignIn(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget brandUI() {
    // double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      margin: true ? EdgeInsets.only(top: 40) : EdgeInsets.all(0),
      padding: EdgeInsets.all(
        true ? 30 : 20,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            width: 200,
            height: 100,
            child: new Image.asset(
              "assests/theFlixjiniLogo.png",
              fit: BoxFit.contain,
            ),
          ),
        ],
      ),
    );
  }

  Widget blurScreen() {
    if (blurFlag) {
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      return new Positioned(
        child: new Container(
          width: screenWidth,
          height: screenHeight,
          color: Constants.appColorL1.withOpacity(0.6),
          child: new Center(
            child: new CircularProgressIndicator(
              backgroundColor: Constants.appColorLogo,
              valueColor:
                  new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
            ),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Constants.appColorL1,
      body: Container(
        padding: const EdgeInsets.all(0.0),
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: height,
              maxHeight: height,
            ),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: brandUI(),
                      flex: 5,
                    ),
                    Expanded(
                      child: authenticationUI(),
                      flex: 16,
                    ),
                  ],
                ),
                blurScreen(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
