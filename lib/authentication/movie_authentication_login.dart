// import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
// import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'dart:convert';
// import 'package:url_launcher/url_launcher.dart';
// import  'dart:io' show Platform;
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/index/movie_index_page.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 'package:flutter_udid/flutter_udid.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:device_info/device_info.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flixjini_webapp/authentication/reset_password.dart';
import 'package:flixjini_webapp/common/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MovieAuthenticationLogin extends StatefulWidget {
  MovieAuthenticationLogin({
    Key? key,
    this.setEmail,
    this.setPassword,
    this.setBlurFlag,
    required this.fromTutorialPage,
    this.existingError,
  }) : super(key: key);

  final setEmail;
  final setPassword;
  final setBlurFlag;
  final bool fromTutorialPage;
  final existingError;

  @override
  _MovieAuthenticationLoginState createState() =>
      new _MovieAuthenticationLoginState();
}

class _MovieAuthenticationLoginState extends State<MovieAuthenticationLogin> {
  bool newUser = true;
  String authenticationToken = "";
  bool showPassword = true;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _autoValidate = false, routeFlag = false;
  String password = "";
  String repeatPassword = "";
  String email = "";
  String errorMessage = "";
  bool receivedErrorMessage = false;
  FocusNode _focusNodeEmail = new FocusNode();
  FocusNode _focusNodePassword = new FocusNode();
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();

    if (!widget.setEmail.isEmpty && !widget.setPassword.isEmpty) {
      printIfDebug(widget.setEmail);
      printIfDebug(widget.setPassword);
      emailController = new TextEditingController(text: widget.setEmail);
      passwordController = new TextEditingController(text: widget.setPassword);
      if (widget.existingError) {
        setState(() {
          receivedErrorMessage = true;
          errorMessage =
              "Account already exists with this Email Address, Please Login.";
        });
      }
    } else {
      printIfDebug("Nothing");
    }
  }

  sendEvent(String method) {
    Map<String, String> eventData = {
      'method': method,
    };
    //platform.invokeMethod('logCompleteRegistrationEvent', eventData);
  }

  authoriseUser(token, email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', token);
      prefs.setString("userEmail", email);
    });
    sendEvent("login-email");
    printIfDebug("authenticationToken is Set, Login Successful");

    if (widget.fromTutorialPage == true) {
      // new user coming from tutorial page
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new CountrySelectorPage(
                authToken: authenticationToken,
              )));
    } else {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieRouting(
                defaultTab: 2,
              )));
    }
  }

  sendRequest(actionUrl, email, password) async {
    bool flag = false;
    Map data = {
      'email': email,
      'password': password,
    };
    printIfDebug("The username and password: $data");
    var url = actionUrl;
    url = await fetchDefaultParams(url);
    printIfDebug('\n\nauth login dart file');
    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
          widget.setBlurFlag(false);
        });
        showToast(
            "There has been an error, if this problem persists, you can click the skip login button and login later",
            'long',
            'bottom',
            3,
            Constants.appColorLogo);
      }
    });
    try {
      http.post(Uri.parse(url), body: data).then((response) {
        printIfDebug("Response status: ${response.statusCode}");
        printIfDebug("Response body: ${response.body}");
        var data = json.decode(response.body);

        if (response.statusCode == 200 && !routeFlag) {
          var data = json.decode(response.body);

          printIfDebug("Value of response");
          printIfDebug(data);
          if (data.containsKey("jwt_token")) {
            flag = true;
            printIfDebug(data["jwt_token"]);
            setState(() {
              receivedErrorMessage = false;
              errorMessage = "";
              routeFlag = true;
            });
            authoriseUser(data["jwt_token"], email);
            showToast("  Logged In Successfully as " + email, 'long', 'bottom',
                1, Constants.appColorLogo);
          } else {
            setState(() {
              receivedErrorMessage = true;
              errorMessage = (response.statusCode).toString();
            });
            printIfDebug("Sign Up Unsuccessful");
            printIfDebug(data);
          }
        } else if ((response.statusCode == 401) ||
            (response.statusCode == 400)) {
          setState(() {
            receivedErrorMessage = true;
            errorMessage = data["message"] != null
                ? data["message"]
                : "Invalid email or password. Please use forgot password if you have forgotten password.";
          });
          printIfDebug("\n\nUnauthorised Access");
          printIfDebug(data["message"]);
        } else {
          setState(() {
            receivedErrorMessage = true;
            errorMessage =
                "Oops! Something went wrong. Please try again later"; // (response.statusCode).toString();
          });
          printIfDebug("Login Unsuccessful");
          printIfDebug(data);
        }
        if (!flag) {
          widget.setBlurFlag(false);
        }
      });
    } catch (exception) {
      // FlutterCrashlytics().log("Login crash report");
      // FlutterCrashlytics().logException(exception, exception);
      printIfDebug('Failed getting IP address');
      widget.setBlurFlag(false);
      setState(() {
        receivedErrorMessage = true;
        // errorMessage = (response.statusCode).toString();
      });
      showToast("  Login UnSuccessfull  ", 'long', 'bottom', 1,
          Constants.appColorError);
      printIfDebug("Sign Up Unsuccessful");
      printIfDebug(data.toString());
    }
  }

  toggleShowPassword() {
    setState(() {
      showPassword = showPassword ? false : true;
    });
  }

  String? validatePassword(String value) {
    if (value.length < 8)
      return 'Password should be atleast 8 chars';
    else
      return null;
  }

  String? validateEmail(String? value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value!))
      return 'Enter Valid Email';
    else
      return null;
  }

  Future validateInputs() async {
    if (_formKey.currentState!.validate()) {
      //    If all data are correct then save data to out variables
      _formKey.currentState!.save();
      printIfDebug("Fields Valid");
      printIfDebug('\n\nexisting user login email: ' + email);
      printIfDebug('\n\nexisting user login password' + password);
      printIfDebug("Send Server email, password, repeatPassword");
      String url =
          "https://api.flixjini.com/flixjini_auth/login?"; // "https://api.flixjini.com/api/v1/tokens.json";
      sendRequest(url, email, password);
    } else {
      //    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
      printIfDebug("Some Fields Invalid");
      widget.setBlurFlag(false);
    }
  }

  Widget errorMessageUI() {
    return new Container(
      padding: const EdgeInsets.only(left: 25.0, right: 25.0),
      margin: EdgeInsets.only(
        bottom: 5,
      ),
      child: new Text(
        errorMessage != null ? errorMessage : "",
        textAlign: TextAlign.center,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          color: Constants.appColorError,
        ),
        textScaleFactor: 1.0,
        overflow: TextOverflow.clip,
        maxLines: 2,
      ),
    );
  }

  Widget logInUI() {
    if (receivedErrorMessage) {
      return new Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            errorMessageUI(),
            formUI(),
            forgotPassword(),
          ],
        ),
      );
    } else {
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            formUI(),
            forgotPassword(),
          ],
        ),
      );
    }
  }

  Color getInputTextColor(_focusNode) {
    return _focusNode.hasFocus
        ? Constants.appColorLogo
        : Constants.appColorDivider;
  }

  Widget formUI() {
    final theme = Theme.of(context);
    var showIcon;
    if (!showPassword) {
      showIcon = Image.asset(
        'assests/show_password_icon.png',
        height: 5,
        width: 5,
        color: Constants.appColorLogo,
      );
    } else {
      showIcon = Image.asset(
        'assests/hide_password_icon.png',
        height: 5,
        width: 5,
        color: Constants.appColorDivider,
      );
    }
    return new Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: new Card(
        color: Constants.appColorL2,
        elevation: 10,
        child: Container(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20,
          ),
          width: 300.0,
          height: 200,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodeEmail),
                  hintColor: Constants.appColorFont,
                  cursorColor: Constants.appColorFont,
                  errorColor: Constants.appColorError,
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Constants.appColorFont),
                  focusNode: _focusNodeEmail,
                  decoration: new InputDecoration(
                    icon: getIconContainer('assests/email_icon.png'),
                    labelText: "Email Address",
                  ),
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  validator: validateEmail,
                  onSaved: (String? val) {
                    email = val!;
                  },
                ),
              ),
              new Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodePassword),
                  hintColor: Constants.appColorFont,
                  cursorColor: Constants.appColorFont,
                  errorColor: Constants.appColorError,
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Constants.appColorFont),
                  focusNode: _focusNodePassword,
                  obscureText: showPassword,
                  decoration: new InputDecoration(
                    labelText: 'Password',
                    icon: getIconContainer('assests/password_icon.png'),
                    suffixIcon: new GestureDetector(
                      child: new Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: showIcon,
                      ),
                      onTap: () {
                        toggleShowPassword();
                      },
                    ),
                  ),
                  // controller: passwordController,
                  keyboardType: TextInputType.text,
                  // validator: validatePassword,
                  onSaved: (String? val) {
                    password = val!;
                  },
                ),
              ),
              new SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget signInButton() {
    return new GestureDetector(
      onTap: () {
        widget.setBlurFlag(true);
        setState(() {
          routeFlag = false;
        });
        printIfDebug("Sign In User");
        validateInputs();
      },
      child: new Container(
        decoration: BoxDecoration(
          color: Constants.appColorLogo,
          borderRadius: BorderRadius.circular(20),
        ),
        height: 40.0,
        width: 150,
        padding: const EdgeInsets.all(5.0),
        child: new Center(
          child: new Text(
            "LOGIN",
            textAlign: TextAlign.center,
            style: new TextStyle(
                fontWeight: FontWeight.bold, color: Constants.appColorFont),
            textScaleFactor: 1.2,
            overflow: TextOverflow.clip,
            maxLines: 2,
          ),
        ),
      ),
    );
  }

  Widget forgotPassword() {
    return new GestureDetector(
      onTap: () {
        String resetPassword = "https://api.flixjini.com/users/password/new";
        printIfDebug("Forgot Password");
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new ResetPassword(
                  resetPassword: resetPassword,
                )));
        // _launchURL(resetPassword);
      },
      child: new Container(
        decoration: BoxDecoration(),
        margin: EdgeInsets.only(top: 26),
        // height: 30.0,
        // padding: const EdgeInsets.all(5.0),
        child: new Text(
          "Forgot Password?",
          textAlign: TextAlign.center,
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: Constants.appColorFont,
              fontSize: 20.0),
          overflow: TextOverflow.clip,
          maxLines: 1,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth * 0.96,
      height: receivedErrorMessage ? 320 : 260,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          logInUI(),
          Positioned(
            bottom: receivedErrorMessage ? 60 : 35,
            child: signInButton(),
          ),
        ],
      ),
    );
  }
}
