import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';

import 'dart:convert';
// import 	'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;
// import 'package:flutter_udid/flutter_udid.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 'package:device_info/device_info.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieAuthenticationSignup extends StatefulWidget {
  MovieAuthenticationSignup({
    Key? key,
    this.setUserAuthDetails,
    this.setBlurFlag,
    this.fromTutorialPage,
  }) : super(key: key);

  final setUserAuthDetails;
  final setBlurFlag;
  final bool? fromTutorialPage;

  @override
  _MovieAuthenticationSignupState createState() =>
      new _MovieAuthenticationSignupState();
}

class _MovieAuthenticationSignupState extends State<MovieAuthenticationSignup> {
  String authenticationToken = "";

  //  _formKey and _autoValidate
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final TextEditingController repeatController = new TextEditingController();
  bool _autoValidate = false;
  String password = "";
  String repeatPassword = "";
  String email = "";
  String errorMessage = "";
  bool showPassword = true;
  bool receivedErrorMessage = false, routeFlag = false;
  FocusNode _focusNodeEmail = new FocusNode();
  FocusNode _focusNodePassword = new FocusNode();
  FocusNode _focusNodeRetypePassword = new FocusNode();
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  toggleShowPassword() {
    setState(() {
      showPassword = showPassword ? false : true;
    });
  }

  authoriseUser(token, email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', token);
      prefs.setString('userEmail', email);
    });
    printIfDebug("authenticationToken is Set, Signup is Successful");
    sendEvent("email-signup");
    if (widget.fromTutorialPage == true) {
      // new user coming from tutorial page
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new CountrySelectorPage(
                authToken: authenticationToken,
              )));
    } else {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieRouting(
                defaultTab: 2,
              )));
    }
  }

  sendEvent(String method) {
    Map<String, String> eventData = {
      'method': method,
    };
    //platform.invokeMethod('logCompleteRegistrationEvent', eventData);
  }

  @override
  void initState() {
    super.initState();
  }

  sendRequest(actionUrl, user) async {
    bool flag = false;
    Map data = {
      'user[email]': user["email"],
      'user[password]': user["password"],
      'user[password_confirmation]': user["password_confirmation"],
    };

    // Uri url = Uri.parse(actionUrl);
    String url = await fetchDefaultParams(actionUrl);
    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
        });
        widget.setBlurFlag(false);

        showToast(
            "There has been an error, if this problem persists, you can click the skip login button and login later",
            'long',
            'bottom',
            3,
            Constants.appColorLogo);
      }
    });
    printIfDebug('\n\nauth sign up dart file' + data.toString());
    try {
      http.post(Uri.parse(url), body: data).then((response) {
        printIfDebug("Response status: ${response.statusCode}");
        printIfDebug("Response body: ${response.body}");
        var data = json.decode(response.body);
        printIfDebug(data["successful"]);
        if (response.statusCode == 200 && !routeFlag) {
          printIfDebug("Value assigned to parm sucess");
          printIfDebug(data["successful"]);
          if (data["successful"] == true && data["error"] == false) {
            flag = true;
            printIfDebug(data["jwt_token"]);
            setState(() {
              receivedErrorMessage = false;
              errorMessage = "";
            });
            authoriseUser(data["jwt_token"], user["email"]);
            showToast("  Signed Up Successfully as " + user["email"].toString(),
                'long', 'bottom', 1, Constants.appColorLogo);
          } else if ((data["successful"] == false) &&
              (data["error"] == true) &&
              ((data["message"].contains(
                  "Account already exists with this Email Address")))) {
            String email = emailController.text;
            String password = passwordController.text;
            printIfDebug(email);
            printIfDebug(password);
            widget.setUserAuthDetails(email, password);
            setState(() {
              receivedErrorMessage = true;
              errorMessage = data["message"];
            });
            // showToast(
            //   "Account already exists with this Email Address",
            //   'long',
            //   'bottom',
            //   1,
            // );
          } else {
            setState(() {
              receivedErrorMessage = true;
              errorMessage = data["message"];
            });
            printIfDebug("Sign Up Unsuccessful");
            showToast("Sign Up Unsuccessful", 'long', 'bottom', 1,
                Constants.appColorError);
            printIfDebug(data);
          }
        } else if (response.statusCode == 500 && !routeFlag) {
          setState(() {
            routeFlag = true;
          });
          widget.setBlurFlag(false);

          showToast(
              "There has been an error, if this problem persists, you can click the skip login button and login later",
              'long',
              'bottom',
              3,
              Constants.appColorLogo);
        }
        if (!flag) {
          widget.setBlurFlag(false);
        }
      });
    } catch (SocketException) {
      printIfDebug(SocketException.toString());
      // FlutterCrashlytics().log("Sign in crash report");
      // FlutterCrashlytics().logException(SocketException, SocketException);
      printIfDebug('Failed getting IP address');
      widget.setBlurFlag(false);
    }
  }

  String? validatePassword(String? value) {
    if (!(value!.contains(" ")) && (value.length < 8))
      return 'Password should be atleast 8 chars';
    else if ((value.contains(" ")) && (value.length > 7)) {
      printIfDebug(value.length.toString());
      return 'Avoid using white spaces';
    } else if ((value.contains(" ")) && (value.length < 8)) {
      printIfDebug(value.length.toString());
      return 'Avoid using white spaces and \nPassword should be atleast 8 chars';
    } else
      return null;
  }

  String? validateRepeatPassword(String? value) {
    printIfDebug("Password");
    printIfDebug(passwordController.text);
    printIfDebug("Repeat-Password");
    printIfDebug(value);
    if ((value!.length <= 8) && (value != passwordController.text))
      return 'Password should be atleast 8 chars \nPassword should match Confirmation';
    else if ((value.length <= 8) && (value == passwordController.text)) {
      return 'Password should be atleast 8 chars \nPassword should match Confirmation';
    } else if ((value.length > 7) && (value != passwordController.text)) {
      return 'Password should match Confirmation';
    } else
      return null;
  }

  String? validateEmail(String? value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value!))
      return 'Enter a Valid Email';
    else
      return null;
  }

  Future validateInputs() async {
    var user = {};
    if (_formKey.currentState!.validate()) {
      // If all data are correct then save data to out variables
      _formKey.currentState!.save();
      printIfDebug("Fields Valid");
      printIfDebug(email);
      printIfDebug(password);
      printIfDebug(repeatPassword);
      user["email"] = email;
      user["password"] = password;
      user["password_confirmation"] = repeatPassword;
      printIfDebug(user.toString());
      printIfDebug("Send Server email, password, repeatPassword");
      String url =
          "https://api.flixjini.com/flixjini_auth/register?"; // "https://api.flixjini.com/users.json";
      sendRequest(url, user);
    } else {
      // If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
      printIfDebug("Some Fields Invalid");
      widget.setBlurFlag(false);
    }
  }

  Color getInputTextColor(_focusNode) {
    return _focusNode.hasFocus
        ? Constants.appColorLogo
        : Constants.appColorDivider;
  }

  Widget formUI() {
    final theme = Theme.of(context);
    var showIcon;
    if (!showPassword) {
      showIcon = Image.asset(
        'assests/show_password_icon.png',
        height: 5,
        width: 5,
        color: Constants.appColorLogo,
      );
    } else {
      showIcon = Image.asset(
        'assests/hide_password_icon.png',
        height: 5,
        width: 5,
        color: Constants.appColorDivider,
      );
    }
    return new Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: new Card(
        color: Constants.appColorL2,
        elevation: 10,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Constants.appColorL2,
          ),
          padding: EdgeInsets.only(left: 20, right: 20),
          width: 300.0,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodeEmail),
                  hintColor: Constants.appColorFont,
                  cursorColor: Constants.appColorFont,
                  errorColor: Constants.appColorError,
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Constants.appColorFont),
                  focusNode: _focusNodeEmail,
                  decoration: new InputDecoration(
                    icon: getIconContainer('assests/email_icon.png'),
                    labelText: 'Email Address',
                  ),
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  validator: validateEmail,
                  onSaved: (String? val) {
                    email = val!;
                  },
                ),
              ),
              new Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodePassword),
                  hintColor: Constants.appColorFont,
                  cursorColor: Constants.appColorFont,
                  errorColor: Constants.appColorError,
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Constants.appColorFont),
                  focusNode: _focusNodePassword,
                  obscureText: showPassword,
                  decoration: new InputDecoration(
                    icon: getIconContainer('assests/password_icon.png'),
                    labelText: 'Password',
                    suffixIcon: new GestureDetector(
                      child: new Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: showIcon,
                      ),
                      onTap: () {
                        toggleShowPassword();
                      },
                    ),
                  ),
                  controller: passwordController,
                  keyboardType: TextInputType.text,
                  validator: validatePassword,
                  onSaved: (String? val) {
                    password = val!;
                  },
                ),
              ),
              new Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodeRetypePassword),
                  hintColor: Constants.appColorFont,
                  cursorColor: Constants.appColorFont,
                  errorColor: Constants.appColorError,
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Constants.appColorFont),
                  focusNode: _focusNodeRetypePassword,
                  obscureText: showPassword,
                  decoration: InputDecoration(
                      icon: getIconContainer('assests/password_icon.png'),
                      labelText: 'Repeat Password'),
                  controller: repeatController,
                  keyboardType: TextInputType.text,
                  validator: validateRepeatPassword,
                  onSaved: (String? val) {
                    repeatPassword = val!;
                  },
                ),
              ),
              new SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget signUpButton() {
    return new GestureDetector(
      onTap: () {
        widget.setBlurFlag(true);
        setState(() {
          routeFlag = false;
        });
        printIfDebug("\n\nSign Up New User - Create Account");
        validateInputs();
      },
      child: new Container(
        decoration: BoxDecoration(
          color: Constants.appColorLogo,
          borderRadius: BorderRadius.circular(30),
        ),
        height: 40.0,
        width: 150,
        padding: const EdgeInsets.all(5.0),
        child: new Center(
          child: new Text(
            "SIGN UP",
            textAlign: TextAlign.center,
            style: new TextStyle(
                fontWeight: FontWeight.bold, color: Constants.appColorFont),
            textScaleFactor: 1.2,
            overflow: TextOverflow.clip,
            maxLines: 2,
          ),
        ),
      ),
    );
  }

  Widget errorMessageUI() {
    return new Container(
      padding: const EdgeInsets.only(left: 3.0, right: 3.0),
      child: new Text(
        errorMessage,
        textAlign: TextAlign.justify,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          color: Constants.appColorFont,
        ),
        textScaleFactor: 1.0,
        overflow: TextOverflow.clip,
        maxLines: 2,
      ),
    );
  }

  Widget signUpUI() {
    if (receivedErrorMessage) {
      return new Container(
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            errorMessageUI(),
            Expanded(
              child: Container(
                color: Constants.appColorL2,
                margin: EdgeInsets.only(bottom: 30),
                child: formUI(),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(bottom: 30),
        child: formUI(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;

    return new Container(
      width: screenWidth * 0.96,
      height: 310,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          signUpUI(),
          Positioned(
            bottom: 10,
            child: signUpButton(),
          ),
        ],
      ),
    );
  }
}
