import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'dart:convert' show json;
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:io' show Platform;
// import 'package:device_info/device_info.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flixjini_webapp/utils/common_utils.dart' as Utils;
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter/cupertino.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';

GoogleSignIn _googleSignIn = new GoogleSignIn(
  scopes: <String>[
    'email',
    // 'https://www.googleapis.com/auth/contacts.readonly',
    // 'https://www.googleapis.com/auth/userinfo.profile',
  ],
);

class MovieAuthenticationSocialNetworks extends StatefulWidget {
  MovieAuthenticationSocialNetworks({
    Key? key,
    this.newUser,
    this.setBlurFlag,
    this.fromTutorialPage,
    this.deviceMake,
  }) : super(key: key);

  final newUser;
  final setBlurFlag;
  final bool? fromTutorialPage;
  final deviceMake;

  @override
  MovieAuthenticationSocialNetworksState createState() =>
      new MovieAuthenticationSocialNetworksState();
}

class MovieAuthenticationSocialNetworksState
    extends State<MovieAuthenticationSocialNetworks> {
  String authenticationToken = "", authToken = '';
  GoogleSignInAccount? _currentUser;
  String token = "";
  String authenticationType = "";
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  bool routeFlag = false;
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      printIfDebug('\n\ninit, current user is now changed to: $account');
      setState(() {
        _currentUser = account;
        _googleSignIn.signInSilently();
        printIfDebug(
            '\n\ninit, statement after invoking the method signin silently');
        printIfDebug("\n\ninit, Current User: $_currentUser");
        printIfDebug('\n\ninit, google sign in object: $_googleSignIn');
      });
    });
  }

  Future? _handleGetToken(email, photo) async {
    try {
      printIfDebug("Get Token Function is Called");
      // printIfDebug(await _currentUser.authHeaders.toString());
      _currentUser!.authHeaders.then((val) async {
        String finalToken = (val["Authorization"])!.replaceAll("Bearer", " ");
        finalToken = finalToken.trim();
        printIfDebug("\n\nfinal google auth token: $finalToken");
        setState(() {
          token = finalToken;
        });
        String type = "google";
        String url =
            "https://api.flixjini.com/flixjini_auth/app?"; // "https://api.flixjini.com/appproviderlogin.json";
        sendRequest(url, type);
        sendEvent("google");
        showToast("Signed in as " + email, 'long', 'bottom', 1,
            Constants.appColorLogo);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setString("userEmail", email);
          prefs.setString("userPhoto", photo);
        });
      });
    } catch (e) {
      printIfDebug('\n\nhandle get token method, exception caught is: $e');
      widget.setBlurFlag(false);
    }
  }

  sendEvent(String method) {
    Map<String, String> eventData = {
      'method': method,
    };
    //platform.invokeMethod('logCompleteRegistrationEvent', eventData);
  }

  Future<Null> _handleSignIn() async {
    try {
      printIfDebug('\n\ntom, **********************');
      printIfDebug("\n\ntom, Before Signing in");
      var accountSignedIn = await _googleSignIn.signIn();
      printIfDebug("\n\ntom, After Signing in");
      printIfDebug('\n\ntom, account signed in: $accountSignedIn');

      _currentUser =
          accountSignedIn; // if not assigned, current user will hold null value, which makes immediate sign-in attempt after logout impossible

      printIfDebug('\n\ntom, current user: $_currentUser');
      _handleGetToken(accountSignedIn!.email, accountSignedIn.photoUrl);
      // showToast("Signed in as " + accountSignedIn.displayName, 'long', 'bottom',
      //     1, Constants.appColorLogo);
      printIfDebug('\n\ntom, **********************');
    } catch (error) {
      widget.setBlurFlag(false);
      printIfDebug('\n\nthe error caught is: $error');
      // Utils.showCupertinoDialog(context, 'error', error.toString());
      Utils.showToast('Sign-in failed. Please try again later', 'long',
          'bottom', 1, Constants.appColorError);
    }
  }

  Future<Null> handleSignOut() async {
    printIfDebug("Handling Sign Out");
    printIfDebug(_googleSignIn.toString());
    _googleSignIn.disconnect();
    printIfDebug("Signed Out");
  }

  sendRequest(actionUrl, type) async {
    bool flag = false;
    Map data = {
      "token": token,
      "type": type,
    };

    printIfDebug("\n\nparams passed to the validation api: $data");
    printIfDebug(data.toString());

    var url = actionUrl;
    url = await fetchDefaultParams(url);
    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
          widget.setBlurFlag(false);
        });
        showToast(
            "There has been an error, if this problem persists, you can click the skip login button and login later",
            'long',
            'bottom',
            3,
            Constants.appColorLogo);
      }
    });
    try {
      http.post(url, body: data).then((response) {
        printIfDebug("Response status: ${response.statusCode}");
        printIfDebug("Response body: ${response.body}");
        var data = json.decode(response.body);

        if (response.statusCode == 200 && !routeFlag) {
          setState(() {
            routeFlag = true;
          });
          printIfDebug(
              "\n\nmovie auth google pg, validation api response data: ");
          printIfDebug(data);
          printIfDebug(
              '\n\nruntime type of the data["error"]: ${data["error"].runtimeType}');
          if (!data["jwt_token"].isEmpty) {
            flag = true;

            printIfDebug(data["jwt_token"]);
            authToken = data['jwt_token'];
            authoriseUser(data["jwt_token"], 'google');
          } else {
            // handle the case by showing some error msg
            printIfDebug("Authentication Unsuccessful");
            printIfDebug(data);
          }
        }
        if (!flag) {
          widget.setBlurFlag(false);
        }
      });
    } catch (exception) {
      printIfDebug('\n\nmovie auth google pg, Failed getting IP address');
      widget.setBlurFlag(false);
    }
  }

  Widget getHeadingRow(String textInBetweenDividers) {
    return Row(children: <Widget>[
      Expanded(
        child: new Container(
            margin: const EdgeInsets.only(left: 50.0, right: 10.0),
            child: Divider(
              color: Constants.appColorFont,
              height: 36,
            )),
      ),
      Text(
        "OR",
        style: TextStyle(
          color: Constants.appColorFont,
        ),
      ),
      Expanded(
        child: new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 50.0),
            child: Divider(
              color: Constants.appColorFont,
              height: 36,
            )),
      ),
    ]);
  }

  Widget containerForGoogleSignIn() {
    String googleText = "";
    if (widget.newUser == true) {
      googleText = "Sign Up with Google";
    } else {
      googleText = "Sign In with Google";
    }
    return new Container(
      height: 70,
      width: 70,
      child: new GestureDetector(
        onTap: () {
          widget.setBlurFlag(true);
          setState(() {
            routeFlag = false;
          });
          printIfDebug(
              '\n\njust printing this googleText variable for the sake of using it: $googleText');
          printIfDebug("Authenticate using Gmail");
          _handleSignIn();
        },
        child: Container(
          height: 100,
          width: 100,
          child: new Image.asset(
            "assests/colorful_google_logo.png",
          ),
        ),
      ),
    );
  }

  Widget skipButton() {
    return Container(
      padding: const EdgeInsets.only(top: 10.0),
      child: InkWell(
        onTap: () {
          if (widget.fromTutorialPage != null &&
              widget.fromTutorialPage! &&
              false) {
            Navigator.of(context).pushReplacement(new MaterialPageRoute(
                builder: (BuildContext context) => new CountrySelectorPage()));
          } else {
            Navigator.of(context).pushReplacement(new MaterialPageRoute(
                builder: (BuildContext context) => new MovieRouting(
                      defaultTab: 2,
                    )));
          }
        },
        child: Text(
          'Skip Login',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color: Constants.appColorFont,
          ),
        ),
      ),
    );
  }

  Widget containerForFacebookSignIn() {
    return GestureDetector(
      onTap: () async {
        try {
          // setState(() {
          //   routeFlag = false;
          // });
          // widget.setBlurFlag(true);

          // final facebookLogin = FacebookLogin();
          // // facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;

          // final result = await facebookLogin
          //     .logIn(['email']); //logInWithReadPermissions();
          // final token = result.accessToken.token;
          // final graphResponse = await http.get(Uri.parse(
          //     'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=' +
          //         token));
          // final profile = json.decode(graphResponse.body);
          // printIfDebug("fb profile" + profile.toString());
          // String displayText = "";
          // if (profile["email"] != null && profile["email"].length > 0) {
          //   displayText = profile["email"];
          // } else {
          //   displayText = profile["name"];
          // }

          // switch (result.status) {
          //   case FacebookLoginStatus.loggedIn:
          //     printIfDebug(
          //         '\n\nsuccess! result.accessToken.token: ${result.accessToken.token}' +
          //             result.toString());
          //     _sendTokenToServer(result.accessToken.token, 'fb', displayText);
          //     break;
          //   case FacebookLoginStatus.cancelledByUser:
          //     printIfDebug('\n\nlogin cancelled by user');
          //     setState(() {
          //       routeFlag = true;
          //       widget.setBlurFlag(false);
          //     });
          //     break;
          //   case FacebookLoginStatus.error:
          //     printIfDebug('\n\nerror');
          //     widget.setBlurFlag(false);

          //     showToast(
          //         "There has been an error, if this problem persists, you can click the skip login button and login later",
          //         'long',
          //         'bottom',
          //         3,
          //         Constants.appColorLogo);

          //     break;
          //   default:
          //     printIfDebug("default");
          //     break;
          // }
        } catch (e) {
          printIfDebug("try");
          widget.setBlurFlag(false);
          printIfDebug(e.toString());
        }
      },
      child: Container(
        height: 70,
        width: 70,
        child: Image.asset(
          'assests/fb_log_in.png',
          // color: Constants.appColorFont,
        ), // Text('FB Sign In'),
      ),
    );
  }

  _sendTokenToServer(String token, String type, String email) async {
    String url = 'https://api.flixjini.com/flixjini_auth/app.json';
    Map data = {
      "token": token,
      "type": type,
    };
    sendEvent("facebook");

    url = await fetchDefaultParams(url);
    printIfDebug(url);
    printIfDebug(data.toString());

    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
          widget.setBlurFlag(false);
        });
        showToast(
            "There has been an error, if this problem persists, you can click the skip login button and login later",
            'long',
            'bottom',
            3,
            Constants.appColorLogo);
      }
    });
    try {
      http.post(Uri.parse(url), body: data).then((response) {
        var data = json.decode(response.body);

        if (response.statusCode == 200) {
          printIfDebug(
              "\n\nfb movie auth pg, validation api response data: $data");
          if ((data["error"] == "false" || !data["jwt_token"].isEmpty) &&
              !routeFlag) {
            setState(() {
              routeFlag = true;
            });

            showToast(
                data["message"], 'long', 'bottom', 3, Constants.appColorLogo);
          }
          authoriseUser(data["jwt_token"], 'fb', email);
        } else {
          // handle the case by showing some error msg
          setState(() {
            routeFlag = true;
          });
          printIfDebug("Authentication Unsuccessful");
          printIfDebug(
              '\n\nfb authentication unsuccessful, with the response body data: $data');
          widget.setBlurFlag(false);

          Utils.showToast(
              'Facebook Authentication Unsuccessful. Please try again!',
              'long',
              'bottom',
              2,
              Constants.appColorError);
        }
      });
    } catch (e) {
      widget.setBlurFlag(false);
    }
  }

  authoriseUser(String token, String authType, [String? email]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      authenticationType = (prefs.getString('authenticationType') ?? "");

      prefs.setString('authenticationToken', token);
      prefs.setString('authenticationType', authType);
    });

    if (authType == 'fb') {
      setState(() {
        prefs.setString('userEmail', email!);
      });
    }
    String komparifyFireAPIEndpoint =
            'https://api.flixjini.com/appsflyer_fire?',
        retail9FireAPIEndpoint = 'https://www.retail9.com/appsflyer_fire.php?',
        idfa = '';
    idfa = true ? await Utils.getIDFA() : '';

    // making 2 network calls
    Utils.sendEventToOurFlyer(
      komparifyFireAPIEndpoint,
      'Registration',
      idfa,
    );
    Utils.sendEventToOurFlyer(
      retail9FireAPIEndpoint,
      'Registration',
      idfa,
    );
    widget.setBlurFlag(false);

    if (widget.fromTutorialPage == true) {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) =>
              new CountrySelectorPage(authToken: token)));
    } else {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieRouting(defaultTab: 2)));
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (_currentUser == null) {
      printIfDebug("Display Google");
      return new Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
            Expanded(
              // flex: 1,
              child: getHeadingRow('OR'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                containerForGoogleSignIn(),
                containerForFacebookSignIn(),
              ],
            ),
            // Expanded(
            //   // flex: 1,
            //   child: Container(), //getHeadingRow('OR'),
            // ),
            Spacer(),
            Container(
              margin: EdgeInsets.only(
                right: 20,
                bottom: true ? 10 : 20,
              ),
              child: skipButton(),
            ),
          ]));
    }
    return new Container();
  }
}
