import 'dart:async';

// import 'package:flixjini_webapp/authentication/reset_password_new.dart';
import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'dart:convert';
// import 'package:url_launcher/url_launcher.dart';
// import  'dart:io' show Platform;
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/index/movie_index_page.dart';
// import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 'package:pin_code_text_field/pin_code_text_field.dart';
// import 'package:flutter_udid/flutter_udid.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:device_info/device_info.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/splash/country_selector_page.dart';
import 'package:flixjini_webapp/authentication/reset_password.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'movie_authentication_page_new.dart';

class MovieAuthenticationLoginNew extends StatefulWidget {
  MovieAuthenticationLoginNew({
    this.setEmail,
    this.setPassword,
    this.setBlurFlag,
    this.fromTutorialPage,
    this.existingError,
    this.checkPlans,
  });

  final setEmail;
  final setPassword;
  final setBlurFlag;
  final bool? fromTutorialPage;
  final existingError;
  final checkPlans;

  @override
  _MovieAuthenticationLoginNewState createState() =>
      new _MovieAuthenticationLoginNewState();
}

enum RadioFlags { lafayette, jefferson }

class _MovieAuthenticationLoginNewState
    extends State<MovieAuthenticationLoginNew> {
  bool newUser = true;
  String authenticationToken = "", radioText = "", pinText = "";
  bool showPassword = true,
      radioButton = false,
      otpPage = false,
      loading = false,
      nameError = false,
      numberError = false,
      nameChecked = false,
      numberChecked = false;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool routeFlag = false;
  String? password;
  String? repeatPassword;
  String? email;
  String errorMessage = "";
  bool receivedErrorMessage = false;
  FocusNode _focusNodeEmail = new FocusNode();
  FocusNode _focusNodePassword = new FocusNode();
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  bool blurFlag = false;
  String setEmail = "";
  String setPassword = "", deviceMake = "";

  bool receivedOTP = false;

  int timerNumber = 30;
  TextEditingController pinController = TextEditingController();

  @override
  void initState() {
    super.initState();

    if (widget.setEmail != null &&
        widget.setPassword != null &&
        !widget.setEmail.isEmpty &&
        !widget.setPassword.isEmpty) {
      //print(widget.setEmail);
      //print(widget.setPassword);
      emailController = new TextEditingController(text: widget.setEmail);
      passwordController = new TextEditingController(text: widget.setPassword);
      if (widget.existingError) {
        setState(() {
          receivedErrorMessage = true;
          errorMessage =
              "Account already exists with this Email Address, Please Login.";
        });
      }
    } else {
      // print("Nothing");
    }
  }

  sendEvent(String method) {
    Map<String, String> eventData = {
      'method': method,
    };
    platform.invokeMethod('logCompleteRegistrationEvent', eventData);
  }

  authoriseUser(token, flag, email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // sendEvent("reset");
    setState(() {
      prefs.setString('authenticationToken', token);
      prefs.setString("userEmail", email);
      prefs.setString("phNo", passwordController.text);
      routeFlag = true;
    });

    if (true) {
      Navigator.of(context).pushReplacement(
        new MaterialPageRoute(
          builder: (BuildContext context) => new CountrySelectorPage(
            authToken: token,
          ),
        ),
      );
    } else {
      // Navigator.of(context).pushReplacement(MaterialPageRoute(
      //     builder: (BuildContext context) => ISPGeniePage(
      //           authToken: token,
      //         )));
    }
  }

  String? validatePassword(String value) {
    if (!nameChecked) {
      setState(() {
        nameChecked = true;
      });
    }
    if (value.length < 3) {
      setState(() {
        nameError = true;
      });
      return null;
    } else
      setState(() {
        nameError = false;
      });
    return null;
  }

  String? validateEmail(String value) {
    if (!numberChecked) {
      setState(() {
        numberChecked = true;
      });
    }
    Pattern pattern = r'^[0-9]{10}$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value)) {
      setState(() {
        numberError = true;
      });
      // return 'Enter Valid Phone Number';
      return null;
    } else {
      setState(() {
        numberError = false;
      });
      return null;
    }
  }

  Future validateInputs() async {
    if (_formKey.currentState!.validate()) {
      if (true || !numberError && !nameError && numberChecked && nameChecked) {
        //    If all data are correct then save data to out variables

        // setState(() {
        //   otpPage = true;
        // });
        sendEmailReset();
      } else {
        //    If all data are not valid then start auto validation.
        showToast("Enter Valid data!", 'long', 'bottom', 1, Color(0xFF43C681));
        // setState(() {
        //   // _autoValidate = true;
        // });
        //print("Some Fields Invalid");
        // widget.setBlurFlag(false);
      }
    }
  }

  Widget errorMessageUI() {
    return new Container(
      padding: const EdgeInsets.only(left: 25.0, right: 25.0),
      margin: EdgeInsets.only(
        bottom: 5,
      ),
      child: new Text(
        errorMessage != null ? errorMessage : "",
        textAlign: TextAlign.center,
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.red,
        ),
        textScaleFactor: 1.0,
        overflow: TextOverflow.clip,
        maxLines: 2,
      ),
    );
  }

  Widget logInUI() {
    return new Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          receivedErrorMessage ? errorMessageUI() : Container(),
          formUI(),
        ],
      ),
    );
  }

  Widget getIconContainerNew(String imgLoc) {
    return Container(
      // height: 20,
      // width: 20,
      child: Image.asset(
        imgLoc,
        color: Color(0xFF43C681),
      ),
    );
  }

  Color getInputTextColor(_focusNode) {
    return const Color(0xFF111111);
  }

  Widget formUI() {
    final theme = Theme.of(context);
    // var showIcon;
    // if (!showPassword) {
    //   showIcon = Image.asset(
    //     'assests/show_password_icon.png',
    //     height: 5,
    //     width: 5,
    //     color: const Color(0xFF43C681),
    //   );
    // } else {
    //   showIcon = Image.asset(
    //     'assests/hide_password_icon.png',
    //     height: 5,
    //     width: 5,
    //     color: const Color(0xFFd7d7d7),
    //   );
    // }
    return new Form(
      key: _formKey,
      // autovalidate: _autoValidate,
      // autovalidateMode:
      //     !_autoValidate ? AutovalidateMode.disabled : AutovalidateMode.always,
      child: Container(
        padding: const EdgeInsets.only(
          left: 25,
          right: 25,
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    bottom: 20,
                    left: 10,
                  ),
                  child: Text(
                    "Sign In",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 50,
              padding: EdgeInsets.only(
                left: 20,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodeEmail),
                  hintColor: Colors.black,
                  cursorColor: Colors.black,
                  errorColor: Color(0xFFFF220C),
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Colors.black),
                  focusNode: _focusNodeEmail,
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: Container(
                      height: 20,
                      width: 20,
                      child: Icon(
                        Icons.person_outlined,
                        color: Color(0xFF43c681),
                        size: 20,
                      ),
                      // getIconContainerNew('assests/email_icon.png'),
                    ),
                    labelText: "Name",
                  ),
                  controller: emailController,
                  keyboardType: TextInputType.name,
                  // validator: validatePassword,
                  onSaved: (String? val) {
                    email = val!;
                  },
                ),
              ),
            ),
            nameError
                ? Container(
                    margin: EdgeInsets.only(
                      top: 5,
                      left: 20,
                      // bottom: 10,
                    ),
                    child: Row(
                      children: [
                        Text(
                          "Name should be minimum 3 characters",
                          style: TextStyle(
                            color: Colors.red[300],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              height: 50,
              padding: EdgeInsets.only(
                left: 20,
              ),
              margin: EdgeInsets.only(
                top: 20,
              ),
              child: Theme(
                data: theme.copyWith(
                  primaryColor: getInputTextColor(_focusNodePassword),
                  hintColor: Colors.black,
                  cursorColor: Colors.black,
                  errorColor: Color(0xFFFF220C),
                ),
                child: new TextFormField(
                  style: new TextStyle(color: Colors.black),
                  focusNode: _focusNodePassword,
                  // obscureText: showPassword,
                  // expands: true,
                  // maxLines: null,
                  // minLines: null,
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    errorStyle: TextStyle(),
                    errorMaxLines: 1,
                    labelText: 'Phone Number',
                    suffixIcon: Container(
                      height: 20,
                      width: 20,
                      // child: getIconContainerNew('assests/password_icon.png'),
                      child: Icon(
                        Icons.phone_outlined,
                        size: 20,
                        color: Color(0xFF43c681),
                      ),
                    ),
                  ),

                  controller: passwordController,
                  keyboardType: TextInputType.phone,
                  // validator: validateEmail,
                  onSaved: (String? val) {
                    password = val!;
                  },
                ),
              ),
            ),
            numberError
                ? Container(
                    margin: EdgeInsets.only(
                      top: 5,
                      // bottom: 10,
                      left: 20,
                    ),
                    child: Row(
                      children: [
                        Text(
                          "Enter a valid Phone Number",
                          style: TextStyle(
                            color: Colors.red[300],
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
            GestureDetector(
              onTap: () {
                setState(() {
                  radioButton = !radioButton;
                  radioText = radioText;
                });
              },
              child: Container(
                margin: EdgeInsets.only(
                  // left: 10,
                  top: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    // Theme(
                    //   data: ThemeData(
                    //     unselectedWidgetColor: Colors.white,
                    //   ), //set the dark theme or write your own theme
                    //   child: Radio(
                    //     value: "radio",
                    //     groupValue: radioText,
                    //     toggleable: true,
                    //     activeColor: Colors.green,
                    //     focusColor: Colors.white,
                    //     hoverColor: Colors.white,
                    //     onChanged: (value) {
                    //       setState(() {
                    //         radioButton = !radioButton;
                    //         radioText = value;
                    //       });
                    //     },
                    //   ),
                    // ),
                    Container(
                      margin: EdgeInsets.only(right: 10, left: 10),
                      child: Icon(
                        radioButton
                            ? Icons.radio_button_on
                            : Icons.radio_button_off,
                        size: 20,
                        color: radioButton ? Color(0xFF43C681) : Colors.white,
                      ),
                    ),
                    // Checkbox(
                    //   value: true,
                    //   onChanged: (bool value) {
                    //     setState(() {
                    //       radioButton = !radioButton;
                    //     });
                    //   },
                    // ),
                    Expanded(
                      child: Text(
                        "I agree to the terms of service & privacy policy",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            true
                ? GestureDetector(
                    onTap: () {
                      try {
                        _launchURL(
                            context, "https://playboxtv.in/playbox/index.html");
                      } catch (e) {}
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 10,
                      ),
                      child: Text(
                        "Activate Free Plan",
                        style: TextStyle(
                          color: Color(0xFF43C681),
                          fontSize: 18,
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  void _launchURL(BuildContext context, link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Color(0xFF1E1E1E),
          // enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      debugPrint(e.toString());
    }
  }

  Widget signInButton() {
    return new GestureDetector(
      onTap: () {
        if (radioButton && !otpPage) {
          // widget.setBlurFlag(true);
          setState(() {
            routeFlag = false;
          });
          //print("Sign In User");
          validateInputs();
        } else if (radioButton && otpPage && pinController.text.length == 6) {
          setState(() {
            // otpPage = false;
            routeFlag = false;
          });
          widget.setBlurFlag(true);
          sendOTP();
        } else if (!radioButton) {
          showToast("Agree to Terms & Condition", 'long', 'bottom', 1,
              Color(0xFF43C681));

          //print("false");
        }
      },
      child: new Container(
        decoration: BoxDecoration(
          color: Color(0xFF43C681),
          borderRadius: BorderRadius.circular(20),
        ),
        // height: 40.0,
        // width: 150,
        margin: EdgeInsets.only(
          top: 70,
          right: 20,
        ),
        padding: EdgeInsets.only(
          top: 10.0,
          bottom: 10,
          left: 40,
          right: 40,
        ),
        child: new Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Text(
                !otpPage ? "Get OTP" : "SUBMIT",
                textAlign: TextAlign.center,
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color(0xFFFFFFFF)),
                textScaleFactor: 1.2,
                overflow: TextOverflow.clip,
                maxLines: 2,
              ),
              Container(
                margin: EdgeInsets.only(
                  left: 10,
                ),
                child: Image.asset(
                  "assests/right_arrow.png",
                  height: 12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  sendEmailReset() async {
    String url =
        Constants.apiUrl +"/flixjini_auth/onebox_login_send_otp.json?phone_number=" +
            passwordController.text +
            "&";
    url = constructHeader(url);

    url = await fetchDefaultParams(url);

    http.get(Uri.parse(url)).then((response) {
      //print("Response status: ${response.statusCode}");
      //print("response body: ${response.body}");
      String jsonString = response.body;
      var jsonResponse = json.decode(jsonString);
      // print("seen json: " + jsonResponse.toString());
      if (response.statusCode == 200 && !jsonResponse["error"]) {
        startTimer();
        setState(() {
          receivedOTP = true;
          otpPage = true;
        });
        showToast("OTP Sent", 'long', 'bottom', 1, Color(0xFF43C681));
      } else if (response.statusCode == 200 && jsonResponse["error"]) {
        showToast(
            jsonResponse["message"], 'long', 'bottom', 1, Color(0xFF43C681));
        //print("Non sucessesful response from server");
      } else {}
    });
  }

  sendOTP() async {
    String urlLogin =
        Constants.apiUrl +"/flixjini_auth/onebox_actually_login.json?phone_number=" +
            passwordController.text +
            "&otp=" +
            pinController.text +
            "&";
    String url = constructHeader(urlLogin);

    url = await fetchDefaultParams(url);

    Future.delayed(const Duration(milliseconds: 10000), () {
      if (!routeFlag) {
        setState(() {
          routeFlag = true;
          loading = false;
        });
        showToast(
            "There has been an error, you can go back and skip login and try reseting the password later",
            'long',
            'bottom',
            1,
            Color(0xFF43C681));
      }
    });
    http.get(Uri.parse(url)).then((response) async {
      //print("Response status: ${response.statusCode}");
      //print("response body: ${response.body}");
      String jsonString = response.body;
      var jsonResponse = json.decode(jsonString);
      // print("seen json: " + jsonResponse.toString());
      widget.setBlurFlag(false);
      if (response.statusCode == 200) {
        if (!routeFlag &&
            jsonResponse["successful"] != null &&
            jsonResponse["successful"]) {
          if (!jsonResponse["is_exceeded_mobile"]) {
            showToast("Logged In Successfully", 'long', 'bottom', 1,
                Color(0xFF43C681));
            setState(() {
              routeFlag = true;
            });
            authoriseUser(jsonResponse["jwt_token"],
                jsonResponse["plans_present"], emailController.text);
          } else {
            setState(() {
              routeFlag = true;
            });
            // Navigator.of(context).pushReplacement(
            //   new MaterialPageRoute(
            //     builder: (BuildContext context) => MultipleLoginWarningPage(
            //       jsonResponse: jsonResponse,
            //     ),
            //   ),
            // );
          }
        } else {
          // Navigator.of(context).pushReplacement(
          //   new MaterialPageRoute(
          //     builder: (BuildContext context) => new MovieAuthenticationPageNew(
          //       fromTutorialPage: false,
          //     ),
          //   ),
          // );
          setState(() {
            otpPage = true;
            receivedOTP = true;
            routeFlag = true;
            loading = false;
            pinText = "";
          });
          startTimer();
          showToast(
              jsonResponse["message"], 'long', 'bottom', 1, Color(0xFF43C681));
        }
      } else {
        if (!routeFlag) {
          setState(() {
            // loading = false;
            // routeFlag = true;
            // receivedOTP = false;
          });

          Navigator.of(context).pushReplacement(
            new MaterialPageRoute(
              builder: (BuildContext context) => new MovieAuthenticationPageNew(
                fromTutorialPage: false,
              ),
            ),
          );
          showToast(jsonResponse["message"], 'long', 'bottom', 1,
              Constants.appColorLogo);
          //print("Non sucessesful response from server");
        }
      }
    });
  }

  Widget forgotPassword() {
    return new GestureDetector(
      onTap: () {
        String resetPassword = Constants.apiUrl +"/users/password/new";
        //print("Forgot Password");
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new ResetPassword(
                  resetPassword: resetPassword,
                )));
        // _launchURL(resetPassword);
      },
      child: new Container(
        decoration: BoxDecoration(),
        margin: EdgeInsets.only(top: 26),
        // height: 30.0,
        // padding: const EdgeInsets.all(5.0),
        child: new Text(
          "Forgot Password?",
          textAlign: TextAlign.center,
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: const Color(0xFFFFFFFF),
              fontSize: 20.0),
          overflow: TextOverflow.clip,
          maxLines: 1,
        ),
      ),
    );
  }

  Widget otpPageUi() {
    // final theme = Theme.of(context);
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth - 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            child: Text(
              "OTP Verification",
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              bottom: 20,
            ),
            child: Text(
              "Enter OTP Code sent to +91" + passwordController.text,
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ),
          PinCodeTextField(
            appContext: context,
            pastedTextStyle: TextStyle(
              color: Colors.green.shade600,
              fontWeight: FontWeight.bold,
            ),
            length: 6,
            obscureText: false,
            obscuringCharacter: '*',
            animationType: AnimationType.fade,
            validator: (v) {
              // if (v.length < 6) {
              //   return "I'm from validator";
              // } else {
              return null;
              // }
            },
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.box,
              borderRadius: BorderRadius.circular(5),
              fieldHeight: 50,
              fieldWidth: 30,
              activeFillColor: Colors.white,
              inactiveFillColor: Colors.white,
              activeColor: Colors.white,
              inactiveColor: Colors.white,
              selectedFillColor: Colors.white,
            ),
            cursorColor: Colors.black,
            animationDuration: Duration(milliseconds: 300),
            textStyle: TextStyle(fontSize: 20, height: 1.6),
            backgroundColor: Color(0xFF1E1E1E),
            enableActiveFill: true,

            // errorAnimationController: errorController,
            controller: pinController,
            keyboardType: TextInputType.number,
            boxShadows: [
              BoxShadow(
                offset: Offset(0, 1),
                color: Colors.black12,
                blurRadius: 10,
              )
            ],
            onCompleted: (v) {
              setState(() {
                pinText = v;
              });
              //print("Completed");
            },
            // onTap: () {
            // //print("Pressed");
            // },
            onChanged: (value) {
              //print(value);
              setState(() {
                pinText = value;
              });
            },
            beforeTextPaste: (text) {
              // print("Allowing to paste $text");
              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
              //but you can show anything you want here, like your pop up saying wrong paste format or etc
              return true;
            },
          ),
          resendOtp(),
        ],
      ),
    );
  }

  void startTimer() {
    // CountdownTimer countDownTimer = new CountdownTimer(
    //   new Duration(seconds: timerNumber),
    //   new Duration(seconds: 1),
    setState(() {
      timerNumber = 30;
    });
    // );
    Timer.periodic(
      Duration(seconds: 1),
      (Timer t) {
        // print('hi!');
        if (timerNumber > 0) {
          setState(() {
            timerNumber = timerNumber - 1;
          });
        } else {
          t.cancel();
        }
      },
    );
  }

  Widget resendOtp() {
    return receivedOTP
        ? Container(
            padding: EdgeInsets.only(
              top: 20,
              // left: 40,
              // right: 40,
              // bottom: 20,
            ),
            child: Column(
              children: <Widget>[
                Text(
                  "OTP has been sent to " +
                      passwordController.text.toString() +
                      "",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Didn't Recieve OTP? ",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      timerNumber == 0
                          ? GestureDetector(
                              onTap: () {
                                sendEmailReset();
                              },
                              child: Text(
                                "Resend",
                                style: TextStyle(
                                  color: Color(0xFF43C681),
                                ),
                              ),
                            )
                          : Text(
                              "Resend",
                              style: TextStyle(
                                color: Color(0xFF777777),
                              ),
                            ),
                      timerNumber == 0
                          ? Container()
                          : Text(
                              "(" + timerNumber.toString() + ")",
                              style: TextStyle(
                                color: Color(0xFF777777),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : Container();
  }

  @override
  Widget build(BuildContext context) {
    // print('\n\nhey there! i am inside build method of ${this.runtimeType}');

    return loading
        ? getProgressBar()
        : Container(
            margin: EdgeInsets.only(
                // left: 10,
                // right: 10,
                ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                !otpPage ? logInUI() : otpPageUi(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    signInButton(),
                  ],
                ),
              ],
            ),
          );
  }
}
