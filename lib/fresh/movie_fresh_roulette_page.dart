// import 'package:flixjini_webapp/fresh/story_design.dart';
// import 'package:flixjini_webapp/navigation/movie_navigator_with_notch.dart';
import 'package:flixjini_webapp/fresh/roulette_signin_alert.dart';
import 'package:flixjini_webapp/search/movie_roulette_new.dart';
// import 'package:flixjini_webapp/search/movie_search_roulette.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
// import  'package:streaming_entertainment/search/movie_search_bar.dart';
import 'package:flixjini_webapp/search/movie_search_filter.dart';
// import 'package:flixjini_webapp/search/movie_search_results.dart';
// import  'package:streaming_entertainment/search/movie_search_popular.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import	'package:streaming_entertainment/navigation/movie_navigator.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_roulette.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'dart:ui';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFreshRoulettePage extends StatefulWidget {
  MovieFreshRoulettePage({
    this.moveToPreviousTab,
    this.freshness,
    this.showOriginalPosters,
    this.checkStack,
    this.urlAppendString,
    Key? key,
  }) : super(key: key);

  final moveToPreviousTab;
  final freshness;
  final showOriginalPosters;
  final checkStack;
  final urlAppendString;

  @override
  _MovieFreshRoulettePageState createState() =>
      new _MovieFreshRoulettePageState();
}

class _MovieFreshRoulettePageState extends State<MovieFreshRoulettePage> {
  ScrollController searchResultsController = new ScrollController();
  final dataKey = new GlobalKey();
  final TextEditingController searchController = new TextEditingController();
  bool fetchedMovieDetail = false;
  bool fetchedUserPreference = false;
  bool fetchedUserQueuePreference = false;
  bool endOfSearchResults = false;
  bool filterMenuStatus = false;
  bool searchFilterDataReady = false, filterAnimationFlag = true;
  List userSavedFilters = [];
  String? urlAppendStringLocal = "";

  bool unkownResults = false, filterShowFlag = false, freeFlag = false;
  final PageController ctrl = PageController(viewportFraction: 0.8);
  FirebaseAnalytics analytics = FirebaseAnalytics();

  int pageNo = 1;
  var sortData = {};
  var movieDetail = {};
  var userDetail = {};
  var userQueue = {};
  var searchFilterData, searchFilterUserData;
  var options = [];
  bool firstSearch = true;
  bool loadingSymbolStatusElasticSearch = false;
  bool loadingSymbolFetchMoreResults = false;
  bool resultsHaveBeenCleared = false;
  bool searchBoxEmpty = true;
  int chosenIndex = 0;
  String searchString = "";
  String checkString = "";
  String authenticationToken = "";
  String filtersSet = "";
  String stringSecondaryMenu = "";
  String howFresh = "7 Days Ago";
  Set<String> skippedIds = new Set();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    if (widget.urlAppendString != null && widget.urlAppendString.isNotEmpty) {
      fetchedMovieDetail = false;
      urlAppendStringLocal = widget.urlAppendString ?? "";
    } else {
      getRouletteSp();
    }
    printIfDebug("Welcome to Search Page");
    sendEvent();
    authenticationStatus();
    // logUserScreen('Flixjini Fresh Page', 'MovieFreshRoulettePage');
    // basedOnFresshness();
  }

  sendEvent() {
    _sendAnalyticsEventDetail("genie_click");
    //platform.invokeMethod('logAddPaymentInfoEvent');
  }

  Future<void> _sendAnalyticsEventDetail(tagName) async {
    await analytics.logEvent(
      name: tagName,
      parameters: <String, dynamic>{},
    );
  }

  void loadSavedFilters() async {
    printIfDebug('\n\nsaved filters button is tapped');
    String getFiltersAPIEndpoint =
        "https://api.flixjini.com/queue/get_filters.json?";

    String defaultParamsAddedURL =
        await fetchDefaultParams(getFiltersAPIEndpoint);
    printIfDebug('\n\ndefault params added url: $defaultParamsAddedURL');

    defaultParamsAddedURL += "&";

    String headerAddedURL = constructHeader(defaultParamsAddedURL);
    printIfDebug('\n\nheader added url is: $headerAddedURL');

    headerAddedURL += "&magic=true&country_code=IN&jwt_token=";

    // String authToken = await getStringFromSP('authenticationToken');
    headerAddedURL += authenticationToken;

    printIfDebug('\n\nurl before making request is: $headerAddedURL');

    callGetFiltersAPIEndpoint(headerAddedURL);
  }

  void callGetFiltersAPIEndpoint(String url) {
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        if (resBody["error"] == false) {
          userSavedFilters = resBody["filters"];
          // takeUsersSavedFilters(userSavedFilters);
          setState(() {
            //
          });
        } else {
          // error occurred

        }
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url' +
              exception.toString());
    }
  }

  basedOnFresshness() {
    String value = "7 Days Ago";
    if (widget.freshness != null) {
      if (widget.freshness.contains("fourteen-days-ago-movies")) {
        value = "14 Days Ago";
      } else if (widget.freshness.contains("one-month-ago-movies")) {
        value = "1 Month Ago";
      } else {
        printIfDebug("7 Days or something else -> resort to default");
      }
    }
    setToTitle(value);
  }

  prepareForFreshSecondaryMenu(secondaryMenu) {
    for (int index = 0; index < secondaryMenu["topics"].length; index++) {
      if (!secondaryMenu["topics"][index]["belongsTo"].contains("fresh")) {
        secondaryMenu["topics"].removeAt(index);
      }
    }
    return secondaryMenu;
  }

  loadStaticData() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        stringSecondaryMenu = prefs.getString('stringFilterRouletteMenu') ?? "";
      });
      printIfDebug("stringSecondaryMenu" + stringSecondaryMenu.toString());
      if (stringSecondaryMenu != null && stringSecondaryMenu.isNotEmpty) {
        movieAPICall();
        getFilterMenu(false);
      } else {
        movieAPICall();

        getFilterMenu(false);
      }
    } catch (e) {
      printIfDebug("loadStaticData exception" + e.toString());
    }
    // printIfDebug("Search Filter Menu");
    // printIfDebug(searchFilterData["topics"]);
  }

  setFilterStatus() {
    printIfDebug("set filter status");
    List filterList = searchFilterUserData.keys.toList();
    // for (int i = 0; i < searchFilterUserData["filters"].length; i++) {
    //  searchFilterData[]
    // }
    printIfDebug(filterList.toString());
    for (int i = 0; i < searchFilterData["topics"].length; i++) {
      if (filterList.contains(searchFilterData["topics"][i]["key"])) {
        printIfDebug(searchFilterData["topics"][i]["key"]);

        for (int j = 0;
            j < searchFilterData["topics"][i]["options"].length;
            j++) {
          if (searchFilterUserData[searchFilterData["topics"][i]["key"]]
              .contains(
                  searchFilterData["topics"][i]["options"][j]["filter_id"])) {
            printIfDebug(
                searchFilterData["topics"][i]["options"][j]["filter_id"]);

            searchFilterData["topics"][i]["options"][j]["status"] = true;
          } else {
            searchFilterData["topics"][i]["options"][j]["status"] = false;
          }
        }
      }
    }
  }

  setSavedFilterStatus(Map searchSavedFilterData) {
    List filterList = searchSavedFilterData.keys.toList();

    for (int i = 0; i < searchFilterData["topics"].length; i++) {
      if (filterList.contains(searchFilterData["topics"][i]["key"])) {
        for (int j = 0;
            j < searchFilterData["topics"][i]["options"].length;
            j++) {
          List checkList =
              searchSavedFilterData[searchFilterData["topics"][i]["key"]]
                  .toString()
                  .replaceAll("[", "")
                  .replaceAll("]", "")
                  .split(",");

          if (checkList.contains(
              searchFilterData["topics"][i]["options"][j]["filter_id"])) {
            searchFilterData["topics"][i]["options"][j]["status"] = true;
          } else {
            searchFilterData["topics"][i]["options"][j]["status"] = false;
          }
        }
      } else if (filterList.contains(searchFilterData["topics"][i]["key_1"])) {
        setState(() {
          searchFilterData["topics"][i]["features"]["rangeLower"] = int.parse(
              searchSavedFilterData[searchFilterData["topics"][i]["key_1"]]
                  .toString()
                  .replaceAll("[", "")
                  .replaceAll("]", ""));
        });
      } else if (filterList.contains(searchFilterData["topics"][i]["key_2"])) {
        setState(() {
          searchFilterData["topics"][i]["features"]["rangeUpper"] = int.parse(
              searchSavedFilterData[searchFilterData["topics"][i]["key_2"]]
                  .toString()
                  .replaceAll("[", "")
                  .replaceAll("]", ""));
        });
      } else {
        if (searchFilterData["topics"][i]["group"] != "slider") {
          resetSelectedFilterStatus(i);
        } else {
          resetRangeOptions(i);
        }
      }
    }
  }

  setSelectedFilterStatus(int i) {
    printIfDebug("set selected filter status");
    List filterList = searchFilterUserData.keys.toList();
    // for (int i = 0; i < searchFilterUserData["filters"].length; i++) {
    //  searchFilterData[]
    // }
    printIfDebug(filterList.toString());
    if (filterList.contains(searchFilterData["topics"][i]["key"])) {
      printIfDebug(searchFilterData["topics"][i]["key"]);

      for (int j = 0;
          j < searchFilterData["topics"][i]["options"].length;
          j++) {
        if (searchFilterUserData[searchFilterData["topics"][i]["key"]].contains(
            searchFilterData["topics"][i]["options"][j]["filter_id"])) {
          printIfDebug(
              searchFilterData["topics"][i]["options"][j]["filter_id"]);

          this.searchFilterData["topics"][i]["options"][j]["status"] = true;
        } else {
          this.searchFilterData["topics"][i]["options"][j]["status"] = false;
        }
      }
    }
    setState(() {
      searchFilterData = this.searchFilterData;
    });
  }

  resetSelectedFilterStatus(int i) {
    printIfDebug("reset filter status");
    List filterList = searchFilterUserData.keys.toList();
    // for (int i = 0; i < searchFilterUserData["filters"].length; i++) {
    //  searchFilterData[]
    // }
    printIfDebug(filterList.toString());
    // if (filterList.contains(searchFilterData["topics"][i]["key"])) {
    printIfDebug(searchFilterData["topics"][i]["key"]);

    for (int j = 0; j < searchFilterData["topics"][i]["options"].length; j++) {
      this.searchFilterData["topics"][i]["options"][j]["status"] = false;
      // }
    }
    setState(() {
      searchFilterData = this.searchFilterData;
    });
  }

  getFilterMenu(bool flag) async {
    String filterMenuOptionsUrl =
        "https://api.flixjini.com/roulette/get_profile.json?jwt_token=" +
            authenticationToken +
            (urlAppendStringLocal ?? "") +
            "&";
    printIfDebug("Load Menu from");
    // printIfDebug(filterMenuOptionsUrl);
    // printIfDebug(filterMenuOptionsUrl.length);
    String rootUrl = filterMenuOptionsUrl;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));

      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        // printIfDebug(responseBody);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('stringFilterRouletteMenu', responseBody);
        // printIfDebug("after saving" + responseBody.toString());
        var data = json.decode(responseBody);
        setState(() {
          searchFilterData = data;
          searchFilterDataReady = true;
          searchFilterUserData = data["filters"];
        });

        setFilterStatus();

        if (flag) {
          setState(() {
            filterShowFlag = flag;
          });
          movieAPICall();
        }
      } else {
        this.searchFilterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      printIfDebug("get filter menu" + exception.toString());
      this.searchFilterData["error"] = 'Failed getting IP address';
    }

    if (!mounted) return;
  }

  void toogleSearchBoxEmpty(value) {
    setState(() {
      searchBoxEmpty = value;
    });
  }

  getPreferences() {
    getUserPreference();
    getUserQueuePreference();
  }

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  setDefaultMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] = false;
    });
    printIfDebug("Update Status");
    printIfDebug(searchFilterData["topics"][i]);
  }

  toogleMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] =
          searchFilterData["topics"][i]["status"] ? false : true;
      filterMenuStatus = searchFilterData["topics"][i]["status"];
      chosenIndex = i;
    });
  }

  toogleLoadingSymbolStatusElasticSearch(bool value) {
    printIfDebug("Elastic Search State Changed");
    printIfDebug(new DateTime.now());
    setState(() {
      loadingSymbolStatusElasticSearch = value;
    });
    printIfDebug(loadingSymbolStatusElasticSearch);
  }

  toogleLoadingSymbolFetchMoreResults(bool value) {
    setState(() {
      loadingSymbolFetchMoreResults = value;
    });
  }

  toogleOptionStatus(int chosenIndex, int index) {
    bool filterStatus = false;

    if (searchFilterData["topics"][chosenIndex]["options"][index]["status"]) {
      filterStatus = false;
    } else {
      filterStatus = true;
    }

    setState(() {
      searchFilterData["topics"][chosenIndex]["options"][index]["status"] =
          filterStatus;
    });
  }

  setFilterMenu() {
    setState(() {
      filterMenuStatus = false;
    });
    for (int i = 0; i < searchFilterData["topics"].length; i++) {
      setState(() {
        searchFilterData["topics"][i]["status"] = false;
      });
    }
  }

  updateFilterMenu(index) {
    int categories = searchFilterData["topics"].length;
    for (int i = 0; i < categories; i++) {
      if (index == i) {
        toogleMenuState(i);
      } else {
        setDefaultMenuState(i);
      }
    }
  }

  clearSearchResults() {
    setState(() {
      unkownResults = false;
      fetchedMovieDetail = false;
    });
    printIfDebug("Calling Clear Search Results");
    printIfDebug(resultsHaveBeenCleared);
    if (movieDetail.isNotEmpty) {
      if (movieDetail.containsKey("movies")) {
        if (movieDetail["movies"].isNotEmpty) {
          printIfDebug("Search Results Cleared");
          setState(() {
            movieDetail["movies"] = [];
            resultsHaveBeenCleared = true;
          });
          printIfDebug(unkownResults);
          printIfDebug(movieDetail["movies"]);
        }
      }
    }
  }

  changePageNo(int flag) {
    setState(() {
      pageNo += 1;
    });
    printIfDebug("Move to Page Number");
    printIfDebug(pageNo);
    String pageStartsAtMovieNumber = ((pageNo * 70) - 70).toString();
    String url = "";
    String appliedUrl = "";
    if (flag == 0) {
      url = constructUrlWithFilters(
          "https://api.flixjini.com/roulette/update_next.json?", 0);
      appliedUrl = url;
    } else {
      url = constructUrlWithFilters(
          "https://api.flixjini.com/roulette/start.json?",
          (urlAppendStringLocal!.isEmpty ? 1 : 0));
      appliedUrl = url + "elements=" + pageStartsAtMovieNumber;
    }

    if (skippedIds.length > 0) {
      appliedUrl = appliedUrl + "&movie_ids_next=";
      String ids = "";
      for (int i = 0; i < skippedIds.length; i++) {
        if (i == skippedIds.length - 1) {
          ids = ids + skippedIds.elementAt(i);
        } else {
          ids = ids + skippedIds.elementAt(i) + ",";
        }
      }
      appliedUrl += ids;
    }
    getMoreSearchResults(appliedUrl);
  }

  void movetoSourceCard() {
    printIfDebug("Go To Index 1");
    try {
      (searchResultsController).animateTo(
          (20.0 *
              1), // 100 is the height of container and index of 6th element is 5
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    } catch (exception) {
      printIfDebug("movetoSourceCard" + exception.toString());
      printIfDebug(
          'List Does not exist to move to the top -> This is an Exception');
    }
  }

  getMoreSearchResults(String urlWithSearchParameter) async {
    String rootUrl = urlWithSearchParameter + urlAppendStringLocal! + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    printIfDebug("Paginate to Page");
    printIfDebug(url);
    toogleLoadingSymbolFetchMoreResults(true);
    try {
      var response = await http.get(Uri.parse(url));

      printIfDebug("Entered Try Block");
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("More Search Results Obtained");
        if (data["results"] != null && data["results"].length > 0) {
          var infiniteData = new List.from(movieDetail["movies"])
            ..addAll(data["results"]);
          setState(() {
            movieDetail["movies"] = infiniteData;
          });
          printIfDebug("Movies Fetched, New Length");
          printIfDebug(movieDetail["movies"].length);
        } else {
          printIfDebug("Reached End of Search Results");
          changeSearchResultsStatus(true);
        }
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        changeSearchResultsStatus(true);
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
      toogleLoadingSymbolFetchMoreResults(false);
    } catch (exception) {
      printIfDebug("getMoreSearchResults" + exception.toString());
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  changeSearchResultsStatus(value) {
    setState(() {
      endOfSearchResults = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool flag1 = prefs.getBool('freeflag') ?? false;
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      freeFlag = flag1;
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      printIfDebug("User is already Logged in -> Obtain User Preferences");
      // getPreferences();
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
      loadSavedFilters();
    } else {
      printIfDebug("User can't access User Preferences as he is not logged in");
      showAlert();
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }
    loadStaticData();
  }

  showAlert() {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return RouletteSigninAlert();
      },
    );
  }

  movieAPICall() {
    String urlWithSearchParameter = "";
    if (widget.freshness != null) {
      urlWithSearchParameter = "https://api.flixjini.com" + widget.freshness;
    } else {
      if (authenticationToken.isNotEmpty) {
        urlWithSearchParameter =
            "https://api.flixjini.com/roulette/start.json?jwt_token=" +
                authenticationToken;
        urlWithSearchParameter = freeFlag
            ? urlWithSearchParameter + "&streamingtypelist=free"
            : urlWithSearchParameter + "&streamingtypelist=free,subscription";
        //"https://api.flixjini.com/entertainment/categories/seven-days-ago-movies.json";
      } else {
        urlWithSearchParameter = "https://api.flixjini.com/roulette/start.json";
        urlWithSearchParameter = freeFlag
            ? urlWithSearchParameter + "?streamingtypelist=free"
            : urlWithSearchParameter + "?streamingtypelist=free,subscription";
      }
    }
    if (movieDetail != null &&
        movieDetail["movies"] != null &&
        movieDetail["movies"].length > 0) {
      urlWithSearchParameter = urlWithSearchParameter + "&movie_ids_next=";
      String ids = "";
      for (int i = 0; i < 5; i++) {
        if (i == 4) {
          ids = ids + movieDetail["movies"][i]["id"].toString();
        } else {
          ids = ids + movieDetail["movies"][i]["id"].toString() + ",";
        }
      }
      urlWithSearchParameter += ids;
    }
    getMovieDetails(urlWithSearchParameter);
  }

  getUserQueuePreference() async {
    var response = await http.get(Uri.parse(
        'https://api.flixjini.com/queue/list.json?jwt_token=' +
            authenticationToken));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        // printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('getUserQueuePreference Failed getting IP address' +
          exception.toString());
    }
    if (!mounted) return;
  }

  getUserPreference() async {
    var response = await http.get(Uri.parse(
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        // printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug(
          'getUserPreference Failed getting IP address' + exception.toString());

      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkForOptionSelections(filters) {
    filters.forEach((key, filterIds) {
      for (int i = 0; i < searchFilterData["topics"].length; i++) {
        if (searchFilterData["topics"][i]["key"] == key) {
          printIfDebug("Key Found");
          printIfDebug(key);
          filterIds.forEach((filterId) {
            for (int j = 0;
                j < searchFilterData["topics"][i]["options"].length;
                j++) {
              if (searchFilterData["topics"][i]["options"][j]["filter_id"] ==
                  filterId) {
                printIfDebug("The Filter Ids are");
                printIfDebug(filterId);
                printIfDebug(
                    searchFilterData["topics"][i]["options"][j]["name"]);
                setState(() {
                  searchFilterData["topics"][i]["options"][j]["status"] = true;
                });
              }
            }
          });
        }
      }
    });
  }

  getMovieDetails(String urlWithSearchParameter) async {
    int lengthOfUrlWithSearchParameter = urlWithSearchParameter.length;
    String rootUrl;
    printIfDebug("SubString");
    printIfDebug(urlWithSearchParameter.substring(
        lengthOfUrlWithSearchParameter - 4, lengthOfUrlWithSearchParameter));
    if (urlWithSearchParameter.substring(lengthOfUrlWithSearchParameter - 4,
            lengthOfUrlWithSearchParameter) ==
        "json") {
      rootUrl = urlWithSearchParameter + '?';
    } else {
      rootUrl = urlWithSearchParameter + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    url += urlAppendStringLocal ?? "";
    printIfDebug('\n\nfresh movies pg, url before making request: $url');
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug("Status Code");
      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        // printIfDebug("responseBody" + responseBody.toString());
        storeRouletteSp(responseBody);
        var data = json.decode(responseBody);

        var data1 = [];
        if (urlAppendStringLocal != null && urlAppendStringLocal!.isNotEmpty) {
          this.movieDetail["movies"] = data["results"];
        } else {
          if (movieDetail != null &&
              movieDetail["movies"] != null &&
              movieDetail["movies"].length > 0) {
            data1.add(movieDetail["movies"][0]);
            data1.add(movieDetail["movies"][1]);
            data1.add(movieDetail["movies"][2]);
            data1.add(movieDetail["movies"][3]);
            data1.add(movieDetail["movies"][4]);
            data1.addAll(data["results"]);
            this.movieDetail["movies"] = data1;
          } else {
            this.movieDetail["movies"] = data["results"];
          }
        }

        if (this.mounted) {
          setState(() {
            movieDetail = this.movieDetail;
            fetchedMovieDetail = true;
          });
        }
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (e) {
      printIfDebug("roulette exception" + e.toString());
    }
    if (!mounted) return;
  }

  storeRouletteSp(String rouletteString) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('rouletteOffline', rouletteString);
  }

  getRouletteSp() async {
    printIfDebug("this is done");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? rouletteStringSP = prefs.getString('rouletteOffline');

    int count = prefs.getInt("appopencount") ?? 0;
    if (count > 20) {
      setState(() {
        filterAnimationFlag = false;
      });
    }
    if (rouletteStringSP != null && rouletteStringSP.length > 0) {
      var data = json.decode(rouletteStringSP);
      printIfDebug("roulette sp" + data.toString());
      this.movieDetail["movies"] = data["results"];
      if (this.mounted) {
        setState(() {
          movieDetail = this.movieDetail;
          fetchedMovieDetail = true;
        });
      }
    }
  }

  sendElasticGetRequest(actionUrl) async {
    printIfDebug("LOL");
    setState(() {
      unkownResults = false;
      fetchedMovieDetail = false;
      firstSearch = true;
    });
    String rootUrl = actionUrl;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.statusCode);
      printIfDebug(response.statusCode == 200);
      if (response.statusCode == 200) {
        var fetchedData = json.decode(response.body);
        // printIfDebug(fetchedData);
        printIfDebug("Not yet In - resultsHaveBeenCleared");
        printIfDebug(resultsHaveBeenCleared);
        printIfDebug("Yes we In");
        setState(() {
          movieDetail["movies"] = fetchedData["results"];
          fetchedMovieDetail = true;
        });
        if (movieDetail["movies"].length == 0) {
          setState(() {
            unkownResults = true;
          });
        }
        toogleLoadingSymbolStatusElasticSearch(false);
        printIfDebug("New Data Fetched by Elastic Search");
        changeSearchResultsStatus(false);
        printIfDebug(firstSearch);
        setState(() {
          pageNo = 1;
        });
        printIfDebug("Fresh Batch of reviews");
        printIfDebug(pageNo);
        movetoSourceCard();
        if (!firstSearch) {
          printIfDebug("Go to Top");
          movetoSourceCard();
        } else {
          setState(() {
            firstSearch = false;
            resultsHaveBeenCleared = false;
          });
          printIfDebug("Reset resultsHaveBeenCleared");
          printIfDebug(resultsHaveBeenCleared);
        }
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address' + exception.toString());

      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  // When and How to Trigger Request to Elastic Search For Search Results
  Future delayServerRequest() async {
    if (searchString.length >= 2) {
      setState(() {
        pageNo = 1;
      });
      toogleLoadingSymbolStatusElasticSearch(true);
      String checkString = searchString;
      printIfDebug("Before Timeout");
      printIfDebug("Search (Current) String");
      printIfDebug(searchString);
      printIfDebug("Check (Previous) String");
      printIfDebug(checkString);
      new Timer(const Duration(seconds: 1), () {
        printIfDebug("After Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        if (searchString != checkString) {
          printIfDebug("Dont Make Server Request");
        } else {
          printIfDebug("Make Server Request : Elastic Search");
          String appliedMoviesFilterUrl = constructUrlWithFilters(
              "https://api.flixjini.com/roulette/start.json?", 1);
          printIfDebug(appliedMoviesFilterUrl);
          sendElasticGetRequest(appliedMoviesFilterUrl);
        }
      });
    }
  }

  changeFreeFlag() {
    if (freeFlag) {
      setState(() {
        freeFlag = false;
      });
    } else {
      setState(() {
        freeFlag = true;
      });
    }
  }

  addSkippedId(value) {
    skippedIds.add(value);
    printIfDebug(value);
  }

  Widget displayMovieDetails() {
    return MovieRouletteNew(
      movieDetail: movieDetail,
      userDetail: userDetail,
      userQueue: userQueue,
      authenticationToken: authenticationToken,
      endOfSearchResults: endOfSearchResults,
      changePageNo: changePageNo,
      searchResultsController: searchResultsController,
      dataKey: dataKey,
      showOriginalPosters: widget.showOriginalPosters,
      addSkippedId: addSkippedId,
      ctrl: ctrl,
      searchFilterUserData: searchFilterUserData,
    );
  }

  Widget closeOption() {
    if (filterMenuStatus) {
      return new Positioned(
        top: 25.0,
        right: 20.0,
        child: new GestureDetector(
          child: new Container(
            child: new Container(
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: new Image.asset(
                "assests/close_circle.png",
                width: 25.0,
                height: 25.0,
              ),
            ),
          ),
          onTap: () {
            setFilterMenu();
          },
        ),
      );
    } else {
      return new Container();
    }
  }

  Widget displayLoader() {
    return new Center(
      child: new CircularProgressIndicator(
          backgroundColor: Constants.appColorLogo),
    );
  }

  Widget buildOptions(BuildContext context, int index) {
    printIfDebug("GGGG");
    printIfDebug(index);
    return new Center(
      child: new Container(
        width: 100.0,
        height: 70.0,
        child: new Text(options[index]["name"]),
      ),
    );
  }

  constructUrlWithFilters(String pURL, int flag) {
    String getUrl = "";
    String url = "";
    if (flag == 0) {
      url = authenticationToken.isNotEmpty
          ? pURL + "jwt_token=" + authenticationToken + "&"
          : pURL;
    } else {
      for (int index = 0; index < searchFilterData["topics"].length; index++) {
        if ((searchFilterData["topics"][index]["group"] == "clickableImages") ||
            (searchFilterData["topics"][index]["group"] == "checkBoxes")) {
          printIfDebug("Have caught clickable images");
          printIfDebug(searchFilterData["topics"][index]["heading"]);
          bool flag1 = true;
          bool selected = false;
          List<String> options = [];
          for (int i = 0;
              i < searchFilterData["topics"][index]["options"].length;
              i++) {
            if (searchFilterData["topics"][index]["options"][i]["status"]) {
              printIfDebug(
                  searchFilterData["topics"][index]["options"][i]["name"]);
              selected = true;
              if (flag1) {
                getUrl += searchFilterData["topics"][index]["key"] + "=";
              }
              flag1 = false;
              options.add(
                  searchFilterData["topics"][index]["options"][i]["filter_id"]);
            }
          }
          getUrl += options.join(',');
          if (selected) {
            getUrl += "&";
          }
        } else if (searchFilterData["topics"][index]["group"] == "slider") {
          getUrl += (searchFilterData["topics"][index]["key_1"] + "=");
          getUrl += (searchFilterData["topics"][index]["features"]["rangeLower"]
                  .round())
              .toString();
          if (searchFilterData["topics"][index]["features"]["rangeLower"] ==
              searchFilterData["topics"][index]["features"]["min"]) {
            printIfDebug("Range Lower is equal to the default, dont display");
          } else {
            // visibleFilters[searchFilterData["topics"][index]["key_1"]] = [
            //   (searchFilterData["topics"][index]["features"]["rangeLower"]
            //           .round())
            //       .toString()
            // ];
          }
          getUrl += "&";
          getUrl += (searchFilterData["topics"][index]["key_2"] + "=");
          getUrl += (searchFilterData["topics"][index]["features"]["rangeUpper"]
                  .round())
              .toString();
          if (searchFilterData["topics"][index]["features"]["rangeUpper"] ==
              searchFilterData["topics"][index]["features"]["max"]) {
            printIfDebug("Range Upper is equal to the default, dont display");
          } else {
            // visibleFilters[searchFilterData["topics"][index]["key_2"]] = [
            //   (searchFilterData["topics"][index]["features"]["rangeUpper"]
            //           .round())
            //       .toString()
            // ];
          }
          getUrl += "&";
        }
      }
      if (searchFilterData.containsKey("hidden_topics")) {
        printIfDebug("Contains Location");
        for (int i = 0; i < searchFilterData["hidden_topics"].length; i++) {
          if (searchFilterData["hidden_topics"][i]["key"] == "location") {
            if (searchFilterData["hidden_topics"][i]["value"].isNotEmpty) {
              getUrl += searchFilterData["hidden_topics"][i]["key"] +
                  "=" +
                  searchFilterData["hidden_topics"][i]["value"] +
                  "&";
              printIfDebug("Location Added to update url");
              printIfDebug(searchFilterData["hidden_topics"][i]["value"]);
            }
            break;
          }
        }
      } else {
        printIfDebug("Location can't be set");
      }

      url = authenticationToken.isNotEmpty
          ? pURL + "jwt_token=" + authenticationToken + "&"
          : pURL;
      url = freeFlag
          ? url + "streamingtypelist=free"
          : url + "streamingtypelist=free,subscription";
      url = url + "&" + getUrl;
    }

    printIfDebug('\n\nfor the update user profile api, complete url: $url');
    return url;
  }

  filterSearchResults() {
    setState(() {
      urlAppendStringLocal = "";
    });
    String rootUrl = constructUrlWithFilters(
        "https://api.flixjini.com/roulette/start.json?", 1);
    printIfDebug("New Search with Filter Applied");
    printIfDebug(rootUrl);
    sendElasticGetRequest(rootUrl);
    resetPage();
    setFilterMenu();
  }

  savedFilterResults(String filterString) {
    String rootUrl = "https://api.flixjini.com/roulette/start.json?";

    rootUrl = authenticationToken.isNotEmpty
        ? rootUrl + "jwt_token=" + authenticationToken + "&"
        : rootUrl;
    rootUrl = freeFlag
        ? rootUrl + "streamingtypelist=free"
        : rootUrl + "streamingtypelist=free,subscription";
    rootUrl = rootUrl + filterString + "&";

    printIfDebug("New Search with Filter Applied");
    printIfDebug(rootUrl);
    sendElasticGetRequest(rootUrl);
    resetPage();
    // setFilterMenu();
  }

  resetPage() {
    ctrl.animateToPage(0,
        duration: Duration(
          milliseconds: 500,
        ),
        curve: Curves.easeIn);
  }

  setToTitle(value) {
    setState(() {
      howFresh = value;
    });
  }

  setToDisplayOnTitle(topic, index) {
    if (topic["key"] == "movie_tags") {
      setState(() {
        howFresh = topic["options"][index]["name"];
      });
    }
  }

  Widget renderDefaultMessage(String defaultMessage, FontWeight fontWeight) {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: width * 0.75,
      child: new Center(
        child: new Text(
          defaultMessage,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: Constants.appColorFont,
            fontSize: 15.0,
          ),
          textAlign: TextAlign.center,
          maxLines: 4,
        ),
      ),
    );
  }

  Widget renderDefaultMessagewithSpan(
      String defaultMessage, String defaultSpanMessage) {
    return new Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultMessage(defaultMessage, FontWeight.normal),
            renderDefaultMessage(defaultSpanMessage, FontWeight.bold),
          ],
        ),
      ),
    );
  }

  Widget renderDefaultImage() {
    return new Center(
      child: new Container(
        width: 100.0,
        height: 100.0,
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "sadsmile"),
      ),
    );
  }

  Widget insertDivider() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(
        top: 15,
      ),
      height: 1,
      width: width - 10,
      color: Constants.appColorDivider,
    );
  }

  Widget defaultSearchResults(String defaultMessage) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        renderDefaultImage(),
        renderDefaultMessage(defaultMessage, FontWeight.normal),
      ],
    );
  }

  Widget beforeSearchingMessage(
      String defaultMessage, String defaultSpanMessage) {
    return Container(
      color: Constants.appColorL1,
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultImage(),
            renderDefaultMessagewithSpan(defaultMessage, defaultSpanMessage),
          ],
        ),
      ),
    );
  }

  displayLoaderDuringFetchMore() {
    if (loadingSymbolStatusElasticSearch) {
      return new Positioned.fill(
        child: new Container(
          color: Constants.appColorL2,
          child: new Center(
            child: RouletteShimmerCards(),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  displayLoaderForInitialFetch() {
    return Container(
      color: Constants.appColorFont,
      child: new Center(
        child: RouletteShimmerCards(),
      ),
    );
  }

  checkIfFirstSearchRequestTriggered() {
    printIfDebug("We In");
    printIfDebug(pageNo);
    printIfDebug(fetchedMovieDetail);
    printIfDebug(loadingSymbolStatusElasticSearch);
    return displayLoaderForInitialFetch();
  }

  checkIfUnkownResults() {
    String defaultMessage = "";
    String defaultSpanMessage = "";
    printIfDebug("Due to Unkown Results");
    printIfDebug(unkownResults);
    if (unkownResults) {
      String defaultMessage = "Oop's \n\n No Movies / Tv Shows Found";
      return defaultSearchResults(defaultMessage);
    } else {
      defaultMessage = "Discover your Favourite Movies / Tv Shows";
      defaultSpanMessage = "Search Now";
      return beforeSearchingMessage(defaultMessage, defaultSpanMessage);
    }
  }

  Widget checkDataReady() {
    // double screenHeight = MediaQuery.of(context).size.height;

    try {
      return (fetchedMovieDetail &&
              fetchedUserPreference &&
              fetchedUserQueuePreference)
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 40,
                  ),
                  child: Row(
                    children: <Widget>[
                      // widget.checkStack() || true
                      //     ?
                      Container(
                        width: 40,
                      ),
                      // : GestureDetector(
                      //     onTap: () {
                      //       widget.moveToPreviousTab();
                      //     },
                      //     child: Card(
                      //       color: Constants.appColorL2,
                      //       // with Card
                      //       child: Image.asset(
                      //         "assests/back_roulette.png",
                      //         height: 40,
                      //         width: 40,
                      //       ),
                      //       elevation: 18.0,
                      //       shape: CircleBorder(),
                      //       clipBehavior: Clip.antiAlias,
                      //     ),
                      //   ),
                      Spacer(),
                      Text(
                        "GENIE",
                        style: TextStyle(
                          color: Constants.appColorFont.withOpacity(0.7),
                          fontSize: 18,
                        ),
                      ),
                      Spacer(),
                      GestureDetector(
                        child: !filterAnimationFlag
                            ? Card(
                                color: Constants.appColorL2,
                                child: Image.asset(
                                  "assests/filter_roulette.png",
                                  height: 40,
                                  width: 40,
                                ),
                                elevation: 18.0,
                                shape: CircleBorder(),
                                clipBehavior: Clip.antiAlias,
                              )
                            : ClipRRect(
                                borderRadius: new BorderRadius.circular(20.0),
                                child: Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    // shape: BoxShape.circle,
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                  child: FlareActor(
                                    "assests/geniefilter.flr",
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    animation: "geniefilter",
                                  ),
                                ),
                              ),
                        onTap: () {
                          setState(() {
                            filterShowFlag = true;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    // height: screenHeight - 120,
                    child: movieDetail["movies"].length > 0
                        ? displayMovieDetails()
                        : checkIfUnkownResults(),
                  ),
                ),
              ],
            )
          : checkIfFirstSearchRequestTriggered();
    } catch (exception) {
      printIfDebug("checkDataReady Exception" + exception.toString());
      return checkIfFirstSearchRequestTriggered();
    }
  }

  void resetRangeOptions(int i) {
    setState(() {
      searchFilterData["topics"][i]["features"]["rangeLower"] =
          searchFilterData["topics"][i]["features"]["min"];
      searchFilterData["topics"][i]["features"]["rangeUpper"] =
          searchFilterData["topics"][i]["features"]["max"];
    });
    printIfDebug("Range Options have been Reset");
  }

  void updateMinMaxRange(
      double newLowerValue, double newUpperValue, int index) {
    setState(() {
      searchFilterData["topics"][index]["features"]["rangeLower"] =
          newLowerValue;
      searchFilterData["topics"][index]["features"]["rangeUpper"] =
          newUpperValue;
    });
    printIfDebug("Updated Min/ Max Values");
  }

  showFilter(bool flag) {
    setState(() {
      filterShowFlag = flag;
    });
  }

  @override
  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;
    // double screenHeight = MediaQuery.of(context).size.height;
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      body: Stack(children: <Widget>[
        checkDataReady(),
        filterShowFlag
            ? MovieSearchFilter(
                updateFilterMenu: updateFilterMenu,
                searchFilterData: searchFilterData,
                setToDisplayOnTitle: setToDisplayOnTitle,
                toogleOptionStatus: toogleOptionStatus,
                filterSearchResults: filterSearchResults,
                showFilter: showFilter,
                changeFreeFLag: changeFreeFlag,
                setSelectedFilterStatus: setSelectedFilterStatus,
                resetSelectedFilterStatus: resetSelectedFilterStatus,
                updateMinMaxRange: updateMinMaxRange,
                resetRangeOptions: resetRangeOptions,
                userSavedFilters: userSavedFilters,
                authenticationToken: authenticationToken,
                loadSavedFilters: loadSavedFilters,
                savedFilterResults: savedFilterResults,
                setSavedFilterStatus: setSavedFilterStatus,
              )
            : Container(),
      ]),
    );
  }
}
