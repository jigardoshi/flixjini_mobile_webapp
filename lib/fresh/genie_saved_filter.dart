import 'dart:convert';

import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/constants.dart';

class GenieSavedFilter extends StatefulWidget {
  GenieSavedFilter({
    this.userSavedFilters,
    this.authenticationToken,
    this.savedFilterResults,
    this.loadSavedFilters,
    this.setSavedFilterStatus,
  });
  final userSavedFilters,
      authenticationToken,
      loadSavedFilters,
      savedFilterResults,
      setSavedFilterStatus;
  GenieSavedFilterState createState() => GenieSavedFilterState();
}

class GenieSavedFilterState extends State<GenieSavedFilter> {
  List<Widget> listOfWidgetsForSavedFilters = <Widget>[];
  List<Widget> secondListOfWidgetsForSavedFilters = <Widget>[];

  bool hideFlagSaved = true, showMoreFlag = false;

  @override
  void initState() {
    super.initState();
    takeUsersSavedFilters(widget.userSavedFilters);
  }

  void takeUsersSavedFilters(List userSavedFilters) {
    int userSavedFiltersCount = userSavedFilters.length;
    Map oneFilterJSON = new Map();
    printIfDebug('\n\nno of filters user has saved is: $userSavedFiltersCount');
    for (int i = 0; i < userSavedFiltersCount; i++) {
      // int id = userSavedFilters[i]["movie_saved_filter"]["id"];
      // printIfDebug('\n\nid is: $id');
      oneFilterJSON = userSavedFilters[i]["movie_saved_filter"];
      // printIfDebug('\n\none filter json: $oneFilterJSON');
      if (i < 4) {
        addOneFilterToList(oneFilterJSON, i, listOfWidgetsForSavedFilters);
      } else {
        addOneFilterToList(
            oneFilterJSON, i, secondListOfWidgetsForSavedFilters);
      }
    }
  }

  void addOneFilterToList(
      Map oneFilterJSON, int i, listOfWidgetsForSavedFilters) {
    String filterName = oneFilterJSON["name"];
    int filterId = oneFilterJSON["id"];
    String filterQueryString = oneFilterJSON["query_string"];

    listOfWidgetsForSavedFilters.add(
      GestureDetector(
        child: Container(
          height: 50,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 20.0,
                      ),
                      child: Text(
                        capitalize(filterName),
                        style: TextStyle(
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(
                        right: 10.0,
                      ),
                      // height: 20,
                      child: GestureDetector(
                        child: Image.asset(
                          'assests/trash_icon.png',
                          color: Constants.appColorDivider.withOpacity(0.6),
                          height: 15,
                        ),
                        onTap: () {
                          getConfirmationDialogForDeleteFilter(
                              filterId, filterName, filterQueryString);
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  left: 10,
                  right: 10,
                ),
                // width: 100,
                height: 1,
                color: Constants.appColorL2,
              ),
            ],
          ),
        ),
        onTap: () {
          getConfirmationBeforeApplyingFilters(
              filterId, filterName, filterQueryString);
        },
      ),
    );
  }

  Future<dynamic> getConfirmationBeforeApplyingFilters(
      int filterId, String filterName, String filterQueryString) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Constants.appColorL2,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Confirm',
                style: TextStyle(
                  color: Constants.appColorFont,
                ),
              ),
              Container(
                color: Constants.appColorLogo,
                margin: EdgeInsets.only(
                  top: 5,
                ),
                height: 3,
                width: 50,
              ),
            ],
          ),
          content:
              // Text(
              //   'Are you sure you want to apply the filter $filterName?',
              //   style: TextStyle(
              //     color: Constants.appColorFont,
              //   ),
              // ),
              Container(
            height: 90,
            child: Column(
              children: <Widget>[
                Text(
                  'Are you sure you want to apply the filter $filterName?',
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                      ),
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Apply',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          printIfDebug('\n\nuser chose yes to logout');
                          Navigator.of(context).pop();
                          takeUserToFilterPage(
                              filterId, filterName, filterQueryString);
                          // showToast(
                          // 'Logout Successful', 'long', 'bottom', 1, Constants.appColorLogo);
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void takeUserToFilterPage(
      int filterId, String filterName, String filterQueryString) {
    printIfDebug(
        '\n\ntapped filter details: \nfilter name: $filterName, filter id: $filterId, filter query string: $filterQueryString');
    widget.savedFilterResults(filterQueryString);
    String appliedFilterUrl =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?";
    // filterQueryString = filterQueryString.substring(1);
    appliedFilterUrl += filterQueryString;
    Map<String, dynamic> myMap = new Map<String, dynamic>(),
        visibleFiltersMap = new Map();
    Uri uri = Uri.parse(appliedFilterUrl);
    myMap = uri.queryParametersAll;

    printIfDebug(
        '\n\n final applied filter url: $appliedFilterUrl, and its type is string');
    printIfDebug('\n\n query params map: $myMap, and its type is map');
    try {
      widget.setSavedFilterStatus(myMap);
    } catch (e) {
      printIfDebug("filter apply exception" + e.toString());
    }
    // /*
    //  * for each key, if the corresponding value has more than one element in the list,
    //  * all the elements in the list are together considered as one element.
    //  * so, to fix this issue, list is encoded to string and then added double proper quotes
    //  * to make sure that the elements are discrete and not considered together as one element.
    //  */
    // myMap.forEach((key, value) {
    //   printIfDebug('\n\nkey $key');
    //   int valueLength = value.length;
    //   printIfDebug('\n\nvalue length: $valueLength');
    //   String tempString = json.encode(value);
    //   tempString = tempString.replaceAll(',', '", "');
    //   printIfDebug('\n\ncomma and space added temp string: $tempString');

    //   List tempList = json.decode(tempString);
    //   printIfDebug('\n\ntemp list length: ${tempList.length}');
    //   visibleFiltersMap[key] = tempList;
    // });

    // printIfDebug('\n\nfinal map before navigating: $visibleFiltersMap');

    // Navigator.of(context).push(new MaterialPageRoute(
    //     builder: (BuildContext context) => new MovieFilterPage(
    //           appliedFilterUrl: appliedFilterUrl,
    //           visibleFilters: visibleFiltersMap,
    //           // add these 2 params
    //           // visibilityStatus: ,
    //           inTheaterKey: false,
    //         )));
  }

  Future<dynamic> getConfirmationDialogForDeleteFilter(
      int filterId, String filterName, String filterQueryString) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Constants.appColorL2,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Confirm',
                style: TextStyle(
                  color: Constants.appColorFont,
                ),
              ),
              Container(
                color: Constants.appColorLogo,
                margin: EdgeInsets.only(
                  top: 5,
                ),
                height: 3,
                width: 50,
              ),
            ],
          ),
          content:
              // Text(
              //   'Are you sure you want to delete the filter $filterName?',
              //   style: TextStyle(
              //     color: Constants.appColorFont,
              //   ),
              // ),
              Container(
            height: 90,
            child: Column(
              children: <Widget>[
                Text(
                  'Are you sure you want to delete the filter $filterName?',
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 15,
                    height: 1.3,
                  ),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: 35,
                        right: 35,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'No',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                          ),
                        ),
                        onTap: () {
                          printIfDebug('\n\nuser chose no to logout');
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.only(
                      //   left: 20,
                      // ),
                      padding: EdgeInsets.only(
                        left: 35,
                        right: 35,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Yes',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                          ),
                        ),
                        onTap: () {
                          printIfDebug(
                              '\n\nuser chose yes to delete the filter');
                          Navigator.of(context).pop();
                          printIfDebug(
                              '\n\ndelete the filter, having the following details: \n\nfilterId: $filterId, filterName: $filterName, filterQueryString: $filterQueryString');
                          deleteSavedFilter(
                              filterId, filterName, filterQueryString);
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void deleteSavedFilter(
      int filterId, String filterName, String filterQueryString) async {
    String apiEndpointForDeleteFilter =
        "https://api.flixjini.com/queue/delete_filters.json?";
    apiEndpointForDeleteFilter =
        await fetchDefaultParams(apiEndpointForDeleteFilter);
    apiEndpointForDeleteFilter += constructHeader('&');
    apiEndpointForDeleteFilter +=
        "&magic=true&country_code=IN&id=$filterId&jwt_token=";
    // String authToken = await getStringFromSP('widget.authenticationToken');
    apiEndpointForDeleteFilter += widget.authenticationToken;
    printIfDebug(
        '\n\ncomplete url before making a request to delete filter api is: $apiEndpointForDeleteFilter');

    callDeleteFilterAPI(apiEndpointForDeleteFilter, filterId);
  }

  void callDeleteFilterAPI(String url, int filterId) {
    var resBody;
    try {
      http.get(
          Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        setState(() {
          listOfWidgetsForSavedFilters = [];
          secondListOfWidgetsForSavedFilters = [];
          widget.loadSavedFilters();
        });
        //
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  Widget myWidget1() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        child: new Card(
          color: Constants.appColorL2,
          elevation: 6.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 20,
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10,
                  // bottom: 10,
                ),
                child: Text(
                  "SAVED FILTERS",
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14.0,
                    letterSpacing: 1.0,
                    color:
                        // highlightFreeOptions()
                        //     ? Constants.appColorLogo
                        //     :
                        Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ),
              Container(
                color: Constants.appColorL2,
                child: setupAlertDialogContainer(),
              ),
              showMoreFlag
                  ? Container(
                      color: Constants.appColorL2,
                      child: secondSetupAlertDialogContainer(),
                    )
                  : Container(),
              showMore(),
            ],
          ),
        ),
      ),
    );
  }

  showMore() {
    return Center(
      child: secondListOfWidgetsForSavedFilters != null &&
              secondListOfWidgetsForSavedFilters.isNotEmpty
          ? GestureDetector(
              onTap: () {
                setState(() {
                  showMoreFlag = !showMoreFlag;
                });
              },
              child: Container(
                width: 60,
                margin: EdgeInsets.only(
                  bottom: 10,
                ),
                child: !showMoreFlag
                    ? Image.asset(
                        "assests/down_arrow_search.png",
                        color: Constants.appColorDivider,
                      )
                    : Image.asset(
                        "assests/up_arrow_search.png",
                        color: Constants.appColorDivider,
                      ),
              ),
            )
          : Container(),
    );
  }

// Widget myWidget1() {
//     return new Container(
//       padding: const EdgeInsets.all(10.0),
//       child: new Container(
//         padding: const EdgeInsets.all(5.0),
//         child: new Card(
//           color: Constants.appColorL2,
//           elevation: 6.0,
//           child: ExpansionTile(
//             backgroundColor: Constants.appColorL2,
//             initiallyExpanded: false,
//             onExpansionChanged: hideTagTextListSaved,
//             trailing: hideFlagSaved
//                 ? Image.asset(
//                     'assests/greendown.png',
//                     height: 10,
//                     width: 10,
//                   )
//                 : Image.asset(
//                     'assests/greenup.png',
//                     height: 10,
//                     width: 10,
//                   ),
//             title: Container(
//               child: new Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 textDirection: TextDirection.ltr,
//                 children: <Widget>[
//                   new Expanded(
//                       child: new Stack(
//                         children: <Widget>[
//                           new Container(
//                             height: 20.0,
//                             color: Constants.appColorL2,
//                           ),
//                           new Positioned(
//                             width: 3.0,
//                             height: 20.0,
//                             child: new Container(
//                               color: Constants.appColorLogo,
//                             ),
//                           ),
//                         ],
//                       ),
//                       flex: 1),
//                   new Expanded(
//                     child: new Text(
//                       'Saved Filters',
//                       style: new TextStyle(
//                           fontWeight: FontWeight.bold,
//                           color: Constants.appColorFont,
//                           fontSize: 13.0),
//                       textAlign: TextAlign.left,
//                     ),
//                     flex: 8,
//                   ),
//                 ],
//               ),
//             ),
//             children: <Widget>[
//               new Container(
//                 color: Constants.appColorL2,
//                 padding: const EdgeInsets.all(0.0),
//                 child: setupAlertDialogContainer(),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
  hideTagTextListSaved(bool open) {
    if (open) {
      setState(() {
        hideFlagSaved = false;
      });
    } else {
      setState(() {
        hideFlagSaved = true;
      });
    }
  }

  Widget setupAlertDialogContainer() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: ListView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        addAutomaticKeepAlives: false,
        scrollDirection: Axis.vertical,
        padding: const EdgeInsets.all(0.0),
        itemCount: listOfWidgetsForSavedFilters.length,
        itemBuilder: (BuildContext context, int index) {
          return listOfWidgetsForSavedFilters[index];
        },
      ),
    );
  }

  Widget secondSetupAlertDialogContainer() {
    return Container(
      child: ListView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        addAutomaticKeepAlives: false,
        scrollDirection: Axis.vertical,
        padding: const EdgeInsets.all(0.0),
        itemCount: secondListOfWidgetsForSavedFilters.length,
        itemBuilder: (BuildContext context, int index) {
          return secondListOfWidgetsForSavedFilters[index];
        },
      ),
    );
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  @override
  Widget build(BuildContext context) {
    return myWidget1();
  }
}
