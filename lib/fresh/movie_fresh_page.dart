import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
// import  'package:streaming_entertainment/search/movie_search_bar.dart';
import 'package:flixjini_webapp/search/movie_search_filter.dart';
import 'package:flixjini_webapp/search/movie_search_results.dart';
// import  'package:streaming_entertainment/search/movie_search_popular.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import	'package:streaming_entertainment/navigation/movie_navigator.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_cards.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'dart:ui';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFreshPage extends StatefulWidget {
  MovieFreshPage({
    this.moveToPreviousTab,
    this.freshness,
    this.showOriginalPosters,
    Key? key,
  }) : super(key: key);

  final moveToPreviousTab;
  final freshness;
  final showOriginalPosters;

  @override
  _MovieFreshPageState createState() => new _MovieFreshPageState();
}

class _MovieFreshPageState extends State<MovieFreshPage> {
  ScrollController searchResultsController = new ScrollController();
  final dataKey = new GlobalKey();
  final TextEditingController searchController = new TextEditingController();
  bool fetchedMovieDetail = false;
  bool fetchedUserPreference = false;
  bool fetchedUserQueuePreference = false;
  bool endOfSearchResults = false;
  bool filterMenuStatus = false;
  bool searchFilterDataReady = false;
  bool unkownResults = false;

  int pageNo = 1;
  var sortData = {};
  var movieDetail = {};
  var userDetail = {};
  var userQueue = {};
  var searchFilterData;
  var options = [];
  bool firstSearch = true;
  bool loadingSymbolStatusElasticSearch = false;
  bool loadingSymbolFetchMoreResults = false;
  bool resultsHaveBeenCleared = false;
  bool searchBoxEmpty = true;
  int chosenIndex = 0;
  String searchString = "";
  String checkString = "";
  String authenticationToken = "";
  String filtersSet = "";
  String stringSecondaryMenu = "";
  String howFresh = "7 Days Ago";

  var addedOption = {
    "heading": "Added",
    "url": "assests/type.png",
    "group": "selectable",
    "key": "movie_tags",
    "status": false,
    "options": [
      {"name": "7 Days Ago", "filter_id": "seven-days-ago", "status": false},
      {
        "name": "14 Days Ago",
        "filter_id": "fourteen-days-ago",
        "status": false
      },
      {"name": "1 Month Ago", "filter_id": "one-month-ago", "status": false}
    ]
  };

  @override
  void initState() {
    super.initState();
    printIfDebug("Welcome to Search Page");
    loadStaticData();
    authenticationStatus();
    logUserScreen('Flixjini Fresh Page', 'MovieFreshPage');
    basedOnFresshness();
  }

  basedOnFresshness() {
    String value = "7 Days Ago";
    if (widget.freshness != null) {
      if (widget.freshness.contains("fourteen-days-ago-movies")) {
        value = "14 Days Ago";
      } else if (widget.freshness.contains("one-month-ago-movies")) {
        value = "1 Month Ago";
      } else {
        printIfDebug("7 Days or something else -> resort to default");
      }
    }
    setToTitle(value);
  }

  prepareForFreshSecondaryMenu(secondaryMenu) {
    for (int index = 0; index < secondaryMenu["topics"].length; index++) {
      if (!secondaryMenu["topics"][index]["belongsTo"].contains("fresh")) {
        secondaryMenu["topics"].removeAt(index);
      }
    }
    return secondaryMenu;
  }

  loadStaticData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringSecondaryMenu = (prefs.getString('stringSecondaryMenu') ?? "");
    });
    printIfDebug("Search Menu in String Form");
    printIfDebug(stringSecondaryMenu);
    var data = json.decode(stringSecondaryMenu);
    data = prepareForFreshSecondaryMenu(data);
    setState(() {
      searchFilterData = data;
      searchFilterDataReady = true;
    });
    printIfDebug("Search Filter Menu");
    printIfDebug(searchFilterData["topics"]);
  }

  getSearchFilterMenu() async {
    String filterMenuOptionsUrl =
        "https://api.flixjini.com/movies/get_user_profile.json?jwt_token=" +
            authenticationToken;
    String rootUrl = filterMenuOptionsUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("Connect to profile");
        printIfDebug(data);
      } else {
        this.searchFilterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.searchFilterData["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  void toogleSearchBoxEmpty(value) {
    setState(() {
      searchBoxEmpty = value;
    });
  }

  getPreferences() {
    getUserPreference();
    getUserQueuePreference();
  }

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  setDefaultMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] = false;
    });
    printIfDebug("Update Status");
    printIfDebug(searchFilterData["topics"][i]);
  }

  toogleMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] =
          searchFilterData["topics"][i]["status"] ? false : true;
      filterMenuStatus = searchFilterData["topics"][i]["status"];
      chosenIndex = i;
    });
  }

  toogleLoadingSymbolStatusElasticSearch(bool value) {
    printIfDebug("Elastic Search State Changed");
    printIfDebug(new DateTime.now());
    setState(() {
      loadingSymbolStatusElasticSearch = value;
    });
    printIfDebug(loadingSymbolStatusElasticSearch);
  }

  toogleLoadingSymbolFetchMoreResults(bool value) {
    setState(() {
      loadingSymbolFetchMoreResults = value;
    });
  }

  toogleOptionStatus(int chosenIndex, int index) {
    bool filterStatus = false;
    printIfDebug(searchFilterData["topics"][chosenIndex]["options"].length);
    for (int i = 0;
        i < searchFilterData["topics"][chosenIndex]["options"].length;
        i++) {
      if (i == index) {
        if (searchFilterData["topics"][chosenIndex]["options"][i]["status"]) {
          filterStatus = false;
        } else {
          filterStatus = true;
        }
      } else {
        filterStatus = false;
      }
      setState(() {
        searchFilterData["topics"][chosenIndex]["options"][i]["status"] =
            filterStatus;
      });
    }
  }

  setFilterMenu() {
    setState(() {
      filterMenuStatus = false;
    });
    for (int i = 0; i < searchFilterData["topics"].length; i++) {
      setState(() {
        searchFilterData["topics"][i]["status"] = false;
      });
    }
  }

  updateFilterMenu(index) {
    int categories = searchFilterData["topics"].length;
    for (int i = 0; i < categories; i++) {
      if (index == i) {
        toogleMenuState(i);
      } else {
        setDefaultMenuState(i);
      }
    }
  }

  clearSearchResults() {
    setState(() {
      unkownResults = false;
      fetchedMovieDetail = false;
    });
    printIfDebug("Calling Clear Search Results");
    printIfDebug(resultsHaveBeenCleared);
    if (movieDetail.isNotEmpty) {
      if (movieDetail.containsKey("movies")) {
        if (movieDetail["movies"].isNotEmpty) {
          printIfDebug("Search Results Cleared");
          setState(() {
            movieDetail["movies"] = [];
            resultsHaveBeenCleared = true;
          });
          printIfDebug(unkownResults);
          printIfDebug(movieDetail["movies"]);
        }
      }
    }
  }

  changePageNo() {
    setState(() {
      pageNo += 1;
    });
    printIfDebug("Move to Page Number");
    printIfDebug(pageNo);
    String pageStartsAtMovieNumber = ((pageNo * 70) - 70).toString();
    String url = constructUrlWithFilters();
    String appliedUrl = url + "&elements=" + pageStartsAtMovieNumber;
    getMoreSearchResults(appliedUrl);
  }

  void movetoSourceCard() {
    printIfDebug("Go To Index 1");
    try {
      (searchResultsController).animateTo(
          (20.0 *
              1), // 100 is the height of container and index of 6th element is 5
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    } catch (exception) {
      printIfDebug(
          'List Does not exist to move to the top -> This is an Exception');
    }
  }

  getMoreSearchResults(String urlWithSearchParameter) async {
    String rootUrl = urlWithSearchParameter + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    printIfDebug("Paginate to Page");
    printIfDebug(url);
    toogleLoadingSymbolFetchMoreResults(true);
    var response = await http.get(Uri.parse(url));
    try {
      printIfDebug("Entered Try Block");
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("More Search Results Obtained");
        if (data["movies"].length > 0) {
          var infiniteData = new List.from(movieDetail["movies"])
            ..addAll(data["movies"]);
          setState(() {
            movieDetail["movies"] = infiniteData;
          });
          printIfDebug("Movies Fetched, New Length");
          printIfDebug(movieDetail["movies"].length);
        } else {
          printIfDebug("Reached End of Search Results");
          changeSearchResultsStatus(true);
        }
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        changeSearchResultsStatus(true);
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
      toogleLoadingSymbolFetchMoreResults(false);
    } catch (exception) {
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  changeSearchResultsStatus(value) {
    setState(() {
      endOfSearchResults = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      printIfDebug("User is already Logged in -> Obtain User Preferences");
      getPreferences();
    } else {
      printIfDebug("User can't access User Preferences as he is not logged in");
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }

    String urlWithSearchParameter = "";
    if (widget.freshness != null) {
      urlWithSearchParameter = "https://api.flixjini.com" + widget.freshness;
    } else {
      urlWithSearchParameter =
          "https://api.flixjini.com/entertainment/categories/seven-days-ago-movies.json?jwt_token=" +
              authenticationToken;
    }
    getMovieDetails(urlWithSearchParameter);
  }

  getUserQueuePreference() async {
    var response = await http.get(Uri.parse(
        'https://api.flixjini.com/queue/list.json?jwt_token=' +
            authenticationToken));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  getUserPreference() async {
    var response = await http.get(Uri.parse(
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkForOptionSelections(filters) {
    filters.forEach((key, filterIds) {
      for (int i = 0; i < searchFilterData["topics"].length; i++) {
        if (searchFilterData["topics"][i]["key"] == key) {
          printIfDebug("Key Found");
          printIfDebug(key);
          filterIds.forEach((filterId) {
            for (int j = 0;
                j < searchFilterData["topics"][i]["options"].length;
                j++) {
              if (searchFilterData["topics"][i]["options"][j]["filter_id"] ==
                  filterId) {
                printIfDebug("The Filter Ids are");
                printIfDebug(filterId);
                printIfDebug(
                    searchFilterData["topics"][i]["options"][j]["name"]);
                setState(() {
                  searchFilterData["topics"][i]["options"][j]["status"] = true;
                });
              }
            }
          });
        }
      }
    });
  }

  getMovieDetails(String urlWithSearchParameter) async {
    int lengthOfUrlWithSearchParameter = urlWithSearchParameter.length;
    String rootUrl;
    printIfDebug("SubString");
    printIfDebug(urlWithSearchParameter.substring(
        lengthOfUrlWithSearchParameter - 4, lengthOfUrlWithSearchParameter));
    if (urlWithSearchParameter.substring(lengthOfUrlWithSearchParameter - 4,
            lengthOfUrlWithSearchParameter) ==
        "json") {
      rootUrl = urlWithSearchParameter + '?';
    } else {
      rootUrl = urlWithSearchParameter + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);

    printIfDebug('\n\nfresh movies pg, url before making request: $url');

    var response = await http.get(Uri.parse(url));
    printIfDebug("Status Code");
    printIfDebug(response.statusCode);
    printIfDebug(url);
    if (response.statusCode == 200) {
      String responseBody = response.body;
      var data = json.decode(responseBody);
      printIfDebug("Fresh Movie Results");
      printIfDebug(data);
      checkForOptionSelections(data["filter_json"]);
      printIfDebug("Filter Json - Fresh Movie Results");
      printIfDebug(data["filter_json"]);
      this.movieDetail = data;
      if (this.mounted) {
        setState(() {
          movieDetail = this.movieDetail;
          fetchedMovieDetail = true;
        });
      }
    } else {
      printIfDebug(
          'Error getting IP address:\nHttp status ${response.statusCode}');
    }
    if (!mounted) return;
  }

  sendElasticGetRequest(actionUrl) async {
    printIfDebug("LOL");
    setState(() {
      unkownResults = false;
      firstSearch = true;
    });
    String rootUrl = actionUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.statusCode);
      printIfDebug(response.statusCode == 200);
      if (response.statusCode == 200) {
        var fetchedData = json.decode(response.body);
        printIfDebug(fetchedData);
        printIfDebug("Not yet In - resultsHaveBeenCleared");
        printIfDebug(resultsHaveBeenCleared);
        printIfDebug("Yes we In");
        setState(() {
          movieDetail = fetchedData;
          fetchedMovieDetail = true;
        });
        if (movieDetail["movies"].length == 0) {
          setState(() {
            unkownResults = true;
          });
        }
        toogleLoadingSymbolStatusElasticSearch(false);
        printIfDebug("New Data Fetched by Elastic Search");
        changeSearchResultsStatus(false);
        printIfDebug(firstSearch);
        setState(() {
          pageNo = 1;
        });
        printIfDebug("Fresh Batch of reviews");
        printIfDebug(pageNo);
        movetoSourceCard();
        if (!firstSearch) {
          printIfDebug("Go to Top");
          movetoSourceCard();
        } else {
          setState(() {
            firstSearch = false;
            resultsHaveBeenCleared = false;
          });
          printIfDebug("Reset resultsHaveBeenCleared");
          printIfDebug(resultsHaveBeenCleared);
        }
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  // When and How to Trigger Request to Elastic Search For Search Results
  Future delayServerRequest() async {
    if (searchString.length >= 2) {
      setState(() {
        pageNo = 1;
      });
      toogleLoadingSymbolStatusElasticSearch(true);
      String checkString = searchString;
      printIfDebug("Before Timeout");
      printIfDebug("Search (Current) String");
      printIfDebug(searchString);
      printIfDebug("Check (Previous) String");
      printIfDebug(checkString);
      new Timer(const Duration(seconds: 1), () {
        printIfDebug("After Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        if (searchString != checkString) {
          printIfDebug("Dont Make Server Request");
        } else {
          printIfDebug("Make Server Request : Elastic Search");
          String appliedMoviesFilterUrl = constructUrlWithFilters();
          printIfDebug(appliedMoviesFilterUrl);
          sendElasticGetRequest(appliedMoviesFilterUrl);
        }
      });
    }
  }

  Widget displayMovieDetails() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        new MovieSearchResults(
          movieDetail: movieDetail,
          userDetail: userDetail,
          userQueue: userQueue,
          authenticationToken: authenticationToken,
          endOfSearchResults: endOfSearchResults,
          changePageNo: changePageNo,
          searchResultsController: searchResultsController,
          dataKey: dataKey,
          showOriginalPosters: widget.showOriginalPosters,
        ),
      ],
    );
  }

  Widget closeOption() {
    if (filterMenuStatus) {
      return new Positioned(
        top: 25.0,
        right: 20.0,
        child: new GestureDetector(
          child: new Container(
            child: new Container(
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: new Image.asset(
                "assests/close_circle.png",
                width: 25.0,
                height: 25.0,
              ),
            ),
          ),
          onTap: () {
            setFilterMenu();
          },
        ),
      );
    } else {
      return new Container();
    }
  }

  Widget displayLoader() {
    return new Center(
      child: new CircularProgressIndicator(
          backgroundColor: Constants.appColorLogo),
    );
  }

  Widget buildOptions(BuildContext context, int index) {
    printIfDebug("GGGG");
    printIfDebug(index);
    return new Center(
      child: new Container(
        width: 100.0,
        height: 70.0,
        child: new Text(options[index]["name"]),
      ),
    );
  }

  constructUrlWithFilters() {
    filtersSet = "";
    String url =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&" +
            searchString;
    for (int category = 0;
        category < searchFilterData["topics"].length;
        category++) {
      for (int option = 0;
          option < searchFilterData["topics"][category]["options"].length;
          option++) {
        if (searchFilterData["topics"][category]["options"][option]["status"]) {
          String shortendedkey = searchFilterData["topics"][category]["key"];
          filtersSet = "&" +
              shortendedkey +
              "=" +
              searchFilterData["topics"][category]["options"][option]
                  ["filter_id"];
          url += shortendedkey +
              "=" +
              searchFilterData["topics"][category]["options"][option]
                  ["filter_id"] +
              "&";
        }
      }
    }
    return url;
  }

  filterSearchResults() {
    String rootUrl = constructUrlWithFilters();
    printIfDebug("New Search with Filter Applied");
    printIfDebug(rootUrl);
    sendElasticGetRequest(rootUrl);
    setFilterMenu();
  }

  setToTitle(value) {
    setState(() {
      howFresh = value;
    });
  }

  setToDisplayOnTitle(topic, index) {
    if (topic["key"] == "movie_tags") {
      setState(() {
        howFresh = topic["options"][index]["name"];
      });
    }
  }

  Widget optionList() {
    printIfDebug("List to be rendered");
    printIfDebug(searchFilterData["topics"][chosenIndex]["options"].length);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return new Positioned(
      child: new Center(
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new SizedBox.fromSize(
            size: new Size(screenWidth * 0.7, screenHeight * 0.6),
            child: new ListView.builder(
                itemCount:
                    searchFilterData["topics"][chosenIndex]["options"].length,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(2.0),
                itemBuilder: (BuildContext context, int index) {
                  return new GestureDetector(
                    onTap: () {
                      printIfDebug("Clicked");
                      printIfDebug(searchFilterData["topics"][chosenIndex]
                          ["options"][index]["name"]);
                      setToDisplayOnTitle(
                          searchFilterData["topics"][chosenIndex], index);
                      toogleOptionStatus(chosenIndex, index);
                      filterSearchResults();
                    },
                    child: new Container(
                      padding: const EdgeInsets.all(10.0),
                      child: new Text(
                        searchFilterData["topics"][chosenIndex]["options"]
                            [index]["name"],
                        style: new TextStyle(
                            fontWeight: FontWeight.normal,
                            color: searchFilterData["topics"][chosenIndex]
                                    ["options"][index]["status"]
                                ? Constants.appColorLogo
                                : Constants.appColorL3,
                            fontSize: 22.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                }),
          ),
        ),
      ),
    );
  }

  Widget blurSearchPage() {
    if (filterMenuStatus) {
      double overlayOpacity = filterMenuStatus ? 0.6 : 0.0;
      double sigma = filterMenuStatus ? 10.0 : 0.0;
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      return new Stack(
        children: <Widget>[
          new Container(
            width: screenWidth,
            height: screenHeight,
            color: Constants.appColorFont.withOpacity(overlayOpacity),
            child: new ClipRect(
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
                child: new Container(
                  decoration: new BoxDecoration(
                      color:
                          Constants.appColorFont.withOpacity(overlayOpacity)),
                ),
              ),
            ),
          ),
          optionList(),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget displaySearchFilterMenu() {
    if (searchFilterDataReady) {
      return new MovieSearchFilter(
        updateFilterMenu: updateFilterMenu,
        searchFilterData: searchFilterData,
        setToDisplayOnTitle: setToDisplayOnTitle,
        toogleOptionStatus: toogleOptionStatus,
        filterSearchResults: filterSearchResults,
      );
    } else {
      return Container();
    }
  }

  Widget renderDefaultMessage(String defaultMessage, FontWeight fontWeight) {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: width * 0.75,
      child: new Center(
        child: new Text(
          defaultMessage,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: Constants.appColorFont,
            fontSize: 15.0,
          ),
          textAlign: TextAlign.center,
          maxLines: 4,
        ),
      ),
    );
  }

  Widget renderDefaultMessagewithSpan(
      String defaultMessage, String defaultSpanMessage) {
    return new Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultMessage(defaultMessage, FontWeight.normal),
            renderDefaultMessage(defaultSpanMessage, FontWeight.bold),
          ],
        ),
      ),
    );
  }

  Widget renderDefaultImage() {
    return new Center(
      child: new Container(
        width: 100.0,
        height: 100.0,
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "sadsmile"),
      ),
    );
  }

  Widget insertDivider() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(
        top: 15,
      ),
      height: 1,
      width: width - 10,
      color: Constants.appColorDivider,
    );
  }

  Widget defaultSearchResults(String defaultMessage) {
    return new Stack(
      children: <Widget>[
        new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultImage(),
            renderDefaultMessage(defaultMessage, FontWeight.normal),
          ],
        ),
        // blurSearchPage(),
        // displaySearchFilterMenu(),
        // closeOption(),
      ],
    );
  }

  Widget beforeSearchingMessage(
      String defaultMessage, String defaultSpanMessage) {
    return Container(
      color: Constants.appColorL1,
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultImage(),
            renderDefaultMessagewithSpan(defaultMessage, defaultSpanMessage),
          ],
        ),
      ),
    );
  }

  displayLoaderDuringFetchMore() {
    if (loadingSymbolStatusElasticSearch) {
      return new Positioned.fill(
        child: new Container(
          color: Constants.appColorL2,
          child: new Center(
            child: ShimmerCards(),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  displayLoaderForInitialFetch() {
    return Container(
      color: Constants.appColorFont,
      child: new Center(
        child: ShimmerCards(),
      ),
    );
  }

  checkIfFirstSearchRequestTriggered() {
    printIfDebug("We In");
    printIfDebug(pageNo);
    printIfDebug(fetchedMovieDetail);
    printIfDebug(loadingSymbolStatusElasticSearch);
    return displayLoaderForInitialFetch();
  }

  checkIfUnkownResults() {
    String defaultMessage = "";
    String defaultSpanMessage = "";
    printIfDebug("Due to Unkown Results");
    printIfDebug(unkownResults);
    if (unkownResults) {
      String defaultMessage = "Oop's \n\n No Movies / Tv Shows Found";
      return defaultSearchResults(defaultMessage);
    } else {
      defaultMessage = "Discover your Favourite Movies / Tv Shows";
      defaultSpanMessage = "Search Now";
      return beforeSearchingMessage(defaultMessage, defaultSpanMessage);
    }
  }

  Widget checkDataReady() {
    try {
      if ((fetchedMovieDetail && fetchedUserPreference) &&
          fetchedUserQueuePreference) {
        if (movieDetail["movies"].length > 0) {
          return new Stack(
            children: <Widget>[
              new SingleChildScrollView(
                child: new ConstrainedBox(
                  constraints: new BoxConstraints(),
                  child: displayMovieDetails(),
                ),
              ),
              displayLoaderDuringFetchMore(),
              // blurSearchPage(),
              // displaySearchFilterMenu(),
              // closeOption(),
            ],
          );
        } else {
          return checkIfUnkownResults();
        }
      } else {
        printIfDebug("First Search");
        return checkIfFirstSearchRequestTriggered();
      }
    } catch (exception) {
      return checkIfFirstSearchRequestTriggered();
    }
  }

  AppBar appHeaderBar() {
    return new AppBar(
      elevation: 4.0,
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () => widget.moveToPreviousTab(),
        ),
      ),
      title: Row(
        children: <Widget>[
          new Text(
            "Fresh : " + howFresh,
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: Constants.appColorFont,
            ),
            textAlign: TextAlign.center,
            maxLines: 1,
          ),
          Spacer(),
          GestureDetector(
            child: Container(
              width: 20,
              height: 20,
              child: Image.asset("assests/filter.png"),
            ),
            onTap: () {
              showDialog<void>(
                context: context,
                barrierDismissible: false, // user must tap button!
                builder: (BuildContext context) {
                  return AlertDialog(
                    backgroundColor: Constants.appColorL2,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                    ),
                    title: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Filters',
                              style: TextStyle(
                                color: Constants.appColorFont,
                              ),
                            ),
                            Spacer(),
                            GestureDetector(
                              child: Text(
                                'CANCEL',
                                style: TextStyle(
                                  color: Constants.appColorLogo,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                left: 15,
                              ),
                              child: GestureDetector(
                                child: Text(
                                  'APPLY',
                                  style: TextStyle(
                                    color: Constants.appColorLogo,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ),
                          ],
                        ),
                        insertDivider(),
                      ],
                    ),
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: <Widget>[
                          displaySearchFilterMenu(),

                          // Text('You will never be satisfied.'),
                          // Text('You\’re like me. I’m never satisfied.'),
                        ],
                      ),
                    ),
                    // actions: <Widget>[],
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      appBar: appHeaderBar(),
      body: checkDataReady(),
    );
  }
}
