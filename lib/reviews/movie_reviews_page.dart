import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/navigation/movie_navigator.dart';
// import  'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_card.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_stats.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_highlight_words.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_reviews.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieReviewsPage extends StatefulWidget {
  MovieReviewsPage({
    Key? key,
    this.id,
    this.authenticationToken,
    this.urlPhase,
    this.deepLink,
  }) : super(key: key);

  final id;
  final authenticationToken;
  final urlPhase;
  final deepLink;

  @override
  _MovieReviewsPageState createState() => new _MovieReviewsPageState();
}

class _MovieReviewsPageState extends State<MovieReviewsPage> {
  var movieReviews;
  var reviewPerPage = [];
  bool fetchedReviews = false;
  double screenWidth = 0.0;
  double screenHeight = 0.0;
  int chosenPage = 0;
  var totalNumberOfPages = 0;

  @override
  void initState() {
    super.initState();
    printIfDebug("Received Information to obtain all reviews");
    printIfDebug(widget.authenticationToken);
    printIfDebug(widget.id);
    getMovieDetails();
    logUserScreen('Flixjini Reviews Page', 'MovieReviewsPage');
  }

  setTotalNumberOfPage() {
    totalNumberOfPages = (movieReviews["reviews"].length / 20).ceil();
  }

  setChosenPage(value) {
    setState(() {
      chosenPage = value;
    });
  }

  keepOnlyRenderedReviews() {
    int start = chosenPage * 20;
    int stop = start + 20;
    int numberOfReview = movieReviews["reviews"].length;
    if (stop > numberOfReview) {
      stop = numberOfReview;
    }
    var temp = [];
    for (int i = start; i < stop; i++) {
      temp.add(movieReviews["reviews"][i]);
    }
    setState(() {
      reviewPerPage = temp;
    });
  }

  constructUrl() {
    String urlName = "";
    String refinedUrlPhase = "";
    refinedUrlPhase = (widget.urlPhase).replaceAll(".json", '/reviews.json');
    if (widget.authenticationToken.isEmpty) {
      urlName = "https://api.flixjini.com" + refinedUrlPhase + '?';
    } else {
      urlName = "https://api.flixjini.com" +
          refinedUrlPhase +
          "?" +
          "jwt_token=" +
          widget.authenticationToken +
          "&";
    }
    return urlName;
  }

  getMovieDetails() async {
    bool requestFlag = false;
    var data;
    String url = "";
    if (widget.urlPhase.toString().contains("reviews.json")) {
      url = widget.urlPhase;
    } else {
      url = constructUrl();
    }
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    printIfDebug("The Url Response");
    printIfDebug(url);

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.body);
      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        data = json.decode(responseBody);
        requestFlag = true;
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (requestFlag) {
      setState(() {
        movieReviews = data;
        fetchedReviews = true;
      });
      setTotalNumberOfPage();
      keepOnlyRenderedReviews();
    }
    if (!mounted) return;
    printIfDebug("All Reviews - Reviews Page");
    printIfDebug(movieReviews);
  }

  Widget displayLoader() {
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      appBar: appHeaderBar(),
      body: new Center(
        child: ShimmerReviews(),
      ),
    );
  }

  Widget renderReviewCards(BuildContext context, int index) {
    screenWidth = MediaQuery.of(context).size.width;
    double fontSize = 13.0;
    double height = 150.0;
    double width = screenWidth * 0.80;
    double paddingLeft = 20.0;
    return new Center(
      child: new Container(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: MovieDetailReviewCard(
          review: reviewPerPage[index],
          fontSize: fontSize,
          height: height,
          width: width,
          paddingLeft: paddingLeft,
          less: false,
        ),
      ),
    );
  }

  Widget displayReviewsList() {
    screenHeight = MediaQuery.of(context).size.height;
    return new ListView.builder(
      itemCount: reviewPerPage.length,
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.all(10.0),
      itemBuilder: renderReviewCards,
      shrinkWrap: true,
      addAutomaticKeepAlives: false,
      physics: NeverScrollableScrollPhysics(),
    );
  }

  AppBar appHeaderBar() {
    return new AppBar(
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () {
            if (widget.deepLink != null && widget.deepLink) {
              Navigator.of(context).pushAndRemoveUntil(
                new MaterialPageRoute(
                  builder: (
                    BuildContext context,
                  ) =>
                      MovieRouting(defaultTab: 2),
                ),
                (Route<dynamic> route) => false,
              );
            } else {
              Navigator.pop(context);
            }
          },
        ),
      ),
      title: new Text("Movie Reviews",
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
              fontWeight: FontWeight.normal, color: Constants.appColorFont),
          textAlign: TextAlign.center,
          maxLines: 1),
    );
  }

  pageStatusUI() {
    if (totalNumberOfPages == 1) {
      return new Expanded(
        child: new Container(),
        flex: 1,
      );
    } else {
      String currentPage = (chosenPage + 1).toString();
      String total = totalNumberOfPages.toString();
      return new Expanded(
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Text(
              currentPage + " / " + total,
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
                color: Constants.appColorFont,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          ),
        ),
        flex: 1,
      );
    }
  }

  pageMoveForward() {
    Widget button;
    if ((chosenPage + 1) >= totalNumberOfPages) {
      button = new Container();
    } else {
      button = new Container(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: new RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: new Text(
              "Next",
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
                color: Constants.appColorFont,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            color: Constants.appColorL2,
            onPressed: () {
              setChosenPage(chosenPage + 1);
              keepOnlyRenderedReviews();
            }),
      );
    }
    return new Expanded(
      child: button,
      flex: 1,
    );
  }

  pageMoveBackward() {
    Widget button;
    if (chosenPage <= 0) {
      button = new Container();
    } else {
      button = new Container(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: new RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: new Text(
              "Previous",
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
                color: Constants.appColorFont,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            color: Constants.appColorL2,
            onPressed: () {
              setChosenPage(chosenPage - 1);
              keepOnlyRenderedReviews();
            }),
      );
    }
    return new Expanded(
      child: button,
      flex: 1,
    );
  }

  Widget displayReviewsNavigation() {
    return new Container(
      padding: const EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        top: 10,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          pageMoveBackward(),
          pageStatusUI(),
          pageMoveForward(),
        ],
      ),
    );
  }

  bool nullChecker(String key) {
    if (movieReviews.containsKey(key)) {
      if (movieReviews[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  renderSections() {
    var sections = <Widget>[];
    bool statsFlag = nullChecker("stats");
    bool sammaWordFlag = nullChecker("sammaWords");
    printIfDebug(sammaWordFlag);
    bool reviewsFlag = nullChecker("reviews");
    if (sammaWordFlag) {
      sections.add(new MovieDetailReviewHighlightWords(
          reviewHighlightWords: movieReviews["sammaWords"],
          reviewHighlightWordsImage: movieReviews["sammaWordsImage"]));
    }
    if (statsFlag) {
      sections
          .add(new MovieDetailReviewStats(reviewStats: movieReviews["stats"]));
    }
    sections.add(displayReviewsNavigation());
    if (reviewsFlag) {
      sections.add(displayReviewsList());
    }
    return sections;
  }

  Widget constructFloatingFilterButton() {
    return new Container(
      // child: new FittedBox(
      child: new FloatingActionButton(
        child: Container(
          child: Image.asset(
            "assests/genie_inactive.png",
            height: 50,
            width: 50,
          ),
        ),
        onPressed: () {},
        backgroundColor: Colors.transparent,
      ),
      // ),
    );
  }

  Widget displayReviews() {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: Align(
      //   child: constructFloatingFilterButton(),
      //   alignment: Alignment(0, 1.2),
      // ),
      appBar: appHeaderBar(),
      body: new Container(
        width: screenWidth,
        padding: const EdgeInsets.only(bottom: 0.0),
        color: Constants.appColorL1,
        child: new SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: renderSections(),
          ),
        ),
      ),
      bottomNavigationBar: MovieNavigator(),
    );
  }

  Widget checkDataReady() {
    try {
      if (fetchedReviews) {
        return displayReviews();
      } else {
        return displayLoader();
      }
    } catch (exception) {
      return displayLoader();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return checkDataReady();
  }
}
