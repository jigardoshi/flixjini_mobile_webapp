import 'dart:async';

import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/utils/google_advertising_id.dart'; // platform dependent code
// import 'package:device_apps/device_apps.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
// import 'package:device_info/device_info.dart';
import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
// import 'access_shared_pref.dart';
import 'dart:ui';
// import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'dart:ui' as ui;
import 'dart:math';

Future<void> launchURL(String url) async {
  try {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  } catch (e) {
    printIfDebug('\n\nexception in launch url method, and e: $e');
  }
}

getAppurl() {
  return "https://api.flixjini.com";
}

String getMovieName(String uriPath) {
  int lIndexOfSlash = uriPath.lastIndexOf('/');
  printIfDebug('\n\nindex of last slash is: $lIndexOfSlash');
  String s = '';
  for (int i = lIndexOfSlash + 1; i < uriPath.length; i++) {
    s += uriPath[i];
  }
  printIfDebug('\n\nextracted string from the uri path is: $s');
  return s;
}

void showToast(String msg, String length, String gravity, int timeInSecForIos,
    Color toastColor) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: length == 'short' ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG,
    gravity: getToastGravity(gravity),
    // timeInSecForIos: timeInSecForIos,
    backgroundColor: toastColor, // Constants.appColorError, // red color
    textColor: Constants.appColorFont,
    fontSize: 16.0,
  );
}

ToastGravity getToastGravity(String gravity) {
  switch (gravity) {
    case 'top':
      return ToastGravity.TOP;
      break;
    case 'center':
      return ToastGravity.CENTER;
      break;
    case 'bottom':
      return ToastGravity.BOTTOM;
      break;
    default:
      return ToastGravity.BOTTOM;
      break;
  }
}

void copyToClipBoard(String whatIsBeingCopied, String stringToCopy) {
  Clipboard.setData(new ClipboardData(text: stringToCopy));
  showToast('Copied "$whatIsBeingCopied" to Clipboard', 'long', 'bottom', 2,
      Constants.appColorError);
}

void makeHTTPGETRequest(
  String rDetails,
  String apiEndpoint,
  final String methodUsed,
  final String googleAdId,
) async {
  String versionName = '', versionCode = '';
  if (true) {
    versionName = getVersionNameForIOS();
    versionCode = getVersionCodeForIOS();
  } else {
    versionName = getVersionNameForAndroid();
    versionCode = getVersionCodeForAndroid();
  }
  const FLIXJINI = "Flixjini";
  apiEndpoint +=
      "app_name=$FLIXJINI&method_used=$methodUsed&aff_sub=$rDetails&gaid=$googleAdId&version_name=$versionName&version_code=$versionCode";

  apiEndpoint += await getDeviceDetails();

  try {
    http.get(
      Uri.parse(apiEndpoint),
      headers: {"Accept": "application/json"},
    ).then((response) async {
      var resBody = json.decode(response.body);
      printIfDebug('\n\ndecoded response: $resBody');
    });
  } catch (exception) {
    printIfDebug('\n\nexception caught in making http get request');
  }
}

void sendEventToOurFlyer(
  String apiEndpoint,
  String eventName,
  String googleAdId,
) async {
  String versionName = '', versionCode = '';
  if (true) {
    versionName = getVersionNameForIOS();
    versionCode = getVersionCodeForIOS();
  } else {
    versionName = getVersionNameForAndroid();
    versionCode = getVersionCodeForAndroid();
  }

  const FLIXJINI = "Flixjini";
  if (true) {
    googleAdId = await getGoogleAdId(); // platform dependent code

  } else if (true) {
    googleAdId = await getIDFA();
  }
  apiEndpoint +=
      "app_name=$FLIXJINI&eventname=$eventName&gaid=$googleAdId&version_name=$versionName&version_code=$versionCode";

  apiEndpoint += await getDeviceDetails();

  try {
    http.get(
      Uri.parse(apiEndpoint),
      headers: {"Accept": "application/json"},
    ).then((response) async {
      var resBody =
          !apiEndpoint.contains('retail9') ? json.decode(response.body) : '';
      printIfDebug('\n\ndecoded response: $resBody');
    });
  } catch (exception) {
    printIfDebug('\n\nexception caught in making http get request');
  }
}

Widget getIconContainer(String imgLoc) {
  return Container(
    height: 20,
    width: 20,
    child: Image.asset(
      imgLoc,
      color: Constants.appColorFont,
    ),
  );
}

Future<String> getDeviceDetails() async {
  String deviceDetails = '';

  if (true) {
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

    // String androidId = androidInfo.androidId;
    // String brand = androidInfo.brand;
    // String model = androidInfo.model;

    // AndroidBuildVersion version = androidInfo.version;
    // String versionRelease = version.release;
    // int versionSDKInt = version.sdkInt;

    // deviceDetails +=
    //     '&device_android_id=$androidId&device_brand=$brand&device_model=$model&device_version_release=$versionRelease&device_version_sdk_int=$versionSDKInt';
    // printIfDebug('\n\ndevice details: $deviceDetails');
  } else if (true) {
    // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    // IosDeviceInfo iOSDeviceInfo = await deviceInfo.iosInfo;

    // String idfa = iOSDeviceInfo.identifierForVendor;
    // String model = iOSDeviceInfo.model;
    // String machine = iOSDeviceInfo.utsname.machine;
    // String release = iOSDeviceInfo.utsname.release;
    // String systemVersion = iOSDeviceInfo.systemVersion;

    // deviceDetails +=
    //     '&device_android_id=$idfa&device_brand=$model&device_model=$machine&device_version_release=$release&device_version_sdk_int=$systemVersion';

    // printIfDebug('\n\ndevice details: $deviceDetails');
  } else {
    deviceDetails = '';
  }
  return deviceDetails;
}

Future<void> shareViaWhatsApp(String imageURL, String content) async {
  if (imageURL == null || imageURL == '') {
    imageURL =
        'https://movieassetsnew.komparify.com/original/9e07b90cdca0e71d8ab6d23c997cba99024018ad';
  }
  if (content == null || content == '') {
    content =
        "Petta\n\nMore details here at Flixjini: https://in.flixjini.com/movie/petta?search_keyword=Petta \n\nCheck out Flixjini App at: https://in.flixjini.com";
  }
  Map<String, String> shareMessageDetails = {
    "imageURL": imageURL,
    "content": content,
  };
  const platform = const MethodChannel('api.komparify/advertisingid');
  String returnedString = "";
  // await platform.invokeMethod('shareViaWhatsApp', shareMessageDetails);
  printIfDebug(
      '\n\nreturned string after invoking the method shareViaWhatsApp is: $returnedString');
}

Future<void> shareTextToWhatsapp(String content) async {
  Map<String, String> shareMessageDetails = {
    "content": content,
  };
  const platform = const MethodChannel('api.komparify/advertisingid');
  String returnedString = "";
  // await platform.invokeMethod('shareTextToWhatsapp', shareMessageDetails);
  printIfDebug(
      '\n\nreturned string after invoking the method shareTextToWhatsapp is: $returnedString');
}

Future<String> getIDFA() async {
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  // IosDeviceInfo iOSDeviceInfo = await deviceInfo.iosInfo;
  // String idfa = iOSDeviceInfo.identifierForVendor;
  return ""; //idfa;
}

Future<dynamic> showCupertinoDialog(
    BuildContext context, String dialogTitle, String dialogContent) {
  return showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return CupertinoAlertDialog(
        title: new Text(dialogTitle),
        content: new Text(dialogContent),
        actions: <Widget>[
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text(
              "Okay",
              style: TextStyle(
                color: Constants.appColorLogo,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget getContainerForVersion() {
  String versionDetail = '';
  if (true) {
    versionDetail = 'Version ' +
        getVersionNameForAndroid() +
        ' (' +
        getVersionCodeForAndroid() +
        ')';
  } else {
    versionDetail = 'Version ' +
        getVersionNameForIOS() +
        ' (' +
        getVersionCodeForIOS() +
        ')';
  }
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.info,
              color: Constants.appColorDivider,
            ),
            Container(
              margin: EdgeInsets.only(
                left: 2,
              ),
              child: Text(
                versionDetail,
                style: TextStyle(
                  color: Constants.appColorDivider,
                ),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(
            top: 10,
          ),
          color: Constants.appColorL1,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: 5,
                  ),
                  child: Image.asset("assests/indianflag.png"),
                ),
                Text(
                  "Made In India",
                  style: TextStyle(
                    color: Constants.appColorDivider,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

Future<dynamic> showAlertDialog(
    BuildContext context, String dialogTitle, String dialogContent) {
  return showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(dialogTitle),
        content: Text(dialogContent),
        actions: <Widget>[
          FlatButton(
            textColor: Constants.appColorL1,
            child: const Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<bool> checkIfAndroidAppIsInstalled(String pName) async {
  bool pInstalled = false;
  if (true) {
    try {
      // Application? app = await DeviceApps.getApp(pName);
      // printIfDebug('\n\npackage details: ' + app.toString());
      // if (app != null) {
      //   pInstalled = true;
      // } else {
      //   pInstalled = false;
      // }
    } catch (e) {
      printIfDebug('\n\nexception caught in checking the package details');
      pInstalled = false;
    }
  }
  // else {
  // pInstalled = false;
  // }
  printIfDebug('\n\nreturn bool value for the pinstalled: $pInstalled');
  return pInstalled;
}

printIfDebug(var printStatement) {
  //change before every release
  if (Constants.debug) {
    print(printStatement.toString());
  }
}

void openPlaystore(String pName) {
  launchURL("https://play.google.com/store/apps/details?id=" + pName);
}

Widget getProgressBar() {
  return Center(
    child: const CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
        const Color(
          0xFF43C681,
        ),
      ),
    ),
  );
}

Widget getProgressBarWithLoading() {
  return Container(
    color: Constants.appColorL1,
    height: 50,
    width: double.infinity,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        getProgressBar(),
        Padding(
          padding: EdgeInsets.all(
            10,
          ),
        ),
        Text(
          'Loading...',
          style: TextStyle(
            color: Constants.appColorFont,
          ),
        ),
      ],
    ),
  );
}

Widget getBlurredImageContainer() {
  return Container(
    height: 150,
    width: 150,
    decoration: new BoxDecoration(
      image: new DecorationImage(
        image: ExactAssetImage(
          'assests/movie_details_page_test_image.png',
        ),
        fit: BoxFit.fill,
      ),
    ),
  );
}

Widget getIndexPageBlurredImageContainer(
  String movieName, [
  int? index,
  bool? bottomRadiusNeeded,
]) {
  return Container(
    decoration: new BoxDecoration(
      // borderRadius: getBorderRadius(
      //   bottomRadiusNeeded != null ? bottomRadiusNeeded : false,
      // ),
      image: new DecorationImage(
        image: ExactAssetImage(
          getRandomImageFilePath(index!),
          // 'assests/movie_details_page_test_image.png',
        ),
        fit: BoxFit.fill,
      ),
    ),
  );
}

bool checkNetworkImageDimentions(imageUrl) {
  Image image = new Image.network(imageUrl);
  bool isVertical = true;
  Completer<ui.Image> completer = new Completer<ui.Image>();
  image.image.resolve(ImageConfiguration()).addListener(
    ImageStreamListener(
      (ImageInfo info, bool synchronousCall) {
        completer.complete(info.image);
        // printIfDebug(imageUrl);
        // printIfDebug(info.image.height);
        // printIfDebug(info.image.width);
        // printIfDebug("-----------");
        isVertical = info.image.height > info.image.width;
      },
    ),
  );
  return isVertical;
}

String getRandomImageFilePath(
  int index,
) {
  return 'assests/blurred_images/' + getImageFileNumber(index) + '.png';
}

BorderRadius getBorderRadius(
  bool bottomRadiusNeeded,
) {
  if (bottomRadiusNeeded) {
    return BorderRadius.only(
      topLeft: const Radius.circular(
        8.0,
      ),
      topRight: const Radius.circular(
        8.0,
      ),
      bottomRight: const Radius.circular(
        8.0,
      ),
      bottomLeft: const Radius.circular(
        8.0,
      ),
    );
  } else {
    return BorderRadius.only(
      topLeft: const Radius.circular(
        8.0,
      ),
      topRight: const Radius.circular(
        8.0,
      ),
    );
  }
}

String getImageFileNumber(
  int index,
) {
  index = Random().nextInt(10);
  if (index == 0) {
    index++;
  }
  return index.toString();
}

testWidget(BuildContext context) {
  var screenWidth = MediaQuery.of(context).size.width;
  var screenHeight = MediaQuery.of(context).size.height;
  double overlayOpacity = 0.4;
  var headinOverlay = new Positioned(
    child: new Center(
      child: new Container(
        width: screenWidth * 0.80,
        height: screenHeight * 0.20,
        child: new ClipRect(
          child: new BackdropFilter(
            filter: new ImageFilter.blur(
              sigmaX: 4.0,
              sigmaY: 4.0,
            ),
            child: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Constants.appColorFont,
                  width: 1,
                ),
                color: Constants.appColorL1.withOpacity(
                  overlayOpacity,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  return headinOverlay;
}

Positioned getOverlayBlur(BuildContext context) {
  var screenWidth = MediaQuery.of(context).size.width;
  var screenHeight = MediaQuery.of(context).size.height;
  double overlayOpacity = 0.4;
  return Positioned(
    child: new Center(
      child: new Container(
        width: screenWidth * 0.80,
        height: screenHeight * 0.20,
        child: new ClipRect(
          child: new BackdropFilter(
            filter: new ImageFilter.blur(
              sigmaX: 4.0,
              sigmaY: 4.0,
            ),
            child: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Constants.appColorFont,
                  width: 1,
                ),
                color: Constants.appColorL1.withOpacity(
                  overlayOpacity,
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}

Positioned getOverlayHeading(
  String title,
  BuildContext context,
) {
  var screenWidth = MediaQuery.of(context).size.width;
  return Positioned(
    child: Container(
      child: new Center(
        child: new Container(
          width: screenWidth * 0.70,
          padding: const EdgeInsets.all(15.0),
          child: new Text(
            title.toUpperCase(),
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(
              fontWeight: FontWeight.bold,
              color: Constants.appColorFont.withOpacity(0.9),
              fontSize: 30.0,
              letterSpacing: 3.0,
            ),
            textAlign: TextAlign.center,
            maxLines: 3,
          ),
        ),
      ),
    ),
  );
}

Future<String> getStringFromFRC(
  String key,
) async {
  String value = 'true';
  try {
    // final RemoteConfig remoteConfig = await RemoteConfig.instance;
    // await remoteConfig.fetch();
    // fetch();
    // await remoteConfig.fetchAndActivate(); //activateFetched();
    // value = remoteConfig.getString(
    //   key,
    // );
    if (value == null) {
      value = 'true';
    }
    return value;
  } catch (e) {
    return "true";
  }
}

bool checkIfMoviePosterPresent(
  String moviePosterUrl,
) {
  if (moviePosterUrl.isNotEmpty &&
      !moviePosterUrl.contains(
          "assets/movies/pdef-c8c66cd883a125253c23d3eca047865e.png")) {
    return true;
  }

  return false;
}

String getVersionCodeForAndroid() {
  // change before every release

  return Constants.versionCodeAndroid;
}

String getVersionNameForAndroid() {
  return Constants.versionNameAndroid;
}

String getVersionCodeForIOS() {
  return '19';
}

String getVersionNameForIOS() {
  return '5.0.14';
}

Widget getContainerForTMDBLogo() {
  return Container(
    height: 100,
    width: 100,
    child: Image.asset(
      'assests/tmdb_logo.png',
    ),
  );
}
