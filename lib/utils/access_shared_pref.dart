import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

void setStringToSP(String key, String value) async {
  SharedPreferences sp = await SharedPreferences.getInstance();
  sp.setString(key, value);
  printIfDebug('\n\nstored in sp\nkey: $key\nvalue: $value');
}

Future<String> getStringFromSP(String key) async {
  SharedPreferences sp = await SharedPreferences.getInstance();
  String emptyString = '', value = sp.getString(key)!;
  if (value == null) {
    printIfDebug('\n\nno value found for the key $key');
    return emptyString;
  } else {
    return value;
  }
}

Future<String> getGAIDFromSP() async {
  SharedPreferences sp = await SharedPreferences.getInstance();
  String deviceInfoString = sp.getString('stringDeviceInformation')!;
  Map deviceInfoMap = json.decode(deviceInfoString);
  String spGAID = deviceInfoMap['ga_id'];
  printIfDebug(
      '\n\nfrom getGAIDFromSP method, in access_shared_pref.dart file, value of gaid: $spGAID');
  return spGAID;
}
