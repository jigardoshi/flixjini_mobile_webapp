// import 'package:advertising_id/advertising_id.dart'; // platform dependent code
import 'package:flixjini_webapp/utils/common_utils.dart';

import 'access_shared_pref.dart';
import 'dart:io' show Platform;

String? googleAdId = "";

Future<String> getGoogleAdId() async {
  if (true) {
    // googleAdId = await AdvertisingId.id(); // platform dependent code
    // String oldSPGAID = await getStringFromSP('spGAID');
    // if (oldSPGAID == '' || oldSPGAID == null || oldSPGAID != googleAdId) {
    //   setStringToSP('spGAID', googleAdId!);
    // }
  } else if (true) {
    googleAdId = await getIDFA();
    String oldSPGAID = await getStringFromSP('spGAID');
    if (oldSPGAID == '' || oldSPGAID == null || oldSPGAID != googleAdId) {
      setStringToSP('spGAID', googleAdId!);
    }
  } else {
    googleAdId = '';
  }

  return googleAdId!;
}
