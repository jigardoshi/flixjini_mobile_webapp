// import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flixjini_webapp/common/constants.dart';

class SigninAlertAlert extends StatefulWidget {
  @override
  SigninAlertAlertState createState() => SigninAlertAlertState();
}

class SigninAlertAlertState extends State<SigninAlertAlert> {
  bool cancelFlag = false, signinFlag = false;

  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;

    return AlertDialog(
      backgroundColor: Constants.appColorL2,
      content: Container(
        height: screenwidth / 3,
        child: Column(
          children: <Widget>[
            Text(
              "To perform this action you need to be logged into Flixjini.\nPlease sign up or login to continue.",
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 15,
                height: 1.3,
              ),
              textAlign: TextAlign.center,
            ),
            Spacer(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color:
                          !signinFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                        fontSize: 13,
                        color:
                            signinFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      signinFlag = true;
                    });
                    Future.delayed(const Duration(milliseconds: 500), () {
                      Navigator.of(context).pushReplacement(
                        new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new MovieAuthenticationPage(
                            type: false,
                          ),
                        ),
                      );
                    });
                  },
                ),
                GestureDetector(
                  child: Container(
                    // margin: EdgeInsets.only(
                    //   left: 20,
                    // ),
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color:
                          !cancelFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                        color:
                            cancelFlag ? Constants.appColorL2 : Constants.appColorLogo,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      cancelFlag = true;
                    });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
