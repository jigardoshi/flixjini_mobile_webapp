import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/ensure_visible_when_focussed.dart';
import 'dart:async';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_menu.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterChosenFilters extends StatefulWidget {
  final chosenFilters;
  final filterVisibilities;
  final convertAplliedFilterToUrl;
  final filterData;
  final setTheTheaterKey;

  MovieFilterChosenFilters({
    this.chosenFilters,
    this.filterVisibilities,
    this.convertAplliedFilterToUrl,
    this.filterData,
    this.setTheTheaterKey,
  });

  @override
  _MovieFilterChosenFiltersState createState() =>
      new _MovieFilterChosenFiltersState();
}

class _MovieFilterChosenFiltersState extends State<MovieFilterChosenFilters> {
  String modifiedChosenFiltersString = "";
  bool chosenFilterLoaded = false, hideFlag = true, saveLoading = false;

  List<Widget> listOfWidgets = <Widget>[];
  String userEnteredFilterName = "", authenticationToken = "";
  TextEditingController filterNameController = new TextEditingController();
  FocusNode saveFilterFocusNode = new FocusNode();
  List<Widget> listOfWidgetsForSavedFilters = <Widget>[];

  @override
  void initState() {
    super.initState();
    loadSavedFilters();
  }

  void loadSavedFilters() async {
    printIfDebug('\n\nsaved filters button is tapped');
    String getFiltersAPIEndpoint =
        "https://api.flixjini.com/queue/get_filters.json?";

    String defaultParamsAddedURL =
        await fetchDefaultParams(getFiltersAPIEndpoint);
    printIfDebug('\n\ndefault params added url: $defaultParamsAddedURL');

    defaultParamsAddedURL += "&";

    String headerAddedURL = constructHeader(defaultParamsAddedURL);
    printIfDebug('\n\nheader added url is: $headerAddedURL');

    headerAddedURL += "&magic=true&country_code=IN&jwt_token=";

    String authToken = await getStringFromSP('authenticationToken');
    headerAddedURL += authToken;

    printIfDebug('\n\nurl before making request is: $headerAddedURL');

    callGetFiltersAPIEndpoint(headerAddedURL);
  }

  void callGetFiltersAPIEndpoint(String url) {
    List userSavedFilters = [];
    var resBody;
    try {
      http.get(
         Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        if (resBody["error"] == false) {
          userSavedFilters = resBody["filters"];
          takeUsersSavedFilters(userSavedFilters);
          setState(() {
            //
          });
        } else {
          // error occurred

        }
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  void takeUsersSavedFilters(List userSavedFilters) {
    int userSavedFiltersCount = userSavedFilters.length;
    Map oneFilterJSON = new Map();
    printIfDebug('\n\nno of filters user has saved is: $userSavedFiltersCount');
    for (int i = 0; i < userSavedFiltersCount; i++) {
      oneFilterJSON = userSavedFilters[i]["movie_saved_filter"];

      addOneFilterToList(oneFilterJSON, i);
    }
  }

  void addOneFilterToList(Map oneFilterJSON, int i) {
    String filterName = oneFilterJSON["name"];
    int filterId = oneFilterJSON["id"];
    String filterQueryString = oneFilterJSON["query_string"];

    listOfWidgetsForSavedFilters.add(
      Container(
        height: 50,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 10.0,
                      ),
                      child: Text(
                        filterName,
                        style: TextStyle(
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: GestureDetector(
                      child: Container(
                        padding: const EdgeInsets.all(3.0),
                        width: 100,
                        height: 20.0,
                        decoration: new BoxDecoration(
                          border: new Border.all(
                            color: Constants.appColorLogo,
                            width: 1.0,
                          ),
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child: new Center(
                          child: new Text(
                            "LOAD",
                            overflow: TextOverflow.ellipsis,
                            style: new TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Constants.appColorLogo,
                              fontSize: 10,
                              letterSpacing: 0.6,
                            ),
                            textScaleFactor: 0.75,
                            textAlign: TextAlign.center,
                            maxLines: 3,
                          ),
                        ),
                      ),
                      // Container(

                      onTap: () {
                        Navigator.of(context).pop();

                        takeUserToFilterPage(
                            filterId, filterName, filterQueryString);

                        // getConfirmationBeforeApplyingFilters(
                        //     filterId, filterName, filterQueryString);
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Constants.appColorDivider,
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> getConfirmationBeforeApplyingFilters(
      int filterId, String filterName, String filterQueryString) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Confirm'),
          content:
              Text('Are you sure you want to apply the filter $filterName?'),
          actions: <Widget>[
            FlatButton(
              textColor: Constants.appColorL1,
              child: const Text('Cancel'),
              onPressed: () {
                printIfDebug(
                    '\n\nuser chose no to apply the selected filter $filterName');
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              textColor: Constants.appColorL1,
              child: Text(
                'Apply',
                style: TextStyle(
                  color: Constants.appColorLogo,
                  // fontSize: 13,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                printIfDebug(
                    '\n\nuser chose yes to apply the selected filter $filterName');
                Navigator.of(context).pop();
                takeUserToFilterPage(filterId, filterName, filterQueryString);
              },
            )
          ],
        );
      },
    );
  }

  void takeUserToFilterPage(
      int filterId, String filterName, String filterQueryString) {
    printIfDebug(
        '\n\ntapped filter details: \nfilter name: $filterName, filter id: $filterId, filter query string: $filterQueryString');
    String appliedFilterUrl =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&";
    filterQueryString = filterQueryString.substring(1);
    appliedFilterUrl += filterQueryString;
    Map<String, dynamic> myMap = new Map<String, dynamic>(),
        visibleFiltersMap = new Map();
    Uri uri = Uri.parse(appliedFilterUrl);
    myMap = uri.queryParametersAll;
    printIfDebug(
        '\n\nfinal applied filter url: $appliedFilterUrl, and its type is string');
    printIfDebug('\n\n, query params map: $myMap, and its type is map');

    /*
     * for each key, if the corresponding value has more than one element in the list, 
     * all the elements in the list are together considered as one element.
     * so, to fix this issue, list is encoded to string and then added double proper quotes 
     * to make sure that the elements are discrete and not considered together as one element.
     */
    myMap.forEach((key, value) {
      printIfDebug('\n\nkey $key');
      int valueLength = value.length;
      printIfDebug('\n\nvalue length: $valueLength');
      String tempString = json.encode(value);
      tempString = tempString.replaceAll(',', '", "');
      printIfDebug('\n\ncomma and space added temp string: $tempString');

      List tempList = json.decode(tempString);
      printIfDebug('\n\ntemp list length: ${tempList.length}');
      visibleFiltersMap[key] = tempList;
    });

    printIfDebug('\n\nfinal map before navigating: $visibleFiltersMap');

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new MovieFilterPage(
              appliedFilterUrl: appliedFilterUrl,
              visibleFilters: visibleFiltersMap,
              // add these 2 params
              // visibilityStatus: ,
              inTheaterKey: false,
            )));
  }

  loadChosenFilter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String temp = json.encode(widget.chosenFilters);
    setState(() {
      prefs.setString('modifiedChosenFiltersString', temp);
      modifiedChosenFiltersString =
          (prefs.getString('modifiedChosenFiltersString') ?? "");
    });
  }

  displayTitle() {
    return new GestureDetector(
      child: new Container(
        padding: const EdgeInsets.all(
          15.0,
        ),
        child: Text(
          'Filtered By',
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            color: Constants.appColorFont,
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          ),
          maxLines: 1,
        ),
      ),
    );
  }

  extractTagName(String optionKey, String optionFilterId) {
    String tagName = "";
    if (optionKey == widget.filterData["sort"]["key"]) {
      for (int filterId = 0;
          filterId < widget.filterData["sort"]["options"].length;
          filterId++) {
        if (widget.filterData["sort"]["options"][filterId]["filter_id"] ==
            optionFilterId) {
          tagName = widget.filterData["sort"]["options"][filterId]["name"];
          return tagName;
        }
      }
    }
    for (int group = 0; group < widget.filterData["topics"].length; group++) {
      if (widget.filterData["topics"][group].containsKey("key_1") ||
          widget.filterData["topics"][group].containsKey("key_2")) {
        if (widget.filterData["topics"][group]["key_1"] == optionKey) {
          tagName = "Min" +
              widget.filterData["topics"][group]["heading"] +
              " : " +
              optionFilterId;
          return tagName;
        } else if (widget.filterData["topics"][group]["key_2"] == optionKey) {
          tagName = "Max" +
              widget.filterData["topics"][group]["heading"] +
              " : " +
              optionFilterId;
          return tagName;
        }
      }
      if (widget.filterData["topics"][group].containsKey("key")) {
        if (widget.filterData["topics"][group]["key"] == optionKey) {
          for (int filterId = 0;
              filterId < widget.filterData["topics"][group]["options"].length;
              filterId++) {
            if (widget.filterData["topics"][group]["options"][filterId]
                    ["filter_id"] ==
                optionFilterId) {
              tagName = widget.filterData["topics"][group]["options"][filterId]
                  ["name"];
              return tagName;
            }
          }
        }
      }
    }
    if (tagName == null) {
      tagName = optionFilterId;
      return tagName;
    } else if (tagName.isEmpty) {
      tagName = optionFilterId;
      return tagName;
    } else {
      return null;
    }
  }

  Widget invisibleBreadcrumbTitle(int index, var keyfilters) {
    String tagName = "Tag : " + keyfilters[index]["filter_id"];
    return new Container(
      child: new Center(
        child: new Text(
          tagName,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: Constants.appColorFont,
              fontSize: 12.0),
          textAlign: TextAlign.center,
          maxLines: 2,
        ),
      ),
    );
  }

  Widget breadcrumbTitle(int index, var keyfilters) {
    String tagName = "";
    tagName = extractTagName(
        keyfilters[index]["key"], keyfilters[index]["filter_id"]);
    return new Container(
      child: new Center(
        child: new Text(
          tagName,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: FontWeight.normal,
            color: Constants.appColorFont,
            fontSize: 15,
          ),
          // textScaleFactor: 0.75,
          textAlign: TextAlign.center,
          maxLines: 3,
        ),
      ),
    );
  }

  removeCrumb(int index, var keyfilters) {
    printIfDebug("The Breadcrumbs");
    printIfDebug(widget.chosenFilters);
    printIfDebug("Crumb that has been Tapped on");
    printIfDebug(keyfilters[index]["key"]);
    var modifiedChosenFilters = json.decode(modifiedChosenFiltersString);
    printIfDebug(modifiedChosenFilters);
    if (modifiedChosenFilters.containsKey(keyfilters[index]["key"])) {
      for (int i = 0;
          i < modifiedChosenFilters[keyfilters[index]["key"]].length;
          i++) {
        if (modifiedChosenFilters[keyfilters[index]["key"]][i] ==
            keyfilters[index]["filter_id"]) {
          printIfDebug("Remove");
          printIfDebug(modifiedChosenFilters[keyfilters[index]["key"]]);
          printIfDebug(keyfilters[index]["filter_id"]);
          modifiedChosenFilters[keyfilters[index]["key"]]
              .remove(keyfilters[index]["filter_id"]);
          if (modifiedChosenFilters[keyfilters[index]["key"]].isEmpty) {
            modifiedChosenFilters.remove(keyfilters[index]["key"]);
          }
          break;
        }
      }
    }
    var visiblityKeys = {};
    modifiedChosenFilters.forEach((k, v) {
      visiblityKeys[k] = true;
    });
    printIfDebug("Track all Applied filters");
    printIfDebug(modifiedChosenFilters);
    printIfDebug(visiblityKeys);
    printIfDebug(
        widget.convertAplliedFilterToUrl(modifiedChosenFilters, visiblityKeys));
  }

  Widget breadcrumbDismissIcon(int index, var keyfilters) {
    return new GestureDetector(
      onTap: () {
        removeCrumb(index, keyfilters);
        widget.setTheTheaterKey(false);
      },
      child: new Container(
          height: 20,
          decoration: BoxDecoration(
            color: Constants.appColorL1,
            shape: BoxShape.circle,
          ),
          child: Image.asset(
            "assests/close_grey.png",
            height: 20,
            width: 20,
          )
          //  Icon(
          //   Icons.close,
          //   size: 20,
          //   color: Constants.appColorLogo,
          // ),
          ),
    );
  }

  Widget checkIfTagisVisibleOrNot(int index, var keyfilters) {
    if (keyfilters[index]["status"]) {
      return new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Expanded(
            child: breadcrumbTitle(index, keyfilters),
            // flex: 2,
          ),
          // new Expanded(
          //   child: breadcrumbDismissIcon(index, keyfilters),
          //   flex: 1,
          // ),
        ],
      );
    } else {
      return invisibleBreadcrumbTitle(index, keyfilters);
    }
  }

  Widget constructBreadCrumb(
      BuildContext context, int index, var keyfilters, nos) {
    return index < nos - 1
        ? Stack(
            alignment: AlignmentDirectional.topEnd,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(5.0),
                child: new InkWell(
                  onTap: () {
                    printIfDebug("Crumb has been selected");
                  },
                  child: new Container(
                    padding: const EdgeInsets.all(3.0),
                    // width: 100.0,
                    decoration: new BoxDecoration(
                      color: Constants.appColorL1,
                      border: new Border.all(
                        color: Constants.appColorL3,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: checkIfTagisVisibleOrNot(index, keyfilters),
                  ),
                ),
              ),
              breadcrumbDismissIcon(index, keyfilters),
            ],
          )
        : Container(
            padding: const EdgeInsets.all(5.0),
            child: new InkWell(
              onTap: () {
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    builder: (BuildContext context) => new MovieFilterMenu(
                      filterStatus: widget.chosenFilters,
                      filterVisibilities: widget.filterVisibilities,
                      showOriginalPosters: true,
                      authToken: authenticationToken,
                    ),
                  ),
                );
              },
              child: new Container(
                padding: const EdgeInsets.all(3.0),
                width: 100.0,
                decoration: new BoxDecoration(
                  color: Constants.appColorL1,
                  // border: new Border.all(
                  //   color: Constants.appColorL2,
                  //   width: 3.0,
                  // ),
                  // borderRadius: BorderRadius.circular(10),
                ),
                child: new Container(
                  child: new Center(
                    child: new Text(
                      "+ MORE OPTIONS",
                      overflow: TextOverflow.ellipsis,
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorL3,
                        fontSize: 12.0,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 3,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  checkIfFilterOptionCantBeConstructedByFilterMenu(String key) {
    if (widget.filterVisibilities != null) {
      if (widget.filterVisibilities.containsKey(key)) {
        if (!widget.filterVisibilities[key]) {
          return false;
        }
      }
    }
    return true;
  }

  Widget filterBreadCrumbSection() {
    var keyfilters = [];
    if (widget.chosenFilters == null) {
      printIfDebug("widget.chosenFilters == null");
      return Container();
    } else {
      widget.chosenFilters.forEach((key, filterIds) {
        if (filterIds is List) {
          filterIds.forEach((filterId) {
            var keyfilterMap = {};
            keyfilterMap["key"] = key;
            keyfilterMap["filter_id"] = filterId;
            keyfilterMap["status"] =
                checkIfFilterOptionCantBeConstructedByFilterMenu(key);
            if (keyfilterMap["filter_id"].toString().length > 1)
              keyfilters.add(keyfilterMap);
          });
        } else {
          // var keyfilterMap = {};
          // keyfilterMap["key"] = key;
          // keyfilterMap["filter_id"] = filterIds;
          // keyfilterMap["status"] =
          //     checkIfFilterOptionCantBeConstructedByFilterMenu(key);
          // keyfilters.add(keyfilterMap);
        }
      });
      if (keyfilters.length > 0) {
        return constructBreadCrumbGridView(keyfilters);
      } else {
        printIfDebug("keyfilters.length < 0");
        return Container();
      }
    }
  }

  Widget constructBreadCrumbGridView(var keyfilters) {
    double heightNew = (((keyfilters.length + 1) / 2.ceil()) * 60.0) > 80
        ? (((keyfilters.length + 1) / 2.ceil()) * 60.0)
        : 80.0;
    return new SizedBox.fromSize(
      size: Size.fromHeight(heightNew),
      child: new GridView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        // physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3.5, // 1.75,
          mainAxisSpacing: 10, // 2
          crossAxisSpacing: 2, // 2
        ),
        itemCount: keyfilters.length + 1,
        padding: const EdgeInsets.only(
          top: 15,
          bottom: 15,
          left: 10,
          right: 10,
        ),
        itemBuilder: (context, index) => constructBreadCrumb(
            context, index, keyfilters, keyfilters.length + 1),
      ),
    );
  }

  filterBreadCrumbSectionNew() {
    var keyfilters = [];
    if (widget.chosenFilters == null) {
      return null;
    } else {
      widget.chosenFilters.forEach((key, filterIds) {
        if (filterIds is List) {
          filterIds.forEach((filterId) {
            var keyfilterMap = {};
            keyfilterMap["key"] = key;
            keyfilterMap["filter_id"] = filterId;
            keyfilterMap["status"] =
                checkIfFilterOptionCantBeConstructedByFilterMenu(key);
            if (keyfilterMap["filter_id"].toString().length > 1)
              keyfilters.add(keyfilterMap);
          });
        } else {
          // var keyfilterMap = {};
          // keyfilterMap["key"] = key;
          // keyfilterMap["filter_id"] = filterIds;
          // keyfilterMap["status"] =
          //     checkIfFilterOptionCantBeConstructedByFilterMenu(key);
          // keyfilters.add(keyfilterMap);
        }
      });
      if (keyfilters.length > 0) {
        return keyfilters;
      } else {
        return null;
      }
    }
  }

  String textBoxTags(keyfilters) {
    if (keyfilters != null) {
      String tags = "";
      for (int len = 0; len < keyfilters.length; len++) {
        String tag = extractTagName(
            keyfilters[len]["key"], keyfilters[len]["filter_id"]);
        printIfDebug(tag);
        if (len == keyfilters.length - 1) {
          tags = tags + tag + "";
        } else {
          tags = tags + tag + " ,";
        }
      }
      return tags;
    } else
      return "";
    // Container(
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.start,
    //     children: <Widget>[
    //       new ListView.builder(
    //         itemCount: keyfilters.length,
    //         scrollDirection: Axis.horizontal,
    //         itemBuilder: (context, index) =>
    //             constructBreadCrumbNew(context, index, keyfilters),
    //       ),
    //     ],
    //   ),
    // );
  }

  Widget chosenFilterSection() {
    return new Container(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // displayTitle(),
          // getWidgetForSaveCurrentFilterOptions(),
          Card(
            color: Constants.appColorL2,
            margin: EdgeInsets.only(
              right: 10.0,
              top: 5.0,
              left: 10.0,
              bottom: 5.0,
            ),
            elevation: 0,
            child: getNewFilter(),
          ),
        ],
      ),
    );
  }

  hideTagTextList(bool open) {
    if (open) {
      setState(() {
        hideFlag = false;
      });
    } else {
      setState(() {
        hideFlag = true;
      });
    }
  }

  Widget getNewFilter() {
    return ExpansionTile(
      backgroundColor: Constants.appColorL2,
      title: titleOfExpandable(),
      onExpansionChanged: hideTagTextList,
      trailing: hideFlag
          ? Image.asset(
              'assests/c_down.png',
              height: 30,
              width: 30,
            )
          : Image.asset(
              'assests/c_up.png',
              height: 30,
              width: 30,
            ),
      children: [
        Container(
          decoration: BoxDecoration(
            color: Constants.appColorL1,
            border: Border.all(
              width: 3,
              color: Constants.appColorL2,
            ),
          ),
          child: Column(
            children: [
              filterBreadCrumbSection(),
              loadSaveButtons(),
            ],
          ),
        )
      ],
    );
  }

  Widget loadSaveButtons() {
    return Row(
      children: <Widget>[
        Expanded(
          child: GestureDetector(
            onTap: () {
              if (listOfWidgetsForSavedFilters.length > 0) {
                showDialog(
                  context: context,
                  builder: (_) => new AlertDialog(
                    backgroundColor: Constants.appColorL2,
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Text(
                              "Saved Filters:",
                              style: TextStyle(
                                color: Constants.appColorFont,
                              ),
                            ),
                            Spacer(),
                            Container(
                              padding: EdgeInsets.only(
                                  // right: 10,
                                  ),
                              // margin: EdgeInsets.only(
                              //   right: 10,
                              // ),
                              child: InkWell(
                                child: Image.asset(
                                  "assests/greenclose.png",
                                  height: 25,
                                  width: 25,
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  printIfDebug('\n\nclose button is tapped');
                                },
                              ),
                            ),
                          ],
                        ),
                        // insertDivider(),
                        Container(
                          // padding: const EdgeInsets.only(
                          //   right: 220.0,
                          //   left: 8.0,
                          // ),
                          margin: EdgeInsets.only(
                              // bottom: 10,
                              ),
                          child: SizedBox(
                            height: 2.5,
                            child: Container(
                              // margin: new EdgeInsetsDirectional.only(right),
                              width: 50,
                              height: 2.5,
                              color: Constants.appColorLogo,
                            ),
                          ),
                        ),
                      ],
                    ),
                    content: Container(
                      height: listOfWidgetsForSavedFilters.length * 50.0 < 300
                          ? listOfWidgetsForSavedFilters.length * 50.0
                          : 300,
                      child: new ListView(
                        shrinkWrap: true,
                        children: listOfWidgetsForSavedFilters,
                      ),
                    ),
                  ),
                );
              } else {
                showToast(
                  "No Filters to load",
                  'long',
                  'bottom',
                  1,
                  Constants.appColorLogo,
                );
              }
            },
            child: Container(
              padding:
                  EdgeInsets.only(top: 12, bottom: 12.0, left: 20, right: 20),
              decoration: BoxDecoration(
                color: Constants.appColorL2,
              ),
              child: Center(
                child: Text(
                  "Load Filter",
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 13,
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () {
              actionForSaveCurrentFilterOnTap();
            },
            child: Container(
              padding:
                  EdgeInsets.only(top: 12, bottom: 12.0, left: 20, right: 20),
              decoration: BoxDecoration(
                color: Constants.appColorL2,
              ),
              child: Center(
                child: Text(
                  "Save Filter",
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 13,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget titleOfExpandable() {
    return Container(
      color: Constants.appColorL2,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: getFilterbyTab(),
          ),
          Expanded(
            flex: 3,
            child: getTagsbyTab(hideFlag),
          ),
        ],
      ),
    );
  }

  Widget expandedFilterIcon() {
    return GestureDetector(
      onTap: () {
        showToast('New filter save selected', 'long', 'bottom', 1,
            Constants.appColorLogo);
      },
      child: Container(
        margin: EdgeInsets.only(
          right: 5.0,
          top: 10.0,
          left: 5.0,
          bottom: 5.0,
        ),
        child: Center(
          child: Image.asset(
            'assests/greendown.png',
          ),
        ),
      ),
    );
  }

  Widget getFilterbyTab() {
    return Container(
      color: Constants.appColorL2,
      // margin: EdgeInsets.only(left: 10.0),
      height: 40,
      // width: 140,

      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 25,
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    width: 3.0,
                    color: Constants.appColorLogo,
                  ),
                ),
              ),
              padding: EdgeInsets.only(
                left: 8,
              ),
              child: Center(
                child: Text(
                  hideFlag ? 'Filtered By :' : 'Filtered By',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 16,
                    color: Constants.appColorFont,
                    // letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getTagsbyTab(bool hideFlag) {
    var keyfilters = filterBreadCrumbSectionNew();

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        color: Constants.appColorL2,

        // margin: EdgeInsets.only(left: 10.0),
        height: 40,
        // width: 140,

        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                // margin: EdgeInsets.only(left: 4),
                child: Text(
                  hideFlag ? textBoxTags(keyfilters) : "",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 13,
                    color: Constants.appColorFont,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getWidgetForSaveCurrentFilterOptions() {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.only(
                right: 5.0,
              ), // padding
              child: getContainerForSaveCurrentFilter(),
            ),
          ),
          Expanded(
            flex: 2,
            child: filterBreadCrumbSection(),
          ),
        ],
      ),
    );
  }

  void actionForSaveCurrentFilterOnTap() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    authenticationToken = (prefs.getString('authenticationToken') ?? "");
    if (authenticationToken.isEmpty) {
      printIfDebug('\n\nuser is not signed in');
      showToast('Login to save the current filter options', 'long', 'bottom', 1,
          Constants.appColorLogo);
    } else {
      printIfDebug(
          '\n\nuser is signed in and save current filter button is tapped');
      printIfDebug('\n\ncurrent chosen filter is: ${widget.chosenFilters}');
      Map userChosenFilter = widget.chosenFilters,
          myFilterJSON = widget.chosenFilters;
      saveFilterDialogBox(myFilterJSON, userChosenFilter);
    }
  }

  Widget getContainerForSaveCurrentFilter() {
    return GestureDetector(
      onTap: () {
        actionForSaveCurrentFilterOnTap();
      },
      child: Container(
        padding: EdgeInsets.only(top: 2),
        margin: EdgeInsets.only(left: 10.0),
        height: 40,
        // width: 140,
        decoration: new BoxDecoration(
          color: Constants.appColorLogo,
          border: new Border.all(color: Constants.appColorLogo),
          borderRadius: new BorderRadius.circular(30.0),
        ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 4),
                child: Text(
                  'SAVE',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16,
                    color: Constants.appColorFont,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Container(
                height: 23,
                margin: EdgeInsets.only(left: 6.0),
                child: Image.asset(
                  'assests/save_filter_save_button_icon.png',
                  color: Constants.appColorFont,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void callSaveFilterAPIEndpoint(Map myFilterJSON, Map userChosenFilter) async {
    String queryStringValue = "",
        saveFilterAPIEndpoint =
            "https://api.flixjini.com/queue/save_filter.json?";
    userChosenFilter.forEach((key, value) {
      if (value.length <= 0) {
        // dont add it to query string
      } else {
        queryStringValue += "&";
        queryStringValue += key + "=";
        for (int i = 0; i < value.length; i++) {
          String trimmedValue = value[i].trim();
          trimmedValue = trimmedValue.toLowerCase();
          queryStringValue += trimmedValue;
          i == value.length - 1
              ? queryStringValue += ""
              : queryStringValue += ",";
        }
      }
    });

    printIfDebug('\n\nquery string value is: $queryStringValue');

    queryStringValue = queryStringValue.replaceAll('&', '%26');
    queryStringValue = queryStringValue.replaceAll('=', '%3D');
    queryStringValue = queryStringValue.replaceAll(',', '%2C');

    saveFilterAPIEndpoint +=
        "query_string=" + queryStringValue; // adding query_string param
    printIfDebug('\n\nquery string added url: $saveFilterAPIEndpoint');

    saveFilterAPIEndpoint +=
        "&name=" + userEnteredFilterName; // adding name param

    String authToken = await getStringFromSP('authenticationToken');
    saveFilterAPIEndpoint += "&jwt_token=" + authToken; // adding auth token

    String urlHavingHeader = "", saveFilterAPIEndpointWithDefaultParams = "";

    urlHavingHeader = constructHeader("");

    saveFilterAPIEndpoint += "&" + urlHavingHeader;

    saveFilterAPIEndpointWithDefaultParams =
        await fetchDefaultParams(saveFilterAPIEndpoint);

    // adding magic and country_code params
    saveFilterAPIEndpointWithDefaultParams += "&magic=true&country_code=IN";

    printIfDebug(
        '\n\nurl before making a network call: $saveFilterAPIEndpointWithDefaultParams');
    finalCallToSaveFilterAPIEndpoint(
        saveFilterAPIEndpointWithDefaultParams.toString());
  }

  void finalCallToSaveFilterAPIEndpoint(String url) async {
    var resBody;
    try {
      http.get(
          Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        //
        printIfDebug(
            '\n\nresbody error runtime type is: ${resBody['error'].runtimeType}');
        if (resBody["error"] == false) {
          Navigator.of(context).pop();
          printIfDebug('\n\nfilter is saved successfully');
          showToast('Filter saved successfully', 'long', 'bottom', 1,
              Constants.appColorLogo);
        } else {
          Navigator.of(context).pop();
          printIfDebug('\n\nfilter is not saved');
          showToast(
              'Unable to save Filter at this moment. Please try again later',
              'long',
              'bottom',
              1,
              Constants.appColorError);
        }
        setState(() {
          saveLoading = false;
        });
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  Future<void> saveFilterDialogBox(Map myFilterJSON, Map userChosenFilter) {
    printIfDebug('\n\nhi');
    getListOfWidgetsFromFilterJSON(myFilterJSON);
    printIfDebug('\n\nbye');
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Constants.appColorL2,
            // shape: RoundedRectangleBorder(
            //   borderRadius: BorderRadius.all(
            //     Radius.circular(14.0),
            //   ),
            // ),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              padding: EdgeInsets.only(left: 6, right: 6),
              width: 320.0,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 6),
                                child: Text(
                                  "Current Applied Filter",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Constants.appColorFont,
                                  ),
                                ),
                              ),
                              Spacer(),
                              Container(
                                // padding: EdgeInsets.only(
                                //   left: 35,
                                // ),
                                margin: EdgeInsets.only(
                                  right: 10,
                                ),
                                child: InkWell(
                                  child: Image.asset(
                                    "assests/greenclose.png",
                                    height: 25,
                                    width: 25,
                                  ),
                                  onTap: () {
                                    Navigator.of(context).pop();
                                    printIfDebug('\n\nclose button is tapped');
                                  },
                                ),
                              ),
                            ],
                          ),
                          //     // Text(
                          //     //   "Save this Filter to access it Later",
                          //     //   style: TextStyle(fontSize: 12.0),
                          //     // ),
                        ],
                      ),
                    ),

                    insertDivider(),

                    Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: listOfWidgets,
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(
                        bottom: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              bottom: 5,
                            ),
                            child: Text(
                              "Save this Filter to access it later.",
                              style: TextStyle(
                                color: Constants.appColorDivider,
                                fontSize: 10,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Container(
                                  height: 43,

                                  padding: EdgeInsets.only(
                                    left: 10.0,
                                    top: 15.0,
                                  ),
                                  // margin: EdgeInsets.only(
                                  //   top: 20,
                                  //   bottom: 20,
                                  // ),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1.0,
                                      color: Constants.appColorFont,
                                    ),
                                  ),
                                  child: EnsureVisibleWhenFocused(
                                    key: widget.key,
                                    focusNode: saveFilterFocusNode,
                                    child: TextField(
                                      style: TextStyle(
                                          color: Constants.appColorFont),
                                      cursorColor: Constants.appColorFont,
                                      controller: filterNameController,
                                      focusNode: saveFilterFocusNode,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                        ),
                                        // enabled: false,

                                        hintText: "Filter Name",
                                        fillColor: Constants.appColorFont,
                                        focusColor: Constants.appColorFont,
                                        hoverColor: Constants.appColorFont,
                                        hintStyle: TextStyle(
                                          color: Constants.appColorFont,
                                          decorationColor:
                                              Constants.appColorFont,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: saveLoading
                                    ? getProgressBar()
                                    : Container(
                                        height: 43,
                                        decoration: BoxDecoration(
                                          color: Constants.appColorLogo,
                                          border: Border.all(
                                            width: 1.0,
                                            color: Constants.appColorLogo,
                                          ),
                                        ),
                                        margin: EdgeInsets.only(
                                          left: 10.0,
                                          right: 10,
                                        ),
                                        child: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              saveLoading = true;
                                            });
                                            actionForSaveFilterButtonOnTap(
                                                myFilterJSON, userChosenFilter);
                                          },
                                          child: Center(
                                            child: Text(
                                              "Save",
                                              style: TextStyle(
                                                color: Constants.appColorFont,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    //     child: Text(
                    //       "Close",
                    //       style: TextStyle(
                    //           color: Constants.appColorL1, fontWeight: FontWeight.bold),
                    //       textAlign: TextAlign.center,
                    //     ),
                    //   ),
                    //   onTap: () {
                    //     Navigator.of(context).pop();
                    //     printIfDebug('\n\nclose button is tapped');
                    //   },
                    // ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Widget insertDivider() {
    return Container(
      padding: const EdgeInsets.only(
        right: 220.0,
        left: 8.0,
      ),
      margin: EdgeInsets.only(
        bottom: 10,
      ),
      child: SizedBox(
        height: 2.5,
        child: Container(
          // margin: new EdgeInsetsDirectional.only(right),
          width: 30,
          height: 2.5,
          color: Constants.appColorLogo,
        ),
      ),
    );
  }

  void actionForSaveFilterButtonOnTap(Map myFilterJSON, Map userChosenFilter) {
    userEnteredFilterName = filterNameController.text;
    if (userEnteredFilterName == '' || userEnteredFilterName == null) {
      printIfDebug("\n\nfilter name can't be empty");
      setState(() {
        saveLoading = false;
      });
      showToast('Enter a Filter Name to Save', 'long', 'bottom', 1,
          Constants.appColorLogo);
    } else {
      printIfDebug('\n\nsaving filter with the name $userEnteredFilterName');

      callSaveFilterAPIEndpoint(myFilterJSON, userChosenFilter);
    }
  }

  void getListOfWidgetsFromFilterJSON(Map myFilterJSON) {
    listOfWidgets = [];
    myFilterJSON.forEach((key, value) {
      printIfDebug('\n\ntaking the key: $key and its value: $value');
      try {
        takeKey(key, value);
      } catch (e) {
        printIfDebug('\n\nexception caught: $e');
      }
    });
  }

  void takeKey(String key, List<dynamic> value) {
    printIfDebug('\n\ndai machan');
    printIfDebug('\n\nkey $key has values $value');
    String values = "";
    bool keyHasMultipleValues;

    if (value.length > 1) {
      keyHasMultipleValues = true;
    } else {
      keyHasMultipleValues = false;
    }

    for (int i = 0; i < value.length; i++) {
      value[i] = value[i][0].toUpperCase() + value[i].substring(1);
      if (i == value.length - 1) {
        values += value[i] + ". ";
      } else {
        values += value[i] + ", ";
      }
    }
    try {
      String returnedString = getProperKey(key, keyHasMultipleValues);

      listOfWidgets.add(
        Container(
          margin: EdgeInsets.all(5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$returnedString : ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Constants.appColorFont,
                ),
              ),
              Expanded(
                // flex: ,
                child: Text(
                  '$values',
                  textAlign: TextAlign.left,
                  maxLines: 3,
                  style: TextStyle(
                    // fontWeight: FontWeight.bold,
                    color: Constants.appColorFont,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } catch (e) {
      printIfDebug('\n\nexception caught is: $e');
    }
  }

  String getProperKey(String s, bool keyHasMultipleValues) {
    if (s.contains('list')) {
      s = s.replaceAll('list', '');
    }
    if (s.contains('type')) {
      s = s.replaceAll('type', ' Type');
    }
    int lastCharIndex = s.length - 1;
    printIfDebug('\n\nindex of last char of the string $s is: $lastCharIndex');
    if (keyHasMultipleValues && s[lastCharIndex] != 's') {
      printIfDebug('\n\nlast char is not s, so append s');
      s += "s";
    } else if (!keyHasMultipleValues && s[lastCharIndex] == 's') {
      // key has a single value
      printIfDebug(
          '\n\nkey has a single value but is in plural form... so, remove char s from the string');
      s = s.substring(0, lastCharIndex);
    }

    return s[0].toUpperCase() + s.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (!chosenFilterLoaded) {
      loadChosenFilter();
      chosenFilterLoaded = true;
    }
    try {
      return chosenFilterSection();
    } catch (e) {
      printIfDebug("chosenFilterSection" + e.toString());
    }
    return Container();
  }
}
