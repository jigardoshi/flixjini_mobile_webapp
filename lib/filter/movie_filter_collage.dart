// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_poster.dart';
// import  'package:flutter_swiper/flutter_swiper.dart';
// import 'package:flixjini_webapp/common/arc_clipper.dart';
import 'package:flixjini_webapp/common/slant_clipper.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import "dart:async";
import 'dart:ui';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterCollage extends StatefulWidget {
  MovieFilterCollage({
    Key? key,
    this.movieDetail,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieDetail, showOriginalPosters;

  @override
  _MovieFilterCollageState createState() => new _MovieFilterCollageState();
}

class _MovieFilterCollageState extends State<MovieFilterCollage> {
  int bannerPosition = 0;
  var heroCollage;
  bool collageFlag = true;

  void initState() {
    super.initState();
  }

  carouselEffect() async {
    if (!mounted)
      return;
    else {
      const oneSec = const Duration(seconds: 5);
      new Timer.periodic(oneSec, (Timer t) {
        if (bannerPosition <
            widget.movieDetail["gallary"]["posters"].length - 1) {
          setState(() {
            bannerPosition = bannerPosition + 1;
          });
          printIfDebug("Banner Position Updated");
          printIfDebug(new DateTime.now());
          printIfDebug(bannerPosition);
        } else {
          printIfDebug("Resetting Banner Position");
          setState(() {
            bannerPosition = 0;
          });
        }
      });
    }
  }

  selectedBannerImage() {
    printIfDebug("Clicked on Banner Iamge");
  }

  positionPoster(double positionLeft, double positionTop, int i) {
    var poster = new Positioned(
      left: positionLeft,
      top: positionTop,
      child: renderImage(i),
    );
    return poster;
  }

  overlayHeading() {
    String title = "Discover from our Best";
    if (widget.movieDetail.containsKey("title")) {
      title = widget.movieDetail["title"];
    }
    var screenWidth = MediaQuery.of(context).size.width;
    var headinblur = new Positioned(
      child: Container(
        child: new Center(
          child: new Container(
            width: screenWidth * 0.70,
            padding: const EdgeInsets.all(15.0),
            child: new Text(
              title.toUpperCase(),
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: Constants.appColorFont.withOpacity(0.9),
                fontSize: 30.0,
                letterSpacing: 3.0,
              ),
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
        ),
      ),
    );
    return headinblur;
  }

  slightlyBlurFullHeroCollage() {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    double overlayOpacity = widget.movieDetail["movies"].length < 7 ? 0.8 : 0.6;
    double sigma = widget.showOriginalPosters ? 0.5 : 10;
    var headinOverlay = new Positioned.fill(
      child: new BackdropFilter(
        filter: new ImageFilter.blur(
          sigmaX: sigma,
          sigmaY: sigma,
        ),
        child: new Container(
          width: screenWidth,
          height: screenHeight * 0.6,
          color: Constants.appColorL1.withOpacity(overlayOpacity),
        ),
      ),
    );
    return headinOverlay;
  }

  overlayBlur() {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    double overlayOpacity = 0.4;
    var headinOverlay = new Positioned(
      child: new Center(
        child: new Container(
          width: screenWidth * 0.80,
          height: screenHeight * 0.20,
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: 4.0,
                sigmaY: 4.0,
              ),
              child: new Container(
                width: screenWidth,
                height: screenHeight * 0.6,
                decoration: new BoxDecoration(
                  border: new Border.all(
                    color: Constants.appColorFont,
                    width: 1,
                  ),
                  color: Constants.appColorL1.withOpacity(
                    overlayOpacity,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
    return headinOverlay;
  }

  createCollage() {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenheight = MediaQuery.of(context).size.height;
    double positionLeft;
    double positionTop;
    var posters = <Widget>[];
    int length = widget.movieDetail["movies"].length > 7
        ? 8
        : widget.movieDetail["movies"].length;
    if (length > 7) {
      for (int i = 0; i < length; i++) {
        var poster;
        if (i == 0) {
          positionLeft = screenWidth * -0.20;
          positionTop = -6.0;
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 1) {
          positionLeft = screenWidth * 0.10;
          positionTop = -6.0;
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 2) {
          positionLeft = screenWidth * 0.40;
          positionTop = -6.0;
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 3) {
          positionLeft = screenWidth * 0.70;
          positionTop = -6.0;
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 4) {
          positionLeft = screenWidth * -0.1;
          positionTop = (screenheight * 0.34);
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 5) {
          positionLeft = screenWidth * 0.20;
          positionTop = (screenheight * 0.34);
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 6) {
          positionLeft = screenWidth * 0.50;
          positionTop = (screenheight * 0.34);
          poster = positionPoster(positionLeft, positionTop, i);
        } else if (i == 7) {
          positionLeft = screenWidth * 0.80;
          positionTop = (screenheight * 0.34);
          poster = positionPoster(positionLeft, positionTop, i);
        } else {
          printIfDebug("None");
        }
        posters.add(poster);
      }
    } else {
      var poster;

      poster = Container(
        width: screenWidth,
        height: screenheight * 0.6,
        child:
            widget.movieDetail["movies"][0]["moviePhoto"].toString().length > 1
                ? Image(
                    image: NetworkImage(
                      widget.movieDetail["movies"][0]["moviePhoto"],
                      // useDiskCache: false,
                    ),
                    fit: BoxFit.fill,
                    gaplessPlayback: true,
                  )
                : Container(
                    color: Constants.appColorL2,
                    child: displayNoResultsFound(),
                  ),
      );
      posters.add(poster);
    }
    var poster;
    poster = slightlyBlurFullHeroCollage();
    posters.add(poster);
    poster = overlayBlur();
    posters.add(poster);
    poster = overlayHeading();
    posters.add(poster);
    var gradientEffect = new Positioned(
      child: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
          colors: [
            Constants.appColorL1.withOpacity(0.9),
            Colors.transparent.withOpacity(0.0),
          ],
          stops: [0.0, 1.0],
          begin: FractionalOffset.bottomCenter,
          end: FractionalOffset.topCenter,
          tileMode: TileMode.clamp,
        )),
      ),
    );
    printIfDebug(
        "from create collage method, just printing gradientEffect, just for the sake of not getting unused variable: $gradientEffect");
    setState(() {
      collageFlag = true;
    });
    return posters;
  }

  Widget renderImage(i) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenheight = MediaQuery.of(context).size.height;
    Widget displayPart = new Container(color: Constants.appColorL1);
    if (widget.movieDetail["movies"][i]["moviePhoto"] != null) {
      if (widget.movieDetail["movies"][i]["moviePhoto"].isNotEmpty) {
        displayPart = Image(
          image: NetworkImage(
            widget.movieDetail["movies"][i]["moviePhoto"],
            // useDiskCache: false,
          ),
          fit: BoxFit.cover,
          gaplessPlayback: true,
        );
      }
    }

    return new Container(
      width: screenWidth * 0.40,
      height: screenheight * 0.35,
      child: new RotationTransition(
        turns: new AlwaysStoppedAnimation(0),
        child: new GestureDetector(
          child: ClipPath(
            clipper: SlantClipper(),
            child: displayPart,
          ),
          onTap: () {
            selectedBannerImage();
          },
        ),
      ),
      decoration: new BoxDecoration(boxShadow: [
        new BoxShadow(
          color: Constants.appColorL1,
          blurRadius: 40.0,
        ),
      ]),
    );
  }

  Widget defaultHeroImage() {
    return new Container(
      color: Constants.appColorFont,
    );
  }

  checkIfCollageReady() {
    if (widget.movieDetail["movies"].length < 8) {
      return heroCollage;
    } else {
      if (collageFlag) {
        return heroCollage;
      } else {
        var posters = <Widget>[];
        posters.add(new Container(
          color: Constants.appColorL2,
          child: displayNoResultsFound(),
        ));

        return posters;
      }
    }
  }

  nullCheckImageUrl() {
    var carousel;
    var screenWidth = MediaQuery.of(context).size.width;
    var screenheight = MediaQuery.of(context).size.height;
    if (collageFlag) {
      carousel = new Container(
        color: Constants.appColorL1,
        // child: ClipPath(
        //   clipper: ArcClipper(),
        child: new GestureDetector(
          onTap: selectedBannerImage(),
          child: new Container(
            width: screenWidth,
            height: screenheight * 0.6,
            decoration: new BoxDecoration(
              color: Constants.appColorL1,
            ),
            child: new Stack(
                children: checkIfCollageReady() != null
                    ? checkIfCollageReady()
                    : <Widget>[
                        Container(
                          color: Constants.appColorL2,
                          child: displayNoResultsFound(),
                        ),
                      ]

                // getFitlerPageBlurredCarouselImage(),
                ),
          ),
        ),
        // ),
      );
    } else {
      carousel = defaultHeroImage();
    }
    return carousel;
  }

  addStackContents() {
    var poster, posters = <Widget>[];

    poster = overlayBlur();
    posters.add(poster);
    poster = overlayHeading();
    posters.add(poster);
    return posters;
  }

  Widget renderDefaultMessage(String defaultMessage, FontWeight fontWeight) {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: width * 0.75,
      child: new Center(
        child: new Text(
          defaultMessage,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: Constants.appColorFont,
            fontSize: 15.0,
          ),
          textAlign: TextAlign.center,
          maxLines: 4,
        ),
      ),
    );
  }

  Widget renderDefaultMessagewithSpan(
      String defaultMessage, String defaultSpanMessage) {
    return new Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultMessage(defaultMessage, FontWeight.normal),
            renderDefaultMessage(defaultSpanMessage, FontWeight.bold),
          ],
        ),
      ),
    );
  }

  Widget renderDefaultImage() {
    return new Center(
      child: new Container(
        width: 100.0,
        height: 100.0,
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "sadsmile"),
      ),
    );
  }

  Widget displayNoResultsFound() {
    String defaultMessage = "Sorry No Results Found";
    String defaultSpanMessage = "For the Selected Filter Options";
    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          renderDefaultImage(),
          renderDefaultMessagewithSpan(defaultMessage, defaultSpanMessage),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (widget.movieDetail.containsKey("movies")) {
      if (widget.movieDetail["movies"].isNotEmpty) {
        setState(() {
          heroCollage = createCollage();
        });
      }
    }
    var carousel = nullCheckImageUrl();
    return carousel;
  }
}
