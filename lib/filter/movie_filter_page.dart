// import 'dart:io';

import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
// import 	'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
// import	'package:streaming_entertainment/filter/movie_filter_activity.dart';
import 'package:flixjini_webapp/filter/movie_filter_filtered_movies.dart';
// import  'package:streaming_entertainment/filter/movie_filter_pagination.dart';
// import 'package:flixjini_webapp/filter/movie_filter_search_bar.dart';
import 'package:flixjini_webapp/filter/movie_filter_location.dart';
import 'package:flixjini_webapp/filter/movie_filter_chosen_filters.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/filter/movie_filter_collapsable_appbar.dart';
import 'package:flixjini_webapp/navigation/movie_navigator_with_notch.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 	'package:streaming_entertainment/common/encryption_function.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/navigation/movie_navigator.dart';
import 'package:flutter/services.dart' show rootBundle;
// import  'package:android_intent/android_intent.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:simple_permissions/simple_permissions.dart';
// import  'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_filter.dart';
import 'dart:ui';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterPage extends StatefulWidget {
  MovieFilterPage({
    this.appliedFilterUrl,
    this.visibleFilters,
    this.visibilityStatus,
    this.inTheaterKey,
    this.showOriginalPosters,
    Key? key,
  }) : super(key: key);

  final appliedFilterUrl;
  final visibleFilters;
  final visibilityStatus;
  final inTheaterKey;
  final showOriginalPosters;

  @override
  _MovieFilterPageState createState() => new _MovieFilterPageState();
}

class _MovieFilterPageState extends State<MovieFilterPage> {
  final TextEditingController searchController = new TextEditingController();
  bool endOfSearchResults = false;
  bool searchOff = false;
  bool waiting = false;
  bool fetchedMovieDetail = false;
  bool fetchedUserPreference = false;
  bool fetchedUserQueuePreference = false;
  bool disablePaginationButton = false;
  bool filterHaveBeenApplied = false;
  bool loadingSymbolFetchMoreResults = false;
  bool loadingSymbolRemoveBreadcrumb = false;
  bool filterDataReady = false;
  bool theTheaterKey = false, showOriginalPosters = false;
  var sortData = {};
  var movieDetail = {};
  var userDetail = {};
  var userQueue = {};
  var filterData = {}, userFilterData = {};
  int pageNo = 1;
  String stringFilterData = "";
  String searchString = "";
  String checkString = "";
  String authenticationToken = "";
  String appliedMoviesFilterUrl = "";
  String cacheInvisibleTags = "";
  ScrollController filterScrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    checkTheTheaterKey();

    printIfDebug("Received Fliter Options from Index Page");
    printIfDebug(widget.appliedFilterUrl);
    authenticationStatus();
    loadFilterMenu();
    checkURLData(true);
    checkShowOriginalPosters();
    logUserScreen('Flixjini Filter Page', 'MovieFilterPage');
  }

  checkShowOriginalPosters() async {
    if (widget.showOriginalPosters == null) {
      String showOriginalPostersFromFRC =
          await getStringFromFRC('show_original_posters');
      if (showOriginalPostersFromFRC == 'true') {
        setState(() {
          this.showOriginalPosters = true;
        });
      } else {
        setState(() {
          this.showOriginalPosters = false;
        });
      }
    } else {
      setState(() {
        showOriginalPosters = widget.showOriginalPosters;
      });
    }
  }

  checkURLData(bool flag) {
    if (widget.appliedFilterUrl != null && flag) {
      printIfDebug("Filters have been applied");
      appliedMoviesFilterUrl = widget.appliedFilterUrl;
      printIfDebug(appliedMoviesFilterUrl);
      loadStaticData();
      getMovieDetails(appliedMoviesFilterUrl);
    } else {
      printIfDebug("Filters have not been applied");
      appliedMoviesFilterUrl =
          "https://api.flixjini.com/entertainment/filter_retrieve.json?" +
              authenticationToken +
              "&languagelist=tamil,hindi,english&contenttypelist=movie";
      loadStaticData();
      getMovieDetails(appliedMoviesFilterUrl);
    }
  }

  setTheTheaterKey(bool value) {
    setState(() {
      theTheaterKey = value;
    });
  }

  checkTheTheaterKey() {
    if (widget.inTheaterKey != null) {
      setTheTheaterKey(widget.inTheaterKey);
    } else {
      setState(() {
        setTheTheaterKey(false);
      });
    }
    filterScrollController.addListener(_scrollListener);
  }

  _scrollListener() {
    if (filterScrollController.offset >=
            filterScrollController.position.maxScrollExtent &&
        !filterScrollController.position.outOfRange) {
      setState(() {
        // moreTagsloading = true;
      });
      changePageNo();
    }
    if (filterScrollController.offset <=
            filterScrollController.position.minScrollExtent &&
        !filterScrollController.position.outOfRange) {
      printIfDebug("top sucesss//////////");

      // setState(() {
      //   message = "reach the top";
      // });
    }
  }

  loadFilterMenu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // setState(() {
    //   stringFilterData = (prefs.getString('stringFilterData') ?? "");
    // });
    if (stringFilterData.isEmpty || true) {
      getFilterMenu();
    } else {
      printIfDebug("Filter Options already present");
      var data = json.decode(stringFilterData);
      printIfDebug("Need to convert Data to String");
      String temp = json.encode(data);
      setState(() {
        filterData = data;
        filterDataReady = true;
        prefs.setString('stringFilterData', temp);
      });
      printIfDebug("Reading filters from Cache");
      printIfDebug(filterData);
    }
  }

  getFilterMenu() async {
    String filterMenuOptionsUrl =
        'https://api.flixjini.com/entertainment/filter_options.json?';
    // String filterMenuOptionsUrl =
    //     "https://api.flixjini.com/roulette/get_profile.json?jwt_token=";
    filterMenuOptionsUrl =
        authenticationToken != null && authenticationToken.length > 0
            ? filterMenuOptionsUrl + "jwt_token=" + authenticationToken + "&"
            : filterMenuOptionsUrl;
    printIfDebug("Load Menu from");
    printIfDebug(filterMenuOptionsUrl);
    String rootUrl = filterMenuOptionsUrl;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));

      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        printIfDebug(responseBody);
        var data = json.decode(responseBody);
        this.filterData = data;
        setState(() {
          filterData = this.filterData;
          userFilterData = filterData["filters"];
          filterDataReady = true;
        });
        printIfDebug("Data Fetched");
        printIfDebug(filterData);
      } else {
        this.filterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.filterData["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  void setInvisibleTags() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('cacheInvisibleTags', cacheInvisibleTags);
  }

  toogleLoadingSymbolFetchMoreResults(bool value) {
    setState(() {
      loadingSymbolFetchMoreResults = value;
    });
  }

  toogleLoadingSymbolRemoveBreadcrumb(bool value) {
    setState(() {
      loadingSymbolRemoveBreadcrumb = value;
    });
  }

  getPreferences() {
    getUserPreference();
    getUserQueuePreference();
  }

  loadFilterAsset() async {
    return await rootBundle.loadString('static/sort_options_data.json');
  }

  loadStaticData() async {
    String jsonString = await loadFilterAsset();
    final jsonResponse = json.decode(jsonString);
    setState(() {
      sortData = jsonResponse;
    });
    printIfDebug(sortData);
  }

  toogleSearchOff() {
    setState(() {
      searchOff = searchOff ? false : true;
    });
  }

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      printIfDebug("User is already Logged in -> Obtain User Preferences");
      getPreferences();
    } else {
      printIfDebug("User can't access User Preferences as he is not logged in");
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }
  }

  getUserQueuePreference() async {
    String urlAuthentication =
        "https://api.flixjini.com/queue/list.json?jwt_token=" +
            authenticationToken;
    String url = await fetchDefaultParams(urlAuthentication);
    var response = await http.get(Uri.parse(url));

    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        printIfDebug(" filter queue page" + data.toString());
        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  getUserPreference() async {
    String url =
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  setUserTheaterLocation(locationBasedSources) {
    setState(() {
      movieDetail["userLocation"] = locationBasedSources;
    });
  }

  getMovieDetails(String urlWithSearchParameter) async {
    int lengthOfUrlWithSearchParameter = urlWithSearchParameter.length;
    String rootUrl;
    printIfDebug("SubString");
    printIfDebug(
      urlWithSearchParameter.substring(
          lengthOfUrlWithSearchParameter - 4, lengthOfUrlWithSearchParameter),
    );
    if (urlWithSearchParameter.substring(lengthOfUrlWithSearchParameter - 4,
            lengthOfUrlWithSearchParameter) ==
        "json") {
      rootUrl = urlWithSearchParameter + '?';
    } else {
      rootUrl = urlWithSearchParameter + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);

    printIfDebug("Used Url");
    printIfDebug(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug("Status Code");
      printIfDebug(response.statusCode);
      printIfDebug(url);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("Value of Movies");
        printIfDebug(data);
        if (widget.visibleFilters != null) {
          data["filter_json"] = widget.visibleFilters;
          data["filter_json_visibility"] = widget.visibilityStatus;
        }
        printIfDebug("Filter Json Check");
        printIfDebug(data["filter_json_visibility"]);
        printIfDebug(data["filter_json"]);
        if (data.containsKey("filter_json_visibility")) {
          if ((data["filter_json_visibility"] != null) &&
              (data["filter_json_visibility"].containsKey("opt"))) {
            data["opt"] = data["filter_json_visibility"].remove("opt");
            data["filter_json"].remove("opt");
          }
        }
        printIfDebug("The updated Filter Json and Filter Retrive Values");
        printIfDebug(data["filter_json"]);
        if (theTheaterKey) {
          data = checkIfLocationSet(data);
        }
        this.movieDetail = data;
        if (this.mounted) {
          setState(() {
            movieDetail = this.movieDetail;
            fetchedMovieDetail = true;
          });
        }
        printIfDebug("Data Fetched");
        printIfDebug(movieDetail);
        if (movieDetail.containsKey("filter_json")) {
          printIfDebug("Filter Json After Filter Results");
          printIfDebug(widget.appliedFilterUrl);
          printIfDebug(movieDetail["filter_json"]);
          logCertainFilter(movieDetail["filter_json"]);
        } else {
          printIfDebug("Filter Json does not exsist");
        }
      } else if (response.statusCode == 404) {
        checkURLData(false);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (e) {
      printIfDebug(e);
    }
    if (!mounted) return;
  }

  checkIfLocationSet(var data) {
    if (data.containsKey("userLocation")) {
      printIfDebug("key is already present");
    } else {
      data["userLocation"] = "Chennai";
    }
    return data;
  }

  checkifLocationSettingNeeded() {
    if (theTheaterKey) {
      return new MovieFilterLocation(
        userLocation: movieDetail["userLocation"],
        setUserTheaterLocation: setUserTheaterLocation,
      );
    } else {
      return new Container();
    }
  }

  logCertainFilter(var filterJson) {
    printIfDebug("Log function called");
    printIfDebug(filterJson);
    String filterKey;
    var filterOptions;
    var featureList = ["sourceslist", "genreslist", "contenttypelist"];
    int featureListLength = featureList.length;
    if (filterJson != null) {
      filterJson.forEach((key, values) {
        for (var feature = 0; feature < featureListLength; feature++) {
          String formattedValues = "";
          if (key == featureList[feature]) {
            printIfDebug("Yes Equal : " + key + " : " + values.join(','));
            filterKey = key;
            formattedValues = values.join(',').replaceAll('-', '_');
            filterOptions = formattedValues;
            logFilterKey(filterKey, filterOptions);
            if (key != "movie_tags") {
              values.forEach((value) {
                String formattedValue = "";
                formattedValue = value.replaceAll('-', '_');
                logFilterOption(filterKey, formattedValue);
              });
            }
          }
        }
      });
    }
  }

  sendElasticGetRequest(actionUrl) async {
    var url = actionUrl;
    http.get(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      var fetchedData = json.decode(response.body);
      printIfDebug(fetchedData);
      setState(() {
        movieDetail = fetchedData;
      });
    });
    printIfDebug("New Data Fetched by Elastic Search");
  }

  sendGetRequest(String url) async {
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("Value of Movies");
        printIfDebug(data["movies"]);
        setState(() {
          movieDetail["movies"] = data["movies"];
          disablePaginationButton = false;
        });
        printIfDebug("Data Fetched");
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        setState(() {
          disablePaginationButton = false;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  String checkTypeofAppliedFilterUrl() {
    printIfDebug("Url is in Tag Form");
    printIfDebug(movieDetail["filter_json"]);
    String url =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&";
    printIfDebug(url);
    if (movieDetail.containsKey("filter_json")) {
      if (movieDetail["filter_json"].isNotEmpty) {
        movieDetail["filter_json"].forEach((k, v) {
          url += k + "=";
          if (v[0].length > 1) {
            v.forEach((movie) {
              url += movie + ",";
            });
          } else {
            url += v;
          }

          url += "&";
        });
        printIfDebug(url);
        int urlLength = url.length - 1;
        url = url.substring(0, urlLength);
      }
    }
    return url;
  }

  convertAplliedFilterToUrl(var alteredFilters, var visiblityKeys) {
    String url =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&";
    if (alteredFilters.isNotEmpty) {
      alteredFilters.forEach((k, v) {
        url += k + "=";
        url += v.join(',');
        url += "&";
      });
      int urlLength = url.length - 1;
      url = url.substring(0, urlLength);
    }
    printIfDebug("Convert Dictionary to URL");
    getMovieDetailsWhenBreadcrumbsClicked(alteredFilters, url, visiblityKeys);
  }

  getMovieDetailsWhenBreadcrumbsClicked(var alteredFilters,
      String urlWithSearchParameter, var visiblityKeys) async {
    int lengthOfUrlWithSearchParameter = urlWithSearchParameter.length;
    String rootUrl;
    printIfDebug("SubString");
    printIfDebug(urlWithSearchParameter.substring(
        lengthOfUrlWithSearchParameter - 4, lengthOfUrlWithSearchParameter));
    if (urlWithSearchParameter.substring(lengthOfUrlWithSearchParameter - 4,
            lengthOfUrlWithSearchParameter) ==
        "json") {
      rootUrl = urlWithSearchParameter + '?';
    } else {
      rootUrl = urlWithSearchParameter + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    toogleLoadingSymbolRemoveBreadcrumb(true);
    var response = await http.get(Uri.parse(url));
    printIfDebug("Status Code");
    printIfDebug(response.statusCode);
    printIfDebug(url);
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("Value of Movies");
        printIfDebug(data);
        printIfDebug(data["filter_json"]);
        printIfDebug(widget.visibleFilters);
        if (widget.visibleFilters != null) {
          data["filter_json"] = alteredFilters;
          data["filter_json_visibility"] = widget.visibilityStatus;
        } else {
          data["filter_json"] = alteredFilters;
          data["filter_json_visibility"] = visiblityKeys;
        }
        this.movieDetail = data;
        setState(() {
          movieDetail = this.movieDetail;
          fetchedMovieDetail = true;
        });
        printIfDebug("Data Fetched");
        printIfDebug(movieDetail);
        if (movieDetail.containsKey("filter_json")) {
          printIfDebug("Filter Json After Filter Results");
          printIfDebug(widget.appliedFilterUrl);
          printIfDebug(movieDetail["filter_json"]);
        } else {
          printIfDebug("Filter Json does not exsist");
        }
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;

    toogleLoadingSymbolRemoveBreadcrumb(false);
  }

  changePageNo() {
    pageNo += 1;
    printIfDebug("Move to Page Number");
    printIfDebug(pageNo);
    String pageStartsAtMovieNumber = ((pageNo * 50) - 50).toString();
    printIfDebug("Starting Index");
    printIfDebug(pageStartsAtMovieNumber);
    String url = checkTypeofAppliedFilterUrl();
    printIfDebug("Tag Form --> Url Form");
    printIfDebug(url);
    String appliedUrl = url + "&elements=" + pageStartsAtMovieNumber;
    printIfDebug("Paginate to Page");
    printIfDebug(appliedUrl);
    getMoreSearchResults(appliedUrl);
  }

  getMoreSearchResults(String urlWithSearchParameter) async {
    int lengthOfUrlWithSearchParameter = urlWithSearchParameter.length;
    String rootUrl;
    printIfDebug("SubString");
    printIfDebug(urlWithSearchParameter.substring(
        lengthOfUrlWithSearchParameter - 4, lengthOfUrlWithSearchParameter));
    if (urlWithSearchParameter.substring(lengthOfUrlWithSearchParameter - 4,
            lengthOfUrlWithSearchParameter) ==
        "json") {
      rootUrl = urlWithSearchParameter + '?';
    } else {
      rootUrl = urlWithSearchParameter + '&';
    }
    String url = constructHeader(rootUrl);
    printIfDebug("Filter Page Apply");
    printIfDebug(url);
    toogleLoadingSymbolFetchMoreResults(true);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("More Search Results Obtained");
        if (data["movies"].length > 0) {
          var infiniteData = new List.from(movieDetail["movies"])
            ..addAll(data["movies"]);
          setState(() {
            movieDetail["movies"] = infiniteData;
          });
          printIfDebug("Movies Fetched, New Length");
          printIfDebug(movieDetail["movies"].length);
        } else {
          printIfDebug("Reached End of Search Results");
          changeSearchResultsStatus(true);
        }
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        changeSearchResultsStatus(true);
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    toogleLoadingSymbolFetchMoreResults(false);
    if (!mounted) return;
  }

  changeSearchResultsStatus(value) {
    setState(() {
      endOfSearchResults = value;
    });
  }

  Future delayServerRequest() async {
    if (searchString.length >= 2) {
      if (waiting) {
        printIfDebug("Still Waiting..");
      } else {
        String checkString = searchString;
        printIfDebug("Before Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        new Timer(const Duration(seconds: 1), () {
          printIfDebug("After Timeout");
          printIfDebug("Search (Current) String");
          printIfDebug(searchString);
          printIfDebug("Check (Previous) String");
          printIfDebug(checkString);
          if (searchString != checkString) {
            printIfDebug("Dont Make Server Request");
          } else {
            printIfDebug("Make Server Request : Elastic Search");
            appliedMoviesFilterUrl =
                "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
                    authenticationToken +
                    "search=" +
                    searchString;
            printIfDebug(appliedMoviesFilterUrl);
            sendElasticGetRequest(appliedMoviesFilterUrl);
          }
        });
      }
    }
  }

  Future openSearchOrderPopup() {
    var options = <Widget>[];
    var optionsLength = sortData["options"].length;
    for (int i = 0; i < optionsLength; i++) {
      var option = new ListTile(
          title: new Text(sortData["options"][i]["name"]),
          onTap: () {
            printIfDebug(sortData["options"][i]["name"]);
            Navigator.pop(context);
          });
      options.add(option);
    }
    return showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text(sortData["heading"]),
        content: new ListView(
          shrinkWrap: true,
          children: options,
        ),
      ),
    );
  }

  Widget checkifAnyFilterHasBeenApplied() {
    if (movieDetail.containsKey("filter_json")) {
      if (movieDetail["filter_json"].isNotEmpty) {
        printIfDebug('\n\nmovieDetail: $movieDetail');
        printIfDebug(
            '\n\nconvert applied filter to url: $appliedMoviesFilterUrl');
        printIfDebug('\n\nfilter data: $filterData');
        printIfDebug('\n\nset the theatre key: $setTheTheaterKey');
        if (!widget.inTheaterKey)
          return MovieFilterChosenFilters(
            chosenFilters: movieDetail["filter_json"],
            filterVisibilities: movieDetail["filter_json_visibility"],
            convertAplliedFilterToUrl: convertAplliedFilterToUrl,
            filterData: filterData,
            setTheTheaterKey: setTheTheaterKey,
          );
      }
    }
    return new Container();
  }

  Widget renderDefaultMessage(String defaultMessage, FontWeight fontWeight) {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: width * 0.75,
      child: new Center(
        child: new Text(
          defaultMessage,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: Constants.appColorFont,
            fontSize: 15.0,
          ),
          textAlign: TextAlign.center,
          maxLines: 4,
        ),
      ),
    );
  }

  Widget renderDefaultMessagewithSpan(
      String defaultMessage, String defaultSpanMessage) {
    return new Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultMessage(defaultMessage, FontWeight.normal),
            renderDefaultMessage(defaultSpanMessage, FontWeight.bold),
          ],
        ),
      ),
    );
  }

  Widget renderDefaultImage() {
    return new Center(
      child: new Container(
        width: 100.0,
        height: 100.0,
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "sadsmile"),
      ),
    );
  }

  Widget displayNoResultsFound() {
    String defaultMessage = "Sorry No Results Found";
    String defaultSpanMessage = "For the Selected Filter Options";
    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          renderDefaultImage(),
          renderDefaultMessagewithSpan(defaultMessage, defaultSpanMessage),
        ],
      ),
    );
  }

  Widget overlayOnMovieResults() {
    // double overlayOpacity = 0.6;
    // double sigma = 10.0;
    // double screenWidth = MediaQuery.of(context).size.width;
    // double screenHeight = MediaQuery.of(context).size.height;
    return new Stack(
      children: <Widget>[
        new Positioned(
          child: new MovieFilterFilteredMovies(
            movieDetail: movieDetail,
            userDetail: userDetail,
            userQueue: userQueue,
            authenticationToken: authenticationToken,
            endOfSearchResults: endOfSearchResults,
            changePageNo: changePageNo,
            loadingSymbolFetchMoreResults: loadingSymbolFetchMoreResults,
            theTheaterKey: theTheaterKey,
          ),
        ),
        // new Positioned.fill(
        //   child: new Container(
        //     width: screenWidth,
        //     height: screenHeight,
        //     color: new Constants.appColorFont.withOpacity(overlayOpacity),
        //     child: new ClipRect(
        //       child: new BackdropFilter(
        //         filter: new ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
        //         child: new Container(
        //           decoration: new BoxDecoration(
        //               color: Constants.appColorFont.withOpacity(overlayOpacity)),
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
      ],
    );
  }

  Widget checkifThereExistsMovieResultsForAppliedFilterOptions() {
    if (movieDetail.containsKey("movies")) {
      if (movieDetail["movies"].isNotEmpty) {
        if (loadingSymbolRemoveBreadcrumb) {
          return overlayOnMovieResults();
        } else {
          return new MovieFilterFilteredMovies(
            movieDetail: movieDetail,
            userDetail: userDetail,
            userQueue: userQueue,
            authenticationToken: authenticationToken,
            endOfSearchResults: endOfSearchResults,
            changePageNo: changePageNo,
            loadingSymbolFetchMoreResults: loadingSymbolFetchMoreResults,
            theTheaterKey: theTheaterKey,
            showOriginalPosters: showOriginalPosters,
          );
        }
      } else {
        // return displayNoResultsFound();
        return Container(
          color: Constants.appColorL1,
        );
      }
    } else {
      // return displayNoResultsFound();
      return Container(
        color: Constants.appColorL1,
      );
    }
  }

  Widget displayMovieDetails() {
    return new Padding(
      padding: const EdgeInsets.only(top: 1.0),
      child: new Container(
        padding: const EdgeInsets.all(0.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Text('start'),
            checkifAnyFilterHasBeenApplied(),
            // Text('end'),
            checkifLocationSettingNeeded(),
            checkifThereExistsMovieResultsForAppliedFilterOptions(),
          ],
        ),
      ),
    );
  }

  Widget displayLoader() {
    return Scaffold(
      backgroundColor: Constants.appColorFont,
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation:
          theTheaterKey ? null : FloatingActionButtonLocation.centerDocked,
      floatingActionButton:
          theTheaterKey ? null : constructFloatingFilterButton(),
      body: new Center(
        child: ShimmerFilter(),
      ),
      bottomNavigationBar:
          theTheaterKey ? new MovieNavigator() : new MovieNavigatorWithNotch(),
    );
  }

  Widget checkDataReady() {
    try {
      if ((fetchedMovieDetail && fetchedUserPreference) &&
          (fetchedUserQueuePreference && filterDataReady)) {
        printIfDebug("GG Here");
        return displayFilterPage();
      } else {
        return displayLoader();
      }
    } catch (exception) {
      return displayLoader();
    }
  }

  Widget constructFloatingFilterButton() {
    return new Container(
      height: 75.0,
      width: 75.0,
      child: new FittedBox(
        child: new FloatingActionButton(
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.appColorLogo,
            ),
            padding: EdgeInsets.all(8.0),
            width: 50.0,
            height: 50.0,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Expanded(
                  child: new Image.asset(
                    "assests/filter.png",
                    fit: BoxFit.cover,
                    color: Constants.appColorL1,
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: new Container(
                    padding: EdgeInsets.only(top: 2.0),
                    child: new Center(
                      child: new Text(
                        "FILTER",
                        style: new TextStyle(
                          fontStyle: FontStyle.normal,
                          color: Constants.appColorL1,
                          fontSize: 7.0,
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 3,
                      ),
                    ),
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
          onPressed: () {
            // Navigator.of(context).pop();
            Navigator.of(context).push(
              new MaterialPageRoute(
                builder: (BuildContext context) => new MovieFilterMenu(
                  filterStatus: movieDetail["filter_json"],
                  filterVisibilities: movieDetail["filter_json_visibility"],
                  showOriginalPosters: showOriginalPosters,
                  authToken: authenticationToken,
                  userFilterData: userFilterData,
                  filterData: filterData,
                ),
              ),
            );
          },
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }

  Widget displayFilterPage() {
    return new Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: theTheaterKey
          ? FloatingActionButtonLocation.centerFloat
          : FloatingActionButtonLocation.centerDocked,
      floatingActionButton:
          theTheaterKey ? Container() : constructFloatingFilterButton(),
      body: new SingleChildScrollView(
        controller: filterScrollController,
        child: new Container(
          color: Constants.appColorL1,
          child: new SingleChildScrollView(
            child: Column(
              children: <Widget>[
                new MovieFilterCollapsableAppbar(
                  movieDetail: movieDetail,
                  showOriginalPosters: showOriginalPosters,
                ),
                displayMovieDetails(),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar:
          theTheaterKey ? new MovieRouting() : new MovieNavigatorWithNotch(),
    );
  }

  @override
  Widget build(BuildContext context) {
    var myAppliedFilterURL = widget.appliedFilterUrl,
        myVisibleFilters = widget.visibleFilters,
        myVisibilityStatus = widget.visibilityStatus,
        myInTheaterKey = widget.inTheaterKey;

    // showToast('applied filter url: $myAppliedFilterURL', 'long', 'bottom', 1);

    printIfDebug(
      '\n\nhey man! found out where this file is...',
    );
    printIfDebug(
      '\n\nhey there! i am inside build method of ${this.runtimeType}',
    );
    printIfDebug(
      '\n\napplied filter url: $myAppliedFilterURL, \nvisible filters: $myVisibleFilters, \nvisibility status: $myVisibilityStatus, \nin theater key: $myInTheaterKey',
    );
    try {
      return checkDataReady();
    } catch (e) {
      printIfDebug(e);
    }
    return Container();
  }
}
