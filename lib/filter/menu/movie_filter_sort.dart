import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterSort extends StatefulWidget {
  final filterData;
  final updateSortOrder;
  final counter;

  MovieFilterSort({
    this.filterData,
    this.updateSortOrder,
    this.counter,
  });

  @override
  _MovieFilterSortState createState() => new _MovieFilterSortState();
}

class _MovieFilterSortState extends State<MovieFilterSort> {
  Widget checkBoxGroup(int index) {
    var options = <Widget>[];
    var optionsLength = widget.filterData["topics"][index]["options"].length;
    for (int i = 0; i < optionsLength; i++) {
      var option = new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        textDirection: TextDirection.ltr,
        children: [
          new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new Checkbox(
              value: widget.filterData["topics"][index]["options"][i]["status"],
              activeColor: Constants.appColorLogo,
              onChanged: (bool? value) {
                widget.updateSortOrder(value, index, i);
              },
            ),
          ),
          new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new Text(
              widget.filterData["topics"][index]["options"][i]["name"],
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorL2,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      );
      options.add(option);
    }
    return new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        verticalDirection: VerticalDirection.up,
        children: options);
  }

  Widget buildcheckBox(i) {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new InkWell(
        onTap: () {},
        child: new Container(
          padding: const EdgeInsets.all(3.0),
          decoration: new BoxDecoration(
            color: widget.filterData["topics"][widget.counter]["options"][i]
                    ["status"]
                ? Constants.appColorLogo
                : Constants.appColorL2,
            border: new Border.all(
                color: widget.filterData["topics"][widget.counter]["options"][i]
                        ["status"]
                    ? Constants.appColorLogo
                    : Constants.appColorL2,
                width: 2.0),
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: new Center(
            child: new Text(
              widget.filterData["topics"][widget.counter]["options"][i]["name"],
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: widget.filterData["topics"][widget.counter]["options"][i]
                        ["status"]
                    ? Constants.appColorFont
                    : Constants.appColorL1,
              ),
              textScaleFactor: 0.75,
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
        ),
      ),
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 2.5,
        width: 100.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorLogo,
          ),
        ),
      ),
    );
  }

  Widget sortOrderOptions() {
    int numberOfOptions = widget.filterData["sort"]["options"].length;
    return new Container(
      color: Constants.appColorL2,
      padding: const EdgeInsets.all(1.0),
      child: new ListView.builder(
          addAutomaticKeepAlives: false,
          physics: new NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: numberOfOptions,
          itemBuilder: (BuildContext context, int index) {
            return new Container(
              child: ListTile(
                title: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: TextDirection.ltr,
                  children: <Widget>[
                    new Text(
                      widget.filterData["sort"]["options"][index]["name"],
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Constants.appColorFont,
                          fontSize: 12),
                      textAlign: TextAlign.left,
                    ),
                    new Divider(height: 10.0),
                  ],
                ),
                onTap: () {
                  widget.updateSortOrder(index);
                },
              ),
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return sortOrderOptions();
  }
}
