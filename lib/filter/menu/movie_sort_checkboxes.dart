import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSortCheckboxes extends StatefulWidget {
  final filterData;
  final updateSortOrder;
  final resetSort;

  MovieSortCheckboxes({
    this.filterData,
    this.updateSortOrder,
    this.resetSort,
  });

  @override
  _MovieSortCheckboxesState createState() => new _MovieSortCheckboxesState();
}

class _MovieSortCheckboxesState extends State<MovieSortCheckboxes> {
  Widget checkBoxGroup(int index) {
    var options = <Widget>[];
    var optionsLength = widget.filterData["sort"]["options"].length;
    for (int i = 0; i < optionsLength; i++) {
      var option = new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        textDirection: TextDirection.ltr,
        children: [
          new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new Checkbox(
              value: widget.filterData["topics"][index]["options"][i]["status"],
              activeColor: Constants.appColorLogo,
              onChanged: (bool? value) {
                widget.updateSortOrder(value, index, i);
              },
            ),
          ),
          new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new Text(
              widget.filterData["topics"][index]["options"][i]["name"],
              style: new TextStyle(
                  fontWeight: FontWeight.normal, color: Constants.appColorL2),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      );
      options.add(option);
    }
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      verticalDirection: VerticalDirection.up,
      children: options,
    );
  }

  Widget buildcheckBox(i) {
    printIfDebug("filter name ::::" + widget.filterData["sort"].toString());
    String orderName =
        widget.filterData["sort"]["options"][i]["name"].toString();
    orderName = orderName.replaceAll("Komparify Score", "Jini Score");
    orderName = orderName.replaceAll("IMDB Score", "IMDB Rating");

    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new InkWell(
        onTap: () {
          widget.updateSortOrder(i);
        },
        child: new Container(
          padding: const EdgeInsets.all(3.0),
          decoration: new BoxDecoration(
            color:
                // Constants.appColorL2,
                widget.filterData["sort"]["selected"]["filter_id"] ==
                        widget.filterData["sort"]["options"][i]["filter_id"]
                    ? Constants.appColorLogo
                    : Constants.appColorL3,
            border: new Border.all(
              color: widget.filterData["sort"]["selected"]["filter_id"] ==
                      widget.filterData["sort"]["options"][i]["filter_id"]
                  ? Constants.appColorLogo
                  : Constants.appColorL2,
              width: 2.0,
            ),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: new Text(
              orderName.toUpperCase(),
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: widget.filterData["sort"]["selected"]["filter_id"] ==
                        widget.filterData["sort"]["options"][i]["filter_id"]
                    ? Constants.appColorL1
                    : Constants.appColorFont,
                letterSpacing: 1.5,
              ),
              textScaleFactor: 0.65,
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
        ),
      ),
    );
  }

  Widget displayCheckboxes(context, i) {
    printIfDebug("filter name ::::" + widget.filterData["sort"].toString());
    String orderName =
        widget.filterData["sort"]["options"][i]["name"].toString();
    orderName = orderName.replaceAll("Komparify Score", "Jini Score");
    orderName = orderName.replaceAll("IMDB Score", "IMDB Rating");

    return Card(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 5,
        right: 5,
      ),
      color: Constants.appColorL3,
      child: Container(
        color: Constants.appColorL3,
        child: new InkWell(
          onTap: () {
            widget.updateSortOrder(i);
          },
          child: new Container(
            padding: EdgeInsets.all(5.0),
            height: 50.0,
            decoration: new BoxDecoration(
              color: widget.filterData["sort"]["selected"]["filter_id"] ==
                      widget.filterData["sort"]["options"][i]["filter_id"]
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            child: new Center(
              child: new Text(
                orderName.toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: widget.filterData["sort"]["selected"]["filter_id"] ==
                          widget.filterData["sort"]["options"][i]["filter_id"]
                      ? Constants.appColorL1
                      : Constants.appColorFont.withOpacity(0.75),
                ),
                textScaleFactor: 0.5,
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget checkBoxGrid() {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      child: GridView.builder(
          addAutomaticKeepAlives: false,
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          itemCount: widget.filterData["sort"]["options"].length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 3,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            crossAxisCount: (orientation == Orientation.portrait) ? 2 : 12,
          ),
          padding: EdgeInsets.all(
            10,
          ),
          itemBuilder: (BuildContext context1, int i) {
            return displayCheckboxes(context1, i);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: 15,
                left: 10,
                bottom: 10,
              ),
              child: Text(
                "SORT BY",
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 12.0,
                  color: Constants.appColorFont,
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                widget.resetSort();
              },
              child: Container(
                padding: EdgeInsets.only(
                  top: 15,
                  bottom: 10,
                  right: 15,
                ),
                child: Text(
                  "RESET",
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 12.0,
                    color: Constants.appColorLogo.withOpacity(0.5),
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ),
            ),
          ],
        ),
        checkBoxGrid(),
      ],
    );
  }
}
