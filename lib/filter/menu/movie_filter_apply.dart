import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
// import 	'package:streaming_entertainment/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterApply extends StatefulWidget {
  final filterData;
  final resetAll;
  final filterStatus;
  final filterVisibilities;
  final showOriginalPosters;
  final authToken;

  MovieFilterApply(
      {this.filterData,
      this.resetAll,
      this.filterStatus,
      this.filterVisibilities,
      this.showOriginalPosters,
      this.authToken});

  @override
  _MovieFilterApplyState createState() => new _MovieFilterApplyState();
}

class _MovieFilterApplyState extends State<MovieFilterApply> {
  @override
  void initState() {
    super.initState();
    printIfDebug("Received the Applied Filters");
    printIfDebug(widget.filterStatus);
    printIfDebug("Filter Visibility");
    printIfDebug(widget.filterVisibilities);
  }

  findInvisibleKey() {
    String invisibleTag = "";
    bool flag = false;
    var invisibleKeys = [];
    if (widget.filterVisibilities != null) {
      widget.filterVisibilities.forEach((k, v) {
        if (!v) {
          printIfDebug("Invisible Tag");
          printIfDebug(k);
          printIfDebug(v);
          invisibleKeys.add(k);
          flag = true;
        } else {
          printIfDebug("Visible Tags");
          printIfDebug(k);
        }
      });
      if (flag) {
        invisibleKeys.forEach((key) {
          if (widget.filterStatus.containsKey(key))
            visibleFilters[key] = widget.filterStatus[key];
          invisibleTag += key + "=";
          invisibleTag += widget.filterStatus[key].join(',') + "&";
        });
      }
    }
    return invisibleTag;
  }

  String stringFilterData = "";
  var visibleFilters = {};

  updateCachedFilterMenu() async {
    printIfDebug("Update Cached Filter Menu");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    printIfDebug("\n\nNeed to convert Data to String");
    String temp = json.encode(widget.filterData);
    setState(() {
      prefs.setString('stringFilterData', temp);
    });
    printIfDebug("\n\nfilters that are read from Cache: $stringFilterData");
  }

  String checkSortOrder() {
    String sortUrl = "";
    if (widget.filterData.containsKey("sort")) {
      if (widget.filterData["sort"].isNotEmpty) {
        sortUrl = widget.filterData["sort"]["key"] +
            "=" +
            widget.filterData["sort"]["selected"]["filter_id"];
        if (widget.filterData["sort"]["selected"]["filter_id"] ==
            widget.filterData["sort"]["options"][0]["filter_id"]) {
          printIfDebug("Sort Order is equal to the default, dont display");
        } else {
          visibleFilters[widget.filterData["sort"]["key"]] = [
            widget.filterData["sort"]["selected"]["filter_id"]
          ];
        }
      }
    }
    return sortUrl;
  }

  String getRequestUrlBuilder() {
    String getInvisibleTag = findInvisibleKey();
    String getUrl = "";
    getUrl += getInvisibleTag;
    printIfDebug("The invisible included");
    printIfDebug(getUrl);
    for (int index = 0; index < widget.filterData["topics"].length; index++) {
      if (widget.filterData["topics"][index]["group"] == "clickableImages") {
        printIfDebug("Have caught clickable images");
        bool flag = true;
        bool selected = false;
        List<String> options = [];
        for (int i = 0;
            i < widget.filterData["topics"][index]["options"].length;
            i++) {
          if (widget.filterData["topics"][index]["options"][i]["status"]) {
            selected = true;
            if (flag) {
              getUrl += widget.filterData["topics"][index]["key"] + "=";
            }
            flag = false;
            options.add(
                widget.filterData["topics"][index]["options"][i]["filter_id"]);
          }
        }
        getUrl += options.join(',');
        if (selected) {
          visibleFilters[widget.filterData["topics"][index]["key"]] = options;
          getUrl += "&";
        }
      } else if (widget.filterData["topics"][index]["group"] == "checkBoxes") {
        bool flag = true;
        bool selected = false;
        List<String> options = [];
        for (int i = 0;
            i < widget.filterData["topics"][index]["options"].length;
            i++) {
          if (widget.filterData["topics"][index]["options"][i]["status"]) {
            selected = true;
            if (flag) {
              getUrl += widget.filterData["topics"][index]["key"] + "=";
            }
            flag = false;
            options.add(
                widget.filterData["topics"][index]["options"][i]["filter_id"]);
          }
        }
        getUrl += options.join(',');
        if (selected) {
          visibleFilters[widget.filterData["topics"][index]["key"]] = options;
          getUrl += "&";
        }
      } else if (widget.filterData["topics"][index]["group"] == "slider") {
        getUrl += (widget.filterData["topics"][index]["key_1"] + "=");
        getUrl += (widget.filterData["topics"][index]["features"]["rangeLower"]
                .round())
            .toString();
        if (widget.filterData["topics"][index]["features"]["rangeLower"] ==
            widget.filterData["topics"][index]["features"]["min"]) {
          printIfDebug("Range Lower is equal to the default, dont display");
        } else {
          visibleFilters[widget.filterData["topics"][index]["key_1"]] = [
            (widget.filterData["topics"][index]["features"]["rangeLower"]
                    .round())
                .toString()
          ];
        }
        getUrl += "&";
        getUrl += (widget.filterData["topics"][index]["key_2"] + "=");
        getUrl += (widget.filterData["topics"][index]["features"]["rangeUpper"]
                .round())
            .toString();
        if (widget.filterData["topics"][index]["features"]["rangeUpper"] ==
            widget.filterData["topics"][index]["features"]["max"]) {
          printIfDebug("Range Upper is equal to the default, dont display");
        } else {
          visibleFilters[widget.filterData["topics"][index]["key_2"]] = [
            (widget.filterData["topics"][index]["features"]["rangeUpper"]
                    .round())
                .toString()
          ];
        }
        getUrl += "&";
      } else {
        continue;
      }
    }
    String sortUrl = checkSortOrder();
    if (sortUrl.isNotEmpty) {
      getUrl += sortUrl;
    } else {
      int urlLength = getUrl.length - 1;
      getUrl = getUrl.substring(0, urlLength);
    }
    return getUrl;
  }

  Widget clearAllButton() {
    return new InkWell(
      child: new Container(
        height: 50.0,
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Card(
                elevation: 5,
                color: Constants.appColorL3,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                child: Container(
                  decoration: new BoxDecoration(
                    color: Constants.appColorL3,
                    borderRadius: new BorderRadius.circular(5.0),
                  ),
                  padding: EdgeInsets.only(
                    left: 50,
                    right: 50,
                    top: 10,
                    bottom: 10,
                  ),
                  child: Text(
                    'CANCEL',
                    style: TextStyle(
                      color: Constants.appColorFont,
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              // Container(
              //   padding: const EdgeInsets.all(1.0),
              //   child: new Text(
              //     "CANCEL",
              //     textAlign: TextAlign.right,
              //     style: new TextStyle(
              //       fontWeight: FontWeight.normal,
              //       color: Constants.appColorFont,
              //       fontSize: 25,
              //       letterSpacing: 1,
              //     ),
              //     textScaleFactor: 0.75,
              //   ),
              // ),
            ]),
      ),
      onTap: () {
        printIfDebug("Clear All Fields");
        widget.resetAll();
        Navigator.of(context).pop();
      },
    );
  }

  Widget applyButton() {
    return new InkWell(
      child: new Container(
        height: 50.0,
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Card(
                elevation: 5,
                color: Constants.appColorLogo,
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                child: Container(
                  padding: EdgeInsets.only(
                    left: 50,
                    right: 50,
                    top: 10,
                    bottom: 10,
                  ),
                  decoration: new BoxDecoration(
                    color: Constants.appColorLogo,
                    borderRadius: new BorderRadius.circular(5.0),
                  ),
                    child: Text(
                      'APPLY',
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                ),
              ),
              // new Container(
              //   padding: const EdgeInsets.all(1.0),
              //   child: new Text(
              //     "APPLY",
              //     textAlign: TextAlign.right,
              //     style: new TextStyle(
              //       fontWeight: FontWeight.normal,
              //       color: const Color(
              //         0xFFFFFFFFF,
              //       ),
              //       fontSize: 25,
              //       letterSpacing: 1,
              //     ),
              //     textScaleFactor: 0.75,
              //   ),
              // ),
            ]),
      ),
      onTap: () {
        printIfDebug("\n\nbefore navigating to filter page, applied filters");
        updateCachedFilterMenu();
        String appliedFilterUrl =
            "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
                widget.authToken +
                "&";
        appliedFilterUrl += getRequestUrlBuilder();
        printIfDebug(
            "\n\nFilters Applied, and the appliedFilterUrl: $appliedFilterUrl \nand its type is: ${appliedFilterUrl.runtimeType}");
        printIfDebug(
            "\n\nVisible Tag Constructed Json and the visibleFilters: $visibleFilters \nand its type is: ${visibleFilters.runtimeType}");

        var myFilterVisibilities = widget.filterVisibilities;
        printIfDebug(
          '\n\nmy filter visibilities: $myFilterVisibilities and its type: ${myFilterVisibilities.runtimeType}',
        );

        // visibleFilters.forEach((key, value) {
        //   printIfDebug(
        //       '\n\nkey $key has value $value of data type ${value.runtimeType}');
        // });

        Navigator.of(context).pop();
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => new MovieFilterPage(
              appliedFilterUrl: appliedFilterUrl,
              visibleFilters: visibleFilters,
              visibilityStatus: widget.filterVisibilities,
              showOriginalPosters: widget.showOriginalPosters,
              inTheaterKey: false,
            ),
          ),
        );
      },
    );
  }

  Widget applyOrClear() {
    return new BottomAppBar(
      color: Constants.appColorL2,
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            clearAllButton(),
            applyButton(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return applyOrClear();
  }
}
