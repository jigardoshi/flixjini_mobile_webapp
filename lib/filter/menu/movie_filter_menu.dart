import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
// import  'package:flutter_range_slider/flutter_range_slider.dart';
import 'package:http/http.dart' as http;
// import  'package:streaming_entertainment/filter/movie_filter_page.dart';
// import  'dart:collection';
import 'package:flixjini_webapp/filter/menu/movie_filter_structure.dart';
// import  'package:streaming_entertainment/filter/menu/movie_filter_selected.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_apply.dart';
// import	'package:streaming_entertainment/navigation/movie_navigator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterMenu extends StatefulWidget {
  MovieFilterMenu({
    Key? key,
    this.filterStatus,
    this.filterVisibilities,
    this.showOriginalPosters,
    this.authToken,
    this.userFilterData,
    this.filterData,
  }) : super(key: key);

  final filterStatus;
  final filterVisibilities;
  final showOriginalPosters;
  final authToken;
  final userFilterData;
  final filterData;

  @override
  _MovieFilterMenuState createState() => new _MovieFilterMenuState();
}

class _MovieFilterMenuState extends State<MovieFilterMenu> {
  final sortKey = new GlobalKey();
  ScrollController menuController = new ScrollController();
  final filterKey = new GlobalKey();
  ScrollController filterMenuController = new ScrollController();

  var filterData = {}, userFilterData = {};
  String stringFilterData = "";
  bool filterDataReady = false;
  bool filterFlag = false;
  String appliedFilters = "";
  var jumperMenuStatus = {"filter": false, "sort": true};
  String filterMenuOptionsUrl =
      "https://api.flixjini.com/entertainment/filter_options.json";

  void initState() {
    super.initState();
    printIfDebug("Filter Json");
    printIfDebug(widget.filterStatus);
    filterMenuController.addListener(filterMenuControllerListener);
    loadFilterMenu();
  }

  filterMenuControllerListener() {
    printIfDebug(filterMenuController.offset);
    if (filterMenuController.offset == 0) {
      printIfDebug("Reached Start of Grid");
    } else {
      printIfDebug("Moved Out");
    }
  }

  checkIfAnySelected() {
    for (int i = 0; i < filterData["topics"].length; i++) {
      if (filterData["topics"][i]["group"] == "clickableImages") {
        bool flag = true;
        for (int index = 0;
            index < filterData["topics"][i]["options"].length;
            index++) {
          if (filterData["topics"][i]["options"][index]["status"]) {
            flag = false;
            updateShadeAllOptions(i, false);
            printIfDebug("Category Checking");
            printIfDebug(filterData["topics"][i]["heading"]);
          }
          if (flag) {
            updateShadeAllOptions(i, true);
          }
        }
      }
    }
  }

  loadFilterMenu() async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // setState(() {
    //   stringFilterData = (prefs.getString('stringFilterData') ?? "");
    // });
    if (widget.filterData == null || widget.userFilterData == null) {
      getFilterMenu();
    } else {
      // printIfDebug("Filter Options already present");
      // var data = json.decode(stringFilterData);
      // data = updateMenu(data);
      // printIfDebug("Need to convert Data to String");
      // String temp = json.encode(data);
      // setState(() {
      //   filterData = data;
      //   filterDataReady = true;
      //   prefs.setString('stringFilterData', temp);
      // });
      var data = widget.filterData;
      data = addClickableStatus(data);
      data = updateMenu(data);
      setState(() {
        filterData = data;
        filterDataReady = true;

        userFilterData = widget.userFilterData;
      });
      // printIfDebug("Reading filters from Cache");
      // printIfDebug(filterData);
    }
    checkIfAnySelected();
  }

  setSelectedFilterStatus(int i) {
    printIfDebug("set filter status");
    List filterList = userFilterData.keys.toList();
    // for (int i = 0; i < userFilterData["filters"].length; i++) {
    //  filterData[]
    // }
    printIfDebug(filterList.toString());
    if (filterList.contains(filterData["topics"][i]["key"])) {
      printIfDebug(filterData["topics"][i]["key"]);

      for (int j = 0; j < filterData["topics"][i]["options"].length; j++) {
        if (userFilterData[filterData["topics"][i]["key"]]
            .contains(filterData["topics"][i]["options"][j]["filter_id"])) {
          printIfDebug(filterData["topics"][i]["options"][j]["filter_id"]);
          setState(() {
            this.filterData["topics"][i]["options"][j]["status"] = true;
          });
        } else {
          setState(() {
            this.filterData["topics"][i]["options"][j]["status"] = false;
          });
        }
      }
    }
    checkIfAnySelected();
  }

  clearAllFields(data) {
    if (data.containsKey("sort")) {
      if (data["sort"].isNotEmpty) {
        data["sort"]["selected"]["name"] = "New Releases";
        data["sort"]["selected"]["filter_id"] = "newrelease";
      }
    }
    data["topics"].forEach((topic) {
      if (topic.containsKey("options")) {
        topic["options"].forEach((option) {
          option["status"] = false;
        });
      }
      if (topic.containsKey("features")) {
        topic["features"]["rangeUpper"] =
            double.parse(topic["features"]["max"].toString());
        topic["features"]["rangeLower"] =
            double.parse(topic["features"]["min"].toString());
        printIfDebug("Features");
      }
    });
  }

  updateMenu(data) {
    for (int i = 0; i < data["topics"].length; i++) {
      if (data["topics"][i]["group"] == "slider") {
        data["topics"][i]["features"]["rangeUpper"] = double.parse(
            data["topics"][i]["features"]["rangeUpper"].toString());
        data["topics"][i]["features"]["rangeLower"] = double.parse(
            data["topics"][i]["features"]["rangeLower"].toString());
        data["topics"][i]["features"]["max"] =
            double.parse(data["topics"][i]["features"]["max"].toString());
        data["topics"][i]["features"]["min"] =
            double.parse(data["topics"][i]["features"]["min"].toString());
        // printIfDebug("RangeUpper");
        // printIfDebug(data["topics"][i]["features"]["rangeUpper"]);
      }
    }
    // printIfDebug("Parsing Done");
    clearAllFields(data);
    if (widget.filterStatus != null) {
      // printIfDebug("Matched Keys");
      // printIfDebug(widget.filterStatus);
      widget.filterStatus.forEach((k, v) {
        if (k == "sortorder") {
          // printIfDebug("Yes Sort Order");
          // printIfDebug(k);
          // printIfDebug(v);
          if (data.containsKey("sort")) {
            for (int i = 0; i < data["sort"]["options"].length; i++) {
              if (data["sort"]["options"][i]["filter_id"] == v[0]) {
                data["sort"]["selected"] = data["sort"]["options"][i];
                // printIfDebug(data["sort"]["selected"]);
              }
            }
          }
        }

        for (int i = 0; i < data["topics"].length; i++) {
          if (data["topics"][i].containsKey("key")) {
            if (data["topics"][i]["key"] == k) {
              // printIfDebug(data["topics"][i]["key"]);
              v.forEach((value) {
                for (int j = 0; j < data["topics"][i]["options"].length; j++) {
                  // printIfDebug(data["topics"][i]["options"][j]["filter_id"]);
                  if (data["topics"][i]["options"][j]["filter_id"] == value) {
                    // printIfDebug(data["topics"][i]["options"][j]["filter_id"]);
                    data["topics"][i]["options"][j]["status"] = true;
                    break;
                  }
                }
              });
              break;
            }
          } else if (data["topics"][i].containsKey("key_1") &&
              data["topics"][i].containsKey("key_2")) {
            if (data["topics"][i]["key_1"] == k) {
              // printIfDebug(data["topics"][i]["key_1"]);
              data["topics"][i]["features"]["rangeLower"] = double.parse(v[0]);
              // printIfDebug(data["topics"][i]["features"]["rangeLower"]);
            } else if (data["topics"][i]["key_2"] == k) {
              // printIfDebug(data["topics"][i]["key_2"]);
              data["topics"][i]["features"]["rangeUpper"] = double.parse(v[0]);
              // printIfDebug(data["topics"][i]["features"]["rangeUpper"]);
            }
          } else {
            // printIfDebug("GG");
          }
        }
      });
    }
    return data;
  }

  addClickableStatus(var data) {
    for (int i = 0; i < data["topics"].length; i++) {
      if (data["topics"][i]["group"] == "clickableImages") {
        data["topics"][i]["status"] = true;
        // printIfDebug("Added Now");
        // printIfDebug(data["topics"][i]["status"]);
      }
    }
    return data;
  }

  getFilterMenu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String rootUrl = filterMenuOptionsUrl + '?';
    rootUrl = widget.authToken != null && widget.authToken.length > 0
        ? rootUrl + "jwt_token=" + widget.authToken + "&"
        : rootUrl;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        printIfDebug(responseBody);
        var data = json.decode(responseBody);
        data = addClickableStatus(data);
        data = updateMenu(data);
        printIfDebug("Need to convert Data to String");
        String temp = json.encode(data);
        this.filterData = data;
        setState(() {
          filterData = this.filterData;
          filterDataReady = true;
          userFilterData = filterData["filters"];
        });
        // printIfDebug("Data Fetched");
        // printIfDebug(filterData);
        setState(() {
          stringFilterData = (prefs.getString('stringFilterData') ?? "");
          prefs.setString('stringFilterData', temp);
        });
        // printIfDebug("Saving Filters to Cache");
        // printIfDebug(stringFilterData);
      } else {
        this.filterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.filterData["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  void setFilterFlag(String url) {
    setState(() {
      filterFlag = true;
      appliedFilters = url;
    });
  }

  void updateupdateCheckbox(int index, int i) {
    setState(() {
      filterData["topics"][index]["options"][i]["status"] =
          filterData["topics"][index]["options"][i]["status"] ? false : true;
    });
    // printIfDebug("Updated Checkbox Status");
    // printIfDebug(filterData["topics"][index]["options"][i]["status"]);
  }

  void updateMinMaxRange(
      double newLowerValue, double newUpperValue, int index) {
    setState(() {
      filterData["topics"][index]["features"]["rangeLower"] = newLowerValue;
      filterData["topics"][index]["features"]["rangeUpper"] = newUpperValue;
    });
    printIfDebug("Updated Min/ Max Values");
  }

  void updateClickableImages(int index, int j) {
    setState(() {
      filterData["topics"][index]["options"][j]["status"] =
          filterData["topics"][index]["options"][j]["status"] ? false : true;
    });
    printIfDebug("Updated Clickable Image Status");
  }

  void updateSortOrder(int i) {
    setState(() {
      filterData["sort"]["selected"]["name"] =
          filterData["sort"]["options"][i]["name"];
      filterData["sort"]["selected"]["filter_id"] =
          filterData["sort"]["options"][i]["filter_id"];
    });
    printIfDebug("Updated Sort Order");
  }

  // Used to Reset both Clickable Images and Checkbox Status
  void resetCheckboxOptions(int i, int j) {
    setState(() {
      filterData["topics"][i]["options"][j]["status"] = false;
    });
    printIfDebug("Checkbox values have been Reset");
  }

  void resetRangeOptions(int i) {
    setState(() {
      filterData["topics"][i]["features"]["rangeLower"] =
          filterData["topics"][i]["features"]["min"];
      filterData["topics"][i]["features"]["rangeUpper"] =
          filterData["topics"][i]["features"]["max"];
    });
    printIfDebug("Range Options have been Reset");
  }

  void resetSortOptions() {
    setState(() {
      filterData["sort"]["selected"]["name"] = "New Releases";
      filterData["sort"]["selected"]["filter_id"] = "newrelease";
    });
    printIfDebug("Reset Sort Order");
  }

  void resetAll() {
    printIfDebug("Reset");
    for (int index = 0; index < filterData["topics"].length; index++) {
      if (filterData["topics"][index].containsKey("options")) {
        int categoryLength = filterData["topics"][index]["options"].length;
        for (int j = 0; j < categoryLength; j++) {
          resetCheckboxOptions(index, j);
        }
        if (filterData["topics"][index]["group"] == "clickableImages") {
          filterData["topics"][index]["status"] = true;
          updateShadeAllOptions(index, true);
        }
      } else if (filterData["topics"][index].containsKey("features")) {
        resetRangeOptions(index);
        // printIfDebug("On Touch");
        // printIfDebug(filterData["topics"][index]["heading"]);
      } else {
        continue;
      }
    }
  }

  updateShadeAllOptions(int index, bool value) {
    setState(() {
      filterData["topics"][index]["status"] = value;
    });
  }

  Widget displayFilterMenu() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      textDirection: TextDirection.ltr,
      children: [
        new MovieFilterStructure(
          filterData: filterData,
          resetCheckboxOptions: resetCheckboxOptions,
          resetRangeOptions: resetRangeOptions,
          resetSortOptions: resetSortOptions,
          updateupdateCheckbox: updateupdateCheckbox,
          updateClickableImages: updateClickableImages,
          updateMinMaxRange: updateMinMaxRange,
          updateSortOrder: updateSortOrder,
          sortKey: sortKey,
          filterKey: filterKey,
          filterMenuController: filterMenuController,
 
          updateShadeAllOptions: updateShadeAllOptions,
          checkIfAnySelected: checkIfAnySelected,
          setSelectedFilterStatus: setSelectedFilterStatus,
        ),
        new Container(),
      ],
    );
  }

  Widget displayLoader() {
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight,
      // margin: EdgeInsets.only(top: 200),
      child: new Center(
        child: new CircularProgressIndicator(
          backgroundColor: Constants.appColorFont,
          valueColor: new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
        ),
      ),
    );
  }

  Widget checkDataReady() {
    if (filterDataReady) {
      return displayFilterMenu();
    } else {
      return displayLoader();
    }
  }

  void setjumperMenuStatus(String selected, String unselected) {
    setState(() {
      jumperMenuStatus[selected] = true;
      jumperMenuStatus[unselected] = false;
    });
  }

  Widget appHeaderBar() {
    return new AppBar(
      titleSpacing: 0.0,
      automaticallyImplyLeading: false,
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      centerTitle: true,
      title: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Expanded(
              child: new InkWell(
                onTap: () {
                  setjumperMenuStatus("sort", "filter");
                  Scrollable.ensureVisible(
                    sortKey.currentContext!,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeOut,
                  );
                },
                child: new Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: new Image.asset(
                                  "assests/sort_by.png",
                                  width: 15.0,
                                  height: 15.0,
                                ),
                              ),
                              new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: new Center(
                                  child: new Text("Sort By",
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                          fontWeight: FontWeight.normal,
                                          color: Constants.appColorFont),
                                      textAlign: TextAlign.center,
                                      maxLines: 1),
                                ),
                              ),
                            ],
                          ),
                          flex: 6),
                      new Expanded(
                          child: new Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              border: new Border(
                                bottom: BorderSide(
                                    color: jumperMenuStatus["sort"]!
                                        ? Constants.appColorLogo
                                        : Constants.appColorFont
                                            .withOpacity(0.9),
                                    width: 4.0),
                              ),
                            ),
                          ),
                          flex: 1),
                    ],
                  ),
                ),
              ),
              flex: 1),
          new Expanded(
              child: new InkWell(
                onTap: () {
                  setjumperMenuStatus("filter", "sort");
                  Scrollable.ensureVisible(
                    filterKey.currentContext!,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeOut,
                  );
                },
                child: new Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: new Image.asset(
                                  "assests/filter_by.png",
                                  width: 15.0,
                                  height: 15.0,
                                ),
                              ),
                              new Container(
                                padding: const EdgeInsets.all(5.0),
                                child: new Center(
                                  child: new Text("Filter By",
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.normal,
                                        color: Constants.appColorFont,
                                      ),
                                      textAlign: TextAlign.center,
                                      maxLines: 1),
                                ),
                              ),
                            ],
                          ),
                          flex: 6),
                      new Expanded(
                          child: new Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Constants.appColorFont.withOpacity(0.9),
                              border: new Border(
                                bottom: BorderSide(
                                    color: jumperMenuStatus["filter"]!
                                        ? Constants.appColorLogo
                                        : Constants.appColorFont
                                            .withOpacity(0.9),
                                    width: 4.0),
                              ),
                            ),
                          ),
                          flex: 1),
                    ],
                  ),
                ),
              ),
              flex: 1),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    try {
      printIfDebug(
          '\n\nhey there! i am inside build method of ${this.runtimeType}');
      return new Scaffold(
        backgroundColor: Constants.appColorL1,
        resizeToAvoidBottomInset: false,
        // appBar: appHeaderBar(),
        body: new Container(
          padding: const EdgeInsets.only(top: 30.0, bottom: 10.0),
          decoration: new BoxDecoration(color: Constants.appColorL1),
          child: new SingleChildScrollView(
            controller: menuController,
            scrollDirection: Axis.vertical,
            child: checkDataReady(),
          ),
        ),
        bottomNavigationBar: new MovieFilterApply(
          filterData: filterData,
          resetAll: resetAll,
          filterStatus: widget.filterStatus,
          filterVisibilities: widget.filterVisibilities,
          showOriginalPosters: widget.showOriginalPosters,
          authToken: widget.authToken,
        ),
      );
    } catch (e) {
      printIfDebug(
          '\n\nexception caught in movie filter menu page, and exception is: $e');
      return Container();
    }
  }
}
