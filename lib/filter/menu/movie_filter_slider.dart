import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:flixjini_webapp/common/constants.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class MovieFilterSlider extends StatefulWidget {
  final filterData;
  final updateMinMaxRange;
  final counter;
  final resetRangeOptions;

  MovieFilterSlider({
    this.filterData,
    this.updateMinMaxRange,
    this.counter,
    this.resetRangeOptions,
  });

  @override
  _MovieFilterSliderState createState() => new _MovieFilterSliderState();
}

class _MovieFilterSliderState extends State<MovieFilterSlider> {
  @override
  void initState() {
    super.initState();
  }

  Widget sliderGroup(int index) {
    //
    var rangeLower =
        widget.filterData["topics"][index]["features"]["rangeLower"];
    var rangeUpper =
        widget.filterData["topics"][index]["features"]["rangeUpper"];
    var min = widget.filterData["topics"][index]["features"]["min"];
    var max = widget.filterData["topics"][index]["features"]["min"];

    printIfDebug('\n\nrange lower: $rangeLower');
    printIfDebug('\n\nrange upper: $rangeUpper');

    printIfDebug('\n\nmin: $min');
    printIfDebug('\n\nmax: $max');

    //
    return new Container(
      color: Constants.appColorL2,
      height: 130.0,
      padding: const EdgeInsets.all(5.0),
      child: Center(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          textDirection: TextDirection.ltr,
          children: [
            new Expanded(
              child: new Container(
                padding: const EdgeInsets.all(0.0),
                child: new Text(
                  ((widget.filterData["topics"][index]["features"]
                              ["rangeLower"])
                          .round())
                      .toString(),
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 12.0,
                  ),
                  textAlign: TextAlign.right,
                ),
              ),
              flex: 1,
            ),
            new Expanded(
              child: new Container(
                child: new SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                    overlayColor: Constants.appColorLogo,
                    activeTickMarkColor: Constants.appColorLogo,
                    activeTrackColor: Constants.appColorLogo,
                    inactiveTrackColor: Constants.appColorDivider,
                    thumbColor: Constants.appColorLogo,
                    valueIndicatorColor: Constants.appColorLogo,
                  ),
                  child: SfRangeSlider(
                    min: //0,
                        widget.filterData["topics"][index]["features"]["min"] *
                            1.0,
                    max: //10,
                        widget.filterData["topics"][index]["features"]["max"] *
                            1.0,
                    values: SfRangeValues(
                        widget.filterData["topics"][index]["features"]
                                ["rangeLower"] *
                            1.0,
                        widget.filterData["topics"][index]["features"]
                                ["rangeUpper"] *
                            1.0),
                    onChanged: (SfRangeValues value) {
                      widget.updateMinMaxRange(value.start, value.end, index);
                    },
                  ),
                  // new frs.RangeSlider(
                  //   min: // 0,
                  //       widget.filterData["topics"][index]["features"]["min"],
                  //   max: // 10,
                  //       widget.filterData["topics"][index]["features"]["max"],
                  //   lowerValue: // 0,
                  //       widget.filterData["topics"][index]["features"]
                  //           ["rangeLower"],
                  //   upperValue: // 10,
                  //       widget.filterData["topics"][index]["features"]
                  //           ["rangeUpper"],
                  //   // divisions: (widget.filterData["topics"][index]["features"]
                  //   //             ["max"] -
                  //   //         widget.filterData["topics"][index]["features"]
                  //   //             ["min"])
                  //   //     .round(),
                  //   // showValueIndicator: true,
                  //   valueIndicatorMaxDecimals: 0,
                  //   onChanged: (double newLowerValue, double newUpperValue) {
                  //     widget.updateMinMaxRange(
                  //         newLowerValue, newUpperValue, index);
                  //   },
                  //   onChangeStart:
                  //       (double startLowerValue, double startUpperValue) {
                  //     printIfDebug(
                  //         'Started with values: $startLowerValue and $startUpperValue');
                  //   },
                  //   onChangeEnd: (double newLowerValue, double newUpperValue) {
                  //     printIfDebug(
                  //         'Ended with values: $newLowerValue and $newUpperValue');
                  //   },
                  // ),
                ),
              ),
              flex: 4,
            ),
            new Expanded(
              child: new Container(
                padding: const EdgeInsets.all(0.0),
                child: new Text(
                  ((widget.filterData["topics"][index]["features"]
                              ["rangeUpper"])
                          .round())
                      .toString(),
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 12.0,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              flex: 1,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    try {
      printIfDebug(
          '\n\nhey there! i am inside build method of ${this.runtimeType}');
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10,
                  bottom: 10,
                ),
                child: Text(
                  widget.filterData["topics"][widget.counter]["heading"]
                      .toString()
                      .toUpperCase(),
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14.0,
                    color:
                        // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                        //     ? Constants.appColorLogo
                        //     :
                        Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  widget.resetRangeOptions(widget.counter);
                },
                child: Container(
                  padding: EdgeInsets.only(
                    top: 15,
                    bottom: 10,
                    right: 10,
                  ),
                  child: Text(
                    "RESET",
                    style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color:
                          // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                          //     ? Constants.appColorLogo
                          //     :
                          Constants.appColorLogo.withOpacity(0.5),
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
                ),
              ),
            ],
          ),
          sliderGroup(widget.counter),
        ],
      );
    } catch (e) {
      printIfDebug('\n\nin movie filter slider page, exception caught: $e');
      return Container();
    }
  }
}
