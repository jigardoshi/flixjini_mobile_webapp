import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';

class MovieFilterImages extends StatefulWidget {
  final filterData;
  final updateClickableImages;
  final counter;
  final updateShadeAllOptions;
  final checkIfAnySelected;
  final setSelectedFilterStatus;
  final resetCheckboxOptions;

  MovieFilterImages({
    this.filterData,
    this.updateClickableImages,
    this.counter,
    this.updateShadeAllOptions,
    this.checkIfAnySelected,
    this.setSelectedFilterStatus,
    this.resetCheckboxOptions,
  });

  @override
  _MovieFilterImagesState createState() => new _MovieFilterImagesState();
}

class _MovieFilterImagesState extends State<MovieFilterImages> {
  void initState() {
    super.initState();
  }

  void clickedOnStreamingSources(i) {
    printIfDebug("Image Id that has been clicked");
    printIfDebug(
        widget.filterData["topics"][widget.counter]["options"][i]["name"]);
    widget.updateClickableImages(widget.counter, i);
    printIfDebug("Current Status");
    printIfDebug(
        widget.filterData["topics"][widget.counter]["options"][i]["name"]);
    printIfDebug(
        widget.filterData["topics"][widget.counter]["options"][i]["status"]);
  }

  Widget clickableImagesGroup(index) {
    int maxColumnPerRow = 8;
    int numberOfSources = widget.filterData["topics"][index]["options"].length;
    int numberOfRows = (numberOfSources / maxColumnPerRow).ceil();
    int end = maxColumnPerRow;
    int pos = 0;
    List<Widget> rows = numberOfRows as List<Widget>;
    for (int i = 0; i < numberOfRows; i++) {
      if (i == (numberOfRows - 1)) {
        end = numberOfSources;
      }
      int k = 0;
      List<Widget> cols = (end - pos) as List<Widget>;
      for (int j = pos; j < end; j++) {
        cols[k] = new Padding(
          padding: const EdgeInsets.all(4.0),
          child: new GestureDetector(
            onTap: () {
              printIfDebug("Image Id that has been clicked");
              printIfDebug(
                  widget.filterData["topics"][index]["options"][j]["name"]);
              widget.updateClickableImages(index, j);
              printIfDebug("Current Status");
              printIfDebug(
                  widget.filterData["topics"][index]["options"][j]["name"]);
              printIfDebug(
                  widget.filterData["topics"][index]["options"][j]["status"]);
            },
            child: new Opacity(
              opacity: widget.filterData["topics"][index]["options"][j]
                      ["status"]
                  ? 1.0
                  : 0.5,
              child: new Image(
                image: NetworkImage(
                  widget.filterData["topics"][index]["options"][j]["image"],
                  // useDiskCache: false,
                ),
                fit: BoxFit.fill,
                gaplessPlayback: true,
              ),
            ),
          ),
        );
        k = k + 1;
      }
      pos = end;
      end = end + maxColumnPerRow;
      rows[i] = new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: cols,
      );
      printIfDebug(rows[i]);
    }
    return new Container(
      padding: const EdgeInsets.all(4.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: TextDirection.ltr,
        verticalDirection: VerticalDirection.down,
        children: rows,
      ),
    );
  }

  double basedOnshadeAllOptions(int i) {
    if (widget.filterData["topics"][widget.counter]["status"]) {
      return 0.0;
    } else {
      return widget.filterData["topics"][widget.counter]["options"][i]["status"]
          ? 0.0
          : 0.6;
    }
  }

  uponToogle(int i) {
    printIfDebug("Toogled");
    if (widget.filterData["topics"][widget.counter]["options"][i]["status"]) {
      widget.updateShadeAllOptions(widget.counter, false);
    }
    widget.checkIfAnySelected();
  }

  Widget buildGenre(i) {
    double toggle = basedOnshadeAllOptions(i);
    return Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(1.0),
      width: 40.0,
      height: 40.0,
      child: new GestureDetector(
        onTap: () {
          clickedOnStreamingSources(i);
          uponToogle(i);
        },
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Expanded(
              child: new Card(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                ),
                child: new Stack(
                  children: <Widget>[
                    new ClipRRect(
                      borderRadius: new BorderRadius.circular(4.0),
                      child: new Container(
                        color: Constants.appColorFont,
                        padding: const EdgeInsets.all(10.0),
                        child: new Image(
                          image: NetworkImage(
                            widget.filterData["topics"][widget.counter]
                                ["options"][i]["image"],
                            // useDiskCache: false,
                          ),
                          fit: BoxFit.fill,
                          gaplessPlayback: true,
                        ),
                      ),
                    ),
                    new Positioned.fill(
                      child: new ClipRRect(
                        borderRadius: new BorderRadius.circular(4.0),
                        child: new Container(
                          color: Constants.appColorL1.withOpacity(toggle),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              flex: 3,
            ),
            new Expanded(
              child: new Container(
                child: new Center(
                  child: new Text(
                    widget.filterData["topics"][widget.counter]["options"][i]
                        ["name"],
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorL3,
                        fontSize: 10.0),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
              ),
              flex: 1,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildStreamingScource(i) {
    double toggle = basedOnshadeAllOptions(i);
    return Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(1.0),
      width: 35.0,
      height: 35.0,
      child: new Card(
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(4.0),
        ),
        child: new GestureDetector(
          onTap: () {
            clickedOnStreamingSources(i);
            uponToogle(i);
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(4.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: new Image(
                    image: NetworkImage(
                      widget.filterData["topics"][widget.counter]["options"][i]
                          ["image"],
                      // useDiskCache: false,
                    ),
                    fit: BoxFit.fill,
                    gaplessPlayback: true,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(4.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool checkIfAnyAreSelected(index) {
    bool flag = true;
    for (int i = 0;
        i < widget.filterData["topics"][index]["options"].length;
        i++) {
      if (widget.filterData["topics"][index]["options"][i]["status"]) {
        flag = false;
        printIfDebug("Category Checking");
        printIfDebug(widget.filterData["topics"][index]["heading"]);
        break;
      }
    }
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  checkIfAnySelected() {
    for (int i = 0;
        i < widget.filterData["topics"][widget.counter]["options"].length;
        i++) {
      if (widget.filterData["topics"][widget.counter]["options"][i]["status"]) {
        widget.updateShadeAllOptions(widget.counter, false);
        break;
      }
    }
  }

  checkIfOptionTitleNeeded(int i) {
    if (widget.filterData["topics"][widget.counter]["heading"] == "Genre") {
      return buildGenre(i);
    } else {
      return buildStreamingScource(i);
    }
  }

  checkIfTitleNeeded() {
    var setting = {};
    if (widget.filterData["topics"][widget.counter]["heading"] == "Genre") {
      setting["numberOfOptionsPerRow"] = 4;
      setting["blockSize"] = 73.0;
      setting["childAspectRatio"] = 1.0;
      setting["crossAxisCount"] = 4;
      setting["padding"] = 1.0;
    } else {
      setting["numberOfOptionsPerRow"] = 5;
      setting["blockSize"] = 67.0;
      setting["childAspectRatio"] = 1.0;
      setting["crossAxisCount"] = 5;
      setting["padding"] = 1.0;
    }
    return setting;
  }

  Widget clickableImagesGrid() {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      padding: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 10,
      ),
      child: GridView.builder(
          addAutomaticKeepAlives: false,
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          itemCount:
              widget.filterData["topics"][widget.counter]["options"].length,
          // controller: secondaryFilterController,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            crossAxisCount: (orientation == Orientation.portrait) ? 5 : 12,
          ),
          padding: EdgeInsets.all(
            10,
          ),
          itemBuilder: (BuildContext context1, int i) {
            return displayStreamingCheckboxes(widget.counter, i);
          }),
    );
  }

  Widget displayStreamingCheckboxes(index, i) {
    double toggle = basedOnshadeAllOptions(i);
    return Container(
      // color: Constants.appColorL3,
      // padding: const EdgeInsets.all(1.0),
      // width: 35.0,
      // height: 35.0,
      child: new Card(
        elevation: 5.0,
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () {
            // widget.setToDisplayOnTitle(
            //     widget.searchFilterData["topics"][index], i);
            // widget.toogleOptionStatus(index, i);
            // widget.filterSearchResults();
            clickedOnStreamingSources(i);
            uponToogle(i);
            printIfDebug("Send Request to server with User Preferences");
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: getStreamingSourceImage(
                    index,
                    i,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getStreamingSourceImage(
    int index,
    int i,
  ) {
    // if (showOriginalImages == 'true') {
    return Container(
      color: Constants.appColorL3,
      child: Image(
        image: NetworkImage(
          widget.filterData["topics"][index]["options"][i]["image"],
          // useDiskCache: false,
        ),
        fit: BoxFit.fill,
        gaplessPlayback: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: 15,
                left: 10,
                bottom: 10,
              ),
              child: Text(
                widget.filterData["topics"][widget.counter]["heading"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  color:
                      // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                      //     ? Constants.appColorLogo
                      //     :
                      Constants.appColorFont,
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                int categoryLength = widget
                    .filterData["topics"][widget.counter]["options"].length;
                for (int j = 0; j < categoryLength; j++) {
                  widget.resetCheckboxOptions(widget.counter, j);
                }
                widget.updateShadeAllOptions(widget.counter, true);
              },
              child: Container(
                padding: EdgeInsets.only(
                  top: 15,
                  bottom: 10,
                ),
                child: Text(
                  "RESET",
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14.0,
                    color:
                        // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                        //     ? Constants.appColorLogo
                        //     :
                        Constants.appColorLogo.withOpacity(0.5),
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 15,
                bottom: 10,
                left: 5,
                right: 5,
              ),
              child: Text(
                "|",
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  color:
                      // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                      //     ? Constants.appColorLogo
                      //     :
                      Constants.appColorLogo.withOpacity(0.5),
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            GestureDetector(
              onTap: () {
                widget.setSelectedFilterStatus(widget.counter);
              },
              child: Container(
                padding: EdgeInsets.only(
                  top: 15,
                  bottom: 10,
                  right: 10,
                ),
                child: Text(
                  "MY APPS",
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14.0,
                    color: Constants.appColorLogo.withOpacity(0.5),
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ),
            ),
          ],
        ),
        clickableImagesGrid(),
      ],
    );
  }
}
