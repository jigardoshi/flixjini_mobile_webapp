import 'package:flixjini_webapp/filter/menu/movie_sort_checkboxes.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_images.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_checkboxes.dart';
import 'package:flixjini_webapp/filter/menu/movie_filter_slider.dart';
// import 'package:flixjini_webapp/filter/menu/movie_filter_sort.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieFilterStructure extends StatefulWidget {
  final filterData;
  final resetCheckboxOptions;
  final resetRangeOptions;
  final resetSortOptions;
  final updateupdateCheckbox;
  final updateClickableImages;
  final updateMinMaxRange;
  final updateSortOrder;
  final sortKey;
  final filterKey;
  final filterMenuController;

  final shadeAllOptions;
  final updateShadeAllOptions;
  final checkIfAnySelected;
  final setSelectedFilterStatus;

  MovieFilterStructure({
    this.filterData,
    this.resetCheckboxOptions,
    this.resetRangeOptions,
    this.resetSortOptions,
    this.updateupdateCheckbox,
    this.updateClickableImages,
    this.updateMinMaxRange,
    this.updateSortOrder,
    this.sortKey,
    this.filterKey,
    this.filterMenuController,
    this.shadeAllOptions,
    this.updateShadeAllOptions,
    this.checkIfAnySelected,
    this.setSelectedFilterStatus,
  });

  @override
  _MovieFilterStructureState createState() => new _MovieFilterStructureState();
}

class _MovieFilterStructureState extends State<MovieFilterStructure> {
  List<Widget> listOfWidgetsForSavedFilters = <Widget>[];
  String authenticationToken = "";
  bool hideFlag = false, hideFlagSaved = false;
  List hideFlagFilter = <bool>[];

  List hideTagTextListFilter = <Function>[];
  void initState() {
    super.initState();
    loadSavedFilters();
    checkAuthStatus();
  }

  void checkAuthStatus() async {
    authenticationToken = await getStringFromSP('authenticationToken');
    setState(() {
      authenticationToken = authenticationToken;
    });
  }

  Widget undefinedGroup(i) {
    return new Container();
  }

  Widget categoryBlocks(i) {
    if (widget.filterData["topics"][i]["group"] == "checkBoxes" ||
        widget.filterData["topics"][i]["heading"] == "Genre") {
      return new MovieFilterCheckboxes(
        filterData: widget.filterData,
        counter: i,
        updateupdateCheckbox: widget.updateupdateCheckbox,
        resetCheckboxOptions: widget.resetCheckboxOptions,
      );
    } else if (widget.filterData["topics"][i]["group"] == "slider") {
      return new MovieFilterSlider(
        filterData: widget.filterData,
        counter: i,
        updateMinMaxRange: widget.updateMinMaxRange,
        resetRangeOptions: widget.resetRangeOptions,
      );
    } else if (widget.filterData["topics"][i]["group"] == "clickableImages") {
      return new MovieFilterImages(
        filterData: widget.filterData,
        counter: i,
        updateClickableImages: widget.updateClickableImages,
        updateShadeAllOptions: widget.updateShadeAllOptions,
        checkIfAnySelected: widget.checkIfAnySelected,
        setSelectedFilterStatus: widget.setSelectedFilterStatus,
        resetCheckboxOptions: widget.resetCheckboxOptions,
      );
    } else {
      return undefinedGroup(i);
    }
  }

  // Widget expansionMenu(int index) {
  //   printIfDebug("Render Categories");
  //   return new Container(
  //     padding: const EdgeInsets.all(5.0),
  //     child: new Card(
  //       elevation: 6.0,
  //     ),
  //   );
  // }

  Widget fullBlownFilterMenu() {
    var categories = <Widget>[];
    var categoriesLength = widget.filterData["topics"].length;
    for (int index = 0; index < categoriesLength; index++) {
      hideTagTextListFilter.add((bool open) {
        if (open) {
          setState(() {
            hideFlagFilter[index] = false;
          });
        } else {
          setState(() {
            hideFlagFilter[index] = true;
          });
        }
      });
      hideFlagFilter.add(false);
      var category = Container(
        margin: EdgeInsets.only(
          top: 10,
          bottom: 10,
          left: 0,
          right: 0,
        ),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        child: Card(
          elevation: 2.5,
          color: Constants.appColorL2,
          child: Container(
            padding: const EdgeInsets.all(0.0),
            child: categoryBlocks(index),
          ),
        ),
      );
      if (widget.filterData["topics"][index]["heading"]
              .toString()
              .toUpperCase() !=
          "DEVICE TYPE") {
        categories.add(category);
      }
    }
    return new Column(
      key: widget.filterKey,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: categories,
    );
  }

  Widget fullBlownSortMenu() {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
        left: 0,
        right: 0,
      ),
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: Card(
        elevation: 2.5,
        color: Constants.appColorL2,
        child: MovieSortCheckboxes(
          filterData: widget.filterData,
          updateSortOrder: widget.updateSortOrder,
          resetSort: widget.resetSortOptions,
        ),
      ),
    );
  }

  Widget menuTitle(String title) {
    return new Text(
      title,
      textAlign: TextAlign.center,
      style: new TextStyle(
          fontWeight: FontWeight.bold,
          color: Constants.appColorFont,
          fontSize: 13.0),
    );
  }

  hideTagTextList(bool open) {
    if (open) {
      setState(() {
        hideFlag = false;
      });
    } else {
      setState(() {
        hideFlag = true;
      });
    }
  }

  hideTagTextListSaved(bool open) {
    if (open) {
      setState(() {
        hideFlagSaved = false;
      });
    } else {
      setState(() {
        hideFlagSaved = true;
      });
    }
  }

  void loadSavedFilters() async {
    printIfDebug('\n\nsaved filters button is tapped');
    String getFiltersAPIEndpoint =
        "https://api.flixjini.com/queue/get_filters.json?";

    String defaultParamsAddedURL =
        await fetchDefaultParams(getFiltersAPIEndpoint);
    printIfDebug('\n\ndefault params added url: $defaultParamsAddedURL');

    defaultParamsAddedURL += "&";

    String headerAddedURL = constructHeader(defaultParamsAddedURL);
    printIfDebug('\n\nheader added url is: $headerAddedURL');

    headerAddedURL += "&magic=true&country_code=IN&jwt_token=";

    String authToken = await getStringFromSP('authenticationToken');
    headerAddedURL += authToken;

    printIfDebug('\n\nurl before making request is: $headerAddedURL');

    callGetFiltersAPIEndpoint(headerAddedURL);
  }

  void callGetFiltersAPIEndpoint(String url) {
    List userSavedFilters = [];
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        if (resBody["error"] == false) {
          userSavedFilters = resBody["filters"];
          takeUsersSavedFilters(userSavedFilters);
          setState(() {
            //
          });
        } else {
          // error occurred

        }
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  void takeUsersSavedFilters(List userSavedFilters) {
    int userSavedFiltersCount = userSavedFilters.length;
    Map oneFilterJSON = new Map();
    printIfDebug('\n\nno of filters user has saved is: $userSavedFiltersCount');
    for (int i = 0; i < userSavedFiltersCount; i++) {
      // int id = userSavedFilters[i]["movie_saved_filter"]["id"];
      // printIfDebug('\n\nid is: $id');
      oneFilterJSON = userSavedFilters[i]["movie_saved_filter"];
      // printIfDebug('\n\none filter json: $oneFilterJSON');

      addOneFilterToList(oneFilterJSON, i);
    }
  }

  void addOneFilterToList(Map oneFilterJSON, int i) {
    String filterName = oneFilterJSON["name"];
    int filterId = oneFilterJSON["id"];
    String filterQueryString = oneFilterJSON["query_string"];

    listOfWidgetsForSavedFilters.add(
      GestureDetector(
        child: Container(
          height: 50,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 10.0,
                      ),
                      child: Text(
                        capitalize(filterName),
                        style: TextStyle(
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(
                        right: 10.0,
                      ),
                      child: GestureDetector(
                        child: Image.asset(
                          'assests/trash_icon.png',
                          color: Constants.appColorDivider,
                        ),
                        onTap: () {
                          getConfirmationDialogForDeleteFilter(
                              filterId, filterName, filterQueryString);
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  left: 10,
                  right: 10,
                ),
                // width: 100,
                height: 1,
                color: Constants.appColorL2,
              ),
            ],
          ),
        ),
        onTap: () {
          getConfirmationBeforeApplyingFilters(
              filterId, filterName, filterQueryString);
        },
      ),
    );
  }

  Future<dynamic> getConfirmationBeforeApplyingFilters(
      int filterId, String filterName, String filterQueryString) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Constants.appColorL2,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Confirm',
                style: TextStyle(
                  color: Constants.appColorFont,
                ),
              ),
              Container(
                color: Constants.appColorLogo,
                margin: EdgeInsets.only(
                  top: 5,
                ),
                height: 3,
                width: 50,
              ),
            ],
          ),
          content:
              // Text(
              //   'Are you sure you want to apply the filter $filterName?',
              //   style: TextStyle(
              //     color: Constants.appColorFont,
              //   ),
              // ),
              Container(
            height: 90,
            child: Column(
              children: <Widget>[
                Text(
                  'Are you sure you want to apply the filter $filterName?',
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                      ),
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Apply',
                          style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Constants.appColorLogo,
                          ),
                        ),
                        onTap: () {
                          printIfDebug('\n\nuser chose yes to logout');
                          Navigator.of(context).pop();
                          takeUserToFilterPage(
                              filterId, filterName, filterQueryString);
                          // showToast(
                          // 'Logout Successful', 'long', 'bottom', 1, Constants.appColorLogo);
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void takeUserToFilterPage(
      int filterId, String filterName, String filterQueryString) {
    printIfDebug(
        '\n\ntapped filter details: \nfilter name: $filterName, filter id: $filterId, filter query string: $filterQueryString');
    String appliedFilterUrl =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&";
    filterQueryString = filterQueryString.substring(1);
    appliedFilterUrl += filterQueryString;
    Map<String, dynamic> myMap = new Map<String, dynamic>(),
        visibleFiltersMap = new Map();
    Uri uri = Uri.parse(appliedFilterUrl);
    myMap = uri.queryParametersAll;
    printIfDebug(
        '\n\n final applied filter url: $appliedFilterUrl, and its type is string');
    printIfDebug('\n\n query params map: $myMap, and its type is map');

    /*
     * for each key, if the corresponding value has more than one element in the list, 
     * all the elements in the list are together considered as one element.
     * so, to fix this issue, list is encoded to string and then added double proper quotes 
     * to make sure that the elements are discrete and not considered together as one element.
     */
    myMap.forEach((key, value) {
      printIfDebug('\n\nkey $key');
      int valueLength = value.length;
      printIfDebug('\n\nvalue length: $valueLength');
      String tempString = json.encode(value);
      tempString = tempString.replaceAll(',', '", "');
      printIfDebug('\n\ncomma and space added temp string: $tempString');

      List tempList = json.decode(tempString);
      printIfDebug('\n\ntemp list length: ${tempList.length}');
      visibleFiltersMap[key] = tempList;
    });

    printIfDebug('\n\nfinal map before navigating: $visibleFiltersMap');

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new MovieFilterPage(
              appliedFilterUrl: appliedFilterUrl,
              visibleFilters: visibleFiltersMap,
              // add these 2 params
              // visibilityStatus: ,
              inTheaterKey: false,
            )));
  }

  Future<dynamic> getConfirmationDialogForDeleteFilter(
      int filterId, String filterName, String filterQueryString) {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Constants.appColorL2,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Confirm',
                style: TextStyle(
                  color: Constants.appColorFont,
                ),
              ),
              Container(
                color: Constants.appColorLogo,
                margin: EdgeInsets.only(
                  top: 5,
                ),
                height: 3,
                width: 50,
              ),
            ],
          ),
          content:
              // Text(
              //   'Are you sure you want to delete the filter $filterName?',
              //   style: TextStyle(
              //     color: Constants.appColorFont,
              //   ),
              // ),
              Container(
            height: 90,
            child: Column(
              children: <Widget>[
                Text(
                  'Are you sure you want to delete the filter $filterName?',
                  style: TextStyle(
                    color: Constants.appColorFont,
                    fontSize: 15,
                    height: 1.3,
                  ),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        left: 35,
                        right: 35,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'No',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                          ),
                        ),
                        onTap: () {
                          printIfDebug('\n\nuser chose no to logout');
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.only(
                      //   left: 20,
                      // ),
                      padding: EdgeInsets.only(
                        left: 35,
                        right: 35,
                        top: 5,
                        bottom: 5,
                      ),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      child: GestureDetector(
                        child: Text(
                          'Yes',
                          style: TextStyle(
                            color: Constants.appColorLogo,
                          ),
                        ),
                        onTap: () {
                          printIfDebug(
                              '\n\nuser chose yes to delete the filter');
                          Navigator.of(context).pop();
                          printIfDebug(
                              '\n\ndelete the filter, having the following details: \n\nfilterId: $filterId, filterName: $filterName, filterQueryString: $filterQueryString');
                          deleteSavedFilter(
                              filterId, filterName, filterQueryString);
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void deleteSavedFilter(
      int filterId, String filterName, String filterQueryString) async {
    String apiEndpointForDeleteFilter =
        "https://api.flixjini.com/queue/delete_filters.json?";
    apiEndpointForDeleteFilter =
        await fetchDefaultParams(apiEndpointForDeleteFilter);
    apiEndpointForDeleteFilter += constructHeader('&');
    apiEndpointForDeleteFilter +=
        "&magic=true&country_code=IN&id=$filterId&jwt_token=";
    String authToken = await getStringFromSP('authenticationToken');
    apiEndpointForDeleteFilter += authToken;
    printIfDebug(
        '\n\ncomplete url before making a request to delete filter api is: $apiEndpointForDeleteFilter');

    callDeleteFilterAPI(apiEndpointForDeleteFilter, filterId);
  }

  void callDeleteFilterAPI(String url, int filterId) {
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        printIfDebug('\n\ndecoded response: $resBody');
        setState(() {
          listOfWidgetsForSavedFilters = [];
          loadSavedFilters();
        });
        //
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

  Widget setupAlertDialogContainer() {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: listOfWidgetsForSavedFilters.length,
      itemBuilder: (BuildContext context, int index) {
        return listOfWidgetsForSavedFilters[index];
      },
    );
  }

  Widget sortAndFilterMenu() {
    return new Container(
      color: Constants.appColorL1,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          authenticationToken.isNotEmpty ? myWidget1() : Container(),
          new Container(
            child: fullBlownSortMenu(),
          ),
          menuTitle("Filter by"),
          new Container(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: fullBlownFilterMenu(),
          ),
        ],
      ),
    );
  }

  Widget myWidget1() {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        child: new Card(
          color: Constants.appColorL2,
          elevation: 6.0,
          child: ExpansionTile(
            backgroundColor: Constants.appColorL2,
            initiallyExpanded: false,
            onExpansionChanged: hideTagTextListSaved,
            trailing: hideFlagSaved
                ? Image.asset(
                    'assests/greendown.png',
                    height: 10,
                    width: 10,
                  )
                : Image.asset(
                    'assests/greenup.png',
                    height: 10,
                    width: 10,
                  ),
            title: Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                textDirection: TextDirection.ltr,
                children: <Widget>[
                  new Expanded(
                      child: new Stack(
                        children: <Widget>[
                          new Container(
                            height: 20.0,
                            color: Constants.appColorL2,
                          ),
                          new Positioned(
                            width: 3.0,
                            height: 20.0,
                            child: new Container(
                              color: Constants.appColorLogo,
                            ),
                          ),
                        ],
                      ),
                      flex: 1),
                  new Expanded(
                    child: new Text(
                      'Saved Filters',
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Constants.appColorFont,
                          fontSize: 13.0),
                      textAlign: TextAlign.left,
                    ),
                    flex: 8,
                  ),
                ],
              ),
            ),
            children: <Widget>[
              new Container(
                color: Constants.appColorL2,
                padding: const EdgeInsets.all(0.0),
                child: setupAlertDialogContainer(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return sortAndFilterMenu();
  }
}
