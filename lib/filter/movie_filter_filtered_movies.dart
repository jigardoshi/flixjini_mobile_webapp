import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import	'package:streaming_entertainment/filter/movie_filter_card_scores.dart';
// import	'package:streaming_entertainment/filter/movie_filter_user_preferences.dart';
import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'dart:convert';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MovieFilterFilteredMovies extends StatefulWidget {
  MovieFilterFilteredMovies({
    Key? key,
    this.movieDetail,
    this.userDetail,
    this.userQueue,
    this.authenticationToken,
    this.endOfSearchResults,
    this.changePageNo,
    this.loadingSymbolFetchMoreResults,
    this.theTheaterKey,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieDetail;
  final userDetail;
  final userQueue;
  final authenticationToken;
  final endOfSearchResults;
  final changePageNo;
  final loadingSymbolFetchMoreResults;
  final theTheaterKey;
  final showOriginalPosters;

  @override
  _MovieFilterFilteredMoviesState createState() =>
      new _MovieFilterFilteredMoviesState();
}

class _MovieFilterFilteredMoviesState extends State<MovieFilterFilteredMovies> {
  bool endOfSearchResults = false;
  var userPreferences;
  var userQueuePreferences;
  List queueIds = [];
  var watchList = {"movies": []};
  List seenIds = <String>[];

  @override
  void initState() {
    printIfDebug("Hi");
    super.initState();
    if (!(widget.authenticationToken.isEmpty)) {
      setState(() {
        userPreferences = widget.userDetail;
        userQueuePreferences = widget.userQueue;
      });
      createUserQueue();
    } else {
      printIfDebug(
          "No need for User preferences as user hasn't been authorised");
    }
    getSeenMovieIds();
  }

  createUserQueue() {
    printIfDebug("queue filter" + userQueuePreferences["movies"].toString());
    int size = userQueuePreferences["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        var movie = {
          "id": userQueuePreferences["movies"][i]["id"],
          "status": true,
        };
        queueIds.add(userQueuePreferences["movies"][i]["id"]);
        setState(() {
          watchList["movies"]!.add(movie);
        });
      }
    }
    // printIfDebug("Movie Watchlist");
    // printIfDebug(watchList);
    printIfDebug("DOne");
  }

  basedOnUserQueue(movie) {
    int size = watchList["movies"]!.length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (watchList["movies"]![i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          return watchList["movies"]![i]["status"];
        }
      }
    }
    return false;
  }

  basedonUserPreference(movie, entity) {
    int size = userPreferences["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (userPreferences["movies"][i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          return userPreferences["movies"][i][entity];
        }
      }
    }
    return false;
  }

  removeMovieFromWatchList(movie, actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    printIfDebug(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          updateUserQueue(movie);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkAction(movie) {
    int size = watchList["movies"]!.length;
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        if (watchList["movies"]![i]["status"]) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  updateUserQueue(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug("wishlist");
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        setState(() {
          watchList["movies"]![i]["status"] =
              watchList["movies"]![i]["status"] ? false : true;
        });
        flag = 1;
        printIfDebug("Change Existing Entry in Queue");
        break;
      }
    }
    if (flag == 0) {
      var entity = {
        "id": movie["id"],
        "status": true,
      };
      setState(() {
        watchList["movies"]!.add(entity);
      });
      printIfDebug("Add New Entry to Queue");
      printIfDebug(watchList);
    }
    printIfDebug(movie["id"]);
  }

  sendRequest(movie, actionUrl, entity) async {
    printIfDebug((widget.authenticationToken).isEmpty);
    if (!widget.authenticationToken.isEmpty) {
      bool flag = true;
      if (entity == "add") {
        flag = checkAction(movie);
      }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
        };
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              if (entity == "add") {
                updateUserQueue(movie);
                showToast('Added to queue', 'long', 'bottom', 1,
                    Constants.appColorLogo);
              } else {
                updateUserPreferences(movie, entity);
              }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"] +
            '&jwt_token=' +
            widget.authenticationToken;
        showToast(
            'Removed from queue', 'long', 'bottom', 1, Constants.appColorLogo);
        removeMovieFromWatchList(movie, actionUrl);
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  updateUserPreferences(movie, entity) {
    int size = userPreferences["movies"].length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug(entity);
    for (int i = 0; i < size; i++) {
      if (userPreferences["movies"][i]["id"] == movie["id"]) {
        setState(() {
          userPreferences["movies"][i][entity] =
              userPreferences["movies"][i][entity] ? false : true;
        });
        if ((entity == "like") || (entity == "dislike")) {
          String opposite = (entity == "like") ? "dislike" : "like";
          if (userPreferences["movies"][i][entity] &&
              userPreferences["movies"][i][opposite]) {
            setState(() {
              userPreferences["movies"][i][opposite] =
                  !userPreferences["movies"][i][entity];
            });
          }
        } else {
          printIfDebug(
              "Another option apart from like and dislike have been selected");
        }
        flag = 1;
        printIfDebug("Change Existing Entry");
        break;
      }
    }
    if (flag == 0) {
      var userChoice = {
        "id": movie["id"],
        "like": false,
        "dislike": false,
        "seen": false,
      };
      userChoice[entity] = userChoice[entity] ? false : true;
      setState(() {
        userPreferences["movies"].add(userChoice);
      });
      printIfDebug("Add New Entry");
      printIfDebug(userPreferences);
    }
    printIfDebug(movie["id"]);
  }

  Widget movieCardBuilder(index) {
    return new CardStructure(
      movie: widget.movieDetail["movies"][index],
      sendRequest: sendRequest,
      basedOnUserQueue: basedOnUserQueue,
      basedonUserPreference: basedonUserPreference,
      authenticationToken: widget.authenticationToken,
      index: index,
      showOriginalPosters: widget.showOriginalPosters,
      pagefor: 3,
      queueIds: queueIds,
      seenIds: seenIds,
    );
  }

  getSeenMovieIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    seenIds = prefs.getStringList("seenids")!;
    printIfDebug("seenids :" + seenIds.toString());
  }

  Widget loadMoreButton() {
    return new GridTile(
      child: new GestureDetector(
        onTap: () {
          printIfDebug("Load More Option");
          widget.changePageNo();
        },
        child: new Container(
          decoration: BoxDecoration(
            color: Constants.appColorDivider.withOpacity(0.7),
            shape: BoxShape.circle,
          ),
          child: Container(
            margin: EdgeInsets.only(
              left: 25,
              right: 25,
            ),
            child: Center(
                child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset(
                    'assests/expand_icon.png',
                    height: 18,
                    width: 18,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.all(5.0),
                  child: new Text(
                    "More",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: 16.0),
                  ),
                ),
              ],
            )),
          ),
        ),
      ),
    );
  }

  Widget displayLoading() {
    return new GridTile(
      child: new GestureDetector(
        onTap: () {
          printIfDebug("Trigger Load More");
        },
        child: new Container(
          decoration: BoxDecoration(
            color: Constants.appColorDivider.withOpacity(0.7),
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: new CircularProgressIndicator(
              backgroundColor: Constants.appColorLogo,
              valueColor:
                  new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
            ),
          ),
        ),
      ),
    );
  }

  Widget filteredMoviesGrid() {
    var orientation = MediaQuery.of(context).orientation;
    int numberOfFilterResults = widget.movieDetail["movies"].length;
    int extraLoadBoxes = 1;
    printIfDebug("Value of Laod More");
    printIfDebug(widget.loadingSymbolFetchMoreResults);

    // Load More Movies Button - Not needed when inTheaters Option is set
    if (!widget.theTheaterKey) {
      if (widget.loadingSymbolFetchMoreResults) {
        if (numberOfFilterResults % 2 == 0) {
          printIfDebug("2 Boxes Added");
          extraLoadBoxes = 2;
        } else {
          printIfDebug("3 Boxes Added");
          extraLoadBoxes = 3;
        }
      }
    } else {
      extraLoadBoxes = 0;
    }
    return new Container(
      padding: const EdgeInsets.all(0.0),
      child: new GridView.builder(
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        primary: true,
        addAutomaticKeepAlives: false,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 0.57,
          // mainAxisSpacing: 10.0,
          // crossAxisSpacing: 10.0,
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
        ),
        padding: const EdgeInsets.all(10.0),
        itemCount: widget.movieDetail["movies"].length + extraLoadBoxes,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) {
          if (index < numberOfFilterResults) {
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  printIfDebug("Image Id that has been clicked");
                  printIfDebug(
                      widget.movieDetail["movies"][index]["movieName"]);
                },
                child: movieCardBuilder(index),
              ),
            );
          } else {
            if (widget.loadingSymbolFetchMoreResults) {
              return displayLoading();
            } else {
              printIfDebug("Display Load More");
              // return loadMoreButton();
              return Container();
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    return filteredMoviesGrid();
  }
}
