import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/filter/movie_filter_collage.dart';

class MovieFilterCollapsableAppbar extends StatefulWidget {
  MovieFilterCollapsableAppbar({
    Key? key,
    this.movieDetail,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieDetail, showOriginalPosters;

  @override
  _MovieFilterCollapsableAppbarState createState() =>
      new _MovieFilterCollapsableAppbarState();
}

class _MovieFilterCollapsableAppbarState
    extends State<MovieFilterCollapsableAppbar> {
  String movieName = "";

  Widget collapsingAppBar() {
    movieName = "";
    printIfDebug(movieName);
    var screenheight = MediaQuery.of(context).size.height;
    return new SliverAppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      leading: new InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: new Container(
          padding: const EdgeInsets.all(20.0),
          child: new Image.asset(
            "assests/backarrow1.png",
            width: 20.0,
            height: 15.0,
          ),
        ),
      ),
      expandedHeight: screenheight * 0.60,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        background: MovieFilterCollage(
          movieDetail: widget.movieDetail,
          showOriginalPosters: widget.showOriginalPosters,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    // return collapsingAppBar();
    try {
      return MovieFilterCollage(
        movieDetail: widget.movieDetail,
        showOriginalPosters: widget.showOriginalPosters,
      );
    } catch (e) {
      printIfDebug(e);
    }
    return Container();
  }
}
