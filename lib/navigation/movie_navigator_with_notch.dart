import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/page.dart';
// import	'package:streaming_entertainment/filter/movie_filter_page.dart';
// import	'package:streaming_entertainment/index/movie_index_page.dart';
// import	'package:streaming_entertainment/profile/movie_profile_page.dart';
// import	'package:streaming_entertainment/search/movie_search_page.dart';
// import	'package:streaming_entertainment/queue/movie_queue_page.dart';
// import	'package:streaming_entertainment/authentication/movie_authentication_page.dart';
// import	'package:streaming_entertainment/queue/movie_queue_page.dart';
// import	'package:streaming_entertainment/authentication/movie_authentication_google.dart';
// import  'package:shared_preferences/shared_preferences.dart';
// import  'package:google_sign_in/google_sign_in.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 	'dart:async';
import 'package:flixjini_webapp/common/constants.dart';

class MovieNavigatorWithNotch extends StatefulWidget {
  MovieNavigatorWithNotch({
    Key? key,
  }) : super(key: key);

  @override
  _MovieNavigatorWithNotchState createState() =>
      new _MovieNavigatorWithNotchState();
}

class _MovieNavigatorWithNotchState extends State<MovieNavigatorWithNotch> {
  int i = 2;

  Widget navigationOptions(String imageUrl, var route, String optionName) {
    return new Container(
      height: 40.0,
      child: new GestureDetector(
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              new Container(
                padding: const EdgeInsets.all(0.0),
                child: new Image.asset(
                  imageUrl,
                  fit: BoxFit.cover,
                  width: 20.0,
                  height: 20.0,
                ),
              ),
              new Container(
                padding: const EdgeInsets.all(0.0),
                child: new Text(
                  optionName,
                  textAlign: TextAlign.right,
                  style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Constants.appColorL3,
                      fontSize: 11.0),
                ),
              ),
            ]),
        onTap: () {
          printIfDebug("Move to Selected Page");
          Navigator.of(context).push(route);
        },
      ),
    );
  }

  Widget navigationOptionsIcon(String imageUrl) {
    if (imageUrl.isEmpty) {
      return Container(
        height: 20.0,
        width: 20,
      );
    } else {
      return new Container(
        height: 20.0,
        width: 20,
        child: new Image.asset(
          imageUrl,
          color: Constants.appColorDivider,
          fit: BoxFit.fitHeight,
        ),
      );
    }
  }

  Widget navigationOptionsTitle(String optionName, int check) {
    if (optionName.isEmpty) {
      return Container(
        height: 20.0,
      );
    } else {
      return new Container(
        // height: 20.0,
        child: new Center(
          child: new Text(
            optionName,
            textAlign: TextAlign.right,
            style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorDivider,
                fontSize: 11.0),
            // textScaleFactor: 0.75,
          ),
        ),
      );
    }
  }

  Widget universalNavigationBar() {
    return new PreferredSize(
      preferredSize: new Size.fromHeight(50.0),
      child: new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Constants.appColorL2,
        ),
        child: new BottomNavigationBar(
          
          items: [
            new BottomNavigationBarItem(
              icon: navigationOptionsIcon("assests/inactive_home.png"),
              title: navigationOptionsTitle("HOME", 0),
            ),
            new BottomNavigationBarItem(
              icon: navigationOptionsIcon("assests/inactive_search.png"),
              title: navigationOptionsTitle("SEARCH", 1),
            ),
            new BottomNavigationBarItem(
              icon: navigationOptionsIcon(""),
              title: navigationOptionsTitle("", 2),
            ),
            new BottomNavigationBarItem(
              icon: navigationOptionsIcon("assests/inactive_livetv.png"),
              title: navigationOptionsTitle("LIVE TV", 3),
            ),
            new BottomNavigationBarItem(
              icon: navigationOptionsIcon("assests/inactive_profile.png"),
              title: navigationOptionsTitle("PROFILE", 4),
            ),
          ],
          currentIndex: 2,
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) =>
                    MovieRouting(defaultTab: index)));
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return universalNavigationBar();
  }
}
