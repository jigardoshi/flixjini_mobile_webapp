import 'package:flixjini_webapp/fresh/movie_fresh_roulette_page.dart';
import 'package:flixjini_webapp/livetv/live_tv_page.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/index/movie_index_page.dart';
import 'package:flixjini_webapp/profile/movie_profile_page.dart';
import 'package:flixjini_webapp/search/movie_search_page.dart';
// import 'package:flixjini_webapp/queue/movie_queue_page.dart';
// import 'package:flixjini_webapp/fresh/movie_fresh_page.dart';
// import 'package:flixjini_webapp/fresh/story_design.dart';
import 'package:flixjini_webapp/common/constants.dart';

import 'dart:async';

class MovieRouting extends StatefulWidget {
  MovieRouting({
    Key? key,
    this.defaultTab,
    this.indexCatDetail,
    this.indexTopBottomDetail,
    this.showOriginalPosters,
    this.urlAppendString,
  }) : super(key: key);

  final defaultTab;
  final indexCatDetail;
  final indexTopBottomDetail;
  final showOriginalPosters;
  final urlAppendString;

  @override
  MovieRoutingState createState() => new MovieRoutingState();
}

// SingleTickerProviderStateMixin is used for animation
class MovieRoutingState extends State<MovieRouting>
    with SingleTickerProviderStateMixin {
  int i = 0;
  int temp = 0;
  var tabNavHistory = [];
  var freshness;
  bool showOriginalPosters = false;
  bool fetchedFRCMagicSwitch = false;

  @override
  void initState() {
    super.initState();
    getShowOriginalPosters();

    if (widget.defaultTab != null) {
      defaultStateOfTabs(widget.defaultTab);
    } else {
      defaultStateOfTabs(2);
    }
  }

  void getShowOriginalPosters() async {
    if (widget.showOriginalPosters == null) {
      String showOriginalPostersFromFRC =
          await getStringFromFRC('show_original_posters');

      if (showOriginalPostersFromFRC == 'true') {
        setState(() {
          this.showOriginalPosters = true;
          fetchedFRCMagicSwitch = true;
        });
      } else {
        setState(() {
          this.showOriginalPosters = false;
          fetchedFRCMagicSwitch = true;
        });
      }
    } else {
      setState(() {
        this.showOriginalPosters = widget.showOriginalPosters;
        fetchedFRCMagicSwitch = true;
      });
    }
  }

  defaultStateOfTabs(value) {
    setState(() {
      i = value;
      tabNavHistory.add(value);
    });
  }

  bool jumpToPreviousTab() {
    int tabNavHistoryLength = tabNavHistory.length;
    if (tabNavHistoryLength > 1) {
      setState(() {
        tabNavHistory.removeLast();
        i = tabNavHistory.last;
      });
      return false;
    } else {
      printIfDebug("Tab Stack Done - Minimise App");
      return true;
    }
  }

  bool checkStack() {
    int tabNavHistoryLength = tabNavHistory.length;
    if (tabNavHistoryLength > 1) {
      return false;
    } else {
      return true;
    }
  }

  Future<bool> moveToPreviousTab() async {
    int tabNavHistoryLength = tabNavHistory.length;
    if (tabNavHistoryLength > 1) {
      setState(() {
        tabNavHistory.removeLast();
        i = tabNavHistory.last;
      });
      return false;
    } else {
      printIfDebug("Tab Stack Done - Minimise App");
      return true;
    }
  }

  void moveToPreviousTabFromAnotherPage() async {
    printIfDebug("Invoked");
    int tabNavHistoryLength = tabNavHistory.length;
    if (tabNavHistoryLength > 1) {
      setState(() {
        tabNavHistory.removeLast();
        i = tabNavHistory.last;
      });
    } else {
      printIfDebug("Tab Stack Done - Minimise App");
      Navigator.pop(context);
    }
  }

  Widget navigationOptions(String imageUrl, String optionName) {
    return new Container(
      height: 40.0,
      child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            new Container(
              padding: const EdgeInsets.all(1.0),
              child: new Image.asset(
                imageUrl,
                color: Constants.appColorL1,
                fit: BoxFit.cover,
                width: 20.0,
                height: 20.0,
              ),
            ),
            new Container(
              padding: const EdgeInsets.all(1.0),
              child: new Text(
                optionName,
                textAlign: TextAlign.right,
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Constants.appColorL1.withOpacity(0.8)),
                textScaleFactor: 0.75,
              ),
            ),
          ]),
    );
  }

  Widget navigationOptionsTitle(
      String optionName, int check, Color color, FontWeight fontWeight) {
    return new Container(
      height: 20.0,
      child: new Center(
        child: new Text(
          optionName,
          textAlign: TextAlign.right,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: color,
            fontSize: 11.0,
          ),
        ),
      ),
    );
  }

  Widget activeNavigationOptionsIcon(String imageUrl, bool trans) {
    return Container(
      height: trans ? 35 : 20.0,
      child: new Image.asset(
        imageUrl,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget navigationOptionsIcon(String imageUrl, bool trans) {
    return new Container(
      height: trans ? 30 : 20.0,
      child: new Image.asset(
        imageUrl,
        color: !trans ? Constants.appColorDivider : null,
        fit: BoxFit.cover,
      ),
    );
  }

  setFreshness(value) {
    freshness = value;
  }

  Widget renderTab(int i) {
    if (!fetchedFRCMagicSwitch) {
      printIfDebug(
        '\n\nnot fetched magic switch from frc, hence showing progress bar',
      );
      return Container();
      // return getProgressBar();
    } else {
      printIfDebug(
        '\n\nfetched magic switch from frc, and the value is: $showOriginalPosters',
      );

      if (i == 0) {
        return MovieIndexPage(
          indexCatDetail: widget.indexCatDetail,
          indexTopBottomDetail: widget.indexTopBottomDetail,
          setTabAndNavigationHistory: setTabAndNavigationHistory,
          setFreshness: setFreshness,
          showOriginalPosters: showOriginalPosters,
        );
      } else if (i == 1) {
        return new MovieSearchPage(
          moveToPreviousTab: moveToPreviousTabFromAnotherPage,
          showOriginalPosters: showOriginalPosters,
        );
      } else if (i == 2) {
        return
            // StoryDesign();
            MovieFreshRoulettePage(
          moveToPreviousTab: moveToPreviousTabFromAnotherPage,
          freshness: freshness,
          checkStack: checkStack,
          showOriginalPosters: showOriginalPosters,
          urlAppendString: widget.urlAppendString,
        );
      } else if (i == 3) {
        return new LiveTvPage(
          moveToPreviousTab: moveToPreviousTabFromAnotherPage,
          showOriginalPosters: showOriginalPosters,
        );
      } else {
        return new MovieProfilePage(
          moveToPreviousTab: moveToPreviousTabFromAnotherPage,
          switchCardImages: switchCardImages,
          showOriginalPosters: showOriginalPosters,
        );
      }
    }
  }

  void switchCardImages() {
    if (showOriginalPosters == true) {
      // storeMagicSwitchInSharedPref('false');
      setState(() {
        showOriginalPosters = false;
      });
    } else {
      // storeMagicSwitchInSharedPref('true');
      setState(() {
        showOriginalPosters = true;
      });
    }
  }

  void storeMagicSwitchInSharedPref(
    String value,
  ) {
    setStringToSP('show_original_posters', value);
  }

  setTabAndNavigationHistory(int index) {
    int tabNavHistoryLastPosition = tabNavHistory.length - 1;
    printIfDebug(index);
    if (tabNavHistory[tabNavHistoryLastPosition] != index) {
      setState(() {
        tabNavHistory.add(index);
        i = index;
      });
    } else {
      printIfDebug("Same Tab Page");
    }
    printIfDebug("Tab Navigation History");
    printIfDebug(tabNavHistory);
  }

  Widget constructFloatingFilterButton(int i) {
    return new Container(
      // child: new FittedBox(
      child: new FloatingActionButton(
        child: Container(
          child: Image.asset(
            i == 2 || true
                ? "assests/genie_active.png"
                : "assests/genie_inactive.png",
            height: i == 2 || true ? 100 : 50,
            width: i == 2 || true ? 100 : 50,
          ),
        ),
        onPressed: () {},
        backgroundColor: Colors.transparent,
      ),
      // ),
    );
  }

  Widget newBottomItem(
      int index, String active, String inactive, String title) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        decoration: index == 2
            ? BoxDecoration(
                color: Constants.appColorL3,
                borderRadius: BorderRadius.circular(5),
              )
            : null,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: index == 2
                  ? i == index
                      ? 50
                      : 20
                  : 20.0,
              child: new Image.asset(
                i == index ? active : inactive,
                fit: BoxFit.cover,
                color: i == index
                    ? index != 2
                        ? Constants.appColorLogo
                        : null
                    : index != 2
                        ? Constants.appColorDivider
                        : null,
              ),
            ),
            index == 2 && i == index
                ? Container()
                : Container(
                    width: 50,
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: i == index
                            ? Constants.appColorLogo
                            : Constants.appColorDivider,
                        fontSize: 10,
                        fontWeight:
                            i == index ? FontWeight.bold : FontWeight.normal,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
          ],
        ),
      ),
      onTap: () {
        setTabAndNavigationHistory(index);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return WillPopScope(
      onWillPop: moveToPreviousTab,
      child: i != 2 || true
          ? Scaffold(
              backgroundColor: Constants.appColorL2,
              body: renderTab(i),
              //  backgroundColor: Constants.appColorL1,
              resizeToAvoidBottomInset: false,
              // floatingActionButtonLocation:
              //     FloatingActionButtonLocation.centerFloat,
              // floatingActionButton: Align(
              //   child: constructFloatingFilterButton(i),
              //   alignment: Alignment(0, 1.2),
              // ),
              bottomNavigationBar: new PreferredSize(
                preferredSize: new Size.fromHeight(50.0),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Constants.appColorL2,
                  ),
                  child: BottomAppBar(
                    child: Container(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      color: Constants.appColorL2,
                      height: 60,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          newBottomItem(0, "assests/active_home.png",
                              "assests/inactive_home.png", "HOME"),
                          newBottomItem(1, "assests/active_search.png",
                              "assests/inactive_search.png", "SEARCH"),
                          newBottomItem(2, "assests/genie_active.png",
                              "assests/genie_inactive.png", "GENIE"),
                          newBottomItem(3, "assests/active_livetv.png",
                              "assests/inactive_livetv.png", "LIVE TV"),
                          newBottomItem(4, "assests/active_profile.png",
                              "assests/inactive_profile.png", "PROFILE"),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            )
          : Scaffold(
              backgroundColor: Constants.appColorL2,
              body: renderTab(i),
            ),
    );
  }
}
