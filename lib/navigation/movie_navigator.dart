import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/page.dart';
// import	'package:streaming_entertainment/filter/movie_filter_page.dart';
// import	'package:streaming_entertainment/index/movie_index_page.dart';
// import	'package:streaming_entertainment/profile/movie_profile_page.dart';
// import	'package:streaming_entertainment/search/movie_search_page.dart';
// import	'package:streaming_entertainment/queue/movie_queue_page.dart';
// import	'package:streaming_entertainment/authentication/movie_authentication_page.dart';
// import	'package:streaming_entertainment/queue/movie_queue_page.dart';
// import	'package:streaming_entertainment/authentication/movie_authentication_google.dart';
// import  'package:shared_preferences/shared_preferences.dart';
// import  'package:google_sign_in/google_sign_in.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 	'dart:async';
import 'package:flixjini_webapp/common/constants.dart';

class MovieNavigator extends StatefulWidget {
  MovieNavigator({
    Key? key,
  }) : super(key: key);

  @override
  _MovieNavigatorState createState() => new _MovieNavigatorState();
}

class _MovieNavigatorState extends State<MovieNavigator> {
  int i = 0;

  Widget navigationOptionsIcon(String imageUrl) {
    if (imageUrl.isEmpty) {
      return Container(
        height: 20.0,
      );
    } else {
      return new Container(
        height: 20.0,
        child: new Image.asset(
          imageUrl,
          color: Constants.appColorDivider,
          fit: BoxFit.cover,
        ),
      );
    }
  }

  Widget navigationOptionsTitle(String optionName, int check) {
    //  navigation text color
    if (optionName.isEmpty) {
      return Container(
        height: 20.0,
      );
    } else {
      return new Container(
        height: 20.0,
        child: new Center(
          child: new Text(
            optionName,
            textAlign: TextAlign.right,
            style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorDivider,
                fontSize: 11.0),
          ),
        ),
      );
    }
  }

  Widget newBottomItem(
      int index, String active, String inactive, String title) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 5, bottom: 5),
        decoration: index == 2
            ? BoxDecoration(
                color: Constants.appColorL3,
                borderRadius: BorderRadius.circular(5),
              )
            : null,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: ((index == 2 && false) ? ((i == index) ? 60 : 50) : 20.0),
              child: new Image.asset(
                i == index ? active : inactive,
                fit: BoxFit.cover,
                color: i == index
                    ? index != 2
                        ? Constants.appColorLogo
                        : null
                    : index != 2
                        ? Constants.appColorDivider
                        : null,
              ),
            ),
            index == 2 && false
                ? Container()
                : Container(
                    width: 50,
                    margin: EdgeInsets.only(
                      top: 5,
                    ),
                    child: Text(
                      title,
                      style: TextStyle(
                        color: i == index
                            ? Constants.appColorLogo
                            : Constants.appColorDivider,
                        fontSize: 10,
                        fontWeight:
                            i == index ? FontWeight.bold : FontWeight.normal,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
          ],
        ),
      ),
      onTap: () {
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => MovieRouting(defaultTab: index),
          ),
        );
      },
    );
  }

  Widget universalNavigationBar() {
    return new PreferredSize(
      preferredSize: new Size.fromHeight(50.0),
      child: new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Constants.appColorL2,
        ),
        child: BottomAppBar(
          child: Container(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            color: Constants.appColorL2,
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                newBottomItem(0, "assests/active_home.png",
                    "assests/inactive_home.png", "HOME"),
                newBottomItem(1, "assests/active_search.png",
                    "assests/inactive_search.png", "SEARCH"),
                newBottomItem(2, "assests/genie_active.png",
                    "assests/genie_inactive.png", "GENIE"),
                newBottomItem(3, "assests/active_livetv.png",
                    "assests/inactive_livetv.png", "LIVE TV"),
                newBottomItem(4, "assests/active_profile.png",
                    "assests/inactive_profile.png", "PROFILE"),
              ],
            ),
          ),
        ),
        // child:  BottomNavigationBar(
        //   items: [
        //     new BottomNavigationBarItem(
        //       icon: navigationOptionsIcon("assests/inactive_home.png"),
        //       title: navigationOptionsTitle("HOME", 0),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: navigationOptionsIcon("assests/inactive_search.png"),
        //       title: navigationOptionsTitle("SEARCH", 1),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: navigationOptionsIcon(""),
        //       title: navigationOptionsTitle("", 2),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: navigationOptionsIcon("assests/inactive_queue.png"),
        //       title: navigationOptionsTitle("ACTIVITY", 3),
        //     ),
        //     new BottomNavigationBarItem(
        //       icon: navigationOptionsIcon("assests/inactive_profile.png"),
        //       title: navigationOptionsTitle("PROFILE", 4),
        //     ),
        //   ],
        //   currentIndex: i,
        //   type: BottomNavigationBarType.fixed,
        //   onTap: (index) {
        //     Navigator.of(context).push(new MaterialPageRoute(
        //         builder: (BuildContext context) =>
        //             MovieRouting(defaultTab: index)));
        //   },
        // ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return universalNavigationBar();
  }
}
