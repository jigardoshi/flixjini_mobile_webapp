import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:provider/provider.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/splash/movie_landing_page.dart';
import 'package:flixjini_webapp/detail/movie_detail_page.dart';
import 'deeplink_bloc.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:flixjini_webapp/reviews/movie_reviews_page.dart';
import 'package:flixjini_webapp/common/constants.dart';

import 'navigation/movie_routing.dart';

class MainWidget extends StatelessWidget {
  Widget navigateUserToMovieDetailsPage(String uriPath, String deepLink) {
    printIfDebug('\n\nfrom deep link: $deepLink, uri path: $uriPath');
    if (deepLink.contains('flixjini')) {
      uriPath = '/entertainment' + uriPath;
    }
    uriPath += '.json';
    Map myMap = {
      "urlPhase": uriPath,
      "fullUrl": 'null',
      "search_key": 'null',
    };

    printIfDebug(
        '\n\nurl phase before navigating to the movie details page: $uriPath');

    return MovieDetailPage(
      urlInformation: myMap,
      notification: true,
    );
  }

  Widget navigateUserToScreen(link) {
    String screen = "";
    var urlSplit = link.split("/page/");
    screen = urlSplit[1];
    printIfDebug("screen" + screen);
    switch (screen) {
      case 'genie':
        return MovieRouting(
          defaultTab: 2,
        );

        break;
      case 'search':
        return MovieRouting(
          defaultTab: 1,
        );
        break;
      case 'mylist':
        return MovieRouting(
          defaultTab: 3,
        );
        break;
      case 'profile':
        return MovieRouting(
          defaultTab: 4,
        );
        break;
      case 'index':
        return MovieRouting(
          defaultTab: 0,
        );
        break;
      default:
        return MovieRouting(
          defaultTab: 2,
        );
        break;
    }
  }

  Widget navigateToFilterPage(String uriPath, String deepLink) {
    // if (deepLink.contains('in.flixjini.com/filter')) {
    //   deepLink = deepLink.replaceAll(
    //       'in.flixjini.com/filter', 'www.komparify.com/entertainment');
    // } else if (deepLink.contains('sg.flixjini.com/filter')) {
    //   deepLink = deepLink.replaceAll(
    //       'sg.flixjini.com/filter', 'www.komparify.com/entertainment');
    // } else
    if (deepLink.contains('www.flixjini.in/filter/tags')) {
      deepLink = deepLink.replaceAll(
          'www.flixjini.in/filter', 'api.flixjini.com/entertainment');
    } else if (deepLink.contains('www.flixjini.in/filter/categories')) {
      deepLink = deepLink.replaceAll(
          'www.flixjini.in/filter', 'api.flixjini.com/entertainment');
    }
    if (deepLink.contains("?")) {
      var splitted = deepLink.split("?");
      deepLink = splitted[0];
      deepLink += '.json?';

      deepLink = deepLink + splitted[1];
    } else {
      deepLink += '.json';
    }
    return MovieFilterPage(
      appliedFilterUrl: deepLink,
      inTheaterKey: false,
    );
  }

  Widget navigateToFiltersPage(String uriPath, String deepLink) {
    deepLink = deepLink.replaceAll("www.flixjini.in", "api.flixjini.com");
    deepLink = deepLink.replaceAll("filter", "entertainment");
    deepLink = deepLink.replaceAll("?", ".json?");
    return MovieFilterPage(
      appliedFilterUrl: deepLink,
      inTheaterKey: false,
    );
  }

  openBrowser(link) {
    _launchURL(link);
    return MovieLandingPage();
  }

  void _launchURL(link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Constants.appColorL1,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      // debugprintIfDebug(e.toString());
    }
  }

  Widget navigateToReviewsPage(String uriPath, String deepLink) {
    deepLink = deepLink.replaceAll("www.flixjini.in", "api.flixjini.com");
    deepLink = deepLink.replaceAll("movie", "entertainment/movie");

    deepLink = deepLink + ".json?";
    printIfDebug("link for route" + deepLink);
    return MovieReviewsPage(
      urlPhase: deepLink,
      deepLink: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    // DeepLinkBloc _bloc = Provider.of<DeepLinkBloc>(context);
    return StreamBuilder<String>(
      // stream: _bloc.state,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          // app launch is not from deep link

          printIfDebug('\n\napp is not launched via the deep link');

          return MovieLandingPage(); // navigating to the app's main page (entry page)
        } else {
          // app launch is from deep link
          String? deepLink = snapshot.data;
          printIfDebug('\n\napp is launched via the deep link, "$deepLink"');
          Uri uri = Uri.parse(deepLink!);
          String uriPath = uri.path;
          // showToast('deep link: $deepLink, path: $uriPath', 'long', 'bottom', 1);
          if (deepLink.contains('welcome') || uriPath == '' || uriPath == '/') {
            return MovieLandingPage();
          } else if (deepLink.contains('categories') ||
              deepLink.contains('tags')) {
            return navigateToFilterPage(uriPath, deepLink);
          } else if (deepLink.contains('reviews')) {
            return navigateToReviewsPage(uriPath, deepLink);
          } else if (deepLink.contains('filter')) {
            return navigateToFiltersPage(uriPath, deepLink);
          } else if (deepLink.contains('/movie') ||
              deepLink.contains('/tvshow')) {
            return navigateUserToMovieDetailsPage(uriPath, deepLink);
          } else if (deepLink.contains('/page/')) {
            return navigateUserToScreen(deepLink);
          } else {
            return openBrowser(deepLink);
          }
        }
      },
    );
  }
}
