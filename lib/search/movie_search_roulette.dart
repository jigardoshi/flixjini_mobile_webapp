import 'dart:ui';

import 'package:flixjini_webapp/detail/movie_detail_page.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/signin_alert.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import	'package:streaming_entertainment/filter/movie_filter_card_scores.dart';
// import	'package:streaming_entertainment/filter/movie_filter_user_preferences.dart';
// import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'dart:convert';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSearchRoulette extends StatefulWidget {
  MovieSearchRoulette({
    Key? key,
    this.movieDetail,
    this.userDetail,
    this.userQueue,
    this.authenticationToken,
    this.endOfSearchResults,
    this.changePageNo,
    this.searchResultsController,
    this.dataKey,
    this.showOriginalPosters,
    this.index,
    this.nextPage,
    this.addSkippedId,
    this.searchFilterUserData,
  }) : super(key: key);

  final movieDetail;
  final userDetail;
  final userQueue;
  final authenticationToken;
  final endOfSearchResults;
  final changePageNo;
  final searchResultsController;
  final dataKey;
  final showOriginalPosters;
  final index;
  final nextPage;
  final addSkippedId;
  final searchFilterUserData;

  @override
  _MovieSearchRouletteState createState() => new _MovieSearchRouletteState();
}

class _MovieSearchRouletteState extends State<MovieSearchRoulette>
    with SingleTickerProviderStateMixin {
  Animation<Color>? animation;
  AnimationController? controller;
  var userPreferences;
  var userQueuePreferences;
  var watchList = {"movies": []};
  static const platform = const MethodChannel('api.komparify/advertisingid');

  var scoreList = [
    {
      "name": "Award \nWinner",
      "image": "assests/new_emojis/award_logo.png",
      "score": 0,
      "url": "award_winning",
    },
    {
      "name": "Rating",
      "image": "assests/new_emojis/score_section_like.png",
      "score": 0,
      "url": "imdb_rating",
    },
    {
      "name": "Jini Score",
      "image": "assests/new_emojis/flixjini_score.png",
      "score": 0,
      "url": "k_score",
    },
    {
      "name": "Critics Score",
      "image": "",
      "score": 0,
      "url": "critic_score",
    },
    {
      "name": "Block\nBuster",
      "image": "assests/blockbuster.png",
      "score": 0,
      "url": "blockbuster",
    },
    {
      "name": "Rotten Tomatoes",
      "image": "assests/rt_score.png",
      "score": 0,
      "url": "rt_score",
    },
  ];
  List validRatings = [];
  List seenIds = <String>[];
  int movieIndex = 0;
  bool isQueued = false,
      isSeen = false,
      isDisliked = false,
      skippedCard = true,
      isliked = false;

  @override
  void initState() {
    super.initState();
    setMovieIndex();
    if (!(widget.authenticationToken.isEmpty)) {
      setState(() {
        userPreferences = widget.userDetail;
        userQueuePreferences = widget.userQueue;
      });
      createUserQueue();
    } else {
      printIfDebug(
          "No need for User preferences as user hasn't been authorised");
    }
    // widget.searchResultsController.addListener(() {
    //   if (widget.searchResultsController.position.pixels ==
    //       widget.searchResultsController.position.maxScrollExtent) {
    //     if (widget.endOfSearchResults) {
    //       printIfDebug(widget.endOfSearchResults);
    //       printIfDebug("Dont Send Request to Server to Fetch More Movies");
    //     } else {
    //       printIfDebug("Send Request to Server to Fetch More Movies");
    //       widget.changePageNo();
    //     }
    //   }
    // });
    getSeenMovieIds();
    setScore();
  }

  @override
  void dispose() {
    super.dispose();
  }

  setScore() {
    int length = widget.movieDetail["movies"][movieIndex]["scores"] != null
        ? widget.movieDetail["movies"][movieIndex]["scores"].length
        : 0;

    for (int i = 0; i < length; i++) {
      if (widget.movieDetail["movies"][movieIndex]["scores"]
                  [scoreList[i]["url"]] !=
              null &&
          widget.movieDetail["movies"][movieIndex]["scores"]
                  [scoreList[i]["url"]] >
              0) {
        if (scoreList[i]["url"] == "k_score") {
          scoreList[i]["score"] = (widget.movieDetail["movies"][movieIndex]
                          ["scores"][scoreList[i]["url"]] /
                      10)
                  .round()
                  .toString() +
              "%";
        } else if (scoreList[i]["url"] == "critic_score") {
          scoreList[i]["score"] = (widget.movieDetail["movies"][movieIndex]
                      ["scores"][scoreList[i]["url"]])
                  .round()
                  .toString() +
              "%";
        } else if (scoreList[i]["url"] == "imdb_rating") {
          scoreList[i]["score"] = (widget.movieDetail["movies"][movieIndex]
                  ["scores"][scoreList[i]["url"]])
              .toString();
        } else if (scoreList[i]["url"] == "blockbuster" ||
            scoreList[i]["url"] == "award_winning") {
          scoreList[i]["score"] = "";
        } else {
          scoreList[i]["score"] = widget.movieDetail["movies"][movieIndex]
              ["scores"][scoreList[i]["url"]];
        }

        if (scoreList[i]["url"] == "critic_score") {
          if (widget.movieDetail["movies"][movieIndex]["scores"]
                  [scoreList[i]["url"]] >
              75) {
            scoreList[i]["image"] = "assests/new_emojis/happy_smiley.png";
          } else if (widget.movieDetail["movies"][movieIndex]["scores"]
                  [scoreList[i]["url"]] <
              40) {
            scoreList[i]["image"] = "assests/new_emojis/sad_smiley.png";
          } else {
            scoreList[i]["image"] = "assests/new_emojis/neutral_smiley.png";
          }
        }
        validRatings.add(scoreList[i]);
      }
    }
  }

  setMovieIndex() {
    setState(() {
      movieIndex = widget.index;
    });
  }

  createUserQueue() {
    int size = userQueuePreferences["movies"] != null
        ? userQueuePreferences["movies"].length
        : 0;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        var movie = {
          "id": userQueuePreferences["movies"][i]["id"],
          "status": true,
        };
        setState(() {
          watchList["movies"]!.add(movie);
        });
      }
    }
  }

  basedOnUserQueue(movie) {
    int size = watchList["movies"] != null ? watchList["movies"]!.length : 0;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (watchList["movies"]![i]["id"] == movie["id"]) {
          return watchList["movies"]![i]["status"];
        }
      }
    }
    return false;
  }

  basedonUserPreference(movie, entity) {
    int size = userPreferences["movies"] != null
        ? userPreferences["movies"].length
        : 0;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (userPreferences["movies"][i]["id"] == movie["id"]) {
          return userPreferences["movies"][i][entity];
        }
      }
    }
    return false;
  }

  removeMovieFromWatchList(movie, actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    printIfDebug(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          updateUserQueue(movie);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkAction(movie) {
    int size = watchList["movies"] != null ? watchList["movies"]!.length : 0;
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        if (watchList["movies"]![i]["status"]) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  updateUserQueue(movie) {
    int size = watchList["movies"] != null ? watchList["movies"]!.length : 0;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug("wishlist");
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        setState(() {
          watchList["movies"]![i]["status"] =
              watchList["movies"]![i]["status"] ? false : true;
        });
        flag = 1;
        printIfDebug("Change Existing Entry in Queue");
        break;
      }
    }
    if (flag == 0) {
      var entity = {
        "id": movie["id"],
        "status": true,
      };
      setState(() {
        watchList["movies"]!.add(entity);
      });
    }
  }

  sendRequest(movie, actionUrl, entity, flag1) async {
    bool flag = true;

    String toastMsg = flag1 ? "Added to Queue" : "Removed from the queue";
    if (entity == "seen") {
      toastMsg = "Added to Seen List";
    } else if (entity == "like") {
      toastMsg = !flag1 ? "Removed Like" : "Movie liked";
    } else if (entity == "dislike") {
      toastMsg = !flag1 ? "Removed Dislike" : "Movie disliked";
    } else if (entity == "reset_seen") {
      entity = "seen";
      toastMsg = "Removed from Seen List";
    }

    if (movie == null) {
      if (entity == "queue") {
        flag = movie[entity] ? false : true;
      }
      if (flag) {
        printIfDebug(actionUrl);
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
        };
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              printIfDebug("Authentication Successful and no Errors");
              showToast(toastMsg, 'long', 'bottom', 1, Constants.appColorLogo);
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"] +
            '&jwt_token=' +
            widget.authenticationToken;
        printIfDebug(actionUrl);
        // removeMovieFromWatchList(actionUrl);
      }
    } else {
      if (entity == "queue") {
        flag = movie[entity] ? false : true;
      }
      if (flag) {
        printIfDebug(actionUrl);
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"].toString(),
        };
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              printIfDebug("Authentication Successful and no Errors");

              showToast(toastMsg, 'long', 'bottom', 1, Constants.appColorLogo);
            } else {
              printIfDebug("Could not authorise user / Some error occured");
              showToast('Please try again later', 'long', 'bottom', 1,
                  Constants.appColorLogo);
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"].toString() +
            '&jwt_token=' +
            widget.authenticationToken;
        printIfDebug(actionUrl);
        // removeMovieFromWatchList(actionUrl);

        showToast(
            'Removed from Queue', 'long', 'bottom', 1, Constants.appColorLogo);
      }
    }
  }

  updateUserPreferences(movie, entity) {
    int size = userPreferences["movies"] != null
        ? userPreferences["movies"].length
        : 0;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug(entity);
    for (int i = 0; i < size; i++) {
      if (userPreferences["movies"][i]["id"] == movie["id"]) {
        setState(() {
          userPreferences["movies"][i][entity] =
              userPreferences["movies"][i][entity] ? false : true;
        });
        if ((entity == "like") || (entity == "dislike")) {
          String opposite = (entity == "like") ? "dislike" : "like";
          if (userPreferences["movies"][i][entity] &&
              userPreferences["movies"][i][opposite]) {
            setState(() {
              userPreferences["movies"][i][opposite] =
                  !userPreferences["movies"][i][entity];
            });
          }
        } else {
          printIfDebug(
              "Another option apart from like and dislike have been selected");
        }
        flag = 1;
        printIfDebug("Change Existing Entry");
        break;
      }
    }
    if (flag == 0) {
      var userChoice = {
        "id": movie["id"],
        "like": false,
        "dislike": false,
        "seen": false,
      };
      userChoice[entity] = userChoice[entity] ? false : true;
      setState(() {
        userPreferences["movies"].add(userChoice);
      });
      printIfDebug("Add New Entry");
      printIfDebug(userPreferences);
    }
    printIfDebug(movie["id"]);
  }

  getSeenMovieIds() async {
    setState(() {
      // isQueued = widget.movieDetail["movies"][movieIndex]["in_queue"];
      // isSeen = widget.movieDetail["movies"][movieIndex]["seen"];
    });

    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // seenIds = prefs.getStringList("seenids");
    // printIfDebug("seenids :" + seenIds.toString());
  }

  String getCastString(int count, int length) {
    String returnString = "";
    switch (count) {
      case 1:
        for (int i = 0; i < length; i++) {
          if (i == length - 1) {
            returnString += widget.movieDetail["movies"][movieIndex]
                    ["important_cast"][i]
                .toString()
                .split(" ")
                .elementAt(0);
          } else {
            returnString += widget.movieDetail["movies"][movieIndex]
                    ["important_cast"][i] +
                " • ";
          }
        }
        return returnString;
        break;
      case 2:
        for (int i = 0; i < length; i++) {
          if (i == length - 1) {
            returnString += widget.movieDetail["movies"][movieIndex]
                    ["important_cast"][i]
                .toString()
                .split(" ")
                .elementAt(0);
          } else {
            returnString += widget.movieDetail["movies"][movieIndex]
                        ["important_cast"][i]
                    .toString()
                    .split(" ")
                    .elementAt(0) +
                " • ";
          }
        }
        return returnString;
        break;
      case 3:
        for (int i = 0; i < length - 1; i++) {
          if (i == length - 2) {
            returnString += widget.movieDetail["movies"][movieIndex]
                    ["important_cast"][i]
                .toString()
                .split(" ")
                .elementAt(0);
          } else {
            returnString += widget.movieDetail["movies"][movieIndex]
                        ["important_cast"][i]
                    .toString()
                    .split(" ")
                    .elementAt(0) +
                " • ";
          }
        }
        return returnString;

        break;
      case 4:
        returnString +=
            widget.movieDetail["movies"][movieIndex]["important_cast"][0];
        break;
      default:
    }
    return "";
  }

  Widget filteredMoviesRoulette(height) {
    int numberOfSearchResults = widget.movieDetail["movies"] != null
        ? widget.movieDetail["movies"].length
        : 0;
    int extraLoadBoxes = 0;
    double width = MediaQuery.of(context).size.width;

    if (numberOfSearchResults % 2 == 0) {
      extraLoadBoxes = 2;
    } else {
      extraLoadBoxes = 3;
    }
    if (widget.endOfSearchResults) {
      extraLoadBoxes -= 1;
    }
    if (numberOfSearchResults < 10) {
      extraLoadBoxes = 0;
    }
    // printIfDebug(widget.movieDetail["movies"][movieIndex]["scores"]);
    var urlInformation = {
      "urlPhase": widget.movieDetail["movies"][movieIndex]["link"],
      "fullUrl": null,
      "search_key": null,
    };
    String sourcesList = "";
    int sourcesLen = 0;
    if (widget.movieDetail["movies"][movieIndex]["sources"] != null &&
        widget.movieDetail["movies"][movieIndex]["sources"].length > 0) {
      sourcesLen = widget.movieDetail["movies"][movieIndex]["sources"].length;
      var userFilters = widget.searchFilterUserData != null
          ? widget.searchFilterUserData["sourceslist"]
          : [];
      // printIfDebug(widget.movieDetail["movies"][movieIndex]["sources"].toString() +
      //     " and " +
      //     userFilters.toString());
      if (sourcesLen > 1 &&
          userFilters != null &&
          userFilters.length > 0 &&
          widget.movieDetail["movies"][movieIndex] != null) {
        for (int i = 0; i < sourcesLen; i++) {
          var checkFilter = widget.movieDetail["movies"][movieIndex]["sources"]
                  [i]
              .toString()
              .toLowerCase()
              .replaceFirst(" ", "-");
          if (userFilters.contains(checkFilter)) {
            sourcesList =
                widget.movieDetail["movies"][movieIndex]["sources"][i] ?? "";
            break;
          }
        }
        if (sourcesList.isEmpty) {
          sourcesList =
              widget.movieDetail["movies"][movieIndex]["sources"][0] ?? "";
        }
      } else {
        sourcesList =
            widget.movieDetail["movies"][movieIndex]["sources"][0] ?? "";
      }
      sourcesLen = sourcesLen - 1;
    }
    String year = widget.movieDetail["movies"][movieIndex]["year"] != null
        ? widget.movieDetail["movies"][movieIndex]["year"].toString()
        : "";
    String lang = widget.movieDetail["movies"][movieIndex]["content_type"] !=
            null
        ? " - " +
            widget.movieDetail["movies"][movieIndex]["content_type"].toString()
        : "";
    String castNames = "";
    bool exceeded = true;
    var span;
    if (widget.movieDetail["movies"][movieIndex]["important_cast"] != null) {
      int count =
          widget.movieDetail["movies"][movieIndex]["important_cast"].length > 3
              ? 3
              : widget
                  .movieDetail["movies"][movieIndex]["important_cast"].length;
      for (int i = 0; i < count; i++) {
        if (i == count - 1) {
          castNames +=
              widget.movieDetail["movies"][movieIndex]["important_cast"][i];
        } else {
          castNames += widget.movieDetail["movies"][movieIndex]
                  ["important_cast"][i] +
              " • ";
        }
      }
      int cnt = 0;
      while (exceeded) {
        if (cnt > 0) {
          castNames = getCastString(cnt, count);
        }
        span = TextSpan(
          text: castNames,
          style: TextStyle(fontSize: 12),
        );
        var tp = TextPainter(
          maxLines: 1,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr,
          text: span,
        );
        tp.layout(maxWidth: width - 100);
        exceeded = tp.didExceedMaxLines;
        cnt++;
      }
    }

    return GestureDetector(
      onTap: () {
        widget.changePageNo(0);
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => MovieDetailPage(
              urlInformation: urlInformation,
              showOriginalPosters: widget.showOriginalPosters,
              movieDataFromCall: widget.movieDetail["movies"][movieIndex],
              fromGenie: true,
            ),
          ),
        );
      },
      child: Stack(
        alignment: Alignment.topCenter,
        overflow: Overflow.clip,
        children: <Widget>[
          Container(
            child: Image.asset("assests/no_bg.jpg"),
          ),
          Container(
            color: Constants.appColorL1,
            height: height,
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
              child: Container(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          // top: 20,
                          ),
                      color: Constants.appColorL1,
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            color: Constants.appColorL1,
                            child: GestureDetector(
                              onTap: () {
                                widget.changePageNo(0);
                                Navigator.of(context).push(
                                  new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        MovieDetailPage(
                                      urlInformation: urlInformation,
                                      showOriginalPosters:
                                          widget.showOriginalPosters,
                                      movieDataFromCall: widget
                                          .movieDetail["movies"][movieIndex],
                                      fromGenie: true,
                                    ),
                                  ),
                                );
                              },
                              child: moviePoster(width, height),
                            ),
                          ),
                          widget.showOriginalPosters
                              ? Container(
                                  color: Colors.transparent,
                                  margin: EdgeInsets.only(
                                    bottom: 100,
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      widget.changePageNo(0);
                                      Navigator.of(context).push(
                                        new MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              MovieDetailPage(
                                            urlInformation: urlInformation,
                                            showOriginalPosters:
                                                widget.showOriginalPosters,
                                            movieDataFromCall:
                                                widget.movieDetail["movies"]
                                                    [movieIndex],
                                            fromGenie: true,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Image.asset(
                                      "assests/poster_play_roulette.png",
                                      height: 70,
                                      width: 70,
                                    ),
                                  ),
                                )
                              : BackdropFilter(
                                  filter: new ImageFilter.blur(
                                    sigmaX: 15,
                                    sigmaY: 15,
                                  ),
                                  child: Container(
                                    height: height - 214,
                                    width: width,
                                    color: Colors.transparent.withOpacity(
                                      0.6,
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              // top: width - 40,
              left: 10,
              right: 10,
              // bottom: 10,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: widget.showOriginalPosters
                  ? CrossAxisAlignment.start
                  : CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                        top: 10,
                        left: 10,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Constants.appColorLogo.withOpacity(0.8),
                      ),
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 5,
                        left: 10,
                        right: 10,
                      ),
                      child: Text(
                        sourcesList.toString().toUpperCase(),
                        style: TextStyle(
                          color: Constants.appColorL1,
                          fontWeight: FontWeight.bold,
                          fontSize: 10,
                        ),
                      ),
                    ),
                    sourcesLen > 0
                        ? Container(
                            margin: EdgeInsets.only(
                              top: 10,
                              left: 5,
                            ),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Constants.appColorLogo.withOpacity(0.8),
                            ),
                            child: Text(
                              "+" + sourcesLen.toString(),
                              style: TextStyle(
                                color: Constants.appColorL1,
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                              ),
                            ),
                          )
                        : Container(),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Constants.appColorLogo.withOpacity(0.8),
                      ),
                      margin: EdgeInsets.only(
                        top: 10,
                        left: 5,
                      ),
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 5,
                        left: 10,
                        right: 10,
                      ),
                      child: Text(
                        year + lang.toUpperCase(),
                        style: TextStyle(
                          color: Constants.appColorL1,
                          fontWeight: FontWeight.bold,
                          fontSize: 10,
                        ),
                      ),
                    ),
                  ],
                ),
                Spacer(),
                Container(
                  padding: EdgeInsets.only(
                    // top: 20,
                    // bottom: 5,
                    left: 5,
                  ),
                  margin: EdgeInsets.only(
                    bottom: 10,
                  ),
                  child: Text(
                    widget.movieDetail["movies"][movieIndex]["title"],
                    // "Spider-Man: Far From Home",
                    style: TextStyle(
                      color: Constants.appColorFont,
                      fontSize: 30,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                widget.movieDetail["movies"][movieIndex]
                                ["important_cast"] !=
                            null &&
                        widget
                                .movieDetail["movies"][movieIndex]
                                    ["important_cast"]
                                .length >
                            0 &&
                        !widget.showOriginalPosters
                    ? Container(
                        padding: EdgeInsets.only(
                          // left: 5,
                          // right: 10,
                          bottom: 10,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              castNames,
                              style: TextStyle(
                                color: Constants.appColorDivider,
                                fontSize: 12,
                                height: 1.2,
                              ),
                            ),
                          ],
                        ),
                      )
                    : Container(),
                Container(
                  padding: EdgeInsets.only(
                    left: 5,
                    right: 10,
                    bottom: 15,
                  ),
                  child: Text(
                    widget.movieDetail["movies"][movieIndex]["intro"] != null
                        ? widget.movieDetail["movies"][movieIndex]["intro"]
                        : "",
                    style: TextStyle(
                      color: Constants.appColorFont,
                      fontSize: 12,
                      height: 1.2,
                      // letterSpacing: 0.2,
                    ),
                    maxLines: widget.showOriginalPosters ? 4 : 6,
                    textAlign: widget.showOriginalPosters
                        ? TextAlign.left
                        : TextAlign.center,
                  ),
                ),
                Container(
                  width: (width),
                  color: Colors.transparent, //Constants.appColorL2,
                  child: movieDetails(width, urlInformation),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget movieDetails(width, urlInformation) {
    List<Widget> ratingList = [];

    for (int i = 0;
        i < (validRatings.length <= 3 ? validRatings.length : 3);
        i++) {
      if (widget.movieDetail["movies"][movieIndex]["scores"]
              [scoreList[i]["url"]] !=
          null) {
        var ratingBox = Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  right: 2,
                ),
                child: Image.asset(
                  validRatings[i]["image"],
                  height: 25,
                  width: 25,
                ),
              ),
              Container(
                padding: EdgeInsets.all(2),
                margin: EdgeInsets.only(
                    // right: 5,
                    ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      validRatings[i]["name"],
                      style: TextStyle(
                        color: Constants.appColorFont.withOpacity(0.7),
                        fontSize: 9,
                        fontWeight: FontWeight.w800,
                        letterSpacing: 0.4,
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        validRatings[i]["score"].toString().length > 0
                            ? Container(
                                margin: EdgeInsets.only(
                                  top: 2,
                                ),
                                child: Text(
                                  validRatings[i]["score"].toString(),
                                  style: TextStyle(
                                    color:
                                        Constants.appColorFont.withOpacity(0.7),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              )
                            : Container(),
                        validRatings[i]["name"] == "Rating"
                            ? Container(
                                margin: EdgeInsets.only(
                                  bottom: 1,
                                  left: 1,
                                ),
                                child: Text(
                                  "/10",
                                  style: TextStyle(
                                    color: Constants.appColorDivider,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        );
        ratingList.add(ratingBox);
      }
    }
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: widget.showOriginalPosters ? 5 : 15,
            bottom: widget.showOriginalPosters ? 10 : 10,
            left: 10,
            right: 10,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: ratingList,
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            bottom: widget.showOriginalPosters ? 10 : 30,
            top: widget.showOriginalPosters ? 0 : 20,
          ),
          child: Card(
            elevation: 0,
            color: Colors.transparent,

            // shape: RoundedRectangleBorder(
            //   borderRadius: new BorderRadius.circular(30.0),
            // ),
            child: Container(
              padding: EdgeInsets.only(
                  // top: 5,
                  // bottom: 5,
                  ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  getQueue(),
                  getSeen(),
                  // getWatch(),
                  // getNext(),
                  getLike(),
                  getDislike(),
                ],
              ),
            ),
          ),
        ),
        !widget.showOriginalPosters
            ? Container(
                color: Colors.transparent,
                margin: EdgeInsets.only(
                  bottom: 70,
                  top: 0,
                ),
                child: GestureDetector(
                  onTap: () {
                    widget.changePageNo(0);
                    Navigator.of(context).push(
                      new MaterialPageRoute(
                        builder: (BuildContext context) => MovieDetailPage(
                          urlInformation: urlInformation,
                          showOriginalPosters: widget.showOriginalPosters,
                          movieDataFromCall: widget.movieDetail["movies"]
                              [movieIndex],
                          fromGenie: true,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 7,
                      bottom: 7,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color: Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'WATCH NOW',
                      style: TextStyle(
                          fontSize: 13,
                          color: Constants.appColorL1,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  // child: Image.asset(
                  //   "assests/poster_play_roulette.png",
                  //   height: 70,
                  //   width: 70,
                  // ),
                ),
              )
            : Container(),
      ],
    );
  }

  Widget getQueue() {
    return GestureDetector(
      onTap: () {
        if (!widget.authenticationToken.isEmpty) {
          if (!isQueued) {
            setState(() {
              isQueued = true;
            });
            sendQueueEvent("add");
            sendRequest(widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/v2/add.json', 'add', true);
          } else {
            setState(() {
              isQueued = false;
            });
            sendQueueEvent("remove");
            sendRequest(widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/v2/add.json', 'add', false);
          }

          if (skippedCard) {
            widget.addSkippedId(widget.movieDetail["movies"][movieIndex]["id"]);
          }
          Future.delayed(const Duration(milliseconds: 0), () {
            widget.nextPage();
          });
        } else {
          showAlert();
          // showToast('Login to Add to Queue', 'long',
          //     'bottom', 1, Constants.appColorLogo);
        }
      },
      child: Column(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: //widget.movieDetail["movies"][movieIndex]["in_queue"] ||
                    isQueued ? Constants.appColorLogo : Constants.appColorL2,
              ),
              height: 35,
              width: 35,
              padding: EdgeInsets.all(
                5,
              ),
              child: //!widget.movieDetail["movies"][movieIndex]["in_queue"] ||
                  Image.asset(
                "assests/queue_roulette.png",
                height: 25,
                width: 25,
                color: isQueued ? Constants.appColorL1 : Constants.appColorFont,
              )),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "Watch Later",
              style: TextStyle(
                color:
                    isQueued ? Constants.appColorLogo : Constants.appColorFont,
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }

  sendQueueEvent(String func) {
    Map<String, String> eventData = {
      'content': widget.movieDetail["movies"][movieIndex]["movieName"],
      'func': func,
    };
    //platform.invokeMethod('logAddToWishlistEvent', eventData);
  }

  showAlert() {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return SigninAlertAlert();
      },
    );
  }

  sendSeenRequest(movie, actionUrl, entity) async {
    if (widget.authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      bool flag = true;
      // if (entity == "seen") {
      //   flag = true;
      // }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
          'magic': "false",
          'app': "flixjini-mobile",
        };

        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");

          // ("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          // printIfDebug("seen json: " + jsonResponse.toString());
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              showToast(isSeen ? 'Added to Seen' : "Removed from seen", 'long',
                  'bottom', 1, Constants.appColorLogo);

              // if (entity == "add") {
              //   updateUserQueue(movie);
              // } else {
              //   updateuserDetail(movie, entity);
              // }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      }
      // else {}
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  sendEvent() {
    //platform.invokeMethod('logCustomizeProductEvent');
  }

  Widget getSeen() {
    return GestureDetector(
      onTap: () {
        if (!widget.authenticationToken.isEmpty) {
          changeSkippedFlag(false);

          if (!isSeen) {
            setState(() {
              isSeen = true;
            });
            sendEvent();
            sendRequest(widget.movieDetail["movies"][movieIndex],
                "https://api.flixjini.com/queue/seen.json", "seen", true);
          } else {
            setState(() {
              isSeen = false;
            });
            sendRequest(widget.movieDetail["movies"][movieIndex],
                "https://api.flixjini.com/queue/seen.json", "reset_seen", true);
          }
          if (skippedCard) {
            widget.addSkippedId(widget.movieDetail["movies"][movieIndex]["id"]);
          }
        } else {
          showAlert();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: //widget.movieDetail["movies"][movieIndex]["seen"] ||
                  isSeen ? Constants.appColorLogo : Constants.appColorL2,
            ),
            height: 35,
            width: 35,
            padding: EdgeInsets.all(
              5,
            ),
            child: Image.asset(
              "assests/seen_roulette.png",
              height: 25,
              width: 25,
              color: isSeen ? Constants.appColorL1 : Constants.appColorFont,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "Seen",
              style: TextStyle(
                color: isSeen ? Constants.appColorLogo : Constants.appColorFont,
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }

  changeSkippedFlag(bool flag) {
    setState(() {
      skippedCard = false;
    });
  }

  Widget getDislike() {
    return GestureDetector(
      onTap: () {
        if (!widget.authenticationToken.isEmpty) {
          changeSkippedFlag(false);

          if (!isDisliked) {
            setState(() {
              isDisliked = true;
              isliked = false;
            });
            sendEvent();
            sendRequest(widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/dislike.json', "dislike", true);
          } else {
            setState(() {
              isDisliked = false;
            });
            sendRequest(
                widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/dislike.json',
                "dislike",
                false);
          }
          if (skippedCard) {
            widget.addSkippedId(widget.movieDetail["movies"][movieIndex]["id"]);
          }
        } else {
          showAlert();
        }
        widget.nextPage();
      },
      child: Column(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color:
                    isDisliked ? Constants.appColorLogo : Constants.appColorL2,
              ),
              height: 35,
              width: 35,
              padding: EdgeInsets.all(
                5,
              ),
              child: Image.asset(
                "assests/dislike_roulette.png",
                height: 20,
                width: 20,
                color:
                    isDisliked ? Constants.appColorL1 : Constants.appColorFont,
              )),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "Dislike",
              style: TextStyle(
                color: isDisliked
                    ? Constants.appColorLogo
                    : Constants.appColorFont,
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getLike() {
    return GestureDetector(
      onTap: () {
        if (!widget.authenticationToken.isEmpty) {
          changeSkippedFlag(false);

          if (!isliked) {
            setState(() {
              isliked = true;
              isDisliked = false;
            });
            sendEvent();
            sendRequest(widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/like.json', "like", true);
          } else {
            setState(() {
              isliked = false;
            });
            sendRequest(widget.movieDetail["movies"][movieIndex],
                'https://api.flixjini.com/queue/like.json', "like", false);
          }
          if (skippedCard) {
            widget.addSkippedId(widget.movieDetail["movies"][movieIndex]["id"]);
          }
        } else {
          showAlert();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isliked ? Constants.appColorLogo : Constants.appColorL2,
            ),
            height: 35,
            width: 35,
            padding: EdgeInsets.all(
              5,
            ),
            child: Image.asset(
              "assests/like_roulette.png",
              height: 20,
              width: 20,
              color: isliked ? Constants.appColorL1 : Constants.appColorFont,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "Like",
              style: TextStyle(
                color:
                    isliked ? Constants.appColorLogo : Constants.appColorFont,
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getNext() {
    return GestureDetector(
      onTap: () {
        if (skippedCard) {
          widget.addSkippedId(widget.movieDetail["movies"][movieIndex]["id"]);
        }
        widget.nextPage();
      },
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.appColorL2,
            ),
            height: 35,
            width: 35,
            padding: EdgeInsets.all(
              5,
            ),
            child: Image.asset(
              "assests/next_roulette.png",
              height: 25,
              width: 25,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "Next",
              style: TextStyle(
                color: Constants.appColorDivider,
                fontSize: 10,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget moviePoster(width, height) {
    String moviePosterUrl =
        widget.movieDetail["movies"][movieIndex]["full_image_uri"] != null
            ? widget.movieDetail["movies"][movieIndex]["full_image_uri"]
            : widget.movieDetail["movies"][movieIndex]["image_uri"];
    return moviePosterUrl != null && moviePosterUrl.length > 1
        ? Container(
            color: Constants.appColorL1,
            child: ShaderMask(
              shaderCallback: (rect) {
                return LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Constants.appColorL1, Colors.transparent],
                ).createShader(
                  Rect.fromLTRB(
                    0,
                    0,
                    width,
                    widget.showOriginalPosters
                        ? height * 0.75 // width + 40
                        : height - 100,
                  ),
                );
              },
              blendMode: BlendMode.dstIn,
              child: Image.network(
                moviePosterUrl,
                height:
                    widget.showOriginalPosters ? height * 0.75 : height - 214,
                fit: BoxFit.fitHeight,
              ),
            ),
          )
        : Container(
            color: Constants.appColorL1,
            child: ShaderMask(
              shaderCallback: (rect) {
                return LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Constants.appColorL1, Colors.transparent],
                ).createShader(
                  Rect.fromLTRB(
                    0,
                    0,
                    width,
                    width + 40,
                  ),
                );
              },
              blendMode: BlendMode.dstIn,
              child: Image.asset(
                "assests/no_bg.jpg",
                height: width * 1.3,
                fit: BoxFit.fitHeight,
              ),
            ),
          );
  }

  Widget topButtons() {
    return Row(
      children: <Widget>[
        Container(
          child: Image.asset(
            "assests/back_roulette.png",
            height: 40,
            width: 40,
          ),
        ),
        Spacer(),
        Container(
          child: Image.asset(
            "assests/filter_roulette.png",
            height: 40,
            width: 40,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    double height = MediaQuery.of(context).size.height;
    // var orientation = MediaQuery.of(context).orientation;
    try {
      return filteredMoviesRoulette(
        height,
      );
    } catch (e) {
      printIfDebug("genie exceptions" + e.toString());
    }
    return Container();
  }
}
