import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import	'package:streaming_entertainment/filter/movie_filter_card_scores.dart';
// import	'package:streaming_entertainment/filter/movie_filter_user_preferences.dart';
import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSearchResults extends StatefulWidget {
  MovieSearchResults({
    Key? key,
    this.movieDetail,
    this.userDetail,
    this.userQueue,
    this.authenticationToken,
    this.endOfSearchResults,
    this.changePageNo,
    this.searchResultsController,
    this.dataKey,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieDetail;
  final userDetail;
  final userQueue;
  final authenticationToken;
  final endOfSearchResults;
  final changePageNo;
  final searchResultsController;
  final dataKey;
  final showOriginalPosters;

  @override
  _MovieSearchResultsState createState() => new _MovieSearchResultsState();
}

class _MovieSearchResultsState extends State<MovieSearchResults>
    with SingleTickerProviderStateMixin {
  Animation<Color>? animation;
  AnimationController? controller;
  var userPreferences;
  var userQueuePreferences;
  var watchList = {"movies": []};
  List seenIds = <String>[];

  @override
  void initState() {
    super.initState();
    if (!(widget.authenticationToken.isEmpty)) {
      setState(() {
        userPreferences = widget.userDetail;
        userQueuePreferences = widget.userQueue;
      });
      createUserQueue();
    } else {
      printIfDebug(
          "No need for User preferences as user hasn't been authorised");
    }
    widget.searchResultsController.addListener(() {
      if (widget.searchResultsController.position.pixels ==
          widget.searchResultsController.position.maxScrollExtent) {
        if (widget.endOfSearchResults) {
          printIfDebug(widget.endOfSearchResults);
          printIfDebug("Dont Send Request to Server to Fetch More Movies");
        } else {
          printIfDebug("Send Request to Server to Fetch More Movies");
          widget.changePageNo();
        }
      }
    });
    getSeenMovieIds();
  }

  createUserQueue() {
    int size = userQueuePreferences["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        var movie = {
          "id": userQueuePreferences["movies"][i]["id"],
          "status": true,
        };
        setState(() {
          watchList["movies"]!.add(movie);
        });
      }
    }
    // printIfDebug("Movie Watchlist");
    // printIfDebug(watchList);
    printIfDebug("DOne");
  }

  basedOnUserQueue(movie) {
    int size = watchList["movies"]!.length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (watchList["movies"]![i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          return watchList["movies"]![i]["status"];
        }
      }
    }
    return false;
  }

  basedonUserPreference(movie, entity) {
    int size = userPreferences["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (userPreferences["movies"][i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          return userPreferences["movies"][i][entity];
        }
      }
    }
    return false;
  }

  removeMovieFromWatchList(movie, actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    printIfDebug(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          updateUserQueue(movie);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkAction(movie) {
    int size = watchList["movies"]!.length;
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        if (watchList["movies"]![i]["status"]) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  updateUserQueue(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug("wishlist");
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        setState(() {
          watchList["movies"]![i]["status"] =
              watchList["movies"]![i]["status"] ? false : true;
        });
        flag = 1;
        printIfDebug("Change Existing Entry in Queue");
        break;
      }
    }
    if (flag == 0) {
      var entity = {
        "id": movie["id"],
        "status": true,
      };
      setState(() {
        watchList["movies"]!.add(entity);
      });
      printIfDebug("Add New Entry to Queue");
      printIfDebug(watchList);
    }
    printIfDebug(movie["id"]);
  }

  sendRequest(movie, actionUrl, entity) async {
    printIfDebug((widget.authenticationToken).isEmpty);
    if (!widget.authenticationToken.isEmpty) {
      bool flag = true;
      if (entity == "add") {
        flag = checkAction(movie);
      }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
        };
        var url = actionUrl;
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              if (entity == "add") {
                updateUserQueue(movie);
              } else {
                updateUserPreferences(movie, entity);
              }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"] +
            '&jwt_token=' +
            widget.authenticationToken;
        removeMovieFromWatchList(movie, actionUrl);
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  updateUserPreferences(movie, entity) {
    int size = userPreferences["movies"].length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug(entity);
    for (int i = 0; i < size; i++) {
      if (userPreferences["movies"][i]["id"] == movie["id"]) {
        setState(() {
          userPreferences["movies"][i][entity] =
              userPreferences["movies"][i][entity] ? false : true;
        });
        if ((entity == "like") || (entity == "dislike")) {
          String opposite = (entity == "like") ? "dislike" : "like";
          if (userPreferences["movies"][i][entity] &&
              userPreferences["movies"][i][opposite]) {
            setState(() {
              userPreferences["movies"][i][opposite] =
                  !userPreferences["movies"][i][entity];
            });
          }
        } else {
          printIfDebug(
              "Another option apart from like and dislike have been selected");
        }
        flag = 1;
        printIfDebug("Change Existing Entry");
        break;
      }
    }
    if (flag == 0) {
      var userChoice = {
        "id": movie["id"],
        "like": false,
        "dislike": false,
        "seen": false,
      };
      userChoice[entity] = userChoice[entity] ? false : true;
      setState(() {
        userPreferences["movies"].add(userChoice);
      });
      printIfDebug("Add New Entry");
      printIfDebug(userPreferences);
    }
    printIfDebug(movie["id"]);
  }

  getSeenMovieIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    seenIds = prefs.getStringList("seenids")!;
    printIfDebug("seenids :" + seenIds.toString());
  }

  Widget movieCardBuilder(index) {
    return new CardStructure(
      movie: widget.movieDetail["movies"][index],
      sendRequest: sendRequest,
      basedOnUserQueue: basedOnUserQueue,
      basedonUserPreference: basedonUserPreference,
      authenticationToken: widget.authenticationToken,
      showOriginalPosters: widget.showOriginalPosters,
      pagefor: 2,
      seenIds: seenIds,
    );
  }

  Widget filteredMoviesGrid(height, orientation) {
    int numberOfSearchResults = widget.movieDetail["movies"].length;
    int extraLoadBoxes = 0;
    if (numberOfSearchResults % 2 == 0) {
      extraLoadBoxes = 2;
    } else {
      extraLoadBoxes = 3;
    }
    if (widget.endOfSearchResults) {
      extraLoadBoxes -= 1;
    }
    if (numberOfSearchResults < 10) {
      extraLoadBoxes = 0;
    }
    return new Container(
      padding: const EdgeInsets.all(0.0),
      color: Constants.appColorL1,
      height: height,
      child: AnimationLimiter(
        child: new GridView.builder(
          controller: widget.searchResultsController,
          key: widget.dataKey,
          physics: new BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 0.63,
              mainAxisSpacing: 2.0,
              // crossAxisSpacing: 5.0,
              crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
          // padding: const EdgeInsets.all(20.0),
          itemCount: numberOfSearchResults + extraLoadBoxes,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: const Duration(milliseconds: 375),
              columnCount: (orientation == Orientation.portrait) ? 2 : 3,
              child: ScaleAnimation(
                child: FadeInAnimation(
                  child: index < numberOfSearchResults
                      ? GridTile(
                          child: new GestureDetector(
                            onTap: () {
                              printIfDebug("Image Id that has been clicked");
                              printIfDebug(widget.movieDetail["movies"][index]
                                  ["movieName"]);
                            },
                            child: movieCardBuilder(index),
                          ),
                        )
                      : widget.endOfSearchResults
                          ? GridTile(
                              child: new GestureDetector(
                                onTap: () {
                                  printIfDebug(
                                      "No More Search Results to Show");
                                },
                                child: new Container(
                                  // color: Constants.appColorL2,
                                  margin: EdgeInsets.all(10),
                                  child: new Center(
                                    child: new Text(
                                      "No More Results",
                                      textAlign: TextAlign.center,
                                      style: new TextStyle(
                                          fontWeight: FontWeight.normal,
                                          color: Constants.appColorFont,
                                          fontSize: 15.0),
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : GridTile(
                              child: new GestureDetector(
                                onTap: () {
                                  printIfDebug("Trigger Load More");
                                },
                                child: new Container(
                                  decoration: BoxDecoration(
                                    color: Constants.appColorDivider
                                        .withOpacity(0.75),
                                    shape: BoxShape.circle,
                                  ),
                                  child: new Center(
                                    child: new CircularProgressIndicator(
                                      backgroundColor: Constants.appColorLogo,
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Constants.appColorLogo),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    double height = MediaQuery.of(context).size.height - 120.0;
    var orientation = MediaQuery.of(context).orientation;
    return filteredMoviesGrid(height, orientation);
  }
}
