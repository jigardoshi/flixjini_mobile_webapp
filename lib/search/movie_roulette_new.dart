import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/search/movie_search_roulette.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class MovieRouletteNew extends StatefulWidget {
  MovieRouletteNew({
    Key? key,
    this.movieDetail,
    this.userDetail,
    this.userQueue,
    this.authenticationToken,
    this.endOfSearchResults,
    this.changePageNo,
    this.searchResultsController,
    this.dataKey,
    this.showOriginalPosters,
    this.currentPage,
    this.addSkippedId,
    this.ctrl,
    this.searchFilterUserData,
  }) : super(key: key);
  final movieDetail;
  final userDetail;
  final userQueue;
  final authenticationToken;
  final endOfSearchResults;
  final changePageNo;
  final searchResultsController;
  final dataKey;
  final showOriginalPosters;
  final currentPage;
  final addSkippedId;
  final ctrl;
  final searchFilterUserData;
  createState() => MovieRouletteNewState();
}

class MovieRouletteNewState extends State<MovieRouletteNew> {
  // final Firestore db = Firestore.instance;
  late Stream slides;

  int currentPage = 0, newPage = 0, newLoadPage = 0;
  bool apiFlag = true, apiLoadFlag = true;

  @override
  initState() {
    super.initState();

    widget.ctrl.addListener(() {
      int next = widget.ctrl.page.round();
      widget.addSkippedId(widget.movieDetail["movies"][next]["id"]);

      printIfDebug("nextttttttt" +
          next.toString() +
          "  total " +
          widget.movieDetail["movies"].length.toString());
      if (next == widget.movieDetail["movies"].length - 1) {
        if (apiLoadFlag) {
          widget.changePageNo(1);
        }
        setState(() {
          apiLoadFlag = false;
          newLoadPage = next + 1;
        });
      }
      if (next == newLoadPage) {
        setState(() {
          apiLoadFlag = true;
        });
      }
      if (next % 5 == 0) {
        if (apiFlag) {
          widget.changePageNo(0);
        }
        setState(() {
          apiFlag = false;
          newPage = next + 1;
        });
      }
      if (next == newPage) {
        setState(() {
          apiFlag = true;
        });
      }
      if (currentPage != next) {
        setState(() {
          currentPage = next;
        });
      }
    });
  }

  nextPage() {
    int next = widget.ctrl.page.round() + 1;

    widget.ctrl.animateToPage(next,
        duration: Duration(
          milliseconds: 500,
        ),
        curve: Curves.easeIn);
  }

  resetPage() {
    printIfDebug("Reseting pageeeeeeeeeeeeeeeeee");
    widget.ctrl.animateToPage(0,
        duration: Duration(
          milliseconds: 500,
        ),
        curve: Curves.easeIn);
  }

  @override
  Widget build(BuildContext context) {
    try {
      return StreamBuilder(
          // stream: slides,
          // initialData: [],
          builder: (context, AsyncSnapshot snap) {
        // snap.data.toList();

        return PageView.builder(
            controller: widget.ctrl,
            itemCount: widget.movieDetail["movies"].length + 1,
            itemBuilder: (context, int currentIdx) {
              bool active = currentIdx == currentPage;
              if (currentIdx == widget.movieDetail["movies"].length) {
                return _buildLoaderPage(true, currentIdx);
              } else {
                return _buildStoryPage(active, currentIdx);
              }
            });
      });
    } catch (e) {
      printIfDebug(e.toString());

      return Container();
    }
  }

  // Builder Functions

  _buildStoryPage(bool active, int i) {
    // Animated Properties
    // final double blur = active ? 30 : 0;
    // final double offset = active ? 20 : 0;
    final double top = active ? 10 : 75;
    final double left = active ? 0 : 20;
    final double right = active ? 0 : 20;

    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      curve: Curves.easeOutQuint,
      margin: EdgeInsets.only(
        top: top,
        bottom: 50,
        right: right,
        left: left,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
          boxShadow: [
            BoxShadow(
              color: Constants.appColorShadow,
              offset: Offset(3.0, 6.0),
              blurRadius: 20.0,
              spreadRadius: 2.0,
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: MovieSearchRoulette(
            movieDetail: widget.movieDetail,
            userDetail: widget.userDetail,
            userQueue: widget.userQueue,
            authenticationToken: widget.authenticationToken,
            endOfSearchResults: widget.endOfSearchResults,
            changePageNo: widget.changePageNo,
            searchResultsController: widget.searchResultsController,
            dataKey: widget.dataKey,
            showOriginalPosters: widget.showOriginalPosters,
            index: i,
            nextPage: nextPage,
            addSkippedId: widget.addSkippedId,
            searchFilterUserData: widget.searchFilterUserData,
          ),
        ),
      ),
    );
  }

  _buildLoaderPage(bool active, int i) {
    // Animated Properties
    // final double blur = active ? 30 : 0;
    // final double offset = active ? 20 : 0;
    final double top = active ? 10 : 75;
    final double left = active ? 0 : 20;
    final double right = active ? 0 : 20;

    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      curve: Curves.easeOutQuint,
      margin: EdgeInsets.only(
        top: top,
        bottom: 50,
        right: right,
        left: left,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Constants.appColorShadow,
          boxShadow: [
            BoxShadow(
              color: Constants.appColorShadow,
              offset: Offset(3.0, 6.0),
              blurRadius: 20.0,
              spreadRadius: 2.0,
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 50,
                  bottom: 20,
                ),
                child: Text(
                  "Loading...",
                  style: TextStyle(
                    color: Constants.appColorFont,
                    // letterSpacing: 1,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 300,
                color: Constants.appColorL1,
                // height: 300,
                padding: EdgeInsets.only(
                  left: 30,
                  right: 30,
                ),
                child: FlareActor(
                  "assests/star.flr",
                  alignment: Alignment.center,
                  fit: BoxFit.cover,
                  animation: "loader",
                ),
                // getProgressBar(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
