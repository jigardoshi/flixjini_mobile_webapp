import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'dart:convert';
// import 	'dart:io';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
// import	'package:streaming_entertainment/queue/movie_queue_watchlist.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import	'package:streaming_entertainment/navigation/movie_navigator.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/detail/movie_detail_page.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_popular_search.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSearchPopular extends StatefulWidget {
  MovieSearchPopular({
    Key? key,
    this.showOriginalPoster,
  }) : super(key: key);
  final showOriginalPoster;

  @override
  _MovieSearchPopularState createState() => new _MovieSearchPopularState();
}

class _MovieSearchPopularState extends State<MovieSearchPopular> {
  String authenticationToken = "";
  String stringPopularMovies = "";
  bool fetchedpopularSearchDetails = false,
      fetchedRecentSearch = false,
      showMoreFlag = false,
      ipError = false;
  var popularSearchDetails, recentSearchData;

  @override
  void initState() {
    super.initState();
    authenticationStatus();
    loadPopularSearches();
  }

  getFilterMenu() async {
    String url = 'https://api.flixjini.com/entertainment/fresh_movies.json?';
    url = authenticationToken.isNotEmpty
        ? url + "jwt_token=" + authenticationToken + "&"
        : url;
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    printIfDebug(url);

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.body);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.popularSearchDetails = data;
        setState(() {
          popularSearchDetails = this.popularSearchDetails;
          fetchedpopularSearchDetails = true;
        });
      } else {
        this.popularSearchDetails["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.popularSearchDetails["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  formatPopularMoviesData(data) {
    if (data.containsKey("streams") && data.containsKey("movies")) {
      var popular = []..addAll(data["streams"])..addAll(data["movies"]);
      return popular;
    } else if (data.containsKey("streams")) {
      return data["streams"];
    } else if (data.containsKey("movies")) {
      return data["movies"];
    } else {
      return [];
    }
  }

  deleteSearch(String mid) async {
    String url =
        'https://api.flixjini.com/roulette/delete_user_search_history/' +
            mid +
            ".json?";
    url = url + "jwt_token=" + authenticationToken + "&";
    String finalUrl = constructHeader(url);

    finalUrl = await fetchDefaultParams(finalUrl);

    try {
      var response = await http.get(Uri.parse(finalUrl));
      printIfDebug(response.body);
      if (response.statusCode == 200) {
      } else {
        printIfDebug("Error");
      }
    } catch (exception) {
      printIfDebug(exception);
    }
    if (!mounted) return;
  }

  getRecentSearch() async {
    String url = 'https://api.flixjini.com/roulette/user_search_history.json?';
    url = authenticationToken.isNotEmpty
        ? url + "jwt_token=" + authenticationToken + "&"
        : url;
    String finalUrl = constructHeader(url);

    finalUrl = await fetchDefaultParams(finalUrl);

    try {
      var response = await http.get(Uri.parse(finalUrl));
      printIfDebug(response.body);
      String responseBody = response.body;
      var data = json.decode(responseBody);
      if (response.statusCode == 200 && data[0] != null) {
        if (data[0].toString().isNotEmpty) {
          this.recentSearchData = data;
          setState(() {
            recentSearchData = this.recentSearchData;
            fetchedRecentSearch = true;
          });
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('stringRecentSearch', json.encode(recentSearchData));
        }
      } else {
        printIfDebug("Error");
      }
    } catch (exception) {
      printIfDebug(exception);
    }
    if (!mounted) return;
  }

  loadPopularSearches() async {
    printIfDebug("Fetch Popular Searches");
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // setState(() {
    //   stringPopularMovies = (prefs.getString('stringPopularMovies') ?? "");
    //   // recentSearchDataString = (prefs.getString('stringRecentSearch') ?? "");
    // });
    // // if (recentSearchDataString.isNotEmpty) {
    // //   var data = json.decode(recentSearchDataString);
    // //   printIfDebug("recent" + data.toString());
    // //   setState(() {
    // //     recentSearchData = data;
    // //     fetchedRecentSearch = true;
    // //   });
    // // }
    // if (stringPopularMovies.isEmpty) {
    //   printIfDebug("No Popular Movies Found");
    // } else {
    //   var data = json.decode(stringPopularMovies);
    //   printIfDebug("popular" + data.toString());
    //   data = formatPopularMoviesData(data);
    //   setState(() {
    //     popularSearchDetails = data;
    //     fetchedpopularSearchDetails = true;
    //   });
    //   // getFilterMenu();
    // }
    fetchPopularMovies();
  }

  fetchPopularMovies() async {
    String url = "https://api.flixjini.com/entertainment/fresh_movies.json?";

    // String url = getAppurl() + widget.urlName.toString().split("?").first + "?";
    url += (authenticationToken != null && authenticationToken.isNotEmpty
        ? "jwt_token=" + authenticationToken + "&"
        : "");
    url = constructHeader(url);
    url = await fetchDefaultParams(url);

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for fresh streaming page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("popular" + data.toString());
        setState(() {
          popularSearchDetails = data["streams"];
          fetchedpopularSearchDetails = true;
        });
      } else {
        setState(() {
          ipError = true;
        });
        printIfDebug(
            'Error getting fresh streaming IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      setState(() {
        ipError = true;
      });
      printIfDebug(
          '\n\n fresh streaming page api, exception caught is: $exception');
    }
  }

  setRecentSearchSP(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (data != null) {
      prefs.setString('stringRecentSearch', json.encode(data));
    } else {
      prefs.setString('stringRecentSearch', "");
    }
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    String recentSearchDataString = "";

    // getFilterMenu();
    if (authenticationToken.isNotEmpty) {
      setState(() {
        recentSearchDataString = (prefs.getString('stringRecentSearch') ?? "");
      });
      if (recentSearchDataString.isNotEmpty) {
        var data = json.decode(recentSearchDataString);
        printIfDebug("recent" + data.toString());
        setState(() {
          recentSearchData = data;
          fetchedRecentSearch = true;
        });
      }
      getRecentSearch();
    }
  }

  Widget subheading(String title) {
    return new Container(
      height: 30.0,
      margin: EdgeInsets.only(
        top: 20.0,
        left: 5.0,
        bottom: 20,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.only(
              left: 10,
            ),
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                  width: 4.0,
                  color: Constants.appColorLogo,
                ),
              ),
            ),
            child: Center(
              child: new Text(
                title,
                textAlign: TextAlign.center,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Constants.appColorFont,
                  fontSize: 17.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  navigateToMovieDetailPage(String urlValue) {
    String urlTitleTag = urlValue + ".json";
    String urlName = "";
    if (authenticationToken.isEmpty) {
      urlName = "https://api.flixjini.com/entertainment/" + urlTitleTag + '.json?';
    } else {
      urlName = "https://api.flixjini.com/entertainment/" +
          urlTitleTag +
          ".json?" +
          "jwt_token=" +
          authenticationToken +
          "&";
    }
    String urlPhase = '/entertainment/' + urlTitleTag;
    //print("You have selected");
    //print(urlName);
    var urlInformation = {
      "urlPhase": urlPhase,
      "fullUrl": urlName,
    };
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new MovieDetailPage(
              urlInformation: urlInformation,
              showOriginalPosters: widget.showOriginalPoster,
            )));
  }

  Widget goToMovieDetailPageSection() {
    return new Expanded(
      child: new Center(
        child: new Container(
          padding: const EdgeInsets.all(0.0),
          decoration: BoxDecoration(),
          child: new Icon(
            Icons.play_arrow,
            size: 20.0,
            color: Constants.appColorDivider.withOpacity(0.75),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget deleteSearchContainer(String mid, int index) {
    return new Expanded(
      child: GestureDetector(
        onTap: () {
          printIfDebug("delete");
          var deleteData = [];
          deleteData = recentSearchData;
          deleteData.removeAt(index);
          if (recentSearchData.length == 1) {
            setState(() {
              recentSearchData = null;
              fetchedRecentSearch = false;
            });
          } else {
            setState(() {
              recentSearchData = deleteData;
            });
            printIfDebug(recentSearchData.toString());
          }
          setRecentSearchSP(recentSearchData);
          deleteSearch(mid);
        },
        child: new Center(
          child: new Container(
            padding: const EdgeInsets.all(0.0),
            decoration: BoxDecoration(),
            child: new Icon(
              Icons.close,
              size: 17.0,
              color: Constants.appColorDivider.withOpacity(0.75),
            ),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget movieTitle(String title) {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.only(
          left: 20.0,
          top: 10,
          bottom: 10,
        ),
        child: new Text(
          title,
          textAlign: TextAlign.left,
          style: new TextStyle(
            fontWeight: FontWeight.normal,
            color: Constants.appColorFont.withOpacity(0.75),
            fontSize: 15.0,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      flex: 1,
    );
  }

  Widget posterSection(int index) {
    if (popularSearchDetails[index]["poster"] != null) {
      return widget.showOriginalPoster
          ? Expanded(
              child: new Container(
                margin: EdgeInsets.only(
                  top: 10,
                  bottom: 10,
                ),
                alignment: AlignmentDirectional.center,
                child: new Center(
                    child: ClipRRect(
                  borderRadius: new BorderRadius.circular(2.0),
                  child: Image.network(
                    popularSearchDetails[index]["poster"],
                    height: 40.0,
                    width: 40.0,
                    fit: BoxFit.cover,
                  ),
                )),
              ),
              flex: 1,
            )
          : Container(
              height: 60.0,
              width: 10,
            );
    } else {
      return new Expanded(
        child: new Container(),
        flex: 1,
      );
    }
  }

  Widget informationSection(String title, String lang) {
    return new Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              title,
              textAlign: TextAlign.left,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorFont,
                fontSize: 14.0,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 5,
            ),
            child: Text(
              lang.toUpperCase(),
              textAlign: TextAlign.left,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorFont,
                fontSize: 9.0,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
      flex: 4,
    );
  }

  Widget informationTitle(String title) {
    return new Expanded(
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          movieTitle(title),
        ],
      ),
      flex: 4,
    );
  }

  Widget popularSearchResults(int index) {
    return GestureDetector(
      onTap: () {
        printIfDebug("Item has been selected");
        printIfDebug(popularSearchDetails[index]);
        navigateToMovieDetailPage(popularSearchDetails[index]["url"]);
      },
      child: new Container(
        color: Constants.appColorL1,
        child: new Card(
          color: Constants.appColorL2,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              posterSection(index),
              // Expanded(
              //   flex: 4,
              //   child: Column(
              //     // mainAxisAlignment: MainAxisAlignment.center,
              //     // crossAxisAlignment: CrossAxisAlignment.start,
              //     children: <Widget>[
              informationSection(popularSearchDetails[index]["title"],
                  popularSearchDetails[index]["language"]),
              //     ],
              //   ),
              // ),
              goToMovieDetailPageSection(),
            ],
          ),
        ),
      ),
    );
  }

  Widget popularSearchListView() {
    return new Container(
      color: Constants.appColorL1,

      child: AnimationLimiter(
        child: new ListView.builder(
          physics: new NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          itemCount: popularSearchDetails.length,
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(0.0),
          itemBuilder: (context, index) {
            return AnimationConfiguration.staggeredList(
              position: index,
              duration: const Duration(milliseconds: 375),
              child: SlideAnimation(
                verticalOffset: 50.0,
                child: FadeInAnimation(
                  child: popularSearchResults(index),
                ),
              ),
            );
          },
        ),
      ),
      // ),
    );
  }

  Widget recentSearchListView(bool flag) {
    int fixLength = recentSearchData.length > 5 ? 5 : recentSearchData.length;
    return new Container(
      color: Constants.appColorL1,
      child: AnimationLimiter(
        child: new ListView.builder(
            physics: new NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            addAutomaticKeepAlives: false,
            itemCount: flag ? fixLength : recentSearchData.length - 5,
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(0.0),
            itemBuilder: (context, index) {
              return AnimationConfiguration.staggeredList(
                position: index,
                duration: const Duration(milliseconds: 375),
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: recentSearchResults(
                      flag ? index : index + 5,
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }

  Widget recentSearchResults(int index) {
    // return Container(
    //   child: Text(recentSearchData[index]["movie"]["title"]),
    // );
    return GestureDetector(
      onTap: () {
        printIfDebug("Item has been selected");
        navigateToMovieDetailPage(recentSearchData[index]["movie"]["url"]);
      },
      child: new Container(
        color: Constants.appColorL1,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // posterSection(index),
            informationTitle(recentSearchData[index]["movie"]["title"]),
            deleteSearchContainer(
                recentSearchData[index]["movie"]["id"].toString(), index),
          ],
        ),
      ),
    );
  }

  deleteAllSearch() {
    setState(() {
      fetchedRecentSearch = false;
    });

    for (int i = 0; i < recentSearchData.length; i++) {
      deleteSearch(recentSearchData[i]["movie"]["id"].toString());
    }
    setState(() {
      recentSearchData = null;
    });
    setRecentSearchSP(recentSearchData);
  }

  Widget recentSearchContainer() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            subheading("RECENT SEARCHES"),
            GestureDetector(
              onTap: () {
                deleteAllSearch();
              },
              child: Container(
                margin: EdgeInsets.only(
                  right: 5,
                ),
                child: Text(
                  "CLEAR ALL",
                  style: TextStyle(
                    color: Color(
                      0XFF43C681,
                    ),
                    fontSize: 12,
                  ),
                ),
              ),
            ),
          ],
        ),
        recentSearchListView(true),
        showMoreFlag ? recentSearchListView(false) : Container(),
        Container(
          margin: EdgeInsets.only(
            top: 10,
            left: 15,
            right: 15,
          ),
          height: 2,
          color: Constants.appColorDivider.withOpacity(0.1),
        ),
      ],
    );
  }

  showMore() {
    return Center(
      child: recentSearchData != null && recentSearchData.length > 5
          ? GestureDetector(
              onTap: () {
                setState(() {
                  showMoreFlag = !showMoreFlag;
                });
              },
              child: Container(
                width: 60,
                margin: EdgeInsets.only(
                  bottom: 10,
                ),
                child: !showMoreFlag
                    ? Image.asset("assests/down_arrow_search.png")
                    : Image.asset("assests/up_arrow_search.png"),
              ),
            )
          : Container(),
    );
  }

  Widget popularSearch() {
    return new SingleChildScrollView(
      child: new ConstrainedBox(
        constraints: new BoxConstraints(),
        child: new Container(
          color: Constants.appColorL1,
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10.0,
          ),
          child: new Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (fetchedRecentSearch &&
                      recentSearchData != null &&
                      recentSearchData.length > 0)
                  ? recentSearchContainer()
                  : Container(),
              showMore(),
              subheading("POPULAR SEARCHES"),
              popularSearchListView(),
            ],
          ),
        ),
      ),
    );
  }

  Widget displayLoader() {
    return new Container(
      color: Constants.appColorL1,
      child: new Center(
        child: ShimmerPopularSearch(),
      ),
    );
  }

  Widget checkDataReady() {
    if (fetchedpopularSearchDetails) {
      return popularSearch();
    } else {
      return displayLoader();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return checkDataReady();
  }
}
