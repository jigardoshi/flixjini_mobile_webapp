import 'package:flixjini_webapp/fresh/genie_saved_filter.dart';
import 'package:flixjini_webapp/search/roulette_filter_slider.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:ui';
import 'package:flixjini_webapp/common/constants.dart';

import 'package:shared_preferences/shared_preferences.dart';

class MovieSearchFilter extends StatefulWidget {
  MovieSearchFilter({
    Key? key,
    this.updateFilterMenu,
    this.searchFilterData,
    this.setToDisplayOnTitle,
    this.toogleOptionStatus,
    this.filterSearchResults,
    this.setFilterMenu,
    this.showFilter,
    this.changeFreeFLag,
    this.setSelectedFilterStatus,
    this.resetSelectedFilterStatus,
    this.updateMinMaxRange,
    this.resetRangeOptions,
    this.userSavedFilters,
    this.authenticationToken,
    this.loadSavedFilters,
    this.savedFilterResults,
    this.setSavedFilterStatus,
  }) : super(key: key);

  final updateFilterMenu;
  final searchFilterData;
  final setToDisplayOnTitle;
  final toogleOptionStatus;
  final filterSearchResults;
  final setFilterMenu;
  final showFilter;
  final changeFreeFLag;
  final setSelectedFilterStatus;
  final resetSelectedFilterStatus;
  final updateMinMaxRange;
  final resetRangeOptions;
  final userSavedFilters;
  final authenticationToken;
  final loadSavedFilters;
  final savedFilterResults;
  final setSavedFilterStatus;

  @override
  _MovieSearchFilterState createState() => new _MovieSearchFilterState();
}

class _MovieSearchFilterState extends State<MovieSearchFilter> {
  bool arrowDirectionRight = true,
      cancelFlag = false,
      applyFlag = false,
      filterError = true;

  ScrollController secondaryFilterController = new ScrollController();

  var searchFilterFreeData = {
    "topics": [
      {
        "options": [
          {
            "name": "Only Free Content",
            "status": false,
          },
          {
            "name": "All Content",
            "status": false,
          }
        ]
      }
    ]
  };

  var exclude = ["devicetypeslist", "streamingtypelist", "movietypelist"];
  void initState() {
    super.initState();
    setFreeStatus();
  }

  setFreeStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool flag1 = prefs.getBool('freeflag') ?? false;
    if (flag1) {
      setState(() {
        searchFilterFreeData["topics"]![0]["options"]![0]["status"] = true;
        searchFilterFreeData["topics"]![0]["options"]![1]["status"] = false;
      });
    } else {
      setState(() {
        searchFilterFreeData["topics"]![0]["options"]![0]["status"] = false;
        searchFilterFreeData["topics"]![0]["options"]![1]["status"] = true;
      });
    }
    printIfDebug(widget.searchFilterData["filters"]["streamingtypelist"]);
    printIfDebug(widget.searchFilterData["filters"]["streamingtypelist"]
        .contains("subscription"));

    if (widget.searchFilterData["filters"]["streamingtypelist"] != null &&
        widget.searchFilterData["filters"]["streamingtypelist"]
            .contains("subscription")) {
      setState(() {
        searchFilterFreeData["topics"]![0]["options"]![1]["status"] = true;
      });
    } else if (widget.searchFilterData["filters"]["streamingtypelist"] !=
            null &&
        !widget.searchFilterData["filters"]["streamingtypelist"]
            .contains("subscription") &&
        widget.searchFilterData["filters"]["streamingtypelist"]
            .contains("free")) {
      setState(() {
        searchFilterFreeData["topics"]![0]["options"]![0]["status"] = true;
      });
    }
  }

  changeArrowDirection(value) {
    setState(() {
      arrowDirectionRight = value;
    });
  }

  var options = ["Types", "Added", "Sources"];

  Widget setIconColor(index) {
    return new Container(
      padding: const EdgeInsets.all(1.0),
      child: new Image.asset(
        "assests/" + widget.searchFilterData["topics"][index]["url"],
        // "assests/like.png",
        color: Constants.appColorLogo,
        fit: BoxFit.contain,
        width: 20.0,
        height: 20.0,
      ),
    );
  }

  highlightMainOptionIfAnyOfItsSubOptionsApplied(index) {
    printIfDebug(",,,,,,,,,,,,,,,," + index.toString());
    int optionsLength =
        widget.searchFilterData["topics"][index]["options"].length;
    for (int i = 0; i < optionsLength; i++) {
      if (widget.searchFilterData["topics"][index]["options"][i]["status"]) {
        return true;
      }
    }
    return false;
  }

  highlightFreeOptions() {
    int optionsLength = searchFilterFreeData["topics"]![0]["options"]!.length;
    for (int i = 0; i < optionsLength; i++) {
      if (searchFilterFreeData["topics"]![0]["options"]![i]["status"] == true) {
        return true;
      }
    }
    return false;
  }

  resetFreeOptions(index) {
    int optionsLength = searchFilterFreeData["topics"]![0]["options"]!.length;
    for (int i = 0; i < optionsLength; i++) {
      setState(() {
        searchFilterFreeData["topics"]![0]["options"]![i]["status"] = false;
      });
    }
    searchFilterFreeData["topics"]![0]["options"]![index]["status"] = true;
  }

  discover() {
    return Container(
      margin: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 40,
      ),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              widget.showFilter(false);
            },
            child: Card(
              color: Constants.appColorL2,
              // with Card
              child: Image.asset(
                "assests/back_roulette.png",
                height: 40,
                width: 40,
              ),
              elevation: 18.0,
              shape: CircleBorder(),
              clipBehavior: Clip.antiAlias,
            ),
          ),
          Spacer(),
          Text(
            "GENIE",
            style: TextStyle(
              color: Constants.appColorFont.withOpacity(0.7),
              fontSize: 18,
            ),
          ),
          Spacer(),
          GestureDetector(
            child: Container(
              height: 40,
              width: 40,
            ),
            // Card(
            //   color: Constants.appColorL2,
            //   // with Card
            //   child: Image.asset(
            //     "assests/filter_roulette.png",
            //     height: 40,
            //     width: 40,
            //   ),
            //   elevation: 18.0,
            //   shape: CircleBorder(),
            //   clipBehavior: Clip.antiAlias,
            // ),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget filterUI(screenHeight, screenWidth) {
    List<Widget> filterList1 = [];

    List<Widget> filterList = [];
    // filterList.add(discover());
    filterList.add(GenieSavedFilter(
      userSavedFilters: widget.userSavedFilters,
      authenticationToken: widget.authenticationToken,
      loadSavedFilters: widget.loadSavedFilters,
      savedFilterResults: widget.savedFilterResults,
      setSavedFilterStatus: widget.setSavedFilterStatus,
    ));
    filterList.add(
      freeAll(context),
    );
    var filterSection;
    for (int i = 0; i < widget.searchFilterData["topics"].length; i++) {
      if (widget.searchFilterData["topics"][i] != null) {
        if (widget.searchFilterData["topics"][i]["heading"]
                .toString()
                .toUpperCase() ==
            "SOURCES") {
          filterSection = displayStreamingCats(context, i);
        } else if (widget.searchFilterData["topics"][i]["group"]
                .toString()
                .toUpperCase() ==
            "SLIDER") {
          filterSection = displaySliderCats(context, i);
        } else {
          filterSection = displayFilterCats(context, i);
        }
        filterList1.add(
          filterSection,
        );
      }
    }
    filterList.add(filterList1[1]);
    filterList.add(filterList1[0]);
    filterList.add(filterList1[3]);
    filterList.add(filterList1[2]);
    filterList.add(filterList1[4]);
    // filterList.add(filterList1[5]); //subs type
    filterList.add(filterList1[6]); //device type
    filterList.add(filterList1[7]);
    filterList.add(filterList1[8]);
    // filterList.add(filterList1[9]);

    return Container(
      height: screenHeight - 210,
      width: screenWidth,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: filterList,
        ),
      ),
    );
  }

  Widget displaySliderCats(context, i) {
    return RouletteFilterSlider(
      filterData: widget.searchFilterData,
      counter: i,
      updateMinMaxRange: widget.updateMinMaxRange,
      resetRangeOptions: widget.resetRangeOptions,
      filterSearchResults: widget.filterSearchResults,
    );
  }

  applySave() {
    return Container(
      height: 56,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border(
          top: BorderSide(
            color: Constants.appColorLogo.withOpacity(0.5),
            width: 2,
          ),
        ),
        color: Constants.appColorL2,
      ),
      // padding: EdgeInsets.only(
      //   // left: 10,
      //   // right: 10,
      //   top: 6,
      //   bottom: 6,
      // ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                cancelFlag = true;
              });
              Future.delayed(
                  const Duration(
                    milliseconds: 250,
                  ), () {
                widget.showFilter(false);
              });
            },
            child: Card(
              elevation: 5,
              color: Constants.appColorL3,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child: Container(
                decoration: new BoxDecoration(
                  color: Constants.appColorL3,
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  top: 10,
                  bottom: 10,
                ),
                child: Text(
                  'CANCEL',
                  style: TextStyle(
                    color: cancelFlag
                        ? Constants.appColorLogo
                        : Constants.appColorFont,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          // Container(
          //   color: Constants.appColorL1,
          //   height: 20,
          //   width: 1,
          // ),
          GestureDetector(
            onTap: () {
              setState(() {
                applyFlag = true;
              });
              widget.filterSearchResults();

              Future.delayed(const Duration(milliseconds: 100), () {
                widget.showFilter(false);
              });
            },
            child: Card(
              elevation: 5,
              color: Constants.appColorLogo,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0),
              ),
              child: Container(
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  top: 10,
                  bottom: 10,
                ),
                decoration: new BoxDecoration(
                  color: Constants.appColorLogo,
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                child: Text(
                  'PROCEED',
                  style: TextStyle(
                    color: !applyFlag
                        ? Constants.appColorL1
                        : Constants.appColorFont,
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  displayFilterCats(context, index) {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
        left: 0,
        right: 0,
      ),
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: Card(
        elevation: 2.5,
        margin: EdgeInsets.only(
          left: 1,
          right: 1,
        ),
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 20,
              margin: EdgeInsets.only(
                top: 15,
                left: 10,
                bottom: 10,
              ),
              child: Text(
                widget.searchFilterData["topics"][index]["heading"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  letterSpacing: 1.0,
                  color:
                      // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                      //     ? Constants.appColorLogo
                      //     :
                      Constants.appColorFont,
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            Container(
              // height: 200,
              child: GridView.builder(
                  addAutomaticKeepAlives: false,
                  shrinkWrap: true,
                  physics: new NeverScrollableScrollPhysics(),
                  itemCount: widget
                      .searchFilterData["topics"][index]["options"].length,
                  // scrollDirection: Axis.vertical,
                  controller: secondaryFilterController,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 2 : 12,
                  ),
                  padding: EdgeInsets.all(
                    10,
                  ),
                  itemBuilder: (BuildContext context1, int i) {
                    return displayCheckboxes(context1, i, index);
                  }),
            ),

            // insertDivider(),
          ],
        ),
      ),
    );
  }

  freeAll(context) {
    var orientation = MediaQuery.of(context).orientation;

    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
      ),
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: Card(
        elevation: 2.5,
        margin: EdgeInsets.only(
          left: 1,
          right: 1,
        ),
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 20,
              margin: EdgeInsets.only(
                top: 15,
                left: 10,
                bottom: 10,
              ),
              child: Text(
                "CONTENT PRICING",
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                  letterSpacing: 1.0,
                  color:
                      // highlightFreeOptions()
                      //     ? Constants.appColorLogo
                      //     :
                      Constants.appColorFont,
                ),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
            Container(
              // height: 200,
              child: GridView.builder(
                  addAutomaticKeepAlives: false,
                  shrinkWrap: true,
                  physics: new NeverScrollableScrollPhysics(),
                  itemCount: 2,
                  //widget
                  //  .searchFilterData["topics"][index]["options"].length,
                  // scrollDirection: Axis.vertical,
                  controller: secondaryFilterController,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 2 : 12,
                  ),
                  padding: EdgeInsets.all(
                    10,
                  ),
                  itemBuilder: (BuildContext context1, int i) {
                    return displayFreeCheckboxes(context1, i);
                  }),
            ),

            // insertDivider(),
          ],
        ),
      ),
    );
  }

  Widget displayFreeCheckboxes(context, i) {
    return Card(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 5,
        right: 5,
      ),

      color: Constants.appColorL3,
      // elevation: 5.0,
      child: Container(
        color: Constants.appColorL3,
        child: new InkWell(
          onTap: () {
            widget.changeFreeFLag();
            resetFreeOptions(i);
            widget.filterSearchResults();
          },
          child: new Container(
            // margin: EdgeInsets.all(5.0),
            padding: EdgeInsets.all(5.0),

            // width: 100.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: searchFilterFreeData["topics"]![0]["options"]![i]
                          ["status"] ==
                      true
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            child: new Center(
              child: new Text(
                searchFilterFreeData["topics"]![0]["options"]![i]["name"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: searchFilterFreeData["topics"]![0]["options"]![i]
                              ["status"] ==
                          true
                      ? Constants.appColorL1
                      : Constants.appColorFont.withOpacity(0.75),
                ),
                textScaleFactor: 0.5,
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  displayStreamingCats(context, index) {
    bool selectedSectionStatus = checkIfAnyAreSelected(index);

    var orientation = MediaQuery.of(context).orientation;

    return
        // Container();
        Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
      ),
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: Card(
        elevation: 2.5,
        color: Constants.appColorL2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: 15,
                    left: 10,
                    bottom: 10,
                  ),
                  child: Text(
                    widget.searchFilterData["topics"][index]["heading"]
                        .toString()
                        .toUpperCase(),
                    overflow: TextOverflow.ellipsis,
                    style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 14.0,
                      color:
                          // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                          //     ? Constants.appColorLogo
                          //     :
                          Constants.appColorFont,
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    widget.setSelectedFilterStatus(index);
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 15,
                      bottom: 10,
                      right: 5,
                    ),
                    child: Text(
                      "MY APPS",
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 12.0,
                        color:
                            // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                            //     ? Constants.appColorLogo
                            //     :
                            Constants.appColorDivider,
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 15,
                    bottom: 10,
                    left: 5,
                    right: 5,
                  ),
                  child: Text(
                    "|",
                    style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color:
                          // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                          //     ? Constants.appColorLogo
                          //     :
                          Constants.appColorDivider,
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    widget.resetSelectedFilterStatus(index);
                  },
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 15,
                      bottom: 10,
                    ),
                    margin: EdgeInsets.only(left: 5, right: 10),
                    child: Text(
                      "RESET",
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 12.0,
                        color:
                            // highlightMainOptionIfAnyOfItsSubOptionsApplied(index)
                            //     ? Constants.appColorLogo
                            //     :
                            Constants.appColorLogo.withOpacity(0.5),
                      ),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    ),
                  ),
                ),
              ],
            ),

            Container(
              padding: EdgeInsets.only(
                left: 10,
                right: 10,
                bottom: 10,
              ),
              child: GridView.builder(
                  addAutomaticKeepAlives: false,
                  shrinkWrap: true,
                  physics: new NeverScrollableScrollPhysics(),
                  itemCount: widget
                      .searchFilterData["topics"][index]["options"].length,
                  controller: secondaryFilterController,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 1,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 5 : 12,
                  ),
                  padding: EdgeInsets.all(
                    10,
                  ),
                  itemBuilder: (BuildContext context1, int i) {
                    return displayStreamingCheckboxes(
                        index, i, selectedSectionStatus);
                  }),
            ),
            // insertDivider(),
          ],
        ),
      ),
    );
  }

  Widget insertDivider() {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(
        bottom: 10,
      ),
      height: 1,
      width: width - 10,
      color: Constants.appColorDivider,
    );
  }

  bool checkIfAnyAreSelected(index) {
    bool flag = true;
    for (int i = 0;
        i < widget.searchFilterData["topics"][index]["options"].length;
        i++) {
      if (widget.searchFilterData["topics"][index]["options"][i]["status"]) {
        flag = false;
        printIfDebug("Category Checking");
        printIfDebug(widget.searchFilterData["topics"][index]["heading"]);
        break;
      }
    }
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  basedOnshadeAllOptions(int index, int i, bool selectedSectionStatus) {
    if (selectedSectionStatus) {
      return 0.0;
    } else {
      return widget.searchFilterData["topics"][index]["options"][i]["status"]
          ? 0.0
          : 0.6;
    }
  }

  Widget displayStreamingCheckboxes(index, i, selectedSectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectedSectionStatus);
    return Container(
      // color: Constants.appColorL3,
      // padding: const EdgeInsets.all(1.0),
      // width: 35.0,
      // height: 35.0,
      child: new Card(
        elevation: 5.0,
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () {
            widget.setToDisplayOnTitle(
                widget.searchFilterData["topics"][index], i);
            widget.toogleOptionStatus(index, i);
            widget.filterSearchResults();
            printIfDebug("Send Request to server with User Preferences");
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: getStreamingSourceImage(
                    index,
                    i,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getStreamingSourceImage(
    int index,
    int i,
  ) {
    // if (showOriginalImages == 'true') {
    return Container(
      color: Constants.appColorL3,
      child: Image(
        image: NetworkImage(
          widget.searchFilterData["topics"][index]["options"][i]["image"],
          // useDiskCache: false,
        ),
        fit: BoxFit.fill,
        gaplessPlayback: true,
      ),
    );
    // } else {
    //   return Container(
    //     color: Constants.appColorL3,
    //     child: Image.asset(
    //       'assests/streaming_sources/' +
    //           getLocalImageFileNumber(
    //             widget.profileSettingData["topics"][index]["options"][i]
    //                 ["image"],
    //           ),
    //     ),
    //   );
    // }
  }

  Widget displayCheckboxes(context, i, index) {
    return Card(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 5,
        right: 5,
      ),

      color: Constants.appColorL3,
      // elevation: 5.0,
      child: Container(
        color: Constants.appColorL3,
        // padding: const EdgeInsets.only(
        //   left: 5.0,
        //   right: 5.0,
        //   top: 10,
        //   bottom: 10,
        // ),

        child: new InkWell(
          onTap: () {
            // widget.updateClickableImages(index, i);
            widget.setToDisplayOnTitle(
                widget.searchFilterData["topics"][index], i);
            widget.toogleOptionStatus(index, i);
            widget.filterSearchResults();
          },
          child: new Container(
            // margin: EdgeInsets.all(5.0),
            padding: EdgeInsets.all(5.0),

            // width: 100.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: widget.searchFilterData["topics"][index]["options"][i]
                      ["status"]
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
              // border: new Border.all(
              //   color: widget.searchFilterData["topics"][index]["options"][i]
              //           ["status"]
              //       ? Constants.appColorLogo
              //       : Constants.appColorDivider,
              //   width: 1.0,
              // ),
              // borderRadius: new BorderRadius.circular(25.0),
            ),
            child: new Center(
              child: new Text(
                widget.searchFilterData["topics"][index]["options"][i]["name"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: widget.searchFilterData["topics"][index]["options"][i]
                          ["status"]
                      ? Constants.appColorL1
                      : Constants.appColorFont.withOpacity(0.75),
                ),
                textScaleFactor: 0.5,
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    try {
      return Container(
        color: Constants.appColorL1,
        // height: screenHeight,
        // width: screenWidth,
        padding: EdgeInsets.only(
            // top: 40,

            ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            discover(),
            filterUI(screenHeight, screenWidth),
            applySave(),
          ],
        ),
      );
    } catch (e) {
      printIfDebug("filter exception" + e.toString());
      Future.delayed(
          const Duration(
            milliseconds: 3000,
          ), () {
        setState(() {
          filterError = false;
        });
      });
      return filterError
          ? Container(
              color: Constants.appColorL1,
              height: screenHeight,
              width: screenWidth,
              child: getProgressBar(),
            )
          : Container();
    }
  }
}
