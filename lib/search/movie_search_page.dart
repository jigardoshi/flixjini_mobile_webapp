import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';

import 'package:flutter/services.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
// import	'package:streaming_entertainment/movie_detail_navigation.dart';
import 'package:flixjini_webapp/search/movie_search_bar.dart';
import 'package:flixjini_webapp/search/movie_search_filter.dart';
import 'package:flixjini_webapp/search/movie_search_results.dart';
import 'package:flixjini_webapp/search/movie_search_popular.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_cards.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'dart:ui';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSearchPage extends StatefulWidget {
  MovieSearchPage({
    this.moveToPreviousTab,
    this.showOriginalPosters,
    Key? key,
  }) : super(key: key);

  final moveToPreviousTab;
  final showOriginalPosters;

  @override
  _MovieSearchPageState createState() => new _MovieSearchPageState();
}

class _MovieSearchPageState extends State<MovieSearchPage> {
  ScrollController searchResultsController = new ScrollController();
  final dataKey = new GlobalKey();
  final TextEditingController searchController = new TextEditingController();
  bool fetchedMovieDetail = false;
  bool fetchedUserPreference = false;
  bool fetchedUserQueuePreference = false;
  bool endOfSearchResults = false;
  bool filterMenuStatus = false;
  bool searchFilterDataReady = false;
  bool unkownResults = false, suggestionFlag = false;
  int pageNo = 1;
  var sortData = {};
  var movieDetail = {};
  var userDetail = {};
  var userQueue = {};
  var searchFilterData;
  var options = [];
  var suggestionData = [];
  bool suggestionDataReady = false;
  bool firstSearch = true;
  bool loadingSymbolStatusElasticSearch = false;
  bool loadingSymbolFetchMoreResults = false;
  bool resultsHaveBeenCleared = false;
  bool searchBoxEmpty = true;
  int chosenIndex = 0;
  String searchString = "";
  String checkString = "";
  String authenticationToken = "";
  String filtersSet = "";
  String stringSecondaryMenu = "";
  double screenWidth = 0.0;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    printIfDebug("Welcome to Search Page");
    loadStaticData();
    authenticationStatus();
    getSearchFilterMenu();
    logUserScreen('Flixjini Search Page', 'MovieSearchPage');
  }

  prepareForSearchSecondaryMenu(secondaryMenu) {
    for (int index = 0; index < secondaryMenu["topics"].length; index++) {
      if (!secondaryMenu["topics"][index]["belongsTo"].contains("search")) {
        secondaryMenu["topics"].removeAt(index);
      }
    }
    return secondaryMenu;
  }

  loadStaticData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringSecondaryMenu = (prefs.getString('stringSecondaryMenu') ?? "{}");
    });
    printIfDebug("Search Menu in String Form");
    printIfDebug(stringSecondaryMenu);
    Map data = json.decode(stringSecondaryMenu);
    data = {"topics": ""}; //prepareForSearchSecondaryMenu(data);
    setState(() {
      searchFilterData = data;
      searchFilterDataReady = true;
    });
    printIfDebug("Search Filter Menu");
    printIfDebug(searchFilterData["topics"]);
  }

  getSearchFilterMenu() async {
    String filterMenuOptionsUrl =
        "https://api.flixjini.com/movies/get_user_profile.json?jwt_token=" +
            authenticationToken;
    String rootUrl = filterMenuOptionsUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));

      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("Connect to profile");
        printIfDebug(data);
      } else {
        this.searchFilterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.searchFilterData["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  void toogleSearchBoxEmpty(value) {
    setState(() {
      searchBoxEmpty = value;
    });
  }

  getPreferences() {
    getUserPreference();
    getUserQueuePreference();
  }

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  setDefaultMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] = false;
    });
    printIfDebug("Update Status");
    printIfDebug(searchFilterData["topics"][i]);
  }

  toogleMenuState(i) {
    setState(() {
      searchFilterData["topics"][i]["status"] =
          searchFilterData["topics"][i]["status"] ? false : true;
      filterMenuStatus = searchFilterData["topics"][i]["status"];
      chosenIndex = i;
    });
  }

  toogleLoadingSymbolStatusElasticSearch(bool value) {
    printIfDebug("Elastic Search State Changed");
    printIfDebug(new DateTime.now());
    setState(() {
      loadingSymbolStatusElasticSearch = value;
    });
    printIfDebug(loadingSymbolStatusElasticSearch);
  }

  toogleLoadingSymbolFetchMoreResults(bool value) {
    setState(() {
      loadingSymbolFetchMoreResults = value;
    });
  }

  toogleOptionStatus(int chosenIndex, int index) {
    bool filterStatus = false;
    printIfDebug(searchFilterData["topics"][chosenIndex]["options"].length);
    for (int i = 0;
        i < searchFilterData["topics"][chosenIndex]["options"].length;
        i++) {
      if (i == index) {
        if (searchFilterData["topics"][chosenIndex]["options"][i]["status"]) {
          filterStatus = false;
        } else {
          filterStatus = true;
        }
      } else {
        filterStatus = false;
      }
      setState(() {
        searchFilterData["topics"][chosenIndex]["options"][i]["status"] =
            filterStatus;
      });
    }
  }

  setFilterMenu() {
    setState(() {
      filterMenuStatus = false;
    });
    for (int i = 0; i < searchFilterData["topics"].length; i++) {
      setState(() {
        searchFilterData["topics"][i]["status"] = false;
      });
    }
  }

  updateFilterMenu(index) {
    int categories = searchFilterData["topics"].length;
    for (int i = 0; i < categories; i++) {
      if (index == i) {
        toogleMenuState(i);
      } else {
        setDefaultMenuState(i);
      }
    }
  }

  clearSearchResults() {
    setState(() {
      unkownResults = false;
      fetchedMovieDetail = false;
    });
    printIfDebug("Calling Clear Search Results");
    printIfDebug(resultsHaveBeenCleared);
    if (movieDetail.isNotEmpty) {
      if (movieDetail.containsKey("movies")) {
        if (movieDetail["movies"].isNotEmpty) {
          printIfDebug("Search Results Cleared");
          setState(() {
            movieDetail["movies"] = [];
            resultsHaveBeenCleared = true;
          });
          printIfDebug(unkownResults);
          printIfDebug(movieDetail["movies"]);
        }
      }
    }
  }

  changePageNo() {
    setState(() {
      pageNo += 1;
    });
    printIfDebug("Move to Page Number");
    printIfDebug(pageNo);
    String pageStartsAtMovieNumber = ((pageNo * 70) - 70).toString();
    String url = constructUrlWithFilters();
    String appliedUrl = url + "&elements=" + pageStartsAtMovieNumber;
    getMoreSearchResults(appliedUrl);
  }

  void movetoSourceCard() {
    printIfDebug("Go To Index 1");
    try {
      (searchResultsController).animateTo(
          (20.0 *
              1), // 100 is the height of container and index of 6th element is 5
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    } catch (exception) {
      printIfDebug(
          'List Does not exist to move to the top -> This is an Exception');
    }
  }

  getMoreSearchResults(String urlWithSearchParameter) async {
    String rootUrl = urlWithSearchParameter + '&';
    String url = constructHeader(rootUrl);
    printIfDebug("Paginate to Page");
    printIfDebug(url);
    toogleLoadingSymbolFetchMoreResults(true);
    var response = await http.get(Uri.parse(url));
    try {
      printIfDebug("Entered Try Block");
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("More Search Results Obtained");
        if (data["movies"].length > 0) {
          var infiniteData = new List.from(movieDetail["movies"])
            ..addAll(data["movies"]);
          setState(() {
            movieDetail["movies"] = infiniteData;
          });
          printIfDebug("Movies Fetched, New Length");
          printIfDebug(movieDetail["movies"].length);
        } else {
          printIfDebug("Reached End of Search Results");
          changeSearchResultsStatus(true);
        }
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        changeSearchResultsStatus(true);
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
      toogleLoadingSymbolFetchMoreResults(false);
    } catch (exception) {
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  changeSearchResultsStatus(value) {
    setState(() {
      endOfSearchResults = value;
    });
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      printIfDebug("User is already Logged in -> Obtain User Preferences");
      getPreferences();
    } else {
      printIfDebug("User can't access User Preferences as he is not logged in");
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }
  }

  getUserQueuePreference() async {
    String url = 'https://api.flixjini.com/queue/list.json?jwt_token=' +
        authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
        printIfDebug("Queue Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Queue Failed getting IP address');
    }
    if (!mounted) return;
  }

  getUserPreference() async {
    String url =
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("User Preference");
        printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
        printIfDebug("Get all activity Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  sendElasticGetRequest(actionUrl) async {
    printIfDebug("LOL");
    setState(() {
      unkownResults = false;
      firstSearch = true;
    });
    String rootUrl = actionUrl +
        '&include_actor_stuff=true&suggestion=true&include_channels=true&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.statusCode);
      printIfDebug(response.statusCode == 200);
      if (response.statusCode == 200) {
        var fetchedData = json.decode(response.body);
        printIfDebug(fetchedData);
        printIfDebug("Not yet In - resultsHaveBeenCleared");
        printIfDebug(resultsHaveBeenCleared);
        printIfDebug("Yes we In");
        setState(() {
          movieDetail = fetchedData;
          fetchedMovieDetail = true;
          suggestionDataReady = true;
        });
        if (movieDetail["movies"].length == 0) {
          setState(() {
            unkownResults = true;
          });
        }
        toogleLoadingSymbolStatusElasticSearch(false);
        printIfDebug("New Data Fetched by Elastic Search");
        changeSearchResultsStatus(false);
        printIfDebug(firstSearch);
        setState(() {
          pageNo = 1;
        });
        printIfDebug("Fresh Batch of reviews");
        printIfDebug(pageNo);
        movetoSourceCard();
        if (!firstSearch) {
          printIfDebug("Go to Top");
          movetoSourceCard();
        } else {
          setState(() {
            firstSearch = false;
            resultsHaveBeenCleared = false;
          });
          printIfDebug("Reset resultsHaveBeenCleared");
          printIfDebug(resultsHaveBeenCleared);
        }
      } else {
        this.movieDetail["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.movieDetail["error"] = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  // When and How to Trigger Request to Elastic Search For Search Results
  Future delayServerRequest() async {
    if (searchString.length >= 2) {
      setState(() {
        pageNo = 1;
      });
      toogleLoadingSymbolStatusElasticSearch(true);
      String checkString = searchString;
      printIfDebug("Before Timeout");
      printIfDebug("Search (Current) String");
      printIfDebug(searchString);
      printIfDebug("Check (Previous) String");
      printIfDebug(checkString);
      new Timer(const Duration(seconds: 1), () {
        printIfDebug("After Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        if (searchString != checkString) {
          printIfDebug("Dont Make Server Request");
        } else {
          printIfDebug("Make Server Request : Elastic Search");
          String appliedMoviesFilterUrl = constructUrlWithFilters();
          printIfDebug(appliedMoviesFilterUrl);
          sendElasticGetRequest(appliedMoviesFilterUrl);
        }
      });
    }
  }

  Widget displayMovieDetails() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        new MovieSearchResults(
          movieDetail: movieDetail,
          userDetail: userDetail,
          userQueue: userQueue,
          authenticationToken: authenticationToken,
          endOfSearchResults: endOfSearchResults,
          changePageNo: changePageNo,
          searchResultsController: searchResultsController,
          dataKey: dataKey,
          showOriginalPosters: widget.showOriginalPosters,
        ),
      ],
    );
  }

  Widget closeOption() {
    if (filterMenuStatus) {
      return new Positioned(
        top: 25.0,
        right: 20.0,
        child: new GestureDetector(
          child: new Container(
            child: new Container(
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: new Image.asset(
                "assests/close_circle.png",
                width: 25.0,
                height: 25.0,
              ),
            ),
          ),
          onTap: () {
            setFilterMenu();
          },
        ),
      );
    } else {
      return new Container();
    }
  }

  Widget buildOptions(BuildContext context, int index) {
    printIfDebug("GGGG");
    printIfDebug(index);
    return new Center(
      child: new Container(
        width: 100.0,
        height: 70.0,
        child: new Text(options[index]["name"]),
      ),
    );
  }

  constructUrlWithFilters() {
    filtersSet = "";
    String url =
        "https://api.flixjini.com/entertainment/filter_retrieve.json?jwt_token=" +
            authenticationToken +
            "&search=" +
            searchString;
    for (int category = 0;
        category < searchFilterData["topics"].length;
        category++) {
      for (int option = 0;
          option < searchFilterData["topics"][category]["options"].length;
          option++) {
        if (searchFilterData["topics"][category]["options"][option]["status"]) {
          String shortendedkey = searchFilterData["topics"][category]["key"];
          filtersSet = "&" +
              shortendedkey +
              "=" +
              searchFilterData["topics"][category]["options"][option]
                  ["filter_id"];
          url += "&" +
              shortendedkey +
              "=" +
              searchFilterData["topics"][category]["options"][option]
                  ["filter_id"];
        }
      }
    }
    return url;
  }

  filterSearchResults() {
    String rootUrl = constructUrlWithFilters();
    printIfDebug("New Search with Filter Applied");
    printIfDebug(rootUrl);
    sendElasticGetRequest(rootUrl);
    setFilterMenu();
  }

  Widget optionList() {
    printIfDebug("List to be rendered");
    printIfDebug(searchFilterData["topics"][chosenIndex]["options"].length);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return new Positioned(
      child: new Center(
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new SizedBox.fromSize(
            size: new Size(screenWidth * 0.7, screenHeight * 0.6),
            child: new ListView.builder(
                itemCount:
                    searchFilterData["topics"][chosenIndex]["options"].length,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(2.0),
                itemBuilder: (BuildContext context, int index) {
                  return new GestureDetector(
                    onTap: () {
                      printIfDebug("Clicked");
                      printIfDebug(searchFilterData["topics"][chosenIndex]
                          ["options"][index]["name"]);
                      toogleOptionStatus(chosenIndex, index);
                      filterSearchResults();
                    },
                    child: new Container(
                      padding: const EdgeInsets.all(10.0),
                      child: new Text(
                        searchFilterData["topics"][chosenIndex]["options"]
                            [index]["name"],
                        style: new TextStyle(
                            fontWeight: FontWeight.normal,
                            color: searchFilterData["topics"][chosenIndex]
                                    ["options"][index]["status"]
                                ? Constants.appColorLogo
                                : Constants.appColorL3,
                            fontSize: 22.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                }),
          ),
        ),
      ),
    );
  }

  Widget blurSearchPage() {
    if (filterMenuStatus) {
      double overlayOpacity = filterMenuStatus ? 0.6 : 0.0;
      double sigma = filterMenuStatus ? 10.0 : 0.0;
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      return new Stack(
        children: <Widget>[
          new Container(
            width: screenWidth,
            height: screenHeight,
            color: Constants.appColorFont.withOpacity(overlayOpacity),
            child: new ClipRect(
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
                child: new Container(
                  decoration: new BoxDecoration(
                      color:
                          Constants.appColorFont.withOpacity(overlayOpacity)),
                ),
              ),
            ),
          ),
          optionList(),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget displaySearchFilterMenu() {
    if (searchFilterDataReady) {
      return new MovieSearchFilter(
        updateFilterMenu: updateFilterMenu,
        searchFilterData: searchFilterData,
      );
    } else {
      return Container();
    }
  }

  Widget renderDefaultMessage(String defaultMessage, FontWeight fontWeight) {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      width: width * 0.75,
      child: new Center(
        child: new Text(
          defaultMessage,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: fontWeight,
            color: Constants.appColorFont,
            fontSize: 15.0,
          ),
          textAlign: TextAlign.center,
          maxLines: 4,
        ),
      ),
    );
  }

  Widget renderDefaultMessagewithSpan(
      String defaultMessage, String defaultSpanMessage) {
    return new Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            renderDefaultMessage(defaultMessage, FontWeight.normal),
            renderDefaultMessage(defaultSpanMessage, FontWeight.bold),
          ],
        ),
      ),
    );
  }

  Widget renderDefaultImage() {
    return new Center(
      child: new Container(
        width: 100.0,
        height: 100.0,
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            // color: Constants.appColorFont,
            animation: "sadsmile"),
      ),
    );
  }

  Widget defaultSearchResults(String defaultMessage) {
    return Container(
      color: Constants.appColorL1,
      child: new Stack(
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              renderDefaultImage(),
              renderDefaultMessage(defaultMessage, FontWeight.normal),
            ],
          ),
          // blurSearchPage(),
          // displaySearchFilterMenu(),
          // closeOption(),
        ],
      ),
    );
  }

  Widget beforeSearchingMessage(
      String defaultMessage, String defaultSpanMessage) {
    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          renderDefaultImage(),
          renderDefaultMessagewithSpan(defaultMessage, defaultSpanMessage),
        ],
      ),
    );
  }

  displayLoaderDuringFetchMore() {
    if (loadingSymbolStatusElasticSearch) {
      return new Positioned.fill(
        child: new Container(
          color: Constants.appColorFont,
          child: new Center(
            child: ShimmerCards(),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  displayLoaderForInitialFetch() {
    return new Container(
      color: Constants.appColorFont,
      child: new Center(
        child: ShimmerCards(),
      ),
    );
  }

  checkIfFirstSearchRequestTriggered() {
    printIfDebug("We In");
    printIfDebug(pageNo);
    printIfDebug(fetchedMovieDetail);
    printIfDebug(loadingSymbolStatusElasticSearch);
    if (pageNo == 1 && loadingSymbolStatusElasticSearch) {
      return displayLoaderForInitialFetch();
    } else {
      return new MovieSearchPopular(
          showOriginalPoster: widget.showOriginalPosters);
    }
  }

  changeSuggestionFlag(bool flag) {
    setState(() {
      suggestionFlag = flag;
      fetchedMovieDetail = !flag;
    });
  }

  checkIfUnkownResults() {
    String defaultMessage = "";
    String defaultSpanMessage = "";
    printIfDebug("Due to Unkown Results");
    printIfDebug(unkownResults);
    if (unkownResults) {
      String defaultMessage = "Oop's \n\n No Movies / Tv Shows Found";
      return defaultSearchResults(defaultMessage);
    } else {
      defaultMessage = "Discover your Favourite Movies / Tv Shows";
      defaultSpanMessage = "Search Now";
      return beforeSearchingMessage(defaultMessage, defaultSpanMessage);
    }
    // }
  }

  callSuggestion(String searchText) async {
    Timer(const Duration(milliseconds: 600), () {
      if (searchText == searchString) {
        delayedCallSugggestion(searchText);
      }
    });
  }

  delayedCallSugggestion(String searchText) async {
    String suggestionApi = "https://api.flixjini.com/entertainment/suggest/";
    suggestionApi = suggestionApi + searchText + ".json?";
    suggestionApi = authenticationToken.length > 0
        ? (suggestionApi + "jwt_token=" + authenticationToken + "&")
        : suggestionApi;
    suggestionApi =
        suggestionApi + "include_actor_stuff=true&include_channels=true&";
    String suggestionApiUrl = constructHeader(suggestionApi);
    suggestionApiUrl = await fetchDefaultParams(suggestionApiUrl);
    setState(() {
      suggestionDataReady = false;
    });

    try {
      var response = await http.get(Uri.parse(suggestionApiUrl));
      printIfDebug(response.statusCode);
      printIfDebug(response.statusCode == 200);
      if (response.statusCode == 200) {
        var fetchedData = json.decode(response.body);
        printIfDebug(fetchedData);
        printIfDebug("Not yet In - resultsHaveBeenCleared 2");
        printIfDebug(resultsHaveBeenCleared);
        printIfDebug("Yes we In");
        if (searchText == searchString)
          setState(() {
            suggestionData = fetchedData;
            suggestionDataReady = true;
          });
      } else {
        // this.movieDetail[
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug(exception.toString());
    }
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: 'filter_search',
      parameters: <String, dynamic>{
        'search_string': tagName,
      },
    );
    Map<String, String> eventData = {
      'content': tagName.toString(),
    };
    //platform.invokeMethod('logSearchEvent', eventData);
  }

  Widget suggestionList() {
    return SingleChildScrollView(
      child: Container(
        color: Constants.appColorL1,
        child: AnimationLimiter(
          child: new ListView.builder(
              itemCount: suggestionData.length + 1,
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.all(10.0),
              shrinkWrap: true,
              addAutomaticKeepAlives: false,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return AnimationConfiguration.staggeredList(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  child: SlideAnimation(
                    verticalOffset: 50.0,
                    child: FadeInAnimation(
                      child: suggestionTile(index),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget suggestionTile(int index) {
    String suggText = "";
    String typeOfResult = "";
    if (index > 0) {
      var suggSplit = suggestionData[index - 1].toString().split(":,:");
      suggText = suggSplit[0];
      typeOfResult = suggSplit[1];
    } else {
      suggText = searchString;
    }
    return GestureDetector(
      onTap: () {
        setState(() {
          searchString = suggText;
          suggestionDataReady = false;
          searchController.text = suggText;
        });
        // _sendAnalyticsEvent(suggText);

        String appliedMoviesFilterUrl = constructUrlWithFilters();
        appliedMoviesFilterUrl =
            appliedMoviesFilterUrl + "&type_of_result=" + typeOfResult;
        printIfDebug(appliedMoviesFilterUrl);

        sendElasticGetRequest(appliedMoviesFilterUrl);
      },
      child: Container(
        width: screenWidth,
        color: Constants.appColorL1,
        padding: EdgeInsets.only(
          // top: 10,
          // bottom: 10,
          left: 5,
          right: 5,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              suggText,
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 15,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 15,
                bottom: 15,
                left: 5,
                right: 5,
              ),
              height: 1,
              color: Constants.appColorDivider.withOpacity(0.1),
            )
          ],
        ),
      ),
    );
  }

  Widget suggestionContainer() {
    if (suggestionDataReady) {
      return suggestionList();
    } else {
      return getProgressBar();
    }
  }

  Widget checkDataReady() {
    String? defaultMessage, defaultSpanMessage;
    if (1 == 2) {
      printIfDebug(
          "\n\njust using the following 2 variables, for the sake of clearing unused variables warnings\n\n default msg: $defaultMessage, default span msg: $defaultSpanMessage");
    }
    if (!searchBoxEmpty) {
      if ((fetchedMovieDetail && fetchedUserPreference) &&
              fetchedUserQueuePreference ||
          false) {
        if (movieDetail["movies"].length > 0) {
          return new Stack(
            children: <Widget>[
              new SingleChildScrollView(
                child: new ConstrainedBox(
                  constraints: new BoxConstraints(),
                  child: displayMovieDetails(),
                ),
              ),
              displayLoaderDuringFetchMore(),
              // blurSearchPage(),
              // displaySearchFilterMenu(),
              // closeOption(),
            ],
          );
        } else {
          return checkIfUnkownResults();
        }
      } else if (suggestionFlag) {
        return suggestionContainer();
      } else {
        defaultMessage = "Discover your Favourite Movies / Tv Shows";
        defaultSpanMessage = "Search Now";
        printIfDebug("First Search");
        return checkIfFirstSearchRequestTriggered();
      }
    } else {
      return new MovieSearchPopular(
          showOriginalPoster: widget.showOriginalPosters);
    }
  }

  Widget appSearchBar() {
    return MovieSearchBar(
      searchController: searchController,
      delayServerRequest: delayServerRequest,
      setSearchStringValue: setSearchStringValue,
      filterMenuStatus: filterMenuStatus,
      clearSearchResults: clearSearchResults,
      moveToPreviousTab: widget.moveToPreviousTab,
      toogleSearchBoxEmpty: toogleSearchBoxEmpty,
      changeSuggestionFlag: changeSuggestionFlag,
      callSuggestion: callSuggestion,
    );
  }

  PreferredSize displayAppBar() {
    if (!filterMenuStatus) {
      return PreferredSize(
        preferredSize: new Size.fromHeight(60.0),
        child: new Stack(
          children: <Widget>[
            appSearchBar(),
          ],
        ),
      );
    } else {
      return PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: Container(),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    return new Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      appBar: displayAppBar(),
      body: checkDataReady(),
    );
  }
}
