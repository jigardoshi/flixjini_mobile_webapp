import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:ui';
import 'package:flixjini_webapp/common/constants.dart';

class MovieSearchBar extends StatefulWidget {
  final searchController;
  final delayServerRequest;
  final setSearchStringValue;
  final filterMenuStatus;
  final clearSearchResults;
  final moveToPreviousTab;
  final toogleSearchBoxEmpty;
  final changeSuggestionFlag;
  final callSuggestion;

  MovieSearchBar({
    this.searchController,
    this.delayServerRequest,
    this.setSearchStringValue,
    this.filterMenuStatus,
    this.clearSearchResults,
    this.moveToPreviousTab,
    this.toogleSearchBoxEmpty,
    this.changeSuggestionFlag,
    this.callSuggestion,
  });

  @override
  _MovieSearchBarState createState() => new _MovieSearchBarState();
}

class _MovieSearchBarState extends State<MovieSearchBar> {
  FocusNode _focusNodeSearch = new FocusNode();

  Widget appBarElasticSearch() {
    double appBarElevation = widget.filterMenuStatus ? 0.0 : 6.0;
    return new AppBar(
      automaticallyImplyLeading: false,
      elevation: appBarElevation,
      backgroundColor: Constants.appColorL2,
      titleSpacing: 0.0,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      title: new Container(
        color: Constants.appColorL2,
        padding: const EdgeInsets.only(
            top: 12.0, bottom: 8.0, left: 15.0, right: 15.0),
        child: new Theme(
          data: new ThemeData(
            primaryColor: Constants.appColorL2,
            hintColor: Constants.appColorL2,
            canvasColor: Constants.appColorL2,
            inputDecorationTheme: new InputDecorationTheme(
              labelStyle: new TextStyle(
                color: Constants.appColorFont,
              ),
            ),
          ),
          child: new TextField(
            cursorColor: Constants.appColorFont,
            focusNode: _focusNodeSearch,
            autofocus: false,
            style: new TextStyle(
              color: Constants.appColorFont,
              fontSize: 15.0,
            ),
            textAlign: TextAlign.left,
            controller: widget.searchController,
            decoration: new InputDecoration(
              prefixIcon: new Icon(
                Icons.search,
                color: Constants.appColorLogo,
                size: 25.0,
              ),
              suffixIcon: new GestureDetector(
                onTap: () {
                  widget.clearSearchResults();
                  widget.searchController.clear();
                  widget.toogleSearchBoxEmpty(true);

                  printIfDebug("Clear TextField");
                },
                child: new Container(
                  padding: const EdgeInsets.all(0.0),
                  child: new Icon(
                    Icons.clear,
                    color: Constants.appColorLogo,
                    size: 25.0,
                  ),
                ),
              ),
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
              hintText: "Search",
              filled: true,
              fillColor: Constants.appColorL1,
              hintStyle: new TextStyle(
                color: Constants.appColorDivider,
              ),
              border: new OutlineInputBorder(
                borderRadius: const BorderRadius.all(
                  const Radius.circular(30.0),
                ),
                borderSide: new BorderSide(
                  color: Constants.appColorL2,
                ),
              ),
            ),
            onChanged: (value) {
              printIfDebug("Value");
              printIfDebug(value);

              if (value.isEmpty) {
                widget.toogleSearchBoxEmpty(true);
                widget.clearSearchResults();
                widget.changeSuggestionFlag(false);
              } else {
                widget.changeSuggestionFlag(true);
                widget.toogleSearchBoxEmpty(false);
                widget.setSearchStringValue(value);

                widget.callSuggestion(value);
              }
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return appBarElasticSearch();
  }
}
