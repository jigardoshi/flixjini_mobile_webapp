import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailLessCard extends StatefulWidget {
  MovieDetailLessCard({
    Key? key,
    this.toogleExpansionMinimisation,
    this.cardHeight,
    this.cardWidth,
  }) : super(key: key);

  final toogleExpansionMinimisation;
  final cardHeight;
  final cardWidth;

  @override
  _MovieDetailLessCardState createState() => new _MovieDetailLessCardState();
}

class _MovieDetailLessCardState extends State<MovieDetailLessCard> {
  String lessPhoto = "assests/less.png";

  Widget lessCardUI() {
    return new GridTile(
      child: new GestureDetector(
        onTap: () {
          widget.toogleExpansionMinimisation();
        },
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Image.asset(
                "assests/collapse_icon.png",
                height: 18,
                width: 18,
              ),
              margin: EdgeInsets.only(
                right: 5,
              ),
            ),
            Text(
              "Less",
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: Constants.appColorFont,
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return lessCardUI();
  }
}
