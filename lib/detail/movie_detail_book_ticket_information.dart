import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';

class MovieDetailBookTicketInformation extends StatefulWidget {
  MovieDetailBookTicketInformation({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailBookTicketInformationState createState() =>
      new _MovieDetailBookTicketInformationState();
}

class _MovieDetailBookTicketInformationState
    extends State<MovieDetailBookTicketInformation> {
  Widget buildQuickFactCards(BuildContext context, int index) {
    var photo =
        widget.movieDetail["bookTickets"]["quickFacts"][index]["factIcon"];
    photo = "images/pending.png";
    return new Padding(
      padding: const EdgeInsets.only(right: 5.0),
      child: new Card(
        child:
            new Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          new Padding(
            padding: const EdgeInsets.all(5.0),
            child: new Image.asset(
              photo,
              width: 45.0,
              height: 45.0,
              fit: BoxFit.cover,
              alignment: Alignment.center,
            ),
          ),
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
              width: 80.0,
              height: 20.0,
              child: new Text(
                widget.movieDetail["bookTickets"]["quickFacts"][index]["fact"],
                style: new TextStyle(fontWeight: FontWeight.bold),
                textScaleFactor: 0.85,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 4,
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Widget quickFactsCardView() {
    return new SizedBox.fromSize(
      size: const Size.fromHeight(150.0),
      child: new ListView.builder(
        itemCount: widget.movieDetail["bookTickets"]["quickFacts"].length,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(top: 8.0, left: 5.0),
        itemBuilder: buildQuickFactCards,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Container(
      child: new Padding(
        padding: const EdgeInsets.all(5.0),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Padding(
                padding: const EdgeInsets.only(top: 10.0, right: 5.0),
                child: quickFactsCardView(),
              ),
            ]),
      ),
    );
  }
}
