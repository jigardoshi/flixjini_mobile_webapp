import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_gallery.dart';
// import	'package:streaming_entertainment/detail/movie_detail_youtube.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:flixjini_webapp/detail/movie_detail_more_card.dart';
import 'package:flixjini_webapp/detail/movie_detail_less_card.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailVideos extends StatefulWidget {
  MovieDetailVideos({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailVideosState createState() => new _MovieDetailVideosState();
}

class _MovieDetailVideosState extends State<MovieDetailVideos> {
  bool expandMoviePhotos = false;
  bool needMoreGalleryView = true;
  int condensedDisplaySize = 4;
  int sampleLength = 0;
  String morePhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/517808-200.png";
  String lessPhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/8299-200.png";

  void playYoutubeVideo(String url) {
    // FlutterYoutube.playYoutubeVideoById(
    //   apiKey: "AIzaSyD71aPyq5vFg6dSRmtM_R-ES0AtyuykXgI",
    //   videoId: getYouTubeVideoId(
    //     url,
    //   ),
    // );
  }

  String getYouTubeVideoId(String videoUrl) {
    return Uri.parse(videoUrl).queryParameters['v']!;
  }

  void toogleExpansionMinimisation() {
    setState(() {
      expandMoviePhotos = expandMoviePhotos ? false : true;
    });
  }

  Widget buildPhotos(BuildContext context, int index) {
    if (index < sampleLength) {
      var photo = widget.movieDetail["gallary"]["videos"][index]["videoImage"];
      return new Padding(
        padding: const EdgeInsets.all(0.0),
        child: new GestureDetector(
          onTap: () {
            printIfDebug("Image Id that has been clicked");
            printIfDebug(photo);
            playYoutubeVideo(
                widget.movieDetail["gallary"]["videos"][index]["videoLink"]);
          },
          child: new Card(
            elevation: 6.0,
            color: Constants.appColorFont,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: new Stack(children: [
              new Padding(
                padding: const EdgeInsets.only(bottom: 0.0),
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(8.0),
                  child: new Image.network(
                    photo,
                    width: 220.0,
                    height: 180.0,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              new Positioned(
                bottom: 16.0,
                top: 16.0,
                left: 16.0,
                right: 16.0,
                child: new Container(
                  padding: const EdgeInsets.all(0.0),
                  child: new Image.asset(
                    "assests/play_video.png",
                  ),
                ),
              ),
            ]),
          ),
        ),
      );
    } else {
      return new MovieDetailMoreCard(
          toogleExpansionMinimisation: toogleExpansionMinimisation,
          cardHeight: 180.0,
          cardWidth: 220.0);
    }
  }

  Widget galleryView() {
    int gallaryVideoLength = widget.movieDetail["gallary"]["videos"].length;
    if (gallaryVideoLength > condensedDisplaySize) {
      printIfDebug("Has greater than 4 images");
      printIfDebug(gallaryVideoLength);
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      printIfDebug("Doesn't have greater than 4 images");
      printIfDebug(gallaryVideoLength);
      setState(() {
        needMoreGalleryView = false;
        sampleLength = gallaryVideoLength;
      });
    }
    printIfDebug("Based on Number of videos");
    printIfDebug(needMoreGalleryView);
    if (needMoreGalleryView) {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(150.0),
        child: new ListView.builder(
          itemCount: sampleLength + 1,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    } else {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(150.0),
        child: new ListView.builder(
          itemCount: sampleLength,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    }
  }

  Widget expandedGalleryView() {
    int numberOfOptions = widget.movieDetail["gallary"]["videos"].length + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 105.0 * numberOfRows;
    printIfDebug("from expanded gallery view method, grid height: $gridHeight");
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      color: Constants.appColorFont.withOpacity(0.9),
      padding: const EdgeInsets.all(4.0),
      child: new GridView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 6,
          childAspectRatio: 1.8,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
        itemCount: widget.movieDetail["gallary"]["videos"].length + 1,
        itemBuilder: (BuildContext context, int i) {
          if (i < widget.movieDetail["gallary"]["videos"].length) {
            var photo =
                widget.movieDetail["gallary"]["videos"][i]["videoImage"];
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  printIfDebug("Image Id that has been clicked");
                  printIfDebug(
                      widget.movieDetail["gallary"]["videos"][i]["videoImage"]);
                  playYoutubeVideo(
                      widget.movieDetail["gallary"]["videos"][i]["videoLink"]);
                },
                child: new Container(
                  child: new Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: new Container(
                        child: new Stack(children: [
                          new Positioned.fill(
                            child: new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: new ClipRRect(
                                borderRadius: new BorderRadius.circular(8.0),
                                child: new Image.network(
                                  photo,
                                  width: 150.0,
                                  height: 100.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          new Positioned(
                            bottom: 16.0,
                            top: 16.0,
                            left: 16.0,
                            right: 16.0,
                            child: new Container(
                              padding: const EdgeInsets.all(0.0),
                              child: new Image.asset(
                                "assests/play_video.png",
                              ),
                            ),
                          ),
                        ]),
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return new MovieDetailLessCard(
                toogleExpansionMinimisation: toogleExpansionMinimisation,
                cardHeight: 180.0,
                cardWidth: 220.0);
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (expandMoviePhotos && needMoreGalleryView) {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new MovieDetailSubheading(subheading: "Movie Videos"),
                new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
              ]),
        ),
      );
    } else {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new MovieDetailSubheading(subheading: "Movie Videos"),
                new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: galleryView(),
                ),
              ]),
        ),
      );
    }
  }
}
