import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailSubheading extends StatefulWidget {
  MovieDetailSubheading({
    Key? key,
    this.subheading,
  }) : super(key: key);

  final subheading;

  @override
  MovieDetailSubheadingState createState() => new MovieDetailSubheadingState();
}

class MovieDetailSubheadingState extends State<MovieDetailSubheading> {
  @override
  void initState() {
    super.initState();
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 2.5,
        width: 100.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorLogo,
          ),
        ),
      ),
    );
  }

  Widget getSubHeading() {
    // return new Container(
    //   decoration: BoxDecoration(
    //     border: new Border(
    //       left: BorderSide(
    //         color: Constants.appColorLogo,
    //         width: 4,
    //       ),
    //     ),
    //   ),
    //   padding: const EdgeInsets.only(
    //     top: 15.0,
    //     left: 10,
    //     bottom: 10,
    //   ),
    //   child: new Text(
    //     widget.subheading,
    //     textAlign: TextAlign.center,
    //     style: new TextStyle(
    //         fontWeight: FontWeight.normal,
    //         color: Constants.appColorFont,
    //         fontSize: 20.0),
    //   ),
    // );
    return Container(
      margin: EdgeInsets.only(
        bottom: 14,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // insertDividerVertical(),
          Expanded(
            // flex: 6,
            child: Container(
              margin: EdgeInsets.only(
                  // let: 10,
                  ),
              padding: EdgeInsets.only(
                top: 5,
                left: 8,
              ),
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    width: 4.0,
                    color: Constants.appColorLogo,
                  ),
                ),
              ),
              child: Text(
                widget.subheading.toString().toUpperCase(),
                textAlign: TextAlign.left,
                style: new TextStyle(
                  letterSpacing: 0,
                  fontWeight: FontWeight.normal,
                  color: Constants.appColorFont,
                  fontSize: 19.0,
                ),
                maxLines: 2,
              ),
            ),
          ),

          
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return getSubHeading();
  }
}
