import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/detail/movie_detail_more_card.dart';
import 'package:flixjini_webapp/detail/movie_detail_less_card.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailAwards extends StatefulWidget {
  MovieDetailAwards({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailAwardsState createState() => new _MovieDetailAwardsState();
}

class _MovieDetailAwardsState extends State<MovieDetailAwards> {
  bool expandMoviePhotos = false;
  bool needMoreGalleryView = true;
  bool nominations = false;
  int awardNo = -1;
  int condensedDisplaySize = 4;
  int sampleLength = 0;
  String morePhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/517808-200.png";
  String lessPhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/8299-200.png";

  void initState() {
    super.initState();
    printIfDebug("Awards");
    printIfDebug(widget.movieDetail["awards"]);
  }

  void toogleExpansionMinimisation() {
    setState(() {
      expandMoviePhotos = expandMoviePhotos ? false : true;
    });
  }

  void setNominationsFalse() {
    setState(() {
      nominations = false;
    });
  }

  String getAwardImageUrl(
    String imgUrl,
  ) {
    if (imgUrl.contains('s3-ap-southeast-1.amazonaws.com')) {
      imgUrl = imgUrl.replaceAll(
        'https://s3-ap-southeast-1.amazonaws.com/assets.komparify.com/',
        'https://komparify.sgp1.cdn.digitaloceanspaces.com/assets/',
      );
    }
    return imgUrl;
  }

  Widget renderAwardCard(int index) {
    // printIfDebug('\n\njson: ');
    // printIfDebug(widget.movieDetail['awards']);
    String defaultImage = "images/pending.png";
    if (widget.movieDetail["awards"]["prizes"][index]
        .containsKey("awardImage")) {
      defaultImage = getAwardImageUrl(
        widget.movieDetail["awards"]["prizes"][index]["awardImage"].toString(),
      );
    } else {
      defaultImage = "https://c.kmpr.in/assets/award_dui.png";
    }
    if (index < sampleLength) {
      printIfDebug("Render Photo");
      String photo = "";
      photo = defaultImage;
      String noOfWins =
          double.parse(widget.movieDetail["awards"]["prizes"][index]["won"]) >
                  0.0
              ? "Won : " + widget.movieDetail["awards"]["prizes"][index]["won"]
              : "";
      String noOfNominations = "Nominated : " +
          widget.movieDetail["awards"]["prizes"][index]["nominations"];
      // String awardDetails =
      // widget.movieDetail["awards"]["prizes"][index]
      //         ["awardName"] +
      //     "\n" +
      // noOfWins + "\n" + noOfNominations;
      return new Container(
        height: 120.0,
        width: 220.0,
        padding: const EdgeInsets.all(0.0),
        child: new GestureDetector(
          onTap: () {
            printIfDebug("Image Id that has been clicked");
            printIfDebug(photo);
            setState(() {
              nominations = true;
              awardNo = index;
            });
            printIfDebug("Nomination Status");
            printIfDebug(nominations);
            printIfDebug("Clicked on Award Position");
            printIfDebug(awardNo);
          },
          child: new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Card(
              // elevation: 6.0,
              color:  Constants.appColorL2,
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0),
              ),
              child: new Container(
                child: new Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Expanded(
                      child: new Container(
                        // padding: const EdgeInsets.all(10.0),
                        child: new ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0),
                            ),

                            // borderRadius: new BorderRadius.circular(8.0),
                            child: widget.movieDetail["awards"]["prizes"][index]
                                    .containsKey("awardImage")
                                ? new Image(
                                    // color: Constants.appColorDivider,
                                    width: 220.0,
                                    // height: 40,

                                    image: NetworkImage(
                                      photo,
                                      // useDiskCache: false,
                                    ),
                                    fit: BoxFit.fill,
                                    gaplessPlayback: true,
                                  )
                                : Center(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                      ),
                                      child: Text(
                                        widget.movieDetail["awards"]["prizes"]
                                            [index]["awardName"],
                                        style: TextStyle(
                                          color: Constants.appColorFont,
                                          fontSize: 12,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  )
                            // new Image(
                            //     height: 40,
                            //     // width: 30,
                            //     width: 220.0,

                            //     color: Constants.appColorDivider,
                            //     image: NetworkImage(
                            //       photo,
                            //       // useDiskCache: false,
                            //     ),
                            //     fit: BoxFit.fitWidth,
                            //     gaplessPlayback: true,
                            //   ),
                            ),
                      ),
                      flex: 2,
                    ),
                    new Expanded(
                      child: new Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            widget.movieDetail["awards"]["prizes"][index]
                                    .containsKey("awardImage")
                                ? Container()
                                : Container(
                                    margin: EdgeInsets.only(
                                      right: 10,
                                      bottom: 10,
                                    ),
                                    child:
                                        Image.asset("assests/awesomestar.png"),
                                  ),
                            new Container(
                              padding: const EdgeInsets.only(
                                top: 7.0,
                                right: 5.0,
                                bottom: 5,
                                left: 5,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  noOfWins.length > 1
                                      ? Text(
                                          noOfWins,
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 10.0,
                                            color: Constants.appColorFont,
                                          ),
                                          maxLines: 6,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                      : Container(
                                          height: 2,
                                        ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 5,
                                    ),
                                    child: new Text(
                                      noOfNominations,
                                      textAlign: TextAlign.center,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 10.0,
                                        color: Constants.appColorFont,
                                      ),
                                      maxLines: 6,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      flex: 2,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return new MovieDetailMoreCard(
          toogleExpansionMinimisation: toogleExpansionMinimisation,
          cardHeight: 180.0,
          cardWidth: 100.0);
    }
  }

  Widget buildPhotos(BuildContext context, int index) {
    return renderAwardCard(index);
  }

  Widget galleryView() {
    int galleryAwardLength = widget.movieDetail["awards"]["prizes"].length;
    if (galleryAwardLength > condensedDisplaySize) {
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      setState(() {
        needMoreGalleryView = false;
        sampleLength = galleryAwardLength;
      });
    }
    printIfDebug("Based on Number of Awards");
    printIfDebug(needMoreGalleryView);
    if (needMoreGalleryView) {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(130.0),
        child: new ListView.builder(
          itemCount: sampleLength + 1,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    } else {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(130.0),
        child: new ListView.builder(
          itemCount: sampleLength,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    }
  }

  Widget expandedGalleryView() {
    var orientation = MediaQuery.of(context).orientation;
    String defaultImage = "images/pending.png";
    int numberOfOptions = widget.movieDetail["awards"]["prizes"].length + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 105.0 * numberOfRows;
    printIfDebug("grid height from expanded gallery view method: $gridHeight");
    return new Container(
      color: Constants.appColorL1,
      child: new GridView.builder(
        addAutomaticKeepAlives: false,
        padding: const EdgeInsets.all(4.0),
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 6,
          childAspectRatio: 1.8,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
        itemCount: widget.movieDetail["awards"]["prizes"].length + 1,
        itemBuilder: (BuildContext context, int i) {
          if (i < widget.movieDetail["awards"]["prizes"].length) {
            if (widget.movieDetail["awards"]["prizes"][i]
                .containsKey("awardImage")) {
              defaultImage =
                  // widget.movieDetail["awards"]["prizes"][i]["awardImage"];
                  getAwardImageUrl(
                widget.movieDetail["awards"]["prizes"][i]["awardImage"]
                    .toString(),
              );
            } else {
              defaultImage = "https://c.kmpr.in/assets/award_dui.png";
            }
            String photo = defaultImage;
            String noOfWins =
                double.parse(widget.movieDetail["awards"]["prizes"][i]["won"]) >
                        0.0
                    ? "Won : " +
                        widget.movieDetail["awards"]["prizes"][i]["won"]
                    : "";
            String noOfNominations = "Nominated : " +
                widget.movieDetail["awards"]["prizes"][i]["nominations"];
            // String awardDetails =
            // widget.movieDetail["awards"]["prizes"][i]
            //         ["awardName"] +
            //     "\n" +
            // noOfWins + "\n" + noOfNominations;
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  printIfDebug("Image Id that has been clicked");
                  printIfDebug(photo);
                  setState(() {
                    nominations = true;
                    awardNo = i;
                  });
                  printIfDebug("Nomination Status");
                  printIfDebug(nominations);
                  printIfDebug("Clicked on Award Position");
                  printIfDebug(awardNo);
                },
                child: new Card(
                  elevation: 6.0,
                  color:  Constants.appColorL2,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  child: new Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        child: new Container(
                          // padding: const EdgeInsets.all(10.0),
                          child: new ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(8.0),
                                topRight: Radius.circular(8.0),
                              ),

                              // borderRadius: new BorderRadius.circular(8.0),
                              child: widget.movieDetail["awards"]["prizes"][i]
                                      .containsKey("awardImage")
                                  ? new Image(
                                      // color: Constants.appColorDivider,
                                      width: 220.0,
                                      // height: 40,

                                      image: NetworkImage(
                                        photo,
                                        // useDiskCache: false,
                                      ),
                                      fit: BoxFit.fill,
                                      gaplessPlayback: true,
                                    )
                                  : Center(
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                        ),
                                        child: Text(
                                          widget.movieDetail["awards"]["prizes"]
                                              [i]["awardName"],
                                          style: TextStyle(
                                            color: Constants.appColorFont,
                                            fontSize: 12,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    )
                              // new Image(
                              //     height: 40,
                              //     // width: 30,
                              //     width: 220.0,

                              //     color: Constants.appColorDivider,
                              //     image: NetworkImage(
                              //       photo,
                              //       // useDiskCache: false,
                              //     ),
                              //     fit: BoxFit.fitWidth,
                              //     gaplessPlayback: true,
                              //   ),
                              ),
                        ),
                        flex: 1,
                      ),
                      new Expanded(
                        child: new Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              widget.movieDetail["awards"]["prizes"][i]
                                      .containsKey("awardImage")
                                  ? Container()
                                  : Container(
                                      margin: EdgeInsets.only(
                                        right: 10,
                                        bottom: 10,
                                      ),
                                      child: Image.asset(
                                          "assests/awesomestar.png")),
                              new Container(
                                padding: const EdgeInsets.only(
                                  top: 7.0,
                                  right: 5.0,
                                  bottom: 5,
                                  left: 5,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    noOfWins.length > 1
                                        ? Text(
                                            noOfWins,
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 10.0,
                                              color: Constants.appColorFont,
                                            ),
                                            maxLines: 6,
                                            overflow: TextOverflow.ellipsis,
                                          )
                                        : Container(
                                            height: 2,
                                          ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 5,
                                      ),
                                      child: new Text(
                                        noOfNominations,
                                        textAlign: TextAlign.center,
                                        style: new TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 10.0,
                                          color: Constants.appColorFont,
                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return new MovieDetailLessCard(
                toogleExpansionMinimisation: toogleExpansionMinimisation,
                cardHeight: 180.0,
                cardWidth: 220.0);
          }
        },
      ),
    );
  }

  Widget nominationGridView() {
    String photo = "images/pending.png";
    int nominationListLength =
        widget.movieDetail["awards"]["prizes"][awardNo]["nominatedFor"].length;
    printIfDebug("Length of nominationListLength");
    printIfDebug(nominationListLength);
    var orientation = MediaQuery.of(context).orientation;
    int numberOfOptions = nominationListLength + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 105.0 * numberOfRows;
    printIfDebug("from nominationGridView method, grid height: $gridHeight");
    return new Container(
      color: Constants.appColorL1,
      child: new GridView.builder(
        padding: const EdgeInsets.all(4.0),
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        addAutomaticKeepAlives: false,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 6,
          childAspectRatio: 1.8,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
        itemCount: nominationListLength + 1,
        itemBuilder: (BuildContext context, int i) {
          if (i < nominationListLength) {
            String nominationStatus;
            if (widget.movieDetail["awards"]["prizes"][awardNo]["nominatedFor"]
                    [i]["won"] ==
                "true") {
              nominationStatus = " (Won)";
              photo = "assests/awdef.png";
            } else {
              nominationStatus = " (Nominated)";
              photo = "assests/awnom.png";
            }
            String nominationDetails = widget.movieDetail["awards"]["prizes"]
                    [awardNo]["nominatedFor"][i]["nominationName"] +
                nominationStatus;
            return new Container(
              child: new GestureDetector(
                onTap: () {
                  printIfDebug("Image Id that has been clicked");
                },
                child: new Card(
                  elevation: 6.0,
                  color:  Constants.appColorL2,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Expanded(
                        child: new ClipRRect(
                          borderRadius: new BorderRadius.circular(8.0),
                          child: new Stack(
                            children: <Widget>[
                              new Container(
                                color:  Constants.appColorL2,
                                child: new Center(
                                  child: new Image.asset(
                                    photo,
                                    width: 160.0,
                                    height: 140.0,
                                  ),
                                ),
                              ),
                              new Positioned(
                                child: new Center(
                                  child: new Container(
                                    width: 70.0,
                                    child: new Center(
                                      child: new Text(
                                        nominationDetails,
                                        textAlign: TextAlign.center,
                                        style: new TextStyle(
                                          fontWeight: FontWeight.normal,
                                          color: Constants.appColorFont,
                                        ),
                                        textScaleFactor: 0.75,
                                        maxLines: 5,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return new MovieDetailLessCard(
                toogleExpansionMinimisation: setNominationsFalse,
                cardHeight: 180.0,
                cardWidth: 220.0);
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    printIfDebug(widget.movieDetail['awards']);

    if ((expandMoviePhotos && needMoreGalleryView) && !nominations) {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Awards"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
              ]),
        ),
      );
    } else if ((expandMoviePhotos && needMoreGalleryView) && nominations) {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Awards"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
                new MovieDetailSubheading(subheading: "Nominations"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: nominationGridView(),
                ),
              ]),
        ),
      );
    } else if (nominations && !expandMoviePhotos) {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Awards"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: galleryView(),
                ),
                new MovieDetailSubheading(subheading: "Nominations"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: nominationGridView(),
                ),
              ]),
        ),
      );
    } else {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Awards"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: galleryView(),
                ),
              ]),
        ),
      );
    }
  }
}
