import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailWatched extends StatefulWidget {
  MovieDetailWatched({
    Key? key,
    this.movieDetail,
    this.sendRequest,
    this.updateDidNotWatch,
    this.changeFeedback,
  }) : super(key: key);

  final movieDetail, sendRequest, updateDidNotWatch, changeFeedback;

  @override
  _MovieDetailWatchedState createState() => new _MovieDetailWatchedState();
}

class _MovieDetailWatchedState extends State<MovieDetailWatched> {
  bool likeFlag = false, dislikeFlag = false, notWatchedFlag = false;
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    likeFlag = widget.movieDetail["like"] ?? false;
    dislikeFlag = widget.movieDetail["dislike"] ?? false;
  }

  sendEvent() {
    //platform.invokeMethod('logCustomizeProductEvent');
  }

  Widget watchSections() {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Text(
            "WHAT DID YOU THINK?",
            style: TextStyle(
              color: Constants.appColorFont,
              fontWeight: FontWeight.normal,
              fontSize: 13,
              letterSpacing: 1,
            ),
          ),
        ),
        insertHalfDivider(),
        likeDislike(),
        reportBug(),
      ],
    );
  }

  Widget reportBug() {
    return GestureDetector(
      onTap: () {
        widget.changeFeedback(true);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              right: 5,
            ),
            child: Image.asset(
              "assests/rab.png",
              height: 12,
              width: 12,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 10,
              right: 10,
              bottom: 10,
            ),
            child: Text(
              "Report a bug",
              style: TextStyle(
                color: Constants.appColorFont.withOpacity(0.7).withOpacity(0.85),
                fontWeight: FontWeight.bold,
                fontSize: 8,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget likeDislike() {
    return Container(
      margin: EdgeInsets.only(
        top: 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                likeFlag = !likeFlag;
              });
              sendEvent();
              widget.sendRequest(
                  'https://api.flixjini.com/queue/like.json', "like", null, 1);
            },
            child: Container(
              width: 80,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      "assests/like.png",
                      height: 25,
                      width: 25,
                      color: likeFlag ? Constants.appColorLogo : Constants.appColorFont,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                    ),
                    child: Text(
                      "LIKED",
                      style: TextStyle(
                        color: likeFlag ? Constants.appColorLogo : Constants.appColorFont,
                        fontWeight: FontWeight.normal,
                        letterSpacing: 0.8,
                        fontSize: 8,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                dislikeFlag = !dislikeFlag;
              });
              sendEvent();
              widget.sendRequest('https://api.flixjini.com/queue/dislike.json',
                  "dislike", null, 1);
            },
            child: Container(
              width: 80,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      "assests/dislike.png",
                      height: 25,
                      width: 25,
                      color: dislikeFlag ? Constants.appColorLogo : Constants.appColorFont,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                    ),
                    child: Text(
                      "DISLIKED",
                      style: TextStyle(
                        color: dislikeFlag ? Constants.appColorLogo : Constants.appColorFont,
                        fontWeight: FontWeight.normal,
                        letterSpacing: 0.8,
                        fontSize: 8,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              widget.updateDidNotWatch();

              setState(() {
                notWatchedFlag = !notWatchedFlag;
              });
            },
            child: Container(
              width: 80,
              child: Column(
                children: <Widget>[
                  Container(
                    child: Image.asset(
                      "assests/dnw.png",
                      height: 25,
                      width: 25,
                      color: notWatchedFlag ? Constants.appColorLogo : Constants.appColorFont,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                    ),
                    child: Text(
                      "DID NOT WATCH",
                      style: TextStyle(
                        color:
                            notWatchedFlag ? Constants.appColorLogo : Constants.appColorFont,
                        fontWeight: FontWeight.normal,
                        letterSpacing: 0.8,
                        fontSize: 8,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget insertHalfDivider() {
    return new Container(
      width: 100,
      padding: const EdgeInsets.only(
        top: 6.0,
        bottom: 16.0,
        // left: 90,
        // right: 90,
      ),
      child: SizedBox(
        height: 1.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorDivider,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Constants.appColorL2,
      ),
      margin: EdgeInsets.only(
        top: 10,
        // left: 10,
        // right: 10,
      ),
      child: watchSections(),
    );
  }
}
