import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailUnderline extends StatelessWidget {
  MovieDetailUnderline({
    Key? key,
  }) : super(key: key);

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.all(4.0),
      child: SizedBox(
        height: 2.5,
        width: 100.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorLogo,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return insertDivider();
  }
}
