import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/detail/movie_detail_gallery.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/detail/movie_detail_more_card.dart';
import 'package:flixjini_webapp/detail/movie_detail_less_card.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailMovieGallery extends StatefulWidget {
  MovieDetailMovieGallery({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailMovieGalleryState createState() =>
      new _MovieDetailMovieGalleryState();
}

class _MovieDetailMovieGalleryState extends State<MovieDetailMovieGallery> {
  bool expandMoviePhotos = false;
  bool needMoreGalleryView = true;
  int condensedDisplaySize = 4;
  int sampleLength = 0;
  String morePhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/517808-200.png";
  String lessPhoto = "https://d30y9cdsu7xlg0.cloudfront.net/png/8299-200.png";

  void toogleExpansionMinimisation() {
    setState(() {
      expandMoviePhotos = expandMoviePhotos ? false : true;
    });
  }

  Widget buildPhotos(BuildContext context, int index) {
    if (index < sampleLength) {
      var photo =
          widget.movieDetail["gallary"]["posters"][index]["posterPhoto"];
      return new Padding(
        padding: const EdgeInsets.only(right: 0.0),
        child: new GestureDetector(
          onTap: () {
            printIfDebug("Image Id that has been clicked");
            printIfDebug(photo);
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new MovieDetailGallery(
                    title: "Movie Images Gallery",
                    photos: widget.movieDetail["gallary"]["posters"],
                    current: index,
                    type: "posterLargePhoto")));
          },
          child: new Card(
            elevation: 6.0,
            color: Constants.appColorFont,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Image.network(
                photo,
                width: 100.0,
                height: 160.0,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      );
    } else {
      return new MovieDetailMoreCard(
          toogleExpansionMinimisation: toogleExpansionMinimisation,
          cardHeight: 160.0,
          cardWidth: 100.0);
    }
  }

  Widget galleryView() {
    int gallaryPosterLength = widget.movieDetail["gallary"]["posters"].length;
    if (gallaryPosterLength > condensedDisplaySize) {
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      setState(() {
        needMoreGalleryView = false;
        sampleLength = gallaryPosterLength;
      });
    }
    if (needMoreGalleryView) {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(180.0),
        child: new ListView.builder(
          itemCount: sampleLength + 1,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    } else {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(180.0),
        child: new ListView.builder(
          itemCount: sampleLength,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    }
  }

  Widget expandedGalleryView() {
    int numberOfOptions = widget.movieDetail["gallary"]["posters"].length + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 107.0 * numberOfRows;
    printIfDebug("from expanded gallery view method, grid height: $gridHeight");
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      color:  Constants.appColorFont.withOpacity(0.9),
      child: new GridView.builder(
        addAutomaticKeepAlives: false,
        padding: const EdgeInsets.all(4.0),
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 3 : 6,
          childAspectRatio: 0.75,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
        itemCount: widget.movieDetail["gallary"]["posters"].length + 1,
        itemBuilder: (BuildContext context, int i) {
          if (i < widget.movieDetail["gallary"]["posters"].length) {
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  printIfDebug("Image Id that has been clicked");
                  printIfDebug(widget.movieDetail["gallary"]["posters"][i]
                      ["posterPhoto"]);
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new MovieDetailGallery(
                          title: "Movie Images Gallery",
                          photos: widget.movieDetail["gallary"]["posters"],
                          current: i,
                          type: "posterLargePhoto")));
                },
                child: new Container(
                  child: new Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(8.0),
                      child: new Container(
                        width: 100.0,
                        height: 160.0,
                        child: new ClipRRect(
                          borderRadius: new BorderRadius.circular(8.0),
                          child: new Image.network(
                            widget.movieDetail["gallary"]["posters"][i]
                                ["posterPhoto"],
                            width: 100.0,
                            height: 160.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return new MovieDetailLessCard(
                toogleExpansionMinimisation: toogleExpansionMinimisation,
                cardHeight: 160.0,
                cardWidth: 100.0);
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (expandMoviePhotos && needMoreGalleryView) {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new MovieDetailSubheading(subheading: "Movie Posters"),
                new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
              ]),
        ),
      );
    } else {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new MovieDetailSubheading(subheading: "Movie Posters"),
                new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: galleryView(),
                ),
              ]),
        ),
      );
    }
  }
}
