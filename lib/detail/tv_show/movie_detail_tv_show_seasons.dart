import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_chosen_season.dart';
import 'package:flixjini_webapp/tvshow/movie_tvshow_page.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'dart:ui';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailTvShowSeasons extends StatefulWidget {
  MovieDetailTvShowSeasons({
    Key? key,
    this.tvShow,
    this.authenticationToken,
    this.urlPhase,
    this.updateSelectedSeasonEpisodes,
  }) : super(key: key);

  final tvShow;
  final authenticationToken;
  final urlPhase;
  final updateSelectedSeasonEpisodes;

  @override
  _MovieDetailTvShowSeasonsState createState() =>
      new _MovieDetailTvShowSeasonsState();
}

class _MovieDetailTvShowSeasonsState extends State<MovieDetailTvShowSeasons> {
  int chosenSeason = 0;
  int chosenPage = 0;

  var cardLayout = {};

  @override
  void initState() {
    cardLayout = {"height": 150.0, "width": 220.0};
    setChosenSeasonToLastSeason();
    setChosenPage(0);
    super.initState();
  }

  setChosenSeasonToLastSeason() {
    setState(() {
      chosenSeason = widget.tvShow["seasons"].length - 1;
    });
  }

  bool nullChecker(String key) {
    if (widget.tvShow.containsKey(key)) {
      if (widget.tvShow[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  selectedSeason(int index) {
    setState(() {
      chosenSeason = index;
    });
    if (widget.tvShow["seasons"][chosenSeason].containsKey("episodes")) {
      printIfDebug(
          "Episodes have already been fetched --> No need API request");
    } else {
      fetchSelectedSeasonEpisdoes();
    }
  }

  setChosenPage(value) {
    setState(() {
      chosenPage = value;
    });
    printIfDebug("Here the value chnages");
    printIfDebug(chosenPage);
  }

  constructUrl() {
    String urlName = "";
    String refinedUrlPhase = "";
    String seasonNo = (chosenSeason + 1).toString();
    refinedUrlPhase =
        (widget.urlPhase).replaceAll(".json", '/season/' + seasonNo + '.json');
    if (widget.authenticationToken.isEmpty) {
      urlName = "https://api.flixjini.com" + refinedUrlPhase + '?';
    } else {
      urlName = "https://api.flixjini.com" +
          refinedUrlPhase +
          "?" +
          "jwt_token=" +
          widget.authenticationToken +
          "&";
    }
    return urlName;
  }

  fetchSelectedSeasonEpisdoes() async {
    bool requestFlag = false;
    var data;
    String url = "";
    url = constructUrl();
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    printIfDebug("Tv Show Get Season");
    printIfDebug(url);

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(response.body);
      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        data = json.decode(responseBody);
        requestFlag = true;
      } else {
        printIfDebug(
            "Error getting IP address:\nHttp status ${response.statusCode}");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (requestFlag) {
      printIfDebug("Episode Recived");
      printIfDebug(data);
      widget.updateSelectedSeasonEpisodes(data["episodes"], chosenSeason);
    }
    if (!mounted) return;
  }

  Widget setSeasonBanner(int index) {
    return new Expanded(
      child: new Container(
        width: 220.0,
        padding: const EdgeInsets.all(0.0),
        child: new Center(
          child: new ClipRRect(
            borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(8.0),
                topRight: const Radius.circular(8.0)),
            child: new Image(
              image: NetworkImage(
                widget.tvShow["seasons"][index]["seasonPoster"],
                // useDiskCache: false,
              ),
              fit: BoxFit.cover,
              width: cardLayout["width"],
              gaplessPlayback: true,
            ),
          ),
        ),
      ),
      flex: 2,
    );
  }

  Widget setSeasonName(int index) {
    Color overallColor = Constants.appColorL3;
    return new Expanded(
      child: new Container(
        child: new ClipRRect(
          borderRadius: new BorderRadius.only(
              bottomLeft: const Radius.circular(8.0),
              bottomRight: const Radius.circular(8.0)),
          child: new Container(
            color: Constants.appColorFont,
            padding: const EdgeInsets.all(0.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new Text(
                    widget.tvShow["seasons"][index]["label"],
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: overallColor,
                        fontSize: 10.0),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: new Image.asset(
                    "assests/season_select.png",
                    fit: BoxFit.cover,
                    width: 20.0,
                    height: 20.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget buildAllSeasonCards(BuildContext context, int index) {
    // var sections = <Widget>[];
    // sections.add(setSeasonBanner(index));
    // sections.add(setSeasonName(index));
    // printIfDebug("shakthi" +
    //     widget.tvShow["seasons"][index].keys.toString());
    // String episodesLength = (widget.tvShow["seasons"][index] != null &&
    //         widget.tvShow["seasons"][index]["episodes"] != null)
    //     ? widget.tvShow["seasons"][index]["episodes"].length.toString()
    //     : "0";
    String episodesLength =
        widget.tvShow["seasons"][index]["episodeCount"].toString();
    return Container(
      width: 160,

      // height: index == chosenSeason ? 100 : 100,
      child: AnimationConfiguration.staggeredList(
        position: index,
        duration: const Duration(milliseconds: 375),
        child: SlideAnimation(
          verticalOffset: 50.0,
          child: FadeInAnimation(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    setChosenPage(0);
                    selectedSeason(index);
                    Navigator.of(context).push(
                      new MaterialPageRoute(
                        builder: (BuildContext context) => new MovieTvshowPage(
                          tvShowMin: widget.tvShow,
                          defaultChosenSeason: chosenSeason,
                          authenticationToken: widget.authenticationToken,
                          urlPhase: widget.urlPhase,
                        ),
                      ),
                    );
                  },
                  child:

                      // getBlurredImageContainer(index),
                      getSeasonCard(
                          index, episodesLength, index == chosenSeason),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getSeasonCard(
    int index,
    String episodesLength,
    bool flag,
  ) {
    return Container(
      child:
          // Stack(
          //   children: <Widget>[
          // Container(
          //   height: 350,
          //   width: 200,
          //   margin: EdgeInsets.all(
          //     5,
          //   ),
          //   child: Image.asset(
          //     getRandomImageFilePath(
          //       index,
          //     ),
          //     fit: BoxFit.fill,
          //   ),
          // ),
          Container(
        // color: Constants.appColorFont,
        width: 160,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: flag ? Constants.appColorLogo : Constants.appColorL1,
        ),
        // height: index == chosenSeason ? 100 : 100,
        height: flag ? cardLayout["height"] - 60 : 70,

        margin: EdgeInsets.all(
          5,
        ),
        padding: EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Center(
                child: Row(
                  children: [
                    Text(
                      widget.tvShow["seasons"][index]["title"] ?? "",
                      // textAlign: TextAlign.center,
                      style: TextStyle(
                        color: flag
                            ? Constants.appColorL1
                            : Constants.appColorFont,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.5,
                        fontSize: 15,
                      ),
                    ),
                    Spacer(),
                    flag
                        ? Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Constants.appColorL1,
                            ),
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.play_arrow,
                              size: 15,
                              color: Constants.appColorLogo,
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
            flag
                ? Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // margin: EdgeInsets.only(bottom: 10),
                          color: Constants.appColorL1.withOpacity(0.70),
                          height: 1,
                        ),
                        Spacer(),
                        Text(
                          episodesLength + " Episodes",
                          style: TextStyle(
                            color: index == chosenSeason
                                ? Constants.appColorL1
                                : Constants.appColorFont,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            letterSpacing: 1.0,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      //   ],
      // ),
    );
  }

  Widget getBlurredImageContainer(int index) {
    return Container(
      margin: EdgeInsets.only(
        right: 10,
      ),
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: ExactAssetImage('assests/movie_details_page_test_image.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: new Center(
        child: new ClipRect(
          child: new SizedBox(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: 8.0,
                sigmaY: 8.0,
              ),
              child: new Center(
                child: Container(
                  decoration: new BoxDecoration(
                      color: Constants.appColorL1.withOpacity(0.5)),
                  child: Center(
                    child: Text(
                      "SEASON ${index + 1}",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 20,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget tvShowSeasonsView() {
    return Container(
      height: cardLayout["height"] - 20,
      child: ListView.builder(
        itemCount: widget.tvShow["seasons"].length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        // padding: const EdgeInsets.all(10.0),
        itemBuilder: (BuildContext context, int index) {
          return buildAllSeasonCards(
            context,
            index,
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return widget.tvShow["seasons"].length > chosenSeason
        ? Container(
            color: Constants.appColorL1,
            width: screenWidth,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                tvShowSeasonsView(),
                new MovieDetailTvShowChosenSeason(
                  season: widget.tvShow["seasons"][chosenSeason],
                  setChosenPage: setChosenPage,
                  chosenPage: chosenPage,
                ),
              ],
            ),
          )
        : Container();
  }
}
