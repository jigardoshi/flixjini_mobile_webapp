import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:html_unescape/html_unescape.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_seasons.dart';
// import 	'dart:convert';
// import 'dart:ui';

class MovieDetailTvShowSection extends StatefulWidget {
  MovieDetailTvShowSection({
    Key? key,
    this.tvShowSection,
    this.authenticationToken,
    this.urlPhase,
    this.updateSelectedSeasonEpisodes,
  }) : super(key: key);

  final tvShowSection;
  final authenticationToken;
  final urlPhase;
  final updateSelectedSeasonEpisodes;

  @override
  _MovieDetailTvShowSectionState createState() =>
      new _MovieDetailTvShowSectionState();
}

class _MovieDetailTvShowSectionState extends State<MovieDetailTvShowSection> {
  @override
  void initState() {
    super.initState();
    printIfDebug("Tv Show");
    printIfDebug(widget.tvShowSection);
  }

  bool nullChecker(String key) {
    if (widget.tvShowSection.containsKey(key)) {
      if (widget.tvShowSection[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  renderSections() {
    var sections = <Widget>[];
    bool seasonsFlag = nullChecker("seasons");
    if (seasonsFlag) {
      sections.add(new MovieDetailTvShowSeasons(tvShow: widget.tvShowSection));
    }
    return sections;
  }

  Widget getTVShowDetailsSection() {
    return new Container(
      padding: const EdgeInsets.all(3.0),
      child: new MovieDetailTvShowSeasons(
        tvShow: widget.tvShowSection,
        authenticationToken: widget.authenticationToken,
        urlPhase: widget.urlPhase,
        updateSelectedSeasonEpisodes: widget.updateSelectedSeasonEpisodes,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new MovieDetailSubheading(
            subheading: "TV Show Details",
          ),
          // new MovieDetailUnderline(),
          getTVShowDetailsSection(),
        ],
      ),
    );
  }
}
