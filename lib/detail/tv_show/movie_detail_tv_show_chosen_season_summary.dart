import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailTvShowChosenSeasonSummary extends StatefulWidget {
  MovieDetailTvShowChosenSeasonSummary({
    Key? key,
    this.title,
    this.summary,
  }) : super(key: key);

  final title;
  final summary;

  @override
  _MovieDetailTvShowChosenSeasonSummaryState createState() =>
      new _MovieDetailTvShowChosenSeasonSummaryState();
}

class _MovieDetailTvShowChosenSeasonSummaryState
    extends State<MovieDetailTvShowChosenSeasonSummary> {
  var cardLayout = {};
  double screenWidth = 0.0;

  @override
  void initState() {
    cardLayout = {"height": 120.0, "width": 220.0};
    super.initState();
  }

  Widget seasonTitle() {
    Color overallColor = Constants.appColorFont;
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Container(
          width: screenWidth * 0.9,
          // padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
          child: new Text(
            widget.title,
            textAlign: TextAlign.left,
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: overallColor,
                fontSize: 18.0),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }

  Widget seasonSummary() {
    Color overallColor = Constants.appColorFont;
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Center(
          child: new Container(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
            width: screenWidth * 0.9,
            child: new Text(
              widget.summary,
              textAlign: TextAlign.left,
              style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: overallColor,
                  fontSize: 10.0),
              maxLines: 10,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      width: screenWidth,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          seasonTitle(),
          seasonSummary(),
        ],
      ),
    );
  }
}
