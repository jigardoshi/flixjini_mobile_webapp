import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/rendering.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShareSheet extends StatefulWidget {
  ShareSheet({
    this.sharingFlag,
    this.shareSocial1,
    this.takeScreenShot,
    this.changeShareType,
    this.showBottomSheet,
    Key? key,
  }) : super(key: key);
  final sharingFlag;
  final shareSocial1;
  final takeScreenShot;
  final changeShareType;
  final showBottomSheet;

  @override
  _ShareSheetState createState() => new _ShareSheetState();
}

class _ShareSheetState extends State<ShareSheet> {
  Widget bottomSheetBox() {
    return Container(
      color: Constants.appColorL2,
      child: GridView.builder(
          physics: new BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1,
            // mainAxisSpacing: 10.0,
            // crossAxisSpacing: 10.0,
            crossAxisCount: 4,
          ),
          padding: const EdgeInsets.all(10.0),
          itemCount: widget.shareSocial1.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                widget.showBottomSheet(false);
                widget.changeShareType(
                    widget.shareSocial1[index]["shareType"], true);
                Future.delayed(const Duration(milliseconds: 100), () {
                  widget.takeScreenShot(index);
                });
              },
              child: Container(
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      widget.shareSocial1[index]["icon"],
                      color: Constants.appColorDivider,
                      height: 50,
                    ),
                    Text(
                      widget.shareSocial1[index]["name"],
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 12,
                      ),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return bottomSheetBox();
  }
}
