import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_chosen_season_poster.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_chosen_season_summary.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_chosen_season_episodes.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailTvShowChosenSeason extends StatefulWidget {
  MovieDetailTvShowChosenSeason(
      {Key? key,
      this.season,
      this.chosenSeason,
      this.setChosenPage,
      this.chosenPage})
      : super(key: key);

  final season;
  final chosenSeason;
  final setChosenPage;
  final chosenPage;

  @override
  _MovieDetailTvShowChosenSeasonState createState() =>
      new _MovieDetailTvShowChosenSeasonState();
}

class _MovieDetailTvShowChosenSeasonState
    extends State<MovieDetailTvShowChosenSeason> {
  @override
  void initState() {
    printIfDebug("The Chosen Season is");
    printIfDebug(widget.chosenSeason);
    super.initState();
  }

  bool nullChecker(String key) {
    if (widget.season.containsKey(key)) {
      if (widget.season[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  bool nullCheckerSpecial(String key) {
    if (widget.season.containsKey(key)) {
      return true;
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  Widget setSeasonBanner(int index) {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(0.0),
        child: new Center(
          child: new ClipRRect(
            borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(8.0),
                topRight: const Radius.circular(8.0)),
            child: new Image(
              image: NetworkImage(
                widget.season["episodes"][index]["seasonPoster"],
                // useDiskCache: false,
              ),
              fit: BoxFit.cover,
              width: 220.0,
              height: 180.0,
              gaplessPlayback: true,
            ),
          ),
        ),
      ),
      flex: 2,
    );
  }

  Widget setSeasonName(int index) {
    Color overallColor = Constants.appColorL3;
    return new Expanded(
      child: new Container(
        child: new ClipRRect(
          borderRadius: new BorderRadius.only(
              bottomLeft: const Radius.circular(8.0),
              bottomRight: const Radius.circular(8.0)),
          child: new Container(
              color: Constants.appColorFont,
              padding: const EdgeInsets.all(0.0),
              child: new Center(
                child: new Text(
                  widget.season["episodes"][index]["label"],
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      color: overallColor,
                      fontSize: 10.0),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              )),
        ),
      ),
      flex: 1,
    );
  }

  Widget buildAllSeasonCards(BuildContext context, int index) {
    var sections = <Widget>[];
    sections.add(setSeasonBanner(index));
    sections.add(setSeasonName(index));
    return new Container(
      child: new Container(
        width: 220.0,
        height: 150.0,
        child: new Card(
          elevation: 6.0,
          color: Constants.appColorFont,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: sections,
          ),
        ),
      ),
    );
  }

  Widget tvShowSeasonsView() {
    return new SizedBox.fromSize(
      size: const Size.fromHeight(150.0),
      child: new ListView.builder(
        itemCount: widget.season["episodes"].length,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.all(10.0),
        itemBuilder: (context, index) => buildAllSeasonCards(
          context,
          index,
        ),
      ),
    );
  }

  renderSections() {
    var sections = <Widget>[];
    bool posterFlag = nullChecker("seasonPoster");
    bool labelFlag = nullChecker("label");
    bool summaryFlag = nullCheckerSpecial("summary");
    bool episodeFlag = nullCheckerSpecial("episodes");
    if (posterFlag) {
      // sections.add(new MovieDetailTvShowChosenSeasonPoster(
      //     poster: widget.season["seasonPoster"]));
    }
    if (labelFlag && summaryFlag) {
      sections.add(new MovieDetailTvShowChosenSeasonSummary(
          title: widget.season["label"], summary: widget.season["summary"]));
    }

    sections.add(
      Container(
        margin: EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Divider(
          height: 0,
          color: Constants.appColorDivider,
        ),
      ),
    );

    if (labelFlag &&
        summaryFlag &&
        episodeFlag &&
        widget.season["streamingSources"] != null) {
      sections.add(
        MovieDetailTvShowChosenSeasonEpisodes(
          episodes: widget.season["episodes"],
          seasonLevelStreamingSources: widget.season["streamingSources"],
          setChosenPage: widget.setChosenPage,
          chosenPage: widget.chosenPage,
        ),
      );
    }
    return sections;
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      color: Constants.appColorL1,
      margin: EdgeInsets.only(
        top: 20,
      ),
      width: screenWidth,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: renderSections(),
      ),
    );
  }
}
