import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import	'package:streaming_entertainment/detail/tv_show/movie_detail_tv_show_chosen_season_poster.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';

class MovieDetailTvShowChosenSeasonPoster extends StatefulWidget {
  MovieDetailTvShowChosenSeasonPoster({
    Key? key,
    this.poster,
  }) : super(key: key);

  final poster;

  @override
  _MovieDetailTvShowChosenSeasonPosterState createState() =>
      new _MovieDetailTvShowChosenSeasonPosterState();
}

class _MovieDetailTvShowChosenSeasonPosterState
    extends State<MovieDetailTvShowChosenSeasonPoster> {
  var cardLayout = {};

  @override
  void initState() {
    cardLayout = {"height": 120.0, "width": 220.0};
    super.initState();
  }

  Widget chosenSeasonBanner() {
    return new Container(
      padding: const EdgeInsets.all(0.0),
      child: new Center(
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: new Image(
            image: NetworkImage(
              widget.poster,
              // useDiskCache: false,
            ),
            fit: BoxFit.cover,
            width: cardLayout["width"],
            height: cardLayout["height"],
            gaplessPlayback: true,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      width: screenWidth,
      child: chosenSeasonBanner(),
    );
  }
}
