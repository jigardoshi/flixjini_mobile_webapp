import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import  'package:streaming_entertainment/detail/tv_show/custom_expansion_panel_list.dart';
import 'package:url_launcher/url_launcher.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailTvShowChosenSeasonEpisodes extends StatefulWidget {
  MovieDetailTvShowChosenSeasonEpisodes({
    Key? key,
    this.episodes,
    this.seasonLevelStreamingSources,
    this.setChosenPage,
    this.chosenPage,
  }) : super(key: key);

  final episodes;
  final seasonLevelStreamingSources;
  final setChosenPage;
  final chosenPage;

  @override
  _MovieDetailTvShowChosenSeasonEpisodesState createState() =>
      new _MovieDetailTvShowChosenSeasonEpisodesState();
}

class _MovieDetailTvShowChosenSeasonEpisodesState
    extends State<MovieDetailTvShowChosenSeasonEpisodes> {
  var cardLayout = {};
  double screenWidth = 0.0;
  var reviewPerPage = [];
  var totalNumberOfPages = 0;
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    printIfDebug("Recieved Episodes");
    printIfDebug(widget.episodes);
    cardLayout = {"height": 120.0, "width": 220.0};
    super.initState();
  }

  _launchURL(seed) async {
    String url = seed;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  setTotalNumberOfPage() {
    totalNumberOfPages = (widget.episodes.length / 20).ceil();
  }

  keepOnlyRenderedReviews() {
    printIfDebug("The chosen Page value");
    printIfDebug(widget.chosenPage);
    int start = widget.chosenPage * 20;
    int stop = start + 20;
    int numberOfReview = widget.episodes.length;
    if (stop > numberOfReview) {
      stop = numberOfReview;
    }
    var temp = [];
    for (int i = start; i < stop; i++) {
      temp.add(widget.episodes[i]);
    }
    setState(() {
      reviewPerPage = temp;
    });
  }

  Widget subheading() {
    Color overallColor = Constants.appColorFont;
    return new Expanded(
      child: new Container(
        child: new Text(
          "Streaming On",
          textAlign: TextAlign.left,
          style: new TextStyle(
              fontWeight: FontWeight.bold, color: overallColor, fontSize: 12.0),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      flex: 1,
    );
  }

  Widget buildAllSeasonCards(BuildContext context, int index) {
    printIfDebug('\n\nhey there! ??????????');
    printIfDebug(
      widget.seasonLevelStreamingSources[index]["image"],
    );
    return new Container(
      child: new Container(
        width: 60.0,
        height: 60.0,
        child: new GestureDetector(
          onTap: () {
            printIfDebug("Tapped of Season Level Source");
            openLinkFunction(
                widget.seasonLevelStreamingSources[index]["link"],
                widget.seasonLevelStreamingSources[index]["deeplink_helper"] !=
                        null
                    ? widget.seasonLevelStreamingSources[index]
                        ["deeplink_helper"]["package"]
                    : "",
                widget.seasonLevelStreamingSources[index]["name"]);
          },
          child: new Card(
            elevation: 4.0,
            color: Constants.appColorL1,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Container(
                child: Image(
                  image:
                      // ExactAssetImage(
                      //   'assests/streaming_sources/' +
                      //       getStreamingSourceNumber(
                      //         widget.seasonLevelStreamingSources[index]['image'],
                      //       ),
                      NetworkImage(
                    widget.seasonLevelStreamingSources[index]["image"],
                    // useDiskCache: false,
                  ),
                  fit: BoxFit.contain,
                  height: 60.0,
                  gaplessPlayback: true,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  String getStreamingSourceNumber(
    String imgUrl,
  ) {
    imgUrl = imgUrl.replaceAll(
      'https://c.kmpr.in/assets/streaming-sources/',
      '',
    );
    return imgUrl;
  }

  Widget buildEpisodeStreamingSourcesBreadcrumbs(
      BuildContext context, int index, int i) {
    Color overallColor = Constants.appColorFont;
    return new Container(
      padding: const EdgeInsets.all(1.0),
      child: new InkWell(
        onTap: () {
          printIfDebug("Episode Level - Streaming Source Clicked on");
          _launchURL(reviewPerPage[i]["streamingSources"][index]["link"]);
        },
        child: new Container(
          padding: const EdgeInsets.only(left: 5.0, right: 5.0),
          decoration: new BoxDecoration(
            color: Constants.appColorL2,
            border:
                new Border.all(color: Constants.appColorDivider, width: 1.0),
            borderRadius: new BorderRadius.circular(30.0),
          ),
          child: new Container(
            padding: const EdgeInsets.only(left: 5.0, right: 5.0),
            child: new Center(
              child: new Text(
                reviewPerPage[i]["streamingSources"][index]["name"],
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: overallColor,
                    fontSize: 8.0),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget episodeStreamingScourceList() {
    return new Expanded(
      child: new SizedBox.fromSize(
        size: new Size.fromHeight(80.0),
        child: new ListView.builder(
          itemCount: widget.seasonLevelStreamingSources.length,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: (context, index) => buildAllSeasonCards(
            context,
            index,
          ),
        ),
      ),
      flex: 3,
    );
  }

  Widget episodeDropDownArrow() {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.only(bottom: 3.0),
        child: new Image.asset(
          "assests/drop_down_arrow.png",
          width: 13.0,
          height: 13.0,
          fit: BoxFit.cover,
        ),
      ),
      flex: 1,
    );
  }

  Widget episodeSymbol(i) {
    return new Expanded(
      child: new Container(
        height: 40.0,
        width: 40.0,
        padding: const EdgeInsets.all(1.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Constants.appColorL2,
        ),
        child: Center(child: episodeShortTitle(i)),
      ),
      flex: 1,
    );
  }

  Widget episodeShortTitle(int i) {
    Color overallColor = Constants.appColorFont;
    return new Container(
      padding: const EdgeInsets.only(top: 2.0),
      child: new Center(
        child: new Text(
          "E" + (i + 1).toString(),
          textAlign: TextAlign.left,
          style: new TextStyle(
              fontWeight: FontWeight.bold, color: overallColor, fontSize: 12.0),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }

  Widget episodeTitle(int i) {
    Color overallColor = Constants.appColorFont;
    return new Container(
      padding: const EdgeInsets.only(left: 5.0),
      child: new Text(
        reviewPerPage[i]["label"],
        textAlign: TextAlign.left,
        style: new TextStyle(
            fontWeight: FontWeight.bold, color: overallColor, fontSize: 12.0),
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget episodeSources(int i) {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.only(
          top: 5.0,
        ),
        child: new ListView.builder(
            itemCount: reviewPerPage[i]["streamingSources"].length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(1.0),
            itemBuilder: (context, index) =>
                buildEpisodeStreamingSourcesBreadcrumbs(context, index, i)),
      ),
      // ),
      flex: 4,
    );
  }

  Widget episodeHeading(int i) {
    return new Container(
      padding: const EdgeInsets.only(left: 15.0),
      height: 35.0,
      child: Center(child: episodeTitle(i)),
    );
  }

  Widget episodePlay(int i) {
    return new InkWell(
      onTap: () {
        //print("Go to TV Show Streaming Source");
        // _launchURL(reviewPerPage[i]["streamingSources"][0]["link"]);
        printIfDebug(reviewPerPage[i]["streamingSources"]);
        openLinkFunction(
            reviewPerPage[i]["streamingSources"][0]["link"],
            reviewPerPage[i]["streamingSources"][0]["deeplink_helper"] != null
                ? reviewPerPage[i]["streamingSources"][0]["deeplink_helper"]
                    ["package"]
                : "",
            reviewPerPage[i]["streamingSources"][0]["name"]);
      },
      child: new Container(
        child: new Image.asset(
          "assests/play_episode.png",
          width: 25.0,
          height: 25.0,
          color: Constants.appColorLogo,
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }

  Widget eachEpisodeStreamingSources(int i) {
    return Container(
      height: 20,
      margin: EdgeInsets.only(top: 10, bottom: 00),
      child: ListView.builder(
        itemCount: reviewPerPage[i]["streamingSources"].length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          bool activatedFlag = reviewPerPage[i]["streamingSources"][index]
                          ["deeplink_helper"] !=
                      null &&
                  reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                          ["activated"] !=
                      null
              ? reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                  ["activated"]
              : false;
          bool upgradableFlag = reviewPerPage[i]["streamingSources"][index]
                          ["deeplink_helper"] !=
                      null &&
                  reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                          ["upgradable"] !=
                      null
              ? reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                  ["upgradable"]
              : false;
          bool includedFlag = reviewPerPage[i]["streamingSources"][index]
                          ["deeplink_helper"] !=
                      null &&
                  reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                      ["included"]
              ? reviewPerPage[i]["streamingSources"][index]["deeplink_helper"]
                  ["included"]
              : false;

          return GestureDetector(
            onTap: () {
              openLinkFunction(
                  reviewPerPage[i]["streamingSources"][index]["link"],
                  reviewPerPage[i]["streamingSources"][index]
                              ["deeplink_helper"] !=
                          null
                      ? reviewPerPage[i]["streamingSources"][index]
                          ["deeplink_helper"]["package"]
                      : "",
                  reviewPerPage[i]["streamingSources"][index]["name"]);
            },
            child: Container(
              margin: EdgeInsets.only(right: 10),
              padding: EdgeInsets.only(
                left: 15,
                right: 15,
                top: 5,
                bottom: 5,
              ),
              decoration: BoxDecoration(
                color: Constants.appColorL1,
                border: Border.all(
                  color: Constants.appColorDivider.withOpacity(0.3),
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              child: Row(
                children: [
                  Text(
                    reviewPerPage[i]["streamingSources"][index]["name"],
                    style: TextStyle(
                      color: (activatedFlag || includedFlag || upgradableFlag)
                          ? Constants.appColorLogo.withOpacity(0.75)
                          : Constants.appColorDivider.withOpacity(0.75),
                      fontSize: 10,
                    ),
                  ),
                  reviewPerPage[i]["streamingSources"][index]["language"] !=
                          null
                      ? Text(
                          " - " +
                              reviewPerPage[i]["streamingSources"][index]
                                      ["language"]
                                  .toString()
                                  .toUpperCase(),
                          style: TextStyle(
                            color: (activatedFlag ||
                                    includedFlag ||
                                    upgradableFlag)
                                ? Constants.appColorLogo.withOpacity(0.75)
                                : Constants.appColorDivider.withOpacity(0.75),
                            fontSize: 10,
                          ),
                        )
                      : Text(
                          "",
                          style: TextStyle(
                            color: Constants.appColorLogo,
                            fontSize: 10,
                          ),
                        ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget episodeSummary(int i) {
    Color overallColor = Constants.appColorFont;
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Center(
        child: new Text(
          reviewPerPage[i]["summary"],
          textAlign: TextAlign.left,
          style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: overallColor,
              fontSize: 11.0),
          maxLines: 10,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }

  constructEpisodeCard(BuildContext context, int i) {
    printIfDebug(reviewPerPage[i]["streamingSources"]);
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      color: Constants.appColorL1,
      padding: const EdgeInsets.all(2.0),
      width: screenWidth * 0.9,
      child: new Card(
        color: Constants.appColorL2,
        // elevation: 6.0,

        child: Container(
          padding: EdgeInsets.all(10),
          color: Constants.appColorL2,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              episodeSymbol(i),
              reviewPerPage[i]["streamingSources"].length > 1
                  ? Expanded(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          episodeHeading(i),
                          Container(
                            height: 1,
                            // margin: EdgeInsets.only(left: 5, right: 10),
                            // width: screenWidth * 0.9,
                            color: Constants.appColorDivider.withOpacity(0.30),
                          ),
                          eachEpisodeStreamingSources(i),
                        ],
                      ),
                    )
                  : Expanded(
                      flex: 4,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            flex: 4,
                            child: episodeHeading(i),
                          ),
                          Expanded(
                            flex: 1,
                            child: episodePlay(i),
                          ),
                        ],
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  checkAppAndLaunch(
      bool appPresent, String packageName, String urlLaunch) async {
    if (appPresent) {
      if (urlLaunch.contains("https")) {
        launchURL(urlLaunch);
      } else {
        Map<String, String> eventData = {
          'url': urlLaunch,
          'app_name': packageName,
        };

        // await platform.invokeMethod('openAppIntent', eventData);
      }
    } else {
      launchURL("https://play.google.com/store/apps/details?id=" + packageName);
    }
  }

  openLinkFunction(String url, String package, String sourceKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String alwaysOpenkeysString = prefs.getString('alwaysOpenAppKeys') ?? "[]";
    List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);
    bool alwaysOpenInAppFlag = alwaysOpenKeysList.contains(package);
    bool appPresent = true;
    if (package.isNotEmpty) {
      appPresent = await checkIfAndroidAppIsInstalled(package);
    }

    if (!alwaysOpenInAppFlag) {
      Map<String, String> eventData = {'url': url, 'app_name': sourceKey};

      String check =
          ""; // await platform.invokeMethod('openWebView', eventData);
      printIfDebug(check);
      switch (check) {
        case "":
          break;
        case "yes":
          saveAppSP(package,
              sourceKey.toLowerCase().replaceAll("-", "").replaceAll(" ", ""));

          checkAppAndLaunch(appPresent, package, url);

          break;
        case "no":
          checkAppAndLaunch(appPresent, package, url);

          break;
        default:
      }
    } else {
      checkAppAndLaunch(appPresent, package, url);
    }
    // launchURL(url);
    // } else {
    //   printIfDebug("launch webview");
    //   _launchURL(context, url);
    // }
  }

  saveAppSP(selectedPackageName, selectedAppName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String alwaysOpenString = prefs.getString('alwaysOpenApp') ?? "[]";
    String alwaysOpenkeysString = prefs.getString('alwaysOpenAppKeys') ?? "[]";

    List alwaysOpenList = jsonDecode(alwaysOpenString);
    List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);
    alwaysOpenKeysList.add(selectedPackageName);
    alwaysOpenList.add(
      {
        "package_name": selectedPackageName,
        "key_name": selectedAppName,
      },
    );
    setState(() {
      prefs.setString("alwaysOpenApp", jsonEncode(alwaysOpenList));
      prefs.setString("alwaysOpenAppKeys", jsonEncode(alwaysOpenKeysList));
    });
  }

  constructEpisodeCard1(BuildContext context, int i) {
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      color: Constants.appColorL1,
      padding: const EdgeInsets.all(2.0),
      width: screenWidth * 0.9,
      child: new Card(
        color: Constants.appColorL2,
        // elevation: 6.0,
        child: ExpansionTile(
          backgroundColor: Constants.appColorL2,
          initiallyExpanded: false,
          trailing: episodePlay(i),
          title: new Container(
            color: Constants.appColorL2,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                episodeSymbol(i),
                episodeHeading(i),
              ],
            ),
          ),
          children: <Widget>[
            episodeSummary(i),
          ],
        ),
      ),
    );
  }

  Widget episodeList() {
    return new Container(
      color: Constants.appColorL1,
      padding: const EdgeInsets.only(bottom: 20.0),
      child: new ListView.builder(
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          itemCount: reviewPerPage.length,
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(0.0),
          itemBuilder: (context, index) =>
              constructEpisodeCard(context, index)),
    );
  }

  Widget episodeHeaderBar() {
    return new Container(
      width: screenWidth * 0.9,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          subheading(),
          episodeStreamingScourceList(),
        ],
      ),
    );
  }

  pageStatusUI() {
    if (totalNumberOfPages == 1) {
      return new Expanded(
        child: new Container(),
        flex: 1,
      );
    } else {
      String currentPage = (widget.chosenPage + 1).toString();
      String total = totalNumberOfPages.toString();
      return new Expanded(
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Text(
              currentPage + " / " + total,
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
                color: Constants.appColorFont,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
          ),
        ),
        flex: 1,
      );
    }
  }

  pageMoveForward() {
    Widget button;
    if ((widget.chosenPage + 1) >= totalNumberOfPages) {
      button = new Container();
    } else {
      button = new Container(
        height: 25,
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: new RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: new Text(
              "Next",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w800,
                // letterSpacing: 1,
                color: Constants.appColorFont.withOpacity(0.6),
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            color: Constants.appColorL2,
            onPressed: () {
              widget.setChosenPage(widget.chosenPage + 1);
              keepOnlyRenderedReviews();
            }),
      );
    }
    return new Expanded(
      child: button,
      flex: 1,
    );
  }

  pageMoveBackward() {
    Widget button;
    if (widget.chosenPage <= 0) {
      button = new Container();
    } else {
      button = new Container(
        height: 25,
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: new RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            child: new Text(
              "Previous",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w800,
                // letterSpacing: 1,
                color: Constants.appColorFont.withOpacity(0.6),
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            color: Constants.appColorL2,
            onPressed: () {
              widget.setChosenPage(widget.chosenPage - 1);
              keepOnlyRenderedReviews();
            }),
      );
    }
    return new Expanded(
      child: button,
      flex: 1,
    );
  }

  Widget displayReviewsNavigation() {
    return new Container(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          pageMoveBackward(),
          pageStatusUI(),
          pageMoveForward(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    setTotalNumberOfPage();
    keepOnlyRenderedReviews();
    screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      color: Constants.appColorL1,
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      width: screenWidth,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          episodeHeaderBar(),
          displayReviewsNavigation(),
          episodeList(),
        ],
      ),
    );
  }
}
