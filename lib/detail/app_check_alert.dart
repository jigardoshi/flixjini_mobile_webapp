import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class AppCheckAlert extends StatefulWidget {
  AppCheckAlert({
    this.index,
    this.i,
    this.regularAction,
    this.sourceKey,
    this.updatePackageList,
    this.checkIfAndroidAppIsInstalled,
    this.checkPermissionSP,
  });
  final index;
  final i, regularAction, sourceKey;
  final updatePackageList, checkPermissionSP;

  final checkIfAndroidAppIsInstalled;

  @override
  AppCheckAlertState createState() => AppCheckAlertState();
}

class AppCheckAlertState extends State<AppCheckAlert> {
  bool checkBoxValue = false;

  Widget build(BuildContext context) {
    // double screenWidth = MediaQuery.of(context).size.width;

    return AlertDialog(
      backgroundColor: Constants.appColorL2,
      content: Container(
        height: 180,
        child: Column(
          children: <Widget>[
            Text(
              'This app does not support deeplinks. Do you want to open webpage or open app?',
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 14,
                height: 1.5,
              ),
              textAlign: TextAlign.left,
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.only(
                  // bottom: 7,
                  ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Checkbox(
                      value: checkBoxValue,
                      activeColor: Constants.appColorLogo,
                      checkColor: Constants.appColorL1,
                      onChanged: (bool? newValue) {
                        setState(() {
                          checkBoxValue = newValue!;
                        });
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      // left: 5,
                      bottom: 2,
                      right: 15,
                    ),
                    child: Text(
                      "SAVE SELECTION",
                      style: TextStyle(
                        color: Constants.appColorDivider,
                        fontSize: 10,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 7,
                      bottom: 7,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Open App',
                      style: TextStyle(
                        fontSize: 13,
                        color: Constants.appColorLogo,
                      ),
                    ),
                  ),
                  onTap: () {
                    if (checkBoxValue) {
                      widget.checkPermissionSP(widget.sourceKey);

                      widget.updatePackageList();
                    }
                    widget.checkIfAndroidAppIsInstalled(widget.index, widget.i);

                    Navigator.of(context).pop();
                  },
                ),
                GestureDetector(
                  child: Container(
                    // margin: EdgeInsets.only(
                    //   left: 20,
                    // ),
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top: 7,
                      bottom: 7,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      borderRadius: new BorderRadius.circular(30.0),
                    ),

                    child: Text(
                      'Open Webpage',
                      style: TextStyle(
                        color: Constants.appColorLogo,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  onTap: () {
                    widget.regularAction(widget.index, widget.i);

                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10,
                left: 5,
                right: 5,
              ),
              child: Text(
                "You will have to search for the content after the app opens if you choose Open App",
                style: TextStyle(
                  color: Constants.appColorDivider,
                  fontSize: 8,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
