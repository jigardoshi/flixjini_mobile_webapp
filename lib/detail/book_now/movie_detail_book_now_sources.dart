import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:url_launcher/url_launcher.dart';
// import	'package:streaming_entertainment/detail/movie_detail_webview.dart';
// import  'dart:async';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailBookNowSources extends StatefulWidget {
  MovieDetailBookNowSources({
    Key? key,
    this.bookTickets,
  }) : super(key: key);

  final bookTickets;

  @override
  _MovieDetailBookNowSourcesState createState() =>
      new _MovieDetailBookNowSourcesState();
}

class _MovieDetailBookNowSourcesState extends State<MovieDetailBookNowSources> {
  _launchURL(seed) async {
    String url = seed;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget buildStreamingSourceCards(BuildContext context, int index) {
    return renderCard(index);
  }

  Widget renderCard(int index) {
    String photo = widget.bookTickets["watchNowSources"][index]["playImage"];
    var sourceImage;
    var purchaseOption;
    if (photo.isEmpty) {
      photo = "assests/box_office.png";
      sourceImage = new Container(
        padding: const EdgeInsets.all(25.0),
        child: new Image.asset(
          photo,
          fit: BoxFit.contain,
        ),
      );
    } else {
      sourceImage = new Container(
        padding: const EdgeInsets.all(20.0),
        child: new Image(
          image: NetworkImage(
            photo,
          ),
          //     ExactAssetImage(
          //   getStreamingSourceLocalImageLocation(photo),
          // ),
          // placeholder: new AssetImage("images/pending.png"),
          fit: BoxFit.contain,
        ),
      );
    }
    purchaseOption = new Padding(
      padding: const EdgeInsets.all(1.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // new Container(
          //   width: 180.0,
          //   child: new Padding(
          //     padding: const EdgeInsets.all(1.0),
          //     child: new Text(
          //       widget.bookTickets["watchNowSources"][index]["playlanguage"],
          //       textAlign: TextAlign.center,
          //       style: new TextStyle(
          //           fontWeight: FontWeight.bold,
          //           color: Constants.appColorFont,
          //           fontSize: 12.0),
          //       overflow: TextOverflow.ellipsis,
          //       maxLines: 6,
          //     ),
          //   ),
          // ),
          new Padding(
            padding: const EdgeInsets.all(1.0),
            child: new FlatButton(
              color: Constants.appColorL2,
              textColor: Constants.appColorLogo,
              shape: new RoundedRectangleBorder(
                side: BorderSide(
                  color: Constants.appColorLogo,
                ),
                borderRadius: new BorderRadius.circular(30.0),
              ),
              child: new Text(
                  widget.bookTickets["watchNowSources"][index]["playPrice"],
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),
                  textAlign: TextAlign.left,
                  maxLines: 3),
              onPressed: () {
                _launchURL(
                    widget.bookTickets["watchNowSources"][index]["playURL"]);
                printIfDebug(
                    "\n\nstatement after invoking the launchURL method");
              },
            ),
          ),
        ],
      ),
    );
    return new Container(
      padding: const EdgeInsets.only(right: 5.0),
      child: new Container(
        height: 100.0,
        width: MediaQuery.of(context).size.width - 30,
        child: new Card(
          // elevation: 6.0,
          color: Constants.appColorL2,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: Center(
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Container(
                // width: 260.0,
                color: Constants.appColorL2,
                padding: const EdgeInsets.all(0.0),
                child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Expanded(
                        child: sourceImage,
                        flex: 2,
                      ),
                      new Expanded(
                        child: new Container(
                          width: 180.0,
                          child: new Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: new Text(
                              widget.bookTickets["watchNowSources"][index]
                                  ["playlanguage"],
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Constants.appColorFont,
                                  fontSize: 12.0),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 6,
                            ),
                          ),
                        ),
                        flex: 2,
                      ),
                      new Expanded(
                        child: new Container(
                          child: purchaseOption,
                        ),
                        flex: 2,
                      ),
                    ]),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget streamingSourcesCardViewRow() {
    printIfDebug("Center this");
    var sourceCards = <Widget>[];
    for (int index = 0;
        index < widget.bookTickets["watchNowSources"].length;
        index++) {
      printIfDebug("Start Source");
      var sourceCard = renderCard(index);
      sourceCards.add(sourceCard);
      printIfDebug("Done  Source");
    }
    return new Container(
      padding: const EdgeInsets.all(1.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: sourceCards,
      ),
    );
  }

  Widget streamingSourcesCardView() {
    printIfDebug("Length of watch Now Sources");
    printIfDebug(widget.bookTickets["watchNowSources"].length);
    return new SizedBox.fromSize(
      // size: const Size.fromHeight(260.0),
      child: new ListView.builder(
        itemCount: widget.bookTickets["watchNowSources"].length,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.all(10.0),
        itemBuilder: buildStreamingSourceCards,
      ),
    );
  }

  Widget checkNoOfCardsNeeded() {
    printIfDebug("Number of Rating Cards");
    printIfDebug(widget.bookTickets["watchNowSources"].length);
    if (widget.bookTickets["watchNowSources"].length < 2) {
      return streamingSourcesCardViewRow();
    } else {
      return streamingSourcesCardView();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return checkNoOfCardsNeeded();
  }
}
