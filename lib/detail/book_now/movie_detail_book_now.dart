import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/detail/book_now/movie_detail_book_now_sources.dart';
import 'package:flixjini_webapp/detail/book_now/movie_detail_book_now_search_button.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import 	'dart:convert';
// import 	'dart:io';
// import 'dart:async';
// import  'package:http/http.dart' as http;
// import 'package:url_launcher/url_launcher.dart';
// import  'dart:async';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailBookNow extends StatefulWidget {
  MovieDetailBookNow({
    Key? key,
    this.movieId,
    this.bookTickets,
    this.setBookNowLocation,
    this.bookNowKey,
  }) : super(key: key);

  final movieId;
  final bookTickets;
  final setBookNowLocation;
  final bookNowKey;

  @override
  _MovieDetailBookNowState createState() => new _MovieDetailBookNowState();
}

class _MovieDetailBookNowState extends State<MovieDetailBookNow> {
  sourcesInGivenLocation() {
    if (widget.bookTickets["watchNowSources"].length > 0) {
      return new Container(
        padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: new MovieDetailBookNowSources(
          bookTickets: widget.bookTickets,
        ),
      );
    } else {
      return new Container(
        width: 200.0,
        padding: const EdgeInsets.all(10.0),
        child: new Text(
          widget.bookTickets["sourceNotFoundMessage"],
          textAlign: TextAlign.left,
          style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: Constants.appColorFont,
              fontSize: 10.0),
          overflow: TextOverflow.ellipsis,
          maxLines: 8,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth - 10.0,
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: new Column(
          key: widget.bookNowKey,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            new MovieDetailSubheading(subheading: "Book Now"),
            // new MovieDetailUnderline(),
            new MovieDetailBookNowSearchButton(
              movieId: widget.movieId,
              bookTickets: widget.bookTickets,
              setBookNowLocation: widget.setBookNowLocation,
            ),
            sourcesInGivenLocation(),
          ]),
    );
  }
}
