import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'dart:convert';
// import 	'dart:io';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailBookNowSearchButton extends StatefulWidget {
  MovieDetailBookNowSearchButton(
      {Key? key, this.movieId, this.bookTickets, this.setBookNowLocation})
      : super(key: key);

  final movieId;
  final bookTickets;
  final setBookNowLocation;

  @override
  _MovieDetailBookNowSearchButtonState createState() =>
      new _MovieDetailBookNowSearchButtonState();
}

class _MovieDetailBookNowSearchButtonState
    extends State<MovieDetailBookNowSearchButton> {
  final TextEditingController searchController = new TextEditingController();
  late String searchString;
  bool waiting = false;
  var autoCompleteOptions;

  setSearchStringValue(value) {
    setState(() {
      searchString = value;
    });
  }

  Future delayServerRequest() async {
    if (searchString.length >= 4) {
      if (waiting) {
        printIfDebug("Still Waiting..");
      } else {
        String checkString = searchString;
        printIfDebug("Before Timeout");
        printIfDebug("Search (Current) String");
        printIfDebug(searchString);
        printIfDebug("Check (Previous) String");
        printIfDebug(checkString);
        new Timer(const Duration(seconds: 1), () {
          printIfDebug("After Timeout");
          printIfDebug("Search (Current) String");
          printIfDebug(searchString);
          printIfDebug("Check (Previous) String");
          printIfDebug(checkString);
          if (searchString != checkString) {
            printIfDebug("Dont Make Server Request");
          } else {
            printIfDebug("Make Server Request : Elastic Search");
            String appliedMoviesFilterUrl =
                "https://api.flixjini.com/citysearch/" + searchString + ".json";
            printIfDebug(appliedMoviesFilterUrl);
            sendElasticGetRequest(appliedMoviesFilterUrl);
          }
        });
      }
    }
  }

  sendElasticGetRequest(actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    http.get(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      var fetchedData = response.body;
      var jsonFetchedData = json.decode(fetchedData);
      setState(() {
        autoCompleteOptions = jsonFetchedData;
      });
      printIfDebug("New Data Fetched by Elastic Search for Auto Complete");
      printIfDebug(autoCompleteOptions);
    });
  }

  sendGetRequestWatchMovies(actionUrl) async {
    String rootUrl = actionUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    http.get(Uri.parse(url)).then((response) {
      printIfDebug("Response status: ${response.statusCode}");
      var jsonFetchedData = json.decode(response.body);
      printIfDebug("New Data Fetched Results of a location");
      printIfDebug(jsonFetchedData);
      widget.setBookNowLocation(jsonFetchedData);
    });
  }

  Widget searchButton() {
    return new Container(
      padding: const EdgeInsets.all(2.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 250.0,
            padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
            child: new Theme(
              data: new ThemeData(
                primaryColor: Constants.appColorFont,
                primaryColorDark: Constants.appColorFont,
                hintColor:  Constants.appColorL2,
              ),
              child: new TextField(
                style: new TextStyle(color: Constants.appColorFont),
                cursorColor: Constants.appColorFont,
                textInputAction: TextInputAction.go,
                onSubmitted: (value) {
                  printIfDebug("Submitted");
                  printIfDebug(value);
                },
                controller: searchController,
                decoration: new InputDecoration(
                  prefixIcon: new Icon(
                    Icons.location_on,
                    color: Constants.appColorLogo,
                  ),
                  filled: true,
                  hintStyle: new TextStyle(
                    color: Constants.appColorFont,
                  ),
                  hintText: widget.bookTickets["location"],
                  fillColor: Constants.appColorL2,
                ),
                onChanged: (value) {
                  setSearchStringValue(value);
                  delayServerRequest();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.all(2.0),
      child: SizedBox(
        height: 2.5,
        width: 100.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color:  Constants.appColorDivider,
          ),
        ),
      ),
    );
  }

  Widget buildOptions(BuildContext context, int index) {
    return new Container(
      padding: const EdgeInsets.all(5.0),
      child: new GestureDetector(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              height: 40.0,
              width: 100.0,
              color:  Constants.appColorL2,
              padding: const EdgeInsets.all(2.0),
              child: new Center(
                child: new Text(
                  autoCompleteOptions[index].toString().toUpperCase(),
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    color: Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ),
            ),
            insertDivider(),
          ],
        ),
        onTap: () {
          searchController.clear();
          printIfDebug("Clear TextField");
          printIfDebug("Selected : " + autoCompleteOptions[index]);
          String actionUrl = "https://api.flixjini.com/citymovies/" +
              (autoCompleteOptions[index].toLowerCase()).trim() +
              ".json?movie_id=" +
              widget.movieId;
          printIfDebug("Send Request to");
          printIfDebug(actionUrl);
          setState(() {
            autoCompleteOptions.clear();
          });
          sendGetRequestWatchMovies(actionUrl);
        },
      ),
    );
  }

  Widget autoCompleteList() {
    if ((autoCompleteOptions == null) || (autoCompleteOptions.isEmpty)) {
      return new Container();
    } else {
      printIfDebug(autoCompleteOptions);
      return new Container(
        child: new SizedBox.fromSize(
          size: const Size.fromHeight(100.0),
          child: new ListView.builder(
            itemCount: autoCompleteOptions.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(10.0),
            itemBuilder: buildOptions,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new Container(
          child: new Center(
            child: searchButton(),
          ),
        ),
        autoCompleteList(),
      ],
    );
  }
}
