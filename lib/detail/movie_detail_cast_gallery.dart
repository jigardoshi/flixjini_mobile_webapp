import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_gallery.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import 'package:flixjini_webapp/detail/movie_detail_more_card.dart';
import 'package:flixjini_webapp/detail/movie_detail_less_card.dart';
// import 'package:url_launcher/url_launcher.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailCastGallery extends StatefulWidget {
  MovieDetailCastGallery({
    Key? key,
    this.movieDetail,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieDetail;
  final showOriginalPosters;

  @override
  _MovieDetailCastGalleryState createState() =>
      new _MovieDetailCastGalleryState();
}

class _MovieDetailCastGalleryState extends State<MovieDetailCastGallery> {
  bool expandCastPhotos = false;
  bool needMoreGalleryView = true;
  double screenWidth = 0.0;
  int condensedDisplaySize = 4;
  int sampleLength = 0;
  String morePhoto = "assests/more.png";
  String lessPhoto = "assests/less.png";
  static const platform = const MethodChannel('api.komparify/advertisingid');

  void toogleExpansionMinimisation() {
    setState(() {
      expandCastPhotos = expandCastPhotos ? false : true;
    });
  }

  // _launchURL(seed) async {
  //   String url = "https://www.komparify.com" + seed;
  //   url = await fetchDefaultParams(url, "nonJson");
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch cast information $url';
  //   }
  // }

  Widget castBuildPhotos(BuildContext context, int index) {
    if (index < sampleLength) {
      // var photo =
      //     widget.movieDetail["castAndCrew"]["person"][index]["castPhotos"];
      // printIfDebug("Single Person");
      // printIfDebug(widget.movieDetail["castAndCrew"]["person"][index]);
      return GestureDetector(
        onTap: () {
          // printIfDebug("Image Id that has been clicked");
          // printIfDebug(photo);
          // _launchURL(
          //     widget.movieDetail["castAndCrew"]["person"][index]["castURL"]);
        },
        child: checkIfActorImagePresent(index)
            ? castCardInNormalModel(index)
            : castPhotoWhenActorImageUnavailable(index),
      );
    } else {
      return Container();
    }
  }

  Widget castGalleryView() {
    int gallaryTrackLength = widget.movieDetail["castAndCrew"]["person"].length;
    if (gallaryTrackLength > condensedDisplaySize) {
      printIfDebug("Has greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      printIfDebug("Doesn't have greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        needMoreGalleryView = false;
        sampleLength = gallaryTrackLength;
      });
    }
    printIfDebug("Based on Number of Tracks");
    printIfDebug(needMoreGalleryView);
    if (needMoreGalleryView) {
      return GridView.builder(
          padding: EdgeInsets.only(
            top: 10,
            bottom: 10,
          ),
          addAutomaticKeepAlives: false,
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 2.75,
            mainAxisSpacing: 10, // 2
            crossAxisSpacing: 10, // 2
          ),
          itemCount: 4,
          itemBuilder: (BuildContext context, int index) {
            if (index < sampleLength) {
              // var photo = widget.movieDetail["castAndCrew"]["person"][index]
              //     ["castPhotos"];
              // printIfDebug("Single Person");
              // printIfDebug(widget.movieDetail["castAndCrew"]["person"][index]);
              return GestureDetector(
                  onTap: () {
                    // printIfDebug("Image Id that has been clicked");
                    // printIfDebug(photo);
                    // _launchURL(widget.movieDetail["castAndCrew"]["person"]
                    //     [index]["castURL"]);
                    // callActorURL(widget.movieDetail["castAndCrew"]["person"]
                    //     [index]["castURL"]);
                    openActorGenie(widget.movieDetail["castAndCrew"]["person"]
                            [index]["castID"]
                        .toString());
                    sendEvent(widget.movieDetail["castAndCrew"]["person"][index]
                        ["castNames"]);
                  },
                  child:
                      // checkIfActorImagePresent(index)
                      // ?
                      castCardInNormalModel(index)
                  // : castPhotoWhenActorImageUnavailable(index),
                  );
            } else {
              return Container();
            }
          });
    } else {
      return SizedBox.fromSize(
        size: const Size.fromHeight(80.0),
        child: new ListView.builder(
          itemCount: sampleLength,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: castBuildPhotos,
        ),
      );
    }
  }

  bool checkIfActorImagePresent(int index) {
    var person = widget.movieDetail["castAndCrew"]["person"][index];
    if (person.containsKey("castPhotos") && !person["castPhotos"].isEmpty) {
      if (!person["castPhotos"].contains(
          "assets/movies/udef-ca52c44c901fbec889492abc3d6297c8.png")) {
        return true;
      }
    }
    return false;
  }

  Widget castPhotoWhenActorImageUnavailableExpandedMode(int i) {
    return new Container(
      child: GestureDetector(
        child: Card(
          // elevation: 3.0, // 6.0,
          color: Constants.appColorFont,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new Container(
            padding: EdgeInsets.only(
              left: 10,
              right: 3,
              top: 3,
              bottom: 3,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    bottom: getBottomMargin(i),
                  ),
                  child: getAutoSizeTextWidgetForCastName(
                    i,
                  ),
                ),
                getTextWidgetForCastRole(
                  i,
                ),
              ],
            ),
          ),
        ),
        onTap: () {
          printIfDebug(
              '\n\nwidget.movieDetail["castAndCrew"]["person"][i]: ${widget.movieDetail["castAndCrew"]["person"][i]['superstar']}');
        },
      ),
    );
  }

  Widget getTextWidgetForCastRole(int i) {
    String role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"];
    if (role != 'Actor') {
      return AutoSizeText(
        role.toString(),
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.bold,
          color: Constants.appColorLogo,
        ),
      );
    } else {
      return Container();
    }
  }

  Widget castPhotoWhenActorImageUnavailable(int i) {
    return Container(
      width: 180,
      child: GestureDetector(
        child: Card(
          // elevation: 3.0, // 6.0,
          color: Constants.appColorFont,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new Container(
            padding: EdgeInsets.only(
              left: 10,
              right: 3,
              top: 3,
              bottom: 3,
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          bottom: getBottomMargin(i),
                        ),
                        child: getAutoSizeTextWidgetForCastName(
                          i,
                        ),
                      ),
                      getTextWidgetForCastRole(i),
                    ],
                  ),
                  flex: 5,
                ),
              ],
            ),
          ),
        ),
        onTap: () {
          printIfDebug(
              '\n\nwidget.movieDetail["castAndCrew"]["person"][i]: ${widget.movieDetail["castAndCrew"]["person"][i]}');
        },
      ),
    );
  }

  Widget getAutoSizeTextWidgetForCastName(
    int i,
  ) {
    String actorName =
        widget.movieDetail["castAndCrew"]["person"][i]["castNames"].toString();
    return AutoSizeText(
      actorName,
      textAlign: TextAlign.left,
      overflow: TextOverflow.ellipsis,
      minFontSize: 5,
      wrapWords: false,
      style: TextStyle(
        fontWeight: getFontWeightForActorRole(i),
        fontSize: 15.0,
        color: getTextColorForActorName(
          i,
        ),
      ),
      maxLines: 1,
    );
  }

  Color getTextColorForActorName(
    int i,
  ) {
    String superStar =
            widget.movieDetail["castAndCrew"]["person"][i]["superstar"],
        role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"];
    if (role == 'Actor' && superStar == '1') {
      return Constants.appColorLogo;
    } else {
      return Constants.appColorL3;
    }
  }

  double getBottomMargin(int i) {
    String role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"],
        superStar = widget.movieDetail["castAndCrew"]["person"][i]["superstar"];

    if (role != 'Actor') {
      return 5.0;
    } else {
      if (superStar == '1') {
        return 5.0;
      } else {
        return 0.0;
      }
    }
  }

  Widget castCardInNormalModel(int i) {
    // var photo = widget.movieDetail["castAndCrew"]["person"][i]["castPhotos"];
    var role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"];
    var name = widget.movieDetail["castAndCrew"]["person"][i]["castNames"];
    return new Card(
      // elevation: 6.0,
      color: Constants.appColorL2,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(8.0),
      ),
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 10,
          right: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                bottom: 5,
              ),
              child: Text(
                role,
                style: TextStyle(
                  fontSize: 10,
                  color: Constants.appColorFont,
                ),
              ),
            ),
            Container(
              child: Text(
                name,
                style: TextStyle(
                  // fontSize: 15,
                  color: Constants.appColorFont,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),

      // new Stack(
      //     children: <Widget>[
      //       new ClipRRect(
      //         borderRadius: new BorderRadius.circular(8.0),
      //         child: new Image.network(
      //           photo,
      //           width: 100.0,
      //           height: 160.0,
      //           fit: BoxFit.cover,
      //         ),
      //       ),
      //       new Positioned(
      //         bottom: 0.0,
      //         child: new ClipRRect(
      //           borderRadius: new BorderRadius.only(
      //               bottomLeft: const Radius.circular(8.0),
      //               bottomRight: const Radius.circular(8.0)),
      //           child: new Container(
      //             width: 100.0,
      //             height: 40.0,
      //             color: Constants.appColorFont.withOpacity(0.9),
      //             child: new Center(
      //               child: new Container(
      //                 padding: const EdgeInsets.all(3.0),
      //                 child: new Text(
      //                   name,
      //                   textAlign: TextAlign.center,
      //                   softWrap: true,
      //                   style: new TextStyle(
      //                       fontWeight: getFontWeightForActorRole(i),
      //                       fontSize: 12.0,
      //                       color: Constants.appColorL3),
      //                   maxLines: 4,
      //                   overflow: TextOverflow.ellipsis,
      //                 ),
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
    );
  }

  FontWeight getFontWeightForActorRole(int i) {
    String role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"],
        superStar = widget.movieDetail["castAndCrew"]["person"][i]["superstar"];

    if (role == 'Actor' && superStar == '1') {
      return FontWeight.bold;
    } else {
      return FontWeight.normal;
    }
  }

  callActorURL(String urlActor) async {
    var urls = urlActor.split(".");
    var names = urls[0].split("/");
    String url = "https://www.flixjini.in/cast/" + names[3];
    printIfDebug(url);
    try {
      _launchURL(context, url);
    } catch (e) {
      printIfDebug("url error" + e.toString());
    }
  }

  openActorGenie(String id) async {
    String urlAppendString = "&cast_ids=" + id;
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => new MovieRouting(
          defaultTab: 2,
          urlAppendString: urlAppendString,
          showOriginalPosters: widget.showOriginalPosters,
        ),
      ),
    );
  }

  void _launchURL(BuildContext context, link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Constants.appColorL1,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      printIfDebug(e.toString());
    }
  }

  Widget castCardInExpandedMode(int i) {
    String role = widget.movieDetail["castAndCrew"]["person"][i]["castRole"];
    String name = widget.movieDetail["castAndCrew"]["person"][i]["castNames"];

    return Card(
      // elevation: 6.0,
      color: Constants.appColorL2,
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(8.0),
      ),
      child: Container(
        margin: EdgeInsets.only(
          left: 10,
          top: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                bottom: 5,
              ),
              child: Text(
                role,
                style: TextStyle(
                  color: Constants.appColorFont,
                  fontSize: 10,
                ),
              ),
            ),
            Container(
              child: Text(
                name,
                style: TextStyle(
                  // fontSize: 15,

                  color: Constants.appColorFont,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget castExpandedGalleryView() {
    int numberOfOptions =
        widget.movieDetail["castAndCrew"]["person"].length + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 155.0 * numberOfRows;
    printIfDebug("from cast expanded gallery view, grid height: $gridHeight");
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
      ),
      color: Constants.appColorL1,
      child: new GridView.builder(
          addAutomaticKeepAlives: false,
          shrinkWrap: true,
          physics: new NeverScrollableScrollPhysics(),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: (orientation == Orientation.portrait)
                ? numberOfOptionsPerRow
                : 6,
            childAspectRatio: 2.75, // 1.75,
            mainAxisSpacing: 10, // 2
            crossAxisSpacing: 10, // 2
          ),
          itemCount: widget.movieDetail["castAndCrew"]["person"].length + 1,
          itemBuilder: (BuildContext context, int i) {
            if (i < widget.movieDetail["castAndCrew"]["person"].length) {
              try {
                if (widget.movieDetail["castAndCrew"]["person"][i]['superstar']
                        .toString()
                        .isNotEmpty ||
                    widget.movieDetail["castAndCrew"]["person"][i]
                            ['superstar'] ==
                        null) {
                  // printIfDebug(
                  //     "\n\n${widget.movieDetail['castAndCrew']['person'][i]['superstar']}");
                }
              } catch (e) {
                printIfDebug('\n\nexception: $e');
              }
              // printIfDebug("Single Person");
              // printIfDebug(widget.movieDetail["castAndCrew"]["person"][i]);
              return new GridTile(
                child: new GestureDetector(
                    onTap: () {
                      // printIfDebug("Image Id that has been clicked");
                      // printIfDebug(widget.movieDetail["castAndCrew"]["person"][i]
                      //     ["castPhotos"]);
                      // _launchURL(widget.movieDetail["castAndCrew"]["person"][i]
                      //     ["castURL"]);
                      // callActorURL(widget.movieDetail["castAndCrew"]["person"]
                      //     [i]["castURL"]);
                      openActorGenie(widget.movieDetail["castAndCrew"]["person"]
                              [i]["castID"]
                          .toString());
                      sendEvent(widget.movieDetail["castAndCrew"]["person"][i]
                          ["castNames"]);
                    },
                    child:
                        // checkIfActorImagePresent(i)
                        //     ?
                        castCardInExpandedMode(i)
                    // : castPhotoWhenActorImageUnavailableExpandedMode(
                    //     i,
                    //   ),
                    ),
              );
            } else {
              // return Text(
              //   "Show less",
              //   style: TextStyle(
              //     color: Constants.appColorFont,
              //   ),
              //   textAlign: TextAlign.right,
              // );
              return MovieDetailLessCard(
                toogleExpansionMinimisation: toogleExpansionMinimisation,
                cardHeight: 160.0,
                cardWidth: 100.0,
              );
            }
          }),
    );
  }

  sendEvent(tagName) {
    Map<String, String> eventData = {
      'content': tagName.toString(),
    };
    //platform.invokeMethod('logPurchaseEvent', eventData);
  }

  Widget getMovieDetailSubHeading() {
    return MovieDetailSubheading(
      subheading: widget.movieDetail["castAndCrew"]["title"],
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    if (expandCastPhotos && needMoreGalleryView) {
      return new Container(
        width: screenWidth,
        padding: const EdgeInsets.only(
          top: 2.0,
          bottom: 2.0,
        ),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getMovieDetailSubHeading(),
              // new MovieDetailUnderline(),
              new Padding(
                padding: const EdgeInsets.all(3.0),
                child: castExpandedGalleryView(),
              ),
            ]),
      );
    } else {
      return new Container(
          width: screenWidth,
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getMovieDetailSubHeading(),
              // new MovieDetailUnderline(),
              Container(
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                  top: 10,
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: castGalleryView(),
                    ),
                    // MovieDetailMoreCard(
                    //   toogleExpansionMinimisation: toogleExpansionMinimisation,
                    //   cardHeight: 70.0, // 180.0,
                    //   cardWidth: 100.0,
                    // ),
                    needMoreGalleryView
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  toogleExpansionMinimisation();
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                    // right: 10,
                                    top: 0,
                                  ),
                                  child: Text(
                                    "SEE ALL ",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w800,
                                      letterSpacing: 1,
                                      color: Constants.appColorFont
                                          .withOpacity(0.6),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 20,
                                child: Image.asset(
                                  "assests/c_more_black.png",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ],
                          )
                        : Container(),
                  ],
                ),
              ),
            ],
          ));
    }
  }
}
