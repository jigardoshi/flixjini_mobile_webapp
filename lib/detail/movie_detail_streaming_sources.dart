// import 'dart:io';

// import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:url_launcher/url_launcher.dart' as urlLaunch;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/custom_show_dialog.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flutter_appavailability/flutter_appavailability.dart'; // platform dependent code
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flixjini_webapp/detail/app_check_alert.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:flutter/services.dart';
import 'dart:io' show Platform;
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailStreamingSources extends StatefulWidget {
  MovieDetailStreamingSources({
    Key? key,
    this.movieDetail,
    this.watchNowKey,
    this.authenticationToken,
    this.setStreamingSourceSelected,
  }) : super(key: key);

  final movieDetail;
  final watchNowKey;
  final authenticationToken;
  final setStreamingSourceSelected;

  @override
  _MovieDetailStreamingSourcesState createState() =>
      new _MovieDetailStreamingSourcesState();
}

class _MovieDetailStreamingSourcesState
    extends State<MovieDetailStreamingSources> with WidgetsBindingObserver {
  String stringprofileSettingData = "";
  String updatedStringprofileSettingData = "";
  int selectedIndex = -1, selectedI = -1;
  List<String> checkPermissionSP = [];
  FirebaseAnalytics analytics = FirebaseAnalytics();
  bool checkAppFlag = false;
  // static final facebookAppEvents = FacebookAppEvents();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  var sourceKeys = {};
  // {
  // "alt-balaji": "com.balaji.alt",
  //   "amazon-prime": "com.amazon.avod.thirdpartyclient",
  //   "vodafone-play": "com.vodafone.vodafoneplay",
  //   "viu": "com.vuclip.viu",
  //   "hooq": "tv.hooq.android",
  //   "erosnow": "com.erosnow",
  //   "voot": "com.tv.v18.viola",
  //   "sonyliv": "com.sonyliv",
  //   "jio-cinema": "com.jio.media.ondemand",
  //   "hungama-play": "com.hungama.movies",
  //   "shemaroo": "com.sonyklabs.shemaroo.gujarati.plays",
  // };

  @override
  void initState() {
    printIfDebug("Called");
    checkAppPermission();
    fetchUserprofileIfExists();
    fetchPackageList();
    sendEvents();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  sendEvents() {
    Set<String> uniqueId = new Set();
    for (int i = 0;
        i < widget.movieDetail["watchNow"]["streamingSources"].length;
        i++) {
      String source = widget.movieDetail["watchNow"]["streamingSources"][i]
              ["sourceName"]
          .toString()
          .replaceAll(" ", "")
          .toLowerCase()
          .replaceAll("-", "");

      uniqueId.add(source);
    }
    uniqueId.forEach((f) {
      _sendAnalyticsEventDetail("movie_page", f.toString());
    });
    _sendAnalyticsDetail(widget.movieDetail["movieName"]);
  }

  updateStringprofileSettingData(String temp) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('stringprofileSettingData', temp);
    });
  }

  checkAppPermission() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? check = prefs.getBool("appCheckPermission");
    if (check != null) {
      setState(() {
        checkAppFlag = check;
      });
    }
  }

  updatePackageList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setStringList('stringPackagePermission', checkPermissionSP);
  }

  fetchPackageList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> checkPresent = [];

    checkPresent = prefs.getStringList('stringPackagePermission')!;
    if (checkPresent != null && checkPresent.isNotEmpty) {
      setState(() {
        checkPermissionSP = checkPresent;
      });
    }

    String reminderDataFromFRC = await getStringFromFRC('noDeepLinkApps');
    setState(() {
      sourceKeys = jsonDecode(reminderDataFromFRC);
      // sourceKeys = {};
    });
    printIfDebug("source keys" + sourceKeys.toString());
  }

  getMovieDetails(String urlToRegisterClick, bool isDeep) async {
    var sourceDetail;
    printIfDebug("Obtained Movie Url");
    printIfDebug(urlToRegisterClick);
    urlToRegisterClick = widget.authenticationToken.toString().isNotEmpty
        ? urlToRegisterClick + "&jwt_token=" + widget.authenticationToken
        : urlToRegisterClick;
    printIfDebug("shakthi check" + urlToRegisterClick + "&magic=true");

    try {
      var response =
          await http.get(Uri.parse(urlToRegisterClick + "&magic=true"));

      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        sourceDetail = data;
        printIfDebug("Source Url Fetched");
        printIfDebug(sourceDetail["url"]);
        checkDeepLink(sourceDetail["url"], isDeep);
        // launchURL(sourceDetail["url"]);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
        if (true) {
          widget.setStreamingSourceSelected(false);
        }
      }
    } catch (exception) {
      printIfDebug(
          'Failed getting IP address to redirect to Streaming Source page');
      if (true) {
        widget.setStreamingSourceSelected(false);
      }
    }
    if (!mounted) return;
  }

  checkDeepLink(String url, bool isDeep) async {
    // if (isDeep) {
    //   printIfDebug("launch url");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String alwaysOpenkeysString = prefs.getString('alwaysOpenAppKeys') ?? "[]";
    List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);
    String selectedPackageName = widget.movieDetail["watchNow"]
                ["streamingSources"][selectedIndex]["deeplink_helper"] !=
            null
        ? widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                ["deeplink_helper"]["package"]
            .toString()
        : "";
    String appName = widget.movieDetail["watchNow"]["streamingSources"]
            [selectedIndex]["sourceName"]
        .toString();
    bool alwaysOpenInAppFlag = alwaysOpenKeysList.contains(selectedPackageName);

    // url = url.contains("?")
    //     ? (url + "&preventIntent=false")
    //     : (url + "?preventIntent=false");
    bool appPresent = true;
    if (selectedPackageName.isNotEmpty) {
      appPresent = await checkIfAndroidAppIsInstalled(selectedPackageName);
    }

    if (!alwaysOpenInAppFlag) {
      Map<String, String> eventData = {
        'url': url,
        'app_name': appName,
      };
      launchURL(url);

      String check =
          ""; //await platform.invokeMethod('openWebView', eventData);
      printIfDebug(check);
      switch (check) {
        case "":
          break;
        case "yes":
          saveAppSP();
          if (widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                          ["purchaseDetails"][selectedI]
                      ["purchaseMobileAffiliateURL"] !=
                  null &&
              widget
                  .movieDetail["watchNow"]["streamingSources"][selectedIndex]
                      ["purchaseDetails"][selectedI]
                      ["purchaseMobileAffiliateURL"]
                  .isNotEmpty &&
              appPresent &&
              selectedPackageName.isNotEmpty) {
            openMobileURl(
              widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                  ["purchaseDetails"][selectedI]["purchaseMobileAffiliateURL"],
            );
          } else {
            checkAppAndLaunch(appPresent, selectedPackageName, url);
            // launchURL(url);
          }

          break;
        case "no":
          if (widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                          ["purchaseDetails"][selectedI]
                      ["purchaseMobileAffiliateURL"] !=
                  null &&
              widget
                  .movieDetail["watchNow"]["streamingSources"][selectedIndex]
                      ["purchaseDetails"][selectedI]
                      ["purchaseMobileAffiliateURL"]
                  .isNotEmpty &&
              appPresent &&
              selectedPackageName.isNotEmpty) {
            openMobileURl(
              widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                  ["purchaseDetails"][selectedI]["purchaseMobileAffiliateURL"],
            );
          } else {
            checkAppAndLaunch(appPresent, selectedPackageName, url);
          }

          break;
        default:
      }
    } else {
      checkAppAndLaunch(appPresent, selectedPackageName, url);
    }
    // launchURL(url);
    // } else {
    //   printIfDebug("launch webview");
    //   _launchURL(context, url);
    // }
  }

  checkAppAndLaunch(
      bool appPresent, String packageName, String urlLaunch) async {
    if (appPresent) {
      if (urlLaunch.contains("https")) {
        launchURL(urlLaunch);
      } else {
        Map<String, String> eventData = {
          'url': urlLaunch,
          'app_name': "",
        };

        // await platform.invokeMethod('openAppIntent', eventData);
      }
    } else {
      launchURL("https://play.google.com/store/apps/details?id=" + packageName);
    }
  }

  openMobileURl(String urlToRegisterClick) async {
    var sourceDetail;
    // printIfDebug("Obtained Movie Url");
    // printIfDebug(urlToRegisterClick);
    urlToRegisterClick = widget.authenticationToken.toString().isNotEmpty
        ? urlToRegisterClick + "&jwt_token=" + widget.authenticationToken
        : urlToRegisterClick;
    printIfDebug(urlToRegisterClick);

    try {
      var response = await http.get(Uri.parse(urlToRegisterClick));

      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        sourceDetail = data;
        printIfDebug("Source Url Fetched");
        printIfDebug(sourceDetail["url"]);
        // checkAppAndLaunch(true, "", sourceDetail["url"]);

        launchURL(sourceDetail["url"]);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
        if (true) {
          widget.setStreamingSourceSelected(false);
        }
      }
    } catch (exception) {
      printIfDebug(
          'Failed getting IP address to redirect to Streaming Source page');
      if (true) {
        widget.setStreamingSourceSelected(false);
      }
    }
    if (!mounted) return;
  }

  saveAppSP() async {
    if (selectedIndex > -1) {
      String selectedPackageName = widget.movieDetail["watchNow"]
                  ["streamingSources"][selectedIndex]["deeplink_helper"] !=
              null
          ? widget.movieDetail["watchNow"]["streamingSources"][selectedIndex]
                  ["deeplink_helper"]["package"]
              .toString()
          : "";
      String selectedAppName = widget.movieDetail["watchNow"]
              ["streamingSources"][selectedIndex]["sourceKey"]
          .toString();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String alwaysOpenString = prefs.getString('alwaysOpenApp') ?? "[]";
      String alwaysOpenkeysString =
          prefs.getString('alwaysOpenAppKeys') ?? "[]";

      List alwaysOpenList = jsonDecode(alwaysOpenString);
      List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);
      alwaysOpenKeysList.add(selectedPackageName);
      alwaysOpenList.add(
        {
          "package_name": selectedPackageName,
          "key_name": selectedAppName,
        },
      );
      setState(() {
        prefs.setString("alwaysOpenApp", jsonEncode(alwaysOpenList));
        prefs.setString("alwaysOpenAppKeys", jsonEncode(alwaysOpenKeysList));
      });
    }
  }

  void _launchURL(BuildContext context, link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Constants.appColorL1,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      printIfDebug(e.toString());
    }
  }

  getMovieApi(String urlToRegisterClick) async {
    var sourceDetail;
    // printIfDebug("Obtained Movie Url");
    // printIfDebug(urlToRegisterClick);
    urlToRegisterClick = widget.authenticationToken.toString().isNotEmpty
        ? urlToRegisterClick + "&jwt_token=" + widget.authenticationToken
        : urlToRegisterClick;
    printIfDebug(urlToRegisterClick + "&magic=true");

    try {
      var response =
          await http.get(Uri.parse(urlToRegisterClick + "&magic=true"));

      printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        sourceDetail = data;
        printIfDebug("Source Url Fetched");
        printIfDebug(sourceDetail["url"]);
        // launchURL(sourceDetail["url"]);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
        if (true) {
          // widget.setStreamingSourceSelected(false);
        }
      }
    } catch (exception) {
      printIfDebug(
          'Failed getting IP address to redirect to Streaming Source page');
      if (true) {
        // widget.setStreamingSourceSelected(false);
      }
    }
    if (!mounted) return;
  }

  launchURL(seed) async {
    String url = seed;
    if (await urlLaunch.canLaunch(url)) {
      await urlLaunch.launch(url);
      printIfDebug("Yes we lunched url");
      if (true) {
        widget.setStreamingSourceSelected(false);
      }
    } else {
      throw 'Could not launch $url';
    }
  }

  String nullChecker(int index, int i, String key) {
    if ((widget.movieDetail["watchNow"]["streamingSources"][index]
            ["purchaseDetails"][i])
        .containsKey(key)) {
      if (key == "purchasePrice") {
        return "₹ " +
            widget.movieDetail["watchNow"]["streamingSources"][index]
                ["purchaseDetails"][i][key];
      }
      return widget.movieDetail["watchNow"]["streamingSources"][index]
          ["purchaseDetails"][i][key];
    } else {
      printIfDebug("No" + key);
      return "";
    }
  }

  fetchUserprofileIfExists() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringprofileSettingData =
          (prefs.getString('stringprofileSettingData') ?? "");
    });
    printIfDebug("Outside");
    // printIfDebug(stringprofileSettingData);
    if (stringprofileSettingData.isEmpty) {
      printIfDebug("Inside");
      getSearchFilterMenu();
    }
  }

  checkIfAnySelected(var data) {
    for (int i = 0; i < data["topics"].length; i++) {
      if (data["topics"][i]["group"] == "clickableImages") {
        bool flag = true;
        for (int index = 0;
            index < data["topics"][i]["options"].length;
            index++) {
          if (data["topics"][i]["options"][index]["status"]) {
            flag = false;
            data["topics"][i]["status"] = false;
            // printIfDebug("Category Checking");
            // printIfDebug(data["topics"][i]["heading"]);
            break;
          }
        }
        if (flag) {
          printIfDebug("All Cleared");
          data["topics"][i]["status"] = true;
        }
      }
    }
    return data;
  }

  addClickableStatus(var data) {
    for (int i = 0; i < data["topics"].length; i++) {
      if (data["topics"][i]["group"] == "clickableImages") {
        data["topics"][i]["status"] = true;
        // printIfDebug("Added Now");
        // printIfDebug(data["topics"][i]["status"]);
      }
    }
    return data;
  }

  getSearchFilterMenu() async {
    printIfDebug("It is Empty");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String filterMenuOptionsUrl =
        "https://api.flixjini.com/movies/get_user_profile.json?jwt_token=" +
            widget.authenticationToken;
    String rootUrl = filterMenuOptionsUrl + '&';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.get(Uri.parse(url));

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        data = addClickableStatus(data);
        data = checkIfAnySelected(data);
        printIfDebug("Need to convert Data to String");
        printIfDebug(data);
        String temp = json.encode(data);
        setState(() {
          prefs.setString('stringprofileSettingData', temp);
          stringprofileSettingData =
              (prefs.getString('stringprofileSettingData') ?? "");
        });
        printIfDebug("Saving Filters to Cache");
        printIfDebug(stringprofileSettingData);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  updateUserProfileLocally(int index) async {
    if (stringprofileSettingData.isEmpty) {
      printIfDebug(
          "User hasn't yet accessed profile page, anything to update in cache");
    } else {
      printIfDebug("Filter Options already present");
      var data = json.decode(stringprofileSettingData);
      for (int i = 0; i < data["topics"].length; i++) {
        if (data["topics"][i]["key"] == "sourceslist") {
          printIfDebug(data["topics"][i]["key"]);
          printIfDebug(widget.movieDetail["watchNow"]["streamingSources"][index]
              ["sourceKey"]);
          for (int j = 0; j < data["topics"][i]["options"].length; j++) {
            printIfDebug(data["topics"][i]["options"][j]["filter_id"]);
            if (data["topics"][i]["options"][j]["filter_id"] ==
                widget.movieDetail["watchNow"]["streamingSources"][index]
                    ["sourceKey"]) {
              data["topics"][i]["options"][j]["status"] = true;
              printIfDebug("Profile - Update Sucessful");
              printIfDebug(data["topics"][i]["options"][j]["name"]);
              break;
            }
          }
          break;
        }
      }
      printIfDebug("Need to convert Data to String");
      updatedStringprofileSettingData = json.encode(data);
    }
  }

  sendRequestUpdateUserProfile(String url, int index) async {
    printIfDebug("Update User Profile - Server Request");
    printIfDebug(url);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        printIfDebug("Received Response - HEHE");
        printIfDebug(data);
        printIfDebug("Now try to update the local copy of user profile");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  changeUserProfileSourceFavorites(int index) async {
    printIfDebug("Add this to user profile");
    printIfDebug(widget.movieDetail["watchNow"]["streamingSources"][index]);
    printIfDebug(
        widget.movieDetail["watchNow"]["streamingSources"][index]["sourceKey"]);
    String getUrl = "sourceslist=" +
        widget.movieDetail["watchNow"]["streamingSources"][index]["sourceKey"] +
        "&";
    String url = "https://api.flixjini.com/movies/update_user_profile_source?" +
        getUrl +
        "jwt_token=" +
        widget.authenticationToken;
    printIfDebug(url);
    sendRequestUpdateUserProfile(url, index);
  }

  Future sendUserToAuthenticationPage() {
    String note =
        "Please Authenticate Yourself save your most favorite Streaming Sources";
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return CustomAlertDialog(
            contentPadding: const EdgeInsets.all(0.0),
            content: new Container(
              height: 150.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: Constants.appColorFont,
                borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new Container(
                      padding: const EdgeInsets.all(24.0),
                      child: new Text(
                        note.isNotEmpty ? note : "",
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Constants.appColorL3),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 6,
                      ),
                    ),
                    flex: 2,
                  ),
                  new Expanded(
                    child: new Container(
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Constants.appColorLogo,
                        borderRadius: new BorderRadius.only(
                          bottomLeft: Radius.circular(18.0),
                          bottomRight: Radius.circular(18.0),
                        ),
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: new InkWell(
                              onTap: () {
                                printIfDebug("Sign Up");
                                Navigator.of(context).pushReplacement(
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new MovieAuthenticationPage(
                                                type: true)));
                              },
                              child: new Container(
                                child: new Center(
                                  child: new Text(
                                    "Sign Up",
                                    overflow: TextOverflow.ellipsis,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.normal,
                                      color: Constants.appColorFont,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            flex: 4,
                          ),
                          new Expanded(
                            child: new Container(
                              height: 30.0,
                              child: VerticalDivider(
                                  width: 30.0, color: Constants.appColorFont),
                            ),
                            flex: 1,
                          ),
                          new Expanded(
                            child: new InkWell(
                              onTap: () {
                                printIfDebug("Sign In");
                                Navigator.of(context).pushReplacement(
                                    new MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            new MovieAuthenticationPage(
                                                type: false)));
                              },
                              child: new Container(
                                child: new Center(
                                  child: new Text(
                                    "Sign In",
                                    overflow: TextOverflow.ellipsis,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.normal,
                                      color: Constants.appColorFont,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            flex: 4,
                          ),
                        ],
                      ),
                    ),
                    flex: 1,
                  ),
                ],
              ),
            ),
            actions: [],
          );
        });
  }

  basedOnSubscription(index, i, bool isDeep) async {
    String note = " Do you have a " +
        widget.movieDetail["watchNow"]["streamingSources"][index]
            ["sourceName"] +
        " App (or) Subscription?";
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return new CustomAlertDialog(
            contentPadding: const EdgeInsets.all(0.0),
            content: new Container(
              height: 150.0,
              decoration: new BoxDecoration(
                shape: BoxShape.rectangle,
                color: Constants.appColorL2,
                // borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    padding: const EdgeInsets.all(24.0),
                    margin: EdgeInsets.only(
                      bottom: 10,
                    ),
                    child: new Text(
                      note.isNotEmpty ? note : "",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Constants.appColorFont,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 6,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                          left: 30,
                          right: 30,
                          top: 7,
                          bottom: 7,
                        ),
                        decoration: new BoxDecoration(
                          border: new Border.all(
                            color: Constants.appColorLogo,
                          ),
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        child: GestureDetector(
                          child: Text(
                            'Yes',
                            style: TextStyle(
                              fontSize: 13,
                              color: Constants.appColorLogo,
                            ),
                          ),
                          onTap: () {
                            // if (true) {
                            //   widget.setStreamingSourceSelected(true);
                            // }
                            changeUserProfileSourceFavorites(index);
                            printIfDebug("YES");
                            Navigator.pop(context);

                            if (isDeep) {
                              printIfDebug("yes subs yes deep");
                              String seed = widget.movieDetail["watchNow"]
                                      ["streamingSources"][index]
                                  ["purchaseDetails"][i]["purchaseRedirectURL"];
                              getMovieDetails(seed, false);
                            } else {
                              printIfDebug("yes subs no deep");
                              showAlertAppPresent(index, i);
                              String seed = widget.movieDetail["watchNow"]
                                      ["streamingSources"][index]
                                  ["purchaseDetails"][i]["purchaseRedirectURL"];
                              printIfDebug(seed);
                              getMovieApi(seed);
                            }
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 20,
                        ),
                        padding: EdgeInsets.only(
                          left: 30,
                          right: 30,
                          top: 7,
                          bottom: 7,
                        ),
                        decoration: new BoxDecoration(
                          border: new Border.all(
                            color: Constants.appColorLogo,
                          ),
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        child: GestureDetector(
                          child: Text(
                            'No',
                            style: TextStyle(
                              color: Constants.appColorLogo,
                              fontSize: 13,
                            ),
                          ),
                          onTap: () {
                            // if (true) {
                            //   widget.setStreamingSourceSelected(true);
                            // }
                            Navigator.pop(context);
                            String seed = widget.movieDetail["watchNow"]
                                    ["streamingSources"][index]
                                ["purchaseDetails"][i]["purchaseOriginalURL"];
                            printIfDebug(seed);
                            getMovieDetails(seed, false);
                          },
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            actions: [],
          );
        });
  }

  onWatchClick(index, i) async {
    // if (widget.movieDetail["watchNow"]["streamingSources"][index]
    //             ["purchaseDetails"][i]["purchaseRedirectURL"] !=
    //         null &&
    //     widget.movieDetail["watchNow"]["streamingSources"][index]
    //             ["purchaseDetails"][i]["purchaseOriginalURL"] !=
    //         null &&
    //     widget.movieDetail["watchNow"]["streamingSources"][index]
    //             ["purchaseDetails"][i]["purchaseRedirectURL"] ==
    //         widget.movieDetail["watchNow"]["streamingSources"][index]
    //             ["purchaseDetails"][i]["purchaseOriginalURL"]) {
    //   getMovieDetails(widget.movieDetail["watchNow"]["streamingSources"][index]
    //       ["purchaseDetails"][i]["purchaseRedirectURL"]);
    // } else {
    //   // getMovieDetails(widget.movieDetail["purchaseRedirectURL"]);
    //   basedOnSubscription(
    //       index,
    //       i,
    //       !sourceKeys.containsKey(widget.movieDetail["watchNow"]
    //           ["streamingSources"][index]["sourceKey"]));
    // }
    printIfDebug("launch step 1");
    String packageName = widget.movieDetail["watchNow"]["streamingSources"]
                [index]["deeplink_helper"] !=
            null
        ? widget.movieDetail["watchNow"]["streamingSources"][index]
                ["deeplink_helper"]["package"]
            .toString()
        : "";
    bool appPresent = true;
    if (packageName.isNotEmpty) {
      appPresent = await checkIfAndroidAppIsInstalled(packageName);
    }
    setState(() {
      selectedIndex = index;
      selectedI = i;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String alwaysOpenkeysString = prefs.getString('alwaysOpenAppKeys') ?? "[]";
    List alwaysOpenKeysList = jsonDecode(alwaysOpenkeysString);
    bool alwaysOpenInAppFlag = alwaysOpenKeysList.contains(packageName);
    printIfDebug("launch step 2");

    if (alwaysOpenInAppFlag && !appPresent) {
      launchURL("https://play.google.com/store/apps/details?id=" + packageName);
    } else {
      if (widget.movieDetail["watchNow"]["streamingSources"][index]
                  ["purchaseDetails"][i]["purchaseMobileAffiliateURL"] !=
              null &&
          widget
              .movieDetail["watchNow"]["streamingSources"][index]
                  ["purchaseDetails"][i]["purchaseMobileAffiliateURL"]
              .isNotEmpty &&
          alwaysOpenInAppFlag) {
        printIfDebug("launch step 3");

        getMovieDetails(
            widget.movieDetail["watchNow"]["streamingSources"][index]
                ["purchaseDetails"][i]["purchaseMobileAffiliateURL"],
            !sourceKeys.containsKey(widget.movieDetail["watchNow"]
                ["streamingSources"][index]["sourceKey"]));
      } else if (widget.movieDetail["watchNow"]["streamingSources"][index]
                  ["purchaseDetails"][i]["purchaseRedirectURL"] !=
              null &&
          widget
              .movieDetail["watchNow"]["streamingSources"][index]
                  ["purchaseDetails"][i]["purchaseRedirectURL"]
              .isNotEmpty) {
        printIfDebug("launch step 4");

        printIfDebug(widget.movieDetail["watchNow"]["streamingSources"][index]
            ["sourceKey"]);
        printIfDebug(sourceKeys.toString());
        getMovieDetails(
            widget.movieDetail["watchNow"]["streamingSources"][index]
                ["purchaseDetails"][i]["purchaseRedirectURL"],
            !sourceKeys.containsKey(widget.movieDetail["watchNow"]
                ["streamingSources"][index]["sourceKey"]));
      }
    }
  }

  addCheckPermissionSP(key) {
    setState(() {
      checkPermissionSP.add(key);
    });
  }

  showAlertAppPresent(index, i) {
    if (checkPermissionSP.contains(sourceKeys[widget.movieDetail["watchNow"]
        ["streamingSources"][index]["sourceKey"]])) {
      checkIfAndroidAppIsInstalledFunc(index, i);
    } else {
      showDialog(
        context: context,

        barrierDismissible: false, // user must tap button for close dialog!
        builder: (context) {
          return AppCheckAlert(
            index: index,
            i: i,
            regularAction: regularAction,
            sourceKey: sourceKeys[widget.movieDetail["watchNow"]
                ["streamingSources"][index]["sourceKey"]],
            updatePackageList: updatePackageList,
            checkIfAndroidAppIsInstalled: checkIfAndroidAppIsInstalledFunc,
            checkPermissionSP: addCheckPermissionSP,
          );
        },
      );
    }
  }

  Future<bool> checkIfAndroidAppIsInstalledFunc(index, i) async {
    bool pInstalled = false;
    String pName = sourceKeys[widget.movieDetail["watchNow"]["streamingSources"]
        [index]["sourceKey"]];
    if (true) {
      try {
        // Application? app = await DeviceApps.getApp(pName);
        // DeviceApps.openApp(pName);
        // AppAvailability.launchApp(pName);
        // platform dependent code
        // printIfDebug('\n\npackage details: ' + app.toString());
        pInstalled = true;
      } catch (e) {
        regularAction(index, i);
        printIfDebug('\n\nexception caught in checking the package details');
        pInstalled = false;
      }
    } else {
      regularAction(index, i);
//flixjiniios watch implementation
      pInstalled = false;
    }
    printIfDebug('\n\nreturn bool value for the pinstalled: $pInstalled');
    return pInstalled;
  }

  Widget buildStreamingSourceCards(BuildContext context, int index) {
    return renderCard(index);
  }

  String getStreamingSourceNumber(
    String imgUrl,
  ) {
    imgUrl =
        imgUrl.replaceAll('https://c.kmpr.in/assets/streaming-sources/', '');
    return imgUrl;
  }

  regularAction(index, i) {
    String purchaseRedirectURLYes = widget.movieDetail["watchNow"]
            ["streamingSources"][index]["purchaseDetails"][i]
        ["purchaseRedirectURL"];
    String purchaseOriginalURLNo = widget.movieDetail["watchNow"]
            ["streamingSources"][index]["purchaseDetails"][i]
        ["purchaseOriginalURL"];

    if ((purchaseRedirectURLYes == purchaseOriginalURLNo) &&
        (purchaseRedirectURLYes.isNotEmpty)) {
      if (true) {
        widget.setStreamingSourceSelected(true);
      }
      getMovieDetails(purchaseRedirectURLYes, true);
    } else if (purchaseRedirectURLYes != null &&
        purchaseRedirectURLYes.isNotEmpty) {
      if (true) {
        widget.setStreamingSourceSelected(true);
      }
      getMovieDetails(purchaseRedirectURLYes, true);
    } else if (purchaseOriginalURLNo != null &&
        purchaseOriginalURLNo.isNotEmpty) {
      if (true) {
        widget.setStreamingSourceSelected(true);
      }
      getMovieDetails(purchaseOriginalURLNo, true);

      // fetchUserprofileToCheckIfUserAlreadySubscribedToSource(index, i);
    }
  }

  Future<void> _sendAnalyticsEvent(tagName, streamName, type) async {
    await analytics.logEvent(
      name: streamName
              .toString()
              .toLowerCase()
              .replaceAll(" ", "")
              .replaceAll("-", "") +
          '_watch',
      parameters: <String, dynamic>{
        'movie_to_stream': tagName,
        'stream_source': streamName,
        'type': type,
      },
    );
    // facebookAppEvents.logEvent(
    //   name: streamName
    //           .toString()
    //           .toLowerCase()
    //           .replaceAll(" ", "")
    //           .replaceAll("-", "") +
    //       '_watch',
    //   parameters: {
    //     'movie_to_stream': tagName,
    //     'stream_source': streamName,
    //     'type': type,
    //   },
    //   valueToSum: 1,
    // );
    Map<String, String> eventData = {
      'movie_to_stream': tagName.toString(),
      'stream_source': streamName.toString(),
    };
    // await platform.invokeMethod('logSubscribeEvent', eventData);
  }

  Future<void> _sendAnalyticsEventDetail(tagName, streamName) async {
    await analytics.logEvent(
      name: streamName + '_detail',
      parameters: <String, dynamic>{
        'movie_to_stream': tagName,
      },
    );
    // facebookAppEvents.logEvent(
    //   name: streamName + '_detail',
    //   parameters: {
    //     'movie_to_stream': tagName,
    //   },
    //   valueToSum: 1,
    // );
  }

  Future<void> _sendAnalyticsDetail(tagName) async {
    await analytics.logEvent(
      name: 'movie_detail',
      parameters: <String, dynamic>{
        'movie_to_stream': tagName,
      },
    );
  }

  Widget renderCard(int index) {
    String photo = widget.movieDetail["watchNow"]["streamingSources"][index]
        ["streamingSourcesPhotos"];
    // printIfDebug('\n\nphoto url of the play card: $photo');
    var sourceImage;
    int purchaseOptionsLength = widget
        .movieDetail["watchNow"]["streamingSources"][index]["purchaseDetails"]
        .length;
    var purchaseOptions = <Widget>[];
    var purchaseOption;
    if (photo.isEmpty) {
      photo = "assests/box_office.png";
      sourceImage = new Image.asset(
        photo,
        fit: BoxFit.contain,
      );
    } else {
      // printIfDebug("photo:" + photo);
      sourceImage = Image(
        image: NetworkImage(
          photo,
          // fit: BoxFit.contain,
        ),
      );
      // sourceImage = Image.asset(
      //   'assests/streaming_sources/' +
      //       getStreamingSourceNumber(
      //         photo,
      //       ),
      //   height: 25,
      //   width: 25,
      //   // fit: BoxFit.fill,

      //   // fit: BoxFit.fill,
      //   // gaplessPlayback: true,
      // );
    }
    for (int i = 0; i < purchaseOptionsLength; i++) {
      // printIfDebug("Streaming Card Details");
      // printIfDebug(widget.movieDetail["watchNow"]["streamingSources"][index]
      // ["purchaseDetails"][i]["purchasePlan"]);
      String packageDetails = "";
      if (purchaseOptionsLength == 1) {
        packageDetails += nullChecker(index, i, "purchasePrice") + "\n";
        packageDetails += nullChecker(index, i, "purchasePlan") + "\n";
        packageDetails += nullChecker(index, i, "purchaseOffer");
        if (widget.movieDetail["watchNow"]["streamingSources"][index]
                ["purchaseDetails"][i]["purchasePrice"] ==
            null) {
          purchaseOption = Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                    // padding: EdgeInsets.only(
                    //   left: 5,
                    //   right: 5,
                    // ),
                    child: Column(
                      children: <Widget>[
                        widget.movieDetail["watchNow"]["streamingSources"]
                                            [index]["purchaseDetails"][i]
                                        ["purchasePlan"] !=
                                    null &&
                                widget.movieDetail["watchNow"]
                                            ["streamingSources"][index]
                                            ["purchaseDetails"][i]
                                            ["purchasePlan"]
                                        .toString()
                                        .length >
                                    1
                            ? Text(
                                "₹ " +
                                    widget.movieDetail["watchNow"]
                                            ["streamingSources"][index]
                                        ["purchaseDetails"][i]["purchasePlan"],
                                style: TextStyle(
                                  color: Constants.appColorFont,
                                  fontSize: 12,
                                ),
                                textAlign: TextAlign.center,
                              )
                            : Container(),
                        widget.movieDetail["watchNow"]["streamingSources"]
                                        [index]["purchaseDetails"][i]
                                    ["purchaseOffer"] !=
                                null
                            ? Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  widget.movieDetail["watchNow"]
                                                      ["streamingSources"]
                                                  [index]["purchaseDetails"][i]
                                              ["purchaseOffer"] !=
                                          null
                                      ? widget.movieDetail["watchNow"]
                                                  ["streamingSources"][index]
                                              ["purchaseDetails"][i]
                                          ["purchaseOffer"]
                                      : "",
                                  style: TextStyle(
                                    color: Constants.appColorFont,
                                    fontSize: 12,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
                // Spacer(),
                Expanded(
                  flex: 2,
                  child: Container(
                    // padding: EdgeInsets.only(
                    //   left: 20,
                    // ),
                    child: Center(
                      child: new FlatButton(
                        color: Constants.appColorL2,
                        textColor: Constants.appColorLogo,
                        child: new Text(
                          "WATCH",
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(fontWeight: FontWeight.bold),
                          maxLines: 1,
                          softWrap: true,
                        ),
                        onPressed: () {
                          _sendAnalyticsEvent(
                              widget.movieDetail["movieName"].toString(),
                              widget.movieDetail["watchNow"]["streamingSources"]
                                      [index]["sourceKey"]
                                  .toString(),
                              widget.movieDetail["watchNow"]["streamingSources"]
                                      [index]["purchaseDetails"][i]
                                      ["purchaseType"]
                                  .toString());
                          if (widget.authenticationToken.isNotEmpty || true) {
                            // if (sourceKeys.containsKey(
                            //     widget.movieDetail["watchNow"]
                            //         ["streamingSources"][index]["sourceKey"])) {
                            //   showAlertAppPresent(index, i);
                            // } else {
                            //   regularAction(index, i);
                            // }
                            onWatchClick(index, i);
                          } else {
                            sendUserToAuthenticationPage();
                          }
                        },
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(
                            color: Constants.appColorLogo,
                          ),
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          purchaseOption = Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                    // padding: EdgeInsets.only(
                    //   left: 5,
                    //   right: 5,
                    // ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          widget.movieDetail["watchNow"]["streamingSources"]
                                              [index]["purchaseDetails"][i]
                                          ["purchaseType"] !=
                                      null &&
                                  widget.movieDetail["watchNow"]
                                              ["streamingSources"][index]
                                              ["purchaseDetails"][i]
                                              ["purchaseType"]
                                          .toString()
                                          .length >
                                      1
                              ? widget.movieDetail["watchNow"]
                                      ["streamingSources"][index]
                                  ["purchaseDetails"][i]["purchaseType"]
                              : "",
                          style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        // Container(
                        //   padding: EdgeInsets.only(top: 5),
                        //   child: Text(
                        //     widget.movieDetail["watchNow"]["streamingSources"]
                        //                     [index]["purchaseDetails"][i]
                        //                 ["purchaseOffer"] !=
                        //             null
                        //         ? widget.movieDetail["watchNow"]
                        //                 ["streamingSources"][index]
                        //             ["purchaseDetails"][i]["purchaseOffer"]
                        //         : "",
                        //     style: TextStyle(
                        //       color: Constants.appColorFont,
                        //       fontSize: 12,
                        //     ),
                        //     textAlign: TextAlign.center,
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
                // Spacer(),
                Expanded(
                  flex: 2,
                  child: Container(
                    // padding: EdgeInsets.only(
                    //   left: 20,
                    // ),
                    child: Center(
                      child: new FlatButton(
                        color: Constants.appColorL2,
                        textColor: Constants.appColorLogo,
                        child: new Text(
                          widget.movieDetail["watchNow"]["streamingSources"]
                                              [index]["purchaseDetails"][i]
                                          ["purchasePrice"] !=
                                      null &&
                                  widget.movieDetail["watchNow"]
                                              ["streamingSources"][index]
                                              ["purchaseDetails"][i]
                                              ["purchasePrice"]
                                          .toString()
                                          .length >
                                      1
                              ? "₹ " +
                                  widget.movieDetail["watchNow"]
                                          ["streamingSources"][index]
                                      ["purchaseDetails"][i]["purchasePrice"]
                              : "",
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(fontWeight: FontWeight.bold),
                          maxLines: 1,
                          softWrap: true,
                        ),
                        onPressed: () {
                          _sendAnalyticsEvent(
                              widget.movieDetail["movieName"].toString(),
                              widget.movieDetail["watchNow"]["streamingSources"]
                                      [index]["sourceKey"]
                                  .toString(),
                              widget.movieDetail["watchNow"]["streamingSources"]
                                      [index]["purchaseDetails"][i]
                                      ["purchaseType"]
                                  .toString());
                          if (widget.authenticationToken.isNotEmpty || true) {
                            // if (sourceKeys.containsKey(
                            //     widget.movieDetail["watchNow"]
                            //         ["streamingSources"][index]["sourceKey"])) {
                            //   showAlertAppPresent(index, i);
                            // } else {
                            //   regularAction(index, i);
                            // }
                            onWatchClick(index, i);
                          } else {
                            sendUserToAuthenticationPage();
                          }
                        },
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(
                            color: Constants.appColorLogo,
                          ),
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
      } else {
        String purchase = widget.movieDetail["watchNow"]["streamingSources"]
                    [index]["purchaseDetails"][i]["purchasePrice"]
                .toString()
                .isNotEmpty
            ? "₹ " +
                widget.movieDetail["watchNow"]["streamingSources"][index]
                    ["purchaseDetails"][i]["purchasePrice"]
            : "";
        purchase = widget.movieDetail["watchNow"]["streamingSources"][index]
                    ["purchaseDetails"][i]["purchaseType"]
                .toString()
                .contains("WATCH (FREE)")
            ? "FREE"
            : purchase;
        // printIfDebug("CHeck the price");
        // printIfDebug(widget.movieDetail["watchNow"]["streamingSources"][index]
        //     ["purchaseDetails"][i]["purchasePrice"]);
        packageDetails += nullChecker(index, i, "purchaseType");
        purchaseOption = new Container(
          padding: const EdgeInsets.only(left: 10.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              new Expanded(
                child: new Container(
                  padding: const EdgeInsets.all(0.0),
                  child: new Center(
                    child: new Text(
                      packageDetails.isNotEmpty ? packageDetails : "",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Constants.appColorFont,
                        fontSize: 12.0,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ),
                flex: 2,
              ),
              new Expanded(
                child: new Container(
                  padding: const EdgeInsets.only(
                    left: 20, // right: 10.0,
                  ),
                  child: new Center(
                    child: new FlatButton(
                      color: Constants.appColorL2,
                      textColor: Constants.appColorLogo,
                      child: new Text(
                        purchase,
                        overflow: TextOverflow.ellipsis,
                        style: new TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 12.0),
                        maxLines: 1,
                      ),
                      onPressed: () {
                        _sendAnalyticsEvent(
                            widget.movieDetail["movieName"].toString(),
                            widget.movieDetail["watchNow"]["streamingSources"]
                                    [index]["sourceKey"]
                                .toString(),
                            widget.movieDetail["watchNow"]["streamingSources"]
                                    [index]["purchaseDetails"][i]
                                    ["purchaseType"]
                                .toString());
                        if (widget.authenticationToken.isNotEmpty || true) {
                          // if (sourceKeys.containsKey(
                          //     widget.movieDetail["watchNow"]["streamingSources"]
                          //         [index]["sourceKey"])) {
                          //   showAlertAppPresent(index, i);
                          // } else {
                          //   regularAction(index, i);
                          // }
                          onWatchClick(index, i);
                        } else {
                          sendUserToAuthenticationPage();
                        }
                      },
                      shape: new RoundedRectangleBorder(
                        side: BorderSide(
                          color: Constants.appColorLogo,
                        ),
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                ),
                flex: 2,
              ),
            ],
          ),
        );
      }
      purchaseOptions.add(purchaseOption);
    }
    return
        // new Container(
        // padding: const EdgeInsets.only(right: 5.0),
        // child: new Container(
        //   width: 260.0,
        //   height: 160.0,
        // child:
        //   Card(
        // elevation: 6.0,
        // color: Constants.appColorFont,
        // shape: RoundedRectangleBorder(
        //   borderRadius: new BorderRadius.circular(8.0),
        // ),
        // child: new ClipRRect(
        //   borderRadius: new BorderRadius.circular(8.0),
        // child: new Container(
        //   color: Constants.appColorFont,
        // child: new Center(
        // child: new ClipRRect(
        //   borderRadius: new BorderRadius.circular(8.0),
        new Container(
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.all(
          new Radius.circular(5.0),
        ),
        color: Constants.appColorL2,
      ),
      padding: EdgeInsets.only(
        left: 5,
        right: 10,
      ),
      height: 70.0,
      width: 330.0,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new Expanded(
            child: Center(
              child: Container(
                height: 75,
                width: 65,
                decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Constants.appColorL2,
                  borderRadius: new BorderRadius.all(
                    new Radius.circular(10.0),
                  ),
                  border: Border.all(
                    color: Constants.appColorDivider,
                    width: 1.0,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      // color: Colors,
                      height: 40,
                      width: 40,
                      child: GestureDetector(
                        child: sourceImage,
                        onTap: () {
                          String appliedFilterUrl =
                              "https://api.flixjini.com/entertainment/movies.json?" +
                                  "&contenttypelist=movie,series&sourceslist=" +
                                  widget.movieDetail["watchNow"]
                                      ["streamingSources"][index]["sourceKey"];
                          // printIfDebug("filter url" + appliedFilterUrl);
                          Navigator.of(context).push(
                            new MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    MovieFilterPage(
                                      appliedFilterUrl: appliedFilterUrl,
                                      // visibleFilters: visibleFiltersMap,
                                      // add these 2 params
                                      // visibilityStatus: ,
                                      inTheaterKey: false,
                                    )),
                          );
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: 10,
                      ),
                      child: Text(
                        widget.movieDetail["watchNow"]["streamingSources"]
                                    [index]["streamingSourcesLanguage"]
                                .toString()
                                .isNotEmpty
                            ? widget.movieDetail["watchNow"]["streamingSources"]
                                    [index]["streamingSourcesLanguage"]
                                .toString()
                                .toUpperCase()
                            : "",
                        textAlign: TextAlign.right,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Constants.appColorFont,
                          fontSize: 8.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
          // new Expanded(
          //   child: new Container(
          //     padding: const EdgeInsets.all(10.0),
          //     child: new Text(
          //       widget.movieDetail["watchNow"]["streamingSources"][index]
          //                   ["streamingSourcesLanguage"]
          //               .toString()
          //               .isNotEmpty
          //           ? widget.movieDetail["watchNow"]["streamingSources"][index]
          //               ["streamingSourcesLanguage"]
          //           : "",
          //       textAlign: TextAlign.right,
          //       style: new TextStyle(
          //         fontWeight: FontWeight.bold,
          //         color: Constants.appColorFont,
          //         fontSize: 10.0,
          //       ),
          //       maxLines: 2,
          //       overflow: TextOverflow.ellipsis,
          //     ),
          //   ),
          //   flex: 1,
          // ),
          // Spacer(),
          new Expanded(
            child: new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: purchaseOptions,
              ),
            ),
            flex: 3,
          ),
        ],
      ),
      // ),
      // ),
      // ),
      // ),
      // ),
      // ),
    );
  }

  Widget streamingSourcesCardViewRow() {
    var sourceCards = <Widget>[];
    for (int index = 0;
        index < widget.movieDetail["watchNow"]["streamingSources"].length;
        index++) {
      printIfDebug("Start Source");
      var sourceCard = renderCard(index);
      sourceCards.add(sourceCard);
    }
    return new Container(
      padding: const EdgeInsets.all(1.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: sourceCards,
      ),
    );
  }

  Widget checkNoOfCardsNeeded() {
    // printIfDebug("Number of Rating Cards");
    // printIfDebug(widget.movieDetail["watchNow"]["streamingSources"].length);
    if (widget.movieDetail["watchNow"]["streamingSources"].length < 2 &&
        false) {
      return streamingSourcesCardViewRow();
    } else {
      return streamingSourcesCardView();
    }
  }

  Widget streamingSourcesCardView() {
    // printIfDebug("Length of Streaming Sources");
    // printIfDebug(widget.movieDetail["watchNow"]["streamingSources"].length);
    // double streamHeight =
    //     widget.movieDetail["watchNow"]["streamingSources"].length * 90.0;
    // return new SizedBox.fromSize(
    //   size: const Size.fromHeight(280.0),
    //   child: new ListView.builder(
    //     itemCount: widget.movieDetail["watchNow"]["streamingSources"].length,
    //     scrollDirection: Axis.horizontal,
    //     padding: const EdgeInsets.all(10.0),
    //     itemBuilder: buildStreamingSourceCards,
    //   ),
    // );

    return GridView.builder(
      addAutomaticKeepAlives: false,
      shrinkWrap: true,
      physics: new NeverScrollableScrollPhysics(),
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 1,
        childAspectRatio: 3.5, // 1.75,
        mainAxisSpacing: 20, // 2
        crossAxisSpacing: 2, // 2
      ),
      itemCount: widget.movieDetail["watchNow"]["streamingSources"].length,
      itemBuilder: buildStreamingSourceCards,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: new Column(
        key: widget.watchNowKey,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // new MovieDetailSubheading(
          //   subheading: "Watch Now",
          // ),
          Container(
            margin: EdgeInsets.only(
                // bottom: 14,
                ),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // insertDividerVertical(),
                Expanded(
                  // flex: 6,
                  child: Container(
                    margin: EdgeInsets.only(
                        // let: 10,
                        ),
                    padding: EdgeInsets.only(
                      top: 5,
                      left: 8,
                    ),
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(
                          width: 4.0,
                          color: Constants.appColorLogo,
                        ),
                      ),
                    ),
                    child: Text(
                      "WATCH NOW",
                      textAlign: TextAlign.left,
                      style: new TextStyle(
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: 19.0,
                      ),
                      maxLines: 2,
                    ),
                  ),
                ),
              ],
            ),
          ),
          // new MovieDetailUnderline(),
          new Container(
            padding: const EdgeInsets.all(0.0),
            child: checkNoOfCardsNeeded(),
          ),
          new Container(
            padding: const EdgeInsets.only(
              top: 20.0,
              bottom: 20,
            ),
            child: new Text(
              widget.movieDetail["watchNow"]["disclaimer"] != null &&
                      widget.movieDetail["watchNow"]["disclaimer"]
                          .toString()
                          .isNotEmpty
                  ? widget.movieDetail["watchNow"]["disclaimer"]
                  : "",
              textAlign: TextAlign.left,
              style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Constants.appColorFont,
                  fontSize: 10.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 8,
            ),
          ),
        ],
      ),
    );
  }
}
