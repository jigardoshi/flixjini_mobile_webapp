import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailRating extends StatefulWidget {
  MovieDetailRating({
    Key? key,
    this.movieDetail,
    this.movieDataFromCall,
    this.cardFor,
    this.showOriginalPoster,
    this.fromGenie,
  }) : super(key: key);

  final movieDetail, showOriginalPoster, cardFor, movieDataFromCall, fromGenie;

  @override
  _MovieDetailRatingState createState() => new _MovieDetailRatingState();
}

class _MovieDetailRatingState extends State<MovieDetailRating> {
  List validRatings = [];
  var scoreList = [
    {
      "name": "Jini Score",
      "image": "assests/new_emojis/flixjini_score.png",
      "score": 0,
      "url": "k_score",
    },
    {
      "name": "Critics Score",
      "image": "",
      "score": 0,
      "url": "critic_score",
    },
    {
      "name": "Rating",
      "image": "assests/new_emojis/score_section_like.png",
      "score": 0,
      "url": "imdb_rating",
    },
    {
      "name": "Award \nWinner",
      "image": "assests/new_emojis/award_logo.png",
      "score": 0,
      "url": "award_winning",
    },
    {
      "name": "Block\nBuster",
      "image": "assests/blockbuster.png",
      "score": 0,
      "url": "blockbuster",
    },
    {
      "name": "Rotten Tomatoes",
      "image": "assests/rt_score.png",
      "score": 0,
      "url": "rt_score",
    },
  ];

  @override
  void initState() {
    super.initState();
    if (checkCallData()) {
      setScore();
    }
  }

  bool checkCallData() {
    // print("genie ratinngs");
    // print(widget.movieDataFromCall["scores"]);
    return (widget.movieDataFromCall != null &&
        widget.movieDataFromCall.isNotEmpty &&
        widget.movieDataFromCall["scores"] != null &&
        widget.movieDataFromCall["scores"] != null);
  }

  Widget callGetPhoto(
    String imageUrl,
    String score,
  ) {
    return Container(
      margin: EdgeInsets.only(
        left: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
        top: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
        bottom: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
      ),
      child: !(imageUrl.contains('https://c.kmpr.in/assets/movies/') &&
              imageUrl.toString().toLowerCase().contains('imdb'))
          ? Image.network(imageUrl)
          : Image.asset(
              getScorePhoto(
                imageUrl,
                score,
              ),
            ),
    );
  }

  Widget callGetPhotoLocal(
    String imageUrl,
  ) {
    return Container(
      margin: EdgeInsets.only(
        left: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
        top: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
        bottom: widget.showOriginalPoster && widget.cardFor == 0 ? 5 : 0,
      ),
      child: Image.asset(
        imageUrl,
      ),
    );
  }

  double convertStringToDouble(
    String scoreValue,
  ) {
    scoreValue = scoreValue.replaceAll('%', '');
    scoreValue = scoreValue.replaceAll('/10', '');
    return double.parse(scoreValue);
  }

  String getScorePhoto(
    String imageUrl,
    String score,
  ) {
    late double scoreValue;
    if (!imageUrl.contains('boxoffice') && !imageUrl.contains('a_won')) {
      scoreValue = convertStringToDouble(score);
    }

    if (imageUrl.contains('https://c.kmpr.in/assets/') &&
        imageUrl.contains('c_')) {
      imageUrl = getCriticsScoreImage(
        scoreValue,
      );
    } else if (imageUrl.contains('https://c.kmpr.in/assets/movies/') &&
        imageUrl.contains('a_won')) {
      imageUrl = getAwardLogoLocation();
    } else if (imageUrl.contains('https://c.kmpr.in/assets/movies/') &&
        imageUrl.contains('boxoffice')) {
      imageUrl = getBoxOfficeIconLocation();
    } else if (imageUrl.contains('https://c.kmpr.in/assets/movies/') &&
        imageUrl.contains('flixjiniscore')) {
      imageUrl = getFlixjiniScoreImage();
    } else if (imageUrl.contains('https://c.kmpr.in/assets/movies/') &&
        imageUrl.toString().toLowerCase().contains('imdb')) {
      imageUrl = getImdbScoreImage(
        scoreValue,
      );
    }
    return imageUrl;
  }

  String getAwardLogoLocation() {
    return 'assests/new_emojis/award_logo.png';
  }

  String getBoxOfficeIconLocation() {
    return 'assests/new_emojis/box_office_icon.png';
  }

  String getCriticsScoreImage(
    double scoreValue,
  ) {
    String imageUrl;
    if (scoreValue < 40.0) {
      imageUrl = 'assests/new_emojis/sad_smiley.png';
    } else if (scoreValue < 65.0) {
      imageUrl = 'assests/new_emojis/neutral_smiley.png';
    } else {
      imageUrl = 'assests/new_emojis/happy_smiley.png';
    }
    return imageUrl;
  }

  String getImdbScoreImage(
    double scoreValue,
  ) {
    String imageUrl;
    if (scoreValue < 4.0) {
      imageUrl = 'assests/new_emojis/score_section_dislike.png';
    } else if (scoreValue < 6.5) {
      imageUrl = 'assests/new_emojis/score_section_neutral_like.png';
    } else {
      imageUrl = 'assests/new_emojis/score_section_like.png';
    }
    return imageUrl;
  }

  String getFlixjiniScoreImage() {
    return 'assests/new_emojis/flixjini_score.png';
  }

  Widget getContainerForScoreName(
    String scoreName,
  ) {
    return Container(
      margin: EdgeInsets.only(
        top: 5,
        left: 3,
      ),
      child: Text(
        // scoreName.toString().toUpperCase(),
        !scoreName.toLowerCase().contains('award')
            ? scoreName.toString().toUpperCase()
            : '', // 'AWARD\nWINNER',
        style: new TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 6.0, // 12.0,
          color: Constants.appColorFont,
        ),
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        softWrap: true,
        maxLines: 2,
      ),
    );
  }

  Widget gridRatingCard() {
    var ratingCardList = <Widget>[];

    int noOfScores = widget.movieDetail["ratings"]["scorer"].length;
    if (widget.showOriginalPoster && false) {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(50.0),
        child: new ListView.builder(
          itemCount: noOfScores,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.only(top: 10.0),
          itemBuilder: (BuildContext context, int i) {
            return getOneScoreCard(i);
          },
        ),
      );
    } else {
      for (int j = 0; j < noOfScores; j++) {
        ratingCardList.add(
          getOneScoreCard(j),
        );
      }
      return Wrap(
        alignment: WrapAlignment.center,
        runSpacing: 10,
        children: ratingCardList,
      );
    }
  }

  Widget getOneScoreCard(
    int index,
  ) {
    String photo =
        widget.movieDetail["ratings"]["scorer"][index]["scorePhotos"];
    String scoreValue;
    String scoreName = widget.movieDetail["ratings"]["scorer"][index]['name'];
    if (scoreName == "Score") {
      scoreName = "Jini Score";
    }
    if (scoreName == "Rating") {
      scoreName = "IMDB Rating";
    }

    if (widget.movieDetail["ratings"]["scorer"][index]["score"] == "true") {
      scoreValue = "Award\nWinner";
    } else {
      scoreValue = widget.movieDetail["ratings"]["scorer"][index]["score"];
    }
    scoreValue = scoreValue.replaceFirst("Lakhs", "L");
    scoreValue = scoreValue.replaceFirst("Crores", "Cr");
    scoreValue = scoreValue.replaceFirst("Million", "M");
    scoreValue = scoreValue.replaceFirst("Billion", "B");
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.all(
          new Radius.circular(3.0),
        ),
        color: widget.showOriginalPoster
            ? Constants.appColorL2
            : Colors.transparent,
      ),
      width: screenWidth / 3.9,
      // height: 50,
      margin: EdgeInsets.only(
        left: widget.showOriginalPoster ? 10 : 0,
        right: widget.showOriginalPoster ? 10 : 0,
      ),
      child: Row(
        children: <Widget>[
          widget.showOriginalPoster
              ? Expanded(
                  child: Container(
                    child: callGetPhoto(
                      photo,
                      scoreValue,
                    ),
                  ),
                  flex: 1,
                )
              : Container(
                  margin: EdgeInsets.only(
                    top: 5,
                  ),
                  height: 30,
                  width: 30,
                  child: callGetPhoto(
                    photo,
                    scoreValue,
                  ),
                ),
          widget.showOriginalPoster
              ? Expanded(
                  child: Container(
                    // margin: EdgeInsets.only(
                    //   top: 5,
                    // ),
                    padding: EdgeInsets.only(
                      // left: 5,
                      top: 3,
                      bottom: 5,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        !scoreName.toLowerCase().contains('award')
                            ? getContainerForScoreName(
                                scoreName,
                              )
                            : Container(),
                        Container(
                          margin: EdgeInsets.only(
                            top: 5,
                            left: 5,
                          ),
                          child: Text(
                            scoreValue,
                            style: TextStyle(
                              color: Constants.appColorFont,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flex: 2,
                )
              : Container(
                  padding: EdgeInsets.only(
                    left: 5,
                    top: 3,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      !scoreName.toLowerCase().contains('award')
                          ? getContainerForScoreName(
                              scoreName,
                            )
                          : Container(),
                      Container(
                        margin: EdgeInsets.only(
                          top: 5,
                        ),
                        child: Text(
                          scoreValue,
                          style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  genieRating() {
    double screenWidth = MediaQuery.of(context).size.width;
    double leftPad = widget.showOriginalPoster ? 0.0 : 25.0;
    if (validRatings.length == 1) {
      leftPad = widget.showOriginalPoster
          ? (screenWidth / 3) - 10.0
          : (screenWidth / 3) + 15;
    } else if (validRatings.length == 2) {
      leftPad = widget.showOriginalPoster
          ? (screenWidth / 6) - 10.0
          : (screenWidth / 6) + 15;
    }
    return Container(
      margin: EdgeInsets.only(
        left: leftPad,
      ),
      child: new SizedBox.fromSize(
        size: const Size.fromHeight(50.0),
        child: new ListView.builder(
          itemCount: validRatings.length,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.only(top: 10.0),
          itemBuilder: (BuildContext context, int i) {
            return genieEachRating(i);
          },
        ),
      ),
    );
  }

  genieEachRating(int i) {
    // List<Widget> ratingList = new List();

    // for (int i = 0; i < (validRatings.length); i++) {
    if (widget.movieDataFromCall["scores"][scoreList[i]["url"]] != null) {
      return Container(
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: new BorderRadius.all(
            new Radius.circular(3.0),
          ),
          color: widget.showOriginalPoster
              ? Constants.appColorL2
              : Colors.transparent,
        ),
        width: widget.showOriginalPoster ? 100 : 90,
        // height: 50,
        // padding: EdgeInsets.only(top: 5, bottom: 5),
        margin: EdgeInsets.only(
          left: widget.showOriginalPoster ? 10 : 0,
          right: widget.showOriginalPoster ? 10 : 0,
        ),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 30,
                width: 30,
                child: callGetPhotoLocal(
                  validRatings[i]["image"],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.only(
                    // left: 5,
                    // top: 3,
                    ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 2),
                      child: Text(
                        validRatings[i]["name"],
                        style: TextStyle(
                          color: Constants.appColorFont,
                          fontSize: 6.0, // 12.0,
                          fontWeight: FontWeight.w800,
                          letterSpacing: 0.4,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        validRatings[i]["score"].toString().length > 0
                            ? Container(
                                margin: EdgeInsets.only(
                                  top: 2,
                                ),
                                child: Text(
                                  validRatings[i]["score"].toString(),
                                  style: TextStyle(
                                    color: Constants.appColorFont,
                                    fontSize: 14,
                                  ),
                                ),
                              )
                            : Container(),
                        validRatings[i]["name"] == "Rating"
                            ? Container(
                                margin: EdgeInsets.only(
                                  bottom: 1,
                                  left: 1,
                                ),
                                child: Text(
                                  "/10",
                                  style: TextStyle(
                                    color: Constants.appColorFont,
                                    fontSize: 14,
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
      // ratingList.add(ratingBox);
      // }
    } else {
      return Container();
    }
    // return Container(
    //   margin: EdgeInsets.only(
    //     top: 15,
    //     bottom: 10,
    //     left: 10,
    //     right: 10,
    //   ),
    //   // child: Wrap(
    //   //   direction: Axis.horizontal,
    //   //   alignment: WrapAlignment.start,
    //   //   runAlignment: WrapAlignment.start,
    //   //   crossAxisAlignment: WrapCrossAlignment.center,
    //   //   verticalDirection: VerticalDirection.up,
    //   //   children: ratingList,
    //   // )
    //   child: SingleChildScrollView(
    //     scrollDirection: Axis.horizontal,
    //     child: Row(
    //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //       crossAxisAlignment: CrossAxisAlignment.center,
    //       children: ratingList,
    //     ),
    //   ),
    // );
  }

  Widget shareCardRating() {
    int noOfScores = widget.movieDetail["ratings"]["scorer"].length > 4
        ? 4
        : widget.movieDetail["ratings"]["scorer"].length;
    var ratingCardList = <Widget>[];

    // double screenWidth = MediaQuery.of(context).size.width;
    // double marginSpace;

    // switch (noOfScores) {
    //   case 1:
    //     marginSpace = screenWidth / 3;
    //     break;
    //   case 2:
    //     marginSpace = screenWidth / 4;

    //     break;
    //   case 3:
    //     marginSpace = screenWidth / 8;

    //     break;
    //   case 4:
    //     marginSpace = screenWidth / 24;

    //     break;
    //   default:
    //     marginSpace = screenWidth / 24;

    //     break;
    // }
    for (int j = 0; j < noOfScores; j++) {
      ratingCardList.add(
        getOneShareScoreCard(j),
      );
    }
    return Wrap(
      alignment: WrapAlignment.center,
      runSpacing: 10,
      children: ratingCardList,
    );

    // return Container(
    //   margin: EdgeInsets.only(left: marginSpace),
    //   child: new SizedBox.fromSize(
    //     size: const Size.fromHeight(60.0),
    //     child: new ListView.builder(
    //       itemCount: noOfScores <= 4 ? noOfScores : 4,
    //       scrollDirection: Axis.horizontal,
    //       itemBuilder: (BuildContext context, int i) {
    //         return getOneShareScoreCard(i);
    //       },
    //     ),
    //   ),
    // );
  }

  setScore() {
    int length = widget.movieDataFromCall["scores"] != null
        ? widget.movieDataFromCall["scores"].length
        : 0;

    for (int i = 0; i < length; i++) {
      if (widget.movieDataFromCall["scores"][scoreList[i]["url"]] != null &&
          widget.movieDataFromCall["scores"][scoreList[i]["url"]] > 0) {
        if (scoreList[i]["url"] == "k_score") {
          scoreList[i]["score"] =
              (widget.movieDataFromCall["scores"][scoreList[i]["url"]] / 10)
                      .round()
                      .toString() +
                  "%";
        } else if (scoreList[i]["url"] == "critic_score") {
          scoreList[i]["score"] = (widget.movieDataFromCall["scores"]
                      [scoreList[i]["url"]])
                  .round()
                  .toString() +
              "%";
        } else if (scoreList[i]["url"] == "imdb_rating") {
          scoreList[i]["score"] = (widget.movieDataFromCall["scores"]
                  [scoreList[i]["url"]])
              .toString();
        } else if (scoreList[i]["url"] == "blockbuster" ||
            scoreList[i]["url"] == "award_winning") {
          scoreList[i]["score"] = "";
        } else {
          scoreList[i]["score"] =
              widget.movieDataFromCall["scores"][scoreList[i]["url"]];
        }

        if (scoreList[i]["url"] == "critic_score") {
          if (widget.movieDataFromCall["scores"][scoreList[i]["url"]] > 75) {
            scoreList[i]["image"] = "assests/new_emojis/happy_smiley.png";
          } else if (widget.movieDataFromCall["scores"][scoreList[i]["url"]] <
              40) {
            scoreList[i]["image"] = "assests/new_emojis/sad_smiley.png";
          } else {
            scoreList[i]["image"] = "assests/new_emojis/neutral_smiley.png";
          }
        }
        validRatings.add(scoreList[i]);
      }
    }
  }

  Widget getOneShareScoreCard(
    int index,
  ) {
    String photo =
        widget.movieDetail["ratings"]["scorer"][index]["scorePhotos"];
    String scoreValue;
    String scoreName = widget.movieDetail["ratings"]["scorer"][index]['name'];
    if (scoreName == "Score") {
      scoreName = "Jini Score";
    }
    if (scoreName == "Rating") {
      scoreName = "Rating";
    }

    if (widget.movieDetail["ratings"]["scorer"][index]["score"] == "true") {
      scoreValue = "Award\nWinner";
    } else {
      scoreValue = widget.movieDetail["ratings"]["scorer"][index]["score"];
    }
    scoreValue = scoreValue.replaceFirst("Lakhs", "L");
    scoreValue = scoreValue.replaceFirst("Crores", "Cr");
    scoreValue = scoreValue.replaceFirst("Million", "M");
    scoreValue = scoreValue.replaceFirst("Billion", "B");
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: (screenWidth - 60) / 4,
      // height: 50,
      color: Colors.transparent,
      margin: EdgeInsets.only(
        left: 5,
        right: 5,
      ),
      child: widget.cardFor == 2
          ? Row(
              children: <Widget>[
                Container(
                  height: 25,
                  width: 25,
                  child: callGetPhoto(
                    photo,
                    scoreValue,
                  ),
                ),
                Column(
                  children: <Widget>[
                    !scoreName.toLowerCase().contains('award')
                        ? getContainerForScoreName(
                            scoreName,
                          )
                        : Container(),
                    Container(
                      margin: EdgeInsets.only(
                        top: 5,
                        left: 5,
                      ),
                      child: Text(
                        scoreValue,
                        style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            )
          : Column(
              children: <Widget>[
                Container(
                  height: 25,
                  width: 25,
                  child: callGetPhoto(
                    photo,
                    scoreValue,
                  ),
                ),
                Column(
                  children: <Widget>[
                    !scoreName.toLowerCase().contains('award')
                        ? getContainerForScoreName(
                            scoreName,
                          )
                        : Container(),
                    Container(
                      margin: EdgeInsets.only(
                        top: 5,
                        left: 5,
                      ),
                      child: Text(
                        scoreValue,
                        style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            ),
    );
  }

  Widget checkNoOfCardsNeeded() {
    printIfDebug("Number of Rating Cards");
    printIfDebug(widget.movieDetail["ratings"]["scorer"].length);
    if (widget.cardFor == 0) {
      return gridRatingCard();
    } else {
      return shareCardRating();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    double leftPad = widget.showOriginalPoster ? 0.0 : 25.0;
    if (checkCallData() && widget.fromGenie != null && widget.fromGenie) {
      return genieRating();
    } else {
      try {
        if (widget.movieDetail["ratings"]["scorer"].length == 1) {
          leftPad = widget.showOriginalPoster
              ? (screenWidth / 3) - 10.0
              : (screenWidth / 3) + 15;
        } else if (widget.movieDetail["ratings"]["scorer"].length == 2) {
          leftPad = widget.showOriginalPoster
              ? (screenWidth / 6) - 10.0
              : (screenWidth / 6) + 15;
        }
        return new Container(
          margin: EdgeInsets.only(
            // top: 2,
            bottom: widget.cardFor == 2 ? 5 : 25,
            // left:
            //     widget.showOriginalPoster && widget.cardFor != 1 ? leftPad : 0,
          ),

          width: screenWidth - 10.0,
          // height: 100,
          child: Container(
            child: checkNoOfCardsNeeded(),
          ),
        );
      } catch (e) {
        printIfDebug(e);
      }
    }
    return Container(
      margin: EdgeInsets.only(
        // top: 2,
        bottom: 25,
      ),
      height: 130,
      width: screenWidth - 10.0,
    );
  }
}
