import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailTags extends StatefulWidget {
  MovieDetailTags({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailTagsState createState() => new _MovieDetailTagsState();
}

class _MovieDetailTagsState extends State<MovieDetailTags> {
  bool expandMoviePhotos = false;
  bool needMoreGalleryView = true;
  int condensedDisplaySize = 4;
  int sampleLength = 0;

  Widget buildPhotos(BuildContext context, int index) {
    if (index < sampleLength) {
      var tagLink = widget.movieDetail["tags"]["cards"][index]["tagLink"];
      var tagLine = widget.movieDetail["tags"]["cards"][index]["tagName"];
      return new Padding(
        padding: const EdgeInsets.all(0.0),
        child: new GestureDetector(
          onTap: () {
            printIfDebug("Image Id that has been clicked");
            printIfDebug(tagLink);
            String appliedFilterUrl = tagLink;
            printIfDebug("Go to Movie Detail Page");
            printIfDebug(appliedFilterUrl);
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new MovieFilterPage(
                      appliedFilterUrl: appliedFilterUrl,
                      inTheaterKey: false,
                    )));
          },
          child: new Card(
            // elevation: 6.0,
            color: Constants.appColorL2,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: new Container(
              width: 125.0,
              height: 30.0,
              padding: const EdgeInsets.all(2.0),
              child: new Center(
                child: new Text(
                  tagLine,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 12.0,
                    color: Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return new Padding(
        padding: const EdgeInsets.all(0.0),
        child: new GestureDetector(
          onTap: () {
            setState(() {
              expandMoviePhotos = expandMoviePhotos ? false : true;
            });
          },
          child: Container(
            margin: EdgeInsets.only(
              left: 25,
              right: 25,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset(
                    'assests/expand_icon.png',
                    height: 18,
                    width: 18,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.all(5.0),
                  child: new Text(
                    "More",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: 16.0),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget galleryView() {
    int gallaryTrackLength = widget.movieDetail["tags"]["cards"].length;
    if (gallaryTrackLength > condensedDisplaySize) {
      printIfDebug("Has greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      printIfDebug("Doesn't have greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        needMoreGalleryView = false;
        sampleLength = gallaryTrackLength;
      });
    }
    printIfDebug("Based on Number of Tags");
    printIfDebug(needMoreGalleryView);
    if (needMoreGalleryView) {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(80.0),
        child: new ListView.builder(
          itemCount: sampleLength + 1,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    } else {
      return new SizedBox.fromSize(
        size: const Size.fromHeight(80.0),
        child: new ListView.builder(
          itemCount: sampleLength,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: buildPhotos,
        ),
      );
    }
  }

  Widget expandedGalleryView() {
    int numberOfOptions = widget.movieDetail["tags"]["cards"].length + 1;
    int numberOfOptionsPerRow = 2;
    int numberOfRows = (numberOfOptions / numberOfOptionsPerRow).ceil();
    double gridHeight = 40.0 * numberOfRows;
    printIfDebug("from expanded gallery view method, grid height: $gridHeight");
    return new Container(
      padding: const EdgeInsets.all(4.0),
      color: Constants.appColorL1,
      child: new GridView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2.75,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
        itemCount: widget.movieDetail["tags"]["cards"].length + 1,
        itemBuilder: (BuildContext context, int i) {
          if (i < widget.movieDetail["tags"]["cards"].length) {
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  var tagLink =
                      widget.movieDetail["tags"]["cards"][i]["tagLink"];
                  printIfDebug("Image Id that has been clicked");
                  printIfDebug(tagLink);
                  String appliedFilterUrl = tagLink;
                  printIfDebug("Go to Movie Detail Page");
                  printIfDebug(appliedFilterUrl);
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new MovieFilterPage(
                            appliedFilterUrl: appliedFilterUrl,
                            inTheaterKey: false,
                          )));
                },
                child: new Card(
                  // elevation: 6.0,
                  color: Constants.appColorL2,
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  child: new Container(
                    width: 125.0,
                    height: 30.0,
                    padding: const EdgeInsets.all(2.0),
                    child: new Center(
                      child: new Text(
                        widget.movieDetail["tags"]["cards"][i]["tagName"],
                        style: new TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0,
                          color: Constants.appColorFont,
                        ),
                        textScaleFactor: 1.0,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                      ),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return new GridTile(
              child: new GestureDetector(
                onTap: () {
                  setState(() {
                    expandMoviePhotos = expandMoviePhotos ? false : true;
                  });
                },
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assests/collapse_icon.png",
                        height: 18,
                        width: 18,
                      ),
                      margin: EdgeInsets.only(
                        right: 5,
                      ),
                    ),
                    Text(
                      "Less",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: 16.0,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    if (expandMoviePhotos && needMoreGalleryView) {
      return new Container(
        color: Constants.appColorL1,
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Tags"),
                // new MovieDetailUnderline(),
                new Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
              ]),
        ),
      );
    } else {
      return new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new MovieDetailSubheading(subheading: "Tags"),
                // new MovieDetailUnderline(),
                new Container(
                  child: new Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: galleryView(),
                  ),
                ),
              ]),
        ),
      );
    }
  }
}
