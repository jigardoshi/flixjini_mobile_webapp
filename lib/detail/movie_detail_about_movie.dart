import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import 	'dart:convert';

class MovieDetailAboutMovie extends StatefulWidget {
  MovieDetailAboutMovie({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailAboutMovieState createState() =>
      new _MovieDetailAboutMovieState();
}

class _MovieDetailAboutMovieState extends State<MovieDetailAboutMovie> {
  bool expandStory = false;

  @override
  void initState() {
    super.initState();
  }

  Widget toggleStory() {
    var unescape = new HtmlUnescape();
    String story;
    if (expandStory) {
      story = unescape.convert(widget.movieDetail["movieIntro"].join('\n\n'));
      return new Text(
        story,
        textAlign: TextAlign.left,
        softWrap: true,
        style: new TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 13.0,
            color: Constants.appColorFont,
            height: 1),
      );
    } else {
      story = unescape.convert(widget.movieDetail["movieIntro"][0]);
      return new Text(
        story,
        textAlign: TextAlign.left,
        softWrap: true,
        style: new TextStyle(
            fontWeight: FontWeight.normal,
            fontSize: 13.0,
            color: Constants.appColorFont,
            height: 1.3 //You can set your custom height here

            ),
      );
    }
  }

  Widget toggleStoryOption() {
    if (expandStory) {
      return new Padding(
        padding: const EdgeInsets.only(
          top: 2.0,
          left: 5.0,
          right: 5.0,
          bottom: 10.0,
        ),
        child:
        
            Container(
          height: 25,
          width: 25,
          margin: EdgeInsets.only(
            top: 10,
          ),
          child: Image.asset('assests/close_icon.png'),
        ),
      );
    } else {
      return new Padding(
        padding: const EdgeInsets.only(top: 2.0),
        child:
        
            Container(
          child: Icon(
            Icons.arrow_drop_down,
            size: 50,
            color: Constants.appColorLogo,
          ),
        ),
      );
    }
  }

  needToggleOption() {
    if (widget.movieDetail["movieIntro"].length > 1) {
      return new GestureDetector(
        onTap: () {
          setState(() {
            expandStory = expandStory ? false : true;
          });
        },
        child: toggleStoryOption(),
      );
    } else {
      return new Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new MovieDetailSubheading(subheading: "About"),
          // new MovieDetailUnderline(),
          new Padding(
            padding: const EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: 10,
            ),
            child: toggleStory(),
          ),
          // needToggleOption()
        ],
      ),
    );
  }
}
