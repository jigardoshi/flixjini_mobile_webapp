import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailHeading extends StatefulWidget {
  MovieDetailHeading({
    Key? key,
    this.movieName,
    this.showOriginalPosters,
  }) : super(key: key);

  final movieName, showOriginalPosters;

  @override
  _MovieDetailHeadingState createState() => new _MovieDetailHeadingState();
}

class _MovieDetailHeadingState extends State<MovieDetailHeading> {
  Widget displayHeading() {
    return Text(
      widget.movieName != null ? widget.movieName : "",
      textAlign: TextAlign.center,
      style: new TextStyle(
        fontWeight: FontWeight.w500,
        color: Constants.appColorFont,
        fontSize: widget.showOriginalPosters ? 22.0 : 30.0,
      ),
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: widget.showOriginalPosters ? screenWidth : screenWidth,
      padding: const EdgeInsets.all(2.0),
      child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: widget.showOriginalPosters
              ? CrossAxisAlignment.center
              : CrossAxisAlignment.start,
          children: [
            new Container(
            
              width: screenWidth,
              padding: EdgeInsets.only(
                left: widget.showOriginalPosters ? 3 : 0,
                right: widget.showOriginalPosters ? 3 : 0,
                top: widget.showOriginalPosters ? 10.0 : 0,
                bottom: widget.showOriginalPosters ? 10.0 : 0,
              ),

              child: displayHeading(),
            ),
          ]),
    );
  }
}
