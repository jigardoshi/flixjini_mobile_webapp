import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';

class MovieDetailQuickFacts extends StatefulWidget {
  MovieDetailQuickFacts({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailQuickFactsState createState() =>
      new _MovieDetailQuickFactsState();
}

class _MovieDetailQuickFactsState extends State<MovieDetailQuickFacts> {
  matchUrlImage(name) {
    String photo = "images/pending.png";
    printIfDebug("Quick Fact Name");
    printIfDebug(name);
    return photo;
  }

  Widget buildStreamingSourceCards(int index) {
    String fact = widget.movieDetail["quickFacts"][index]["fact"];
    return new Container(
      width: 100.0,
      height: 100.0,
      padding: const EdgeInsets.all(0.0),
      child: new Card(
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        elevation: 6.0,
        color: Constants.appColorFont.withOpacity(0.9),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              new Expanded(
                child: new Center(
                  child: new Container(
                    padding: const EdgeInsets.all(2.0),
                    child: new Image.network(
                      widget.movieDetail["quickFacts"][index]["factImage"],
                      color: Constants.appColorLogo,
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                flex: 1,
              ),
              new Expanded(
                child: new Center(
                  child: new Container(
                    padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                    child: new Text(
                      fact,
                      style: new TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 11.0,
                          color: Constants.appColorL3),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  ),
                ),
                flex: 1,
              ),
            ]),
      ),
    );
  }

  Widget descriptionFactsCardViewGridMimic() {
    int numberOfFactsPerRow = 3;
    int numberOfFacts = widget.movieDetail["quickFacts"].length;
    int numberOfRows =
        (widget.movieDetail["quickFacts"].length / numberOfFactsPerRow).floor();
    printIfDebug("Number of Rows");
    printIfDebug(numberOfRows);
    int numberOfFactsInLastRow =
        numberOfFacts - (numberOfRows * numberOfFactsPerRow);
    printIfDebug("Number of Facts in Last Row");
    printIfDebug(numberOfFactsInLastRow);
    if (numberOfFactsInLastRow != 0) {
      numberOfRows += 1;
    }
    printIfDebug("Total Number of Rows");
    printIfDebug(numberOfRows);
    int index = 0;
    var gridRows = <Widget>[];
    for (int i = 0; i < numberOfRows; i++) {
      var factCards = <Widget>[];
      if (i == (numberOfRows - 1) &&
          (numberOfFactsInLastRow != numberOfFactsPerRow)) {
        numberOfFactsPerRow = numberOfFactsInLastRow;
      }
      for (int j = 0; j < numberOfFactsPerRow; j++) {
        printIfDebug("Index at");
        printIfDebug(index);
        printIfDebug(numberOfFactsInLastRow);
        var factCard = buildStreamingSourceCards(index);
        factCards.add(factCard);
        index += 1;
      }
      var eachRow = new Center(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: factCards,
        ),
      );
      gridRows.add(eachRow);
    }
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: gridRows,
    );
  }

  Widget ratingCardView() {
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      padding: const EdgeInsets.all(0.0),
      child: new GridView.builder(
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            crossAxisCount: (orientation == Orientation.portrait) ? 3 : 6),
        physics: new NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        primary: true,
        itemCount: widget.movieDetail["quickFacts"].length,
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int i) {
          return new GridTile(
            child: buildStreamingSourceCards(i),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      color:  Constants.appColorFont.withOpacity(0.9),
      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            new Container(
              padding: const EdgeInsets.only(top: 10.0),
              child: new Center(
                child: descriptionFactsCardViewGridMimic(),
              ),
            ),
          ]),
    );
  }
}
