import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailGallery extends StatefulWidget {
  MovieDetailGallery({
    Key? key,
    required this.title,
    this.photos,
    required this.current,
    required this.type,
  }) : super(key: key);

  static const String routeName = "/MovieDetailGallery";

  final String title;
  final photos;
  final int current;
  final String type;

  @override
  _MovieDetailGalleryState createState() => new _MovieDetailGalleryState();
}

class _MovieDetailGalleryState extends State<MovieDetailGallery> {
  int position = 0;
  bool moveLeftRight = false;
  ScrollController galleryController = new ScrollController();

  @override
  void initState() {
    super.initState();
    if (!moveLeftRight) {
      setState(() {
        position = widget.current;
      });
      printIfDebug("Initial Setter");
      printIfDebug(position);
      printIfDebug(widget.current);
    }
  }

  Widget appHeaderBar() {
    return new AppBar(
      backgroundColor: Colors.transparent,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      elevation: 0.0,
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorL1),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      title: new Text(widget.title,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
              fontWeight: FontWeight.normal, color: Constants.appColorL3),
          textAlign: TextAlign.center,
          maxLines: 1),
    );
  }

  moveToLeftImage() {
    if (position > 0) {
      printIfDebug("Move To previous image on the left");
      setState(() {
        moveLeftRight = true;
        position = position - 1;
      });
    } else {
      printIfDebug("Your at the first image");
      setState(() {
        moveLeftRight = true;
        position = (widget.photos).length - 1;
      });
    }
    movetoImagePosition(position);
    printIfDebug("New Position");
    printIfDebug(position);
  }

  moreToRightImage() {
    if ((position < (widget.photos).length - 1)) {
      printIfDebug("Move To next image on the right");
      setState(() {
        moveLeftRight = true;
        position = position + 1;
      });
    } else {
      printIfDebug("Your at the last image");
      setState(() {
        moveLeftRight = true;
        position = 0;
      });
    }
    movetoImagePosition(position);
    printIfDebug("New Position");
    printIfDebug(position);
  }

  void movetoImagePosition(int index) {
    printIfDebug("index");
    printIfDebug(index);
    (galleryController).animateTo(
        (100.0 *
            index), // 100 is the height of container and index of 6th element is 5
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut);
  }

  Widget slideShow() {
    double width = MediaQuery.of(context).size.width;
    return new Container(
      width: width,
      padding: const EdgeInsets.all(1.0),
      child: new Center(
        child: new Dismissible(
          resizeDuration: null,
          onDismissed: (DismissDirection direction) {
            printIfDebug("Direction");
            printIfDebug(direction);
            if (direction == DismissDirection.startToEnd) {
              moveToLeftImage();
            }
            if (direction == DismissDirection.endToStart) {
              moreToRightImage();
            }
          },
          direction: DismissDirection.horizontal,
          key: new ValueKey(position),
          child: new Container(
            child: new Image(
              image: NetworkImage(
                widget.photos[position][widget.type],
                // useDiskCache: false,
              ),
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          ),
        ),
      ),
    );
  }

  leftArrow() {
    return new Expanded(
      child: new InkWell(
        onTap: () {
          moveToLeftImage();
        },
        child: new Container(
          child: new Icon(
            Icons.keyboard_arrow_left,
            color: Constants.appColorFont,
          ),
        ),
      ),
      flex: 1,
    );
  }

  rightArrow() {
    return new Expanded(
      child: new InkWell(
        onTap: () {
          moreToRightImage();
        },
        child: new Container(
          child: new Icon(
            Icons.keyboard_arrow_right,
            color: Constants.appColorFont,
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget swipeArrows() {
    return new Positioned.fill(
      bottom: 10.0,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          leftArrow(),
          rightArrow(),
        ],
      ),
    );
  }

  Widget backArrow() {
    return new Positioned(
      top: 2.0,
      left: 1.0,
      child: new Container(
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }

  Widget selectedImage() {
    return new Expanded(
      child: new Container(
        color: Constants.appColorL1.withOpacity(0.9),
        child: new Stack(
          children: [
            slideShow(),
            backArrow(),
          ],
        ),
      ),
      flex: 4,
    );
  }

  Widget displayTickMark(bool displayTick) {
    if (displayTick) {
      return new Positioned.fill(
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: new Container(
            color: Constants.appColorLogo.withOpacity(0.4),
            child: Icon(
              Icons.check_circle,
              size: 30.0,
              color: Constants.appColorFont,
            ),
          ),
        ),
      );
    } else {
      return new Container();
    }
  }

  Widget buildPhotos(BuildContext context, int index) {
    var photo = widget.photos[index][widget.type];
    Color checkBorderSelected =
        (index == position) ? Constants.appColorFont : Colors.transparent;
    bool displayTick = (index == position) ? true : false;
    return new Padding(
      padding: const EdgeInsets.all(0.0),
      child: new GestureDetector(
        onTap: () {
          printIfDebug("Image Id that has been clicked");
          setState(() {
            position = index;
          });
          movetoImagePosition(index);
        },
        child: new Card(
          elevation: 6.0,
          color: Constants.appColorFont,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
            side: BorderSide(color: checkBorderSelected, width: 1.0),
          ),
          child: new Stack(children: [
            new Container(
              padding: const EdgeInsets.all(0.0),
              child: new ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: new Image.network(
                  photo,
                  width: 100.0,
                  height: 150.0,
                  fit: BoxFit.cover,
                  gaplessPlayback: true,
                ),
              ),
            ),
            displayTickMark(displayTick),
          ]),
        ),
      ),
    );
  }

  Widget imagesListView() {
    return new Expanded(
      child: new Container(
        color: Constants.appColorL1.withOpacity(0.9),
        child: new SizedBox.fromSize(
          size: const Size.fromHeight(150.0),
          child: new ListView.builder(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            addAutomaticKeepAlives: true,
            controller: galleryController,
            itemCount: widget.photos.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(10.0),
            itemBuilder: buildPhotos,
          ),
        ),
      ),
      flex: 10,
    );
  }

  imageListViewAndArrows() {
    return new Expanded(
      child: new Container(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            leftArrow(),
            imagesListView(),
            rightArrow(),
          ],
        ),
      ),
      flex: 1,
    );
  }

  Widget imageGallery() {
    double height = MediaQuery.of(context).size.height;
    return new Container(
      padding: new EdgeInsets.only(top: height * 0.05),
      color: Constants.appColorL1,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          selectedImage(),
          imageListViewAndArrows(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Scaffold(
      body: imageGallery(),
    );
  }
}
