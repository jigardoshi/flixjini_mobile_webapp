import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailReviewHighlightWords extends StatefulWidget {
  MovieDetailReviewHighlightWords({
    Key? key,
    this.reviewHighlightWords,
    this.reviewHighlightWordsImage,
  }) : super(key: key);

  final reviewHighlightWords;
  final reviewHighlightWordsImage;

  @override
  _MovieDetailReviewHighlightWordsState createState() =>
      new _MovieDetailReviewHighlightWordsState();
}

class _MovieDetailReviewHighlightWordsState
    extends State<MovieDetailReviewHighlightWords> {
  bool nullChecker(String key) {
    if (widget.reviewHighlightWords.containsKey(key)) {
      if (widget.reviewHighlightWords[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  Widget setHighlightWordImage(int index) {
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(0.0),
          child: new Center(
            child: new Image(
              // color: Constants.appColorFont,
              image: NetworkImage(
                widget.reviewHighlightWordsImage,
                // useDiskCache: false,
              ),
              fit: BoxFit.fitWidth,
              gaplessPlayback: true,
            ),
          )),
      flex: 1,
    );
  }

  Widget displayHighlightWord(int index) {
    Color overallColor = Constants.appColorFont;
    return new Expanded(
      child: new Container(
          width: 120.0,
          padding: const EdgeInsets.all(0.0),
          child: new Center(
            child: new Text(
              widget.reviewHighlightWords[index]["name"][0].toUpperCase() +
                  widget.reviewHighlightWords[index]["name"].substring(1),
              textAlign: TextAlign.center,
              style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: overallColor,
                  fontSize: 10.0),
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
            ),
          )),
      flex: 2,
    );
  }

  Widget buildReviewStatCards(BuildContext context, int index) {
    var sections = <Widget>[];
    sections.add(setHighlightWordImage(index));
    sections.add(displayHighlightWord(index));
    String word = widget.reviewHighlightWords[index]["name"][0].toUpperCase() +
        widget.reviewHighlightWords[index]["name"].substring(1);
    return new Container(
      padding: const EdgeInsets.only(right: 5.0),
      child: new Container(
        width: word.length * 9.0 > 120 ? word.length * 9.0 : 120.0,
        height: 70.0,
        child: new Card(
          elevation: 6.0,
          color:  Constants.appColorL2,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Container(
              height: 70.0,
              child: new Center(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(8.0),
                  child: new Container(
                    padding: const EdgeInsets.all(10.0),
                    color: Constants.appColorL2,
                    height: 55.0,
                    child: new Center(
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: sections,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget reviewStatsView() {
    return new SizedBox.fromSize(
      size: const Size.fromHeight(70.0),
      child: new ListView.builder(
          itemCount: widget.reviewHighlightWords.length,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          itemBuilder: (context, index) =>
              buildReviewStatCards(context, index)),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      child: new Container(
        height: 70.0,
        child: reviewStatsView(),
      ),
    );
  }
}
