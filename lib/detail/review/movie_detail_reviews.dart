import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import  'package:streaming_entertainment/detail/movie_detail_more_card.dart';
import 'package:flixjini_webapp/reviews/movie_reviews_page.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_card.dart';
// import 'package:flixjini_webapp/detail/movie_detail_less_card.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailReviews extends StatefulWidget {
  MovieDetailReviews({
    Key? key,
    this.reviews,
    this.id,
    this.authenticationToken,
    this.urlPhase,
  }) : super(key: key);

  final reviews;
  final id;
  final authenticationToken;
  final urlPhase;

  @override
  _MovieDetailReviewsState createState() => new _MovieDetailReviewsState();
}

class _MovieDetailReviewsState extends State<MovieDetailReviews> {
  bool expandMoviePhotos = false;
  bool needMoreGalleryView = true;
  int condensedDisplaySize = 4;
  int sampleLength = 0;
  String morePhoto = "assests/more.png";
  String lessPhoto = "assests/less.png";
  double screenWidth = 0.0;

  void toogleExpansionMinimisation() {
    setState(() {
      expandMoviePhotos = expandMoviePhotos ? false : true;
    });
  }

  Widget setHighlightWordImage(int index) {
    String highlightWordImage = "images/pending.png";
    return new Expanded(
      child: new Container(
          width: 180.0,
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Image.asset(highlightWordImage,
                color: Constants.appColorL3, fit: BoxFit.cover),
          )),
      flex: 1,
    );
  }

  Widget displayReviewSummary(int index, double fontSize, width) {
    return new Expanded(
      child: new Container(
          width: width,
          padding: new EdgeInsets.only(left: 20.0),
          child: new Center(
            child: new Text(
              widget.reviews[index]["summary"],
              textAlign: TextAlign.left,
              style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Constants.appColorL3,
                  fontSize: fontSize),
              maxLines: 5,
              overflow: TextOverflow.ellipsis,
            ),
          )),
      flex: 3,
    );
  }

  Widget displayReviewSourceImage(int index) {
    String sourceImage = widget.reviews[index]["sourceImage"];
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Image(
              image: NetworkImage(
                sourceImage,
                // useDiskCache: false,
              ),
              fit: BoxFit.cover,
              gaplessPlayback: true,
            ),
          )),
      flex: 1,
    );
  }

  Widget displayReviewSourceTitle(int index) {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(2.0),
        child: new Text(
          widget.reviews[index]["sourceName"],
          textAlign: TextAlign.right,
          style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: Constants.appColorL3,
              fontSize: 10.0),
          maxLines: 4,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      flex: 3,
    );
  }

  Widget displayReviewSource(int index) {
    return new Expanded(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          displayReviewSourceTitle(index),
          displayReviewSourceImage(index),
        ],
      ),
      flex: 2,
    );
  }

  Widget mainReviewCard(int index, var setting, double fontSize, double height,
      double width, var paddingLeft) {
    var sections = <Widget>[];
    sections.add(displayReviewSummary(index, fontSize, width));
    sections.add(displayReviewSource(index));
    return new Positioned(
      child: new Container(
        padding: new EdgeInsets.only(left: paddingLeft, top: 5.0),
        child: new GestureDetector(
          onTap: () {},
          child: new Card(
            elevation: 6.0,
            color: Constants.appColorFont,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Container(
                padding: const EdgeInsets.only(top: 3.0, bottom: 3.0),
                color: setting["overallRatingColor"],
                height: height,
                width: width,
                child: new Center(
                  child: new ClipRRect(
                    borderRadius: new BorderRadius.circular(8.0),
                    child: new Container(
                      padding: const EdgeInsets.all(10.0),
                      color: Constants.appColorFont,
                      height: height - 15.0,
                      child: new Center(
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: sections,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget displayRatingImage(int index, var setting) {
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(10.0),
          child: new Center(
            child: new Image.asset(setting["assestPath"],
                color: Constants.appColorFont, fit: BoxFit.cover),
          )),
      flex: 1,
    );
  }

  Widget displayReviewRating(int index) {
    String rating = "";
    if (widget.reviews[index]["rating"] == null) {
      rating = "-";
    } else {
      rating = widget.reviews[index]["rating"].toString() + "/5";
    }
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(0.0),
          child: new Center(
            child: new Text(
              rating,
              textAlign: TextAlign.center,
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Constants.appColorFont,
                  fontSize: 11.0),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )),
      flex: 1,
    );
  }

  Widget sideReviewRatingCard(int index, var setting) {
    return new Positioned(
      top: 10.0,
      child: new Container(
        width: 50.0,
        height: 85.0,
        child: new Card(
          color: setting["overallRatingColor"],
          elevation: 4.0,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(),
              displayRatingImage(index, setting),
              displayReviewRating(index),
            ],
          ),
        ),
      ),
    );
  }

  basedOnRating(index) {
    var setting = {};
    String assestPath = "assests/review_neutral.png";
    Color overallRatingColor = Constants.appColorFont;
    printIfDebug("Rating");
    printIfDebug(widget.reviews[index]["rating"]);

    setting["overallRatingColor"] = overallRatingColor;
    setting["assestPath"] = assestPath;
    return setting;
  }

  Widget moreCardUI() {
    return new Center(
      child: new Container(
        height: 180.0,
        width: 120.0,
        padding: const EdgeInsets.all(0.0),
        child: new GestureDetector(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => new MovieReviewsPage(
                      id: widget.id,
                      authenticationToken: widget.authenticationToken,
                      urlPhase: widget.urlPhase)));
            },
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Container(
                    margin: EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Image.asset(
                            'assests/expand_icon.png',
                            height: 18,
                            width: 18,
                          ),
                        ),
                        new Container(
                          padding: const EdgeInsets.all(5.0),
                          child: new Text(
                            "More",
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontWeight: FontWeight.normal,
                                color:  Constants.appColorL3,
                                fontSize: 16.0),
                          ),
                        ),
                      ],
                    )),
              ],
            )),
      ),
    );
  }

  Widget buildPhotos(BuildContext context, int index) {
    var setting = {};
    printIfDebug(
        "from build photos method, value of setting after immediate declaration: $setting");
    double fontSize = 13.0;
    double height = 170.0;
    double width = screenWidth * 0.7;
    double paddingLeft = 20.0;
    setting = basedOnRating(index);
    if (index < sampleLength) {
      return MovieDetailReviewCard(
        review: widget.reviews[index],
        fontSize: fontSize,
        height: height,
        width: width,
        paddingLeft: paddingLeft,
        less: true,
      );
    } else {
      return moreCardUI();
    }
  }

  Widget galleryView() {
    int gallaryTrackLength = widget.reviews.length;
    if (gallaryTrackLength > condensedDisplaySize) {
      printIfDebug("Has greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        sampleLength = condensedDisplaySize;
      });
    } else {
      printIfDebug("Doesn't have greater than 4 images");
      printIfDebug(gallaryTrackLength);
      setState(() {
        needMoreGalleryView = false;
        sampleLength = gallaryTrackLength;
      });
    }
    printIfDebug("Based on Number of Tracks");
    printIfDebug(needMoreGalleryView);
    if (needMoreGalleryView) {
      // return new SizedBox.fromSize(
      //   size: const Size.fromHeight(360.0),
      //   child: new ListView.builder(
      //     itemCount: 2,
      //     scrollDirection: Axis.vertical,
      //     padding: const EdgeInsets.all(10.0),
      //     itemBuilder: buildPhotos,
      //   ),
      // );
      return GridView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          childAspectRatio: 2, // 1.75,
          mainAxisSpacing: 5, // 2
          // crossAxisSpacing: 2, // 2
        ),
        itemCount: sampleLength > 2 ? 2 : sampleLength,
        itemBuilder: buildPhotos,
      );
    } else {
      // return new SizedBox.fromSize(
      //   size: const Size.fromHeight(360.0),
      //   child: new ListView.builder(
      //     itemCount: 2,
      //     scrollDirection: Axis.vertical,
      //     padding: const EdgeInsets.all(10.0),
      //     itemBuilder: buildPhotos,
      //   ),
      // );
      return GridView.builder(
        addAutomaticKeepAlives: false,
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          childAspectRatio: 2, // 1.75,
          mainAxisSpacing: 5, // 2
          crossAxisSpacing: 2, // 2
        ),
        itemCount: sampleLength > 2 ? 2 : sampleLength,
        itemBuilder: buildPhotos,
      );
    }
  }

  constructReviewRow(int i) {
    double screenWidth = MediaQuery.of(context).size.width;
    var setting = {};
    setting = basedOnRating(i);
    double fontSize = 11.0;
    double height = 180.0;
    double width = screenWidth * 0.85;
    double paddingLeft = 20.0;
    return new Container(
      padding: const EdgeInsets.all(2.0),
      height: 120.0,
      child: new Stack(
        children: <Widget>[
          mainReviewCard(i, setting, fontSize, height, width, paddingLeft),
          sideReviewRatingCard(i, setting),
        ],
      ),
    );
  }

  Widget lessCard(double cardHeight, double cardWidth) {
    return new GestureDetector(
      onTap: () {
        toogleExpansionMinimisation();
      },
      child: new Container(
        height: cardHeight,
        width: cardWidth,
        child: new Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Expanded(
                child: new Container(
                  child: new Container(
                    width: 70.0,
                    height: 70.0,
                    padding: const EdgeInsets.all(10.0),
                    child: new Image.asset(
                      lessPhoto,
                    ),
                  ),
                ),
                flex: 3,
              ),
              new Expanded(
                child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          "assests/collapse_icon.png",
                          height: 18,
                          width: 18,
                        ),
                        margin: EdgeInsets.only(
                          right: 5,
                        ),
                      ),
                      Text(
                        "Less",
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Constants.appColorL3,
                            fontSize: 16.0),
                      ),
                    ]),
                flex: 1,
              ),
            ]),
      ),
    );
  }

  Widget expandedGalleryView() {
    double cardHeight = 180.0;
    double cardWidth = 180.0;
    int numberOfReviews = widget.reviews.length;
    var sections = <Widget>[];
    for (int i = 0; i < numberOfReviews + 1; i++) {
      if (i < numberOfReviews) {
        printIfDebug("Lol");
        sections.add(constructReviewRow(i));
      } else {
        sections.add(lessCard(cardHeight, cardWidth));
      }
    }
    printIfDebug("Sections");
    printIfDebug(sections);
    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: sections,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    if (expandMoviePhotos && needMoreGalleryView) {
      return new Container(
        color: Constants.appColorL1,
        child: new Padding(
          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new Container(
                  padding: const EdgeInsets.all(3.0),
                  child: expandedGalleryView(),
                ),
              ]),
        ),
      );
    } else {
      return new Container(
        width: screenWidth,
        // height: 360.0,
        child: galleryView(),
      );
    }
  }
}
