import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:html_unescape/html_unescape.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_stats.dart';
// import 'package:flixjini_webapp/detail/review/movie_detail_review_highlight_words.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_reviews.dart';
import 'package:flixjini_webapp/reviews/movie_reviews_page.dart';

// import 	'dart:convert';

class MovieDetailReviewSection extends StatefulWidget {
  MovieDetailReviewSection({
    Key? key,
    this.reviewSection,
    this.id,
    this.authenticationToken,
    this.urlPhase,
  }) : super(key: key);

  final reviewSection;
  final id;
  final authenticationToken;
  final urlPhase;

  @override
  _MovieDetailReviewSectionState createState() =>
      new _MovieDetailReviewSectionState();
}

class _MovieDetailReviewSectionState extends State<MovieDetailReviewSection> {
  @override
  void initState() {
    super.initState();
  }

  bool nullChecker(String key) {
    if (widget.reviewSection.containsKey(key)) {
      if (widget.reviewSection[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  renderSections() {
    var sections = <Widget>[];
    bool statsFlag = nullChecker("stats");
    bool sammaWordFlag = nullChecker("sammaWords");
    printIfDebug(sammaWordFlag);
    bool reviewsFlag = nullChecker("reviews");
    // if (sammaWordFlag) {
    //   sections.add(new MovieDetailReviewHighlightWords(
    //       reviewHighlightWords: widget.reviewSection["sammaWords"],
    //       reviewHighlightWordsImage: widget.reviewSection["sammaWordsImage"]));
    // }
    if (statsFlag) {
      sections.add(new MovieDetailReviewStats(
          reviewStats: widget.reviewSection["stats"]));
    }
    if (reviewsFlag) {
      sections.add(new MovieDetailReviews(
          reviews: widget.reviewSection["reviews"],
          id: widget.id,
          authenticationToken: widget.authenticationToken,
          urlPhase: widget.urlPhase));
      sections.add(showSeeAllReviews());
    }

    return sections;
  }

  Widget showSeeAllReviews() {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new MovieReviewsPage(
                id: widget.id,
                authenticationToken: widget.authenticationToken,
                urlPhase: widget.urlPhase)));
      },
      child: Container(
        padding: EdgeInsets.only(
          // right: 10,
          top: 10,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              child: Text(
                "SEE ALL ",
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w800,
                  letterSpacing: 1,
                  color: Constants.appColorFont.withOpacity(0.6),
                ),
              ),
            ),
            Container(
              height: 20,
              child: Image.asset(
                "assests/c_more_black.png",
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new MovieDetailSubheading(subheading: "Reviews"),
          // new MovieDetailUnderline(),
          new Container(
            padding: const EdgeInsets.all(3.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: renderSections(),
            ),
          ),
        ],
      ),
    );
  }
}
