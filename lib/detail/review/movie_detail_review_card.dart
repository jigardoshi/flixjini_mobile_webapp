import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import  'package:streaming_entertainment/detail/movie_detail_subheading.dart';
// import  'package:streaming_entertainment/detail/movie_detail_underline.dart';
// import  'package:streaming_entertainment/detail/movie_detail_more_card.dart';
// import  'package:streaming_entertainment/reviews/movie_reviews_page.dart';
// import  'package:streaming_entertainment/detail/movie_detail_less_card.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailReviewCard extends StatefulWidget {
  MovieDetailReviewCard({
    Key? key,
    this.review,
    this.fontSize,
    this.height,
    this.width,
    this.paddingLeft,
    this.less,
  }) : super(key: key);

  final review;
  final fontSize;
  final height;
  final width;
  final paddingLeft;
  final less;

  @override
  _MovieDetailReviewCardState createState() =>
      new _MovieDetailReviewCardState();
}

class _MovieDetailReviewCardState extends State<MovieDetailReviewCard> {
  launchReviewWebsite() async {
    if (await canLaunch(widget.review["reviewLink"])) {
      await launch(widget.review["reviewLink"]);
      printIfDebug("Yes we lunched url");
    } else {
      throw 'Could not launch';
    }
  }

  Widget displayReviewSummary() {
    return widget.less
        ? Expanded(
            flex: 4,
            // padding: new EdgeInsets.only(left: 20.0),
            child: new Scrollbar(
              child: new SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: new Container(
                  // width: widget.width,
                  padding: new EdgeInsets.all(5.0),
                  // child: new Center(
                  child: new Text(
                    widget.review["summary"],
                    textAlign: TextAlign.left,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: widget.fontSize),
                    maxLines: 10,
                    overflow: TextOverflow.ellipsis,
                  ),
                  // ),
                ),
              ),
            ),
          )
        : Container(
            // padding: new EdgeInsets.only(left: 20.0),
            child: new Scrollbar(
              child: new SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: new Container(
                  // width: widget.width,
                  padding: new EdgeInsets.all(5.0),
                  // child: new Center(
                  child: new Text(
                    widget.review["summary"],
                    textAlign: TextAlign.left,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: widget.fontSize),
                    maxLines: 10,
                    overflow: TextOverflow.ellipsis,
                  ),
                  // ),
                ),
              ),
            ),
          );
  }

  Widget displayReviewSourceImage() {
    String sourceImage = widget.review["sourceImage"];
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Image(
              image: NetworkImage(
                sourceImage,
                // useDiskCache: false,
              ),
              fit: BoxFit.contain,
              gaplessPlayback: true,
            ),
          )),
      flex: 1,
    );
  }

  Widget displayReviewSourceTitle() {
    return widget.review["sourceName"].toString().length > 0
        ? Expanded(
            child: new Container(
              margin: EdgeInsets.only(
                bottom: 5,
              ),
              padding: const EdgeInsets.all(2.0),
              child: new Text(
                widget.review["sourceName"],
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorLogo,
                    fontSize: 16.0),
                maxLines: 4,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            flex: 5,
          )
        : Container();
  }

  Widget displayReviewSource() {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        displayReviewSourceTitle(),
        // displayReviewSourceImage(),
      ],
    );
  }

  Widget mainReviewCard() {
    var sections = <Widget>[];
    sections.add(displayReviewSource());
    sections.add(displayReviewRating());
    sections.add(displayReviewSummary());

    double lines = widget.review["summary"].toString().length / 50;
    double heightNew = ((lines * 15.0) + 100) > widget.height - 10
        ? ((lines * 15.0) + 100)
        : widget.height - 10;
    return new Positioned(
      child: new Container(
        padding: new EdgeInsets.only(left: 5, top: 5.0),
        child: new Container(
          padding: const EdgeInsets.only(top: 3.0, bottom: 3.0),
          // color: setting["overallRatingColor"],
          height: widget.less ? widget.height : heightNew,
          // width: widget.width,
          child: new Center(
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Container(
                padding: const EdgeInsets.all(10.0),
                color: Constants.appColorL2,
                height: (widget.less ? widget.height : heightNew) - 5.0,
                child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: sections,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget mainReviewCardLess(var setting) {
    var sections = <Widget>[];
    sections.add(displayReviewSource());
    sections.add(displayReviewRating());
    sections.add(displayReviewSummary());

    double lines = widget.review["summary"].toString().length / 50;
    double heightNew = ((lines * 15.0) + 100) > widget.height - 10
        ? ((lines * 15.0) + 100)
        : widget.height - 10;
    return new Positioned(
      child: new Container(
        padding: new EdgeInsets.only(left: 5, top: 5.0),
        child: new Container(
          padding: const EdgeInsets.only(top: 3.0, bottom: 3.0),
          // color: setting["overallRatingColor"],
          height: heightNew,
          // width: widget.width,
          child: new Center(
            child: new ClipRRect(
              borderRadius: new BorderRadius.circular(8.0),
              child: new Container(
                padding: const EdgeInsets.all(10.0),
                color: Constants.appColorL2,
                height: heightNew - 5.0,
                child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: sections,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget displayRatingImage(var setting) {
    return new Expanded(
      child: new Container(
          padding: const EdgeInsets.all(10.0),
          child: new Center(
            child: new Image.asset(
              setting["assestPath"],
              color: Constants.appColorFont,
              fit: BoxFit.cover,
            ),
          )),
      flex: 2,
    );
  }

  Widget displayReviewRating() {
    double rating = 0;
    if (widget.review["rating"].isEmpty) {
      rating = 0;
    } else {
      rating = double.parse(widget.review["rating"]);
    }
    return new Container(
      // padding: const EdgeInsets.all(0.0),
      margin: EdgeInsets.only(
        bottom: 5,
      ),
      child: RatingBar(
        itemSize: 25.0,
        initialRating: rating,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        // itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
        // itemBuilder: (context, _) => Icon(
        //   Icons.star,
        // ),
        ratingWidget: RatingWidget(
          full: Container(
            margin: EdgeInsets.only(
              right: 20,
            ),
            child: Image.asset(
              "assests/review_full.png",
            ),
          ),
          half: Container(
            margin: EdgeInsets.only(
              right: 20,
            ),
            child: Image.asset(
              "assests/review_half.png",
            ),
          ),
          empty: Container(
            margin: EdgeInsets.only(
              right: 20,
            ),
            child: Image.asset(
              "assests/review_empty.png",
            ),
          ),
        ),

        ignoreGestures: true,
        onRatingUpdate: (rating) {
          printIfDebug(rating);
        },
      ),
    );
  }

  Widget sideReviewRatingCard(var setting) {
    return new Positioned(
      top: 15.0,
      child: new Container(
        width: 50.0,
        height: 85.0,
        child: new Card(
          color: setting["overallRatingColor"],
          elevation: 4.0,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8.0),
          ),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(),
              // displayRatingImage(setting),
              displayReviewRating(),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderCard() {
    return new InkWell(
      onTap: () {
        printIfDebug("Go to Review Post");
        launchReviewWebsite();
      },
      child: new Container(
        child: new Stack(
          children: <Widget>[
            mainReviewCard(),
            // sideReviewRatingCard(setting),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return renderCard();
  }
}
