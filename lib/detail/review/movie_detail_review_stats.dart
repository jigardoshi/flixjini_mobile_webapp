import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:percent_indicator/percent_indicator.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailReviewStats extends StatefulWidget {
  MovieDetailReviewStats({
    Key? key,
    this.reviewStats,
  }) : super(key: key);

  final reviewStats;

  @override
  _MovieDetailReviewStatsState createState() =>
      new _MovieDetailReviewStatsState();
}

class _MovieDetailReviewStatsState extends State<MovieDetailReviewStats> {
  bool nullChecker(String key, int index) {
    if (widget.reviewStats[index].containsKey(key)) {
      if (widget.reviewStats[index][key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  Widget statImage(int index, int flexValue) {
    return new Expanded(
      child: new Container(
          width: 95.0,
          padding: const EdgeInsets.all(1.0),
          child: new Center(
            child: new Image(
              image: NetworkImage(
                widget.reviewStats[index]["image"],
                // useDiskCache: false,
              ),
              fit: BoxFit.fitWidth,
              gaplessPlayback: true,
            ),
          )),
      flex: flexValue,
    );
  }

  Widget statRating(
      int index, int flexValue, double fontSize, Color overallColor) {
    return new Expanded(
      child: new Container(
          width: 95.0,
          padding: const EdgeInsets.all(0.0),
          child: new Center(
            child: new Text(
              widget.reviewStats[index]["rating"],
              textAlign: TextAlign.center,
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  color: overallColor,
                  fontSize: fontSize),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          )),
      flex: flexValue,
    );
  }

  Widget statTitle(int index, int flexValue, double fontSize) {
    Color overallColor = Constants.appColorDivider;
    return new Expanded(
      child: new Container(
          width: 95.0,
          padding: const EdgeInsets.all(0.0),
          child: new Center(
            child: new Text(
              widget.reviewStats[index]["name"],
              textAlign: TextAlign.center,
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  color: overallColor,
                  fontSize: fontSize),
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
            ),
          )),
      flex: flexValue,
    );
  }

  Widget reviewStatsView() {
    List percent = widget.reviewStats[1]["rating"].toString().split("/");
    String percentage =
        widget.reviewStats[0]["rating"].toString().replaceAll("%", "");
    return Container(
      margin: EdgeInsets.only(
        top: (percent[0].toString().length > 0 && percent[1] != null) ||
                (percentage.length > 0 && double.parse(percentage) > 0)
            ? 10
            : 0,
      ),
      child: Row(
        children: <Widget>[
          percent[0].toString().length > 0 && percent[1] != null
              ? Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: new CircularPercentIndicator(
                          radius: 65.0,
                          lineWidth: 3.0,
                          percent: double.parse(percent[0]) /
                              double.parse(percent[1]),
                          center: new Text(
                            widget.reviewStats[1]["rating"],
                            style: TextStyle(
                              color: Constants.appColorFont,
                            ),
                          ),
                          backgroundColor: Constants.appColorL1,
                          progressColor: Constants.appColorLogo,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                        ),
                        // padding: EdgeInsets.only(
                        //   left: 50,
                        //   right: 50,
                        // ),
                        child: Text(
                          widget.reviewStats[1]["name"],
                          style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 14,
                          ),
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          percentage.length > 0 && double.parse(percentage) > 0
              ? Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      new CircularPercentIndicator(
                        radius: 65.0,
                        lineWidth: 3.0,
                        percent: double.parse(percentage) / 100,
                        center: new Text(
                          widget.reviewStats[0]["rating"],
                          style: TextStyle(
                            color: Constants.appColorFont,
                          ),
                        ),
                        backgroundColor: Constants.appColorL1,
                        progressColor: Constants.appColorLogo,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                        ),
                        // padding: EdgeInsets.only(
                        //   left: 50,
                        //   right: 50,
                        // ),
                        child: Text(
                          widget.reviewStats[0]["name"],
                          style: TextStyle(
                            color: Constants.appColorFont,
                            fontSize: 14,
                          ),
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
    // new SizedBox.fromSize(
    //   size: const Size.fromHeight(120.0),
    //   child: new ListView.builder(
    //     itemCount: 2,
    //     scrollDirection: Axis.horizontal,
    //     padding: const EdgeInsets.all(10.0),
    //     itemBuilder: (context, index) =>
    //         widget.reviewStats[index]["rating"].isEmpty
    //             ? new Container()
    // : buildReviewStatCards(context, index),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      child: new Container(
        // height: 150.0,
        child: reviewStatsView(),
      ),
    );
  }
}
