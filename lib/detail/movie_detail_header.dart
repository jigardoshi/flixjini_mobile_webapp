import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flixjini_webapp/common/arc_clipper.dart';
// import 'package:flixjini_webapp/detail/movie_detail_gallery.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import "dart:async";
import 'dart:ui';
// import 'dart:math';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailHeader extends StatefulWidget {
  MovieDetailHeader({
    Key? key,
    this.movieDetail,
    this.movieDataFromCall,
  }) : super(key: key);

  final movieDetail, movieDataFromCall;

  @override
  _MovieDetailHeaderState createState() => new _MovieDetailHeaderState();
}

class _MovieDetailHeaderState extends State<MovieDetailHeader> {
  int bannerPosition = 0;

  void initState() {
    super.initState();
  }

  carouselEffect() async {
    if (!mounted)
      return;
    else {
      const oneSec = const Duration(seconds: 5);
      new Timer.periodic(oneSec, (Timer t) {
        if (bannerPosition <
            widget.movieDetail["gallary"]["posters"].length - 1) {
          setState(() {
            bannerPosition = bannerPosition + 1;
          });
          printIfDebug("Banner Position Updated");
          printIfDebug(new DateTime.now());
          printIfDebug(bannerPosition);
        } else {
          printIfDebug("Resetting Banner Position");
          setState(() {
            bannerPosition = 0;
          });
        }
      });
    }
  }

  nullCheckImageUrl(key1, key2) {
    printIfDebug('\n\n');
    var carousel;
    bool flag = true;
    var screenWidth = MediaQuery.of(context).size.width;
    var screenheight = MediaQuery.of(context).size.height;
    if (widget.movieDetail.containsKey(key1)) {
      if (widget.movieDetail[key1].containsKey(key2)) {
        carousel = new Container(
          color: Constants.appColorL1,
          child: ClipPath(
            clipper: ArcClipper(),
            child: new Container(
              width: screenWidth,
              height: screenheight * 0.6,
              decoration: new BoxDecoration(
                color: Constants.appColorL1,
              ),
              child: GestureDetector(
                child: getBlurredCarouselImage(),
              ),
            ),
          ),
        );
        flag = false;
      }
    }
    if (flag) {
      carousel = new Container(
        color: Constants.appColorL1,
        child: ClipPath(
          clipper: ArcClipper(),
          child: new Container(
            width: screenWidth,
            height: screenheight * 0.6,
            decoration: new BoxDecoration(
              color: Constants.appColorL1,
            ),
            child: GestureDetector(
              child: getBlurredCarouselImage(),
            ),
          ),
        ),
      );
    }

    return carousel;
  }

  Widget getBlurredCarouselImage() {
    bool flag = false;
    if (widget.movieDetail != null && widget.movieDetail.length > 1) {
      flag = true;
    }
    String imageurl = "";

    if (!flag) {
      if (widget.movieDataFromCall["moviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["moviePhoto"];
      } else if (widget.movieDataFromCall["image_uri"] != null) {
        imageurl = widget.movieDataFromCall["image_uri"];
      } else if (widget.movieDataFromCall["full_image_uri"] != null) {
        imageurl = widget.movieDataFromCall["full_image_uri"];
      } else if (widget.movieDataFromCall["fullMoviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["fullMoviePhoto"];
      } else if (widget.movieDataFromCall["similarMoviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["similarMoviePhoto"];
      }
    } else {
      if (widget.movieDetail["ogimage"] != null &&
          widget.movieDetail["ogimage"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["ogimage"]
            ? widget.movieDetail["ogimage"]
            : imageurl;
      } else if (widget.movieDetail["moviePoster"] != null &&
          widget.movieDetail["moviePoster"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["moviePoster"]
            ? widget.movieDetail["moviePoster"]
            : imageurl;
      } else if (widget.movieDetail["movieHorizontalPoster"] != null &&
          widget.movieDetail["movieHorizontalPoster"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["movieHorizontalPoster"]
            ? widget.movieDetail["movieHorizontalPoster"]
            : imageurl;
      }
    }
    return Stack(
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            color: Constants.appColorL1,
            image: new DecorationImage(
              image: NetworkImage(imageurl),
              fit: BoxFit.cover,
            ),
          ),
        ),
        new BackdropFilter(
          filter: new ImageFilter.blur(
            sigmaX: 20,
            sigmaY: 20,
          ),
          child: Container(
            color: Constants.appColorL1.withOpacity(
              0.6,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    var carousel = nullCheckImageUrl("gallary", "posters");
    return carousel;
  }
}
