import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';

final people = <Person>[
  new Person('Alice', '123 Main'),
  new Person('Bob', '456 Main')
];
final letters = [
  "Chennai",
  "Bangalore",
  "Delhi",
  "Mumbai",
  "Dubai",
  "Singapore"
];

class Person {
  Person(this.name, this.address);
  final String name, address;
  @override
  String toString() => name;
}

class MovieDetailBookTicketAutoCompleteForm extends StatefulWidget {
  MovieDetailBookTicketAutoCompleteForm({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailBookTicketAutoCompleteFormState createState() =>
      new _MovieDetailBookTicketAutoCompleteFormState();
}

class _MovieDetailBookTicketAutoCompleteFormState
    extends State<MovieDetailBookTicketAutoCompleteForm> {
  void initState() {
    super.initState();
    printIfDebug("The name Auto Complete Fields");
    printIfDebug(people);
    printIfDebug(widget.movieDetail["locations"]["places"]);
    printIfDebug(letters);
  }

  late Person selectedPerson;
  late String selectedLetter;
  var locations = [];
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool autovalidate = false;
  var location = ["Chennai", "Bangalore", "Delhi", "Dubai", "Mumbai"];

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return new Container(
      child: new Padding(
        padding: const EdgeInsets.all(5.0),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                child: new Text(
                  'Book Tickets In',
                  textAlign: TextAlign.left,
                  style: new TextStyle(fontWeight: FontWeight.bold),
                  textScaleFactor: 1.5,
                ),
              ),
              new Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10.0),
                child: new Form(
                  key: formKey,
                  autovalidate: autovalidate,
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        new SizedBox.fromSize(
                          size: const Size.fromHeight(200.0),
                          child: new ListView(children: <Widget>[
                            new SimpleAutocompleteFormField<String>(
                              decoration: new InputDecoration(
                                  labelText: "Location",
                                  border: new OutlineInputBorder()),
                              maxSuggestions: 10,
                              itemBuilder: (context, item) => new Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Text(item!),
                              ),
                              onSearch: (search) async => letters
                                  .where((person) => person
                                      .toLowerCase()
                                      .contains(search.toLowerCase()))
                                  .toList(),
                              itemFromString: (string) => letters.singleWhere(
                                  (letter) => letter == string.toLowerCase(),
                                  orElse: () => null!),
                              onChanged: (value) =>
                                  setState(() => selectedLetter = value!),
                              onSaved: (value) =>
                                  setState(() => selectedLetter = value!),
                              validator: (letter) =>
                                  letter == null ? 'Invalid letter.' : null,
                            ),
                            new SizedBox(height: 16.0),
                            new RaisedButton(
                                child: new Text('Submit'),
                                onPressed: () {
                                  if (formKey.currentState!.validate()) {
                                    formKey.currentState!.save();
                                  } else {
                                    setState(() => autovalidate = true);
                                  }
                                })
                          ]),
                        ),
                      ]),
                ),
              ),
            ]),
      ),
    );
  }
}
