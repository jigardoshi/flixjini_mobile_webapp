import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailPlot extends StatefulWidget {
  MovieDetailPlot({
    Key? key,
    this.movieDetail,
  }) : super(key: key);

  final movieDetail;

  @override
  _MovieDetailPlotState createState() => new _MovieDetailPlotState();
}

class _MovieDetailPlotState extends State<MovieDetailPlot> {
  bool expandStory = false, showMore = false;

  @override
  void initState() {
    super.initState();
  }

  Widget toggleStory() {
    var unescape = new HtmlUnescape();
    String longStory =
        unescape.convert(widget.movieDetail["movieStory"].join('\n\n'));

    String story;
    // if (false) {
    //   story = unescape.convert(widget.movieDetail["movieStory"].join('\n\n'));
    //   return new Text(
    //     story,
    //     textAlign: TextAlign.left,
    //     softWrap: true,
    //     style: new TextStyle(
    //         fontWeight: FontWeight.normal,
    //         fontSize: 13.0,
    //         color: Constants.appColorFont,
    //         height: 1),
    //     textScaleFactor: 1.0,
    //   );
    // } else {
    story = unescape.convert(widget.movieDetail["movieStory"][0]);
    return Column(
      children: <Widget>[
        showMore
            ? Text(
                longStory,
                textAlign: TextAlign.left,
                softWrap: true,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 13.0,
                  color: Constants.appColorFont,
                  height: 1.3,
                ),
                textScaleFactor: 1.0,
              )
            : Text(
                story,
                textAlign: TextAlign.left,
                softWrap: true,
                style: new TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 13.0,
                  color: Constants.appColorFont,
                  height: 1.3,
                ),
                maxLines: 10,
                overflow: TextOverflow.ellipsis,
                textScaleFactor: 1.0,
              ),
        widget.movieDetail["movieStory"].toString().length > 550
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    showMore = !showMore;
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                        // top: 10,
                        right: 5,
                        bottom: 2,
                      ),
                      child: Text(
                        showMore ? "SHOW LESS" : "SHOW MORE",
                        style: TextStyle(
                          color: Constants.appColorFont.withOpacity(0.6),
                        ),
                      ),
                    ),
                    showMore
                        ? Container(
                            margin: EdgeInsets.only(
                              top: 10,
                            ),
                            height: 20,
                            child: Image.asset(
                              "assests/c_up_black.png",
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(
                            margin: EdgeInsets.only(
                              top: 10,
                            ),
                            height: 20,
                            child: Image.asset(
                              "assests/c_down_black.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                  ],
                ),
              )
            : Container(),
      ],
    );
    // }
  }

  Widget toggleStoryOption() {
    if (expandStory) {
      return new Padding(
        padding: const EdgeInsets.only(
          top: 2.0,
          left: 5.0,
          right: 5.0,
          bottom: 10.0,
        ),
        child: Container(
          height: 25,
          width: 25,
          margin: EdgeInsets.only(
            top: 10,
          ),
          child: Image.asset('assests/close_icon.png'),
        ),
      );
    } else {
      return new Padding(
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Text(
            //   'Expand', // 'Show More',
            //   textAlign: TextAlign.right,
            //   style: new TextStyle(
            //       fontWeight: FontWeight.bold,
            //       color: Constants.appColorLogo, // Constants.appColorL1,
            //       fontSize: 14.0),
            //   textScaleFactor: 1.0,
            // ),
            Container(
              child: Icon(
                Icons.arrow_drop_down,
                size: 50,
                color: Constants.appColorLogo,
              ),
            ),
          ],
        ),
      );
    }
  }

  needToggleOption() {
    if (widget.movieDetail["movieStory"].length > 1) {
      return new GestureDetector(
        onTap: () {
          setState(() {
            expandStory = expandStory ? false : true;
          });
        },
        child: toggleStoryOption(),
      );
    } else {
      return new Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new MovieDetailSubheading(subheading: "Plot"),
          // new MovieDetailUnderline(),
          new Padding(
            padding: const EdgeInsets.only(
              left: 10.0,
              right: 10.0,
              top: 10.0,
              bottom: 10,
            ),
            child: toggleStory(),
          ),
          // needToggleOption(),
        ],
      ),
    );
  }
}
