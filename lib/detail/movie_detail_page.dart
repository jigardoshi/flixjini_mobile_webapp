// import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flixjini_webapp/detail/movie_detail_watched.dart';
import 'package:flixjini_webapp/detail/tv_show/share_sheet.dart';
import 'package:flixjini_webapp/feedback/feedback_overlay.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:android_intent/android_intent.dart';
// import 'package:android_intent/flag.dart';
// import 'package:esys_flutter_share/esys_flutter_share.dart';
// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/detail/movie_detail_collapsable_appbar.dart';
import 'package:flixjini_webapp/detail/movie_detail_streaming_sources.dart';
import 'package:flixjini_webapp/detail/review/movie_detail_review_section.dart';
import 'package:flixjini_webapp/detail/book_now/movie_detail_book_now.dart';
// import 'package:flixjini_webapp/detail/movie_detail_quick_facts.dart';
import 'package:flixjini_webapp/detail/movie_detail_similar_movies.dart';
import 'package:flixjini_webapp/detail/movie_detail_about_movie.dart';
import 'package:flixjini_webapp/detail/movie_detail_plot.dart';
import 'package:flixjini_webapp/detail/movie_detail_movie_gallery.dart';
import 'package:flixjini_webapp/detail/movie_detail_cast_gallery.dart';
import 'package:flixjini_webapp/detail/movie_detail_awards.dart';
import 'package:flixjini_webapp/detail/movie_detail_tracks.dart';
import 'package:flixjini_webapp/detail/movie_detail_videos.dart';
import 'package:flixjini_webapp/detail/movie_detail_tags.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_section.dart';
import 'package:flixjini_webapp/navigation/movie_navigator_with_notch.dart';
// import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/error_handle_page.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_index.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/detail/movie_detail_rating.dart';
import 'dart:ui' as ui;
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailPage extends StatefulWidget {
  MovieDetailPage({
    Key? key,
    this.urlInformation,
    this.showOriginalPosters,
    this.notification,
    this.movieDataFromCall,
    this.fromGenie,
  }) : super(key: key);

  final urlInformation,
      showOriginalPosters,
      notification,
      movieDataFromCall,
      fromGenie;

  @override
  _MovieDetailPageState createState() => new _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage>
    with WidgetsBindingObserver {
  var movieDetail = {}, reminderData = {}, movieTopDetail = {};
  bool fetchedMovieDetail = false, fetchedMovieTopDetail = false;
  bool ratingsFlag = false, ipError = false;
  bool streamingSourceSelected = false;
  bool streamingStatus = false, showFeedback = false, showShareReminder = false;
  String authenticationToken = "";
  String playNotchPointer = "";
  String urlName = "";
  final watchNowKey = new GlobalKey();
  final bookNowKey = new GlobalKey();
  ScrollController sourceController = new ScrollController();
  List queueIds = [];
  static const platform = const MethodChannel('api.komparify/advertisingid');
  bool whatsAppInstalled = false,
      sharingFlag = false,
      showOriginalPosters = false;
  var shareSocial1 = [], shareImageAddress = [];
  GlobalKey previewContainer = new GlobalKey();
  int originalSize = 800, shareType = 0;

  var shareSocial = [];
  //   {
  //     "name": "Whatsapp",
  //     "check_name": "com.whatsapp",
  //     "icon": "assests/share/whats_share.png",
  //     "package_name": "com.whatsapp",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "Insta Story",
  //     "icon": "assests/share/insta_share.png",
  //     "check_name": "com.instagram.android",
  //     "package_name": "com.instagram.share.ADD_TO_STORY",
  //     "screenshot": true,
  //     "shareType": 0,
  //   },
  //   {
  //     "name": "FB Story",
  //     "icon": "assests/share/fb_share.png",
  //     "check_name": "com.facebook.katana",
  //     "package_name": "com.facebook.share.ADD_STICKER_TO_STORY",
  //     "screenshot": true,
  //     "shareType": 0,
  //   },
  //   {
  //     "name": "SnapChat",
  //     "check_name": "com.snapchat.android",
  //     "icon": "assests/share/snap_share.png",
  //     "package_name": "com.snapchat.android",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "LinkedIn",
  //     "check_name": "com.linkedin.android",
  //     "icon": "assests/share/link_share.png",
  //     "package_name": "com.linkedin.android",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "Chat",
  //     "check_name": "com.google.android.apps.dynamite",
  //     "icon": "assests/share/gchat_share.png",
  //     "package_name": "com.google.android.apps.dynamite",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "Hangouts",
  //     "check_name": "com.google.android.talk",
  //     "icon": "assests/share/hangouts_share.png",
  //     "package_name": "com.google.android.talk",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "Instagram",
  //     "check_name": "com.instagram.android",
  //     "icon": "assests/share/insta_share.png",
  //     "package_name": "com.instagram.android",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   {
  //     "name": "FaceBook",
  //     "icon": "assests/share/fb_share.png",
  //     "check_name": "com.facebook.katana",
  //     "package_name": "com.facebook.katana",
  //     "screenshot": true,
  //     "shareType": 2,
  //   },
  //   {
  //     "name": "Copy Link",
  //     "icon": "assests/share/copy_url.png",
  //     "check_name": "",
  //     "package_name": "",
  //     "screenshot": false,
  //     "shareType": 3,
  //   },
  //   {
  //     "name": "Share Link",
  //     "icon": "assests/share/share_url.png",
  //     "check_name": "",
  //     "package_name": "",
  //     "screenshot": false,
  //     "shareType": 2,
  //   },
  //   {
  //     "name": "Share Image",
  //     "icon": "assests/share/share_img.png",
  //     "check_name": "",
  //     "package_name": "",
  //     "screenshot": true,
  //     "shareType": 1,
  //   },
  //   // {
  //   //   "name": "Other Apps",
  //   //   "icon": "assests/share/others_share.png",
  //   //   "check_name": "",
  //   //   "package_name": "",
  //   //   "screenshot": true,
  //   //   "shareType": 1,
  //   // },
  // ];

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    printIfDebug("state chages" + state.toString() + state.index.toString());
    try {
      if (state.index == 0 && sharingFlag) {
        setState(() {
          sharingFlag = false;
        });
        closeSheet();
      }
    } catch (e) {
      printIfDebug(e);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    authenticationStatus();
    // getStaticMovieDetails();
    logUserScreen('Flixjini Detail Page', 'MovieDetailPage');
    checkShowOriginalPosters();
    shareReminderCheck();
  }

  checkShowOriginalPosters() async {
    if (widget.showOriginalPosters == null) {
      String showOriginalPostersFromFRC =
          await getStringFromFRC('show_original_posters');
      if (showOriginalPostersFromFRC == 'true') {
        setState(() {
          this.showOriginalPosters = true;
        });
      } else {
        setState(() {
          this.showOriginalPosters = false;
        });
      }
    } else {
      setState(() {
        showOriginalPosters = widget.showOriginalPosters;
      });
    }
  }

  Future<Uri> createDynamicLink(String shareText) async {
    try {
      String urlPhase = widget.urlInformation["urlPhase"];
      urlPhase = urlPhase
          .replaceAll("/entertainment", "https://www.flixjini.in")
          .replaceAll(".json", "");
      printIfDebug(urlPhase);
      // final DynamicLinkParameters parameters = DynamicLinkParameters(
      //   uriPrefix: "https://a.kmpr.in",
      //   link: Uri.parse(urlPhase),
      //   androidParameters: AndroidParameters(
      //     packageName: 'com.flixjini.watchonline',
      //     minimumVersion: 85,
      //     fallbackUrl: Uri.parse(""),
      //   ),
      //   dynamicLinkParametersOptions: DynamicLinkParametersOptions(
      //       shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short),
      //   socialMetaTagParameters: SocialMetaTagParameters(
      //     title: 'Flixjini',
      //     description: shareText,
      //   ),
      //   // iosParameters: IosParameters(
      //   //   bundleId: 'your_ios_bundle_identifier',
      //   //   minimumVersion: '1',
      //   //   appStoreId: 'your_app_store_id',
      //   // ),
      // );
      // final Uri dynamicUrl = await parameters.buildUrl();
      // printIfDebug(dynamicUrl.toString() + "\n\n");
      // final ShortDynamicLink shortenedLink =
      //     await DynamicLinkParameters.shortenUrl(
      //   dynamicUrl,
      //   DynamicLinkParametersOptions(),
      // );

      // final Uri shortUrl = shortenedLink.shortUrl;

      // printIfDebug(shortUrl.toString());

      // ShortDynamicLink shortDynamicLink = await parameters.buildShortLink();

      // printIfDebug("dynamic url");
      // printIfDebug(shortDynamicLink.shortUrl.toString() + "\n\n");

      return Uri.parse(""); //shortDynamicLink.shortUrl;
      // return dynamicUrl;
      // return Uri.parse("");
    } catch (e) {
      printIfDebug("Dynamic Link Issue " + e.toString());
      return Uri.parse("https://www.flixjini.in/");
    }
  }

  checkAppPresence() async {
    printIfDebug("appcheck ");

    for (int i = 0; i < shareSocial.length; i++) {
      bool appcheck =
          await checkIfAndroidAppIsInstalled(shareSocial[i]["check_name"]);
      if (appcheck || shareSocial[i]["check_name"].toString().length == 0) {
        shareSocial1.add(shareSocial[i]);
      }
      printIfDebug("appcheck " + appcheck.toString());
    }
    setState(() {
      shareSocial1 = shareSocial1;
    });
  }

  shareReminderCheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int reminderFlagCount = (prefs.getInt('shareReminderFlag') ?? 0);
    bool sharedFlag = (prefs.getBool('sharedFlag') ?? false);
    String shareAppInfoString = prefs.getString("shareAppInfo") ?? "[]";
    printIfDebug("shareAppInfoString " + shareAppInfoString);
    setState(() {
      shareSocial = jsonDecode(shareAppInfoString);
    });
    checkAppPresence();

    printIfDebug("share check");
    // checkReminderData();

    if ((reminderFlagCount == 5 || reminderFlagCount == 10) && !sharedFlag) {
      checkReminderData();
    } else {
      setState(() {
        showShareReminder = false;
      });
    }
    reminderFlagCount++;
    prefs.setInt('shareReminderFlag', reminderFlagCount);
    prefs.setBool('sharedFlag', sharedFlag);
  }

  setShareFlagSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('sharedFlag', true);
  }

  sendEvent() {
    Map<String, String> eventData = {
      'content': movieDetail["movieName"].toString(),
    };
    //platform.invokeMethod('logViewContentEvent', eventData);
  }

  updatePlayNotchPointer(String widgetName) {
    setState(() {
      playNotchPointer = widgetName;
    });
  }

  setStreamingSourceSelected(value) {
    printIfDebug("Yes value changed");
    printIfDebug(value);
    setState(() {
      streamingSourceSelected = value;
    });
  }

  authenticationStatus() async {
    printIfDebug("The url info");
    printIfDebug(widget.urlInformation);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Movie Detail Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      getUserQueuePreference();

      printIfDebug("User is already Logged in");
      if (!widget.urlInformation["urlPhase"].contains('.json')) {
        widget.urlInformation["urlPhase"] += ".json";
      }
      if (widget.urlInformation["urlPhase"] != null) {
        if (widget.urlInformation["search_key"] != null) {
          urlName = "https://api.flixjini.com" +
              widget.urlInformation["urlPhase"] +
              "?" +
              "jwt_token=" +
              authenticationToken +
              "&" +
              widget.urlInformation["search_key"] +
              "&";
        } else {
          urlName = "https://api.flixjini.com" +
              widget.urlInformation["urlPhase"] +
              "?" +
              "jwt_token=" +
              authenticationToken +
              "&";
        }
      } else {
        urlName = widget.urlInformation["fullUrl"] +
            "jwt_token=" +
            authenticationToken;
      }
    } else {
      printIfDebug("User is not Logged in");
      if (widget.urlInformation["urlPhase"] != null) {
        if (widget.urlInformation["search_key"] != null) {
          urlName = "https://api.flixjini.com" +
              widget.urlInformation["urlPhase"] +
              '?' +
              widget.urlInformation["search_key"] +
              "&";
        } else {
          urlName = "https://api.flixjini.com" +
              widget.urlInformation["urlPhase"] +
              '?';
        }
      } else {
        urlName = widget.urlInformation["fullUrl"];
      }
    }
    getMovieDetails();

    // getStaticMovieDetails();
  }

  getUserQueuePreference() async {
    printIfDebug("inside details queue fetch");
    String urlAuthentication =
        'https://api.flixjini.com/queue/list.json?jwt_token=' +
            authenticationToken;
    String url = await fetchDefaultParams(urlAuthentication);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        // printIfDebug("User Preference queue");
        // printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        getQueueIds(data["movies"]);

        setState(() {
          // fetchedUserQueuePreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  getQueueIds(data) {
    data.forEach((movie) {
      queueIds.add(movie["id"]);
    });

    setState(() {
      queueIds = this.queueIds;
    });
  }

  getStaticMovieDetails() async {
    // printIfDebug("inforrrrrrrrrrrrrrrrrrrrrrrr" +
    //     widget.urlInformation["urlPhase"].toString());
    var urlInfo = widget.urlInformation["urlPhase"].toString().split("/");
    String movieKey = urlInfo[3].toString().replaceAll(".json", "");
    String startUrl =
        !widget.urlInformation["urlPhase"].toString().contains("tvshow")
            ? "https://www.flixjini.in/movie/"
            : "https://www.flixjini.in/tvshow/";
    String url = startUrl + movieKey + "/routeInfo.json";
    bool requestFlag = false;
    // printIfDebug("Obtained Movie Url");
    // printIfDebug(urlName);
    // String rootUrl = urlName;
    // String url = constructHeader(rootUrl);
    // url = await fetchDefaultParams(url);

    printIfDebug("\n\nstatic details page api, url: $url");

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for movie details page: ' +
            response.statusCode.toString() +
            "for url " +
            url,
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.movieDetail = data["data"]["staticData"];
        setState(() {
          movieDetail = this.movieDetail;
          fetchedMovieDetail = true;
        });
        // printIfDebug("Data Reviews - Movie Detail Page");
        // printIfDebug(movieDetail.toString());
        // printIfDebug(movieDetail["stats"]);
        // printIfDebug(movieDetail["tvShows"]);
        requestFlag = true;
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('\n\ndetails page api, exception caught is: $exception');
    }
    // getMovieDetails();

    if (!mounted) return;
    if (!requestFlag || movieDetail == null || movieDetail.length < 1) {
      setState(() {
        ipError = true;
      });
    } else {
      setState(() {
        ipError = false;
      });
    }
  }

  getMovieAllDetails() {
    getMovieTopDetails();
    getMovieDetails();
  }

  getMovieTopDetails() async {
    bool requestFlag = true;
    printIfDebug("Obtained Movie Url");
    printIfDebug(urlName);

    String rootUrl = urlName;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    url = url
        .replaceAll("/entertainment/movie/", "/entertainment/topload/")
        .replaceAll("/entertainment/tvshow/", "/entertainment/topload/");
    printIfDebug("\n\ndynamic details top page api, url: $url");

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for movie top details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        if (!fetchedMovieDetail) {
          this.movieDetail = data;
          setState(() {
            movieTopDetail = this.movieDetail;
            fetchedMovieDetail = true;
            fetchedMovieTopDetail = true;
          });
          sendEvent();
        }

        // printIfDebug("Data Reviews - Movie Detail Page");
        // printIfDebug(movieDetail.toString());
        // printIfDebug(movieDetail["stats"]);
        // printIfDebug(movieDetail["tvShows"]);
        requestFlag = true;
      } else {
        requestFlag = false;
        // getStaticMovieDetails();
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      // getStaticMovieDetails();

      printIfDebug('\n\ndetails page top api, exception caught is: $exception');
    }
    if (!mounted) return;
    if (!requestFlag || movieDetail == null || movieDetail.length < 1) {
      // setState(() {
      //   ipError = true;
      // });
    } else {
      setState(() {
        ipError = false;
      });
    }
  }

  getMovieDetails() async {
    if (widget.fromGenie == null || !widget.fromGenie) {
      getMovieTopDetails();
    }

    bool requestFlag = true;
    printIfDebug("Obtained Movie Url");
    printIfDebug(urlName);

    String rootUrl = urlName;
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);

    printIfDebug("\n\ndynamic details page api, url: $url");

    try {
      var response = await http.get(Uri.parse(url));
      printIfDebug(
        '\n\njson response status code for movie details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.movieDetail = data;
        setState(() {
          movieDetail = this.movieDetail;
          fetchedMovieDetail = true;
        });
        sendEvent();

        // printIfDebug("Data Reviews - Movie Detail Page");
        // printIfDebug(movieDetail.toString());
        // printIfDebug(movieDetail["stats"]);
        // printIfDebug(movieDetail["tvShows"]);
        requestFlag = true;
      } else {
        requestFlag = false;
        getStaticMovieDetails();
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      getStaticMovieDetails();

      printIfDebug('\n\ndetails page api, exception caught is: $exception');
    }
    if (!mounted) return;
    if (!requestFlag || movieDetail == null || movieDetail.length < 1) {
      // setState(() {
      //   ipError = true;
      // });
    } else {
      setState(() {
        ipError = false;
      });
    }
  }

  void tooglePreference(entity) {
    setState(() {
      movieDetail[entity] = movieDetail[entity] ? false : true;
    });
    if ((entity == "like") || (entity == "dislike")) {
      String opposite = (entity == "like") ? "dislike" : "like";
      if (movieDetail[entity] && movieDetail[opposite]) {
        setState(() {
          movieDetail[opposite] = false;
        });
      }
    }
    printIfDebug(movieDetail[entity]);
  }

  removeMovieFromWatchList(actionUrl) async {
    String url = actionUrl;
    printIfDebug(url);
    url = await fetchDefaultParams(url);

    try {
      var response = await http.post(Uri.parse(url));
      printIfDebug("Response status: ${response.statusCode}");
      printIfDebug("Response body: ${response.body}");
      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          printIfDebug("Authentication Successful and no Errors");
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  sendRequest(actionUrl, entity, movie, [int val = 0]) async {
    bool flag = true;
    String toastMsg = "Added to Queue";
    if (entity == "seen") {
      toastMsg = "Added to Seen List";
    } else if (entity == "like") {
      toastMsg = movieDetail[entity] ? "Removed Like" : "Movie liked";
    } else if (entity == "dislike") {
      toastMsg = movieDetail[entity] ? "Removed Dislike" : "Movie disliked";
    } else if (entity == "reset_seen") {
      entity = "seen";
      toastMsg = "Removed from Seen List";
    }
    if (movie == null) {
      if (entity == "queue") {
        flag = movieDetail[entity] ? false : true;
      }

      tooglePreference(entity);
      if (flag) {
        Map data;
        printIfDebug(actionUrl);
        if (val == 0) {
          data = {
            'jwt_token': authenticationToken,
            'movie_id': movieDetail["id"],
          };
        } else {
          data = {
            'jwt_token': authenticationToken,
            'movie_id': movieDetail["id"],
            'entity': "watched_movie",
          };
        }
        printIfDebug(data);
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              printIfDebug("Authentication Successful and no Errors");
              showToast(toastMsg, 'long', 'bottom', 1, Constants.appColorLogo);
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movieDetail["id"] +
            '&jwt_token=' +
            authenticationToken;
        printIfDebug(actionUrl);
        removeMovieFromWatchList(actionUrl);
      }
    } else {
      if (entity == "queue") {
        flag = movie[entity] ? false : true;
      }
      if (flag) {
        printIfDebug(actionUrl);
        Map data = {
          'jwt_token': authenticationToken,
          'movie_id': movie["similarMovieId"].toString(),
        };
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              printIfDebug("Authentication Successful and no Errors");

              showToast('Added to Queue', 'long', 'bottom', 1,
                  Constants.appColorLogo);
            } else {
              printIfDebug("Could not authorise user / Some error occured");
              showToast('Please try again later', 'long', 'bottom', 1,
                  Constants.appColorLogo);
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["similarMovieId"].toString() +
            '&jwt_token=' +
            authenticationToken;
        printIfDebug(actionUrl);
        removeMovieFromWatchList(actionUrl);

        showToast(
            'Removed from Queue', 'long', 'bottom', 1, Constants.appColorLogo);
      }
    }
  }

  updateDidNotWatch() async {
    Map data = {
      'jwt_token': authenticationToken,
      'movie_id': movieDetail["id"],
      'entity': "watched_movie",
    };
    var url = "https://api.flixjini.com/queue/did_not_watch.json";
    url = await fetchDefaultParams(url);
    try {
      http.post(Uri.parse(url), body: data).then((response) {
        printIfDebug("Response status: ${response.statusCode}");
        printIfDebug("Response body: ${response.body}");
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug(jsonResponse);
        if (response.statusCode == 200) {
          if (jsonResponse["login"] && !jsonResponse["error"]) {
            printIfDebug("Authentication Successful and no Errors");
            showToast("Movie Watch Status Updated", 'long', 'bottom', 1,
                Constants.appColorLogo);
          } else {
            printIfDebug("Could not authorise user / Some error occured");
          }
        } else {
          printIfDebug("Non sucessesful response from server");
        }
      });
    } catch (e) {
      printIfDebug(e.toString());
    }
  }

  updateSelectedSeasonEpisodes(var data, int chosenSeason) {
    setState(() {
      movieDetail["tvShows"]["seasons"][chosenSeason]["episodes"] = data;
    });
  }

  setBookNowLocation(locationBasedSources) {
    setState(() {
      movieDetail["bookTickets"] = locationBasedSources;
    });
    printIfDebug("Updated Location");
    printIfDebug(movieDetail["bookTickets"]);
  }

  bool nullChecker(String key) {
    if (movieDetail.containsKey(key)) {
      if (movieDetail[key].isEmpty) {
        printIfDebug("Empty " + key);
        return false;
      } else {
        return true;
      }
    } else {
      printIfDebug("No " + key);
      return false;
    }
  }

  Widget getMovieRating() {
    return Container(
      child: MovieDetailRating(
        movieDetail: movieDetail,
        // movieDataFromCall: widget.movieDataFromCall ?? {},
        showOriginalPoster: true,
        cardFor: 0,
      ),
      // margin: EdgeInsets.only(
      //   top: 5,
      // ),
    );
  }

  bool nullCheckerWith2Keys(String key1, String key2) {
    if (movieDetail.containsKey(key1)) {
      if (movieDetail[key1].containsKey(key2)) {
        if (movieDetail[key1][key2].isEmpty) {
          printIfDebug("Empty " + key2);
          return false;
        } else {
          return true;
        }
      } else {
        printIfDebug("No " + key2);
        return false;
      }
    } else {
      printIfDebug("No " + key1);
      return false;
    }
  }

  setStreamingStatus(bool streamingStatusCheck) {
    setState(() {
      streamingStatus = streamingStatusCheck;
    });
  }

  renderSections() {
    ratingsFlag = nullCheckerWith2Keys("ratings", "scorer");
    bool streamingSourcesFlag =
        nullCheckerWith2Keys("watchNow", "streamingSources");
    bool aboutFlag = nullChecker("movieIntro");
    bool bookNowFlag = nullChecker("bookTickets");
    // bool factFlag = nullChecker("quickFacts");
    bool simalarFlag = nullCheckerWith2Keys("similarMovies", "movies");
    bool plotFlag = nullChecker("movieStory");
    bool castPersonsFlag = nullCheckerWith2Keys("castAndCrew", "person");
    bool awardsPrizesFlag = nullCheckerWith2Keys("awards", "prizes");
    bool galleryTracksFlag = nullCheckerWith2Keys("gallary", "tracks");
    bool galleryVideosFlag = nullCheckerWith2Keys("gallary", "videos");
    bool galleryPostersFlag = nullCheckerWith2Keys("gallary", "posters");
    bool galleryTagsFlag = nullCheckerWith2Keys("tags", "cards");
    bool reviewSectionFlag = nullChecker("movieReview");
    bool tvShowFlag = nullChecker("tvShows");
    bool watchedFlag = watchedFlagSetter();

    if (streamingSourcesFlag || bookNowFlag) {
      setStreamingStatus(true);
    } else {
      setStreamingStatus(false);
    }
    var sections = <Widget>[];
    if (watchedFlag) {
      sections.add(getWatchedBox());
    }
    if (ratingsFlag && showOriginalPosters) {
      sections.add(getMovieRating());
      // sections.add(insertDivider());
    }
    if (movieDetail["movieSummary"] != null &&
        movieDetail["movieSummary"].toString().length > 0) {
      sections.add(
        getMovieSummary(),
      );
      sections.add(
        insertHalfDivider(),
      );
    }
    if (streamingSourcesFlag) {
      updatePlayNotchPointer("watchNow");
      sections.add(
        new MovieDetailStreamingSources(
            movieDetail: movieDetail,
            watchNowKey: watchNowKey,
            authenticationToken: authenticationToken,
            setStreamingSourceSelected: setStreamingSourceSelected),
      );
      sections.add(insertDivider());
    } else {
      sections.add(noSourceBox());
      sections.add(insertDivider());
    }
    if (bookNowFlag) {
      if (playNotchPointer.isEmpty) {
        updatePlayNotchPointer("bookNow");
      }
      sections.add(
        new MovieDetailBookNow(
            movieId: movieDetail["id"],
            bookTickets: movieDetail["bookTickets"],
            setBookNowLocation: setBookNowLocation,
            bookNowKey: bookNowKey),
      );
      sections.add(insertDivider());
    }

    if (tvShowFlag && movieDetail["tvShows"]["seasons"].length > 0) {
      sections.add(
        new MovieDetailTvShowSection(
            tvShowSection: movieDetail["tvShows"],
            authenticationToken: authenticationToken,
            urlPhase: widget.urlInformation["urlPhase"],
            updateSelectedSeasonEpisodes: updateSelectedSeasonEpisodes),
      );
      sections.add(
        insertDivider(),
      );
    }

    if (reviewSectionFlag) {
      sections.add(
        new MovieDetailReviewSection(
            reviewSection: movieDetail["movieReview"],
            id: movieDetail["id"],
            authenticationToken: authenticationToken,
            urlPhase: widget.urlInformation["urlPhase"]),
      );
      sections.add(insertDivider());
    }
    // if (aboutFlag) {
    //   sections.add(
    //     new MovieDetailAboutMovie(movieDetail: movieDetail),
    //   );
    //   sections.add(insertDivider());
    // }
    if (plotFlag) {
      sections.add(
        new MovieDetailPlot(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }

    // if (factFlag) {
    //   sections.add(new MovieDetailQuickFacts(movieDetail: movieDetail));
    // }
    if (simalarFlag) {
      sections.add(
        new MovieDetailSimilarMovies(
          movieDetail: movieDetail,
          showOriginalPosters: showOriginalPosters,
          authenticationToken: authenticationToken,
          sendRequest: sendRequest,
          queueIds: queueIds,
        ),
      );
      sections.add(insertDivider());
    }
    // if (plotFlag) {
    //   sections.add(
    //     new MovieDetailPlot(movieDetail: movieDetail),
    //   );
    //   sections.add(insertDivider());
    // }
    if (aboutFlag) {
      sections.add(
        new MovieDetailAboutMovie(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }

    if (castPersonsFlag) {
      sections.add(
        new MovieDetailCastGallery(
            movieDetail: movieDetail,
            showOriginalPosters: widget.showOriginalPosters),
      );
      sections.add(insertDivider());
    }
    if (awardsPrizesFlag) {
      sections.add(
        new MovieDetailAwards(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }
    if (galleryTracksFlag) {
      sections.add(
        new MovieDetailTracks(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }
    if (galleryVideosFlag) {
      sections.add(
        new MovieDetailVideos(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }
    if (galleryPostersFlag) {
      sections.add(
        new MovieDetailMovieGallery(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }
    if (galleryTagsFlag) {
      sections.add(
        new MovieDetailTags(movieDetail: movieDetail),
      );
      sections.add(insertDivider());
    }
    sections.add(reportBug());
    sections.add(
      new Container(
          // height: 20.0,
          ),
    );

    sections.add(
      getContainerForTMDBLogo(),
    );

    sections.add(
      new Container(
        height: 20.0,
      ),
    );
    return sections;
  }

  Widget reportBug() {
    return GestureDetector(
      onTap: () {
        changeFeedback(true);
      },
      child: Card(
        // elevation: 6.0,
        color: Constants.appColorL2,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new Container(
          width: 125.0,
          height: 30.0,
          // padding: const EdgeInsets.all(2.0),
          child: new Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: 5,
                  ),
                  child: Image.asset(
                    "assests/rab.png",
                    height: 18,
                    width: 18,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 5,
                    right: 10,
                    bottom: 5,
                  ),
                  child: Text(
                    "Report a bug",
                    style: TextStyle(
                      color: Constants.appColorFont
                          .withOpacity(0.7)
                          .withOpacity(0.85),
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool watchedFlagSetter() {
    bool flag1 = movieDetail["like"] ?? false;
    bool flag2 = movieDetail["dislike"] ?? false;
    bool flag3 = movieDetail["watched_movie"] ?? false;
    // printIfDebug("flags\\\\\\\\" +
    //     flag1.toString() +
    //     flag2.toString() +
    //     flag3.toString());
    if (!flag1 && !flag2 && flag3) {
      return true;
    } else {
      return false;
    }
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 16.0,
        left: 10,
        right: 10,
      ),
      child: SizedBox(
        height: 2.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget noSourceBox() {
    var sourceImage = new Image.asset(
      "assests/na.png",
      fit: BoxFit.contain,
    );
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.all(
          new Radius.circular(5.0),
        ),
        color: Constants.appColorL2,
      ),
      padding: EdgeInsets.only(
        left: 15,
        top: 5,
        bottom: 5,
        right: 5,
      ),
      // height: 70.0,
      width: screenWidth - 20,
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 65,
            width: 65,
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color: Constants.appColorL2,
              borderRadius: new BorderRadius.all(
                new Radius.circular(10.0),
              ),
              border: Border.all(
                color: Constants.appColorDivider,
                width: 1.0,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  // color: Colors,
                  height: 40,
                  width: 40,
                  child: GestureDetector(
                    child: sourceImage,
                    onTap: () {},
                  ),
                ),
              ],
            ),
          ),
          new Expanded(
            child: new Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(
                      "Not available in any streaming sources in your country",
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 10,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget insertHalfDivider() {
    return new Container(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 16.0,
        left: 70,
        right: 70,
      ),
      child: SizedBox(
        height: 2.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget getWatchedBox() {
    return Container(
      child: MovieDetailWatched(
        movieDetail: movieDetail,
        sendRequest: sendRequest,
        updateDidNotWatch: updateDidNotWatch,
        changeFeedback: changeFeedback,
      ),
    );
  }

  Widget showFeedbackOverlay() {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    String urlFeedback =
        "https://api.flixjini.com" + widget.urlInformation["urlPhase"];
    urlFeedback = urlFeedback.replaceAll("api.", "www.");
    urlFeedback = urlFeedback.replaceAll(".json", "");
    urlFeedback = urlFeedback.replaceAll(".com/entertainment", ".in");
    // printIfDebug(urlFeedback);
    return Stack(
      children: <Widget>[
        Container(
          color: Constants.appColorL1.withOpacity(0.9),
          width: screenWidth,
          height: screenHeight,
        ),
        FeedbackOverlay(
          changeFeedback: changeFeedback,
          authenticationToken: authenticationToken,
          url: urlFeedback,
          textFlag: true,
        ),
      ],
    );
  }

  changeFeedback(flag) {
    setState(() {
      showFeedback = flag;
    });
  }

  Widget getMovieSummary() {
    // printIfDebug(widget.movieDataFromCall);
    String summaryString =
        movieDetail["movieSummary"] != null ? movieDetail["movieSummary"] : "";
    if (summaryString.isEmpty) {
      summaryString = widget.movieDataFromCall["intro"] != null
          ? widget.movieDataFromCall["intro"]
          : "";
    }
    return Container(
      padding: EdgeInsets.only(
        left: 5,
        right: 5,
        bottom: 10,
      ),
      child: Text(
        summaryString,
        style: TextStyle(
          color: Constants.appColorFont.withOpacity(0.6),
          fontSize: 12,
        ),
        maxLines: 3,
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget displayMovieDetails() {
    return Container(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
      ),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: renderSections(),
      ),
    );
  }

  Widget loadingSection() {
    return Container(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
        // bottom: 10,
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
              bottom: 25,
            ),
            child: MovieDetailRating(
              movieDetail: movieDetail,
              movieDataFromCall: widget.movieDataFromCall ?? {},
              showOriginalPoster: true,
              fromGenie: widget.fromGenie,
              cardFor: 0,
            ),
          ),
          getMovieSummary(),
          insertHalfDivider(),
          tempSourcesBox(),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: getProgressBarWithLoading(),
          )
        ],
      ),
    );
  }

  Widget tempSourcesBox() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
              // bottom: 14,
              ),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // insertDividerVertical(),
              Expanded(
                // flex: 6,
                child: Container(
                  margin: EdgeInsets.only(
                      // let: 10,
                      ),
                  padding: EdgeInsets.only(
                    top: 5,
                    left: 8,
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        width: 4.0,
                        color: Constants.appColorLogo,
                      ),
                    ),
                  ),
                  child: Text(
                    "WATCH NOW",
                    textAlign: TextAlign.left,
                    style: new TextStyle(
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.normal,
                      color: Constants.appColorFont,
                      fontSize: 19.0,
                    ),
                    maxLines: 2,
                  ),
                ),
              ),
            ],
          ),
        ),
        tempSourcesList(),
      ],
    );
  }

  Widget tempSourcesList() {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: widget.movieDataFromCall["sources"].length,
        itemBuilder: (BuildContext context, int index) {
          return eachTempSourceCard(index);
        },
      ),
    );
  }

  eachTempSourceCard(int index) {
    return Opacity(
      opacity: 0.60,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.all(
            new Radius.circular(5.0),
          ),
          color: Constants.appColorL2,
        ),
        padding: EdgeInsets.only(
          left: 5,
          // top: 5,
          // bottom: 5,
          right: 10,
        ),
        margin: EdgeInsets.only(
          bottom: 10,
        ),
        height: 80.0,
        width: 330.0,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            new Expanded(
              child: Center(
                child: Container(
                  height: 65,
                  width: 65,
                  decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Constants.appColorL2,
                    borderRadius: new BorderRadius.all(
                      new Radius.circular(10.0),
                    ),
                    border: Border.all(
                      color: Constants.appColorDivider,
                      width: 1.0,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        // color: Colors,
                        height: 40,
                        width: 40,
                        child: GestureDetector(
                          child: Image.network(
                              "https://c.kmpr.in/assets/streaming-sources/" +
                                  widget.movieDataFromCall["sources"][index]
                                      .toString()
                                      .toLowerCase()
                                      .replaceAll(" ", "") +
                                  ".png"),
                          onTap: () {},
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              flex: 1,
            ),
            // new Expanded(
            //   child: new Container(
            //     padding: const EdgeInsets.all(10.0),
            //     child: new Text(
            //       widget.movieDetail["watchNow"]["streamingSources"][index]
            //                   ["streamingSourcesLanguage"]
            //               .toString()
            //               .isNotEmpty
            //           ? widget.movieDetail["watchNow"]["streamingSources"][index]
            //               ["streamingSourcesLanguage"]
            //           : "",
            //       textAlign: TextAlign.right,
            //       style: new TextStyle(
            //         fontWeight: FontWeight.bold,
            //         color: Constants.appColorFont,
            //         fontSize: 10.0,
            //       ),
            //       maxLines: 2,
            //       overflow: TextOverflow.ellipsis,
            //     ),
            //   ),
            //   flex: 1,
            // ),
            // Spacer(),
            new Expanded(
              child: new Container(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Container(
                              // padding: EdgeInsets.only(
                              //   left: 5,
                              //   right: 5,
                              // ),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: Text(
                                      widget.movieDataFromCall["sources"]
                                              [index] ??
                                          "",
                                      style: TextStyle(
                                        color: Constants.appColorFont,
                                        fontSize: 12,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          // Spacer(),
                          Expanded(
                            flex: 2,
                            child: Container(
                              // padding: EdgeInsets.only(
                              //   left: 20,
                              // ),
                              child: Center(
                                child: new FlatButton(
                                  color: Constants.appColorL2,
                                  textColor: Constants.appColorDivider,
                                  child: new Text(
                                    "WATCH",
                                    overflow: TextOverflow.ellipsis,
                                    style: new TextStyle(
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                    softWrap: true,
                                  ),
                                  onPressed: () {},
                                  shape: new RoundedRectangleBorder(
                                    side: BorderSide(
                                      color: Constants.appColorDivider,
                                    ),
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              flex: 3,
            ),
          ],
        ),
        // ),
        // ),
        // ),
        // ),
        // ),
        // ),
      ),
    );
  }

  Widget displayLoader() {
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      body: new Center(
        child: ShimmerIndex(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: withOrWithoutNotch(),
      bottomNavigationBar: withOrWithoutFresh(),
    );
  }

  AppBar? appHeaderBar() {
    return AppBar(
      elevation: 4.0,
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      title: new Text("",
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
            fontWeight: FontWeight.normal,
            color: Constants.appColorFont,
          ),
          textAlign: TextAlign.center,
          maxLines: 1),
    );
  }

  Widget movieNotFound() {
    String message =
        "We could not load this page. This may be because you are not connected to the internet or our server is having issues. Please check your internet connection and be rest assured we will resolve any server issues as quick as we can";
    String imagePath = "assests/no_movie.png";
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      appBar: appHeaderBar(),
      body: new Center(
        child: new ErrorHandlePage(
          message: message,
          imagePath: imagePath,
          refresh: getMovieDetails,
        ),
      ),
    );
  }

  scrollToWidget(GlobalKey value) {
    Scrollable.ensureVisible(
      value.currentContext!,
      alignment: 1.0,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  moveToWatchNowOrBookNow() {
    if (playNotchPointer.isEmpty) {
      printIfDebug("No Watch Now or Book Now");
    } else {
      if (playNotchPointer == "watchNow") {
        scrollToWidget(watchNowKey);
      } else if (playNotchPointer == "bookNow") {
        scrollToWidget(bookNowKey);
      } else {
        printIfDebug(
            "Something Went Wrong -> Didn't Scroll to Both watch now or book now");
      }
    }
  }

  Widget customFloatingButton() {
    return Container(
      height: 75.0,
      width: 75.0,
      // color: Constants.appColorL1,
      child: FittedBox(
        child: FloatingActionButton(
          child: new Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Constants.appColorLogo,
              // boxShadow: [
              //   BoxShadow(
              //     color: Constants.appColorShadow,
              //   )
              // ],
            ),
            padding: const EdgeInsets.all(5.0),
            width: 40.0,
            height: 40.0,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // new Expanded(
                //   child: new Container(
                //     padding: const EdgeInsets.only(left: 3.0, bottom: 3.0),
                //     child: new Image.asset(
                //       "assests/play.png",
                //       fit: BoxFit.cover,
                //       color: Constants.appColorFont,
                //     ),
                //   ),
                //   flex: 2,
                // ),
                new Expanded(
                  child: new Container(
                    // padding: const EdgeInsets.all(1.0),
                    child: new Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                              // top: 5,
                              left: 5,
                            ),
                            child: Image.asset(
                              "assests/play_app.png",
                              // fit: BoxFit.c,
                              height: 25,
                              width: 25,
                            ),
                          ),
                          // Container(
                          //   margin: EdgeInsets.only(
                          //     top: 5,
                          //     // left: 5,
                          //   ),
                          //   child: new Text(
                          //     "WATCH",
                          //     style: new TextStyle(
                          //       fontStyle: FontStyle.normal,
                          //       color: Constants.appColorFont,
                          //       fontSize: 6.5,
                          //     ),
                          //     textAlign: TextAlign.center,
                          //     maxLines: 3,
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
          onPressed: () {
            printIfDebug("Go to Streaming Sources");
            if (playNotchPointer.isNotEmpty) {
              moveToWatchNowOrBookNow();
            }
          },
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }

  withOrWithoutNotch() {
    printIfDebug("\n\nNotch Status: $playNotchPointer");
    if (playNotchPointer.isNotEmpty || true) {
      return customFloatingButton();
    } else {
      return new Container();
    }
  }

  withOrWithoutFresh() {
    printIfDebug("Fresh Status");
    printIfDebug(playNotchPointer);
    if (playNotchPointer.isNotEmpty || true) {
      return new MovieNavigatorWithNotch();
    } else {
      // return new MovieNavigator();
    }
  }

  Widget checkToDisplaySourceLoader() {
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      body: new Center(
        child: new CircularProgressIndicator(
          backgroundColor: Constants.appColorLogo,
          valueColor: new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
        ),
      ),
    );
  }

  Widget getCollapsableAppbar(bool flag) {
    return MovieDetailCollapsableAppbar(
      movieDetail: movieDetail,
      authenticationToken: authenticationToken,
      sendRequest: sendRequest,
      urlName: urlName,
      urlPhase: widget.urlInformation["urlPhase"],
      moveToWatchNowOrBookNow: moveToWatchNowOrBookNow,
      streamingStatus: streamingStatus,
      ratingsFlag: ratingsFlag,
      notification: widget.notification,
      movieDataFromCall: flag ? null : widget.movieDataFromCall,
      showOriginalPosters: showOriginalPosters,
      showBottomSheet: showBottomSheet,
    );
  }

  Widget getNestedScrollView(double screenWidth, bool flag) {
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          getCollapsableAppbar(flag),
        ];
      },
      body: new Container(
        width: screenWidth,
        color: Constants.appColorL1,
        child: new SingleChildScrollView(
          child: new ConstrainedBox(
            constraints: new BoxConstraints(),
            child: new Stack(
              children: <Widget>[
                flag
                    ? displayMovieDetails()
                    : (widget.fromGenie != null && widget.fromGenie
                        ? loadingSection()
                        : Container(
                            margin: EdgeInsets.only(top: 100),
                            child: getProgressBarWithLoading(),
                          )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget displayMoviePage(bool flag) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      resizeToAvoidBottomInset: false,
      body: getNestedScrollView(screenWidth, flag),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: withOrWithoutNotch(),
      bottomNavigationBar: withOrWithoutFresh(),
    );
  }

  Widget checkDataReady() {
    try {
      if (ipError) {
        printIfDebug("ip errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        return movieNotFound();
      } else {
        if ((fetchedMovieDetail == true &&
            movieDetail.isNotEmpty &&
            movieDetail.length > 1)) {
          if (streamingSourceSelected) {
            return checkToDisplaySourceLoader();
          } else {
            return displayMoviePage(true);
          }
        } else if ((movieDetail == null || movieDetail.length == 0) &&
            fetchedMovieDetail == true) {
          printIfDebug("movieDetail.isEmpty /////////////////");

          return movieNotFound();
        } else {
          printIfDebug("shakthi else");
          return widget.movieDataFromCall != null &&
                  widget.movieDataFromCall.isNotEmpty
              ? displayMoviePage(false)
              : displayLoader();
          // return displayLoader();
        }
      }
    } catch (exception) {
      // return displayMoviePage(true);
      printIfDebug("MovieDetail exception" + exception.toString());
      return displayLoader();
    }
  }

  changeShareType(int type, bool flag) {
    setState(() {
      shareType = type;
      sharingFlag = flag;
    });
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        backgroundColor: Constants.appColorL2,
        elevation: 2.5,
        context: context,
        isScrollControlled: true,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return ShareSheet(
              shareSocial1: shareSocial1,
              sharingFlag: sharingFlag,
              changeShareType: changeShareType,
              takeScreenShot: takeScreenShot,
              showBottomSheet: showBottomSheet,
            );
          });
        });
  }

  takeScreenShot(int index) async {
    if (!shareSocial1[index]["screenshot"]) {
      String shareText = "";
      if (widget.urlInformation["urlPhase"].toString().contains("tvshow")) {
        shareText = await getStringFromFRC('sharetextfortvshow');
      } else {
        shareText = await getStringFromFRC('sharetextformovie');
      }
      var shareTextList = shareText.split("|");
      printIfDebug("shareText" + shareText);
      String content = shareTextList[0] != null ? shareTextList[0] : "";
      content += movieDetail["movieName"];
      content += shareTextList[1] != null ? shareTextList[1] : "";
      Uri dynamicUrl;
      dynamicUrl = await createDynamicLink(content);
      content += "\n\n" + dynamicUrl.toString();
      if (shareSocial1[index]["shareType"] == 2) {
        // Share.text("Flixjini", content, "text/plain");
      } else if (shareSocial1[index]["shareType"] == 3) {
        showToast(
          "Link Copied to Clipboard!",
          'long',
          'bottom',
          1,
          Constants.appColorLogo,
        );

        Clipboard.setData(
          new ClipboardData(text: content),
        );
      }

      closeSheet();
      setState(() {
        sharingFlag = false;
      });
    } else {
      String grantedWritePermission = "";
      // await platform.invokeMethod('checkWriteStoragePermission');
      if (grantedWritePermission == "true") {
        setShareFlagSP();
        try {
          RenderRepaintBoundary? boundary = previewContainer.currentContext!
              .findRenderObject() as RenderRepaintBoundary?;

          double pixelRatio = originalSize / MediaQuery.of(context).size.width;
          ui.Image image = await boundary!.toImage(pixelRatio: pixelRatio);

          ByteData? byteData =
              await image.toByteData(format: ui.ImageByteFormat.png);
          final Uint8List list = byteData!.buffer.asUint8List();
          // Bitmap bitmap = await Bitmap.fromHeadful(200, 300, list);

          printIfDebug("image share");
          late final result;
          try {
            // result = await ImageGallerySaver.saveImage(
            //     byteData.buffer.asUint8List());
          } catch (e) {
            printIfDebug("on error" + e.toString());
          }

          printIfDebug("scheck");
          // printIfDebug("result" + result.toString());

          String shareText = "";
          if (widget.urlInformation["urlPhase"].toString().contains("tvshow")) {
            shareText = await getStringFromFRC('sharetextfortvshow');
          } else {
            shareText = await getStringFromFRC('sharetextformovie');
          }
          var shareTextList = shareText.split("|");
          printIfDebug("shareText" + shareText);
          String content = shareTextList[0] != null ? shareTextList[0] : "";
          content += movieDetail["movieName"];
          content += shareTextList[1] != null ? shareTextList[1] : "";
          Uri dynamicUrl;
          dynamicUrl = await createDynamicLink(content);
          content += "\n\n" + dynamicUrl.toString();
          if (true) {
            //result.toString().isEmpty) {
            showToast("Some Error has occured", 'long', 'bottom', 1,
                Constants.appColorLogo);
            if (sharingFlag) {
              setState(() {
                sharingFlag = false;
              });
              closeSheet();

              // Share.file(
              //   "Flixjini",
              //   movieDetail["movieName"] + ".png",
              //   list,
              //   "image/png",
              //   text: content,
              // );
            }
          }
          if (shareSocial1[index]["check_name"].toString().length == 0) {
            // Share.file(
            //   "Flixjini",
            //   movieDetail["movieName"] + ".png",
            //   list,
            //   "image/png",
            //   text: content,
            // );
          } else {
            // +
            // "\n\n" +
            // "Discover the best streaming content using Flixjini " +
            // "https://moviesapp.page.link/home";
            printIfDebug("content " + content);
            // Map<String, String> shareData = {
            //   'path': result.toString(),
            //   'application': shareSocial1[index]["package_name"],
            //   'content': content,
            //   "dynamicUrl": dynamicUrl.toString(),
            // };
            String checkDone = "";
            // //platform.invokeMethod('shareToSocialMedia', shareData);

            if (checkDone == "true") {
              setState(() {
                // sharingFlag = false;
              });
            } else {}
          }
        } catch (e) {
          printIfDebug("takescreenshot" + e.toString());
        }
      } else {
        closeSheet();
        setState(() {
          sharingFlag = false;
        });
      }
    }
  }

  closeSheet() {
    Navigator.pop(context);
  }

  loadingBottomSheet() {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          height: 200,
          color: Constants.appColorL1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              getProgressBar(),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                ),
                child: Text(
                  "Preparing Share...",
                  style: TextStyle(
                      color: Color(
                    0XFF43C681,
                  )),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  showBottomSheet(bool flag) {
    if (flag) {
      _settingModalBottomSheet(context);
    } else {
      closeSheet();
      loadingBottomSheet();

      Future.delayed(const Duration(milliseconds: 5000), () {
        if (sharingFlag) {
          setState(() {
            sharingFlag = false;
          });
          closeSheet();
        }
      });
      // loadingBottomSheet();
    }
  }

  checkReminderData() async {
    String reminderDataFromFRC = await getStringFromFRC('share_reminder_data');
    printIfDebug(reminderDataFromFRC);
    var reminderData1 = {};
    reminderData1 = json.decode(reminderDataFromFRC);

    setState(() {
      reminderData = reminderData1;
    });
    Future.delayed(const Duration(milliseconds: 2000), () {
      setState(() {
        showShareReminder = true;
      });
    });
  }

  Widget shareReminder() {
    double screenheight = MediaQuery.of(context).size.height;
    double screenwidth = MediaQuery.of(context).size.width;
    return Stack(
      alignment: AlignmentDirectional.center,
      children: <Widget>[
        Container(
          height: screenheight,
          width: screenwidth,
          color: Constants.appColorL1.withOpacity(
            0.9,
          ),
        ),
        Container(
          // margin: EdgeInsets.only(
          //   left: 20,
          //   right: 20,
          // ),
          // padding: EdgeInsets.all(10),
          height: screenheight / 1.75,
          width: screenwidth,
          child: Stack(
            alignment: AlignmentDirectional.topEnd,
            // mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  left: 20,
                  right: 20,
                  top: 10,
                ),
                padding: EdgeInsets.only(
                  top: 15,
                ),
                decoration: BoxDecoration(
                  color: Constants.appColorL1,
                  // border: Border.all(
                  //   color: Color(
                  //     0xFF777777,
                  //   ),
                  //   width: 1.0,
                  // ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                        top: 10,
                      ),
                      padding: EdgeInsets.all(5),
                      child: Text(
                        reminderData["text2"],
                        style: TextStyle(
                          color: Constants.appColorFont,
                          fontSize: 16,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Constants.appColorL2,
                          borderRadius: BorderRadius.circular(10)),
                      margin: EdgeInsets.only(
                        left: 10,
                        right: 10,
                      ),
                      padding: EdgeInsets.all(10),
                      height: screenheight / 4,
                      width: screenwidth,
                      child: Image.network(
                        reminderData["image_url_new"],
                        fit: BoxFit.contain,
                        // height: 100,
                        // width: 100,
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        // Container(
                        //   margin: EdgeInsets.only(
                        //     top: 10,
                        //   ),
                        //   padding: EdgeInsets.all(5),
                        //   child: Text(
                        //     reminderData["text1"],
                        //     style: TextStyle(
                        //       color: Constants.appColorFont,
                        //       fontSize: 16,
                        //     ),
                        //     textAlign: TextAlign.center,
                        //   ),
                        // ),
                        Container(
                          // margin: EdgeInsets.only(
                          //   top: 10,
                          // ),
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(
                            left: 5,
                            right: 5,
                          ),
                          child: Text(
                            reminderData["text1"],
                            style: TextStyle(
                              color: Constants.appColorFont,
                              fontSize: 12,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {
                        try {
                          setState(() {
                            showShareReminder = false;
                          });
                          showBottomSheet(true);
                        } catch (e) {
                          printIfDebug(e);
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Constants.appColorLogo,
                          border: Border.all(
                            color: Color(
                              0xFF43c681,
                            ),
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        padding: EdgeInsets.only(
                          left: 30,
                          right: 30,
                          top: 10,
                          bottom: 10,
                        ),
                        margin: EdgeInsets.only(
                          bottom: 15,
                        ),
                        child: Text(
                          "SHARE",
                          style: TextStyle(
                            color: Constants.appColorL1,
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    showShareReminder = false;
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.only(
                    right: 5,
                  ),
                  decoration: BoxDecoration(
                    color: Constants.appColorL2,
                    border: Border.all(color: Constants.appColorLogo),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.close,
                    color: Color(
                      0xFF43C681,
                    ),
                  ),
                ),
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.end,
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   children: <Widget>[

              //   ],
              // ),
            ],
          ),
        ),
      ],
    );
  }

  Widget screenshotBox(imageurl) {
    String logoUrl = "";
    try {
      double width = 0;

      double height = 0;
      int cardfor = 1;
      switch (shareType) {
        case 0:
          width = MediaQuery.of(context).size.width;
          height = width = MediaQuery.of(context).size.height - 50;
          break;
        case 1:
          width = MediaQuery.of(context).size.width;
          height = width;
          break;
        case 2:
          width = MediaQuery.of(context).size.width;
          height = ((width / 16) * 9) + 25;
          cardfor = 2;
          break;
        default:
          break;
      }
      printIfDebug(
          "height " + height.toString() + "  width  " + width.toString());
      return Container(
        margin: EdgeInsets.only(top: 40.0),
        color: Constants.appColorL1,
        width: width + 20,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              RepaintBoundary(
                key: previewContainer,
                child: Container(
                  height: height,
                  width: width,
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: <Widget>[
                      Container(
                        child: Image.asset("assests/no_bg.jpg"),
                      ),
                      Container(
                        color: Constants.appColorL1,
                        height: height,
                        width: width,
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                          child: Container(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  color: Constants.appColorL1,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Container(
                                        color: Constants.appColorL1,
                                        child: GestureDetector(
                                          onTap: () {},
                                          child: moviePoster(
                                              width, imageurl, height),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          // top: width - 40,
                          left: 10,
                          right: 10,
                          // bottom: 10,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                (movieDetail["watchNow"]["streamingSources"] !=
                                            null &&
                                        movieDetail["watchNow"]
                                                    ["streamingSources"]
                                                .length >
                                            0 &&
                                        movieDetail["watchNow"]
                                                    ["streamingSources"][0]
                                                ["sourceName"] !=
                                            null)
                                    ? Container(
                                        margin: EdgeInsets.only(
                                          top: 10,
                                          left: 10,
                                        ),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          color: Constants.appColorLogo
                                              .withOpacity(0.8),
                                        ),
                                        padding: EdgeInsets.only(
                                          top: 5,
                                          bottom: 5,
                                          left: 10,
                                          right: 10,
                                        ),
                                        child: Text(
                                          movieDetail["watchNow"][
                                                          "streamingSources"] !=
                                                      null &&
                                                  movieDetail["watchNow"][
                                                              "streamingSources"]
                                                          .length >
                                                      0
                                              ? movieDetail["watchNow"]
                                                          ["streamingSources"]
                                                      [0]["sourceName"]
                                                  .toString()
                                                  .toUpperCase()
                                              : "",
                                          style: TextStyle(
                                            color: Constants.appColorL1,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10,
                                          ),
                                        ),
                                      )
                                    : Container(
                                        margin: EdgeInsets.only(
                                          top: 10,
                                          left: 10,
                                        ),
                                        // decoration: BoxDecoration(
                                        //   borderRadius:
                                        //       BorderRadius.circular(20.0),
                                        //   color: Constants.appColorLogo
                                        //       .withOpacity(0.8),
                                        // ),
                                        padding: EdgeInsets.only(
                                          top: 5,
                                          bottom: 5,
                                          left: 10,
                                          right: 10,
                                        ),
                                      ),
                                Spacer(),
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 10,
                                    right: 10,
                                  ),
                                  child: Image.asset(
                                    "assests/app_logo.png",
                                    height: 30,
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            Container(
                              padding: EdgeInsets.only(
                                // top: 20,
                                bottom: 5,
                                left: 15,
                              ),
                              child: Text(
                                movieDetail["movieName"],
                                // "Spider-Man: Far From Home",
                                style: TextStyle(
                                  color: Constants.appColorFont,
                                  fontSize: 30,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                left: 15,
                                right: 10,
                                bottom: 10,
                              ),
                              child: Text(
                                movieDetail["movieIntro"] != null &&
                                        movieDetail["movieIntro"].length > 0 &&
                                        movieDetail["movieIntro"][0] != null
                                    ? movieDetail["movieIntro"][0]
                                    : "",
                                style: TextStyle(
                                  color: Constants.appColorFont,
                                  fontSize: 12,
                                  height: 1.2,
                                  // letterSpacing: 0.2,
                                ),
                                maxLines: 4,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            movieDetail["ratings"] != null &&
                                    movieDetail["ratings"]["scorer"].length > 0
                                ? Container(
                                    width: (width),

                                    // height: 50,
                                    color: Colors
                                        .transparent, //Constants.appColorL2,
                                    child: Container(
                                      child: MovieDetailRating(
                                        movieDetail: movieDetail,
                                        showOriginalPoster: showOriginalPosters,
                                        cardFor: cardfor,
                                      ),
                                      // margin: EdgeInsets.only(
                                      //   top: 5,
                                      // ),
                                    ),
                                  )
                                : Container(
                                    width: (width),
                                    height: 100,
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } catch (e) {
      printIfDebug("catched");
      printIfDebug(e);
    }
    return Container();
  }

  Widget moviePoster(width, moviePosterUrl, height) {
    return Stack(
      children: <Widget>[
        moviePosterUrl != null && moviePosterUrl.length > 1
            ? Container(
                color: Constants.appColorL1,
                child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Constants.appColorL1, Colors.transparent],
                    ).createShader(
                      Rect.fromLTRB(
                        0,
                        0,
                        width,
                        height - 120,
                      ),
                    );
                  },
                  blendMode: BlendMode.dstIn,
                  child: Image.network(
                    moviePosterUrl,
                    height: height,
                    width: width,
                    fit: BoxFit.cover,
                  ),
                ),
              )
            : Container(
                color: Constants.appColorL1,
                child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Constants.appColorL1, Colors.transparent],
                    ).createShader(
                      Rect.fromLTRB(
                        0,
                        0,
                        width,
                        width + 170,
                      ),
                    );
                  },
                  blendMode: BlendMode.dstIn,
                  child: Image.asset(
                    "assests/no_bg.jpg",
                    height: width * 1.4,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
        Container(
          color: Constants.appColorL1.withOpacity(0.5),
          width: width,
          height: height,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    printIfDebug(
      '\n\nurl information is: ${widget.urlInformation}',
    );
    String imageurl = "";
    bool flag = false;
    if (movieDetail != null && movieDetail.length > 1) {
      flag = true;
    }
    try {
      if (!flag) {
        if (widget.movieDataFromCall["moviePhoto"] != null) {
          imageurl = widget.movieDataFromCall["moviePhoto"];
        } else if (widget.movieDataFromCall["image_uri"] != null) {
          imageurl = widget.movieDataFromCall["image_uri"];
        } else if (widget.movieDataFromCall["full_image_uri"] != null) {
          imageurl = widget.movieDataFromCall["full_image_uri"];
        } else if (widget.movieDataFromCall["fullMoviePhoto"] != null) {
          imageurl = widget.movieDataFromCall["fullMoviePhoto"];
        } else if (widget.movieDataFromCall["similarMoviePhoto"] != null) {
          imageurl = widget.movieDataFromCall["similarMoviePhoto"];
        }
      } else {
        if (movieDetail["ogimage"] != null &&
            movieDetail["ogimage"].isNotEmpty) {
          imageurl = imageurl != movieDetail["ogimage"]
              ? movieDetail["ogimage"]
              : imageurl;
        } else if (movieDetail["moviePoster"] != null &&
            movieDetail["moviePoster"].isNotEmpty) {
          imageurl = imageurl != movieDetail["moviePoster"]
              ? movieDetail["moviePoster"]
              : imageurl;
        } else if (movieDetail["movieHorizontalPoster"] != null &&
            movieDetail["movieHorizontalPoster"].isNotEmpty) {
          imageurl = imageurl != movieDetail["movieHorizontalPoster"]
              ? movieDetail["movieHorizontalPoster"]
              : imageurl;
        }
      }
    } catch (e) {
      printIfDebug(e);
    }
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      body: Stack(
        children: <Widget>[
          screenshotBox(imageurl.replaceAll("thumb", "original")),
          WillPopScope(
            child: checkDataReady(),
            onWillPop: () {
              if (widget.notification != null && widget.notification) {
                Navigator.of(context).pushAndRemoveUntil(
                  new MaterialPageRoute(
                    builder: (
                      BuildContext context,
                    ) =>
                        new MovieRouting(defaultTab: 2),
                  ),
                  (Route<dynamic> route) => false,
                );
              } else {
                Navigator.pop(context);
              }
              return Future.value(false);
            },
          ),
          showFeedback ? showFeedbackOverlay() : Container(),
          showShareReminder ? shareReminder() : Container(),
        ],
      ),
    );
  }
}
