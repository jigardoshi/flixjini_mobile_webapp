import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:streaming_entertainment/detail/movie_detail_header.dart';
// import	'package:streaming_entertainment/index/movie_index_carousel.dart';
import 'package:flixjini_webapp/detail/movie_detail_user_preference.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'dart:convert';
// import 'package:http/http.dart' as http;
// import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'dart:io' show Platform;
import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailCollapsableAppbar extends StatefulWidget {
  MovieDetailCollapsableAppbar({
    Key? key,
    this.movieDetail,
    this.authenticationToken,
    this.sendRequest,
    this.urlName,
    this.urlPhase,
    this.moveToWatchNowOrBookNow,
    this.streamingStatus,
    this.ratingsFlag,
    this.notification,
    this.movieDataFromCall,
    this.showOriginalPosters,
    this.showBottomSheet,
  }) : super(key: key);

  final movieDetail;
  final authenticationToken;
  final sendRequest;
  final urlName;
  final urlPhase;
  final moveToWatchNowOrBookNow;
  final streamingStatus;
  final ratingsFlag;
  final notification;
  final movieDataFromCall;
  final showOriginalPosters;
  final showBottomSheet;

  @override
  _MovieDetailCollapsableAppbarState createState() =>
      new _MovieDetailCollapsableAppbarState();
}

class _MovieDetailCollapsableAppbarState
    extends State<MovieDetailCollapsableAppbar> {
  Widget collapsingAppBar() {
    var screenheight = MediaQuery.of(context).size.height;
    return new SliverAppBar(
      elevation: 0.0,
      leading: new InkWell(
        onTap: () {
          if (widget.notification != null && widget.notification) {
            Navigator.of(context).pushAndRemoveUntil(
              new MaterialPageRoute(
                builder: (
                  BuildContext context,
                ) =>
                    new MovieRouting(defaultTab: 2),
              ),
              (Route<dynamic> route) => false,
            );
          } else {
            Navigator.pop(context);
          }
        },
        child: new Container(
          padding: const EdgeInsets.all(20.0),
          child: new Image.asset(
            "assests/backarrow1.png",
            width: 20.0,
            height: 15.0,
          ),
        ),
      ),
      expandedHeight: screenheight * 0.61,
      floating: false,
      pinned: true,
      backgroundColor: Constants.appColorL1,
      flexibleSpace: getFlexibleSpaceWidget(),
    );
  }

  Widget getFlexibleSpaceWidget() {
    return FlexibleSpaceBar(
      centerTitle: true,
      background: getBackground(),
    );
  }

  Widget getBackground() {
    return MovieDetailUserPreference(
      movieDetail: widget.movieDetail,
      authenticationToken: widget.authenticationToken,
      sendRequest: widget.sendRequest,
      urlName: widget.urlName,
      urlPhase: widget.urlPhase,
      moveToWatchNowOrBookNow: widget.moveToWatchNowOrBookNow,
      streamingStatus: widget.streamingStatus,
      ratingsFlag: widget.ratingsFlag ?? false,
      movieDataFromCall: widget.movieDataFromCall,
      showOriginalPosters: widget.showOriginalPosters ?? true,
      showBottomSheet: widget.showBottomSheet,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return collapsingAppBar();
  }
}
