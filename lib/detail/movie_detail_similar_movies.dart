import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/detail/movie_detail_page.dart';
import 'package:flixjini_webapp/detail/movie_detail_subheading.dart';
// import 'package:flixjini_webapp/detail/movie_detail_underline.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'dart:ui';
import 'package:flixjini_webapp/utils/signin_alert.dart';

// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
// import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'dart:io' show Platform;
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';

class MovieDetailSimilarMovies extends StatefulWidget {
  MovieDetailSimilarMovies({
    Key? key,
    this.movieDetail,
    this.showOriginalPosters,
    this.authenticationToken,
    this.sendRequest,
    this.queueIds,
  }) : super(key: key);

  final movieDetail,
      showOriginalPosters,
      authenticationToken,
      sendRequest,
      queueIds;

  @override
  _MovieDetailSimilarMoviesState createState() =>
      new _MovieDetailSimilarMoviesState();
}

class _MovieDetailSimilarMoviesState extends State<MovieDetailSimilarMovies> {
  bool isQueued = false, isSeen = false;
  List<String> seenIds = [];
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    // getMagicSwitchValueFromRemoteConfig();
    getSeenMovieIds();
  }

  getSeenMovieIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    seenIds = prefs.getStringList("seenids")!;
    // printIfDebug("seenids :" + seenIds.toString());
  }

  addSeenMovieId(index) async {
    printIfDebug("added seen " +
        widget.movieDetail["similarMovies"]["movies"][index]["similarMovieId"]
            .toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (seenIds == null) {
      seenIds = <String>[];
    }

    seenIds.add(widget.movieDetail["similarMovies"]["movies"][index]
            ["similarMovieId"]
        .toString());

    prefs.setStringList("seenids", seenIds);
    updateIsSeen(index);
  }

  removeSeenMovieId(var index) async {
    printIfDebug("removed seen " +
        widget.movieDetail["similarMovies"]["movies"][index]["similarMovieId"]
            .toString());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (seenIds == null) {
      seenIds = <String>[];
    }

    seenIds.remove(widget.movieDetail["similarMovies"]["movies"][index]
            ["similarMovieId"]
        .toString());

    prefs.setStringList("seenids", seenIds);
    updateIsSeen(index);
  }

  updateIsSeen(index) {
    // printIfDebug("object" + seenIds.toString());
    if (seenIds != null &&
        seenIds.contains(widget.movieDetail["similarMovies"]["movies"][index]
                ["similarMovieId"]
            .toString())) {
      widget.movieDetail["similarMovies"]["movies"][index]["isSeen"] = true;
    } else {
      widget.movieDetail["similarMovies"]["movies"][index]["isSeen"] = false;
    }
    // printIfDebug(widget.movieDetail["similarMovies"]["movies"][index]["isSeen"]
    //     .toString());
  }

  sendSeenRequest(movie, actionUrl, entity) async {
    printIfDebug((widget.authenticationToken).isEmpty);
    if (widget.authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      bool flag = true;
      // if (entity == "seen") {
      //   flag = true;
      // }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["similarMovieId"].toString(),
          'magic': "false",
          'app': "flixjini-mobile",
        };

        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          // printIfDebug("seen json: " + jsonResponse.toString());
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              showToast(movie["isSeen"] ? 'Added to Seen' : "Removed from seen",
                  'long', 'bottom', 1, Constants.appColorLogo);

              // if (entity == "add") {
              //   updateUserQueue(movie);
              // } else {
              //   updateuserDetail(movie, entity);
              // }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        // actionUrl = 'https://api.flixjini.com/queue/remove.json?movie_id=' +
        //     movie["id"] +
        //     '&jwt_token=' +
        //     authenticationToken;
        // showToast('Removed from queue', 'long', 'bottom', 1, Constants.appColorLogo);
        // removeMovieFromWatchList(movie, actionUrl);
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  Widget buildQuickFactCards(BuildContext context, int index) {
    String similarMoviePhoto = widget.movieDetail["similarMovies"]["movies"]
            [index]["similarMoviePhoto"]
        .toString();
    printIfDebug(
      '\n\nsimilar movie photo: $similarMoviePhoto',
    );
    return new Container(
      padding: const EdgeInsets.all(0.0),
      width: 150.0,
      child: new Card(
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        elevation: 6.0,
        color: Constants.appColorFont,
        child: new Padding(
          padding: const EdgeInsets.all(0.0),
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new Expanded(
                  child: new GestureDetector(
                    onTap: () {
                      printIfDebug("You have selected");
                      printIfDebug(widget.movieDetail["similarMovies"]["movies"]
                          [index]["similarMovieLink"]);
                      String urlPhase = widget.movieDetail["similarMovies"]
                              ["movies"][index]["similarMovieLink"]
                          .replaceAll('https://api.flixjini.com', '');
                      urlPhase = widget.movieDetail["similarMovies"]["movies"]
                              [index]["similarMovieLink"]
                          .replaceAll('https://www.komparify.com', '');
                      printIfDebug("The Url Phase");
                      printIfDebug(urlPhase);
                      String urlName = widget.movieDetail["similarMovies"]
                              ["movies"][index]["similarMovieLink"] +
                          "?";
                      printIfDebug(urlName);
                      var urlInformation = {
                        "urlPhase": urlPhase,
                        "fullUrl": urlName,
                      };
                      Navigator.of(context).push(
                        new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new MovieDetailPage(
                            urlInformation: urlInformation,
                            showOriginalPosters: widget.showOriginalPosters,
                            movieDataFromCall: widget
                                .movieDetail["similarMovies"]["movies"][index],
                          ),
                        ),
                      );
                    },
                    child: new Stack(
                      children: <Widget>[
                        new ClipRRect(
                          borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(
                              8.0,
                            ),
                            topRight: const Radius.circular(
                              8.0,
                            ),
                          ),
                          child: new Container(
                            color: Constants.appColorFont,
                            child: new Image(
                              image: NetworkImage(
                                widget.movieDetail["similarMovies"]["movies"]
                                    [index]["similarMoviePhoto"],
                                // useDiskCache: false,
                              ),
                              fit: BoxFit.cover,
                              gaplessPlayback: true,
                              width: 150.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flex: 4,
                ),
                new Expanded(
                  child: new Center(
                    child: new Text(
                      widget.movieDetail["similarMovies"]["movies"][index]
                          ["similarMovieName"],
                      style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 12.0,
                        color: Constants.appColorL3,
                      ),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  ),
                  flex: 1,
                ),
              ]),
        ),
      ),
    );
  }

  Widget quickFactsCardView() {
    return new SizedBox.fromSize(
      size: const Size.fromHeight(280.0),
      child: new ListView.builder(
        itemCount: widget.movieDetail["similarMovies"]["movies"].length,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(
          left: 10.0,
          right: 10,
        ),
        itemBuilder: (context, index) {
          return callGetSimilarMovies(
            index,
          );
        },
      ),
    );
  }

  Widget callGetSimilarMovies(
    int index,
  ) {
    return similarCardUI(
      // context,
      index,
    );
  }

  Widget getCardsWithImages(
    double screenHeight,
    double screenWidth,
    int index,
    String similarMovieName,
  ) {
    return Container(
      child: GestureDetector(
        onTap: () {
          printIfDebug("You have selected");
          printIfDebug(widget.movieDetail["similarMovies"]["movies"][index]
              ["similarMovieLink"]);
          String urlPhase = widget.movieDetail["similarMovies"]["movies"][index]
                  ["similarMovieLink"]
              .replaceAll('https://api.flixjini.com', '');
          urlPhase = widget.movieDetail["similarMovies"]["movies"][index]
                  ["similarMovieLink"]
              .replaceAll('https://www.komparify.com', '');
          printIfDebug("The Url Phase");
          printIfDebug(urlPhase);
          String urlName = widget.movieDetail["similarMovies"]["movies"][index]
                  ["similarMovieLink"] +
              "?";
          printIfDebug(urlName);
          var urlInformation = {
            "urlPhase": urlPhase,
            "fullUrl": urlName,
          };
          Navigator.of(context).push(
            new MaterialPageRoute(
              builder: (BuildContext context) => new MovieDetailPage(
                urlInformation: urlInformation,
                movieDataFromCall: widget.movieDetail["similarMovies"]["movies"]
                    [index],
                showOriginalPosters: widget.showOriginalPosters,
              ),
            ),
          );
        },
        child: Center(
          child: new Container(
            padding: const EdgeInsets.all(5.0),
            width: 160.0,
            child: new Card(
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(
                  8.0,
                ),
              ),
              elevation: 9.0,
              color: Constants.appColorFont.withOpacity(0.9),
              child: new Container(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  child: Stack(
                    children: <Widget>[
                      getIndexPageBlurredImageContainer(
                        similarMovieName,
                        1,
                        true,
                      ),
                      getBlurredTopWidget(
                        screenHeight,
                        screenWidth,
                      ),
                      Positioned(
                        left: 15,
                        right: 15,
                        top: 40,
                        bottom: 25,
                        child: Center(
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 2,
                              right: 2,
                            ),
                            width: screenWidth,
                            height: screenHeight,
                            child: Center(
                              child: getAutoSizeTextWidget(
                                similarMovieName,
                              ),
                            ),
                          ),
                        ),
                      ),
                      addCardType(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget similarCardUI(int index) {
    // var cardBlur = userMenuStatus ? 50.0 : 0.0;
    // var cardOpacity = userMenuStatus ? 0.5 : 0.0;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return new Center(
      child: new Container(
        padding: EdgeInsets.all(true ? 5.0 : 0),
        width: 160.0,
        child: new Card(
          // shape: RoundedRectangleBorder(
          //   borderRadius: BorderRadius.circular(
          //     8.0,
          //   ),
          // ),
          // elevation: 9.0,
          color: Constants.appColorL2,
          child: new Container(
            padding: const EdgeInsets.all(5.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Expanded(
                  child: new GestureDetector(
                    onTap: () {
                      printIfDebug("You have selected");
                      printIfDebug(widget.movieDetail["similarMovies"]["movies"]
                          [index]["similarMovieLink"]);
                      String urlPhase = widget.movieDetail["similarMovies"]
                              ["movies"][index]["similarMovieLink"]
                          .replaceAll('https://api.flixjini.com', '');
                      urlPhase = widget.movieDetail["similarMovies"]["movies"]
                              [index]["similarMovieLink"]
                          .replaceAll('https://www.komparify.com', '');
                      printIfDebug("The Url Phase");
                      printIfDebug(urlPhase);
                      String urlName = widget.movieDetail["similarMovies"]
                              ["movies"][index]["similarMovieLink"] +
                          "?";
                      printIfDebug(urlName);
                      var urlInformation = {
                        "urlPhase": urlPhase,
                        "fullUrl": urlName,
                      };
                      Navigator.of(context).push(
                        new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new MovieDetailPage(
                            urlInformation: urlInformation,
                            movieDataFromCall: widget
                                .movieDetail["similarMovies"]["movies"][index],
                            showOriginalPosters: widget.showOriginalPosters,
                          ),
                        ),
                      );
                    },
                    child: getSimpleSimilarCard(
                      screenHeight,
                      screenWidth,
                      index,
                    ),
                  ),
                  flex: widget.showOriginalPosters ? 4 : 1,
                ),
                widget.showOriginalPosters
                    ? titleBox(index, true)
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget titleBox(int index, bool flag) {
    return flag
        ? Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    top: 5,
                    // left: 5,
                  ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        // padding: EdgeInsets.only(left: 3),
                        child: Text(
                          widget.movieDetail["similarMovies"]["movies"][index]
                                      ["similarMovieYear"] !=
                                  null
                              ? widget.movieDetail["similarMovies"]["movies"]
                                      [index]["similarMovieYear"] +
                                  " - "
                              : " ",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 10,
                            color: const Color(
                              0xFF777777,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          widget.movieDetail["similarMovies"]["movies"][index]
                                      ["similarMovieLanguage"] !=
                                  null
                              ? widget.movieDetail["similarMovies"]["movies"]
                                          [index]["similarMovieLanguage"]
                                      .toString()
                                      .toUpperCase() +
                                  " - "
                              : " ",
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontSize: 10,
                            color: const Color(
                              0xFF777777,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        // padding: EdgeInsets.only(left: 5),
                        child: Text(
                          widget.movieDetail["similarMovies"]["movies"][index]
                                      ["similarMovieContentType"] !=
                                  null
                              ? widget.movieDetail["similarMovies"]["movies"]
                                  [index]["similarMovieContentType"]
                              : " ",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 10,
                            color: const Color(
                              0xFF777777,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(
                      top: 5.0,
                      // left: 5.0,
                    ),
                    child: Text(
                      widget.movieDetail["similarMovies"]["movies"][index]
                          ["similarMovieName"],
                      style: TextStyle(color: Constants.appColorFont
                          // fontSize: 10,
                          ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  flex: 1,
                ),
              ],
            ),
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  left: 5,
                  right: 5,
                ),
                child: Text(
                  widget.movieDetail["similarMovies"]["movies"][index]
                      ["similarMovieName"],
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Constants.appColorFont,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 5,
                  // left: 5,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      // padding: EdgeInsets.only(left: 3),
                      child: Text(
                        widget.movieDetail["similarMovies"]["movies"][index]
                                    ["similarMovieYear"] !=
                                null
                            ? widget.movieDetail["similarMovies"]["movies"]
                                    [index]["similarMovieYear"] +
                                " - "
                            : " ",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 10,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.movieDetail["similarMovies"]["movies"][index]
                                    ["similarMovieLanguage"] !=
                                null
                            ? widget.movieDetail["similarMovies"]["movies"]
                                        [index]["similarMovieLanguage"]
                                    .toString()
                                    .toUpperCase() +
                                " - "
                            : " ",
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontSize: 10,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(left: 5),
                      child: Text(
                        widget.movieDetail["similarMovies"]["movies"][index]
                                    ["similarMovieContentType"] !=
                                null
                            ? widget.movieDetail["similarMovies"]["movies"]
                                [index]["similarMovieContentType"]
                            : " ",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 10,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              listSeenBox(index, false),
            ],
          );
  }

  bool checkIfMoviImagePresent(int index) {
    if (widget.movieDetail["similarMovies"]["movies"][index]
            ["similarMoviePhoto"] !=
        null) {
      return true;
    } else {
      return false;
    }
  }

  Widget movieImageAvailable(int index) {
    bool ratio = true;
    if (widget.movieDetail["similarMovies"]["movies"][index]
                ["moviePhotoHeight"] !=
            null &&
        widget.movieDetail["similarMovies"]["movies"][index]
                ["moviePhotoWidth"] !=
            null) {
      ratio = (double.parse(widget.movieDetail["similarMovies"]["movies"][index]
                  ["moviePhotoHeight"]) >
              double.parse(widget.movieDetail["similarMovies"]["movies"][index]
                  ["moviePhotoWidth"]))
          ? true
          : false;
    } else {
      ratio = false;
    }
    return new Container(
      color: ratio ? Constants.appColorL2 : Constants.appColorL1,
      child: GestureDetector(
          child: getOneCachedImageWidget(
              widget.movieDetail["similarMovies"]["movies"][index]
                  ["similarMoviePhoto"],
              ratio,
              index),
          onLongPress: () {
            // printIfDebug('\n\nmovie details: ${widget.movie}');
          }),
    );
  }

  Widget getOneCachedImageWidget(
    String imageUrl,
    bool ratio,
    int index,
  ) {
    return Image.network(imageUrl);
    //   imageBuilder:
    //       (BuildContext context, ImageProvider<Object> imageProvider) =>
    //           Container(
    //     decoration: BoxDecoration(
    //       // borderRadius: BorderRadius.only(
    //       //   topLeft: Radius.circular(
    //       //     8.0,
    //       //   ),
    //       //   topRight: Radius.circular(
    //       //     8.0,
    //       //   ),
    //       // ),
    //       image: DecorationImage(
    //         image: imageProvider,
    //         fit: BoxFit.cover,
    //         alignment: Alignment.topCenter,
    //       ),
    //     ),
    //   ),
    //   placeholder: (context, url) => Center(
    //     child: Text(widget.movieDetail["similarMovies"]["movies"][index]
    //         ["similarMovieName"]),
    //   ),
    //   errorWidget: (context, url, error) => Center(
    //     child: Text(widget.movieDetail["similarMovies"]["movies"][index]
    //         ["similarMovieName"]),
    //   ),
    // );
  }

  updateIsQueued(index) {
    isQueued = widget.queueIds.contains(widget.movieDetail["similarMovies"]
            ["movies"][index]["similarMovieId"]
        .toString());
    widget.movieDetail["similarMovies"]["movies"][index]["queue"] = isQueued;
    printIfDebug(widget.movieDetail["similarMovies"]["movies"][index]
                ["similarMovieId"]
            .toString() +
        isQueued.toString());
  }

  Widget getSimpleSimilarCard(
    double screenHeight,
    double screenWidth,
    int index,
  ) {
    var cardBlur = 0.0;
    var cardOpacity = 0.0;
    updateIsQueued(index);
    updateIsSeen(index);
    // printIfDebug('\n\nfrom card structure, value of show original posters: ${widget.showOriginalPosters}');
    return Container(
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            // child: new ClipRRect(
            //   borderRadius: new BorderRadius.only(
            //     topLeft: const Radius.circular(
            //       8.0,
            //     ),
            //     topRight: const Radius.circular(
            //       8.0,
            //     ),
            //   ),
            child: Container(
              color: Constants.appColorL1,
              child: movieImageAvailable(index),
              // checkIfMoviImagePresent()
              //     ? movieImageAvailable()
              //     : movieImageUnavailable(),
            ),
            // ),
          ),
          new ClipRRect(
            borderRadius: new BorderRadius.only(
              topLeft: widget.showOriginalPosters
                  ? Radius.circular(8.0)
                  : Radius.circular(0.0),
              topRight: widget.showOriginalPosters
                  ? Radius.circular(8.0)
                  : Radius.circular(0.0),
            ),
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: widget.showOriginalPosters ? cardBlur : 15,
                sigmaY: widget.showOriginalPosters ? cardBlur : 15,
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(8.0),
                    topRight: const Radius.circular(8.0),
                  ),
                  color: Constants.appColorL1.withOpacity(
                    cardOpacity,
                  ),
                ),
              ),
            ),
          ),
          // displayUserMenu(),
          // !userMenuStatus ? getAwardLogo() : Container(),
          widget.showOriginalPosters
              ? listSeenBox(index, true)
              : titleBox(index, false),
        ],
      ),
    );
  }

  Widget listSeenBox(int index, bool flag) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: flag ? 5 : 70,
            right: flag ? 5 : 0,
            bottom: flag ? 0 : 20,
          ),
          child: Row(
            mainAxisAlignment:
                flag ? MainAxisAlignment.end : MainAxisAlignment.center,
            children: <Widget>[
              Opacity(
                opacity: 0.8,
                child: GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(
                      right: 5,
                    ),
                    padding: EdgeInsets.all(5),
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      // color: Constants.appColorLogo,
                      shape: BoxShape.circle,
                      color: widget.movieDetail["similarMovies"]["movies"]
                              [index]["isSeen"]
                          ? Constants.appColorLogo
                          : Constants.appColorDivider,
                    ),
                    child: Image.asset(
                      "assests/seen_dark.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  onTap: () {
                    if (!widget.authenticationToken.isEmpty) {
                      if (!widget.movieDetail["similarMovies"]["movies"][index]
                          ["isSeen"]) {
                        addSeenMovieId(index);
                        sendEvent();
                        sendSeenRequest(
                            widget.movieDetail["similarMovies"]["movies"]
                                [index],
                            "https://api.flixjini.com/queue/seen.json",
                            "seen");
                        setState(() {
                          widget.movieDetail["similarMovies"]["movies"][index]
                              ["isSeen"] = true;
                        });
                      } else {
                        removeSeenMovieId(index);
                        sendSeenRequest(
                            widget.movieDetail["similarMovies"]["movies"]
                                [index],
                            "https://api.flixjini.com/queue/reset_seen.json",
                            "seen");
                        setState(() {
                          widget.movieDetail["similarMovies"]["movies"][index]
                              ["isSeen"] = false;
                        });
                      }
                    } else {
                      showAlert();
                    }
                  },
                ),
              ),
              Opacity(
                opacity: 0.8,
                child: GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      // color: Constants.appColorLogo,
                      shape: BoxShape.circle,
                      color: isQueued
                          ? Constants.appColorLogo
                          : Constants.appColorDivider,
                    ),
                    height: 25,
                    width: 25,
                    child: isQueued
                        ? Image.asset(
                            "assests/mylisttick.png",
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            "assests/mylist_dark.png",
                            fit: BoxFit.cover,
                          ),
                  ),
                  onTap: () {
                    if (!widget.authenticationToken.isEmpty) {
                      if (!widget.movieDetail["similarMovies"]["movies"][index]
                          ["queue"]) {
                        setState(() {
                          widget.queueIds.add(widget
                              .movieDetail["similarMovies"]["movies"][index]
                                  ["similarMovieId"]
                              .toString());
                          widget.movieDetail["similarMovies"]["movies"][index]
                              ["queue"] = false;
                        });
                        sendQueueEvent("add", index);
                        widget.sendRequest(
                            'https://api.flixjini.com/queue/v2/add.json',
                            'queue',
                            widget.movieDetail["similarMovies"]["movies"]
                                [index]);
                      } else {
                        setState(() {
                          widget.queueIds.remove(widget
                              .movieDetail["similarMovies"]["movies"][index]
                                  ["similarMovieId"]
                              .toString());
                          widget.movieDetail["similarMovies"]["movies"][index]
                              ["queue"] = true;
                        });
                        sendQueueEvent("remove", index);

                        widget.sendRequest(
                            'https://api.flixjini.com/queue/v2/remove.json',
                            'queue',
                            widget.movieDetail["similarMovies"]["movies"]
                                [index]);
                      }
                    } else {
                      showAlert();
                      //   showToast('Login to Add to Queue', 'long', 'bottom',
                      //       1, Constants.appColorLogo);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  sendEvent() {
    //platform.invokeMethod('logCustomizeProductEvent');
  }

  sendQueueEvent(String func, index) {
    Map<String, String> eventData = {
      'content': widget.movieDetail["similarMovies"]["movies"][index]
          ["similarMovieName"],
      'func': func,
    };
    //platform.invokeMethod('logAddToWishlistEvent', eventData);
  }

  addCardType() {
    return new Positioned(
      top: 5.0,
      left: 8, // 5.0,
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          // color: Constants.appColorLogo,
          shape: BoxShape.circle,
        ),
        child: new Image.asset(
          "assests/movies.png",
          color: Constants.appColorFont,
          fit: BoxFit.cover,
          width: 18, // 13.0,
          height: 18, // 13.0,
        ),
      ),
    );
  }

  Widget getAutoSizeTextWidget(
    String movieName,
  ) {
    return AutoSizeText(
      cleanMovieName(
        movieName,
      ),
      minFontSize: 10,
      wrapWords: false,
      textAlign: TextAlign.center,
      style: TextStyle(
        letterSpacing: 0.7,
        fontWeight: FontWeight.bold,
        color: Constants.appColorFont,
      ),
      maxLines: 6,
    );
  }

  showAlert() {
    // double screenwidth = MediaQuery.of(context).size.width;

    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return SigninAlertAlert();
      },
    );
  }

  String cleanMovieName(String movieName) {
    movieName = movieName.replaceAll('-', ' ');
    movieName = movieName.replaceAll('/', ' ');
    movieName = movieName.replaceAll(':', ' ');

    movieName = movieName.replaceAll('?', ' ');
    movieName = movieName.replaceAll('_', ' ');
    movieName = movieName.replaceAll(';', ' ');

    return movieName.toUpperCase();
  }

  Positioned getBlurredTopWidget(
    double screenHeight,
    double screenWidth,
  ) {
    double overlayOpacity = 0.2;
    return Positioned(
      child: new Center(
        child: new Container(
          margin: EdgeInsets.only(
            left: 15,
            right: 15,
            top: 40,
            bottom: 25,
          ),
          width: screenWidth, // * 0.25,
          height: screenHeight, // * 0.20,
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: 4.0,
                sigmaY: 4.0,
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  border: new Border.all(
                    color: Constants.appColorFont,
                    width: 1,
                  ),
                  color: Constants.appColorL1.withOpacity(
                    overlayOpacity,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;
    return new Container(
      width: screenWidth,
      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          new MovieDetailSubheading(
            subheading: "Similar",
          ),
          // new MovieDetailUnderline(),
          new Container(
            height: 300.0,
            padding: const EdgeInsets.all(3.0),
            child: quickFactsCardView(),
          ),
        ],
      ),
    );
  }
}
