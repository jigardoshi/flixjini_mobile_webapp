import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailMoreCard extends StatefulWidget {
  MovieDetailMoreCard({
    Key? key,
    this.toogleExpansionMinimisation,
    this.cardHeight,
    this.cardWidth,
  }) : super(key: key);

  final toogleExpansionMinimisation;
  final cardHeight;
  final cardWidth;

  @override
  _MovieDetailMoreCardState createState() => new _MovieDetailMoreCardState();
}

class _MovieDetailMoreCardState extends State<MovieDetailMoreCard> {
  String morePhoto = "assests/more.png";

  Widget moreCardUI() {
    return Container(
      child: GestureDetector(
        onTap: () {
          widget.toogleExpansionMinimisation();
        },
        child: Center(
          child: new Container(
            height: widget.cardHeight,
            width: widget.cardWidth,
            margin: EdgeInsets.only(
              left: 25,
              right: 25,
            ),
            child: new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Image.asset(
                    'assests/expand_icon.png',
                    height: 18,
                    width: 18,
                  ),
                ),
                new Container(
                  padding: const EdgeInsets.all(5.0),
                  child: new Text(
                    "More",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Constants.appColorFont,
                        fontSize: 16.0),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return moreCardUI();
  }
}
