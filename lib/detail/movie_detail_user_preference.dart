import 'dart:io';

import 'dart:ui';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/rendering.dart';
// import 'package:flutter_youtube/flutter_youtube.dart';
import 'dart:async';
import 'package:flixjini_webapp/utils/signin_alert.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:flixjini_webapp/detail/movie_detail_rating.dart';
import 'package:flixjini_webapp/detail/movie_detail_heading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'movie_detail_header.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieDetailUserPreference extends StatefulWidget {
  MovieDetailUserPreference({
    Key? key,
    this.movieDetail,
    this.authenticationToken,
    this.sendRequest,
    this.urlName,
    this.urlPhase,
    this.moveToWatchNowOrBookNow,
    this.streamingStatus,
    this.ratingsFlag,
    this.movieDataFromCall,
    this.showOriginalPosters,
    this.showBottomSheet,
  }) : super(key: key);

  final movieDetail;
  final authenticationToken;
  final sendRequest;
  final urlName;
  final urlPhase;
  final moveToWatchNowOrBookNow;
  final streamingStatus;
  final ratingsFlag;
  final movieDataFromCall;
  final showOriginalPosters;
  final showBottomSheet;

  @override
  _MovieDetailUserPreferenceState createState() =>
      new _MovieDetailUserPreferenceState(movieDetail: movieDetail);
}

class _MovieDetailUserPreferenceState extends State<MovieDetailUserPreference>
    with WidgetsBindingObserver {
  final movieDetail;
  String shareUrl = '';
  bool isSeen = false, showShareAnimation = true;
  static const platform = const MethodChannel('api.komparify/advertisingid');

  _MovieDetailUserPreferenceState({
    this.movieDetail,
  }) : super() {
    platform.setMethodCallHandler(myNewHandleMethod);
  }

  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    shareReminderCheck();
  }

  sendEvent() {
    //platform.invokeMethod('logCustomizeProductEvent');
  }

  shareReminderCheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int reminderFlagCount = (prefs.getInt('shareReminderFlag') ?? 0);

    if (reminderFlagCount > 25) {
      setState(() {
        showShareAnimation = false;
      });
    }
    printIfDebug("reminderFlagCount" + reminderFlagCount.toString());
  }

  Future<dynamic> myNewHandleMethod(MethodCall call) async {
    printIfDebug(
      '\n\ncall name: ${call.method}, and call args: ${call.arguments}',
    );
    switch (call.method) {
      case "shareViaWhatsApp":
        printIfDebug(
            '\n\ninvoke share text method with args: ${call.arguments}');
        String moviePosterImageURL = widget.movieDetail["ogimage"];

        shareViaWhatsApp(
          moviePosterImageURL,
          getShareMessage(),
        );
        break;
      case "shareText":
        printIfDebug(
            '\n\ninvoke shareText method with args: ${call.arguments}');
        // share(
        //   getShareMessage(),
        // );

        shareTextToWhatsapp(getShareMessage());
        break;
      default:
        printIfDebug('\n\nswitch case default block');
        break;
    }
  }

  void playYoutubeVideo(String url) {
    printIfDebug(
      '\n\ntrailer url is: $url',
    );
    try {
      // FlutterYoutube.playYoutubeVideoById(
      //   apiKey: "AIzaSyD71aPyq5vFg6dSRmtM_R-ES0AtyuykXgI",
      //   videoId: getYouTubeVideoId(
      //     url,
      //   ),
      // );
    } catch (exception) {
      printIfDebug('\n\nexception caught is: $exception');
      Fluttertoast.showToast(
        msg: "Sorry trailer unavailable for " + widget.movieDetail["movieName"],
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        // timeInSecForIos: 1,
        backgroundColor: Constants.appColorError,
        textColor: Constants.appColorFont,
        fontSize: 16.0,
      );
    }
  }

  String getYouTubeVideoId(String videoUrl) {
    return Uri.parse(videoUrl).queryParameters['v']!;
  }

  Future basedOnSubscription() {
    // double screenwidth = MediaQuery.of(context).size.width;

    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return SigninAlertAlert();
      },
    );
  }

  String getShareMessage() {
    shareUrl = "https://www.flixjini.in";
    String movieLink = "";
    if ((widget.movieDetail["ogurl"] != null) && (widget.urlPhase != null)) {
      movieLink = shareUrl + widget.urlPhase;

      movieLink = movieLink.replaceAll(".json", "");
      movieLink = movieLink.replaceAll('/entertainment', '');

      String ogDescription = widget.movieDetail["movieSummary"];
      if (ogDescription != null) {
        printIfDebug("Shakkthi" + ogDescription);
      }
      {
        ogDescription = "";
      }
      // if (ogDescription == null || ogDescription.length == 0) {
      //   ogDescription = widget.movieDetail["ogdescription"] ?? "";
      // }
      ogDescription = ogDescription.replaceAll('Use komparify.com to d', 'D');
      ogDescription += '.\n\nDownload Now at https://a.kmpr.in/download';

      shareUrl = widget.movieDetail["ogtitle"] +
          "\n" +
          movieLink +
          "\n\n" +
          ogDescription;
    }
    printIfDebug(shareUrl);
    return shareUrl;
  }

  void iconSpecficAction(
    bool authStatus,
    String iconName,
  ) async {
    if (iconName == "share") {
      if (widget.movieDetail != null) {
        widget.showBottomSheet(true);

        Map<String, String> eventData = {
          'share': "all_share",
          'content': widget.movieDetail["movieName"].toString(),
        };
        //platform.invokeMethod('logCompleteTutorialEvent', eventData);
      }
      // String moviePosterImageURL = widget.movieDetail["ogimage"] != null
      //     ? widget.movieDetail["ogimage"]
      //     : widget.movieDetail["moviePoster"];
      // printIfDebug("poster url///////////" + moviePosterImageURL.toString());

      // if (true &&
      //     await checkIfAndroidAppIsInstalled('com.whatsapp')) {
      //   const platform = const MethodChannel('api.komparify/advertisingid');
      //   String grantedWritePermission =
      //       await //platform.invokeMethod('checkWriteStoragePermission');
      //   printIfDebug('\n\ngranted write permission: $grantedWritePermission');
      //   if (grantedWritePermission == 'true') {
      //     Map<String, String> eventData = {
      //       'share': "watsapp_share",
      //       'content': widget.movieDetail["movieName"].toString(),
      //     };
      //     //platform.invokeMethod('logCompleteTutorialEvent', eventData);
      //     shareViaWhatsApp(
      //       moviePosterImageURL,
      //       getShareMessage(),
      //     );
      //   }
      // } else {
      //   Map<String, String> eventData = {
      //     'share': "all_share",
      //     'content': widget.movieDetail["movieName"].toString(),
      //   };
      //   //platform.invokeMethod('logCompleteTutorialEvent', eventData);

      //   Share.text("", getShareMessage(), "text/plain");
      // }
    } else if (iconName == "trailer") {
      printIfDebug('\n\ntrailer url: ${widget.movieDetail['trailer_url']}');
      bool flag = false;
      if (widget.movieDetail["trailer_url"] != null) {
        if (widget.movieDetail["trailer_url"].isNotEmpty) {
          flag = true;
          printIfDebug("\n\ntrailer url: ${widget.movieDetail['trailer_url']}");

          playYoutubeVideo(
            widget.movieDetail["trailer_url"].toString(),
          );
        }
      }
      if (flag == false) {
        Fluttertoast.showToast(
            msg: "Sorry trailer unavailable for " +
                widget.movieDetail["movieName"],
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            // timeInSecForIos: 1,
            backgroundColor: Constants.appColorError,
            textColor: Constants.appColorFont,
            fontSize: 16.0);
      }
    } else if (iconName == "play") {
      widget.moveToWatchNowOrBookNow();
    } else {
      if (authStatus) {
        if (iconName == "like") {
          printIfDebug("Like Button Clicked");
          sendEvent();
          widget.sendRequest(
              'https://api.flixjini.com/queue/like.json', "like", null);
        } else if (iconName == "dislike") {
          sendEvent();
          widget.sendRequest(
              'https://api.flixjini.com/queue/dislike.json', "dislike", null);
        } else if (iconName == "queue") {
          sendQueueEvent("add");
          widget.sendRequest(
              'https://api.flixjini.com/queue/v2/add.json', "queue", null);
        } else if (iconName == "seen") {
          printIfDebug("seen icon done");
          printIfDebug(isSeen);
          if (isSeen) {
            printIfDebug("reset_seen");
            widget.sendRequest('https://api.flixjini.com/queue/reset_seen.json',
                "reset_seen", null);
            setState(() {
              isSeen = false;
            });
          } else {
            printIfDebug("seen");

            sendEvent();
            widget.sendRequest(
                'https://api.flixjini.com/queue/seen.json', "seen", null);
            setState(() {
              isSeen = true;
            });
          }
        }
      } else {
        basedOnSubscription();
      }
    }
  }

  sendQueueEvent(String func) {
    Map<String, String> eventData = {
      'content': widget.movieDetail["movieName"],
      'func': func,
    };
    //platform.invokeMethod('logAddToWishlistEvent', eventData);
  }

  Widget preferenceIcons(
      double? locationTop,
      double? locationBottom,
      double? locationLeft,
      double? locationRight,
      bool authStatus,
      String iconName,
      String iconPath,
      double iconSize) {
    Color iconStatus;
    printIfDebug(iconName);
    if ((iconName == "like") ||
        (iconName == "dislike") ||
        (iconName == "queue") ||
        (iconName == "seen")) {
      printIfDebug("Icon Name");
      printIfDebug(iconName);
      printIfDebug("status");
      // printIfDebug(authStatus && widget.movieDetail[iconName]);
      iconStatus = authStatus != null &&
              widget.movieDetail[iconName] != null &&
              authStatus &&
              widget.movieDetail[iconName]
          ? Constants.appColorLogo
          : Constants.appColorDivider;
    } else {
      iconStatus = Constants.appColorDivider;
    }
    return iconName != "share"
        ? Positioned(
            top: locationTop,
            bottom: locationBottom,
            left: locationLeft,
            right: locationRight,
            child: new GestureDetector(
              onTap: () {
                iconSpecficAction(
                  authStatus,
                  iconName,
                );
              },
              child: new Container(
                padding: iconName == "queue"
                    ? EdgeInsets.all(15.0)
                    : EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Constants.appColorL2,
                ),
                child: new Image.asset(
                  iconPath,
                  color: iconName.contains('share')
                      ? Constants.appColorLogo
                      : iconStatus,
                  fit: BoxFit.cover,
                  width: iconName == "queue" ? iconSize - 10 : iconSize,
                  height: iconName == "queue" ? iconSize - 10 : iconSize,
                ),
              ),
            ),
          )
        : showShareAnimation
            ? Positioned(
                top: locationTop,
                bottom: locationBottom,
                left: locationLeft,
                right: locationRight,
                child: new GestureDetector(
                  onTap: () {
                    iconSpecficAction(
                      authStatus,
                      iconName,
                    );
                  },
                  child: new Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Constants.appColorL2,
                    ),
                    child: Container(
                      height: 40,
                      width: 40,
                      child: FlareActor(
                        "assests/share.flr",
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        animation: "share",
                      ),
                    ),
                  ),
                ),
              )
            : Positioned(
                top: locationTop,
                bottom: locationBottom,
                left: locationLeft,
                right: locationRight,
                child: new GestureDetector(
                  onTap: () {
                    iconSpecficAction(
                      authStatus,
                      "share",
                    );
                  },
                  child: new Container(
                    padding: iconName == "queue"
                        ? EdgeInsets.all(15.0)
                        : EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Constants.appColorL2,
                    ),
                    child: new Image.asset(
                      "assests/share.png",
                      color: iconName.contains('share')
                          ? Constants.appColorLogo
                          : iconStatus,
                      fit: BoxFit.cover,
                      width: iconName == "queue" ? iconSize - 10 : iconSize,
                      height: iconName == "queue" ? iconSize - 10 : iconSize,
                    ),
                  ),
                ),
              );
  }

  Widget moviePoster(width, moviePosterUrl, height) {
    return Stack(
      children: <Widget>[
        moviePosterUrl != null && moviePosterUrl.length > 1
            ? Container(
                color: Constants.appColorL1,
                child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Constants.appColorL1, Colors.transparent],
                    ).createShader(
                      Rect.fromLTRB(
                        0,
                        0,
                        width,
                        height - 120,
                      ),
                    );
                  },
                  blendMode: BlendMode.dstIn,
                  child: Image.network(
                    moviePosterUrl,
                    height: height,
                    width: width,
                    fit: BoxFit.cover,
                  ),
                ),
              )
            : Container(
                color: Constants.appColorL1,
                child: ShaderMask(
                  shaderCallback: (rect) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Constants.appColorL1, Colors.transparent],
                    ).createShader(
                      Rect.fromLTRB(
                        0,
                        0,
                        width,
                        width + 170,
                      ),
                    );
                  },
                  blendMode: BlendMode.dstIn,
                  child: Image.asset(
                    "assests/no_bg.jpg",
                    height: width * 1.4,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
        Container(
          color: Constants.appColorL1.withOpacity(0.5),
          width: width,
          height: height,
        ),
      ],
    );
  }

  Widget userPreferenceChoices(bool authStatus) {
    double screenheight = MediaQuery.of(context).size.height;
    double screenwidth = MediaQuery.of(context).size.width;

    bool flag = false;
    if (widget.movieDetail != null && widget.movieDetail.length > 1) {
      flag = true;
    }
    String imageurl = "";

    if (!flag) {
      if (widget.movieDataFromCall["moviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["moviePhoto"];
      } else if (widget.movieDataFromCall["image_uri"] != null) {
        imageurl = widget.movieDataFromCall["image_uri"];
      } else if (widget.movieDataFromCall["full_image_uri"] != null) {
        imageurl = widget.movieDataFromCall["full_image_uri"];
      } else if (widget.movieDataFromCall["fullMoviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["fullMoviePhoto"];
      } else if (widget.movieDataFromCall["similarMoviePhoto"] != null) {
        imageurl = widget.movieDataFromCall["similarMoviePhoto"];
      }
    } else {
      if (widget.movieDetail["ogimage"] != null &&
          widget.movieDetail["ogimage"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["ogimage"]
            ? widget.movieDetail["ogimage"]
            : imageurl;
      } else if (widget.movieDetail["moviePoster"] != null &&
          widget.movieDetail["moviePoster"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["moviePoster"]
            ? widget.movieDetail["moviePoster"]
            : imageurl;
      } else if (widget.movieDetail["movieHorizontalPoster"] != null &&
          widget.movieDetail["movieHorizontalPoster"].isNotEmpty) {
        imageurl = imageurl != widget.movieDetail["movieHorizontalPoster"]
            ? widget.movieDetail["movieHorizontalPoster"]
            : imageurl;
      }
    }
    bool vertical = checkNetworkImageDimentions(imageurl);

    return new Stack(
      children: [
        Container(
          height: screenheight,
          width: screenwidth,
          color: Constants.appColorL1,
        ),
        MovieDetailHeader(
          movieDetail: widget.movieDetail,
          movieDataFromCall: widget.movieDataFromCall,
        ),

        preferenceIcons(
          // 1
          null, // t
          screenheight * 0.055,
          true ? screenwidth * 0.07 : screenwidth * 0.09, // l
          null,
          authStatus,
          "like",
          "assests/like.png",
          20.0,
        ),
        preferenceIcons(
          // 2
          null, // t
          screenheight * 0.03,
          screenwidth * 0.24, // l
          null,
          authStatus,
          "dislike",
          "assests/dislike.png",
          20.0,
        ),
        preferenceIcons(
          // 3
          null, // t
          true ? screenheight * 0.013 : screenheight * 0.009,
          (screenwidth * 0.50) - 26.0, // l
          null,
          authStatus,
          "queue",
          authStatus != null &&
                  widget.movieDetail["queue"] != null &&
                  authStatus &&
                  widget.movieDetail["queue"]
              ? "assests/mylisttick.png"
              : "assests/mylist_dark.png",
          35.0,
        ),
        // flag // 4
        //     ? preferenceIcons(
        //         null, // top
        //         screenheight * 0.02, // bottom
        //         null, // left
        //         true
        //             ? screenwidth * 0.26
        //             : screenwidth * 0.24, // right
        //         authStatus,
        //         "trailer",
        //         "assests/trailer.png",
        //         20.0,
        //       )
        //     : preferenceIcons(
        //         null, // t
        //         screenheight * 0.02,
        //         null, // l
        //         true
        //             ? screenwidth * 0.26

        //             : screenwidth * 0.24, // right
        //         authStatus,
        //         "play",
        //         "assests/play.png",
        //         20.0,
        //       ),
        preferenceIcons(
          null, // t
          screenheight * 0.03,
          null, // l
          true ? screenwidth * 0.24 : screenwidth * 0.24, // right
          authStatus,
          "seen",
          "assests/seen_dark.png",
          20.0,
        ),
        preferenceIcons(
          // 5
          null, // t
          screenheight * 0.05,
          null, // l
          true ? screenwidth * 0.07 : screenwidth * 0.08,
          authStatus,
          "share",
          "",
          20.0,
        ),

        getContainerForHeadingAndRating(
            screenheight, screenwidth, vertical, imageurl),
        // getContainerForShare(),
      ],
    );
  }

  Widget getContainerForShare() {
    if (true) {
      return Positioned(
        top: 40,
        right: 15,
        child: GestureDetector(
          child: Container(
            height: 20,
            width: 25,
            child: Image.asset(
              'assests/share.png',
            ),
          ),
          onTap: () {
            widget.showBottomSheet(true);

            Map<String, String> eventData = {
              'share': "all_share",
              'content': widget.movieDetail["movieName"].toString(),
            };
            //platform.invokeMethod('logCompleteTutorialEvent', eventData);
          },
        ),
      );
    } else {
      return Container();
    }
  }

  Widget getContainerForHeadingAndRating(
      double screenHeight, double screenWidth, bool vertical, String imageurl) {
    if (movieDetail["moviePosterWidth"] != null &&
        movieDetail["moviePosterHeight"] != null &&
        movieDetail["moviePosterWidth"].toString().length > 0 &&
        movieDetail["moviePosterHeight"].toString().length > 0 &&
        (double.parse(movieDetail["moviePosterWidth"]) >
            double.parse(movieDetail["moviePosterHeight"]))) {
      vertical = false;
    }
    // checkNetworkImageDimentions(imageUrl)
    return Positioned(
      bottom: widget.showOriginalPosters
          ? screenHeight * 0.10
          : screenHeight * 0.15,
      child: Container(
        margin: EdgeInsets.only(
          bottom: vertical || !widget.showOriginalPosters ? 0 : 30,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            widget.showOriginalPosters
                ? getMovieHeading(screenWidth, imageurl, vertical)
                : Center(
                    child: getMovieHeading(screenWidth, imageurl, vertical),
                  ),
            // MovieDetailUnderline(),
            widget.showOriginalPosters ? getMovieYearLanguage() : Container(),
            widget.ratingsFlag != null &&
                    widget.ratingsFlag &&
                    !widget.showOriginalPosters
                ? getMovieRating(screenHeight)
                : Container(
                    margin: EdgeInsets.only(
                        // top: 2,
                        // bottom: 25,
                        ),
                    height: 50,
                    width: screenWidth - 10.0,
                  ),
          ],
        ),
      ),
    );
  }

  Widget getMovieYearLanguage() {
    String lang = "";

    if (widget.movieDetail["canonicalLanguage"] != null) {
      lang = widget.movieDetail["canonicalLanguage"];
    } else if (widget.movieDetail["primaryLanguage"] != null) {
      lang = widget.movieDetail["primaryLanguage"];
    } else if (widget.movieDataFromCall["language"] != null) {
      lang = widget.movieDataFromCall["language"];
    } else if (widget.movieDataFromCall["similarMovieLanguage"] != null) {
      lang = widget.movieDataFromCall["similarMovieLanguage"];
    } else {
      lang = "";
    }
    String year = "";

    if (widget.movieDetail["year"] != null) {
      year = widget.movieDetail["year"].toString();
    } else if (widget.movieDataFromCall["year"] != null) {
      year = widget.movieDataFromCall["year"].toString();
    } else if (widget.movieDataFromCall["similarMovieYear"] != null) {
      year = widget.movieDataFromCall["similarMovieYear"].toString();
    } else {
      year = "";
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Text(
            year != null ? year : "",
            style: TextStyle(
              color: Constants.appColorFont,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            left: 10,
          ),
          child: Text(
            "|",
            style: TextStyle(
              color: Constants.appColorFont,
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(
            left: 10,
          ),
          child: Text(
            lang != null && lang.length > 1 ? lang.toUpperCase() : "",
            style: TextStyle(
              color: Constants.appColorFont,
            ),
          ),
        ),
      ],
    );
  }

  Widget getMovieHeading(double screenWidth, String imageurl, bool vertical) {
    bool flag = false;
    if (widget.movieDetail != null && widget.movieDetail.length > 1) {
      flag = true;
    }
    // String imageurl = "";

    // if (!flag) {
    //   if (widget.movieDataFromCall["moviePhoto"] != null) {
    //     imageurl = widget.movieDataFromCall["moviePhoto"];
    //   } else if (widget.movieDataFromCall["image_uri"] != null) {
    //     imageurl = widget.movieDataFromCall["image_uri"];
    //   } else if (widget.movieDataFromCall["full_image_uri"] != null) {
    //     imageurl = widget.movieDataFromCall["full_image_uri"];
    //   } else if (widget.movieDataFromCall["fullMoviePhoto"] != null) {
    //     imageurl = widget.movieDataFromCall["fullMoviePhoto"];
    //   } else if (widget.movieDataFromCall["similarMoviePhoto"] != null) {
    //     imageurl = widget.movieDataFromCall["similarMoviePhoto"];
    //   }
    // } else {
    //   if (widget.movieDetail["ogimage"] != null &&
    //       widget.movieDetail["ogimage"].isNotEmpty) {
    //     imageurl = imageurl != widget.movieDetail["ogimage"]
    //         ? widget.movieDetail["ogimage"]
    //         : imageurl;
    //   } else if (widget.movieDetail["moviePoster"] != null &&
    //       widget.movieDetail["moviePoster"].isNotEmpty) {
    //     imageurl = imageurl != widget.movieDetail["moviePoster"]
    //         ? widget.movieDetail["moviePoster"]
    //         : imageurl;
    //   } else if (widget.movieDetail["movieHorizontalPoster"] != null &&
    //       widget.movieDetail["movieHorizontalPoster"].isNotEmpty) {
    //     imageurl = imageurl != widget.movieDetail["movieHorizontalPoster"]
    //         ? widget.movieDetail["movieHorizontalPoster"]
    //         : imageurl;
    //   }
    // }

    String movieName;
    if (flag) {
      movieName = widget.movieDetail["movieName"];
    } else if (widget.movieDataFromCall["movieName"] != null) {
      movieName = widget.movieDataFromCall["movieName"];
    } else if (widget.movieDataFromCall["title"] != null) {
      movieName = widget.movieDataFromCall["title"];
    } else if (widget.movieDataFromCall["similarMovieName"] != null) {
      movieName = widget.movieDataFromCall["similarMovieName"];
    } else {
      movieName = "";
    }

    return Container(
      margin: EdgeInsets.only(
        bottom: widget.showOriginalPosters ? 0 : 50,
      ),
      child: Column(
        crossAxisAlignment: widget.showOriginalPosters
            ? CrossAxisAlignment.center
            : CrossAxisAlignment.start,
        children: <Widget>[
          widget.showOriginalPosters
              ? Container(
                  margin: EdgeInsets.only(
                    bottom: vertical ? 0 : 5,
                  ),
                  decoration: BoxDecoration(
                    border: new Border.all(
                      color: Constants.appColorFont,
                      width: 1,
                    ),
                    // borderRadius: new BorderRadius.all(
                    //   new Radius.circular(20.0),
                    // ),
                    color: Constants.appColorL2,
                  ),
                  height: vertical ? 200 : 126,
                  width: vertical ? 126 : 200,
                  child: Image(
                    image: NetworkImage(
                      imageurl,
                    ),
                    fit: vertical ? BoxFit.fitHeight : BoxFit.fitWidth,
                  ),
                )
              : Container(),
          MovieDetailHeading(
            movieName: movieName,
            showOriginalPosters: widget.showOriginalPosters,
          ),
        ],
      ),
    );
  }

  Widget getMovieRating(double screenHeight) {
    return Container(
      child: MovieDetailRating(
        movieDetail: widget.movieDetail,
        showOriginalPoster: widget.showOriginalPosters,
        cardFor: 0,
      ),
      margin: EdgeInsets.only(
        top: widget.showOriginalPosters ? screenHeight * 0.05 : 0,
      ),
    );
  }

  Widget basedOnAuthStatus() {
    if (!widget.authenticationToken.isEmpty && !widget.movieDetail.isEmpty) {
      return userPreferenceChoices(true);
    } else {
      return userPreferenceChoices(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    isSeen = widget.movieDetail["seen"];
    printIfDebug("init check" + isSeen.toString());
    return basedOnAuthStatus();
  }
}
