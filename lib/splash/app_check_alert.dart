// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class AppCheckAlert extends StatefulWidget {
  AppCheckAlert({
    this.createAppsList,
    this.updateAppPermission,
    this.changeappsChecked,
    this.changeproceedFlag,
  });
  final createAppsList;
  final updateAppPermission;
  final changeappsChecked;
  final changeproceedFlag;

  @override
  AppCheckAlertState createState() => AppCheckAlertState();
}

class AppCheckAlertState extends State<AppCheckAlert> {
  bool progressFlag = false, noFlag = false;

  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return AlertDialog(
      backgroundColor: Constants.appColorL2,
      content: Container(
        height: screenWidth / 3.5,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: 10,
              ),
              child: Text(
                "Autoselect streaming apps you have installed on your device?",
                style: TextStyle(
                  color: Constants.appColorFont,
                  fontSize: 12,
                  height: 1.5,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Spacer(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color:
                          !progressFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Yes',
                      style: TextStyle(
                        fontSize: 13,
                        color: progressFlag
                            ? Constants.appColorL2
                            : Constants.appColorLogo,
                      ),
                    ),
                  ),
                  onTap: () async {
                    setState(() {
                      progressFlag = true;
                    });

                    Future.delayed(const Duration(milliseconds: 500), () async {
                      // List<Application> appsIn =
                      //     await DeviceApps.getInstalledApplications();

                      // widget.createAppsList(appsIn);
                      widget.updateAppPermission(true);
                    });

                    Navigator.of(context).pop();
                  },
                ),
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color: !noFlag ? Constants.appColorL2 : Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      ' No ',
                      style: TextStyle(
                        color: noFlag ? Constants.appColorL2 : Constants.appColorLogo,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      noFlag = true;
                    });
                    Future.delayed(const Duration(milliseconds: 200), () {
                      widget.changeappsChecked(true);

                      widget.updateAppPermission(false);
                    });

                    Future.delayed(const Duration(milliseconds: 1000), () {
                      widget.changeproceedFlag(true);
                    });
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
