import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/main.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
// import 'dart:io' show Platform;
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/splash/movie_tutorial_page.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:firebase_remote_config/firebase_remote_config.dart';
// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
// import 'package:device_info/device_info.dart';
// import  'package:get_ip/get_ip.dart';

import 'package:firebase_messaging/firebase_messaging.dart'; // keshav imported
import 'package:flixjini_webapp/common/push_notification.dart';
import 'package:uuid/uuid.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/google_advertising_id.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:shake/shake.dart';
// import 'package:package_info/package_info.dart';

ShakeDetector? detector;

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    printIfDebug("myBackgroundMessageHandler data" + data.toString());
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    printIfDebug(
        "myBackgroundMessageHandler notification" + notification.toString());
  }

  // Or do other work.
}

class MovieLandingPage extends StatefulWidget {
  MovieLandingPage({
    Key? key,
    this.navigatorKey,
    this.context,
  }) : super(key: key);
  final navigatorKey;
  final context;

  @override
  _MovieLandingPageState createState() => new _MovieLandingPageState();
}

class _MovieLandingPageState extends State<MovieLandingPage> {
  // static const platform = const MethodChannel('api.komparify/advertisingid');
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  String authenticationToken = "", googleAdId = "";
  String uuidToken = "";
  bool tutorialNeeded = false,
      notificFlag = false,
      indexCatDetailFlag = false,
      indexTopBottomDetailFlag = false,
      freeFlag = false,
      showOriginalPosters = true;
  var indexDetail = {}, indexCatDetail = {}, indexTopBottomDetail = {};

  bool remoteConfigReady = false;
  bool pushNotificationReady = false;
  bool deviceInformationReady = false;
  // static final facebookAppEvents = FacebookAppEvents();
  static final navigatorKey = GlobalKey<NavigatorState>();

  // AppTrack Information
  String stringDeviceInformation = "";

  // Cached Variables - Remote Config Data
  String stringFilterData = "";
  String stringSecondaryMenu = "";
  String stringPopularMovies = "";

  // For Dynamic Links
  String linkMessage = "", fcmToken = "";
  bool isCreatingLink = false;

  // For Push Notification - Firebase Messaging
  String _homeScreenText = "Waiting for token...";

  late final FirebaseMessaging _firebaseMessaging;

  final Map<String, Item> _items = <String, Item>{};
  final Connectivity _connectivity = Connectivity();
  // StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    printIfDebug("Landing page 1");
    checkShowOriginalPosters();
    printIfDebug("Landing page 2");
    retriveDeviceInformationStatus();
    printIfDebug("Landing page 3");
    // logUserScreen('Flixjini Landing Page', 'MovieLandingPage');
    setAppOpenCount();
    printIfDebug("Landing page 4");
    callGetGoogleAdId();
    printIfDebug("Landing page 5");
    initConnectivity();
    printIfDebug("Landing page 6");
    checkShakeSwitch();
    printIfDebug("Landing page 7");

    // getFreeFlagSP();
    super.initState();
  }

  checkShowOriginalPosters() async {
    String showOriginalPostersFromFRC =
        await getStringFromFRC('show_original_posters');
    if (showOriginalPostersFromFRC == 'false') {
      setState(() {
        this.showOriginalPosters = false;
      });
    } else {
      setState(() {
        this.showOriginalPosters = true;
      });
    }
  }

  checkShakeSwitch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool shakeSwitch = prefs.getBool("shakeSwitch") ?? true;
    prefs.setBool("shakeSwitch", shakeSwitch);
    detector = ShakeDetector.waitForStart(
      onPhoneShake: () {
        printIfDebug("shake is happening");
        shakeFunc();
      },
    );
    if (shakeSwitch) {
      detector!.startListening();
    } else {
      detector!.stopListening();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  initConnectivity() async {
    // facebookAppEvents.setAutoLogAppEventsEnabled(true);

    ConnectivityResult? result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } catch (e) {
      printIfDebug("_connectivity" + e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
    switch (result) {
      case ConnectivityResult.wifi:
        printIfDebug("Landing page 8");
        checkInternet();
        break;
      case ConnectivityResult.mobile:
        printIfDebug("Landing page 9");
        checkInternet();

        break;
      case ConnectivityResult.none:
        printIfDebug("Landing page 10");
        Future.delayed(const Duration(milliseconds: 1000), () {
          Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
              builder: (
                BuildContext context,
              ) =>
                  new MovieRouting(),
            ),
            (Route<dynamic> route) => false,
          );
        });

        break;
      default:
        printIfDebug("Landing page 11");
        printIfDebug("  failed net");
        break;
    }
  }

  checkInternet() {
    printIfDebug("Landing page 12");
    pushNotificationSetup();
    callGetReferrerDetails();
  }

  Item _itemForMessage(Map<String, dynamic> message) {
    final String itemId = message['data']['id'];
    final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
      ..status = message['data']['status'];
    return item;
  }

  shakeFunc() {
    printIfDebug("shake is done");
    try {
      MyApp.pushScreen("genie", showOriginalPosters);
    } catch (e) {
      printIfDebug(e);
    }
  }

  getFreeFlagSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? flag1 = prefs.getBool('freeflag');
    if (flag1 == null) {
      flag1 = false;
    }
    setState(() {
      freeFlag = flag1!;
    });
    getFcmidGaidAndroidid();
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
    });
    // printIfDebug("Index Page Authentication Report");
    // printIfDebug(authenticationToken);
  }

  uuidStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uuidToken = (prefs.getString('uuidToken') ?? "");
      prefs.setString('uuidToken', uuidToken);
    });
    // printIfDebug("UUID");
    // printIfDebug(uuidToken);
  }

  retriveDeviceInformationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringDeviceInformation =
          (prefs.getString('stringDeviceInformation') ?? "");
      prefs.setString('stringDeviceInformation', stringDeviceInformation);
    });
    // printIfDebug("\n\nexisting app track info from sp: $stringDeviceInformation");
  }

  setDeviceInformationStatus(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringDeviceInformation =
          (prefs.getString('stringDeviceInformation') ?? "");
      prefs.setString('stringDeviceInformation', data);
    });
    // printIfDebug("Updated App Track Information");
    // printIfDebug(data);
  }

  void callGetReferrerDetails() {
    // if (true) {
    _getReferrerDetails();
    // } else if (true) {}
  }

  Future<void> _getReferrerDetails() async {
    String komparifyCheckerAPIEndpoint =
            "https://api.flixjini.com/appsflyer_checker?",
        retail9CheckerAPIEndpoint =
            "https://www.retail9.com/appsflyer_checker.php?";

    String spCheckerAPIFired = await getStringFromSP('spCheckerAPIFired');

    if (spCheckerAPIFired == null || spCheckerAPIFired == '') {
      // checker api is not fired

      try {
        // final PendingDynamicLinkData? data =
        //     await FirebaseDynamicLinks.instance.getInitialLink();

        // final Uri deepLink = data!.link;
        // googleAdId = await getGoogleAdId();
        // if (deepLink != null) {
        //   // install is via firebase dynamic link
        //   getDataFromDeepLink(
        //     deepLink,
        //     komparifyCheckerAPIEndpoint,
        //     retail9CheckerAPIEndpoint,
        //   );
        // } else {
        //   // install is not via firebase dynamic link, so get details from google play install referrer api
        //   getReferrerDetailsFromInstallReferrerAPI(
        //     komparifyCheckerAPIEndpoint,
        //     retail9CheckerAPIEndpoint,
        //   );
        // }

        /* begin // platform dependent code: comment for android and uncomment for ios 
        // FirebaseDynamicLinks.instance.onLink(
        //     onSuccess: (PendingDynamicLinkData dynamicLink) async {
        //   final Uri deepLink = dynamicLink?.link;
        //   if (deepLink != null) {
        //     getDataFromDeepLink(
        //       deepLink,
        //       komparifyCheckerAPIEndpoint,
        //       retail9CheckerAPIEndpoint,
        //     );
        //   }
        // }, onError: (OnLinkErrorException e) async {
        //   printIfDebug(
        //     '\n\nexception caught in listener, onLinkError is: ${e.message}',
        //   );
        // });
        end */
      } catch (e) {
        getReferrerDetailsFromInstallReferrerAPI(
          komparifyCheckerAPIEndpoint,
          retail9CheckerAPIEndpoint,
        );
      }

      setStringToSP('spCheckerAPIFired', 'true');
    } // else checker api already fired
  }

  void getReferrerDetailsFromInstallReferrerAPI(
    String komparifyCheckerAPIEndpoint,
    String retail9CheckerAPIEndpoint,
  ) async {
    const platform = const MethodChannel('api.komparify/advertisingid');
    var newReferrerDetailsMap = {};
    // await platform.invokeMethod('myGetReferrerDetailsMethod');
    String iReferrerString = newReferrerDetailsMap['installReferrer'];
    // printIfDebug('\n\ninstall referrer string: $iReferrerString');
    if (iReferrerString.contains('not') ||
        iReferrerString.contains('set') ||
        iReferrerString == null ||
        iReferrerString == '') {
      // play install referrer api failed
      getValueFromIPBasedTracking(
        komparifyCheckerAPIEndpoint,
        retail9CheckerAPIEndpoint,
      );
    } else {
      // play install referrer api success
      make2Calls(
        iReferrerString,
        komparifyCheckerAPIEndpoint,
        retail9CheckerAPIEndpoint,
        googleAdId,
        'play_install_referrer',
      );
    }
  }

  void getDataFromDeepLink(
    Uri deepLink,
    String komparifyCheckerAPIEndpoint,
    String retail9CheckerAPIEndpoint,
  ) {
    String referrerDetails =
        getReferrerDetailsFromFirebaseDynamicLink(deepLink);
    if (referrerDetails.contains('not') ||
        referrerDetails.contains('set') ||
        referrerDetails == null ||
        referrerDetails == '') {
      // fdl failed
      getReferrerDetailsFromInstallReferrerAPI(
          komparifyCheckerAPIEndpoint, retail9CheckerAPIEndpoint);
    } else {
      // fdl success
      make2Calls(
        referrerDetails,
        komparifyCheckerAPIEndpoint,
        retail9CheckerAPIEndpoint,
        googleAdId,
        'firebase_dynamic_link',
      );
    }
  }

  void make2Calls(
    String referrerValue,
    String endpoint1,
    String endpoint2,
    String googleAdId,
    String methodUsed,
  ) {
    makeHTTPGETRequest(
      referrerValue,
      endpoint1,
      methodUsed,
      googleAdId,
    );
    makeHTTPGETRequest(
      referrerValue,
      endpoint2,
      methodUsed,
      googleAdId,
    );
  }

  void getValueFromIPBasedTracking(
    String komparifyCheckerAPIEndpoint,
    String retail9CheckerAPIEndpoint,
  ) async {
    String ipBasedTrackingEndpoint =
        "https://api.flixjini.com/check_reward_redirect?app=flixjini";
    try {
      http.get(
        Uri.parse(ipBasedTrackingEndpoint),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        var resBody = json.decode(response.body);
        // printIfDebug('\n\ndecoded response: $resBody');
        String referrerValueFromIPBasedTracking = resBody['aff_sub'];
        make2Calls(
          referrerValueFromIPBasedTracking,
          komparifyCheckerAPIEndpoint,
          retail9CheckerAPIEndpoint,
          googleAdId,
          'ip_based_tracking',
        );
      });
    } catch (exception) {
      printIfDebug(
        '\n\nip based tracking endpoint, exception caught in making http get request is: $exception',
      );
    }
  }

  String getReferrerDetailsFromFirebaseDynamicLink(Uri deepLink) {
    // printIfDebug('\n\ndeep link from fdl: ${deepLink.toString()}');
    Map myMap = deepLink.queryParameters;
    String referrerDetails = myMap["referrer"];
    // printIfDebug('\n\nfrom firebase dynamic links, referrerDetails: $referrerDetails');
    return referrerDetails;
  }

  Future<void> callGetGoogleAdId() async {
    googleAdId = await getGoogleAdId();
  }

  Widget _buildDialog(BuildContext context, Item item) {
    return AlertDialog(
      content: Text("Item ${item.itemId} has been updated"),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            if (1 == 2) {
              _showItemDialog({});
            }
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(Map<String, dynamic> message) {
    // printIfDebug("show item dialog");
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, _itemForMessage(message)),
    ).then((bool? shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final Item item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  Future<void> pushNotificationSetup() async {
    // _firebaseMessaging.setAutoInitEnabled(true);
    // _firebaseMessaging. configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     printIfDebug("\n\nonMessage: $message");
    //     // launchURL('http://www.google.com');
    //     // processFCMMessage(
    //     //     message, 'message'); // construct notification and send it

    //     // _showItemDialog(message); // keshav code
    //   },
    //   onBackgroundMessage: myBackgroundMessageHandler,
    //   onLaunch: (Map<String, dynamic> message) async {
    //     setState(() {
    //       notificFlag = true;
    //     });
    //     printIfDebug("\n\nonLaunch: $message");
    //     printIfDebug('\n\ntype of the above msg: ${message.runtimeType}');
    //     processFCMMessage(
    //         message, 'launch'); // construct notification and send it

    //     // _navigateToItemDetail(message); // keshav code
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     printIfDebug(
    //         '\n\n*********************************************** ON RESUME');
    //     printIfDebug("\n\nonResume: $message");
    //     // launchURL('http://www.google.com');
    //     processFCMMessage(
    //         message, 'resume'); // construct notification and send it

    //     // _navigateToItemDetail(message); // keshav code
    //   },
    // );

    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(sound: true, badge: true, alert: true));
    // if (true) {
    //   _firebaseMessaging.getToken().then((String token) {
    //     assert(token != null);
    //     // set fcm token in shared pref
    //     setFCMTokenInSP(token);
    //     setState(() {
    //       _homeScreenText = "Push Messaging token: $token";
    //     });
    //     // String gcmId = token;
    //     // printIfDebug("gcmId from fcm push notification setup: $gcmId");
    //     // // FCM ID
    //     // printIfDebug("Got Message");
    //     // printIfDebug(_homeScreenText);
    //     // setPushNotification(true);
    //     getFcmidGaidAndroidid();
    //   });
    // } else if (true) {
    //   // _firebaseMessaging.requestNotificationPermissions();
    //   String gcmId = "";
    //   // printIfDebug(
    //   //     "\n\ngcm id initialised to an empty string and the value is: $gcmId");
    //   _firebaseMessaging.onIosSettingsRegistered
    //       .listen((IosNotificationSettings settings) async {
    //     // printIfDebug("Settings registered: $settings");
    //     // printIfDebug("Heloooooo");
    //     await _firebaseMessaging
    //         .getToken()
    //         .timeout(Duration(milliseconds: 3000))
    //         .then((String token) {
    //       assert(token != null);
    //       setState(() {
    //         _homeScreenText = "Push Messaging token: $token";
    //       });
    //       gcmId = token;
    //       // FCM ID
    //       // printIfDebug("Got Message");
    //       // printIfDebug(_homeScreenText);
    //       // setPushNotification(true);
    //     });
    //     getFcmidGaidAndroidid();
    //   });
    // }
    getFcmidGaidAndroidid();
  }

  void processFCMMessage(Map<String, dynamic> message, String event) {
    // getting data from fcm json
    String nAction =
        message['data']['action']; // action tells where to take the user to
    // String nType = message['data']['type'];
    // String nTitle = message['data']['title'];
    // String nBigPicture = message['data']['bigPicture'];
    String forFlixjini = message['data']['flixjini'];
    String forKomparify = message['data']['komparify'];
    String forIReff = message['data']['iReff'];

    // printIfDebug(
    //     '\n\njust printing the following 2 variables for the sake of clearing the unused variables warnings');
    // printIfDebug('\n\nnTitle: $nTitle, nBigPicture: $nBigPicture');

    if (forKomparify == null || forIReff == null) {
      forKomparify = 'true';
      forIReff = 'true';
    }

    // if (forFlixjini == 'true' || forKomparify == 'true' || forIReff == 'true') { // uncomment if u wanna allow flixjini, ireff, & komparify notifications
    if (forFlixjini == 'true') {
      // allows only allows flixjini notifications
      // construct notification and send it
      // printIfDebug(
      //     '\n\nnotification is for flixjini and notification type is: $nType');
      // getCustomNotification(
      // 'New message from Flixjini', nTitle, nTitle, nBigPicture, nTitle);

      if (event == 'resume') {
        // redirect user onLaunch or onResume
        // printIfDebug(
        //     '\n\nevent is onresume... so, re-directing the user to some page');
        if (nAction.contains('categories') || nAction.contains('tags')) {
          navigateToFilterPage(nAction);
        } else if (nAction.contains("filter")) {
          navigateToFiltersPage(nAction);
        } else if (nAction.contains("movie") || nAction.contains("tvshow")) {
          navigateToSomePage(nAction);
        } else if (nAction.contains("/page/")) {
          navigateToScreen(nAction);
        } else if (!nAction.contains("www.flixjini")) {
          openBrowser(nAction);
        }
      } else if (event == 'launch') {
        // printIfDebug('\n\nevent is, onlaunch so, re-directing the user to some page');
        if (nAction.contains('categories') || nAction.contains('tags')) {
          navigateToFilterPage(nAction);
        } else if (nAction.contains("filter")) {
          navigateToFiltersPage(nAction);
        } else if (nAction.contains("movie") || nAction.contains("tvshow")) {
          navigateToSomePage(nAction);
        } else if (nAction.contains("/page/")) {
          navigateToScreen(nAction);
        } else if (!nAction.contains("www.flixjini")) {
          openBrowser(nAction);
        }
      } else {
        // if (nAction.contains('categories') || nAction.contains('tags')) {
        //   navigateToFilterPage(nAction);
        // } else if (nAction.contains("filter")) {
        //   navigateToFiltersPage(nAction);
        // } else {
        //   navigateToSomePage(nAction);
        // }

        // printIfDebug(
        //     '\n\nevent is on message... so, not redirecting the user to some pages');
      }
    } else {
      openBrowser(nAction);
    }
  }

  void navigateToFilterPage(String deepLink) {
    if (deepLink.contains('www.flixjini.in/filter/tags')) {
      deepLink = deepLink.replaceAll(
          'www.flixjini.in/filter', 'api.flixjini.com/entertainment');
    } else if (deepLink.contains('www.flixjini.in/filter/categories')) {
      deepLink = deepLink.replaceAll(
          'www.flixjini.in/filter', 'api.flixjini.com/entertainment');
    }

    if (deepLink.contains("?")) {
      var splitted = deepLink.split("?");
      deepLink = splitted[0];
      deepLink += '.json?';

      deepLink = deepLink + splitted[1];
    } else {
      deepLink += '.json';
    }
    MyApp.pushFilterPage(deepLink, showOriginalPosters);
  }

  void navigateToFiltersPage(String deepLink) {
    deepLink = deepLink.replaceAll("www.flixjini.in", "api.flixjini.com");
    deepLink = deepLink.replaceAll("filter", "entertainment");
    deepLink = deepLink.replaceAll("?", ".json?");
    MyApp.pushFilterPage(deepLink, showOriginalPosters);
  }

  void navigateToSomePage(map) {
    String urlPhase = '/entertainment/movie/',
        movieName,
        uriScheme,
        uriHost,
        uriPath,
        action = map;
    // printIfDebug('\n\nbefore navigating, action present in the map is: $action');

    Uri myUri = Uri.parse(action);
    uriScheme = myUri.scheme;
    uriHost = myUri.host;
    uriPath = myUri.path;
    // printIfDebug(
    //     '\n\nscheme: $uriScheme, authority: $uriAuthority, host: $uriHost, path: $uriPath');

    if (uriScheme == 'https' &&
        uriHost == 'www.flixjini.in' &&
        (uriPath.contains('movie/') || uriPath.contains('tvshow/'))) {
      movieName = getMovieName(uriPath);
      if (!movieName.contains(".json")) {
        urlPhase += movieName + ".json";
      } else {
        urlPhase += movieName;
      }

      Map myMap = {
        "urlPhase": urlPhase,
        "fullUrl": null,
        "search_key": null,
      };
      // printIfDebug('\n\nnavigating user to "$movieName" movie details pg');
      // printIfDebug(myMap.toString());
      MyApp.pushMoviePage(myMap, showOriginalPosters);
    }
  }

  void navigateToScreen(String map) {
    String screen = "";
    var urlSplit = map.split("/page/");
    screen = urlSplit[1];

    MyApp.pushScreen(screen, showOriginalPosters);
  }

  openBrowser(link) {
    _launchURL(link);
  }

  void _launchURL(link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Constants.appColorL1,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      printIfDebug(e.toString());
    }
  }

  void takeUserToSomePlace(String action) async {
    Uri gaidAddedUri;
    String gaidAddedUriString, gaid;
    gaid = await getGAIDFromSP();
    // printIfDebug('\n\nfor appending gaid to the action url, gaid added is: $gaid');
    if ((action.contains('http') || action.contains('www.')) &&
        action.contains("filter")) {
      launchURL(action);
    } else if (action.contains('http') || action.contains('www.')) {
      // take user to web view
      gaidAddedUri = getGAIDAddedUri(action, gaid);
      gaidAddedUriString = gaidAddedUri.toString();
      // printIfDebug(
      //     '\n\ngaid added string b4 launching the web view is: $gaidAddedUriString');
      launchURL(gaidAddedUriString);
    } else {
      // deep link
      printIfDebug('\n\naction contains deep link');
    }
  }

  Uri getGAIDAddedUri(String s, String gaid) {
    // adds gaid to the action url for web view
    Uri? myUri, uri;
    uri = Uri.parse(s);
    // printIfDebug('parsed url: $uri');

    String scheme = uri.scheme;
    String host = uri.host;
    String path = uri.path;

    var qParams = {
      'ga_id': gaid,
    };

    if (scheme == 'http') {
      // printIfDebug('\n\nscheme is http');
      myUri = Uri.http(host, path, qParams);
    } else if (scheme == 'https') {
      // printIfDebug('\n\nscheme is https');
      myUri = Uri.https(host, path, qParams);
    } else if (s.contains('www.')) {
      // printIfDebug('\n\nneither http nor https... but contains www.');
      myUri = Uri.http(host, path, qParams);
    } else {
      // printIfDebug('\n\nscheme is neither http not https ');
    }

    // printIfDebug('\n\nnmy uri: $myUri');
    return myUri!;
  }

  void setFCMTokenInSP(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('spFCMToken', token);
    setState(() {
      fcmToken = token;
    });
    // printIfDebug('\n\nstoring in sp, this fcm token: $token');
  }

  setIsCreatingLink(bool value) {
    setState(() {
      isCreatingLink = value;
    });
  }

  setRemoteConfig(bool value) {
    setState(() {
      remoteConfigReady = value;
    });
  }

  setPushNotification(bool value) {
    setState(() {
      pushNotificationReady = value;
    });
  }

  setDeviceInformation(bool value) {
    setState(() {
      deviceInformationReady = value;
    });
  }

  setLinkMessage(String url) {
    setState(() {
      linkMessage = url;
    });
  }

  setIndexData(var response) {
    setState(() {
      indexDetail = response;
    });
  }

  fetchCachedRemoteConfigData(SharedPreferences prefs) {
    setState(() {
      stringFilterData = (prefs.getString('stringFilterData') ?? "");
      stringSecondaryMenu = (prefs.getString('stringSecondaryMenu') ?? "");
      // stringPopularMovies = (prefs.getString('stringPopularMovies') ?? "");
    });
  }

  // setCachedRemoteConfigData(
  //     SharedPreferences prefs, RemoteConfig remoteConfig) {
  //   setState(() {
  //     prefs.setString(
  //         'stringFilterData', remoteConfig.getString('filter_menu'));
  //     prefs.setString('stringSecondaryMenu',
  //         remoteConfig.getString('profile_and_secondary_menu'));
  //     // prefs.setString(
  //     //     'stringPopularMovies', remoteConfig.getString('popular_searches'));
  //     prefs.setString(
  //         'shareAppInfo', remoteConfig.getString('shareAppInfo') ?? "[]");
  //     Constants.shareMovieText =
  //         remoteConfig.getString('sharetextformovie') ?? "";
  //     Constants.shareTvshowText =
  //         remoteConfig.getString('sharetextfortvshow') ?? "";
  //   });
  // }

  fetchTutorialNeededStatus(SharedPreferences prefs) {
    if (this.mounted) {
      setState(() {
        tutorialNeeded = (prefs.getBool('tutorialNeeded') ?? true);
        prefs.setBool('tutorialNeeded', tutorialNeeded);
      });
    }
  }

  setTutorialNeededStatus(SharedPreferences prefs, value) {
    setState(() {
      prefs.setBool('tutorialNeeded', value);
    });
  }

  setAppOpenCount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    int count;
    count = prefs.getInt('appopencount')!;
    if (count != null) {
      prefs.setInt('appopencount', count + 1);
    } else {
      prefs.setInt('appopencount', 1);
    }
  }

  checkIfAppTrackRequestsNeedsToBeSent(deviceInformation) {
    String newDeviceInfo = json.encode(deviceInformation);
    if (newDeviceInfo == stringDeviceInformation) {
      printIfDebug("Perfect Match Don't send apptrack request");
      return false;
    } else {
      printIfDebug("Send apptrack request - Device Information has changed");
      return true;
    }
  }

  sendRequestToAppTrack(url, deviceInformation) async {
    // printIfDebug('\n\napp track api, url: $url, device info: $deviceInformation');
    try {
      var response = await http.post(url, body: deviceInformation);

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var appTrackResponse = json.decode(responseBody);
        printIfDebug("The response after calling app_track");
        printIfDebug(appTrackResponse);
      } else {
        // printIfDebug('Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!deviceInformationReady) {
      setDeviceInformation(true);
    }
    setupRemoteConfig();
    if (!mounted) return;
  }

  setUuid(token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      uuidToken = (prefs.getString('uuidToken') ?? "");
      prefs.setString('uuidToken', token);
    });
  }

  checkIfNewUuidNeeded() async {
    await uuidStatus();
    if (uuidToken.isEmpty) {
      // printIfDebug("constructed new uuid");
      var uuidData = new Uuid();
      String result = uuidData.v1();
      await setUuid(result);
      return result;
    } else {
      // printIfDebug("uuid already exists");
      return uuidToken;
    }
  }

  Future<void> getFcmidGaidAndroidid() async {
    printIfDebug("Landing page 13");
    var deviceInformation = {
      'android_id': "",
      'device_model': "",
      'device_maker': "",
      'app_name': "Flixjini",
      'app_version': "",
      'jwt_token': "",
    };
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
    });
    deviceInformation["jwt_token"] = authenticationToken;
    try {
      // if (true) {
      //   var result = await getGoogleAdId();
      //   if (result != null) {
      //     // printIfDebug(
      //     //     "\n\nno need for uuid, as the following gaid is obtained: $result");
      //     // printIfDebug(result);
      //     deviceInformation["ga_id"] = result;
      //   } else {
      //     String result = await checkIfNewUuidNeeded();
      //     // printIfDebug("\n\nuuid obtained is: $result");
      //     deviceInformation["ga_id"] = result;
      //   }
      // } else if (true) {
      //   String result = await checkIfNewUuidNeeded();
      //   // printIfDebug("uuid obtained is: $result");
      //   deviceInformation["ga_id"] = result;
      // } else {
      //   // printIfDebug("Unkown OS");
      // }
    } on PlatformException catch (e) {
      // printIfDebug("Failed to get Android Advertising Id: '${e.message}'.");
      String result = await checkIfNewUuidNeeded();
      // printIfDebug("uuid obtained");
      // printIfDebug(result);
      deviceInformation["ga_id"] = result;
      printIfDebug(e);
    }
    // if (true) {
    // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    // deviceInformation["android_id"] = androidInfo.androidId;
    // deviceInformation["device_model"] = androidInfo.model;
    // deviceInformation["device_maker"] = androidInfo.manufacturer;
    // deviceInformation["app_version"] = getVersionCodeForAndroid();
    // deviceInformation["os"] = 'android';
    // deviceInformation["gcm_id"] = fcmToken;
    // } else if (true) {
    // IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    // deviceInformation["device_model"] = iosInfo.model;
    // deviceInformation["device_maker"] = "Apple Inc";
    // deviceInformation["app_version"] = iosInfo.utsname.release;
    // deviceInformation["ga_id"] = '';
    // deviceInformation["idfa"] = iosInfo.identifierForVendor;
    // deviceInformation["os"] = 'ios';
    // deviceInformation["gcm_id"] = fcmToken;
    // } else {
    //   printIfDebug("Unkown OS");
    // }
    String url = "https://api.flixjini.com/app_track.json";
    // printIfDebug(
    //     "\n\nfor app_track api endpoint, the device info: $deviceInformation");
    bool appTrackSatus = false;
    appTrackSatus = checkIfAppTrackRequestsNeedsToBeSent(deviceInformation);
    if (appTrackSatus) {
      printIfDebug("Landing page 14");
      String data = json.encode(deviceInformation);
      setDeviceInformationStatus(data);
      sendRequestToAppTrack(url, deviceInformation);
    } else {
      printIfDebug("Landing page 15");
      setupRemoteConfig();
    }
  }

  setupRemoteConfig() async {
    try {
      // final RemoteConfig remoteConfig = await RemoteConfig.instance;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      fetchCachedRemoteConfigData(prefs);
      // Enable developer mode to relax fetch throttling
      // remoteConfig.setConfigSettings(RemoteConfigSettings(
      //     fetchTimeout: const Duration(seconds: 0),
      //     minimumFetchInterval: Duration(hours: 6)));
      // remoteConfig.setDefaults(<String, dynamic>{
      //   'filter_menu': stringFilterData,
      //   'profile_and_secondary_menu': stringSecondaryMenu,
      //   'popular_searches': stringPopularMovies
      // });
      // await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      // await remoteConfig.fetchAndActivate(); //activateFetched();
      // setCachedRemoteConfigData(prefs, remoteConfig);
    } catch (exception) {
      // printIfDebug("Remote Config Failed");
    }
    printIfDebug("Landing page 15");
    // setRemoteConfig(true);
    checkNeedForTutorial();
  }

  moveToTutorialPage() async {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => MovieTutorialPage()));
  }

  getMovieDetailsNew() async {
    String rootUrl = "";
    if (authenticationToken.isEmpty) {
      rootUrl = freeFlag
          ? 'https://api.flixjini.com/entertainment/fast_top.json?free_only=true&'
          : 'https://api.flixjini.com/entertainment/fast_top.json?';
    } else {
      rootUrl = freeFlag
          ? 'https://api.flixjini.com/entertainment/fast_top.json' +
              '?' +
              'jwt_token=' +
              authenticationToken +
              '&free_only=true&'
          : 'https://api.flixjini.com/entertainment/fast_top.json' +
              '?' +
              'jwt_token=' +
              authenticationToken +
              '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    // printIfDebug("URL after default params " + url.toString());

    try {
      var response = await http.get(Uri.parse(url));

      // printIfDebug("The Index Status Code");
      // printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        // printIfDebug(responseBody);
        var data = json.decode(responseBody);
        // printIfDebug("Value of Movie Id");
        // printIfDebug(data["carousel"]);
        this.indexTopBottomDetail = data;
        setState(() {
          indexTopBottomDetail = this.indexTopBottomDetail;
          indexTopBottomDetailFlag = true;
        });
        // callIndexPage();
      }
    } catch (SocketException) {
      printIfDebug(" " + SocketException.toString());
    }

    if (!mounted) return;

    printIfDebug("Data Fetched");
  }

  getMovieDetailsNew1() async {
    String rootUrl = "";
    if (authenticationToken.isEmpty) {
      rootUrl = freeFlag
          ? "https://api.flixjini.com/entertainment/only_film_strip.json?free_only=true&"
          : "https://api.flixjini.com/entertainment/only_film_strip.json?";
    } else {
      rootUrl = freeFlag
          ? 'https://api.flixjini.com/entertainment/only_film_strip.json' +
              '?' +
              'jwt_token=' +
              authenticationToken +
              '&free_only=true&'
          : 'https://api.flixjini.com/entertainment/only_film_strip.json' +
              '?' +
              'jwt_token=' +
              authenticationToken +
              '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    // printIfDebug("URL after default params " + url.toString());

    try {
      var response = await http.get(Uri.parse(url));

      // printIfDebug("The Index Status Code");
      // printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        // printIfDebug(responseBody);
        var data = json.decode(responseBody);
        // printIfDebug("Value of Movie Id");
        // printIfDebug(data["carousel"]);
        this.indexCatDetail = data;
        setState(() {
          indexCatDetail = this.indexCatDetail;
          indexCatDetailFlag = true;
        });
        callIndexPage();
      }
    } catch (SocketException) {
      printIfDebug(" " + SocketException.toString());
    }

    if (!mounted) return;
  }

  callIndexPage() {
    if (indexCatDetailFlag && indexTopBottomDetailFlag) {
      Navigator.of(context).pushAndRemoveUntil(
        new MaterialPageRoute(
          builder: (
            BuildContext context,
          ) =>
              new MovieRouting(
            indexCatDetail: indexCatDetail,
            indexTopBottomDetail: indexTopBottomDetail,
          ),
        ),
        (Route<dynamic> route) => false,
      );
    } else if (indexCatDetailFlag) {
      Navigator.of(context).pushAndRemoveUntil(
        new MaterialPageRoute(
          builder: (
            BuildContext context,
          ) =>
              new MovieRouting(
            indexCatDetail: indexCatDetail,
          ),
        ),
        (Route<dynamic> route) => false,
      );
    } else if (indexTopBottomDetailFlag) {
      Navigator.of(context).pushAndRemoveUntil(
        new MaterialPageRoute(
          builder: (
            BuildContext context,
          ) =>
              new MovieRouting(
            indexTopBottomDetail: indexTopBottomDetail,
          ),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }

  getIndexPageJson() async {
    // getMovieDetailsNew();
    // getMovieDetailsNew1();
    printIfDebug("Landing page 20");
    Future.delayed(const Duration(milliseconds: 000), () {
      if (!indexCatDetailFlag || !indexTopBottomDetailFlag) {
        printIfDebug("passing through this function onlyyyyy");
        Navigator.of(context).pushAndRemoveUntil(
          new MaterialPageRoute(
            builder: (
              BuildContext context,
            ) =>
                new MovieRouting(
              defaultTab: 2,
            ),
          ),
          (Route<dynamic> route) => false,
        );
      }
    });

    if (!mounted) return;
  }

  checkNeedForTutorial() async {
    printIfDebug("Landing page 16");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    fetchTutorialNeededStatus(prefs);
    if (tutorialNeeded) {
      setTutorialNeededStatus(prefs, false);
      printIfDebug("Landing page 17");
      moveToTutorialPage();
    } else if (notificFlag) {
      printIfDebug("Landing page 18");
      printIfDebug("notification received");
    } else {
      printIfDebug("Landing page 19");
      getIndexPageJson();
    }
  }

  prepareForPofileMenu(profileMenu) {
    for (int index = 0; index < profileMenu["topics"].length; index++) {
      if (!profileMenu["topics"][index]["belongsTo"].contains("profile")) {
        profileMenu["topics"].removeAt(index);
      }
    }
    return profileMenu;
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    return new Scaffold(
      //OPENING FLARE LOGO THEME
      backgroundColor: Constants.appColorL1,
      body: new Center(
        // child: new FlareActor(
        //   "assests/newlandingpageflare.flr",
        //   alignment: Alignment.center,
        //   fit: BoxFit.contain,
        //   animation: "logo",
        // ),
        child: Container(
          margin: EdgeInsets.only(
            left: 50,
            right: 50,
          ),
          child: Image.asset(
            "assests/theFlixjiniLogo.png",
            height: 70,
            width: 210,
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 50,
        color: Constants.appColorL1,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  right: 5,
                ),
                child: Image.asset("assests/indianflag.png"),
              ),
              Text(
                "Made In India",
                style: TextStyle(
                  color: Constants.appColorFont,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
