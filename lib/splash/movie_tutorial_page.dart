// import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
// import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
// import 'package:flare_flutter/flare_actor.dart';
import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class MovieTutorialPage extends StatefulWidget {
  @override
  _MovieTutorialPageState createState() => new _MovieTutorialPageState();
}

class _MovieTutorialPageState extends State<MovieTutorialPage> {
  int indexColor = 0;

  final SwiperController _swiperController = SwiperController();
  final steps = [
    "assests/tutorial_img1.png",
    "assests/tutorial_img2.png",
    "assests/tutorial_img3.png",
    "assests/tutorial_img4.png",
  ];

  final animationName = [
    "what",
    "go",
    "why",
    "where",
  ];

  @override
  void initState() {
    super.initState();

    logUserScreen('Flixjini Tutorial Page', 'MovieTutorialPage');
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget tutorialUi() {
    return Expanded(
      child: Container(
        // color: ,
        padding: EdgeInsets.only(top: 0.0),
        child: SizedBox(
          child: Swiper(
            controller: _swiperController,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: Center(
                  child: Image.asset(steps[index]),
                  // FlareActor(
                  //   steps[index],
                  //   alignment: Alignment.center,
                  //   fit: BoxFit.contain,
                  //   animation: animationName[index],
                  // ),
                ),
              );
            },
            itemCount: steps.length,
            viewportFraction: 1.0,
            autoplay: true,
            autoplayDelay: 4000,
            autoplayDisableOnInteraction: true,
            onIndexChanged: (int index) {
              onchange(index);
            },
          ),
        ),
      ),
      // flex: 8,
    );
  }

  onchange(index) {
    printIfDebug(index);
    setState(() {
      indexColor = index;
    });
  }

  Widget quickStart(BuildContext context) {
    return Expanded(
      child: new Container(
        padding: const EdgeInsets.only(left: 50.0, right: 50.0),
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => MovieAuthenticationPage(
                  type: true,
                  fromTutorialPage: true,
                ),
              ),
            );
          },
          child: new ClipRRect(
            borderRadius: new BorderRadius.circular(30.0),
            child: Container(
              height: 40.0,
              width: 200.0,
              color: Constants.appColorFont,
              child: SizedBox(
                width: 200.0,
                child: new Center(
                  child: Text(
                    'Get Started',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Constants.appColorL1,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget emptyBox() {
    return Expanded(
      child: Container(),
      flex: 1,
    );
  }

  Widget signUpButton(BuildContext context) {
    return Expanded(
      child: Container(
        child: FlatButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) =>
                    MovieAuthenticationPage(type: false)));
          },
          child: Text(
            'Sign In',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Constants.appColorFont),
            textAlign: TextAlign.left,
          ),
        ),
      ),
      flex: 1,
    );
  }

  Widget avilableOptions(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        bottom: 60,
        left: 20,
        right: 20,
      ),
      color: Constants.appColorL1,
      // padding: const EdgeInsets.all(0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          quickStart(context),
        ],
      ),
    );
  }

  Widget pageIcons() {
    return Container(
      padding: EdgeInsets.only(
        bottom: 60,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: Icon(
              Icons.fiber_manual_record,
              color: indexColor == 0
                  ? Constants.appColorLogo
                  : Constants.appColorFont,
              size: 15,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: Icon(
              Icons.fiber_manual_record,
              color: indexColor == 1
                  ? Constants.appColorLogo
                  : Constants.appColorFont,
              size: 15,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: Icon(
              Icons.fiber_manual_record,
              color: indexColor == 2
                  ? Constants.appColorLogo
                  : Constants.appColorFont,
              size: 15,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 10,
            ),
            child: Icon(
              Icons.fiber_manual_record,
              color: indexColor == 3
                  ? Constants.appColorLogo
                  : Constants.appColorFont,
              size: 15,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      body: Container(
        child: new ConstrainedBox(
          constraints: BoxConstraints(),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              tutorialUi(),
              pageIcons(),
              avilableOptions(context),
            ],
          ),
        ),
      ),
    );
  }
}
