import 'package:flixjini_webapp/splash/app_check_alert.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;
// import 'package:advertising_id/advertising_id.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:shared_preferences/shared_preferences.dart'; // platform dependent code
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:flixjini_webapp/common/constants.dart';

class CountrySelectorPage extends StatefulWidget {
  final String? authToken;

  CountrySelectorPage({
    this.authToken,
  });
  @override
  State<StatefulWidget> createState() {
    return CountrySelectorPageState();
  }
}

class CountrySelectorPageState extends State<CountrySelectorPage> {
  String languagesAndStreamingTypesJSONString = '';
  var languagesAndStreamingTypesJSON;
  bool jsonLoaded = false,
      filterDataReady = false,
      appsChecked = false,
      proceedFlag = false,
      routeFlag = false;
  var filterData = {};
  List appsInstalled = <String>[];
  List languageIP = <String>[];
  List userChosenLanguagesList = [],
      userChosenStreamingTypesList = [],
      userChosenContentTypesList = [];
  String countryChosen = "";
  FirebaseAnalytics analytics = FirebaseAnalytics();
  // static final facebookAppEvents = FacebookAppEvents();

  var sourceKeys = {
    "hotstar": "in.startv.hotstar",
    "erosnow": "com.erosnow",
    "hooq": "tv.hooq.android",
    "amazon-prime": "com.amazon.avod.thirdpartyclient",
    "netflix": "com.netflix.mediaclient",
    "jio-cinema": "com.jio.media.ondemand",
    "hungama-play": "com.hungama.movies",
    "voot": "com.tv.v18.viola",
    "sonyliv": "com.sonyliv",
    "bigflix": "com.bigflix.Movies",
    "google-play": "com.android.vending",
    "youtube-movies": "com.google.android.youtube",
    "spuul": "com.spuul.android",
    "viu": "com.vuclip.viu",
    "viki": "com.viki.android",
    "yupptv": "com.yupptv.yuppflix",
    "alt-balaji": "com.balaji.alt",
    "sun-nxt": "com.suntv.sunnxt",
    "airtel-xstream": "tv.accedo.airtel.wynk",
    "vodafone-play": "com.vodafone.vodafoneplay",
    "zee-5": "com.graymatrix.did",
    "hoichoi": "com.viewlift.hoichoi",
    "mxplayer": "com.mxtech.videoplayer.ad",
    "shemaroo": "com.saranyu.shemarooworld",
    "tata-sky": "com.ryzmedia.tatasky",
    "aha-video": "ahaflix.tv",
    "tvf": "com.tvf.tvfplay",
    "quibitv": "com.quibi.qlient",
    "tubitv": "com.tubitv",
    "voot-kids": "com.viacom18.vootkids",
  };
  static const platform = const MethodChannel('api.komparify/advertisingid');

  void initState() {
    super.initState();
    getFilterMenu();
    sendEvent();
    // loadJSON();
  }

  sendEvent() {
    //platform.invokeMethod('logStartTrialEvent');
  }

  changeappsChecked(bool ch) {
    setState(() {
      appsChecked = ch;
    });
  }

  changeproceedFlag(bool ch) {
    setState(() {
      proceedFlag = ch;
    });
  }

  getFilterMenu() async {
    String filterMenuOptionsUrl =
        'https://api.flixjini.com/entertainment/filter_options.json';
    printIfDebug("Load Menu from");
    printIfDebug(filterMenuOptionsUrl);
    printIfDebug(filterMenuOptionsUrl.length);
    String rootUrl = filterMenuOptionsUrl + '?';
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    printIfDebug(url);
    try {
      var response = await http.get(Uri.parse(url));

      printIfDebug("Inside try");
      if (response.statusCode == 200) {
        printIfDebug("200");
        String responseBody = response.body;
        printIfDebug(responseBody);
        var data = json.decode(responseBody);
        this.filterData = data;
        setState(() {
          languagesAndStreamingTypesJSON = this.filterData;
          filterDataReady = true;
        });
        printIfDebug("Data Fetched");
        printIfDebug(filterData);
        loadIPLanguage();
        checkAppsAlert();
        changeContentStatus(5);
      } else {
        this.filterData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      this.filterData["error"] = 'Failed getting IP address';
    }

    if (!mounted) return;
  }

  checkAppsAlert() async {
    // double screenwidth = MediaQuery.of(context).size.width;

    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AppCheckAlert(
          createAppsList: createAppsList,
          updateAppPermission: updateAppPermission,
          changeappsChecked: changeappsChecked,
          changeproceedFlag: changeproceedFlag,
        );
      },
    );
  }

  updateAppPermission(bool per) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("appCheckPermission", per);
  }

  createAppsList(List list) {
    appsInstalled.clear();
    for (int i = 0; i < list.length; i++) {
      // printIfDebug(list[i]["app_name"]);
      appsInstalled.add(list[i].packageName);
    }
    setState(() {
      appsInstalled = this.appsInstalled;
    });
    printIfDebug(appsInstalled);
    changeStreamStatus(0);
  }

  changeStreamStatus(int index) {
    int len = languagesAndStreamingTypesJSON["topics"][index]["options"].length;
    List<String> sourcesInstalled = <String>[];
    sourceKeys.forEach((k, v) {
      if (appsInstalled.contains(v)) {
        sourcesInstalled.add(k);
      }
    });
    for (int i = 0; i < len; i++) {
      if (sourcesInstalled.contains(languagesAndStreamingTypesJSON["topics"]
              [index]["options"][i]["filter_id"]) ||
          languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                  ["filter_id"] ==
              "google-play" ||
          languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                  ["filter_id"] ==
              "youtube-movies") {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
        userChosenStreamingTypesList.add(
            languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                ["filter_id"]);
      }
    }
    setState(() {
      languagesAndStreamingTypesJSON = this.languagesAndStreamingTypesJSON;
    });
    Future.delayed(const Duration(milliseconds: 2000), () {
      setState(() {
        appsChecked = true;
      });
    });
    Future.delayed(const Duration(milliseconds: 3500), () {
      setState(() {
        proceedFlag = true;
      });
    });
  }

  loadIPLanguage() async {
    String url =
        "https://api.komparify.com/entertainment/get_languages.json?jwt_token=" +
            widget.authToken! +
            "&";
    url = constructHeader(url);

    String defaulturl = await fetchDefaultParams(url);
    printIfDebug(defaulturl);
    try {
      var response = await http.get(Uri.parse(defaulturl));
      printIfDebug(
        '\n\njson response status code for movie details page: ' +
            response.statusCode.toString(),
      );

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        setState(() {
          languageIP = data;
        });
        printIfDebug("language check");
        printIfDebug(data.toString());
        changeLangStatus(4);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('\n\ndetails page api, exception caught is: $exception');
    }
  }

  changeLangStatus(int index) {
    int len = languagesAndStreamingTypesJSON["topics"][index]["options"].length;
    for (int i = 0; i < len; i++) {
      if (languageIP.contains(languagesAndStreamingTypesJSON["topics"][index]
          ["options"][i]["filter_id"])) {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
        userChosenLanguagesList.add(languagesAndStreamingTypesJSON["topics"]
            [index]["options"][i]["filter_id"]);
      }
    }
    setState(() {
      languagesAndStreamingTypesJSON = this.languagesAndStreamingTypesJSON;
      userChosenLanguagesList = this.userChosenLanguagesList;
    });
    printIfDebug("langguages  " + userChosenLanguagesList.toString());
  }

  String getListInString(List<dynamic> list) {
    String listInString = '';
    if (list.isEmpty) {
      return listInString;
    }
    for (int i = 0; i < list.length; i++) {
      printIfDebug('\n\n${list[i]}');
      listInString += list[i].toLowerCase();
      if (i != list.length - 1) {
        listInString += ',';
      }
    }
    printIfDebug('\n\nlist in string: $listInString');
    return listInString;
  }

  String getCountryCode() {
    switch (countryChosen) {
      case 'india':
        return 'IN';
      case 'singapore':
        return 'SG';
      default:
        return 'IN';
    }
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: tagName.toString().replaceAll("-", "").toLowerCase() + '_profile',
      parameters: <String, dynamic>{},
    );
    // facebookAppEvents.logEvent(
    //   name: tagName.toString().toLowerCase() + '_profile',
    //   parameters: {
    //     'page': 'profile',
    //   },
    //   valueToSum: 1,
    // );
    if (tagName.toString().toLowerCase() == "zee5") {
      //platform.invokeMethod('logSpendCreditsEvent');
    }
    if (tagName.toString().toLowerCase() == "hindi") {
      //platform.invokeMethod('logAchieveLevelEvent');
    }
    if (tagName.toString().toLowerCase() == "hotstar") {
      //platform.invokeMethod('logDonateEvent');
    }
  }

  Future<void> updateUserProfile() async {
    String url = await fetchDefaultParams(
        'https://api.flixjini.com/movies/update_user_profile.json?');

    url += '&jwt_token=${widget.authToken}';

    url += '&country_code=${getCountryCode()}';

    if (userChosenLanguagesList != null && userChosenLanguagesList.length > 0) {
      userChosenLanguagesList.forEach(
          (f) => _sendAnalyticsEvent(f.toString().replaceAll(" ", "")));
    }
    if (userChosenStreamingTypesList != null &&
        userChosenStreamingTypesList.length > 0) {
      userChosenStreamingTypesList.forEach(
          (f) => _sendAnalyticsEvent(f.toString().replaceAll(" ", "")));
    }
    url += '&languagelist=${getListInString(userChosenLanguagesList)}';
    url += '&sourceslist=${getListInString(userChosenStreamingTypesList)}';
    url += "&streamingtypelist=${getListInString(userChosenContentTypesList)}";

    printIfDebug('\n\nfinal url before making a request: $url');

    var response = await http.post(Uri.parse(url));
    Future.delayed(const Duration(milliseconds: 8000), () {
      if (!routeFlag) {
        printIfDebug("routed after 8 seconds");
        setState(() {
          routeFlag = true;
        });
        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) =>
                new MovieRouting(defaultTab: 2)));
      }
    });
    try {
      var responseBody = json.decode(response.body);
      printIfDebug('\n\nresponse body: $responseBody');
      // for checking
      // for (int i = 0; i < responseBody['topics'].length; i++) {
      //   for (int j = 0; j < responseBody['topics'][i]['options'].length; j++) {
      //     if (responseBody['topics'][i]['options'][j]['status'] == true) {
      //       printIfDebug('\n\n${responseBody['topics'][i]['options'][j]['name']}');
      //     }
      //   }
      // }
      setState(() {
        routeFlag = true;
      });
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieRouting(defaultTab: 2)));
    } catch (exception) {
      printIfDebug('\n\nupdate user profile api, got exception: $exception');
    }
  }

  void actionForLanguageOnTap(int i, int index) {
    String langTapped =
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]["name"];

    if (languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] ==
        true) {
      removeLanguageFromTheList(langTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = false;
      });
    } else {
      addLanguageToTheList(langTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
      });
    }
  }

  void removeLanguageFromTheList(String languageToRemove) {
    userChosenLanguagesList.remove(languageToRemove);
  }

  void addLanguageToTheList(languageTapped) {
    userChosenLanguagesList.add(languageTapped);
    printIfDebug(" " + userChosenLanguagesList.toString());
  }

  String getImageURL(String lId) {
    String baseImageURL =
        "https://komparify.sgp1.cdn.digitaloceanspaces.com/assets/flixjini/languages/";
    baseImageURL += lId + ".png";
    return baseImageURL;
  }

  Widget getButtonForNextPage() {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.all(
          20.0,
        ),
        decoration: BoxDecoration(
          color: Constants.appColorLogo,
          shape: BoxShape.circle,
        ),
        child: Icon(
          Icons.navigate_next,
          size: 40,
          color: Constants.appColorFont,
        ),
      ),
      onTap: () async {
        printIfDebug('\n\nuser chosen country: $countryChosen');
        printIfDebug('\n\nlanguages list: $userChosenLanguagesList');
        printIfDebug('\n\nstreaming types list: $userChosenStreamingTypesList');

        setStringToSP('userCountryCode', getCountryCode());

        widget.authToken != null
            ? updateUserProfile()
            : printIfDebug('\n\nauth token is null');

        Navigator.of(context).pushReplacement(new MaterialPageRoute(
            builder: (BuildContext context) =>
                new MovieRouting(defaultTab: 2)));
      },
    );
  }

  checkIfTitleNeeded(int index) {
    var setting = {};

    setting["numberOfOptionsPerRow"] = 5;
    setting["blockSize"] = 75.0;
    setting["childAspectRatio"] = 1.0;
    setting["crossAxisCount"] = 5;
    return setting;
  }

  bool checkIfAnyAreSelected(index) {
    bool flag = true;
    for (int i = 0;
        i < languagesAndStreamingTypesJSON["topics"][index]["options"].length;
        i++) {
      if (languagesAndStreamingTypesJSON["topics"][index]["options"][i]
          ["status"]) {
        flag = false;
        printIfDebug("Category Checking");
        printIfDebug(
            languagesAndStreamingTypesJSON["topics"][index]["heading"]);
        break;
      }
    }
    if (flag) {
      return true;
    } else {
      return false;
    }
  }

  Widget clickableImagesGrid(int index) {
    // var setting = checkIfTitleNeeded(index);
    // int numberOfOptionsPerRow = setting["numberOfOptionsPerRow"];
    bool selectedSectionStatus = checkIfAnyAreSelected(index);
    int numberOfSources =
        languagesAndStreamingTypesJSON["topics"][index]["options"].length;

    var orientation = MediaQuery.of(context).orientation;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getSubHeading(
            languagesAndStreamingTypesJSON["topics"][index]["heading"]),
        Container(
          padding: EdgeInsets.only(
            left: 10,
            right: 10,
            bottom: 10,
          ),
          child: GridView.builder(
              addAutomaticKeepAlives: false,
              shrinkWrap: true,
              physics: new NeverScrollableScrollPhysics(),
              itemCount:
                  numberOfSources, // controller: secondaryFilterController,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                crossAxisCount: (orientation == Orientation.portrait) ? 5 : 12,
              ),
              padding: EdgeInsets.all(
                10,
              ),
              itemBuilder: (BuildContext context1, int i) {
                return displayStreamingCheckboxes(
                    index, i, selectedSectionStatus);
              }),
        ),
      ],
    );
  }

  Widget displayStreamingCheckboxes(index, i, selectedSectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectedSectionStatus);
    return Container(
      child: new Card(
        elevation: 5.0,
        color: Constants.appColorL3,
        shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new GestureDetector(
          onTap: () {
            actionForStreamingTypesOnTap(i, index);

            printIfDebug("Send Request to server with User Preferences");
          },
          child: new Stack(
            children: <Widget>[
              new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  color: Constants.appColorL3,
                  padding: const EdgeInsets.all(0.0),
                  child: getStreamingSourceImage(
                    index,
                    i,
                  ),
                ),
              ),
              new Positioned.fill(
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(5.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getStreamingSourceImage(
    int index,
    int i,
  ) {
    // if (showOriginalImages == 'true') {
    return Container(
      color: Constants.appColorL3,
      child: Image(
        image: NetworkImage(languagesAndStreamingTypesJSON["topics"][index]
            ["options"][i]["image"]),
        fit: BoxFit.fill,
        gaplessPlayback: true,
      ),
    );
  }

  Widget buildStreamingSource(int index, int i, bool selectedSectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectedSectionStatus);
    return Container(
      color: Constants.appColorL3,
      // padding: const EdgeInsets.all(5),
      width: 35.0,
      height: 35.0,
      // child: new Card(
      //   color: Constants.appColorL1,
      //   shape: RoundedRectangleBorder(
      //     borderRadius: new BorderRadius.circular(8.0),
      //   ),
      child: new GestureDetector(
        onTap: () {
          // widget.updateClickableImages(index, i);
          actionForStreamingTypesOnTap(i, index);
        },
        child: new Stack(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(5),
              child: new ClipRRect(
                borderRadius: new BorderRadius.circular(5.0),
                child: new Container(
                  child: Image.network(
                    languagesAndStreamingTypesJSON["topics"][index]["options"]
                        [i]["image"],
                    fit: BoxFit.fill,
                    gaplessPlayback: true,
                  ),
                ),
              ),
            ),
            new Positioned.fill(
              child: Container(
                padding: const EdgeInsets.all(5),
                child: new ClipRRect(
                  borderRadius: new BorderRadius.circular(4.0),
                  child: new Container(
                    color: Constants.appColorL1.withOpacity(toggle),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      // ),
    );
  }

  void removeStreamingTypeFromTheList(String streamingTypeToRemove) {
    userChosenStreamingTypesList.remove(streamingTypeToRemove);
  }

  void actionForStreamingTypesOnTap(int i, int index) {
    String streamingTypeTapped =
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]["name"];

    if (languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] ==
        true) {
      removeStreamingTypeFromTheList(streamingTypeTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = false;
      });
    } else {
      addStreamingTypeToTheList(streamingTypeTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
      });
    }
  }

  void addStreamingTypeToTheList(streamingTypeTapped) {
    userChosenStreamingTypesList.add(streamingTypeTapped);
  }

  Widget clickableLangGrid(int index) {
    var setting = checkIfTitleNeeded(index);
    int numberOfOptionsPerRow = setting["numberOfOptionsPerRow"];
    bool selectedSectionStatus = checkIfAnyAreSelected(index);
    int numberOfSources =
        languagesAndStreamingTypesJSON["topics"][index]["options"].length;
    // widget.profileSettingData["topics"][index]["options"].length;
    double heightPerRowInSettings = setting["blockSize"];
    printIfDebug("Height");
    printIfDebug(heightPerRowInSettings *
        (((numberOfSources / numberOfOptionsPerRow).floor()) + 1));
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      // color: Constants.appColorL3,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Constants.appColorL3,
      ),
      margin: EdgeInsets.only(top: 5),
      // padding: const EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getSubHeading(
              languagesAndStreamingTypesJSON["topics"][index]["heading"]),
          Container(
            padding: EdgeInsets.all(10),
            child: new GridView.builder(
              physics: new NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              primary: true,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.8,
                crossAxisCount: (orientation == Orientation.portrait)
                    ? numberOfOptionsPerRow
                    : 12,
              ),
              itemCount: numberOfSources,
              scrollDirection: Axis.vertical,
              itemBuilder: (
                BuildContext context,
                int i,
              ) {
                return GridTile(
                  child: buildLangSource(
                    index,
                    i,
                    selectedSectionStatus,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  basedOnshadeAllOptions(int index, int i, bool selectedSectionStatus) {
    if (selectedSectionStatus) {
      return 0.0;
    } else {
      return languagesAndStreamingTypesJSON["topics"][index]["options"][i]
              ["status"]
          ? 0.0
          : 0.6;
    }
  }

  Widget buildLangSource(int index, int i, bool selectedSectionStatus) {
    double toggle = basedOnshadeAllOptions(index, i, selectedSectionStatus);
    return Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(5),
      // width: 25.0,
      // height: 25.0,
      // child: new Card(
      //   color: Constants.appColorL1,
      //   shape: RoundedRectangleBorder(
      //     borderRadius: new BorderRadius.circular(8.0),
      //   ),
      child: Column(
        children: <Widget>[
          new GestureDetector(
            onTap: () {
              // widget.updateClickableImages(index, i);
              actionForLanguageOnTap(i, index);
            },
            child: new Stack(
              children: <Widget>[
                new ClipRRect(
                  borderRadius: new BorderRadius.circular(4.0),
                  child: new Container(
                    padding: const EdgeInsets.all(5),
                    child: Image.network(
                      getImageURL(languagesAndStreamingTypesJSON["topics"]
                          [index]["options"][i]["filter_id"]),
                      fit: BoxFit.contain,
                      gaplessPlayback: true,
                    ),
                  ),
                ),
                new Positioned.fill(
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    child: new ClipRRect(
                      borderRadius: new BorderRadius.circular(30.0),
                      child: new Container(
                        color: Constants.appColorL1.withOpacity(toggle),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              // margin: EdgeInsets.only(
              //   top: 5,
              // ),
              child: Text(
                languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                    ["name"],
                style: TextStyle(
                  color: Constants.appColorFont,
                  fontSize: 8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
      // ),
    );
  }

  Widget clickableCheckboxGrid(int index) {
    // int numberOfOptionsPerRow = 2;
    // bool selectedSectionStatus = false;
    int numberOfSources =
        languagesAndStreamingTypesJSON["topics"][index]["options"].length;

    var orientation = MediaQuery.of(context).orientation;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getSubHeading(
            languagesAndStreamingTypesJSON["topics"][index]["heading"]),
        Container(
          // height: 200,
          child: GridView.builder(
              addAutomaticKeepAlives: false,
              shrinkWrap: true,
              physics: new NeverScrollableScrollPhysics(),
              itemCount: numberOfSources,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                crossAxisCount: (orientation == Orientation.portrait) ? 2 : 12,
              ),
              padding: EdgeInsets.all(
                10,
              ),
              itemBuilder: (BuildContext context1, int i) {
                return displayCheckboxes(context1, i, index);
              }),
        ),
      ],
    );
  }

  Widget getSubHeading(subheading) {
    return Container(
      margin: EdgeInsets.only(
        top: 15,
        left: 10,
        bottom: 10,
      ),
      child: Text(
        subheading.toString().toUpperCase(),
        overflow: TextOverflow.ellipsis,
        style: new TextStyle(
          fontWeight: FontWeight.normal,
          fontSize: 14.0,
          color: Constants.appColorFont,
        ),
        textAlign: TextAlign.center,
        maxLines: 2,
      ),
    );
  }

  Widget displayCheckboxes1(index, i, selectedSectionStatus) {
    return new Container(
      color: Constants.appColorL3,
      padding: const EdgeInsets.all(10.0),
      child: new InkWell(
        onTap: () {
          actionForContentTypesOnTap(i, index);
        },
        child: new Container(
          padding: const EdgeInsets.all(3.0),
          width: 100.0,
          height: 50.0,
          decoration: new BoxDecoration(
            color: languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                    ["status"]
                ? Constants.appColorLogo
                : Constants.appColorL3,
            border: new Border.all(
              color: languagesAndStreamingTypesJSON["topics"][index]["options"]
                      [i]["status"]
                  ? Constants.appColorLogo
                  : Constants.appColorL2,
              width: 3.0,
            ),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: new Center(
            child: new Text(
              languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                      ["name"]
                  .toString()
                  .toUpperCase(),
              overflow: TextOverflow.ellipsis,
              style: new TextStyle(
                fontWeight: FontWeight.normal,
                color: languagesAndStreamingTypesJSON["topics"][index]
                        ["options"][i]["status"]
                    ? Constants.appColorL1
                    : Constants.appColorFont,
                fontSize: 14,
                letterSpacing: 0.6,
              ),
              textScaleFactor: 0.65,
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
        ),
      ),
    );
  }

  Widget displayCheckboxes(context, i, index) {
    return Card(
      margin: EdgeInsets.only(
        top: 5,
        bottom: 5,
        left: 5,
        right: 5,
      ),

      color: Constants.appColorL3,
      // elevation: 5.0,
      child: Container(
        color: Constants.appColorL3,
        child: new InkWell(
          onTap: () {
            actionForContentTypesOnTap(i, index);
          },
          child: new Container(
            // margin: EdgeInsets.all(5.0),
            padding: EdgeInsets.all(5.0),

            // width: 100.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: languagesAndStreamingTypesJSON["topics"][index]["options"]
                      [i]["status"]
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            child: new Center(
              child: new Text(
                languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                        ["name"]
                    .toString()
                    .toUpperCase(),
                overflow: TextOverflow.ellipsis,
                style: new TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: languagesAndStreamingTypesJSON["topics"][index]
                          ["options"][i]["status"]
                      ? Constants.appColorL1
                      : Constants.appColorFont.withOpacity(0.75),
                ),
                textScaleFactor: 0.5,
                textAlign: TextAlign.center,
                maxLines: 3,
              ),
            ),
          ),
        ),
      ),
    );
  }

  changeContentStatus(int index) {
    int len = languagesAndStreamingTypesJSON["topics"][index]["options"].length;
    for (int i = 0; i < len; i++) {
      if (languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                  ["name"] ==
              "Free" ||
          languagesAndStreamingTypesJSON["topics"][index]["options"][i]
                  ["name"] ==
              "Subscription") {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
        userChosenContentTypesList.add(languagesAndStreamingTypesJSON["topics"]
            [index]["options"][i]["filter_id"]);
      }
    }
    setState(() {
      languagesAndStreamingTypesJSON = this.languagesAndStreamingTypesJSON;
    });
  }

  void actionForContentTypesOnTap(int i, int index) {
    String contentTypeTapped =
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]["name"];

    if (languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] ==
        true) {
      userChosenContentTypesList.remove(contentTypeTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = false;
      });
    } else {
      userChosenContentTypesList.add(contentTypeTapped);
      setState(() {
        languagesAndStreamingTypesJSON["topics"][index]["options"][i]
            ["status"] = true;
      });
    }
  }

  Widget getSelectors() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            // top: 10,
            bottom: 10,
            left: 0,
            right: 0,
          ),
          // padding: EdgeInsets.only(
          //   left: 20,
          //   right: 20,
          // ),
          child: Card(
            elevation: 2.5,
            color: Constants.appColorL2,
            child: clickableLangGrid(4),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 10,
            bottom: 10,
            left: 0,
            right: 0,
          ),
          // padding: EdgeInsets.only(
          //   left: 20,
          //   right: 20,
          // ),
          child: Card(
            elevation: 2.5,
            color: Constants.appColorL2,
            child: clickableImagesGrid(0),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 10,
            bottom: 10,
            left: 0,
            right: 0,
          ),
          // padding: EdgeInsets.only(
          //   left: 20,
          //   right: 20,
          // ),
          child: Card(
            elevation: 2.5,
            color: Constants.appColorL2,
            child: clickableCheckboxGrid(5),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!appsChecked) {
      return Scaffold(
        backgroundColor: Constants.appColorL1,
        body: Center(
          child: getProgressBarWithLoading(),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Constants.appColorL1,
          iconTheme: new IconThemeData(color: Constants.appColorDivider),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Set Your Preferences"),
              Container(
                color: Constants.appColorLogo,
                margin: EdgeInsets.only(
                  top: 5,
                ),
                height: 3,
                width: 100,
              ),
            ],
          ),
        ),
        backgroundColor: Constants.appColorL1,
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(10.0),
            color: Constants.appColorL1,
            child: getSelectors(),
          ),
        ),
        bottomNavigationBar: Container(
          height: 60,
          color: Constants.appColorL1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              !proceedFlag
                  ? getProgressBar()
                  : GestureDetector(
                      child: Container(
                        margin: EdgeInsets.only(
                            // right: 20,
                            ),
                        padding: EdgeInsets.only(
                          left: 40,
                          right: 40,
                          top: 10,
                          bottom: 10,
                        ),
                        decoration: new BoxDecoration(
                          color: Constants.appColorLogo,
                          border: new Border.all(
                            color: Constants.appColorLogo,
                          ),
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        child: Text(
                          'GET STARTED',
                          style: TextStyle(
                            fontSize: 13,
                            color: Constants.appColorL1,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onTap: () {
                        // checkAppsAlert();
                        setState(() {
                          appsChecked = false;
                        });
                        updateUserProfile();
                      },
                    ),
            ],
          ),
        ),
      );
    }
  }
}
