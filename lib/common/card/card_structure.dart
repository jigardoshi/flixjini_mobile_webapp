import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flixjini_webapp/livetv/live_tv_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:html_unescape/html_unescape.dart';

import 'dart:ui';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/detail/movie_detail_page.dart';

import 'package:flixjini_webapp/common/default_api_params.dart';
import 'dart:convert';

import 'package:flixjini_webapp/common/constants.dart';

import 'package:flixjini_webapp/utils/signin_alert.dart';

import 'package:flixjini_webapp/filter/movie_filter_page.dart';

import '../constants.dart';

class CardStructure extends StatefulWidget {
  CardStructure({
    Key? key,
    this.movie,
    this.sendRequest,
    this.basedOnUserQueue,
    this.basedonUserPreference,
    this.authenticationToken,
    this.index,
    this.showOriginalPosters,
    this.pagefor,
    this.queueIds,
    this.seenIds,
  }) : super(key: key);

  final movie;
  final sendRequest;
  final basedOnUserQueue;
  final basedonUserPreference;
  final authenticationToken;
  final index;
  final showOriginalPosters;
  final pagefor;
  final queueIds;
  final seenIds;

  @override
  _CardStructureState createState() => new _CardStructureState();
}

class _CardStructureState extends State<CardStructure> {
  bool userMenuStatus = false,
      isQueued = false,
      authenticationTokenStatus = false,
      isSeen = false;
  List seenIdsLocal = <String>[];
  late double screenHeight, screenWidth;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  toogleUserMenuStatus() {
    setState(() {
      userMenuStatus = userMenuStatus ? false : true;
    });
  }

  void initState() {
    super.initState();
    seenIdsLocal = widget.seenIds;
    updateIsQueued();
    // getSeenMovieIds();
    updateIsSeen();
  }

  // getSeenMovieIds() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   widget.seenIds = prefs.getStringList("seenids");
  //   printIfDebug("seenids :" + seenIds.toString());
  // }

  // addSeenMovieId(var movieid) async {
  //   printIfDebug("added seen " + movieid.toString());
  //   SharedPreferences prefs = await SharedPreferences.getInstance();

  //   if (seenIdsLocal == null) {
  //     seenIdsLocal = new List<String>();
  //     seenIdsLocal.add(movieid.toString());
  //   } else {
  //     seenIdsLocal.add(movieid.toString());
  //   }

  //   prefs.setStringList("seenids", seenIdsLocal);
  //   updateIsSeen();
  // }

  // removeSeenMovieId(var movieid) async {
  //   printIfDebug("removed seen " + movieid.toString());
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   if (seenIdsLocal == null) {
  //     seenIdsLocal = new List<String>();
  //     seenIdsLocal.remove(movieid.toString());
  //   } else {
  //     seenIdsLocal.remove(movieid.toString());
  //   }

  //   prefs.setStringList("seenids", seenIdsLocal);
  //   updateIsSeen();
  // }

  updateIsSeen() {
    isSeen = widget.movie["seen"];
  }

  updateIsQueued() {
    isQueued = widget.movie["in_queue"];
  }

  // authenticationStatus() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   setState(() {
  //     authenticationTokenStatus = true;
  //   });
  //   if (authenticationToken.isNotEmpty) {
  //     getPreferences();
  //   } else {
  //     setState(() {
  //       fetchedUserQueuePreference = true;
  //       fetchedUserPreference = true;
  //     });
  //   }
  // }

  sendSeenRequest(movie, actionUrl, entity) async {
    printIfDebug((widget.authenticationToken).isEmpty);
    if (widget.authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      bool flag = true;
      // if (entity == "seen") {
      //   flag = true;
      // }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
          'magic': "false",
          'app': "flixjini-mobile",
        };

        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          // printIfDebug("seen json: " + jsonResponse.toString());
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              showToast(isSeen ? 'Added to Seen' : "Removed from seen", 'long',
                  'bottom', 1, Constants.appColorLogo);

              // if (entity == "add") {
              //   updateUserQueue(movie);
              // } else {
              //   updateuserDetail(movie, entity);
              // }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  addCardType() {
    if (widget.movie["content_type"]
        .toString()
        .toLowerCase()
        .contains('movie')) {
      return new Positioned(
        top: 5.0,
        left: 8, // 5.0,
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            // color: Constants.appColorLogo,
            shape: BoxShape.circle,
          ),
          child: new Image.asset(
            "assests/movies.png",
            color: Constants.appColorFont,
            fit: BoxFit.cover,
            width: 18, // 13.0,
            height: 18, // 13.0,
          ),
        ),
      );
    } else {
      return new Positioned(
        top: 5.0,
        left: 5.0,
        child: new Container(
          padding: const EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            // color: Constants.appColorLogo,
            shape: BoxShape.circle,
          ),
          child: new Image.asset(
            "assests/tv_ic.png",
            color: Constants.appColorFont,
            fit: BoxFit.cover,
            width: 13.0,
            height: 13.0,
          ),
        ),
      );
    }
  }

  bool checkIfMoviImagePresent() {
    if (widget.movie.containsKey("moviePhoto") &&
        !widget.movie["moviePhoto"].isEmpty) {
      return true;
    } else if (widget.movie.containsKey("fullMoviePhoto") &&
        !widget.movie["fullMoviePhoto"].isEmpty) {
      return true;
    } else if (widget.movie.containsKey("fullLandscapeMoviePhoto") &&
        !widget.movie["fullLandscapeMoviePhoto"].isEmpty) {
      return true;
    } else if (widget.movie.containsKey("landscapeMoviePhoto") &&
        !widget.movie["landscapeMoviePhoto"].isEmpty) {
      return true;
    }
    return false;
  }

  Widget movieImageAvailable() {
    bool isVertical = true;
    String imageURL = widget.movie["moviePhoto"] != null &&
            !widget.movie["moviePhoto"].isEmpty
        ? widget.movie["moviePhoto"]
        : "";
    if (imageURL.isEmpty) {
      imageURL = widget.movie["fullMoviePhoto"] != null &&
              !widget.movie["fullMoviePhoto"].isEmpty
          ? widget.movie["fullMoviePhoto"]
          : "";
    }
    if (imageURL.isEmpty) {
      imageURL = widget.movie["fullLandscapeMoviePhoto"] != null &&
              !widget.movie["fullLandscapeMoviePhoto"].isEmpty
          ? widget.movie["fullLandscapeMoviePhoto"]
          : "";
    }
    if (imageURL.isEmpty) {
      imageURL = widget.movie["landscapeMoviePhoto"] != null &&
              !widget.movie["landscapeMoviePhoto"].isEmpty
          ? widget.movie["landscapeMoviePhoto"]
          : "";
    }
    if (widget.movie["moviePhotoHeight"].isNotEmpty &&
        widget.movie["moviePhotoWidth"].isNotEmpty) {
      isVertical = (double.parse(widget.movie["moviePhotoHeight"]) >
              double.parse(widget.movie["moviePhotoWidth"]))
          ? true
          : false;
    } else {
      isVertical = false;
    }
    isVertical = checkNetworkImageDimentions(imageURL);
    return new Container(
      color: isVertical ? Constants.appColorL2 : Constants.appColorL1,
      child: GestureDetector(
          child: getOneCachedImageWidget(imageURL, isVertical),
          onLongPress: () {
            printIfDebug('\n\nmovie details: ${widget.movie}');
          }),
    );
  }

  Widget getOneCachedImageWidget(
    String imageUrl,
    bool isVertical,
  ) {
    return isVertical
        ? Image.network(imageUrl)
        //   imageBuilder:
        //       (BuildContext context, ImageProvider<Object> imageProvider) =>
        //           Container(
        //     decoration: BoxDecoration(
        //       // borderRadius: BorderRadius.only(
        //       //   topLeft: Radius.circular(
        //       //     8.0,
        //       //   ),
        //       //   topRight: Radius.circular(
        //       //     8.0,
        //       //   ),
        //       // ),
        //       image: DecorationImage(
        //         image: imageProvider,
        //         fit: isVertical ? BoxFit.cover : BoxFit.fitWidth,
        //         alignment: Alignment.center,
        //       ),
        //     ),
        //   ),
        //   placeholder: (context, url) => Center(
        //     child: Text(
        //       widget.movie["movieName"],
        //       style: TextStyle(color: Constants.appColorFont),
        //     ),
        //   ),
        //   errorWidget: (context, url, error) => Center(
        //     child: Text(
        //       widget.movie["movieName"],
        //       style: TextStyle(color: Constants.appColorFont),
        //     ),
        //   ),
        // )
        : Stack(
            alignment: AlignmentDirectional.center,
            children: [
              Container(
                color: Constants.appColorFont,
                child: Image.network(
                  imageUrl,
                  height: 500,
                  // width: 250,
                  fit: BoxFit.fill,
                ),
              ),
              ClipRect(
                child: BackdropFilter(
                  filter: new ImageFilter.blur(
                    sigmaX: 5,
                    sigmaY: 5,
                  ),
                  child: Container(
                    color: Constants.appColorL2.withOpacity(
                      0.4,
                    ),
                  ),
                ),
              ),
              Image.network(imageUrl),
              // imageBuilder: (BuildContext context,
              //         ImageProvider<Object> imageProvider) =>
              //     Container(
              //   decoration: BoxDecoration(
              //     // borderRadius: BorderRadius.only(
              //     //   topLeft: Radius.circular(
              //     //     8.0,
              //     //   ),
              //     //   topRight: Radius.circular(
              //     //     8.0,
              //     //   ),
              //     // ),
              //     image: DecorationImage(
              //       image: imageProvider,
              //       fit: isVertical ? BoxFit.cover : BoxFit.fitWidth,
              //       alignment: Alignment.center,
              //     ),
              //   ),
              // ),
              // placeholder: (context, url) => Center(
              //   child: Text(
              //     widget.movie["movieName"],
              //     style: TextStyle(color: Constants.appColorFont),
              //   ),
              // ),
              // errorWidget: (context, url, error) => Center(
              //   child: Text(
              //     widget.movie["movieName"],
              //     style: TextStyle(color: Constants.appColorFont),
              //   ),
              // ),
              // ),
            ],
          );
  }

  Widget cardUI() {
    var cardBlur = userMenuStatus ? 50.0 : 0.0;
    var cardOpacity = userMenuStatus ? 0.5 : 0.0;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Center(
      child: new Container(
        padding: EdgeInsets.all(true ? 5.0 : 0),
        width: widget.pagefor != 0 ? 200.0 : 160,
        child: new Card(
          color: Constants.appColorL2,
          child: widget.movie["type_of_object"] != "1" && widget.pagefor == 2
              ? Container(
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new Expanded(
                        child: new GestureDetector(
                          //the actor and tag card
                          onTap: () {
                            actorFunc();
                          },
                          child: getActorTagCard(
                            screenHeight,
                            screenWidth,
                            cardBlur,
                            cardOpacity,
                          ),
                        ),
                        flex: 4,
                      ),
                      widget.movie["type_of_object"] == "2"
                          ? Expanded(
                              flex: 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 10,
                                      left: 5,
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            widget.movie["sub_headline_string"]
                                                .toString()
                                                .toUpperCase(),
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 9,
                                              color: const Color(
                                                0xFF777777,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 5,
                                      left: 5,
                                    ),
                                    child: widget.movie["type_of_object"] == "3"
                                        ? Container()
                                        : Text(
                                            widget.movie["movieName"],
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Constants.appColorFont,
                                            ),
                                          ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.all(5.0),
                  child: new GestureDetector(
                    onTap: () {
                      movieFunc();
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Expanded(
                          child: getSimpleCard(
                            screenHeight,
                            screenWidth,
                            cardBlur,
                            cardOpacity,
                          ),
                          flex: widget.showOriginalPosters ? 4 : 1,
                        ),
                        widget.showOriginalPosters
                            ? Expanded(
                                flex: 1,
                                child: nameCard(true),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    );
  }

  Widget nameCard(bool flag) {
    return flag
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  left: 5,
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text(
                        widget.movie["year"] != null
                            ? widget.movie["year"]
                            : "",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: const Color(
                            0xFF777777,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(left: 5),
                      child: Text(
                        widget.movie["language"] != null
                            ? " - " +
                                widget.movie["language"]
                                    .toString()
                                    .toUpperCase()
                            : " ",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: const Color(
                            0xFF777777,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.movie["content_type"] != null
                            ? " - " +
                                widget.movie["content_type"]
                                    .toString()
                                    .toUpperCase()
                            : "",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: const Color(
                            0xFF777777,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 5,
                  left: 5,
                ),
                child: Text(
                  widget.movie["movieName"],
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    color: Constants.appColorFont,
                  ),
                ),
              ),
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 5,
                ),
                child: Text(
                  widget.movie["movieName"],
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 17,
                    color: Constants.appColorFont,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  left: 5,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        widget.movie["year"] != null
                            ? widget.movie["year"]
                            : "",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(left: 5),
                      child: Text(
                        widget.movie["language"] != null
                            ? " - " +
                                widget.movie["language"]
                                    .toString()
                                    .toUpperCase()
                            : " ",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.movie["content_type"] != null
                            ? " - " +
                                widget.movie["content_type"]
                                    .toString()
                                    .toUpperCase()
                            : "",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 9,
                          color: Constants.appColorFont,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              widget.pagefor == 1
                  ? Container(
                      height: 100,
                    )
                  : listSeenBox(false),
            ],
          );
  }

  Widget followingCardUI() {
    var cardBlur = userMenuStatus ? 50.0 : 0.0;
    var cardOpacity = userMenuStatus ? 0.5 : 0.0;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Center(
      child: new Container(
        padding: EdgeInsets.all(true ? 5.0 : 0),
        width: widget.pagefor != 0 ? 200.0 : 160,
        child: new Card(
            color: Constants.appColorL2,
            child: Container(
              padding: EdgeInsets.all(5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Expanded(
                    child: new GestureDetector(
                      //the actor and tag card
                      onTap: () {
                        actorFunc();
                      },
                      child: getFollowingCard(
                        screenHeight,
                        screenWidth,
                        cardBlur,
                        cardOpacity,
                      ),
                    ),
                    flex: 4,
                  ),
                ],
              ),
            )),
      ),
    );
  }

  actorFunc() {
    _sendAnalyticsEvent(widget.movie["movieName"]);
    if (widget.pagefor == 4) {
      moveToFilterPage(widget.movie["query_string"]
          .toString()
          .replaceAll("movies?", "movies.json?"));
    } else if (widget.movie["type_of_object"] == "3") {
      moveToFilterPage(widget.movie["url_name"]);
    } else if (widget.movie["type_of_object"] == "5") {
      moveToChannelsPage();
    } else {
      callActorURL(widget.movie["url_name"]);
    }
  }

  moveToChannelsPage() {
    printIfDebug(widget.movie);
    printIfDebug(widget.movie["url_name"]);
    Navigator.of(context).push(
      new MaterialPageRoute(
        maintainState: false,
        builder: (BuildContext context) => LiveTvDetailPage(
          fromSearch: true,
          authToken: widget.authenticationToken,
          showOriginalPosters: widget.showOriginalPosters,
          urlName: widget.movie["url_name"],
        ),
      ),
    );
  }

  movieFunc() {
    _sendAnalyticsEvent(widget.movie["movieName"]);
    printIfDebug("You have selected");
    String searchKey = "";
    printIfDebug(widget.movie["url_name"]);
    if (widget.movie["url_name"].contains('?search_keyword=')) {
      int end = widget.movie["url_name"].indexOf('?search_keyword=');
      int urlLength = widget.movie["url_name"].length;
      searchKey = widget.movie["url_name"].substring(end + 1, urlLength);
      widget.movie["url_name"] = widget.movie["url_name"].substring(0, end);
    }
    String urlName = "";
    if (widget.authenticationToken.isEmpty) {
      urlName = "https://api.flixjini.com" + widget.movie["url_name"] + '?';
    } else {
      urlName = "https://api.flixjini.com" +
          widget.movie["url_name"] +
          "?" +
          "jwt_token=" +
          widget.authenticationToken +
          "&";
      printIfDebug(urlName);
    }
    var urlInformation = {
      "urlPhase": widget.movie["url_name"],
      "fullUrl": null,
      "search_key": null,
    };
    if (searchKey.isNotEmpty) {
      urlInformation["search_key"] = searchKey;
    }

    printIfDebug(
      '\n\nhey, url information before navigating to the movie details page: $urlInformation',
    );
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) => new MovieDetailPage(
          urlInformation: urlInformation,
          showOriginalPosters: widget.showOriginalPosters,
          movieDataFromCall: widget.movie,
        ),
      ),
    );
  }

  callActorURL(String urlActor) async {
    var urls = urlActor.split(".");
    var names = urls[0].split("/");
    String url = "https://www.flixjini.in/cast/" + names[3];
    printIfDebug(url);
    try {
      _launchURL(context, url);
    } catch (e) {
      printIfDebug("url error" + e.toString());
    }
  }

  void _launchURL(BuildContext context, link) async {
    try {
      await launch(
        link,
        customTabsOption: CustomTabsOption(
          toolbarColor: Constants.appColorL1,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          // animation: new CustomTabsAnimation.slideIn(),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      printIfDebug(e.toString());
    }
  }

  Widget getActorTagCard(
    double screenHeight,
    double screenWidth,
    double cardBlur,
    double cardOpacity,
  ) {
    String nameNew =
        // widget.movie["movieName"].toString().endsWith("Movies")
        //     ? widget.movie["movieName"]
        //         .toString()
        //         .substring(0, widget.movie["movieName"].toString().length - 7)
        //     :
        widget.movie["movieName"];
    if (checkIfMoviImagePresent()) {
      return Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned.fill(
              child: Container(
                color: Constants.appColorL1,
                child: movieImageAvailable(),
              ),
            ),
            ClipRRect(
              borderRadius: new BorderRadius.only(
                topLeft: Radius.circular(0.0),
                topRight: Radius.circular(0.0),
              ),
              child: new BackdropFilter(
                filter: new ImageFilter.blur(
                  sigmaX: widget.movie["type_of_object"] == "3" ? 5 : cardBlur,
                  sigmaY: widget.movie["type_of_object"] == "3" ? 5 : cardBlur,
                ),
                child: new Container(
                  decoration: new BoxDecoration(
                    // borderRadius: new BorderRadius.only(
                    //   topLeft: Radius.circular(8.0),
                    //   topRight: Radius.circular(8.0),
                    // ),
                    color: Constants.appColorL1.withOpacity(
                      widget.movie["type_of_object"] == "3" ? 0.8 : cardOpacity,
                    ),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 15,
                      padding: EdgeInsets.only(
                        top: 3,
                        bottom: 3,
                        left: widget.movie["type_of_object"] == "3" ? 10 : 3,
                        right: widget.movie["type_of_object"] == "3" ? 10 : 3,
                      ),
                      margin: EdgeInsets.only(
                        left: 10,
                        bottom: 10,
                      ),
                      decoration: new BoxDecoration(
                        borderRadius: new BorderRadius.only(
                          topLeft: widget.movie["type_of_object"] == "3"
                              ? Radius.circular(10.0)
                              : Radius.circular(0.0),
                          topRight: widget.movie["type_of_object"] == "3"
                              ? Radius.circular(10.0)
                              : Radius.circular(0.0),
                          bottomLeft: widget.movie["type_of_object"] == "3"
                              ? Radius.circular(10.0)
                              : Radius.circular(0.0),
                          bottomRight: widget.movie["type_of_object"] == "3"
                              ? Radius.circular(10.0)
                              : Radius.circular(0.0),
                        ),
                        color: Constants.appColorLogo.withOpacity(
                          widget.movie["type_of_object"] == "3" ? 1 : 1,
                        ),
                      ),
                      child: Text(
                        // widget.movie["type_of_object"] == "2"
                        //     ? "ACTOR"
                        //     : "LIST",
                        widget.movie["sub_headline_string"]
                            .toString()
                            .toUpperCase(),
                        style: TextStyle(
                          color: Constants.appColorL1,
                          fontWeight: FontWeight.w900,
                          fontSize: 10,
                          letterSpacing: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            widget.movie["type_of_object"] == "3"
                ? Container(
                    margin: EdgeInsets.only(
                        // bottom: 70,
                        ),
                    padding: EdgeInsets.all(
                      10,
                    ),
                    child: Text(
                      nameNew,
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 17,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                : Container(),
          ],
        ),
      );
    } else {
      return Stack(
        children: <Widget>[
          getIndexPageBlurredImageContainer(
            widget.movie["movieName"],
            widget.index,
          ),
          !userMenuStatus
              ? getBlurredTopWidget(
                  screenHeight,
                  screenWidth,
                )
              : Container(),
          !userMenuStatus
              ? Positioned(
                  left: 15,
                  right: 15,
                  top: 40,
                  bottom: 25,
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 2,
                        right: 2,
                      ),
                      width: screenWidth,
                      height: screenHeight,
                      child: Center(
                        child: getAutoSizeTextWidget(),
                      ),
                    ),
                  ),
                )
              : Container(),
          !userMenuStatus ? addCardType() : Container(),
          !userMenuStatus ? getCardLabel() : Container(),
          // displayUserMenu(),
          // !userMenuStatus ? getAwardLogo() : Container(),
        ],
      );
    }
  }

  Widget getFollowingCard(
    double screenHeight,
    double screenWidth,
    double cardBlur,
    double cardOpacity,
  ) {
    String nameNew =
        // widget.movie["movieName"].toString().endsWith("Movies")
        //     ? widget.movie["movieName"]
        //         .toString()
        //         .substring(0, widget.movie["movieName"].toString().length - 7)
        //     :
        widget.movie["name"];

    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: Constants.appColorL1,
              child: getIndexPageBlurredImageContainer(
                widget.movie["name"],
                widget.index,
              ),
            ),
          ),
          ClipRRect(
            borderRadius: new BorderRadius.only(
              topLeft: Radius.circular(0.0),
              topRight: Radius.circular(0.0),
            ),
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: widget.movie["type_of_object"] == "3" ? 5 : cardBlur,
                sigmaY: widget.movie["type_of_object"] == "3" ? 5 : cardBlur,
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  // borderRadius: new BorderRadius.only(
                  //   topLeft: Radius.circular(8.0),
                  //   topRight: Radius.circular(8.0),
                  // ),
                  color: Constants.appColorL1.withOpacity(
                    widget.movie["type_of_object"] == "3" ? 0.8 : cardOpacity,
                  ),
                ),
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 15,
                    padding: EdgeInsets.only(
                      top: 3,
                      bottom: 3,
                      left: widget.movie["type_of_object"] == "3" ? 10 : 3,
                      right: widget.movie["type_of_object"] == "3" ? 10 : 3,
                    ),
                    margin: EdgeInsets.only(
                      left: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.only(
                        topLeft: widget.movie["type_of_object"] == "3"
                            ? Radius.circular(10.0)
                            : Radius.circular(0.0),
                        topRight: widget.movie["type_of_object"] == "3"
                            ? Radius.circular(10.0)
                            : Radius.circular(0.0),
                        bottomLeft: widget.movie["type_of_object"] == "3"
                            ? Radius.circular(10.0)
                            : Radius.circular(0.0),
                        bottomRight: widget.movie["type_of_object"] == "3"
                            ? Radius.circular(10.0)
                            : Radius.circular(0.0),
                      ),
                      color: Constants.appColorLogo.withOpacity(
                        widget.movie["type_of_object"] == "3" ? 1 : 1,
                      ),
                    ),
                    child: Text(
                      widget.movie["type_of_object"] == "2" ? "ACTOR" : "LIST",
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontWeight: FontWeight.w900,
                        fontSize: 10,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
                // bottom: 70,
                ),
            padding: EdgeInsets.all(
              10,
            ),
            child: Text(
              nameNew,
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 17,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  moveToFilterPage(String url) {
    String filterUrl = "https://api.flixjini.com" + url;
    // filterUrl = filterUrl.replaceAll("tags", "categories");
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => MovieFilterPage(
          appliedFilterUrl: filterUrl,
          showOriginalPosters: widget.showOriginalPosters,
          inTheaterKey: false,
        ),
      ),
    );
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    if (widget.pagefor == 2) {
      await analytics.logEvent(
        name: 'search_result_click',
        parameters: <String, dynamic>{
          'search_result_movie': tagName,
        },
      );
    } else {
      await analytics.logEvent(
        name: 'movie_click',
        parameters: <String, dynamic>{
          'movie': tagName,
        },
      );
    }
  }

  Widget getSimpleCard(
    double screenHeight,
    double screenWidth,
    double cardBlur,
    double cardOpacity,
  ) {
    // printIfDebug('\n\nfrom card structure, value of show original posters: ${widget.showOriginalPosters}');
    // if (showOriginalPosters == 'true' && checkIfMoviImagePresent()) {
    if (checkIfMoviImagePresent()) {
      return Container(
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              // child: new ClipRRect(
              //   borderRadius: new BorderRadius.only(
              //     topLeft: const Radius.circular(
              //       8.0,
              //     ),
              //     topRight: const Radius.circular(
              //       8.0,
              //     ),
              //   ),
              child: Container(
                color: Constants.appColorL1,
                child: movieImageAvailable(),
                // checkIfMoviImagePresent()
                //     ? movieImageAvailable()
                //     : movieImageUnavailable(),
              ),
              // ),
            ),
            new ClipRRect(
              borderRadius: new BorderRadius.only(
                topLeft: widget.showOriginalPosters
                    ? Radius.circular(3.0)
                    : Radius.circular(0.0),
                topRight: widget.showOriginalPosters
                    ? Radius.circular(3.0)
                    : Radius.circular(0.0),
              ),
              child: new BackdropFilter(
                filter: new ImageFilter.blur(
                  sigmaX: widget.showOriginalPosters ? 0 : 20,
                  sigmaY: widget.showOriginalPosters ? 0 : 20,
                ),
                child: new Container(
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(8.0),
                      topRight: const Radius.circular(8.0),
                    ),
                    color: Constants.appColorL1.withOpacity(
                      cardOpacity,
                    ),
                  ),
                ),
              ),
            ),
            // displayUserMenu(),
            // !userMenuStatus ? getAwardLogo() : Container(),
            widget.pagefor == 1 || !widget.showOriginalPosters
                ? Container()
                : listSeenBox(true),

            widget.showOriginalPosters ? Container() : nameCard(false),
          ],
        ),
      );
    } else {
      return Stack(
        children: <Widget>[
          getIndexPageBlurredImageContainer(
            widget.movie["movieName"],
            widget.index,
          ),
          !userMenuStatus
              ? getBlurredTopWidget(
                  screenHeight,
                  screenWidth,
                )
              : Container(),
          !userMenuStatus
              ? Positioned(
                  left: 15,
                  right: 15,
                  top: 40,
                  bottom: 25,
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 2,
                        right: 2,
                      ),
                      width: screenWidth,
                      height: screenHeight,
                      child: Center(
                        child: getAutoSizeTextWidget(),
                      ),
                    ),
                  ),
                )
              : Container(),
          !userMenuStatus ? addCardType() : Container(),
          !userMenuStatus ? getCardLabel() : Container(),
          // displayUserMenu(),
          // !userMenuStatus ? getAwardLogo() : Container(),
        ],
      );
    }
  }

  String getRating() {
    String rating = "";
    if (widget.movie["scores"] != null && widget.movie["scores"].length > 0) {
      for (int i = 0; i < widget.movie["scores"].length; i++) {
        if (widget.movie["scores"][i].containsValue("IMDB")) {
          rating = widget.movie["scores"][i]["score"];
        }
      }
      return rating;
    } else if (widget.movie["display_score"] != null) {
      int? scoredouble = int.tryParse(widget.movie["display_score"] ?? "0");
      String actualScore = scoredouble != null && scoredouble > 0
          ? (scoredouble / 10).toString()
          : "";
      return actualScore;
    } else {
      return "";
    }
  }

  Widget listSeenBox(bool flag) {
    String rating = getRating();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: flag ? 5 : 70,
            right: flag ? 5 : 0,
            bottom: flag ? 0 : 20,
          ),
          child: Row(
            mainAxisAlignment:
                flag ? MainAxisAlignment.end : MainAxisAlignment.center,
            children: <Widget>[
              Opacity(
                opacity: 0.8,
                child: GestureDetector(
                  child: Container(
                    margin: EdgeInsets.only(
                      right: 5,
                    ),
                    padding: EdgeInsets.all(5),
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      // color: Constants.appColorLogo,
                      shape: BoxShape.circle,
                      color: widget.movie["seen"] || isSeen
                          ? Constants.appColorLogo
                          : Constants.appColorDivider,
                    ),
                    child: Image.asset(
                      "assests/seen_dark.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  onTap: () {
                    if (!widget.authenticationToken.isEmpty) {
                      if (!isSeen) {
                        // addSeenMovieId(widget.movie["id"]);
                        sendEvent();
                        sendSeenRequest(widget.movie,
                            "https://api.flixjini.com/queue/seen.json", "seen");
                        setState(() {
                          isSeen = true;
                        });
                      } else {
                        // removeSeenMovieId(widget.movie["id"]);
                        sendSeenRequest(
                            widget.movie,
                            "https://api.flixjini.com/queue/reset_seen.json",
                            "seen");
                        setState(() {
                          isSeen = false;
                        });
                      }
                    } else {
                      showAlert();
                    }
                  },
                ),
              ),
              Opacity(
                opacity: 0.8,
                child: GestureDetector(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      // color: Constants.appColorLogo,
                      shape: BoxShape.circle,
                      color: widget.movie["in_queue"] || isQueued
                          ? Constants.appColorLogo
                          : Constants.appColorDivider,
                    ),
                    height: 25,
                    width: 25,
                    child: widget.movie["in_queue"] || isQueued
                        ? Image.asset(
                            "assests/mylisttick.png",
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            "assests/mylist_dark.png",
                            fit: BoxFit.cover,
                          ),
                  ),
                  onTap: () {
                    if (!widget.authenticationToken.isEmpty) {
                      if (!isQueued) {
                        sendQueueEvent("add");
                        widget.sendRequest(
                            widget.movie,
                            'https://api.flixjini.com/queue/v2/add.json',
                            'add');
                        setState(() {
                          isQueued = true;
                        });
                      } else {
                        sendQueueEvent("remove");

                        widget.sendRequest(
                            widget.movie,
                            'https://api.flixjini.com/queue/v2/remove.json',
                            'add');
                        setState(() {
                          isQueued = false;
                        });
                      }
                    } else {
                      showAlert();
                      // showToast('Login to Add to Queue', 'long',
                      //     'bottom', 1, Constants.appColorLogo);
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        flag && rating.isNotEmpty
            ? Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(5),
                    margin: EdgeInsets.only(
                      bottom: 5,
                      left: 5,
                    ),
                    color: Constants.appColorL2.withOpacity(0.40),
                    child: Row(
                      children: [
                        Image.asset(
                          "assests/flixjini_score.png",
                          height: 10,
                          width: 10,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            rating,
                            style: TextStyle(
                              color: Constants.appColorFont,
                              fontSize: 10,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  sendQueueEvent(String func) {
    Map<String, String> eventData = {
      'content': widget.movie["movieName"],
      'func': func,
    };
    //platform.invokeMethod('logAddToWishlistEvent', eventData);
  }

  Widget getAwardLogo() {
    var scoresList = widget.movie['scores'];
    bool showAwardLogo = false;
    for (int i = 0; i < scoresList.length; i++) {
      if (scoresList[i]['name'].toString().toLowerCase().contains('award')) {
        showAwardLogo = true;
      }
    }
    if (showAwardLogo) {
      return Positioned(
        top: 5,
        left: 5,
        child: Container(
          height: 20,
          width: 20,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(
                50.0,
              ),
            ),
          ),
          child: Image.asset(
            'assests/new_emojis/award_logo.png',
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  sendEvent() {
    //platform.invokeMethod('logCustomizeProductEvent');
  }

  showAlert() {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return SigninAlertAlert();
      },
    );
  }

  Widget getAutoSizeTextWidget() {
    return AutoSizeText(
      cleanMovieName(
        widget.movie['movieName'].toString(),
      ),
      minFontSize: 10,
      wrapWords: false,
      textAlign: TextAlign.center,
      style: TextStyle(
        letterSpacing: 0.7,
        fontWeight: FontWeight.bold,
        color: Constants.appColorFont,
      ),
      maxLines: 6,
    );
  }

  String cleanMovieName(String movieName) {
    movieName = movieName.replaceAll('-', '');
    movieName = movieName.replaceAll('/', '');
    movieName = movieName.replaceAll(':', '');

    movieName = movieName.replaceAll('?', '');
    movieName = movieName.replaceAll('_', '');
    movieName = movieName.replaceAll(';', '');

    movieName = movieName.toLowerCase().replaceAll('&amp', '&');
    movieName = movieName.toLowerCase().replaceAll('!', '');
    movieName = movieName.toLowerCase().replaceAll('--', '');

    movieName = movieName.toLowerCase().replaceAll('.', '');
    movieName = movieName.toLowerCase().replaceAll(',', '');

    movieName = unescapeChars(
      movieName,
    );

    return movieName.toUpperCase();
  }

  String unescapeChars(
    String encodedString,
  ) {
    return new HtmlUnescape().convert(
      encodedString,
    );
  }

  Positioned getCardLabel() {
    return Positioned(
      bottom: 16.5,
      left: 35,
      right: 35,
      child: Container(
        height: 18,
        // width: 30,
        decoration: BoxDecoration(
          color: Constants.appColorLogo,
          borderRadius: BorderRadius.all(
            Radius.circular(
              10,
            ),
          ),
        ),
        child: Center(
          child: Text(
            getMovieLanguage(),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 10,
              letterSpacing: 0.7,
              color: const Color(
                0xFF395E66,
              ),
            ),
          ),
        ),
      ),
    );
  }

  String getMovieLanguage() {
    String movieLanguage = "";
    movieLanguage =
        widget.movie['content_type'].toString().toLowerCase().contains('movie')
            ? 'MOVIE'
            : 'TV SHOW';
    String languages = widget.movie['language'].toString();

    if (widget.movie['language'] != '' && widget.movie['language'] != null) {
      String finalString = '';
      for (int i = 0; i < languages.length && languages[i] != ','; i++) {
        finalString += languages[i];
      }
      movieLanguage = finalString;
    }

    return movieLanguage.toString().toUpperCase();
  }

  Positioned getBlurredTopWidget(
    double screenHeight,
    double screenWidth,
  ) {
    double overlayOpacity = 0.2;
    return Positioned(
      child: new Center(
        child: new Container(
          margin: EdgeInsets.only(
            left: 15,
            right: 15,
            top: 40,
            bottom: 25,
          ),
          width: screenWidth, // * 0.25,
          height: screenHeight, // * 0.20,
          child: new ClipRect(
            child: new BackdropFilter(
              filter: new ImageFilter.blur(
                sigmaX: 4.0,
                sigmaY: 4.0,
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  border: new Border.all(
                    color: Constants.appColorFont,
                    width: 1,
                  ),
                  color: Constants.appColorL1.withOpacity(
                    overlayOpacity,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    try {
      // printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
      return GestureDetector(
        onTap: () {
          if (widget.pagefor == 4) {
            actorFunc();
          } else if (widget.movie["type_of_object"] != "1" &&
              widget.pagefor == 2) {
            actorFunc();
          } else {
            movieFunc();
          }
        },
        child: widget.pagefor == 4 ? followingCardUI() : cardUI(),
      );
    } catch (e) {
      printIfDebug('\n\nerror caught is: $e');
      return Container(
        child: Text(
          'Oops! Something went wrong. Please try again later.',
          textAlign: TextAlign.center,
        ),
      );
    }
  }
}
