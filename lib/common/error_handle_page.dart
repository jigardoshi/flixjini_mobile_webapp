// import 'package:flixjini_webapp/authentication/movie_authentication_page_new.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ErrorHandlePage extends StatefulWidget {
  ErrorHandlePage({
    Key? key,
    this.message,
    this.imagePath,
    this.refresh,
  }) : super(key: key);

  final message;
  final imagePath;
  final refresh;

  @override
  _ErrorHandlePageState createState() => new _ErrorHandlePageState();
}

class _ErrorHandlePageState extends State<ErrorHandlePage> {
  double screenWidth = 0.0;
  double screenHeight = 0.0;
  bool loadingFlag = false;

  @override
  void initState() {
    super.initState();
  }

  Widget quickStart() {
    return new Container(
      padding: const EdgeInsets.all(20.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new RaisedButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPage(type: true)));
            },
            child: new Text(
              "Sign Up",
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Constants.appColorFont),
            ),
          ),
          new RaisedButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new MovieAuthenticationPage(type: false)));
            },
            child: new Text(
              "Sign In",
              style: new TextStyle(
                  fontWeight: FontWeight.bold, color: Constants.appColorFont),
            ),
          ),
        ],
      ),
    );
  }

  Widget errorImage() {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        child: new FlareActor("assests/sadsmile.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: "sadsmile"),
      ),
      flex: 2,
    );
  }

  Widget defaultMessage(message) {
    return new Expanded(
      child: new Container(
        width: screenWidth * 0.8,
        child: new Center(
          child: new Text(message,
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.bold,
                color: Constants.appColorFont,
              ),
              // overflow: TextOverflow.ellipsis,
              textScaleFactor: 1.0),
        ),
      ),
      flex: 2,
    );
  }

  Widget authenticateYourself() {
    return new Container(
      child: new Center(
        child: new Container(
          height:
              widget.refresh != null ? screenHeight * 0.4 : screenHeight * 0.3,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              errorImage(),
              defaultMessage(widget.message),
              widget.refresh != null ? refreshButton() : Container()
            ],
          ),
        ),
      ),
    );
  }

  refreshButton() {
    return !loadingFlag
        ? GestureDetector(
            child: Container(
              padding: EdgeInsets.only(
                left: 30,
                right: 30,
                top: 10,
                bottom: 10,
              ),
              decoration: new BoxDecoration(
                border: new Border.all(
                  color: Constants.appColorLogo,
                ),
                borderRadius: new BorderRadius.circular(30.0),
              ),
              child: Text(
                'Refresh',
                style: TextStyle(
                  fontSize: 13,
                  color: Constants.appColorLogo,
                ),
              ),
            ),
            onTap: () {
              Future.delayed(const Duration(milliseconds: 5000), () {
                setState(() {
                  loadingFlag = false;
                });
              });
              setState(() {
                widget.refresh();

                loadingFlag = true;
              });
            },
          )
        : getProgressBar();
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;

    return authenticateYourself();
  }
}
