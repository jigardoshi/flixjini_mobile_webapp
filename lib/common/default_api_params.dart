// import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/google_advertising_id.dart';
// import 'package:package_info/package_info.dart';
// import 'dart:io' show Platform;
import 'package:uuid/uuid.dart';
import 'package:shared_preferences/shared_preferences.dart';

String uuidToken = "";

uuidStatus(SharedPreferences prefs) async {
  uuidToken = (prefs.getString('uuidToken') ?? "");
  prefs.setString('uuidToken', uuidToken);
  printIfDebug("UUID");
  printIfDebug(uuidToken);
}

setUuid(token) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  uuidToken = (prefs.getString('uuidToken') ?? "");
  prefs.setString('uuidToken', token);
}

checkIfNewUuidNeeded() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await uuidStatus(prefs);
  if (uuidToken.isEmpty) {
    printIfDebug("constructed new uuid");
    var uuidData = new Uuid();
    String result = uuidData.v1();
    await setUuid(result);
    return result;
  } else {
    printIfDebug("uuid already exists");
    return uuidToken;
  }
}

tryToObtainAdvertisingId() async {
  try {
    // const platform = const MethodChannel('api.komparify/advertisingid');
    var result = await getGoogleAdId();
    if (result != null) {
      printIfDebug(
          "\n\nno need for uuid, as the following gaid is obtained: $result");
      return result;
    } else {
      String result = await checkIfNewUuidNeeded();
      printIfDebug("\n\nuuid obtained is: $result");
      return result;
    }
  } on PlatformException catch (e) {
    printIfDebug("Failed to get Android Advertising Id: '${e.message}'.");
  }
  String result = await checkIfNewUuidNeeded();
  printIfDebug("uuid obtained");
  printIfDebug(result);
  return result;
}

fetchDefaultParams(data, [String type = "json"]) async {
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  var deviceInformation = {
    'app_name': "Flixjini",
    // "app_name": "flixjini_web",
  };
  if (true) {
    deviceInformation["app_version"] = getVersionCodeForAndroid();
    deviceInformation["os"] = "web";
    deviceInformation["ga_id"] = await tryToObtainAdvertisingId();
  } else if (true) {
    // IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    // String result = await getIDFA(); // await checkIfNewUuidNeeded();
    // printIfDebug("\n\nobtained uuid: $result");
    // deviceInformation["ga_id"] = result;
    // deviceInformation["os"] = "iOS";
    // deviceInformation["app_version"] = iosInfo.utsname.release;
  } else {
    printIfDebug("\n\nUnknown OS");
  }
  if (data is String) {
    bool flag = false;
    if (type == "json") {
      int lengthOfUrl = data.length;
      if (data[lengthOfUrl - 1] != "?") {
        data = (data.substring(lengthOfUrl - 4, lengthOfUrl) == "json")
            ? (data + '?')
            : (data + '&');
      }
      flag = true;
    } else if (type == "nonJson") {
      data += '?';
      flag = true;
    } else {
      printIfDebug("\n\nUnknown URL type specified");
    }
    if (flag) {
      data += 'app=' + deviceInformation["app_name"]!;
      data += '&' + 'os=' + deviceInformation["os"]!;
      if (deviceInformation["app_version"]!.isNotEmpty) {
        data += '&' + 'app_version=' + deviceInformation["app_version"]!;
        if (type == "json") {
          data += '&' + 'ga_id=' + deviceInformation["ga_id"]!;
        }
      }
    }
  } else {
    data["app"] = deviceInformation["app_name"];
    data["os"] = deviceInformation["os"];
    data["app_version"] = deviceInformation["app_version"];
    if (type == "json") {
      data += '&' + 'ga_id=' + deviceInformation["ga_id"]!;
    }
  }

  data += '&package_name=com.flixjini.watchonline';
  data +=
      '&partner_id=1&app_platform=playstore&partner_name=flixjini&magic=true';
  printIfDebug("\n\nUrl After Adding Default Params: $data");
  return data;
}
