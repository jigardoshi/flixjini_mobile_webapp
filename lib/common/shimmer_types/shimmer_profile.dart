import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerProfile extends StatelessWidget {
  ShimmerProfile({
    Key? key,
  }) : super(key: key);

  Widget spacingBoxes() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget languageOptions() {
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new ClipRRect(
        borderRadius: new BorderRadius.circular(50.0),
        child: Container(
          padding: const EdgeInsets.all(10.0),
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget streamingSourceOptions() {
    return Shimmer.fromColors(
     baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: Container(
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget loadLanguagesShimmer() {
    return Container(
      child: new GridView.builder(
          physics: new BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            crossAxisCount: 4,
          ),
          padding: const EdgeInsets.all(20.0),
          itemCount: 10,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return languageOptions();
          }),
    );
  }

  Widget loadStreamingSourceShimmer() {
    return Container(
      child: new GridView.builder(
          physics: new BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 1.0,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            crossAxisCount: 5,
          ),
          padding: const EdgeInsets.all(20.0),
          itemCount: 19,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return streamingSourceOptions();
          }),
    );
  }

  Widget headingShimmer() {
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: Center(
        child: Container(
          height: 30.0,
          width: 200.0,
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget loadShimmer() {
    return new SingleChildScrollView(
      child: new Container(
        color: Constants.appColorL1,
        padding: const EdgeInsets.only(top: 20.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            headingShimmer(),
            loadStreamingSourceShimmer(),
            headingShimmer(),
            loadLanguagesShimmer(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
