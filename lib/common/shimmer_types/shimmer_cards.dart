import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerCards extends StatelessWidget {
  ShimmerCards({
    Key? key,
  }) : super(key: key);

  Widget spacingBoxes() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget fullCard() {
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: Container(
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget halfCard() {
    return Expanded(
      child: new ClipRRect(
        borderRadius: new BorderRadius.only(
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(8.0),
        ),
        child: Container(
          color: Constants.appColorL1,
        ),
      ),
      flex: 6,
    );
  }

  Widget fullCardRow() {
    return Expanded(
      child: Row(
        children: <Widget>[fullCard(), spacingBoxes(), fullCard()],
      ),
      flex: 10,
    );
  }

  Widget halfCardRow() {
    return Expanded(
      child: Row(
        children: <Widget>[halfCard(), spacingBoxes(), halfCard()],
      ),
      flex: 8,
    );
  }

  Widget loadShimmer() {
    return Container(
      color: Constants.appColorL1,
      child: new GridView.builder(
          physics: new BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 0.62,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            crossAxisCount: 2,
          ),
          padding: const EdgeInsets.all(20.0),
          itemCount: 8,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return fullCard();
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
