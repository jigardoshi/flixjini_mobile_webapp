import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerTvshows extends StatelessWidget {
  ShimmerTvshows({
    Key? key,
  }) : super(key: key);

  Widget spacingBoxes() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget headingShimmer(double width) {
    return Container(
      padding: const EdgeInsets.only(
          left: 15.0, top: 10.0, bottom: 10.0, right: 10.0),
      child: Shimmer.fromColors(
       baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: Container(
          height: 30.0,
          width: width,
          color: Constants.appColorFont,
        ),
      ),
    );
  }

  Widget tvshowChosenSeason() {
    return Container(
      padding: const EdgeInsets.all(5.0),
      child: Center(
        child: Shimmer.fromColors(
            baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
          child: new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: Container(
              height: 120.0,
              width: 220.0,
              padding: const EdgeInsets.all(10.0),
              color: Constants.appColorFont,
            ),
          ),
        ),
      ),
    );
  }

  Widget episodeCards() {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: Shimmer.fromColors(
           baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Container(
            height: 50.0,
            color: Constants.appColorFont,
          ),
        ),
      ),
    );
  }

  Widget seasonCards() {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: Shimmer.fromColors(
          baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Container(
            width: 220.0,
            height: 150.0,
            color: Constants.appColorFont,
          ),
        ),
      ),
    );
  }

  Widget loadEpisodesShimmer() {
    return Container(
      child: new ListView.builder(
          itemCount: 10,
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(10.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return episodeCards();
          }),
    );
  }

  Widget tvshowSeasons() {
    return Container(
      height: 150.0,
      child: new ListView.builder(
          itemCount: 8,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          itemBuilder: (BuildContext context, int index) {
            return seasonCards();
          }),
    );
  }

  Widget loadShimmer() {
    return new SingleChildScrollView(
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            tvshowSeasons(),
            tvshowChosenSeason(),
            headingShimmer(300.0),
            headingShimmer(150.0),
            loadEpisodesShimmer(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
