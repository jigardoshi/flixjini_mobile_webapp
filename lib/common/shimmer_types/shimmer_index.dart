import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
// import 'package:flixjini_webapp/common/arc_clipper.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerIndex extends StatelessWidget {
  ShimmerIndex({
    Key? key,
  }) : super(key: key);

  Widget appBarShimmer() {
    return new Expanded(
      flex: 6,
      child: new Container(
        color: Constants.appColorL2,
      ),
    );
  }

  Widget overviewCardShimmer() {
    return new Expanded(
      flex: 3,
      child: new Container(
        height: 100.0,
        color: Constants.appColorL2,
      ),
    );
  }

  Widget spacingBoxes() {
    return new Expanded(
      flex: 1,
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget spacingRows() {
    return new Expanded(
      flex: 1,
      child: new Container(
        padding: const EdgeInsets.all(5.0),
        height: 20.0,
      ),
    );
  }

  Widget overviewShimmer() {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            spacingBoxes(),
            overviewCardShimmer(),
            spacingBoxes(),
            overviewCardShimmer(),
            spacingBoxes(),
            overviewCardShimmer(),
            spacingBoxes(),
          ],
        ),
      ),
      flex: 2,
    );
  }

  Widget loadShimmer() {
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          appBarShimmer(),
          spacingRows(),
          overviewShimmer(),
          overviewShimmer(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
