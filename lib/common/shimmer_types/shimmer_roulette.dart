// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class RouletteShimmerCards extends StatelessWidget {
  RouletteShimmerCards({
    Key? key,
  }) : super(key: key);

  Widget spacingBoxes() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget fullCard() {
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new ClipRRect(
        borderRadius: new BorderRadius.circular(8.0),
        child: Container(
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget halfCard() {
    return Expanded(
      child: new ClipRRect(
        borderRadius: new BorderRadius.only(
          topLeft: Radius.circular(8.0),
          topRight: Radius.circular(8.0),
        ),
        child: Container(
          color: Constants.appColorL1,
        ),
      ),
      flex: 6,
    );
  }

  Widget fullCardRow() {
    return Expanded(
      child: Row(
        children: <Widget>[fullCard(), spacingBoxes(), fullCard()],
      ),
      flex: 10,
    );
  }

  Widget halfCardRow() {
    return Expanded(
      child: Row(
        children: <Widget>[halfCard(), spacingBoxes(), halfCard()],
      ),
      flex: 8,
    );
  }

  _buildStoryPage(bool active, int i) {
    // Animated Properties
    // final double blur = active ? 30 : 0;
    // final double offset = active ? 20 : 0;
    // final double top = active ? 10 : 75;
    // final double left = active ? 0 : 20;
    // final double right = active ? 0 : 20;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 50,
            bottom: 20,
          ),
          child: Text(
            "Discover What to Stream Tonite",
            style: TextStyle(
              color: Constants.appColorFont,
              // letterSpacing: 1,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          height: 300,
          color: Constants.appColorL1,
          // height: 300,
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          child: FlareActor(
            "assests/star.flr",
            alignment: Alignment.center,
            fit: BoxFit.cover,
            animation: "loader",
          ),
          // getProgressBar(),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 80,
            // bottom: 20,
          ),
          child: Text(
            "TIP: Shake your phone when using Flixjini App to Start Genie.",
            style: TextStyle(
              color: Constants.appColorFont,
              // letterSpacing: 1,
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget loadShimmer(screenHeight, screenWidth) {
    return Container(
      color: Constants.appColorL1,
      height: screenHeight,
      width: screenWidth,
      child: StreamBuilder(
          // stream: slides,
          // initialData: [],

          builder: (context, AsyncSnapshot snap) {
        // snap.data.toList();

        return PageView.builder(
            // controller: ctrl,
            itemCount: 3,
            itemBuilder: (context, int currentIdx) {
              bool active = currentIdx == 1;
              return _buildStoryPage(active, currentIdx);
            });
      }),
    );
    // Container(
    //   color: Constants.appColorL1,
    //   child: new GridView.builder(
    //       physics: new BouncingScrollPhysics(),
    //       shrinkWrap: true,
    //       addAutomaticKeepAlives: false,
    //       gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
    //         childAspectRatio: 0.62,
    //         mainAxisSpacing: 10.0,
    //         crossAxisSpacing: 10.0,
    //         crossAxisCount: 2,
    //       ),
    //       padding: const EdgeInsets.all(20.0),
    //       itemCount: 8,
    //       scrollDirection: Axis.vertical,
    //       itemBuilder: (BuildContext context, int index) {
    //         return fullCard();
    //       }),
    // );
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer(screenHeight, screenWidth);
  }
}
