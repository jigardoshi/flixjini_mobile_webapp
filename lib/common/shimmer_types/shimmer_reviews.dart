import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerReviews extends StatelessWidget {
  ShimmerReviews({
    Key? key,
  }) : super(key: key);

  Widget spacingBoxes() {
    return Expanded(
      flex: 1,
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget renderReviewCards() {
    return new Container(
      child: new Stack(
        children: <Widget>[
          mainReviewCard(),
          sideReviewRatingCard(),
        ],
      ),
    );
  }

  Widget mainReviewCard() {
    return new Container(
      padding: const EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
      child: Shimmer.fromColors(
        baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Container(
            height: 150.0,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget renderStatsCards() {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: Shimmer.fromColors(
        baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Container(
            width: 95.0,
            height: 120.0,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget renderHighlightCards() {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: Shimmer.fromColors(
        baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: new ClipRRect(
          borderRadius: new BorderRadius.circular(8.0),
          child: Container(
            width: 120.0,
            height: 70.0,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget sideReviewRatingCard() {
    return new Positioned(
      top: 15.0,
      child: new Container(
        width: 50.0,
        height: 85.0,
        child: Shimmer.fromColors(
          baseColor: Constants.appColorL2,
          highlightColor: Constants.appColorL2,
          child: new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: Container(
              width: 50.0,
              height: 85.0,
              color: Constants.appColorL2,
            ),
          ),
        ),
      ),
    );
  }

  Widget loadReviewsShimmer() {
    return Container(
      color: Constants.appColorL1,
      child: new ListView.builder(
          itemCount: 10,
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(10.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return renderReviewCards();
          }),
    );
  }

  Widget loadReviewStatsShimmer() {
    return Container(
      color: Constants.appColorL1,
      height: 120.0,
      child: new ListView.builder(
          itemCount: 8,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          itemBuilder: (BuildContext context, int index) {
            return renderStatsCards();
          }),
    );
  }

  Widget reviewHighlightView() {
    return Container(
      color: Constants.appColorL1,
      height: 70.0,
      child: new ListView.builder(
          itemCount: 8,
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(10.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          itemBuilder: (BuildContext context, int index) {
            return renderHighlightCards();
          }),
    );
  }

  Widget loadShimmer() {
    return new SingleChildScrollView(
      child: new Container(
        color: Constants.appColorL1,
        padding: const EdgeInsets.all(5.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            reviewHighlightView(),
            loadReviewStatsShimmer(),
            loadReviewsShimmer(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
