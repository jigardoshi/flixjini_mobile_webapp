import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
// import 'package:flixjini_webapp/common/arc_clipper.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_cards.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerFilter extends StatefulWidget {
  ShimmerFilter({
    Key? key,
  }) : super(key: key);

  @override
  _ShimmerFilterState createState() => new _ShimmerFilterState();
}

class _ShimmerFilterState extends State<ShimmerFilter> {
  double screenWidth = 0.0;
  double screenHidth = 0.0;

  Widget appBarShimmer() {
    screenHidth = MediaQuery.of(context).size.height;
    return Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
      child: new Container(
        height: screenHidth * 0.6,
        color: Constants.appColorL1,
      ),
    );
  }

  Widget overviewCardShimmer() {
    return new Expanded(
      flex: 3,
      child: new Container(
        height: 100.0,
        color: Constants.appColorFont,
      ),
    );
  }

  Widget spacingBoxes() {
    return new Expanded(
      flex: 1,
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        height: 100.0,
      ),
    );
  }

  Widget overviewShimmer() {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(10.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            overviewCardShimmer(),
            spacingBoxes(),
            overviewCardShimmer(),
            spacingBoxes(),
            overviewCardShimmer()
          ],
        ),
      ),
      flex: 2,
    );
  }

  Widget loadShimmer() {
    return new SingleChildScrollView(
      child: Container(
        color: Constants.appColorL1,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            appBarShimmer(),
            ShimmerCards(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
