import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flixjini_webapp/common/constants.dart';

class ShimmerPopularSearch extends StatelessWidget {
  ShimmerPopularSearch({
    Key? key,
  }) : super(key: key);

  Widget headingShimmer(double width) {
    return Container(
      padding: const EdgeInsets.all(20.0),
      child: Shimmer.fromColors(
        baseColor: Constants.appColorL2,
        highlightColor: Constants.appColorL2,
        child: Container(
          height: 40.0,
          width: width,
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget episodeCards() {
    return new Container(
      padding: EdgeInsets.all(5.0),
      child: Shimmer.fromColors(
      baseColor: Constants.appColorL2,
      highlightColor: Constants.appColorL2,
        child: Container(
          height: 35.0,
          color: Constants.appColorL1,
        ),
      ),
    );
  }

  Widget loadEpisodesShimmer() {
    return Container(
      child: new ListView.builder(
          itemCount: 10,
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.all(5.0),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return episodeCards();
          }),
    );
  }

  Widget loadShimmer() {
    return new SingleChildScrollView(
      child: new Container(
        color: Constants.appColorL1,
        padding: const EdgeInsets.all(1.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            headingShimmer(200.0),
            loadEpisodesShimmer(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return loadShimmer();
  }
}
