import 'package:flutter/material.dart';

class Constants {
  // change before every release
  static const String apiUrl =
      "https://api.flixjini.com"; //"https://ae88-159-65-159-247.ngrok.io";
  static bool debug = true;
  static String versionNameAndroid = "1";
  static String versionCodeAndroid = "1.0";
  static String shareMovieText = "";
  static String shareTvshowText = "";
  static Color appColorL1 = Color(0xFF1E1E1E);
  static Color appColorL2 = Color(0xFF2E2E2E);
  static Color appColorL3 = Color(0xFF3E3E3E);
  static Color appColorShadow = Color(0xFF111111);
  static Color appColorDivider = Color(0xFF777777);
  static Color appColorError = Color(0xFFe74c3c);
  static Color appColorLogo = Color(0xFF43C681);
  static Color appColorIcons = Color(0xFF43C681);
  static Color appColorFont = Color(0xFFFFFFFF);
}
