// import	'package:flutter/material.dart';
// import  'dart:async';
// import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:firebase_analytics/observer.dart';

// final FirebaseAnalytics analytics = FirebaseAnalytics();
// final FirebaseAnalyticsObserver observer =
//     FirebaseAnalyticsObserver(analytics: analytics);

import 'package:flixjini_webapp/utils/common_utils.dart';

logUserScreen(String screenNameSent, String screenClassOverrideSent) async {
  // await analytics.setCurrentScreen(
  //   screenName: screenNameSent,
  //   screenClassOverride: screenClassOverrideSent,
  // );
  printIfDebug('Screenlog ' + screenNameSent + ' succeeded');
}

logFilterOption(String filterKey, String filterOption) async {
  // await analytics.logEvent(
  //   name: filterOption,
  //   parameters: <String, dynamic>{
  //     'key': filterKey,
  //   },
  // );
  printIfDebug('Filter Option Logged  -> ' + filterKey + ' : ' + filterOption);
}

logFilterKey(String filterKey, var filterOptions) async {
  // await analytics.logEvent(
  //   name: filterKey,
  //   parameters: <String, dynamic>{'filter_ids': filterOptions},
  // );
  printIfDebug('Filter Key Logged -> ' + filterKey + ' : ' + filterOptions);
}
