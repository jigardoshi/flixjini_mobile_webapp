import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:async';
import 'package:flixjini_webapp/common/constants.dart';

final Map<String, Item> _items = <String, Item>{};

class Item {
  Item({required this.itemId});
  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();
  Stream<Item> get onChanged => _controller.stream;

  late String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = '/detail/$itemId';
    return routes.putIfAbsent(
      routeName,
      () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => PushNotification(itemId),
      ),
    );
  }
}

class PushNotification extends StatefulWidget {
  PushNotification(this.itemId);
  final String itemId;
  @override
  _PushNotificationState createState() => _PushNotificationState();
}

class _PushNotificationState extends State<PushNotification> {
  late Item _item;
  late StreamSubscription<Item> _subscription;

  @override
  void initState() {
    super.initState();
    _item = _items[widget.itemId]!;
    if (_item != null)
      _subscription = _item.onChanged.listen((Item item) {
        if (!mounted) {
          _subscription.cancel();
        } else {
          setState(() {
            _item = item;
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return _item == null
        ? Container()
        : Scaffold(
            backgroundColor: Constants.appColorL2,
            appBar: AppBar(
              iconTheme: new IconThemeData(color: Constants.appColorDivider),
              backgroundColor: Constants.appColorL2,
              title: _item != null
                  ? Text("Item ${_item.itemId}")
                  : Text(
                      "Flixjini",
                      style: TextStyle(
                        color: Constants.appColorFont,
                      ),
                    ),
            ),
            body: Material(
              color: Constants.appColorL2,
              child: Center(
                child: _item != null
                    ? Text("Item status: ${_item.status}")
                    : Text(" "),
              ),
            ),
          );
  }
}
