import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';

class SlantClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(size.width / 4, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo((size.width * 3) / 4, 0.0);
    path.lineTo(0.0, 0.0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
