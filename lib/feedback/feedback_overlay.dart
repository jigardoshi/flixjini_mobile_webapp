import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'dart:async';
// import 'package:flixjini_webapp/utils/common_utils.dart';
// import 'package:launch_review/launch_review.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:facebook_app_events/facebook_app_events.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import 'dart:io' show Platform;
// import 'package:firebase_analytics/firebase_analytics.dart';

class FeedbackOverlay extends StatefulWidget {
  FeedbackOverlay({
    Key? key,
    this.changeFeedback,
    this.authenticationToken,
    this.url,
    this.textFlag,
  }) : super(key: key);

  final changeFeedback;
  final authenticationToken, url, textFlag;

  @override
  _FeedbackOverlayState createState() => new _FeedbackOverlayState();
}

class _FeedbackOverlayState extends State<FeedbackOverlay> {
  bool showText = false, changedFeedback = false, firstFeedback = true;
  String character = "", textInp = "", email = "";
  double? ratingBar;
  TextEditingController feedbackController = TextEditingController();
  FirebaseAnalytics analytics = FirebaseAnalytics();
  // static final facebookAppEvents = FacebookAppEvents();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  @override
  void initState() {
    super.initState();
    showText = widget.textFlag ?? false;
    checkFeedbackFlag();
  }

  checkFeedbackFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? feedbackFlag = prefs.getString("feedback");

    if (feedbackFlag == null || feedbackFlag == "submitted") {
      setState(() {
        firstFeedback = false;
      });
    }
  }

  Widget getTitleContainer() {
    return Container(
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(8.0),
          topRight: const Radius.circular(8.0),
        ),
        color: Constants.appColorL3,
      ),
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            "Give Us Your Feedback",
            style: TextStyle(
              color: Constants.appColorFont.withOpacity(0.7),
              fontSize: 25,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 10,
            ),
            child: Text(
              "How do you like our App? Does it meets your expectations and delight you?",
              style: TextStyle(
                color: Constants.appColorFont.withOpacity(0.7),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget getRatingContainer() {
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenWidth - 20,
      color: !showText ? Constants.appColorL2 : Constants.appColorL3,
      padding: EdgeInsets.only(
        top: !showText ? 40 : 10,
        bottom: !showText ? 40 : 25,
      ),
      child: Center(
        child: RatingBar(
          itemSize: 50.0,
          // initialRating: 3.5,
          direction: Axis.horizontal,
          allowHalfRating: false,
          itemCount: 5,

          ratingWidget: RatingWidget(
            full: Image.asset('assests/star_filled.png'),
            half: Image.asset('assests/star_filled.png'),
            empty: Image.asset('assests/star_empty.png'),
          ),

          onRatingUpdate: (rating) {
            if (rating == 5) {
              setState(() {
                showText = false;
              });
              _sendAnalyticsEvent(rating.round().toString());
              //platform.invokeMethod('logRateEvent');
              showDialog(
                context: context,
                barrierDismissible:
                    false, // user must tap button for close dialog!
                builder: (BuildContext context) {
                  // double screenWidth = MediaQuery.of(context).size.width;

                  return AlertDialog(
                    backgroundColor: Constants.appColorL2,
                    content: Container(
                      height: 120,
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              top: 10,
                            ),
                            child: Text(
                              "We are glad you love our app. Want to give us a 5* rating on the play store?",
                              style: TextStyle(
                                color: Constants.appColorFont,
                                fontSize: 15,
                                height: 1,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Spacer(),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  left: 30,
                                  right: 30,
                                  top: 7,
                                  bottom: 7,
                                ),
                                decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Constants.appColorLogo,
                                  ),
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                child: GestureDetector(
                                  child: Text(
                                    'Yes',
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: Constants.appColorLogo,
                                    ),
                                  ),
                                  onTap: () {
                                    setFeedbackFlag(0);

                                    widget.changeFeedback(false);
                                    try {
                                      // LaunchReview.launch(
                                      //     androidAppId:
                                      //         "com.flixjini.watchonline"
                                      //     // iOSAppId: "585027354",I
                                      //     // writeReview: false,
                                      //     );
                                    } catch (e) {
                                      printIfDebug(e.toString());
                                    }
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                              Container(
                                // margin: EdgeInsets.only(
                                //   left: 20,
                                // ),
                                padding: EdgeInsets.only(
                                  left: 30,
                                  right: 30,
                                  top: 7,
                                  bottom: 7,
                                ),
                                decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Constants.appColorLogo,
                                  ),
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                child: GestureDetector(
                                  child: Text(
                                    'No',
                                    style: TextStyle(
                                      color: Constants.appColorLogo,
                                      fontSize: 13,
                                    ),
                                  ),
                                  onTap: () {
                                    widget.changeFeedback(false);
                                    setFeedbackFlag(1);

                                    Navigator.of(context).pop();
                                    // showToast(
                                    // 'Logout Successful', 'long', 'bottom', 1, Constants.appColorLogo);
                                  },
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              setState(() {
                ratingBar = rating;
                showText = true;
                feedbackController.text = getFeedbackTemplate(rating.round());
                SystemChannels.textInput.invokeMethod('TextInput.show');
              });
            }
            printIfDebug(rating);
          },
        ),
      ),
    );
  }

  String getFeedbackTemplate(int index) {
    switch (index) {
      case 1:
        return "I choose 1 Star because ";
        break;
      case 2:
        return "This app need a lot more work on features like ";
        break;
      case 3:
        return "I would suggest this app needs work on ";
        break;
      case 4:
        return "I like the app but ";
        break;

      default:
        return "Enter Your Feedback";
    }
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: 'star' + tagName.toString(),
      parameters: <String, dynamic>{
        'page': 'feedback',
      },
    );
    // facebookAppEvents.logEvent(
    //   name: 'star' + tagName.toString(),
    //   parameters: {
    //     'page': 'feedback',
    //   },
    //   valueToSum: 1,
    // );
  }

  Widget getSubmitContainer() {
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenWidth - 20,
      decoration: new BoxDecoration(
        color: Constants.appColorL3,
        borderRadius: new BorderRadius.only(
          bottomLeft: Radius.circular(8.0),
          bottomRight: Radius.circular(8.0),
        ),
      ),
      padding: EdgeInsets.only(
        top: showText ? 20 : 0,
        bottom: 10,
        left: 60,
        right: 60,
      ),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          showText
              ? GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 50,
                      right: 50,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      borderRadius: new BorderRadius.circular(30.0),
                      color: Constants.appColorLogo,
                    ),
                    child: Center(
                      child: Text(
                        'Give FeedBack',
                        style: TextStyle(
                            fontSize: 13,
                            color: Constants.appColorL1,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  onTap: () async {
                    if (widget.authenticationToken.toString().isNotEmpty) {
                      if (!changedFeedback && firstFeedback) {
                        showDialog(
                          context: context,
                          barrierDismissible:
                              false, // user must tap button for close dialog!
                          builder: (BuildContext context) {
                            // double screenWidth = MediaQuery.of(context).size.width;

                            return AlertDialog(
                              backgroundColor: Constants.appColorL2,
                              content: Container(
                                height: 120,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: Text(
                                        "Would you like to give us some feedback?",
                                        style: TextStyle(
                                          color: Constants.appColorFont,
                                          fontSize: 15,
                                          height: 1,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Spacer(),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 30,
                                            right: 30,
                                            top: 7,
                                            bottom: 7,
                                          ),
                                          decoration: new BoxDecoration(
                                            border: new Border.all(
                                              color: Constants.appColorLogo,
                                            ),
                                            borderRadius:
                                                new BorderRadius.circular(30.0),
                                          ),
                                          child: GestureDetector(
                                            child: Text(
                                              'Yes',
                                              style: TextStyle(
                                                fontSize: 13,
                                                color: Constants.appColorLogo,
                                              ),
                                            ),
                                            onTap: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ),
                                        Container(
                                          // margin: EdgeInsets.only(
                                          //   left: 20,
                                          // ),
                                          padding: EdgeInsets.only(
                                            left: 30,
                                            right: 30,
                                            top: 7,
                                            bottom: 7,
                                          ),
                                          decoration: new BoxDecoration(
                                            border: new Border.all(
                                              color: Constants.appColorLogo,
                                            ),
                                            borderRadius:
                                                new BorderRadius.circular(30.0),
                                          ),
                                          child: GestureDetector(
                                            child: Text(
                                              'No',
                                              style: TextStyle(
                                                color: Constants.appColorLogo,
                                                fontSize: 13,
                                              ),
                                            ),
                                            onTap: () {
                                              setFeedbackFlag(0);
                                              sendFeedback(ratingBar, textInp,
                                                  character);
                                              _sendAnalyticsEvent(ratingBar!
                                                  .round()
                                                  .toString());
                                              widget.changeFeedback(false);
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        printIfDebug(textInp);
                        setFeedbackFlag(0);
                        _sendAnalyticsEvent(ratingBar!.round().toString());
                        sendFeedback(ratingBar, textInp, character);
                        widget.changeFeedback(false);
                      }
                    } else {
                      showDialog(
                        context: context,
                        barrierDismissible:
                            false, // user must tap button for close dialog!
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: Constants.appColorL2,
                            content: Container(
                              height: 160,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: Text(
                                      "You are not logged in would you like to provide you Email?",
                                      style: TextStyle(
                                        color: Constants.appColorFont,
                                        fontSize: 15,
                                        height: 1,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                    height: 30,
                                    // color: Constants.appColorFont,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Constants.appColorFont
                                              .withOpacity(0.7),
                                        ),
                                      ),
                                    ),
                                    margin: EdgeInsets.only(
                                      top: 30,
                                      left: 20,
                                      right: 20,
                                    ),
                                    child: TextField(
                                      cursorColor: Constants.appColorDivider,
                                      onChanged: (text) {
                                        setState(() {
                                          email = text;
                                        });
                                      },
                                      style: TextStyle(
                                          color: Constants.appColorFont,
                                          fontSize: 13),
                                      decoration: InputDecoration(
                                        hintStyle: TextStyle(
                                          color: Constants.appColorDivider,
                                        ),
                                        border: InputBorder.none,
                                        hintText: 'Enter your Email id!',
                                        fillColor: Constants.appColorFont,
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 30,
                                          right: 30,
                                          top: 7,
                                          bottom: 7,
                                        ),
                                        decoration: new BoxDecoration(
                                          border: new Border.all(
                                            color: Constants.appColorLogo,
                                          ),
                                          borderRadius:
                                              new BorderRadius.circular(30.0),
                                        ),
                                        child: GestureDetector(
                                          child: Text(
                                            'Proceed',
                                            style: TextStyle(
                                              fontSize: 13,
                                              color: Constants.appColorLogo,
                                            ),
                                          ),
                                          onTap: () {
                                            setFeedbackFlag(0);
                                            sendFeedback(
                                                ratingBar, textInp, character);
                                            _sendAnalyticsEvent(
                                                ratingBar!.round().toString());

                                            widget.changeFeedback(false);
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    }
                  },
                )
              : Container(),
          Container(
            padding: EdgeInsets.only(
              top: 10,
            ),
            child: GestureDetector(
              child: Container(
                padding: EdgeInsets.only(
                  left: 50,
                  right: 50,
                  top: 10,
                  bottom: 10,
                ),
                // decoration: new BoxDecoration(
                //   border: new Border.all(
                //     color: Constants.appColorLogo,
                //   ),
                //   borderRadius: new BorderRadius.circular(30.0),
                //   color: Constants.appColorLogo,
                // ),
                child: Center(
                  child: Text(
                    'Not Now',
                    style: TextStyle(
                        fontSize: 13,
                        color: Constants.appColorFont.withOpacity(0.7),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              onTap: () async {
                // setFeedbackFlag(1);
                try {
                  widget.changeFeedback(false);
                } catch (e) {
                  printIfDebug(e);
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  setFeedbackFlag(int i) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (i == 0) {
      prefs.setString("feedback", "submitted");
    } else {
      prefs.setString("feedback", "later");
    }
  }

  Widget getTextContainer() {
    return Container(
      height: 150,
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: TextField(
        controller: feedbackController,
        cursorColor: Constants.appColorDivider,
        onChanged: (text) {
          String checkUrl = widget.url != null &&
                  widget.url.toString().length > 0
              ? feedbackController.text + " , url = " + widget.url.toString()
              : feedbackController.text;
          setState(() {
            changedFeedback = true;
            textInp = checkUrl;
          });
        },
        style: TextStyle(color: Constants.appColorFont, fontSize: 13),
        decoration: InputDecoration(
          hintStyle: TextStyle(
            color: Constants.appColorDivider,
          ),
          border: InputBorder.none,
          hintText: 'Enter your Feedback!',
          fillColor: Constants.appColorFont,
        ),
      ),
    );
  }

  Widget getRadioContainer() {
    double screenWidth = MediaQuery.of(context).size.width - 20;

    return Container(
      color: Constants.appColorL3,
      width: screenWidth,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(
                left: 20,
              ),
              child: Row(
                children: <Widget>[
                  Radio(
                    value: "bug",
                    groupValue: character,
                    onChanged: (String? value) {
                      setState(() {
                        character = value!;
                      });
                    },
                  ),
                  Text(
                    'Bug',
                    style: TextStyle(
                      fontSize: 10,
                      color: Constants.appColorFont.withOpacity(0.7),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  value: "suggestion",
                  groupValue: character,
                  onChanged: (String? value) {
                    setState(() {
                      character = value!;
                    });
                  },
                ),
                Text(
                  'Suggestion',
                  style: TextStyle(
                    fontSize: 10,
                    color: Constants.appColorFont.withOpacity(0.7),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              children: <Widget>[
                Radio(
                  value: "other",
                  groupValue: character,
                  onChanged: (String? value) {
                    setState(() {
                      character = value!;
                    });
                  },
                ),
                Text(
                  'Other',
                  style: TextStyle(
                    fontSize: 10,
                    color: Constants.appColorFont.withOpacity(0.7),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getFeedbackContainer() {
    return Column(
      children: <Widget>[
        getTitleContainer(),
        getRatingContainer(),
        showText ? getTextContainer() : Container(),
        showText ? getRadioContainer() : Container(),
        getSubmitContainer(),
      ],
    );
  }

  sendFeedback(star, feedback, feedbacktype) async {
    Map data = {};
    if (widget.authenticationToken.toString().isNotEmpty) {
      data = {
        'jwt_token': widget.authenticationToken,
        'page_name': 'index_page',
        'feedback_textarea': feedback,
        'star_rating': star.toString(),
        'feedback_type': feedbacktype,
      };
    } else {
      data = {
        'jwt_token': widget.authenticationToken,
        'page_name': 'index_page',
        'feedback_textarea': feedback,
        'star_rating': star.toString(),
        'feedback_type': feedbacktype,
        'email': email,
      };
    }
    printIfDebug(data.toString());
    var url = 'https://api.flixjini.com/queue/feedback.json?';
    url = await fetchDefaultParams(url);
    try {
      http.post(Uri.parse(url), body: data).then((response) {
        printIfDebug("Response status: ${response.statusCode}");
        printIfDebug("Response body: ${response.body}");
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug(jsonResponse);
        if (response.statusCode == 200) {
          if (jsonResponse["error"] == "false")
            showToast('Thank you for your feedback, We shall try to improve!',
                'long', 'bottom', 1, Constants.appColorLogo);
        } else {
          printIfDebug("Non sucessesful response from server");
        }
      });
    } catch (e) {
      printIfDebug("print error:" + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;

    // double screenHeight = MediaQuery.of(context).size.height;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(8.0),
              topRight: const Radius.circular(8.0),
              bottomLeft: const Radius.circular(8.0),
              bottomRight: const Radius.circular(8.0),
            ),
            color: Constants.appColorL2,
          ),
          margin: EdgeInsets.only(
            left: 10,
          ),
          width: screenWidth - 20,
          child: getFeedbackContainer(),
        ),
      ],
    );
  }
}
