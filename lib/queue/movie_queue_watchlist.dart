import 'package:flixjini_webapp/common/constants.dart';
import 'package:flixjini_webapp/common/error_handle_page.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 	"dart:async";
import 'package:shared_preferences/shared_preferences.dart';

class MovieQueueWatchList extends StatefulWidget {
  MovieQueueWatchList({
    Key? key,
    this.queueDetails,
    this.getUpdatedList,
    this.userDetail,
    this.userQueue,
    this.authenticationToken,
    this.showOriginalPosters,
    this.page,
    this.removeFromLocal,
  }) : super(key: key);

  final queueDetails;
  final getUpdatedList;
  final userDetail;
  final userQueue;
  final authenticationToken, showOriginalPosters;
  final page;
  final removeFromLocal;

  @override
  _MovieQueueWatchListState createState() => new _MovieQueueWatchListState();
}

class _MovieQueueWatchListState extends State<MovieQueueWatchList> {
  var userPreferences;
  var userQueuePreferences;
  var watchList = {"movies": []};
  List seenIds = <String>[];

  void initState() {
    super.initState();
    printIfDebug("Hi");
    super.initState();
    if (widget.authenticationToken.isNotEmpty) {
      setState(() {
        userPreferences = widget.userDetail;
        userQueuePreferences = widget.userQueue;
      });
      createUserQueue();
    } else {
      printIfDebug(
          "No need for User preferences as user hasn't been authorised");
    }
    getSeenMovieIds();
  }

  createUserQueue() {
    int size = userQueuePreferences["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        var movie = {
          "id": userQueuePreferences["movies"][i]["id"],
          "status": true,
        };
        setState(() {
          watchList["movies"]!.add(movie);
        });
      }
    }
    // printIfDebug("Movie Watchlist");
    // printIfDebug(watchList);
    printIfDebug("DOne");
  }

  basedOnUserQueue(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (watchList["movies"]![i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          flag = 1;
          return watchList["movies"]![i]["status"];
        }
      }
      if (flag == 0) {
        return false;
      }
    }
  }

  basedonUserPreference(movie, entity) {
    int size = userPreferences["movies"].length;
    int flag = 0;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (userPreferences["movies"][i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          flag = 1;
          return userPreferences["movies"][i][entity];
        }
      }
      if (flag == 0) {
        return false;
      }
    }
  }

  removeMovieFromWatchList(movie, actionUrl) async {
    String url = actionUrl;
    printIfDebug(url);
    // widget.getUpdatedList(movie["id"]);

    url = await fetchDefaultParams(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          updateUserQueue(movie);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  removeMovieFromList(movieId, actionUrl) async {
    printIfDebug(actionUrl.toString());
    String url = actionUrl;
    printIfDebug(url);
    Map data = {
      'jwt_token': widget.authenticationToken,
      'movie_id': movieId,
    };
    printIfDebug(data.toString());

    // widget.getUpdatedList(movie["id"]);
    url = await fetchDefaultParams(url);
    try {
      var response = await http.post(Uri.parse(url), body: data);

      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          // updateUserQueue(movie);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkAction(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        flag = 1;
        if (watchList["movies"]![i]["status"]) {
          return false;
        } else {
          return true;
        }
      }
    }
    if (flag == 0) {
      return true;
    }
  }

  updateUserQueue(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug("wishlist");
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        setState(() {
          watchList["movies"]![i]["status"] =
              watchList["movies"]![i]["status"] ? false : true;
        });
        flag = 1;
        printIfDebug("Change Existing Entry in Queue");
        break;
      }
    }
    if (flag == 0) {
      var entity = {
        "id": movie["id"],
        "status": true,
      };
      setState(() {
        watchList["movies"]!.add(entity);
      });
      printIfDebug("Add New Entry to Queue");
      printIfDebug(watchList);
    }
    printIfDebug(movie["id"]);
  }

  sendRequest(movie, actionUrl, entity) async {
    printIfDebug((widget.authenticationToken).isEmpty);
    if (!widget.authenticationToken.isEmpty) {
      bool flag = true;
      if (entity == "add") {
        flag = checkAction(movie);
      }
      if (flag) {
        Map data = {
          'jwt_token': widget.authenticationToken,
          'movie_id': movie["id"],
        };
        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              if (entity == "add") {
                updateUserQueue(movie);
              } else {
                updateUserPreferences(movie, entity);
              }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"] +
            '&jwt_token=' +
            widget.authenticationToken;
        printIfDebug("Remove From Watch List");
        printIfDebug(actionUrl);
        removeMovieFromWatchList(movie, actionUrl);
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  updateUserPreferences(movie, entity) {
    int size = userPreferences["movies"].length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug(entity);
    for (int i = 0; i < size; i++) {
      if (userPreferences["movies"][i]["id"] == movie["id"]) {
        setState(() {
          userPreferences["movies"][i][entity] =
              userPreferences["movies"][i][entity] ? false : true;
        });
        if ((entity == "like") || (entity == "dislike")) {
          String opposite = (entity == "like") ? "dislike" : "like";
          if (userPreferences["movies"][i][entity] &&
              userPreferences["movies"][i][opposite]) {
            setState(() {
              userPreferences["movies"][i][opposite] =
                  !userPreferences["movies"][i][entity];
            });
          }
        } else {
          printIfDebug(
              "Another option apart from like and dislike have been selected");
        }
        flag = 1;
        printIfDebug("Change Existing Entry");
        break;
      }
    }
    if (flag == 0) {
      var userChoice = {
        "id": movie["id"],
        "like": false,
        "dislike": false,
        "seen": false,
      };
      userChoice[entity] = userChoice[entity] ? false : true;
      setState(() {
        userPreferences["movies"].add(userChoice);
      });
      printIfDebug("Add New Entry");
      printIfDebug(userPreferences);
    }
    printIfDebug(movie["id"]);
  }

  removeFromQueue(index) {
    return new Positioned(
      top: 0.0,
      right: 0.0,
      child: new GestureDetector(
        child: new Container(
          padding: const EdgeInsets.all(2.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Constants.appColorFont.withOpacity(0.9),
          ),
          child: new Image.asset(
            "assests/close_circle.png",
            width: 25.0,
            height: 25.0,
            fit: BoxFit.cover,
          ),
        ),
        onTap: () {
          String actionUrl =
              'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
                  widget.queueDetails["movies"][index]["id"] +
                  '&jwt_token=' +
                  widget.authenticationToken;
          printIfDebug("Remove From Watch List");
          printIfDebug(actionUrl);
          removeMovieFromWatchList(
              widget.queueDetails["movies"][index], actionUrl);
        },
      ),
    );
  }

  getSeenMovieIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    seenIds = prefs.getStringList("seenids")!;
    printIfDebug("seenids :" + seenIds.toString());
  }

  Widget movieCardBuilder(index, String key, int pagefor) {
    return Container(
      child: new Stack(
        // fit: StackFit,
        children: <Widget>[
          Container(
            child: new CardStructure(
              movie: widget.queueDetails[key][index],
              sendRequest: sendRequest,
              basedOnUserQueue: basedOnUserQueue,
              basedonUserPreference: basedonUserPreference,
              authenticationToken: widget.authenticationToken,
              showOriginalPosters: widget.showOriginalPosters,
              pagefor: pagefor,
            ),
          ),
          // removeFromQueue(index),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Container(
                  padding: EdgeInsets.only(
                    right: 15,
                    top: 12,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          height: 25,
                          // width: 25,
                          child: Opacity(
                            opacity: 1,
                            child: Image.asset(
                              "assests/remove_red.png",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        onTap: () {
                          try {
                            String actionUrl = "";
                            String movieId = widget.queueDetails[key][index]
                                    ["id"]
                                .toString();
                            widget.removeFromLocal(index);

                            switch (widget.page) {
                              case 0:
                                actionUrl =
                                    'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
                                        movieId +
                                        '&jwt_token=' +
                                        widget.authenticationToken;
                                printIfDebug("Remove From Watch List");
                                printIfDebug(actionUrl);

                                removeMovieFromWatchList(movieId, actionUrl);
                                break;
                              case 1:
                                actionUrl =
                                    "https://api.flixjini.com/queue/reset_seen.json";
                                printIfDebug("remove seen" + movieId);

                                removeMovieFromList(movieId, actionUrl);
                                break;
                              case 2:
                                actionUrl =
                                    "https://api.flixjini.com/queue/like.json";
                                printIfDebug("remove like" + movieId);
                                removeMovieFromList(movieId, actionUrl);

                                break;
                              case 3:
                                printIfDebug("remove disliked");

                                actionUrl =
                                    "https://api.flixjini.com/queue/dislike.json";
                                removeMovieFromList(movieId, actionUrl);

                                break;
                              case 4:
                                printIfDebug("remove filter");
                                break;

                              default:
                                break;
                            }
                          } catch (e) {
                            printIfDebug(e);
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget filteredMoviesGrid(String key, int pagefor) {
    double height = MediaQuery.of(context).size.height - 200;
    var orientation = MediaQuery.of(context).orientation;
    return new Container(
      height: height,
      child: AnimationLimiter(
        child: GridView.builder(
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 0.63,
              // mainAxisSpacing: 5.0,
              // crossAxisSpacing: 5.0,
              crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
          // padding: const EdgeInsets.all(20.0),
          itemCount: widget.queueDetails[key].length,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: const Duration(milliseconds: 375),
              columnCount: widget.queueDetails[key].length,
              child: ScaleAnimation(
                child: FadeInAnimation(
                  child: GridTile(
                    child: new GestureDetector(
                      onTap: () {},
                      child: movieCardBuilder(index, key, pagefor),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    // double screenHeight = MediaQuery.of(context).size.height;
    if (widget.page != 4) {
      String message = "Sorry, you haven't added any Movies to this List";
      String imagePath = "assests/no_movie_queue.png";
      return widget.queueDetails != null &&
              widget.queueDetails["movies"].length > 0
          ? filteredMoviesGrid("movies", 1)
          : Container(
              // height: screenHeight,
              margin: EdgeInsets.only(top: 150),
              child: ErrorHandlePage(
                message: message,
                imagePath: imagePath,
              ),
            );
    } else {
      String message = "Sorry, you haven't followed any Tags";
      String imagePath = "assests/no_movie_queue.png";
      return widget.queueDetails != null &&
              widget.queueDetails["filters"].length > 0
          ? filteredMoviesGrid("filters", 4)
          : Container(
              // height: screenHeight,
              margin: EdgeInsets.only(top: 150),
              child: ErrorHandlePage(
                message: message,
                imagePath: imagePath,
              ),
            );
    }
  }
}
