import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/navigation/movie_navigator.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/queue/movie_queue_watchlist.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/profile/movie_profile_unauthenticated.dart';
import 'package:flixjini_webapp/common/error_handle_page.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_cards.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieQueuePage extends StatefulWidget {
  MovieQueuePage({
    this.movieQueue,
    this.moveToPreviousTab,
    this.showOriginalPosters,
    this.switchCardImages,
    Key? key,
  }) : super(key: key);

  final movieQueue;
  final moveToPreviousTab;
  final showOriginalPosters;
  final switchCardImages;

  @override
  _MovieQueuePageState createState() => new _MovieQueuePageState();
}

class _MovieQueuePageState extends State<MovieQueuePage> {
  bool queuePresent = false;
  bool fetchedIndexDetail = false;
  bool fetchedUserPreference = false;
  bool fetchedUserQueuePreference = false;
  bool authenticationTokenStatus = false;
  String authenticationToken = "",
      titleExpansion = "Queue",
      upDown = "assests/list_down.png";
  var queueDetails = {};
  var userDetail = {};
  var userQueue = {};
  var userSeen, userLiked, userDisliked, userFollowing;
  var actualData = {};
  bool fetchedUserSeen = false,
      fetchedUserLiked = false,
      fetchedUserDisliked = false,
      fetchedUserFollowing = false;
  static const platform = const MethodChannel('api.komparify/advertisingid');
  double screenWidth = 0.0;
  FirebaseAnalytics analytics = FirebaseAnalytics();

  int page = 0;

  @override
  void initState() {
    super.initState();
    sendEvent();
    if (widget.movieQueue == null) {
      authenticationStatus();
    } else {
      setState(() {
        fetchedIndexDetail = true;
      });
      // getListData();
    }
  }

  getListData() async {
    getList("");
    getList("like");
    getList("dislike");
  }

  getList(String data) async {
    String seenApi = "https://api.flixjini.com/queue/v2/get_lists?";
    seenApi = authenticationToken.length > 0
        ? (seenApi + "jwt_token=" + authenticationToken + "&")
        : seenApi;
    seenApi = data.isNotEmpty ? seenApi + "type=" + data + "&" : seenApi;
    String seenApiUrl = constructHeader(seenApi);
    seenApiUrl = await fetchDefaultParams(seenApiUrl);
    printIfDebug(seenApiUrl);

    try {
      var response = await http.get(Uri.parse(seenApiUrl));
      printIfDebug(response.statusCode);
      printIfDebug(response.statusCode == 200);
      if (response.statusCode == 200) {
        var fetchedData = json.decode(response.body);
        printIfDebug(fetchedData);
        printIfDebug("Not yet In - resultsHaveBeenCleared");
        printIfDebug("Yes we In");
        switch (data) {
          case "":
            setState(() {
              userSeen = fetchedData;
              fetchedUserSeen = true;
              actualData = fetchedData;
            });
            break;
          case "like":
            setState(() {
              userLiked = fetchedData;
              actualData = fetchedData;

              fetchedUserLiked = true;
            });
            break;
          case "dislike":
            setState(() {
              userDisliked = fetchedData;
              actualData = fetchedData;

              fetchedUserDisliked = true;
            });
            break;
          case "following":
            setState(() {
              userFollowing = fetchedData;
              actualData = fetchedData;

              fetchedUserFollowing = true;
            });
            break;
          default:
            break;
        }
      } else {
        // this.movieDetail["error"] =
        //     'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      printIfDebug(exception.toString());
    }
  }

  sendEvent() {
    _sendAnalyticsEvent("mylist_click");
    //platform.invokeMethod('logFindLocationEvent');
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: tagName,
      parameters: <String, dynamic>{},
    );
  }

  getPreferences() {
    getMovieQueue();
    getUserPreference();
    getUserQueuePreference();
    logUserScreen('Flixjini Queue Page', 'MovieQueuePage');
  }

  getMovieQueue() async {
    printIfDebug("Try to get for");
    printIfDebug('https://api.flixjini.com/queue/v2/list.json?jwt_token=' +
        authenticationToken);
    String url = 'https://api.flixjini.com/queue/v2/list.json?jwt_token=' +
        authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        this.queueDetails = data;
        setState(() {
          queueDetails = this.queueDetails;
          actualData = queueDetails;
          fetchedIndexDetail = true;
        });
        printIfDebug("Data has been fetched");
        printIfDebug(queueDetails);
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  removeFromLocal(int index) {
    switch (page) {
      case 0:
        setState(() {
          queueDetails["movies"].removeAt(index);
        });
        break;
      case 1:
        setState(() {
          userSeen["movies"].removeAt(index);
        });
        break;
      case 2:
        setState(() {
          userLiked["movies"].removeAt(index);
        });
        break;
      case 3:
        setState(() {
          userDisliked["movies"].removeAt(index);
        });
        break;
      case 4:
        try {
          deleteSavedFilter(
              userFollowing["filters"][index]["id"],
              userFollowing["filters"][index]["name"],
              userFollowing["filters"][index]["query_string"],
              index);
        } catch (e) {
          printIfDebug(e);
        }
        setState(() {
          userFollowing["filters"].removeAt(index);
        });

        break;
      default:
        break;
    }
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      authenticationTokenStatus = true;
    });
    if (authenticationToken.isNotEmpty) {
      getPreferences();
    } else {
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }
    // getListData();
  }

  getUserQueuePreference() async {
    String url = 'https://api.flixjini.com/queue/v2/list.json?jwt_token=' +
        authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  getUserPreference() async {
    String url =
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  Widget displayMovieDetails() {
    switch (page) {
      case 0:
        return new Container(
          color: Constants.appColorL1,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MovieQueueWatchList(
                queueDetails: queueDetails,
                userDetail: userDetail,
                userQueue: userQueue,
                authenticationToken: authenticationToken,
                showOriginalPosters: widget.showOriginalPosters,
                page: page,
                removeFromLocal: removeFromLocal,
              ),
            ],
          ),
        );
        // break;
      case 1:
        return new Container(
          color: Constants.appColorL1,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MovieQueueWatchList(
                queueDetails: userSeen,
                userDetail: userDetail,
                userQueue: userQueue,
                authenticationToken: authenticationToken,
                showOriginalPosters: widget.showOriginalPosters,
                page: page,
                removeFromLocal: removeFromLocal,
              ),
            ],
          ),
        );
        // break;
      case 2:
        return new Container(
          color: Constants.appColorL1,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MovieQueueWatchList(
                queueDetails: userLiked,
                userDetail: userDetail,
                userQueue: userQueue,
                authenticationToken: authenticationToken,
                showOriginalPosters: widget.showOriginalPosters,
                page: page,
                removeFromLocal: removeFromLocal,
              ),
            ],
          ),
        );
        // break;
      case 3:
        return new Container(
          color: Constants.appColorL1,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MovieQueueWatchList(
                queueDetails: userDisliked,
                userDetail: userDetail,
                userQueue: userQueue,
                authenticationToken: authenticationToken,
                showOriginalPosters: widget.showOriginalPosters,
                page: page,
                removeFromLocal: removeFromLocal,
              ),
            ],
          ),
        );
        // break;
      case 4:
        return new Container(
          color: Constants.appColorL1,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MovieQueueWatchList(
                queueDetails: userFollowing,
                userDetail: userDetail,
                userQueue: userQueue,
                authenticationToken: authenticationToken,
                showOriginalPosters: widget.showOriginalPosters,
                page: page,
                removeFromLocal: removeFromLocal,
              ),
            ],
          ),
        );
        // break;
      default:
        return new Container();
        // break;
    }
  }

  Widget displayLoader() {
    return Scaffold(
      appBar: appHeaderBar(),
      backgroundColor: Constants.appColorFont,
      body: new Center(
        child: Column(
          children: <Widget>[
            listContainerNew(),
            ShimmerCards(),
          ],
        ),
      ),
    );
  }

  Widget noMoviesInQueue() {
    String message = "Sorry, you haven't added any Movies to your Watchlist";
    String imagePath = "assests/no_movie_queue.png";
    return Scaffold(
      backgroundColor: Constants.appColorL1,
      appBar: appHeaderBar(),
      body: Column(
        children: <Widget>[
          listContainerNew(),
          new Center(
            child: new ErrorHandlePage(message: message, imagePath: imagePath),
          ),
        ],
      ),
    );
  }

  AppBar appHeaderBar() {
    return AppBar(
      elevation: 4.0,
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      // leading: Container(
      //   child: GestureDetector(
      //     onTap: () {
      //       widget.moveToPreviousTab();
      //     },
      //     // child: Image.asset(
      //     //   "assests/backarrow1.png",
      //     //   // height: 15,
      //     //   color: Constants.appColorFont,
      //     // ),
      //     child: Icon(Icons.arrow_back),
      //   ),
      // ),
      title: Text(
        // titleExpansion.toUpperCase(),
        "ACTIVITY",
        style: TextStyle(
          color: Constants.appColorFont,
          fontSize: 18,
        ),
      ),
    );
  }

  void deleteSavedFilter(
      int filterId, String filterName, String filterQueryString, int i) async {
    String apiEndpointForDeleteFilter =
        "https://api.flixjini.com/queue/delete_filters.json?";
    apiEndpointForDeleteFilter =
        await fetchDefaultParams(apiEndpointForDeleteFilter);
    apiEndpointForDeleteFilter += constructHeader('&');
    apiEndpointForDeleteFilter +=
        "&magic=true&country_code=IN&id=$filterId&jwt_token=";
    apiEndpointForDeleteFilter += authenticationToken;
    printIfDebug(
        '\n\ncomplete url before making a request to delete filter api is: $apiEndpointForDeleteFilter');

    callDeleteFilterAPI(apiEndpointForDeleteFilter, filterId, i, filterName);
  }

  void callDeleteFilterAPI(String url, int filterId, int i, String filterName) {
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        // printIfDebug('\n\ndecoded response: $resBody');

        showToast('Un-Followed ' + filterName.toString(), 'long', 'bottom', 1,
            Constants.appColorLogo);
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  Widget displayQueueUI() {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Constants.appColorL1,
      appBar: appHeaderBar(),
      body: new Container(
        // margin: EdgeInsets.only(top: 20),
        child: new SingleChildScrollView(
          child: new ConstrainedBox(
            constraints: new BoxConstraints(),
            child: Column(
              children: <Widget>[
                listContainerNew(),
                displayMovieDetails(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  listContainerNew() {
    return Container(
      width: screenWidth,
      color: Constants.appColorL2,
      padding: EdgeInsets.only(
        top: 20,
        bottom: 20,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                getPreferences();
                setState(() {
                  titleExpansion = "Queue";

                  page = 0;
                });
                _sendAnalyticsEvent("queue_mylist_clicked");
              },
              child: Container(
                padding: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 10,
                  // bottom: 5,
                ),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                decoration: BoxDecoration(
                  color:
                      page == 0 ? Constants.appColorLogo : Constants.appColorL3,
                  border: Border.all(
                    color: page == 0
                        ? Constants.appColorLogo
                        : Constants.appColorDivider.withOpacity(0.1),
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assests/queue_roulette.png",
                        height: 20,
                        width: 20,
                        color: page == 0
                            ? Constants.appColorL1
                            : Constants.appColorLogo,
                      ),
                    ),
                    Text(
                      "Queue",
                      style: TextStyle(
                        color: page == 0
                            ? Constants.appColorL1
                            : Constants.appColorFont.withOpacity(0.8),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _sendAnalyticsEvent("following_mylist_clicked");
                setState(() {
                  actualData = userSeen;
                  titleExpansion = "Following";

                  page = 4;
                });
                getList("following");

                // printIfDebug("seen");
                // printIfDebug(actualData.toString());
              },
              child: Container(
                padding: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 10,
                  // bottom: 5,
                ),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                decoration: BoxDecoration(
                  color:
                      page == 4 ? Constants.appColorLogo : Constants.appColorL3,
                  border: Border.all(
                    color: page == 4
                        ? Constants.appColorLogo
                        : Constants.appColorDivider.withOpacity(0.1),
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 20,
                      margin: EdgeInsets.only(
                        right: 3,
                        left: 3,
                      ),
                      child: Image.asset(
                        "assests/following_list.png",
                        height: 10,
                        width: 10,
                        color: page == 4
                            ? Constants.appColorL1
                            : Constants.appColorLogo,
                      ),
                    ),
                    Text(
                      "Following",
                      style: TextStyle(
                        color: page == 4
                            ? Constants.appColorL1
                            : Constants.appColorFont.withOpacity(0.8),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _sendAnalyticsEvent("seen_mylist_clicked");
                setState(() {
                  actualData = userSeen;
                  titleExpansion = "Seen";

                  page = 1;
                });
                getList("");

                printIfDebug("seen");
                printIfDebug(actualData.toString());
              },
              child: Container(
                padding: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 10,
                  // bottom: 5,
                ),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                decoration: BoxDecoration(
                  color:
                      page == 1 ? Constants.appColorLogo : Constants.appColorL3,
                  border: Border.all(
                    color: page == 1
                        ? Constants.appColorLogo
                        : Constants.appColorDivider.withOpacity(0.1),
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assests/seen_roulette.png",
                        height: 20,
                        width: 20,
                        color: page == 1
                            ? Constants.appColorL1
                            : Constants.appColorLogo,
                      ),
                    ),
                    Text(
                      "Seen",
                      style: TextStyle(
                        color: page == 1
                            ? Constants.appColorL1
                            : Constants.appColorFont.withOpacity(0.8),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _sendAnalyticsEvent("like_mylist_clicked");

                setState(() {
                  titleExpansion = "Liked";
                  actualData = userLiked;
                  page = 2;
                });
                getList("like");

                printIfDebug("liked");
                printIfDebug(actualData.toString());
              },
              child: Container(
                padding: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 10,
                  // bottom: 5,
                ),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                decoration: BoxDecoration(
                  color:
                      page == 2 ? Constants.appColorLogo : Constants.appColorL3,
                  border: Border.all(
                    color: page == 2
                        ? Constants.appColorLogo
                        : Constants.appColorDivider.withOpacity(0.1),
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assests/like_roulette.png",
                        height: 20,
                        width: 20,
                        color: page == 2
                            ? Constants.appColorL1
                            : Constants.appColorLogo,
                      ),
                    ),
                    Text(
                      "Liked",
                      style: TextStyle(
                        color: page == 2
                            ? Constants.appColorL1
                            : Constants.appColorFont.withOpacity(0.8),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _sendAnalyticsEvent("dislike_mylist_clicked");

                setState(() {
                  titleExpansion = "Disliked";

                  actualData = userDisliked;
                  page = 3;
                });
                getList("dislike");

                printIfDebug("dislike");
                printIfDebug(actualData.toString());
              },
              child: Container(
                padding: EdgeInsets.only(
                  // top: 5,
                  left: 5,
                  right: 10,
                  // bottom: 5,
                ),
                margin: EdgeInsets.only(
                  left: 10,
                ),
                decoration: BoxDecoration(
                  color:
                      page == 3 ? Constants.appColorLogo : Constants.appColorL3,
                  border: Border.all(
                    color: page == 3
                        ? Constants.appColorLogo
                        : Constants.appColorDivider.withOpacity(0.1),
                  ),
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Image.asset(
                        "assests/dislike_roulette.png",
                        height: 20,
                        width: 20,
                        color: page == 3
                            ? Constants.appColorL1
                            : Constants.appColorLogo,
                      ),
                    ),
                    Text(
                      "Disliked",
                      style: TextStyle(
                        color: page == 3
                            ? Constants.appColorL1
                            : Constants.appColorFont.withOpacity(0.8),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget authenticateToView() {
    String message = "Please Sign In to Access your Entertainment Watchlist";
    String imagePath = "assests/lock.flr";
    String animationName = "lock";
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Constants.appColorL2,
      appBar: appHeaderBar(),
      body: new MovieProfileUnauthenticated(
        message: message,
        imagePath: imagePath,
        animationName: animationName,
        switchCardImages: widget.switchCardImages,
        fromQueuePage: true,
      ),
    );
  }

  Widget checkDataReady() {
    if (authenticationToken.isEmpty && authenticationTokenStatus) {
      return authenticateToView();
    } else {
      if (fetchedIndexDetail &&
          (fetchedUserQueuePreference && fetchedUserPreference)) {
        try {
          if (actualData == null) {
            return displayLoader();
          } else {
            return displayQueueUI();
          }
        } catch (exception) {
          return displayLoader();
        }
      } else {
        return displayLoader();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    screenWidth = MediaQuery.of(context).size.width;
    try {
      return Scaffold(
        body: checkDataReady(),
        bottomNavigationBar: MovieNavigator(),
      );
    } catch (e) {
      printIfDebug(e);
      return Container();
    }
  }
}
