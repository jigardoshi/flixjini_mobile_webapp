import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/detail/movie_detail_page.dart';
import 'package:flixjini_webapp/splash/movie_landing_page.dart';
import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flutter/services.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flixjini_webapp/deeplink_bloc.dart';
import 'package:provider/provider.dart';
import 'main_widget.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:flixjini_webapp/common/constants.dart';

// please read the "notes.txt" file first, if you're new to the project "Flixjini" **************
void main() async {
  bool isInDebugMode = Constants.debug;

  // change before every release
  WidgetsFlutterBinding.ensureInitialized();

  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // Crashlytics.
      Zone.current.handleUncaughtError(details.exception, details.stack!);
    }
  };

  // await FlutterCrashlytics().initialize();

  runZoned<Future<Null>>(() async {
    runApp(MyApp());
  }, onError: (error, stackTrace) async {
    // Whenever an error occurs, call the `reportCrash` function. This will send
    // Dart errors to our dev console or Crashlytics depending on the environment.
    // await FlutterCrashlytics()
    //     .reportCrash(error, stackTrace, forceCrash: false);
  });
}

class MyApp extends StatelessWidget {
  static const platform = const MethodChannel('api.komparify/advertisingid');
  static final navigatorKey = GlobalKey<NavigatorState>();
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  MyApp({
    Key? key,
  }) : super(key: key) {
    // platform.setMethodCallHandler(_handleMethod);
  }

  static pushMoviePage(myMap, bool showOriginalPosters) async {
    // final navigatorKey = GlobalKey<NavigatorState>();

    navigatorKey.currentState!.push(
      MaterialPageRoute(builder: (BuildContext context) {
        return MovieDetailPage(
          urlInformation: myMap,
          notification: true,
          showOriginalPosters: showOriginalPosters,
        );
      }),
    );
  }

  static pushScreen(screen, bool showOriginalPosters) async {
    // final navigatorKey = GlobalKey<NavigatorState>();
    printIfDebug("screen" + screen);
    switch (screen) {
      case 'index':
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 0,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
      case 'genie':
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 2,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
      case 'search':
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 1,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
      case 'mylist':
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 3,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
      case 'profile':
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 4,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
      default:
        navigatorKey.currentState!.push(
          MaterialPageRoute(builder: (BuildContext context) {
            return MovieRouting(
              defaultTab: 2,
              showOriginalPosters: showOriginalPosters,
            );
          }),
        );
        break;
    }
  }

  static pushFilterPage(url, bool showOriginalPosters) async {
    navigatorKey.currentState!.push(
      MaterialPageRoute(
        builder: (BuildContext context) => MovieFilterPage(
          appliedFilterUrl: url,
          inTheaterKey: false,
          showOriginalPosters: showOriginalPosters,
        ),
      ),
    );
  }

  void navigateToSomePage(Map map, bool showOriginalPosters) {
    String urlPhase = '/entertainment/movie/',
        movieName,
        uriScheme,
        uriAuthority,
        uriHost,
        uriPath,
        action = map['action'];
    printIfDebug(
        '\n\nbefore navigating, action present in the map is: $action');

    Uri myUri = Uri.parse(action);
    uriScheme = myUri.scheme;
    uriAuthority = myUri.authority;
    uriHost = myUri.host;
    uriPath = myUri.path;
    printIfDebug(
        '\n\nscheme: $uriScheme, authority: $uriAuthority, host: $uriHost, path: $uriPath');

    if (uriScheme == 'https' &&
        uriHost == 'www.komparify.com' &&
        uriPath.contains('/entertainment/movie')) {
      movieName = getMovieName(uriPath);
      urlPhase += movieName + '.json';

      Map myMap = {
        "urlPhase": urlPhase,
        "fullUrl": 'null',
        "search_key": 'null',
      };
      printIfDebug('\n\nnavigating user to "$movieName" movie details pg');
      navigatorKey.currentState!.push(new MaterialPageRoute(
          builder: (BuildContext context) => new MovieDetailPage(
                urlInformation: myMap,
                showOriginalPosters: showOriginalPosters,
              )));
    }
  }

  MaterialPageRoute buildRoute(RouteSettings settings, Widget builder) {
    return new MaterialPageRoute(
      settings: settings,
      builder: (ctx) => builder,
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');

    // DeepLinkBloc? _bloc = DeepLinkBloc();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    // return Container();
    return new MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      color: Constants.appColorL2,
      title: 'Movie Details',
      onGenerateRoute: (RouteSettings settings) {
        String redirectUrl = "";
        if (settings.name!.contains('/entertainment/movie')) {
          redirectUrl = '/entertainment/movie';
        } else {
          redirectUrl = settings.name!;
        }
        switch (redirectUrl) {
          case '/':
            return MaterialPageRoute(
                builder: (context) => MovieLandingPage(
                      navigatorKey: navigatorKey,
                      context: context,
                    ));
            // break;
          case '/entertainment/index':
            return MaterialPageRoute(
              builder: (context) => MovieRouting(
                defaultTab: 0,
              ),
            );
            // break;
          case '/entertainment/fresh':
            return MaterialPageRoute(
              builder: (context) => MovieRouting(defaultTab: 2),
            );
            // break;
          case '/entertainment/movie':
            return buildRoute(
              settings,
              new MovieDetailPage(urlInformation: settings.arguments),
            );
            // break;
        }
        return null; // just used this for the sake of clearing the warning
      },
      theme: new ThemeData(
        fontFamily: 'ProximaNova',
        accentColor: Constants.appColorL1.withOpacity(1),
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
      ),
      home: Scaffold(
        backgroundColor: Colors.black,
        // body: Container(),
        body:
            // Provider<DeepLinkBloc?>(
            // builder: (context) => _bloc,
            // builder: (context, bloc) => bloc!,
            // create: (context) => _bloc,
            // dispose: (context, bloc) => bloc!.dispose(),
            // child:
            MainWidget(),
        // ),
      ),
    );
  }
}
