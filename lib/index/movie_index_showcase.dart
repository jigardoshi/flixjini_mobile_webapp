// import 'package:flixjini_webapp/authentication/movie_authentication_page.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/utils/signin_alert.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import	'package:streaming_entertainment/detail/movie_detail_page.dart';
// import	'package:streaming_entertainment/common/card/card_list_layout.dart';
import 'package:flixjini_webapp/common/card/card_structure.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
// import	'package:streaming_entertainment/index/movie_index_category_seperator.dart';
import 'package:flixjini_webapp/index/movie_index_showcase_more_option.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 	"dart:async";
import 'dart:convert';
import 'dart:io' show Platform;
import 'package:flixjini_webapp/utils/access_shared_pref.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class MovieIndexShowcase extends StatefulWidget {
  MovieIndexShowcase({
    Key? key,
    this.indexDetail,
    this.setTabAndNavigationHistory,
    this.setFreshness,
    this.showOriginalPosters,
    this.freeFlag,
  }) : super(key: key);

  final indexDetail;
  final setTabAndNavigationHistory;
  final setFreshness;
  final showOriginalPosters;
  final freeFlag;
  @override
  _MovieIndexShowcaseState createState() => new _MovieIndexShowcaseState();
}

class _MovieIndexShowcaseState extends State<MovieIndexShowcase> {
  bool fetchedUserPreference = false,
      fetchedUserQueuePreference = false,
      isFollowing = false;
  String authenticationToken = "", followingText = "FOLLOW";
  var userDetail = {}, userQueue = {}, watchList = {"movies": []};
  List userSavedFilters = [], followingList = [], queueIds = [];
  List followingIds = [];
  List seenIds = <String>[];
  FirebaseAnalytics analytics = FirebaseAnalytics();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  void initState() {
    super.initState();
    authenticationStatus();
    loadSavedFilters();
    getSeenMovieIds();
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
    });
    printIfDebug("LogIn Status on Authentication Page");
    printIfDebug(authenticationToken);
    if (authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      printIfDebug("User is already Logged in -> Obtain User Preferences");
      getPreferences();
    } else {
      printIfDebug("User can't access User Preferences as he is not logged in");
      setState(() {
        fetchedUserQueuePreference = true;
        fetchedUserPreference = true;
      });
    }
  }

  getPreferences() {
    getUserPreference();
    getUserQueuePreference();
  }

  getUserQueuePreference() async {
    String urlAuthentication =
        'https://api.flixjini.com/queue/list.json?jwt_token=' +
            authenticationToken;
    String url = await fetchDefaultParams(urlAuthentication);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        // printIfDebug("User Preference queue");
        // printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userQueue = data;
        getQueueIds(data["movies"]);

        setState(() {
          userQueue = this.userQueue;
          fetchedUserQueuePreference = true;
        });
        printIfDebug("Data Fetched");
        createUserQueue();
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  getQueueIds(data) {
    data.forEach((movie) {
      // printIfDebug("movie ids" + movie.toString());
      queueIds.add(movie["id"]);
    });
  }

  createUserQueue() {
    int size = userQueue["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        var movie = {
          "id": userQueue["movies"][i]["id"],
          "status": true,
        };
        setState(() {
          watchList["movies"]!.add(movie);
        });
      }
    }
    // printIfDebug("Movie Watchlist");
    // printIfDebug(watchList);
    printIfDebug("DOne");
  }

  getUserPreference() async {
    String url =
        'https://api.flixjini.com/queue/get_all_activity.json?jwt_token=' +
            authenticationToken;
    url = await fetchDefaultParams(url);
    var response = await http.get(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);
        // printIfDebug("User Preference");
        // printIfDebug(data["movies"]);
        if (data["movies"] == null) {
          data["movies"] = [];
        }
        this.userDetail = data;
        setState(() {
          userDetail = this.userDetail;
          fetchedUserPreference = true;
        });
        printIfDebug("Data Fetched");
      } else {
        printIfDebug(
            'Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  basedOnUserQueue(movie) {
    int size = watchList["movies"]!.length;
    printIfDebug("Hello");
    printIfDebug("User Queue Function Called");
    printIfDebug(movie["id"]);
    printIfDebug(watchList["movies"]);
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (watchList["movies"]![i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          // flag = 1;
          return watchList["movies"]![i]["status"];
        }
      }
    }
    return false;
  }

  basedonUserPreference(movie, entity) {
    int size = userDetail["movies"].length;
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        if (userDetail["movies"][i]["id"] == movie["id"]) {
          printIfDebug(movie["movieName"]);
          return userDetail["movies"][i][entity];
        }
      }
    }
    return false;
  }

  removeMovieFromWatchList(index, actionUrl) async {
    String url = actionUrl;
    url = await fetchDefaultParams(url);
    printIfDebug(url);

    try {
      var response = await http.post(Uri.parse(url));
      printIfDebug("Response status: ${response.statusCode}");
      printIfDebug("Response body: ${response.body}");
      if (response.statusCode == 200) {
        String jsonString = response.body;
        var jsonResponse = json.decode(jsonString);
        printIfDebug("Remove Movie From Queue");
        printIfDebug(jsonResponse);
        if (jsonResponse["login"] && !jsonResponse["error"]) {
          updateUserQueue(index);
        } else {
          printIfDebug("Could not authorise user / Some error occured");
        }
      } else {
        printIfDebug("Non sucessesful response from server");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (!mounted) return;
  }

  checkAction(movie) {
    int size = watchList["movies"]!.length;
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        if (watchList["movies"]![i]["status"]) {
          return false;
        } else {
          return true;
        }
      }
    }
    return true;
  }

  updateUserQueue(movie) {
    int size = watchList["movies"]!.length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug("wishlist");
    for (int i = 0; i < size; i++) {
      if (watchList["movies"]![i]["id"] == movie["id"]) {
        setState(() {
          watchList["movies"]![i]["status"] =
              watchList["movies"]![i]["status"] ? false : true;
        });
        flag = 1;
        printIfDebug("Change Existing Entry in Queue");
        break;
      }
    }
    if (flag == 0) {
      var entity = {
        "id": movie["id"],
        "status": true,
      };
      setState(() {
        watchList["movies"]!.add(entity);
      });
      printIfDebug("Add New Entry to Queue");
      printIfDebug(watchList);
    }
    printIfDebug(movie["id"]);
  }

  sendRequest(movie, actionUrl, entity) async {
    printIfDebug((authenticationToken).isEmpty);
    if (authenticationToken.isNotEmpty) {
      // if (!(authenticationToken.isEmpty)) {
      bool flag = true;
      if (entity == "add") {
        flag = checkAction(movie);
      }
      if (flag) {
        Map data = {
          'jwt_token': authenticationToken,
          'movie_id': movie["id"],
        };

        var url = actionUrl;
        url = await fetchDefaultParams(url);
        http.post(url, body: data).then((response) {
          printIfDebug("Response status: ${response.statusCode}");
          printIfDebug("Response body: ${response.body}");
          String jsonString = response.body;
          var jsonResponse = json.decode(jsonString);
          printIfDebug(jsonResponse);
          if (response.statusCode == 200) {
            if (jsonResponse["login"] && !jsonResponse["error"]) {
              showToast('Added to the queue', 'long', 'bottom', 1,
                  Constants.appColorLogo);

              if (entity == "add") {
                updateUserQueue(movie);
              } else {
                updateuserDetail(movie, entity);
              }
            } else {
              printIfDebug("Could not authorise user / Some error occured");
            }
          } else {
            printIfDebug("Non sucessesful response from server");
          }
        });
      } else {
        actionUrl = 'https://api.flixjini.com/queue/v2/remove.json?movie_id=' +
            movie["id"] +
            '&jwt_token=' +
            authenticationToken;
        showToast(
            'Removed from queue', 'long', 'bottom', 1, Constants.appColorLogo);
        removeMovieFromWatchList(movie, actionUrl);
      }
    } else {
      printIfDebug(
          "User isn't authorised, Please Login to access User Preferences");
    }
  }

  updateuserDetail(movie, entity) {
    int size = userDetail["movies"].length;
    int flag = 0;
    printIfDebug("Option that is clicked");
    printIfDebug(entity);
    for (int i = 0; i < size; i++) {
      if (userDetail["movies"][i]["id"] == movie["id"]) {
        setState(() {
          userDetail["movies"][i][entity] =
              userDetail["movies"][i][entity] ? false : true;
        });
        if ((entity == "like") || (entity == "dislike")) {
          String opposite = (entity == "like") ? "dislike" : "like";
          if (userDetail["movies"][i][entity] &&
              userDetail["movies"][i][opposite]) {
            setState(() {
              userDetail["movies"][i][opposite] =
                  !userDetail["movies"][i][entity];
            });
          }
        } else {
          printIfDebug(
              "Another option apart from like and dislike have been selected");
        }
        flag = 1;
        printIfDebug("Change Existing Entry");
        break;
      }
    }
    if (flag == 0) {
      var userChoice = {
        "id": movie["id"],
        "like": false,
        "dislike": false,
        "seen": false,
      };
      userChoice[entity] = userChoice[entity] ? false : true;
      setState(() {
        userDetail["movies"].add(userChoice);
      });
      printIfDebug("Add New Entry");
      printIfDebug(userDetail);
    }
    printIfDebug(movie["id"]);
  }

  getSeenMovieIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    seenIds = prefs.getStringList("seenids")!;
    printIfDebug("seenids :" + seenIds.toString());
  }

  Widget movieCardBuilder(BuildContext context, int index, int categoryGroup) {
    // index page movie cards
    if (index <
        widget.indexDetail["showcase"][categoryGroup]["movies"].length) {
      // printIfDebug(widget.indexDetail["showcase"][categoryGroup]["movies"][index]
      //     .toString());
      // printIfDebug('\n\nvalue of switch in index page: ${widget.showOriginalPosters}');
      return Container(
        padding: EdgeInsets.all(true ? 0 : 5),
        child: CardStructure(
          movie: widget.indexDetail["showcase"][categoryGroup]["movies"][index],
          sendRequest: sendRequest,
          basedOnUserQueue: basedOnUserQueue,
          basedonUserPreference: basedonUserPreference,
          authenticationToken: authenticationToken,
          index: index,
          showOriginalPosters: widget.showOriginalPosters,
          pagefor: 0,
          seenIds: seenIds,
        ),
      );
    } else {
      return MovieIndexShowcaseMoreOption(
        categoryGroup: categoryGroup,
        showAllInCategory: showAllInCategory,
      );
    }
  }

  Widget categoryListView(i) {
    int categoryGroup = i;
    return new SizedBox.fromSize(
      size: const Size.fromHeight(300.0),
      child: new ListView.builder(
        physics: BouncingScrollPhysics(),
        itemCount:
            widget.indexDetail["showcase"][categoryGroup]["movies"].length + 1,
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.only(bottom: 5.0),
        itemBuilder: (context, index) => movieCardBuilder(
          context,
          index,
          categoryGroup,
        ),
      ),
    );
  }

  showAllInCategory(int categoryGroup) {
    bool inTheaterKey = false;
    if (widget.indexDetail["showcase"][categoryGroup]["url"]
        .contains("queue")) {
      widget.setTabAndNavigationHistory(3);
    } else if (widget.indexDetail["showcase"][categoryGroup]["url"]
        .contains("now-playing-in-theatres")) {
      inTheaterKey = true;
      String appliedFilterUrl = "https://api.flixjini.com" +
          widget.indexDetail["showcase"][categoryGroup]["url"];
      printIfDebug(
        "\n\nfilter page, sent filter url: " + appliedFilterUrl.toString(),
      );
      Navigator.of(context).push(
        new MaterialPageRoute(
          builder: (BuildContext context) => new MovieFilterPage(
            appliedFilterUrl: appliedFilterUrl,
            inTheaterKey: inTheaterKey,
            showOriginalPosters: widget.showOriginalPosters,
          ),
        ),
      );
    } else if (widget.indexDetail["showcase"][categoryGroup]["url"]
        .contains("seven-days-ago-movies")) {
      widget.setFreshness(widget.indexDetail["showcase"][categoryGroup]["url"]);
      widget.setTabAndNavigationHistory(2);
    } else if (widget.indexDetail["showcase"][categoryGroup]["url"]
        .contains("fourteen-days-ago-movies")) {
      widget.setFreshness(widget.indexDetail["showcase"][categoryGroup]["url"]);
      widget.setTabAndNavigationHistory(2);
    } else if (widget.indexDetail["showcase"][categoryGroup]["url"]
        .contains("one-month-ago-movies")) {
      widget.setFreshness(widget.indexDetail["showcase"][categoryGroup]["url"]);
      widget.setTabAndNavigationHistory(2);
    } else {
      inTheaterKey = false;
      String appliedFilterUrl = "";
      // if (widget.freeFlag != null && widget.freeFlag && false) {
      //   appliedFilterUrl = !widget.indexDetail["showcase"][categoryGroup]["url"]
      //           .toString()
      //           .contains("streamingtypelist=free")
      //       ? "https://api.flixjini.com" +
      //           widget.indexDetail["showcase"][categoryGroup]["url"] +
      //           "&streamingtypelist=free"
      //       : "https://api.flixjini.com" +
      //           widget.indexDetail["showcase"][categoryGroup]["url"];
      // } else {
      appliedFilterUrl = "https://api.flixjini.com" +
          widget.indexDetail["showcase"][categoryGroup]["url"];
      // }
      printIfDebug(
        "\n\nfilter page, sent filter url: " + appliedFilterUrl.toString(),
      );

      Navigator.of(context).push(
        new MaterialPageRoute(
          builder: (BuildContext context) => new MovieFilterPage(
            appliedFilterUrl: appliedFilterUrl,
            inTheaterKey: inTheaterKey,
            showOriginalPosters: widget.showOriginalPosters,
          ),
        ),
      );
    }
  }

  Widget categoryTitle(i) {
    int categoryGroup = i;
    return Container(
      margin: EdgeInsets.only(
        bottom: 14,
      ),
      child: new GestureDetector(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // insertDividerVertical(),
            Expanded(
              // flex: 6,
              child: Container(
                margin: EdgeInsets.only(
                  left: 10,
                ),
                padding: EdgeInsets.only(
                  top: 5,
                  left: 8,
                ),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(
                      width: 4.0,
                      color: Constants.appColorLogo,
                    ),
                  ),
                ),
                child: Text(
                  widget.indexDetail["showcase"][categoryGroup]["title"]
                      .toString(),
                  textAlign: TextAlign.left,
                  style: new TextStyle(
                    letterSpacing: 1,
                    fontWeight: FontWeight.normal,
                    color: Constants.appColorFont,
                    fontSize: 19.0,
                  ),
                  maxLines: 2,
                ),
              ),
            ),
          ],
        ),
        onTap: () {
          _sendAnalyticsEvent(widget.indexDetail["showcase"][categoryGroup]
                  ["title"]
              .toString());
          showAllInCategory(categoryGroup);
        },
      ),
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 8.0,
        left: 10,
        right: 10,
      ),
      child: SizedBox(
        height: 2.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget insertDividerVertical() {
    return new Container(
      padding: const EdgeInsets.only(
        left: 8.0,
        right: 8,
      ),
      child: SizedBox(
        height: 30.0,
        width: 6,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            // height: 100,
            color: Constants.appColorLogo,
          ),
        ),
      ),
    );
  }

  Widget renderCategorySection(BuildContext context, int i) {
    var category;
    if (widget.indexDetail["showcase"][i]["movies"].length < 5) {
      category = new Container();
    } else {
      category = new Container(
        margin: const EdgeInsets.only(
          top: 10,
          bottom: 10.0,
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //homepage each card
            categoryTitle(i),
            categoryListView(i),
            seeAllContainer(i),
            insertDivider(),
          ],
        ),
      );
    }
    return category;
  }

  void loadSavedFilters() async {
    printIfDebug('\n\nsaved filters button is tapped');
    String getFiltersAPIEndpoint =
        "https://api.flixjini.com/queue/get_filters.json?";

    String defaultParamsAddedURL =
        await fetchDefaultParams(getFiltersAPIEndpoint);
    printIfDebug('\n\ndefault params added url: $defaultParamsAddedURL');

    defaultParamsAddedURL += "&";

    String headerAddedURL = constructHeader(defaultParamsAddedURL);
    printIfDebug('\n\nheader added url is: $headerAddedURL');

    headerAddedURL += "&magic=true&country_code=IN&jwt_token=";

    String authToken = await getStringFromSP('authenticationToken');
    headerAddedURL += authToken;

    printIfDebug('\n\nurl before making request is: $headerAddedURL');

    callGetFiltersAPIEndpoint(headerAddedURL);
  }

  void callGetFiltersAPIEndpoint(String url) {
    var resBody;
    List followingListNew = [];

    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        // printIfDebug('\n\ndecoded response: $resBody');
        if (resBody["error"] == false) {
          userSavedFilters = resBody["filters"];
          userSavedFilters.forEach((value) {
            followingListNew.add(value["movie_saved_filter"]["query_string"]);
            followingIds.add(value["movie_saved_filter"]);
          });
          // printIfDebug("saved filter" + userSavedFilters.toString());
          // takeUsersSavedFilters(userSavedFilters);
          setState(() {
            followingList = followingListNew;
          });
        } else {
          // error occurred

        }
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  void callSaveFilterAPIEndpoint(i) async {
    String queryStringValue = "",
        saveFilterAPIEndpoint =
            "https://api.flixjini.com/queue/save_filter.json?";
    // userChosenFilter.forEach((key, value) {
    //   if (value.length <= 0) {
    //     // dont add it to query string
    //   } else {
    //     queryStringValue += "&";
    //     queryStringValue += key + "=";
    //     for (int i = 0; i < value.length; i++) {
    //       String trimmedValue = value[i].trim();
    //       trimmedValue = trimmedValue.toLowerCase();
    //       queryStringValue += trimmedValue;
    //       i == value.length - 1
    //           ? queryStringValue += ""
    //           : queryStringValue += ",";
    //     }
    //   }
    // });
    queryStringValue +=
        widget.indexDetail["showcase"][i]["tag_savefilter"].toString();

    printIfDebug('\n\nquery string value is: $queryStringValue');

    queryStringValue = queryStringValue.replaceAll('&', '%26');
    queryStringValue = queryStringValue.replaceAll('=', '%3D');
    queryStringValue = queryStringValue.replaceAll(',', '%2C');

    saveFilterAPIEndpoint +=
        "query_string=" + queryStringValue; // adding query_string param
    printIfDebug('\n\nquery string added url: $saveFilterAPIEndpoint');

    saveFilterAPIEndpoint += "&name=" +
        widget.indexDetail["showcase"][i]["tag_savefilter_name"]
            .toString(); // adding name param

    String authToken = await getStringFromSP('authenticationToken');
    saveFilterAPIEndpoint += "&jwt_token=" + authToken; // adding auth token

    String urlHavingHeader = "", saveFilterAPIEndpointWithDefaultParams = "";

    urlHavingHeader = constructHeader("");

    saveFilterAPIEndpoint += "&" + urlHavingHeader;

    saveFilterAPIEndpointWithDefaultParams =
        await fetchDefaultParams(saveFilterAPIEndpoint);

    // adding magic and country_code params
    saveFilterAPIEndpointWithDefaultParams += "&magic=true&country_code=IN";

    printIfDebug(
        '\n\nurl before making a network call: $saveFilterAPIEndpointWithDefaultParams');
    finalCallToSaveFilterAPIEndpoint(
        saveFilterAPIEndpointWithDefaultParams.toString(), i);
  }

  void finalCallToSaveFilterAPIEndpoint(String url, int i) async {
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        // printIfDebug('\n\ndecoded response: $resBody');
        //
        printIfDebug(
            '\n\nresbody error runtime type is: ${resBody['error'].runtimeType}');
        if (resBody["error"] == false) {
          // Navigator.of(context).pop();
          printIfDebug('\n\nfollowing tag filter is saved successfully');

          setState(() {
            loadSavedFilters();
          });
          showToast(
              'Following ' +
                  widget.indexDetail["showcase"][i]["tag_savefilter_name"]
                      .toString(),
              'long',
              'bottom',
              1,
              Constants.appColorLogo);
        } else {
          // Navigator.of(context).pop();
          printIfDebug('\n\nfilter is not saved');
          showToast(
              "Couldn't Follow " +
                  widget.indexDetail["showcase"][i]["tag_savefilter_name"]
                      .toString(),
              'long',
              'bottom',
              1,
              Constants.appColorError);
        }
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  bool checkFollowing(int i) {
    // printIfDebug("check for following");

    // printIfDebug(followingList
    //     .contains(widget.indexDetail["showcase"][i]["tag_savefilter"]));
    if (followingList != null &&
        (followingList
            .contains(widget.indexDetail["showcase"][i]["tag_savefilter"]))) {
      followingText = "UNFOLLOW";

      return false;
    } else {
      followingText = "FOLLOW";

      return true;
    }
  }

  bool checkShowFollowing(int i) {
    if (widget.indexDetail["showcase"][i]["tag_savefilter"] == null ||
        widget.indexDetail["showcase"][i]["tag_savefilter_name"] == null)
      return false;
    else
      return true;
  }

  bool checkSeeAll(int i) {
    if (widget.indexDetail["showcase"][i]["url"] == null)
      return false;
    else
      return true;
  }

  int getFilterId(int i) {
    int id = 0;
    followingIds.forEach((value) {
      if (value["query_string"] ==
          widget.indexDetail["showcase"][i]["tag_savefilter"]) {
        id = value["id"];
      }
    });
    // printIfDebug("filter id -=-==-=-=" + id.toString());
    return id;
  }

  Widget seeAllContainer(i) {
    bool showFollowing = checkShowFollowing(i);
    bool isFollowing = checkFollowing(i);
    bool showSeeAll = checkSeeAll(i);
    int filterId = getFilterId(i);
    int categoryGroup = i;
    return Row(
      children: <Widget>[
        showFollowing
            ? GestureDetector(
                child: Container(
                  height: 25,
                  width: 115,
                  margin: EdgeInsets.only(
                    left: 10,
                    // left: isFollowing ? 10 : 0,
                    right: isFollowing ? 10 : 0,
                  ),
                  padding: EdgeInsets.only(
                    top: 2,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: new BorderRadius.circular(25.0),
                    border: Border.all(
                      color: isFollowing
                          ? Constants.appColorLogo
                          : Constants.appColorDivider,
                      width: 1.0,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      followingText,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        color: isFollowing
                            ? Constants.appColorLogo
                            : Constants.appColorDivider,
                        letterSpacing: 1,
                        fontSize: 12,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
                onTap: () {
                  // callSaveFilterAPIEndpoint(i);
                  // Navigator.pop(context, false);
                  if (authenticationToken.isNotEmpty) {
                    if (isFollowing) {
                      _sendAnalyticsFollowEvent(
                          widget.indexDetail["showcase"][categoryGroup]
                              ["title"],
                          isFollowing);
                      callSaveFilterAPIEndpoint(i);
                    } else {
                      _sendAnalyticsFollowEvent(
                          widget.indexDetail["showcase"][categoryGroup]
                              ["title"],
                          isFollowing);

                      deleteSavedFilter(
                        filterId,
                        widget.indexDetail["showcase"][i]
                            ["tag_savefilter_name"],
                        widget.indexDetail["showcase"][i]["tag_savefilter"],
                        i,
                      );
                    }
                  } else {
                    showAlert();
                  }
                },
              )
            : Container(),
        showFollowing ? Spacer() : Container(),
        showSeeAll
            ? GestureDetector(
                child: Container(
                  padding: EdgeInsets.only(
                    right: 15,
                  ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                          left: 10,
                          top: 10,
                          bottom: 10,
                          right: 5,
                        ),
                        child: Text(
                          "SEE ALL ",
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w800,
                            letterSpacing: 1,
                            color: Constants.appColorFont.withOpacity(0.6),
                          ),
                        ),
                      ),
                      Container(
                        height: 20,
                        child: Image.asset(
                          "assests/c_more_black.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  _sendAnalyticsEvent(widget.indexDetail["showcase"]
                          [categoryGroup]["title"]
                      .toString());
                  showAllInCategory(categoryGroup);
                },
              )
            : Container(),
      ],
    );
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: 'index_tag_click',
      parameters: <String, dynamic>{
        'tag': tagName,
      },
    );
    Map<String, String> eventData = {
      'tag': tagName.toString(),
    };
    // await platform.invokeMethod('logAddToCartEvent', eventData);
  }

  Future<void> _sendAnalyticsFollowEvent(tagName, bool follow) async {
    if (!follow) {
      await analytics.logEvent(
        name: 'index_tag_unfollow',
        parameters: <String, dynamic>{
          'tag': tagName,
        },
      );
    } else {
      await analytics.logEvent(
        name: 'index_tag_follow',
        parameters: <String, dynamic>{
          'tag': tagName,
        },
      );
      Map<String, String> eventData = {
        'tag': tagName.toString(),
      };
      // await platform.invokeMethod('logInitiateCheckoutEvent', eventData);
    }
  }

  showAlert() {
    // double screenwidth = MediaQuery.of(context).size.width;

    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return SigninAlertAlert();
      },
    );
  }

  void deleteSavedFilter(
      int filterId, String filterName, String filterQueryString, int i) async {
    String apiEndpointForDeleteFilter =
        "https://api.flixjini.com/queue/delete_filters.json?";
    apiEndpointForDeleteFilter =
        await fetchDefaultParams(apiEndpointForDeleteFilter);
    apiEndpointForDeleteFilter += constructHeader('&');
    apiEndpointForDeleteFilter +=
        "&magic=true&country_code=IN&id=$filterId&jwt_token=";
    String authToken = await getStringFromSP('authenticationToken');
    apiEndpointForDeleteFilter += authToken;
    printIfDebug(
        '\n\ncomplete url before making a request to delete filter api is: $apiEndpointForDeleteFilter');

    callDeleteFilterAPI(apiEndpointForDeleteFilter, filterId, i);
  }

  void callDeleteFilterAPI(String url, int filterId, int i) {
    var resBody;
    try {
      http.get(
        Uri.parse(url),
        headers: {"Accept": "application/json"},
      ).then((response) async {
        resBody = json.decode(response.body);
        // printIfDebug('\n\ndecoded response: $resBody');
        setState(() {
          // listOfWidgetsForSavedFilters = [];
          isFollowing = false;
          loadSavedFilters();
        });
        showToast(
            'Un-Followed ' +
                widget.indexDetail["showcase"][i]["tag_savefilter_name"]
                    .toString(),
            'long',
            'bottom',
            1,
            Constants.appColorLogo);
      });
    } catch (exception) {
      printIfDebug(
          '\n\nexception caught in making http get request at the endpoint: $url');
    }
  }

  Widget variousCategories() {
    int numberOfCategories = widget.indexDetail["showcase"].length;
    return new ListView.builder(
      itemCount: numberOfCategories,
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.all(0.0),
      itemBuilder: renderCategorySection,
      shrinkWrap: true,
      addAutomaticKeepAlives: false,
      physics: NeverScrollableScrollPhysics(),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    // if (fetchedUserPreference && !widget.internetFlag) {
    return variousCategories();
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
