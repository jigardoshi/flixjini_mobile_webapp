import 'package:flixjini_webapp/navigation/movie_routing.dart';
import 'package:flixjini_webapp/profile/appTour_overlay.dart';
import 'package:flixjini_webapp/utils/google_advertising_id.dart';
// import 'package:device_info/device_info.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/index/movie_index_overview.dart';
import 'package:flixjini_webapp/index/movie_index_browse.dart';
import 'package:flixjini_webapp/index/movie_index_showcase.dart';
import 'package:flixjini_webapp/index/movie_index_collapsable_appbar.dart';
import 'package:flixjini_webapp/index/load_data_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
import 'package:flixjini_webapp/feedback/feedback_overlay.dart';
import 'dart:async';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:extended_nested_scroll_view/extended_nested_scroll_view.dart'
    as extended;
import 'dart:io' show Platform;
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import 'package:firebase_messaging/firebase_messaging.dart';

class MovieIndexPage extends StatefulWidget {
  MovieIndexPage({
    Key? key,
    this.indexCatDetail,
    this.indexTopBottomDetail,
    this.setTabAndNavigationHistory,
    this.setFreshness,
    this.showOriginalPosters,
  }) : super(key: key);

  final indexCatDetail;
  final setTabAndNavigationHistory;
  final setFreshness;
  final indexTopBottomDetail;
  final showOriginalPosters;

  @override
  _MovieIndexPageState createState() => new _MovieIndexPageState();
}

class _MovieIndexPageState extends State<MovieIndexPage> {
  bool filterFlag = false,
      fetchedIndexDetail = false,
      appBarCollapsed = false,
      showAppTour = false,
      loadError = true,
      moreTagsloading = false,
      showFeedback = false,
      showFeedbackBanner = false,
      showFreeButton = false,
      freeFlag = false,
      fetchedTopBottomDetail = false,
      fetchedCatDetail = false,
      myAppFlag = false,
      internetFlag = true,
      firstFlag = true;
  String authenticationToken = "", stringDeviceInformation = "";
  String authenticationType = "", fcmToken = "";
  var indexDetailDeepCopy,
      topBottomData,
      movieCategoriesData,
      movieCategoriesData1;
  ScrollController appBarController = new ScrollController();
  final dataKey = new GlobalKey();
  int infiniteScrollCount = 0;
  Set uniqueId = new Set();
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  String _connectionStatus = 'Unknown';
  // DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  int selectedContent = 0, tempSelectedContent = 0;
  List contentTypes = ["All Apps", "Free Apps", "My Apps"],
      contentTypeDesc = [
        "Show Content from all sources. Even those not included in your plan",
        "Show Free Content available across all sources",
        "Show only Content included in your currently active plan"
      ];

  // FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    setOfflineData();
    getFreeFlagSP();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    if (1 == 2) {
      // just mentioning this method for the sake of clearing unused imports and variables
      _testSetCurrentScreen();
    }
    // Future.delayed(const Duration(milliseconds: 25000), () {
    //   setState(() {
    //     showFreeButton = false;
    //   });
    // });
    logUserScreen('Flixjini Home Page', 'MovieIndexPage');
    checkFeedbackFlag();
    appBarController.addListener(_scrollListener);
    retriveDeviceInformationStatus();
  }

  retriveDeviceInformationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      stringDeviceInformation =
          (prefs.getString('stringDeviceInformation') ?? "");
      prefs.setString('stringDeviceInformation', stringDeviceInformation);
    });
    getFCMTokenInSP();
  }

  Future<void> getFcmidGaidAndroidid() async {
    var deviceInformation = {
      'android_id': "",
      'device_model': "",
      'device_maker': "",
      'app_name': "Flixjini",
      'app_version': "",
      'jwt_token': "",
    };
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
    });
    deviceInformation["jwt_token"] = authenticationToken;
    try {
      if (true) {
        var result = await getGoogleAdId();
        if (result != null) {
          // printIfDebug(
          //     "\n\nno need for uuid, as the following gaid is obtained: $result");
          // printIfDebug(result);
          deviceInformation["ga_id"] = result;
        } else {
          String result = await checkIfNewUuidNeeded();
          // printIfDebug("\n\nuuid obtained is: $result");
          deviceInformation["ga_id"] = result;
        }
      } else if (true) {
        String result = await checkIfNewUuidNeeded();
        // printIfDebug("uuid obtained is: $result");
        deviceInformation["ga_id"] = result;
      } else {
        // printIfDebug("Unkown OS");
      }
    } on Exception catch (e) {
      // printIfDebug("Failed to get Android Advertising Id: '${e.message}'.");
      String result = await checkIfNewUuidNeeded();
      // printIfDebug("uuid obtained");
      // printIfDebug(result);
      deviceInformation["ga_id"] = result;
      printIfDebug(e);
    }
    if (true) {
      // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      // deviceInformation["android_id"] = androidInfo.androidId;
      // deviceInformation["device_model"] = androidInfo.model;
      // deviceInformation["device_maker"] = androidInfo.manufacturer;
      // deviceInformation["app_version"] = getVersionCodeForAndroid();
      // deviceInformation["os"] = 'android';
      // deviceInformation["gcm_id"] = fcmToken;
    } else if (true) {
      // IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      // deviceInformation["device_model"] = iosInfo.model;
      // deviceInformation["device_maker"] = "Apple Inc";
      // deviceInformation["app_version"] = iosInfo.utsname.release;
      // deviceInformation["ga_id"] = '';
      // deviceInformation["idfa"] = iosInfo.identifierForVendor;
      // deviceInformation["os"] = 'ios';
      // deviceInformation["gcm_id"] = fcmToken;
    } else {
      printIfDebug("Unkown OS");
    }
    String url = "https://api.flixjini.com/app_track.json";
    // printIfDebug(
    //     "\n\nfor app_track api endpoint, the device info: $deviceInformation");
    bool appTrackSatus = false;
    appTrackSatus =
        checkIfAppTrackRequestsNeedsToBeSent(deviceInformation, prefs);
    bool timeFlag = false;

    String timesp = (prefs.getString('spTime') ?? "");
    var timeNow = new DateTime.now();
    prefs.setString('spTime', timeNow.toString());
    if (timesp.length > 0) {
      var timeSaved = DateTime.parse(timesp);
      if (timeNow.isAfter(timeSaved.add(Duration(days: 3)))) {
        timeFlag = true;
      }
    } else {
      timeFlag = true;
    }
    printIfDebug("app track flag" +
        appTrackSatus.toString() +
        "timeFlag" +
        timeFlag.toString());
    if (appTrackSatus || timeFlag) {
      sendRequestToAppTrack(url, deviceInformation);

      printIfDebug("send_app_track");
    }
  }

  sendRequestToAppTrack(url, deviceInformation) async {
    // printIfDebug('\n\napp track api, url: $url, device info: $deviceInformation');
    try {
      var response = await http.post(url, body: deviceInformation);

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var appTrackResponse = json.decode(responseBody);
        printIfDebug("The response after calling app_track");
        printIfDebug(appTrackResponse);
      } else {
        // printIfDebug('Error getting IP address:\nHttp status ${response.statusCode}');
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }

    if (!mounted) return;
  }

  void getFCMTokenInSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = (prefs.getString('spFCMToken') ?? "");
    setState(() {
      fcmToken = token;
    });

    getFcmidGaidAndroidid();

    // printIfDebug('\n\nstoring in sp, this fcm token: $token');
  }

  checkIfAppTrackRequestsNeedsToBeSent(deviceInformation, prefs) {
    String newDeviceInfo = json.encode(deviceInformation);
    // printIfDebug(newDeviceInfo);
    // printIfDebug(stringDeviceInformation);
    if (newDeviceInfo == stringDeviceInformation) {
      printIfDebug("Perfect Match Don't send apptrack request");
      return false;
    } else {
      printIfDebug("Send apptrack request - Device Information has changed");
      setState(() {
        prefs.setString('stringDeviceInformation', newDeviceInfo);
        stringDeviceInformation = newDeviceInfo;
      });
      return true;
    }
  }

  setOfflineData() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? indexOff = prefs.getString("indexOfflineData");

      String? indexTopOff = prefs.getString("indexOfflineTopData");
      // printIfDebug("index off" + indexOff.toString());
      if (indexOff != null && indexOff.length > 0) {
        var data = json.decode(indexOff);
        printIfDebug("  index off" + data.toString());
        splitData(data);
      }
      if (indexTopOff != null && indexTopOff.length > 0) {
        var data = json.decode(indexTopOff);
        printIfDebug("  index off" + data.toString());
        setState(() {
          fetchedTopBottomDetail = true;
          topBottomData = data;
        });
      }
    } catch (e) {
      printIfDebug(e);
    }
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        setState(() {
          internetFlag = true;
        });
        if (!firstFlag) {
          getMovieDetails();
          printIfDebug("  wifi net");
        } else {
          setState(() {
            firstFlag = false;
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState(() {
          internetFlag = true;
        });
        if (firstFlag) {
          getMovieDetails();
          printIfDebug("  mobile net");
        } else {
          setState(() {
            firstFlag = false;
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        printIfDebug("  none net");
        setState(() {
          internetFlag = false;
        });
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        printIfDebug("  failed net");
        break;
    }
  }

  checkFeedbackFlag() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? feedbackFlag = prefs.getString("feedback");
    int? count = prefs.getInt("appopencount");
    // printIfDebug("  check////////////" +
    //     feedbackFlag +
    //     "count" +
    //     count.toString());
    if (count != null && count >= 3 && count <= 17) {
      // printIfDebug("feedback called");

      if (feedbackFlag == null || feedbackFlag == "notsubmitted") {
        callFeedbackTimer();
      } else if (feedbackFlag != null && feedbackFlag == "later") {
        callFeedbackTimer();

        setFeedbackFlag(0);
      }
    }
  }

  setFeedbackFlag(int i) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (i == 0) {
      prefs.setString("feedback", "submitted");
    } else {
      prefs.setString("feedback", "later");
    }
  }

  callFeedbackTimer() {
    setState(() {
      showFeedbackBanner = true;
    });
  }

  _scrollListener() {
    if (appBarController.offset >= appBarController.position.maxScrollExtent &&
        !appBarController.position.outOfRange) {
      setState(() {
        moreTagsloading = true;
      });
      // printIfDebug("check:" + widget.indexCatDetail.toString());
      if (widget.indexCatDetail.toString().isNotEmpty) {
        changeInfiniteURL();
      }
      printIfDebug("infinite scroll");
      // setState(() {
      //   message = "reach the bottom";
      // });
    }
    if (appBarController.offset <= appBarController.position.minScrollExtent &&
        !appBarController.position.outOfRange) {
      // printIfDebug("top sucesss//////////");
      // _firebaseMessaging.getToken().then((token) {
      //   printIfDebug("token/////////////////////" + token);
      // });

      // setState(() {
      //   message = "reach the top";
      // });
    }
  }

  initConnectivity() async {
    ConnectivityResult? result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } catch (e) {
      printIfDebug(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
    if (result == ConnectivityResult.none) {
      setState(() {
        internetFlag = false;
      });
    } else {
      setState(() {
        internetFlag = true;
      });
    }

    // return _updateConnectionStatus(result);
  }

  Future<void> _testSetCurrentScreen() async {
    try {
      if (true) {
        FirebaseAnalytics analytics = FirebaseAnalytics();

        await analytics.setCurrentScreen(
          screenName: 'Flixjini Home Page',
          screenClassOverride: 'MovieIndexPage',
        );
      }
    } catch (e) {}
  }

  void checkData() {
    printIfDebug("Do we have indexData or Not");
    if (widget.indexCatDetail == null) {
      getMovieDetailsNew1();
    } else {
      printIfDebug("Yes");
      splitData(widget.indexCatDetail);
    }
    if (widget.indexTopBottomDetail == null) {
      getMovieDetailsNew();
    } else {
      setState(() {
        this.topBottomData = widget.indexTopBottomDetail;
        fetchedTopBottomDetail = true;
      });
    }
  }

  authenticationStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      authenticationToken = (prefs.getString('authenticationToken') ?? "");
      authenticationType = (prefs.getString('authenticationType') ?? "");
      prefs.setString('authenticationToken', authenticationToken);
      prefs.setString('authenticationType', authenticationType);
    });

    // printIfDebug("Index Page Authentication Report");
    // printIfDebug(authenticationToken);
    // printIfDebug("type:  ");
    // printIfDebug(authenticationType);

    checkData();
  }

  getMovieDetails() async {
    getMovieDetailsNew();
    getMovieDetailsNew1();
  }

  getMovieDetailsNew() async {
    String rootUrl = "";
    // rootUrl = freeFlag
    //     ? 'https://api.flixjini.com/entertainment/fast_top.json?free_only=true&'
    //     : 'https://api.flixjini.com/entertainment/fast_top.json?';
    // rootUrl = myAppFlag ? rootUrl + "only_profile_sources=true&" : rootUrl;
    // rootUrl = partnerFlag ? rootUrl + "partner_apps=true&" : rootUrl;

    rootUrl = "https://api.flixjini.com/entertainment/fast_top.json?" +
        getParamsForContent();

    if (authenticationToken.isNotEmpty) {
      rootUrl = rootUrl + 'jwt_token=' + authenticationToken + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    // printIfDebug("URL after default params " + url.toString());

    try {
      var response = await http.get(Uri.parse(url));

      // printIfDebug("The Index Status Code");
      // printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        // printIfDebug(responseBody);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("indexOfflineTopData", responseBody);
        var data = json.decode(responseBody);
        // printIfDebug("Value of Movie Id");
        // printIfDebug(data["carousel"]);
        this.topBottomData = data;

        setState(() {
          loadError = false;
        });
      } else {
        this.topBottomData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
        printIfDebug(response.statusCode);
      }
    } catch (SocketException) {
      printIfDebug(" " + SocketException.toString());
      setState(() {
        loadError = true;
      });
    }

    if (!mounted) return;
    if (!loadError) {
      setState(() {
        topBottomData = this.topBottomData;
        fetchedTopBottomDetail = true;
      });
      printIfDebug("Data Fetched");
    } else {
      // Future.delayed(const Duration(milliseconds: 2000), () {
      //   showAlertResponsive();
      // });
    }
  }

  getMovieDetailsNew1() async {
    String rootUrl = "";
    // rootUrl = freeFlag
    //     ? "https://api.flixjini.com/entertainment/only_film_strip.json?free_only=true&"
    //     : "https://api.flixjini.com/entertainment/only_film_strip.json?";
    // rootUrl = myAppFlag ? rootUrl + "only_profile_sources=true&" : rootUrl;
    // rootUrl = partnerFlag ? rootUrl + "partner_apps=true&" : rootUrl;

    rootUrl = "https://api.flixjini.com/entertainment/only_film_strip.json?" +
        getParamsForContent();

    if (authenticationToken.isNotEmpty) {
      rootUrl = rootUrl + 'jwt_token=' + authenticationToken + '&';
    }
    String url = constructHeader(rootUrl);
    url = await fetchDefaultParams(url);
    // printIfDebug("URL after default params " + url.toString());

    try {
      var response = await http.get(Uri.parse(url));

      // printIfDebug("The Index Status Code");
      // printIfDebug(response.statusCode);
      if (response.statusCode == 200) {
        String responseBody = response.body;
        // printIfDebug(responseBody);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("indexOfflineData", responseBody);

        var data = json.decode(responseBody);
        splitData(data);

        // this.movieCategoriesData = data;
        setState(() {
          loadError = false;
        });
      } else {
        this.movieCategoriesData["error"] =
            'Error getting IP address:\nHttp status ${response.statusCode}';
        printIfDebug(response.statusCode);
      }
    } catch (SocketException) {
      printIfDebug(" " + SocketException.toString());
      setState(() {
        loadError = true;
      });
    }

    if (!mounted) return;
    if (!loadError) {
      setState(() {
        movieCategoriesData = this.movieCategoriesData;
        fetchedCatDetail = true;
      });
      printIfDebug("Data Fetched");
    } else {
      // Future.delayed(const Duration(milliseconds: 2000), () {
      //   showAlertResponsive();
      // });
    }
  }

  splitData(var data) {
    var data1 = {"showcase": []};
    data1["showcase"]!.add((data["showcase"][0]));
    data1["showcase"]!.add((data["showcase"][1]));
    data1["showcase"]!.add((data["showcase"][2]));

    data["showcase"].removeAt(0);
    data["showcase"].removeAt(0);
    data["showcase"].removeAt(0);

    // var infiniteData = new List.from(data1)..addAll(data["showcase"][0]);
    setState(() {
      movieCategoriesData1 = data1;
      movieCategoriesData = data;
      fetchedCatDetail = true;
    });
    // Future.delayed(const Duration(milliseconds: 2500), () {
    //   setState(() {
    //     movieCategoriesData = data;
    //   });
    // });
  }

  showAlertResponsive() {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return LoadDataAlert(
          getMovieDetails: getMovieDetails,
        );
      },
    );
  }

  Widget movieIndexSearchBox() {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          "Collapsing Toolbar",
          style: TextStyle(
            color: Constants.appColorL1,
            fontSize: 16.0,
          ),
        ),
        background: Image.network(
          "https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget cushionBetweenHeroAppbarAndBody() {
    return new Container(padding: const EdgeInsets.only(top: 10.0));
  }

  Widget feedbackPoster() {
    double screenwidth = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.only(
        top: 10,
        left: 10,
        right: 10,
        bottom: 5,
      ),
      width: screenwidth,
      color: Constants.appColorL2,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  showAppTour = true;
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 10,
                    ),
                    child: Text(
                      "Take a tour of all the\n features",
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 12,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    child: Image.asset(
                      "assests/app_tour.png",
                      height: 70,
                      width: 70,
                    ),
                  ),
                  Container(
                    decoration: new BoxDecoration(
                      color: Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 5,
                      bottom: 5,
                    ),
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 10,
                    ),
                    child: Text(
                      'APP TOUR',
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontSize: 11,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 120,
            width: 1,
            color: Constants.appColorL1,
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  showFeedback = true;
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: 10,
                      left: 10,
                      right: 10,
                    ),
                    child: Text(
                      "Let us know what you think of the app",
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    child: Image.asset(
                      "assests/feedback_banner.png",
                      height: 70,
                      width: 70,
                    ),
                  ),
                  Container(
                    decoration: new BoxDecoration(
                      color: Constants.appColorLogo,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 5,
                      bottom: 5,
                    ),
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 10,
                    ),
                    child: Text(
                      'FEEDBACK',
                      style: TextStyle(
                        color: Constants.appColorL1,
                        fontSize: 11,
                        letterSpacing: 1,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget discoverBanner(screenWidth) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => MovieRouting(
              defaultTab: 2,
            ),
          ),
        );
      },
      child: Container(
        height: screenWidth / 5,
        margin: EdgeInsets.only(left: 5, right: 5, bottom: 20, top: 25),
        width: screenWidth,
        color: Constants.appColorL1,
        child: Center(
          child: ClipRRect(
            borderRadius: new BorderRadius.circular(5.0),
            child: FlareActor(
              "assests/genie.flr",
              color: Constants.appColorL2,
              alignment: Alignment.center,
              fit: BoxFit.contain,
              animation: "genie",
            ),
            // child: Image.asset("assests/disc_banner.jpg"),
          ),
        ),
      ),
    );
  }

  Widget offlineContainer(screenWidth) {
    return !internetFlag
        ? Container(
            // height: 60,
            margin: EdgeInsets.only(
              left: 10,
              right: 10,
              bottom: 20,
              top: 10,
            ),
            padding: EdgeInsets.all(20),
            width: screenWidth,
            color: Constants.appColorL2,
            child: Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assests/no_internet.png",
                        height: 15,
                        width: 15,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 5,
                        ),
                        child: Text(
                          "You are currently offline",
                          style: TextStyle(
                            color: Constants.appColorDivider,
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 10,
                    ),
                    child: Text(
                      "You are viewing offline content. Please connect to internet to discover latest and updated content",
                      style: TextStyle(
                        color: Constants.appColorDivider,
                        fontSize: 10,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          )
        : Container();
  }

  Widget displayMovieDetails() {
    double screenWidth = MediaQuery.of(context).size.width;

    return new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // discoverBanner(screenWidth),

        topBottomData != null && internetFlag
            ? MovieIndexOverview(
                indexDetail: topBottomData,
                showOriginalPosters: widget.showOriginalPosters,
              )
            : Container(),
        cushionBetweenHeroAppbarAndBody(),
        // edit streaming sources and genre sections
        offlineContainer(screenWidth),
        showFeedbackBanner && internetFlag ? feedbackPoster() : Container(),

        movieCategoriesData1 != null
            ? MovieIndexShowcase(
                indexDetail: movieCategoriesData1,
                setTabAndNavigationHistory: widget.setTabAndNavigationHistory,
                setFreshness: widget.setFreshness,
                showOriginalPosters: widget.showOriginalPosters,
                freeFlag: freeFlag,
              )
            : Container(),
        topBottomData != null
            ? MovieIndexBrowse(
                indexDetail: topBottomData,
                showOriginalPosters: widget.showOriginalPosters,
                topic: "genre",
              )
            : Container(),
        movieCategoriesData != null
            ? MovieIndexShowcase(
                indexDetail: movieCategoriesData,
                setTabAndNavigationHistory: widget.setTabAndNavigationHistory,
                setFreshness: widget.setFreshness,
                showOriginalPosters: widget.showOriginalPosters,
                freeFlag: freeFlag,
              )
            : Container(),
        infiniteScroll(),
      ],
    );
  }

  Widget infiniteScroll() {
    return Container(
      padding: EdgeInsets.only(
        bottom: 20,
      ),
      child: !moreTagsloading
          ? Container()
          : CircularProgressIndicator(
              backgroundColor: Constants.appColorLogo,
              valueColor:
                  new AlwaysStoppedAnimation<Color>(Constants.appColorLogo),
            ),
    );
  }

  changeInfiniteURL() {
    printIfDebug("Infinite scroll count");
    printIfDebug(infiniteScrollCount);

    // String url = checkTypeofAppliedFilterUrl();
    // printIfDebug(url);
    String appliedUrl = "https://api.flixjini.com/entertainment/moretags.json?";
    // appliedUrl = freeFlag ? appliedUrl + "free_only=true&" : appliedUrl;
    // appliedUrl =
    //     myAppFlag ? appliedUrl + "only_profile_sources=true&" : appliedUrl;
    // appliedUrl = partnerFlag ? appliedUrl + "partner_apps=true&" : appliedUrl;

    appliedUrl = "https://api.flixjini.com/entertainment/moretags.json?" +
        getParamsForContent();

    if (authenticationToken.isEmpty) {
      appliedUrl = appliedUrl + "load_i=";
    } else {
      appliedUrl = appliedUrl + "jwt_token=" + authenticationToken + "&load_i=";
    }

    appliedUrl += infiniteScrollCount.toString();
    infiniteScrollCount += 1;
    String tagandid = getUsedTags();
    var tagidcombined = tagandid.split("#");
    appliedUrl += tagidcombined[0];
    appliedUrl += tagidcombined[1];
    // printIfDebug("the applied infinite url is: ");
    // printIfDebug(appliedUrl);
    getInfiniteScroll(appliedUrl);
  }

  String getUsedTags() {
    String usedTags = "&used_tags=";
    String movieIds = "&current_ids=";
    int k = 0;
    for (int i = 0; i < movieCategoriesData["showcase"].length; i++) {
      // printIfDebug(indexDetailDeepCopy["showcase"][i]["tag_name"]);
      if (movieCategoriesData["showcase"][i]["tag_name"] != null) {
        if (k == 0)
          usedTags += movieCategoriesData["showcase"][i]["tag_name"];
        else
          usedTags += "," + movieCategoriesData["showcase"][i]["tag_name"];

        k++;
      }

      for (int j = 0; j < 5; j++) {
        uniqueId.add(movieCategoriesData["showcase"][i]["movies"][j]["id"]);
      }
    }
    String uniqueIds = uniqueId.toString().replaceAll("{", "");
    uniqueIds = uniqueIds.toString().replaceAll("}", "");
    uniqueIds = uniqueIds.toString().replaceAll(" ", "");
    // printIfDebug("Unique ids are:");
    // printIfDebug(uniqueIds);
    movieIds += uniqueIds;
    return usedTags + "#" + movieIds;
  }

  getInfiniteScroll(String urlWithSearchParameter) async {
    urlWithSearchParameter += "&";
    String url = constructHeader(urlWithSearchParameter);
    url = await fetchDefaultParams(url);
    // printIfDebug("Infinite scroll Apply");
    // printIfDebug(url);
    try {
      var response = await http.post(Uri.parse(url));

      if (response.statusCode == 200) {
        String responseBody = response.body;
        var data = json.decode(responseBody);

        printIfDebug("More Search Results Obtained");
        if (data["showcase"].length > 0) {
          var infiniteData = new List.from(this.movieCategoriesData["showcase"])
            ..addAll(data["showcase"]);
          setState(() {
            this.movieCategoriesData["showcase"] = infiniteData;
            moreTagsloading = false;
          });
          // printIfDebug(data["showcase"]);
          printIfDebug("More tags Fetched, New Length");
          printIfDebug(this.movieCategoriesData["showcase"].length.toString());
        } else {
          printIfDebug("Reached End of Search Results");
          // changeSearchResultsStatus(true);
        }
      } else if (response.statusCode == 500) {
        printIfDebug("No More Movies");
        // changeSearchResultsStatus(true);
      } else {
        // this.indexDetailDeepCopy =
        //     'Error getting IP address:\nHttp status ${response.statusCode}';
      }
    } catch (exception) {
      // this.indexDetailDeepCopy = 'Failed getting IP address';
    }
    if (!mounted) return;
  }

  Widget displayLoader() {
    return new Center(
      child: getProgressBarWithLoading(),
      // child: ShimmerIndex(),
    );
  }

  Widget freeSheet() {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.width;
    List<Widget> typeListWidget = [];
    for (int i = 0; i < contentTypes.length; i++) {
      Widget temp = GestureDetector(
        child: Container(
          padding: EdgeInsets.only(
            // left: 15,
            // right: 15,
            top: 10,
            bottom: 10,
          ),
          width: screenWidth / 2,
          margin: EdgeInsets.only(
            top: 10,
            bottom: 15,
          ),
          // height: 25,
          // width: 100,
          decoration: new BoxDecoration(
            border: new Border.all(
              color: selectedContent == i
                  ? Constants.appColorLogo
                  : Constants.appColorL3,
            ),
            borderRadius: new BorderRadius.circular(30.0),
            color: selectedContent == i
                ? Constants.appColorLogo
                : Constants.appColorL3,
          ),
          child: Center(
            child: Text(
              contentTypes[i],
              style: TextStyle(
                fontSize: 11,
                color: selectedContent == i
                    ? Constants.appColorL1
                    : Constants.appColorFont,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        onTap: () {
          switch (i) {
            case 0:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(false);
              setFreeFlagSP(false);
              break;
            case 1:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(false);
              setFreeFlagSP(true);
              break;
            case 2:
              setState(() {
                selectedContent = i;
              });
              setMyAppsFlagSP(true);
              setFreeFlagSP(false);
              break;
            default:
          }
        },
      );
      typeListWidget.add(temp);
    }
    return Container(
      width: screenWidth / 1.2,
      child: GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Constants.appColorL3,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      top: 20,
                      bottom: 10,
                    ),
                    child: Text(
                      "Choose Content View",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Constants.appColorFont,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 5,
                      bottom: 20,
                      left: 20,
                      right: 20,
                    ),
                    child: Text(
                      // "Choose what content is displayed below. My Plan will display content only from your currently active plan, PlayboxTV will display content from curated sources.",
                      contentTypeDesc[selectedContent],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Constants.appColorFont.withOpacity(0.75),
                        fontWeight: FontWeight.normal,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 20),

              decoration: new BoxDecoration(
                color: Constants.appColorL2,
              ),
              width: screenWidth / 1.2,

              // height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: typeListWidget,
              ),
            ),
            Container(
              color: Constants.appColorL3,
              width: screenWidth / 1.2,
              padding: EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.only(
                        // left: 15,
                        // right: 15,
                        top: 10,
                        bottom: 10,
                      ),
                      width: screenWidth / 2,
                      margin: EdgeInsets.only(
                        // top: 10,
                        bottom: 20,
                      ),
                      // height: 25,
                      // width: 100,
                      decoration: new BoxDecoration(
                        border: new Border.all(color: Constants.appColorLogo),
                        borderRadius: new BorderRadius.circular(30.0),
                        color: Constants.appColorLogo,
                      ),
                      child: Center(
                        child: Text(
                          "Apply",
                          style: TextStyle(
                            fontSize: 11,
                            color: Constants.appColorL1,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        fetchedCatDetail = false;
                        tempSelectedContent = selectedContent;
                      });
                      switch (selectedContent) {
                        case 0:
                          setMyAppsFlagSP(false);
                          setFreeFlagSP(false);
                          break;
                        case 1:
                          setMyAppsFlagSP(false);
                          setFreeFlagSP(true);
                          break;
                        case 2:
                          setMyAppsFlagSP(true);
                          setFreeFlagSP(false);
                          break;
                        default:
                      }
                      showSheet();
                      getMovieDetails();
                    },
                  ),
                  // GestureDetector(
                  //   child: Container(
                  //     padding: EdgeInsets.only(
                  //       // left: 15,
                  //       // right: 15,
                  //       top: 10,
                  //       bottom: 10,
                  //     ),
                  //     width: screenWidth / 2,
                  //     margin: EdgeInsets.only(
                  //       top: 10,
                  //       bottom: 15,
                  //     ),
                  //     // height: 25,
                  //     // width: 100,
                  //     decoration: new BoxDecoration(
                  //       border: new Border.all(
                  //         color: Constants.appColorL3,
                  //       ),
                  //       borderRadius: new BorderRadius.circular(30.0),
                  //       color: Constants.appColorL3,
                  //     ),
                  //     child: Center(
                  //       child: Text(
                  //         "CANCEL",
                  //         style: TextStyle(
                  //           fontSize: 11,
                  //           color: Constants.appColorFont,
                  //           fontWeight: FontWeight.bold,
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  //   onTap: () {

                  //     showSheet();
                  //   },
                  // ),
                ],
              ),
            ),
          ],
        ),
        onTap: () {
          // showSheet();
        },
      ),
    );
  }

  getFreeFlagSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool flag1 = prefs.getBool('freeflag') ?? false;
    bool flag3 = prefs.getBool('myappsflag') ?? false;
    // bool flag4 = prefs.getBool('partnerflag') ?? true;

    int typeNo = flag1 ? 1 : (flag3 ? 2 : 0);

    setState(() {
      selectedContent = typeNo;
      tempSelectedContent = typeNo;
    });
    checkInternet();
    authenticationStatus();
  }

  checkInternet() {
    // _connectivity.checkConnectivity();
  }
  setFreeFlagSP(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('freeflag', flag);
  }

  setMyAppsFlagSP(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('myappsflag', flag);
  }

  setFreeSliderFlag(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setBool('freesliderflag', flag);
  }

  Widget homeUI() {
    double screenwidth = MediaQuery.of(context).size.width;
    // double screenHeight = MediaQuery.of(context).size.height;

    return
        // extended .NestedScrollViewRefreshIndicator(
        // displacement: 30,
        // onRefresh: () async {
        //   showToast('Refreshing...', 'short', 'bottom', 1, Constants.appColorLogo);
        //   if (this.mounted) {
        //     authenticationStatus();
        //   }
        // },
        // color: Constants.appColorL1,
        // backgroundColor: Constants.appColorLogo,
        // child:
        SingleChildScrollView(
      controller: appBarController,
      key: dataKey,
      child: Column(
        children: <Widget>[
          new MovieIndexCollapsableAppbar(
            indexDetail: topBottomData,
            authenticationToken: authenticationToken,
            showOriginalPosters: widget.showOriginalPosters,
            showSheet: showSheet,
            contentText: contentTypes[tempSelectedContent],
          ),
          new Container(
            //  home page theme
            color: Constants.appColorL1,
            width: screenwidth,

            child: new SingleChildScrollView(
              child: displayMovieDetails(),
            ),
          ),
        ],
      ),
      // ),
    );
  }

  Widget checkDataReady() {
    if (fetchedCatDetail || fetchedTopBottomDetail) {
      return homeUI();
    } else {
      return displayLoader();
    }
  }

  Widget showFeedbackOverlay() {
    return FeedbackOverlay(
      changeFeedback: changeFeedback,
      authenticationToken: authenticationToken,
    );
  }

  changeFeedback(bool flag) {
    setState(() {
      showFeedback = flag;
    });
  }

  changeAppTourOverlay() {
    setState(() {
      showAppTour = false;
    });
  }

  String getParamsForContent() {
    switch (tempSelectedContent) {
      case 0:
        return "";
        break;
      case 1:
        return "free_only=true&";
        break;
      case 2:
        return "only_profile_sources=true&";
        break;
      default:
        return "";
    }
  }

  Widget showAppTourOverlay() {
    return AppTourOverlay(
      changeAppTourOverlay: changeAppTourOverlay,
    );
  }

  showSheet() {
    if (showFreeButton) {
      setFreeSliderFlag(false);
      setState(() {
        showFreeButton = false;
      });
    } else {
      setFreeSliderFlag(true);

      setState(() {
        showFreeButton = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    double screenWidth = MediaQuery.of(context).size.width;

    double screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: <Widget>[
        Scaffold(
          //  theme home
          backgroundColor: Constants.appColorL1,
          body: checkDataReady(),
        ),
        showFreeButton
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    selectedContent = tempSelectedContent;
                  });
                  showSheet();
                },
                child: Container(
                  color: Constants.appColorL1.withOpacity(0.75),
                  height: screenHeight,
                  width: screenWidth,
                ),
              )
            : Container(),
        showFreeButton
            ? Container(
                alignment: Alignment.center,
                child: freeSheet(),
              )
            : Container(),
        showFeedback
            ? Container(
                height: screenHeight,
                width: screenWidth,
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(10.0),
                    topRight: const Radius.circular(10.0),
                  ),
                  color: Constants.appColorL1.withOpacity(
                    0.9,
                  ),
                ),
              )
            : Container(),
        showFeedback ? showFeedbackOverlay() : Container(),
        showAppTour ? showAppTourOverlay() : Container(),
      ],
    );
  }
}
