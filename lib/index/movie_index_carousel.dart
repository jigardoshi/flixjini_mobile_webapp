// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
// import	'package:movies_app/movie_detail_poster.dart';
// import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flixjini_webapp/detail/movie_detail_page.dart';
// import 'package:flixjini_webapp/common/arc_clipper.dart';
// import  'package:cached_network_image/cached_network_image.dart';
// import 	'package:streaming_entertainment/common/default_api_params.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import "dart:async";
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class MovieIndexCarousel extends StatefulWidget {
  MovieIndexCarousel({
    Key? key,
    this.indexDetail,
    this.authenticationToken,
    this.showSheet,
    this.contentText,
    this.showOriginalPosters,
  }) : super(key: key);

  final indexDetail;
  final authenticationToken;
  final showOriginalPosters;
  final showSheet;
  final contentText;

  @override
  _MovieIndexCarouselState createState() => new _MovieIndexCarouselState();
}

class _MovieIndexCarouselState extends State<MovieIndexCarousel> {
  FirebaseAnalytics analytics = FirebaseAnalytics();

  int bannerPosition = 0;

  void initState() {
    super.initState();
  }

  carouselEffect() async {
    if (!mounted)
      return;
    else {
      const oneSec = const Duration(seconds: 5);
      new Timer.periodic(oneSec, (Timer t) {
        int numberOfBanners =
            widget.indexDetail["carousel"]["bannerImages"].length;
        if (bannerPosition < numberOfBanners - 1) {
          setState(() {
            bannerPosition = bannerPosition + 1;
          });
        } else {
          //print("Resetting Banner Position");
          setState(() {
            bannerPosition = 0;
          });
        }
      });
    }
  }

  Future<void> _sendAnalyticsEvent(tagName) async {
    await analytics.logEvent(
      name: 'index_carousal_click',
      parameters: <String, dynamic>{
        'carousal_movie': tagName,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // print('\n\nhey there! i am inside build method of ${this.runtimeType}');
    var screenWidth = MediaQuery.of(context).size.width;
    var screenheight = MediaQuery.of(context).size.height;
    return Container(
      // carousal color home page
      color: Constants.appColorL1,
      // child:
      // ClipPath(
      // clipper: ArcClipper(),
      child: new Container(
        width: screenWidth,
        height: screenheight * 0.6,
        decoration: new BoxDecoration(
          color: Constants.appColorL2,
        ),
        child: Stack(
          children: [
            new Swiper(
              itemBuilder: (BuildContext context, int index) {
                return new GestureDetector(
                  onTap: () {
                    if (widget.indexDetail != null) {
                      if (widget.indexDetail["carousel"]["bannerImages"][index]
                              ["link"]
                          .toString()
                          .contains("filter")) {
                        String filterLink = widget.indexDetail["carousel"]
                                ["bannerImages"][index]["link"]
                            .toString()
                            .replaceAll("www.", "api.");
                        filterLink = filterLink.replaceAll(".com", ".in");
                        filterLink =
                            filterLink.replaceAll("/filter", "/entertainment");
                        filterLink += '.json';
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context) => MovieFilterPage(
                              appliedFilterUrl: filterLink,
                              showOriginalPosters: "true",
                              inTheaterKey: false,
                            ),
                          ),
                        );
                      } else {
                        String tagStrip = widget.indexDetail["carousel"]
                                ["bannerImages"][index]["link"]
                            .toString();
                        tagStrip = tagStrip.replaceAll(".json", "");
                        tagStrip =
                            tagStrip.replaceAll("/entertainment/movie/", "");
                        tagStrip =
                            tagStrip.replaceAll("/entertainment/tvshow/", "");

                        //print(tagStrip);
                        _sendAnalyticsEvent(tagStrip);
                        String urlName = "";
                        if (widget.authenticationToken.isEmpty) {
                          urlName = "https://api.flixjini.com" +
                              widget.indexDetail["carousel"]["bannerImages"]
                                  [index]["link"];
                        } else {
                          urlName = "https://api.flixjini.com" +
                              widget.indexDetail["carousel"]["bannerImages"]
                                  [index]["link"] +
                              "?" +
                              "jwt_token=" +
                              widget.authenticationToken +
                              "&";
                        }
                        //print("From Index Carousal - Go to Movie Detail Page");
                        //print(urlName);
                        String urlPhase = widget.indexDetail["carousel"]
                            ["bannerImages"][index]["link"];
                        var urlInformation = {
                          "urlPhase": urlPhase,
                          "fullUrl": urlName,
                        };
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new MovieDetailPage(
                                  urlInformation: urlInformation,
                                  showOriginalPosters:
                                      widget.showOriginalPosters,
                                )));
                      }
                    }
                  },
                  child: widget.indexDetail != null
                      ? new Container(
                          color: Constants.appColorL2,
                          child: Image.network(
                            widget.indexDetail["carousel"]["bannerImages"]
                                [index]["posterPhoto"],
                            fit: BoxFit.cover,
                          ),
                          //     Image(
                          //   image: NetworkImage(
                          //     widget.indexDetail["carousel"]["bannerImages"]
                          //         [index]["posterPhoto"],
                          //     // useDiskCache: false,
                          //   ),
                          //   fit: BoxFit.cover,
                          //   gaplessPlayback: true,
                          // ),
                        )
                      : Container(
                          decoration:
                              new BoxDecoration(color: Constants.appColorL2),
                        ),
                );
              },
              itemCount: widget.indexDetail != null
                  ? (widget.indexDetail["carousel"]["bannerImages"]).length
                  : 1,
              viewportFraction: 1.0,
              autoplay: true,
              autoplayDelay: 4000,
              autoplayDisableOnInteraction: false,
            ),
            SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      widget.showSheet();
                    },
                    child: Container(
                      // height: 100,
                      // width: 100,
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 5,
                        left: 15,
                        right: 15,
                      ),
                      margin: EdgeInsets.only(
                        right: 10,
                        top: 10,
                      ),
                      decoration: BoxDecoration(
                          color: Constants.appColorL1.withOpacity(0.5),
                          border: Border.all(
                            color: Constants.appColorFont.withOpacity(0.6),
                          ),
                          borderRadius: BorderRadius.circular(30)),

                      child: Row(
                        children: [
                          Text(
                            widget.contentText,
                            style: TextStyle(
                              color: Constants.appColorFont,
                              fontSize: 12,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              left: 5,
                            ),
                            child: Image.asset(
                              "assests/drop_down_arrow.png",
                              height: 10,
                              width: 10,
                              color: Constants.appColorLogo,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      // ),
    );
  }
}
