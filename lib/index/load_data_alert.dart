import 'package:flutter/material.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class LoadDataAlert extends StatefulWidget {
  LoadDataAlert({
    this.getMovieDetails,
  });
  final getMovieDetails;

  @override
  LoadDataAlertState createState() => LoadDataAlertState();
}

class LoadDataAlertState extends State<LoadDataAlert> {
  bool checkBoxValue = false, progressFlag = false, cancelFlag = false;

  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;

    return AlertDialog(
      backgroundColor: Constants.appColorL2,
      content: Container(
        height: screenwidth / 3,
        child: Column(
          children: <Widget>[
            Text(
              "Couldn't load data check your network connection and Refresh",
              style: TextStyle(
                color: Constants.appColorFont,
                fontSize: 15,
                height: 1.3,
              ),
              textAlign: TextAlign.center,
            ),
            Spacer(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      color:
                          progressFlag ? Constants.appColorLogo : Constants.appColorL2,
                      borderRadius: new BorderRadius.circular(30.0),
                    ),
                    child: Text(
                      'Refresh',
                      style: TextStyle(
                        fontSize: 13,
                        color: !progressFlag
                            ? Constants.appColorLogo
                            : Constants.appColorL2,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      progressFlag = true;
                    });
                    widget.getMovieDetails();
                    Navigator.of(context).pop();
                  },
                ),
                GestureDetector(
                  child: Container(
                    // margin: EdgeInsets.only(
                    //   left: 20,
                    // ),
                    padding: EdgeInsets.only(
                      left: 30,
                      right: 30,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: new BoxDecoration(
                      border: new Border.all(
                        color: Constants.appColorLogo,
                      ),
                      borderRadius: new BorderRadius.circular(30.0),
                      color: cancelFlag ? Constants.appColorLogo : Constants.appColorL2,
                    ),
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                        color:
                            !cancelFlag ? Constants.appColorLogo : Constants.appColorL2,
                        fontSize: 13,
                      ),
                    ),
                  ),
                  onTap: () {
                    setState(() {
                      cancelFlag = true;
                    });
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
