import 'package:flixjini_webapp/common/constants.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:flixjini_webapp/index/movie_index_carousel.dart';
// import 	'package:streaming_entertainment/common/default_api_params.dart';
// import 'dart:ui' as ui;

class MovieIndexCollapsableAppbar extends StatefulWidget {
  MovieIndexCollapsableAppbar({
    Key? key,
    this.indexDetail,
    this.showSheet,
    this.authenticationToken,
    this.showOriginalPosters,
    this.contentText,
  }) : super(key: key);

  final indexDetail, showSheet, contentText;
  final authenticationToken, showOriginalPosters;

  @override
  _MovieIndexCollapsableAppbarState createState() =>
      new _MovieIndexCollapsableAppbarState();
}

class _MovieIndexCollapsableAppbarState
    extends State<MovieIndexCollapsableAppbar> {
  bool filterFlag = false;

  Widget topMenuField(title, url) {
    String appliedFilterUrl = "";
    return new GestureDetector(
      child: new Text(
        title,
        style: TextStyle(
            color: Constants.appColorLogo,
            fontSize: 16.0,
            fontStyle: FontStyle.normal),
      ),
      onTap: () {
        //print(title);
        appliedFilterUrl = url;
        Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) => new MovieFilterPage(
              appliedFilterUrl: appliedFilterUrl,
            ),
          ),
        );
      },
    );
  }

  Widget collapsingAppBar() {
    return new MovieIndexCarousel(
      indexDetail: widget.indexDetail,
      authenticationToken: widget.authenticationToken,
      showOriginalPosters: widget.showOriginalPosters,
      showSheet: widget.showSheet,
      contentText: widget.contentText,
    );
  }

  @override
  Widget build(BuildContext context) {
    // print('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return widget.indexDetail != null
        ? collapsingAppBar()
        : Container(
            height: 30,
          );
  }
}
