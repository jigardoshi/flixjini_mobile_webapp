import 'package:flixjini_webapp/navigation/movie_routing.dart';
// import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';
// import	'package:streaming_entertainment/index/movie_index_category_seperator.dart';
// import 'package:http/http.dart' as http;
// import  'package:cached_network_image/cached_network_image.dart';
// import 	'package:streaming_entertainment/common/default_api_params.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
// import 	"dart:async";

class MovieIndexBrowse extends StatefulWidget {
  MovieIndexBrowse({
    Key? key,
    this.indexDetail,
    this.showOriginalPosters,
    this.topic,
  }) : super(key: key);

  final indexDetail, showOriginalPosters, topic;

  @override
  _MovieIndexBrowseState createState() => new _MovieIndexBrowseState();
}

class _MovieIndexBrowseState extends State<MovieIndexBrowse> {
  bool filterFlag = false;

  void initState() {
    super.initState();
  }

  Widget categoryCardBuilder(
      BuildContext context, int index, String categoryGroup) {
    var photo;
    double boxWidth = 100;
    String genreText = widget.indexDetail["browseByCategory"][categoryGroup]
                ["options"][index]["title"] !=
            null
        ? widget.indexDetail["browseByCategory"][categoryGroup]["options"]
                [index]["title"]
            .toString()
        : widget.indexDetail["browseByCategory"][categoryGroup]["options"]
            [index]["name"];
    if ((genreText.length * 15.0) > boxWidth) {
      boxWidth = genreText.length * 15.0 + 10;
    }
    if ((genreText.length * 15.0) > 200) {
      boxWidth = genreText.length * 15.0 - 40;
    }
    if ((genreText.length * 15.0) > 250) {
      boxWidth = genreText.length * 15.0 - 70;
    }

    if (categoryGroup == "category2") {
      photo = new Image.network(
        widget.indexDetail["browseByCategory"][categoryGroup]["options"][index]
            ["image"],
      );
    } else {
      photo = new AssetImage("images/pending.png");
    }
    // printIfDebug(
    //   "\n\nfrom categoryCardBuilder method, just printing the variable photo, just to use it: $photo",
    // );
    return new Container(
      padding: const EdgeInsets.all(5.0),
      child: new Card(
        //genre cards
        color: Constants.appColorL2,
        // elevation: 9.0,
        child: new GestureDetector(
          onTap: () {
            // _sendAnalyticsEvent(
            //     genreText,
            //     widget.indexDetail["browseByCategory"][categoryGroup]["title"]
            //         .toString());
            String appliedFilterUrl = "https://api.flixjini.com" +
                widget.indexDetail["browseByCategory"][categoryGroup]["options"]
                    [index]["url"];
            printIfDebug(
              "Go to Filter Page",
            );
            printIfDebug(
              appliedFilterUrl,
            );

            printIfDebug(
                '\n\nvalue of show original posters: ${widget.showOriginalPosters}');
            // Navigator.of(context).push(
            //   new MaterialPageRoute(
            //     builder: (BuildContext context) => new MovieFilterPage(
            //       appliedFilterUrl: appliedFilterUrl,
            //       showOriginalPosters: widget.showOriginalPosters,
            //       inTheaterKey: false,
            //     ),
            //   ),
            // );
            String urlAppendString = "&" +
                widget.indexDetail["browseByCategory"][categoryGroup]
                    ["filter-key"] +
                "=" +
                widget.indexDetail["browseByCategory"][categoryGroup]["options"]
                        [index]["url_name"]
                    .toString()
                    .toLowerCase();
            printIfDebug(urlAppendString);
            Navigator.of(context).push(
              new MaterialPageRoute(
                builder: (BuildContext context) => new MovieRouting(
                  defaultTab: 2,
                  urlAppendString: urlAppendString,
                  showOriginalPosters: widget.showOriginalPosters,
                ),
              ),
            );
          },
          child: new Container(
            // height: 45,
            width: boxWidth,
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 20,
                    width: 20,
                    child: getImageForCard(
                      categoryGroup,
                      index,
                    ),
                  ),
                  new Expanded(
                    child: new Container(
                      padding: EdgeInsets.only(
                        left: 5,
                        right: 5,
                      ),
                      child: new Text(
                        genreText.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          // fontSize: 20.0,

                          color: Constants.appColorFont,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }

  Widget getImageForCard(
    String categoryGroup,
    int index,
  ) {
    if (categoryGroup.contains('category1')) {
      return Image.network(
        widget.indexDetail["browseByCategory"][categoryGroup]["options"][index]
            ["image"],
        // useDiskCache: false,

        fit: BoxFit.contain,
      );
    } else {
      return Image.network(
        widget.indexDetail["browseByCategory"][categoryGroup]["options"][index]
            ["image"],
        // useDiskCache: false,

        fit: BoxFit.contain,
      );
    }
  }

  String getLocalImageFileNumber(String imageUrl) {
    String subStringToRemove =
        'https://komparify.sgp1.cdn.digitaloceanspaces.com/assets/streaming-sources/';
    imageUrl = imageUrl.replaceAll(subStringToRemove, '');

    return imageUrl;
  }

  Widget categoryListView(i) {
    String categoryGroup = "category" + (i + 1).toString();
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: new SizedBox.fromSize(
        size: const Size.fromHeight(65.0),
        child: new ListView.builder(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          addAutomaticKeepAlives: false,
          itemCount: widget
              .indexDetail["browseByCategory"][categoryGroup]["options"].length,
          scrollDirection: Axis.horizontal,
          // padding: const EdgeInsets.only(bottom: 20.0),
          itemBuilder: (context, index) => categoryCardBuilder(
            context,
            index,
            categoryGroup,
          ),
        ),
      ),
    );
  }

  Widget categoryTitle(i) {
    String categoryGroup = "category" + (i + 1).toString();
    return Row(
      children: <Widget>[
        new Container(
          padding: EdgeInsets.only(
            left: 10,
            bottom: 10,
          ),
          child: new Text(
            "Browse By " +
                widget.indexDetail["browseByCategory"][categoryGroup]["title"]
                    .toString()
                    .replaceAll("Sources", "Source"),
            textAlign: TextAlign.left,
            style: new TextStyle(
              fontWeight: FontWeight.normal,
              color: Constants.appColorFont,
              fontSize: 18.0,
              letterSpacing: 1,
            ),
          ),
        ),
      ],
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 8.0,
        left: 10,
        right: 10,
      ),
      child: SizedBox(
        height: 2.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget variousBrowseCategories() {
    int numberOfCategories = widget.indexDetail["browseByCategory"].length;
    var categories = <Widget>[];
    for (int i = 0; i < numberOfCategories; i++) {
      // iterate from index 0 to include streaming sources section
      var category = new Container(
        margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
        padding: EdgeInsets.only(top: 20),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage(
                "assests/home_tray_bg.jpg",
              ),
              fit: BoxFit.fill),
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            categoryTitle(i),
            categoryListView(i),
            // insertDivider(),
          ],
        ),
      );
      categories.add(category);
    }
    return new Container(
      // padding: const EdgeInsets.only(top:10),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: categories,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return variousBrowseCategories();
  }
}
