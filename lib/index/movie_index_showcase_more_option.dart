import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieIndexShowcaseMoreOption extends StatefulWidget {
  MovieIndexShowcaseMoreOption(
      {Key? key, this.categoryGroup, this.showAllInCategory})
      : super(key: key);

  final categoryGroup;
  final showAllInCategory;

  @override
  _MovieIndexShowcaseMoreOptionState createState() =>
      new _MovieIndexShowcaseMoreOptionState();
}

class _MovieIndexShowcaseMoreOptionState
    extends State<MovieIndexShowcaseMoreOption> {
  String morePhoto = "assests/more.png";

  Widget moreCardUI() {
    return new Container(
      margin: EdgeInsets.only(
        left: 25,
        right: 25,
      ),
      height: 192.0,
      width: 100.0,
      padding: const EdgeInsets.all(0.0),
      child: new GestureDetector(
          onTap: () {
            printIfDebug("Tap Working");
            widget.showAllInCategory(widget.categoryGroup);
          },
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                  padding: const EdgeInsets.all(2.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          'assests/expand_icon.png',
                          height: 18,
                          width: 18,
                        ),
                      ),
                      new Container(
                        padding: const EdgeInsets.all(5.0),
                        child: new Text(
                          "More",
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Constants.appColorFont.withOpacity(0.6),
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ],
                  )),
            ],
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug('\n\nhey there! i am inside build method of ${this.runtimeType}');
    return moreCardUI();
  }
}
