import 'package:flutter/material.dart';
import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flixjini_webapp/filter/movie_filter_page.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/services.dart';
import 'package:flixjini_webapp/common/constants.dart';

// import 	'package:streaming_entertainment/common/default_api_params.dart';

class MovieIndexOverview extends StatefulWidget {
  MovieIndexOverview({
    Key? key,
    this.indexDetail,
    this.showOriginalPosters,
  }) : super(key: key);

  final indexDetail, showOriginalPosters;

  @override
  _MovieIndexOverviewState createState() => new _MovieIndexOverviewState();
}

class _MovieIndexOverviewState extends State<MovieIndexOverview> {
  bool filterFlag = false;
  FirebaseAnalytics analytics = FirebaseAnalytics();
  static const platform = const MethodChannel('api.komparify/advertisingid');

  Widget categoryTitle(String title) {
    return new Text(
      title,
      textAlign: TextAlign.center,
      style: new TextStyle(
          fontWeight: FontWeight.normal,
          color: Constants.appColorL3,
          fontSize: 20.0),
    );
  }

  Widget insertDivider() {
    return new Container(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 8.0,
        left: 10,
        right: 10,
      ),
      child: SizedBox(
        height: 2.5,
        // width: 200.0,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            height: 2.5,
            color: Constants.appColorL2,
          ),
        ),
      ),
    );
  }

  Widget insertDividerVertical() {
    return new Container(
      padding: const EdgeInsets.only(
        // left: 3.0,
        right: 10,
      ),
      child: SizedBox(
        height: 30.0,
        width: 3,
        child: new Center(
          child: new Container(
            margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
            // height: 100,
            color: Constants.appColorDivider.withOpacity(0.1),
          ),
        ),
      ),
    );
  }

  Widget overviewCardView() {
    var topics = <Widget>[];
    String url;
    var topicsLength = widget.indexDetail["overview"].length;
    for (var i = 0; i < topicsLength; i++) {
      if (widget.indexDetail["overview"][i]["key"] == "movies") {
        url = "assests/movies.png";
      } else if (widget.indexDetail["overview"][i]["key"] == "tv-shows") {
        url = "assests/tv_shows.png";
      } else if (widget.indexDetail["overview"][i]["key"] == "in-theatres") {
        url = "assests/theaters.png";
      } else {
        url = "images/pending.png";
      }
      var score = new Container(
        width: 100.0,
        child: new GestureDetector(
          onTap: () {
            _sendAnalyticsEvent(
                widget.indexDetail["overview"][i]["title"].toString(),
                widget.indexDetail["overview"][i]["key"].toString());
            bool inTheaterKey = false;
            if (widget.indexDetail["overview"][i]["key"] == "movies") {
              inTheaterKey = false;
            } else if (widget.indexDetail["overview"][i]["key"] == "tv-shows") {
              inTheaterKey = false;
            } else if (widget.indexDetail["overview"][i]["key"] ==
                "in-theatres") {
              inTheaterKey = true;
            }
            String appliedFilterUrl = "https://api.flixjini.com" +
                widget.indexDetail["overview"][i]["url"];
            printIfDebug("Applied URL");
            printIfDebug(appliedFilterUrl);
            printIfDebug("Go to Movie Detail Page");
            printIfDebug(
                '\n\napplied filter url before going to movie filter pg: ' +
                    appliedFilterUrl);
            Navigator.of(context).push(
              new MaterialPageRoute(
                builder: (BuildContext context) => new MovieFilterPage(
                  appliedFilterUrl: appliedFilterUrl,
                  inTheaterKey: inTheaterKey,
                  showOriginalPosters: widget.showOriginalPosters,
                ),
              ),
            );
          },
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Movies TV shows in theaters

              Container(
                padding: EdgeInsets.only(
                  top: 1.8,
                  right: 5,
                ),
                child: Image.asset(
                  url,
                  fit: BoxFit.cover,
                  height: 18,
                  width: 18,
                ),
              ),

              new Container(
                // width: 100.0,
                margin: const EdgeInsets.only(
                  top: 5.0,
                  // left: 5,
                  // right: 5,
                ),
                child: new Text(
                  widget.indexDetail["overview"][i]["title"].toUpperCase(),
                  textAlign: TextAlign.left,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Constants.appColorFont,
                    fontSize: 10.0,
                    letterSpacing: 0.5,
                  ),
                  // overflow: TextOverflow.visible,
                  maxLines: 1,
                ),
              ),
              Spacer(),
              i < topicsLength - 1 ? insertDividerVertical() : Container(),
            ],
          ),
        ),
      );
      // topics.add(
      //   insertDividerVertical(),
      // );
      topics.add(score);
    }
    return new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: topics);
  }

  Future<void> _sendAnalyticsEvent(tagName, key) async {
    await analytics.logEvent(
      name: 'index_overview_click',
      parameters: <String, dynamic>{
        'overview': tagName,
      },
    );
    if (key == "tv-shows") {
      //platform.invokeMethod('logScheduleEvent');
    } else if (key == "in-theatres") {
    } else if (key == "movies") {
      //platform.invokeMethod('logContactEvent');
    }
  }

  Widget overviewSection() {
    return new Container(
      decoration: BoxDecoration(
        color: Constants.appColorL2,
        borderRadius: BorderRadius.circular(35),
      ),
      padding: const EdgeInsets.only(
        top: 10.0,
        bottom: 10.0,
        left: 20.0,
        right: 20.0,
      ),
      margin: const EdgeInsets.only(
        top: 20.0,
        bottom: 10.0,
        left: 10.0,
        right: 10.0,
      ),
      child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            overviewCardView(),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return overviewSection();
  }
}
