import 'package:flixjini_webapp/utils/common_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
// import 'package:flixjini_webapp/utils/common_utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flixjini_webapp/navigation/movie_navigator.dart';
// import  'package:shared_preferences/shared_preferences.dart';
import 'package:flixjini_webapp/common/encryption_functions.dart';
import 'package:flixjini_webapp/common/google_analytics_functions.dart';
// import  'package:streaming_entertainment/detail/review/movie_detail_review_card.dart';
import 'package:flixjini_webapp/detail/tv_show/movie_detail_tv_show_chosen_season.dart';
// import  'package:cached_network_image/cached_network_image.dart';
import 'package:flixjini_webapp/common/shimmer_types/shimmer_tvshow.dart';
import 'package:flixjini_webapp/common/default_api_params.dart';
// import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flixjini_webapp/common/constants.dart';

class MovieTvshowPage extends StatefulWidget {
  MovieTvshowPage({
    Key? key,
    this.tvShowMin,
    this.defaultChosenSeason,
    this.authenticationToken,
    this.urlPhase,
  }) : super(key: key);

  final tvShowMin;
  final defaultChosenSeason;
  final authenticationToken;
  final urlPhase;

  @override
  _MovieTvshowPageState createState() => new _MovieTvshowPageState();
}

class _MovieTvshowPageState extends State<MovieTvshowPage> {
  var tvShow;
  var cardLayout = {};
  var chosenSeasonData;
  int chosenSeason = 0;
  int chosenPage = 0;
  bool fetchedTvshow = false;
  double screenWidth = 0.0;
  double screenHeight = 0.0;

  @override
  void initState() {
    cardLayout = {"height": 150.0, "width": 220.0};
    tvShowDetails();
    if (widget.defaultChosenSeason != null) {
      selectedSeason(widget.defaultChosenSeason);
    } else {
      selectedSeason(0);
    }
    setChosenPage(0);
    checkIfEpisodesNeedToBeFetched();
    logUserScreen('Flixjini Tv Show Page', 'MovieTvshowPage');
    super.initState();
  }

  tvShowDetails() {
    setState(() {
      tvShow = widget.tvShowMin;
      fetchedTvshow = true;
    });
    printIfDebug("More Epi");
    printIfDebug(
        tvShow["seasons"][widget.defaultChosenSeason]["more_episodes"]);
  }

  setMoreEpisodes(bool value) {
    setState(() {
      tvShow["more_episodes"] = false;
    });
  }

  selectedSeason(int index) {
    setState(() {
      chosenSeason = index;
    });
    printIfDebug("New Chosen Season");
    printIfDebug(chosenSeason);
    seasonNeedsToBePassed();
  }

  setChosenPage(value) {
    setState(() {
      chosenPage = value;
    });
    printIfDebug("Here the value chnages");
    printIfDebug(chosenPage);
  }

  seasonNeedsToBePassed() {
    setState(() {
      chosenSeasonData = tvShow["seasons"][chosenSeason];
    });
    printIfDebug(chosenSeasonData["episodes"]);
  }

  checkIfEpisodesNeedToBeFetched() {
    bool flag = false;
    printIfDebug("Fetch Checker");
    printIfDebug(tvShow["seasons"][chosenSeason]["more_episodes"]);
    if (tvShow["seasons"][chosenSeason].containsKey("more_episodes")) {
      printIfDebug("More Episodes Exists");
      if (tvShow["seasons"][chosenSeason]["more_episodes"]) {
        fetchSelectedSeasonEpisdoes();
        setMoreEpisodes(false);
      } else {
        flag = true;
      }
    } else {
      flag = true;
    }
    if (flag == true) {
      if (tvShow["seasons"][chosenSeason].containsKey("episodes")) {
        printIfDebug(
            "Episodes have already been fetched --> No need API request");
      } else {
        fetchSelectedSeasonEpisdoes();
      }
    }
  }

  constructUrl() {
    String urlName = "";
    String refinedUrlPhase = "";
    String seasonNo = (chosenSeason + 1).toString();
    printIfDebug("URl phase tv show season");
    printIfDebug(widget.urlPhase);
    refinedUrlPhase =
        (widget.urlPhase).replaceAll(".json", '/season/' + seasonNo + '.json');
    String andOption = "";
    if (refinedUrlPhase.contains('?')) {
      printIfDebug("a param is already present");
      andOption = "&";
    } else {
      refinedUrlPhase += '?';
    }
    if (widget.authenticationToken.isEmpty) {
      urlName = "https://api.flixjini.com" + refinedUrlPhase;
    } else {
      urlName = "https://api.flixjini.com" +
          refinedUrlPhase +
          andOption +
          "jwt_token=" +
          widget.authenticationToken +
          "&";
    }
    return urlName;
  }

  updateSelectedSeasonEpisodes(var episodes, var sources) {
    setState(() {
      tvShow["seasons"][chosenSeason]["episodes"] = episodes;
      tvShow["seasons"][chosenSeason]["streamingSources"] = sources;
    });
  }

  fetchSelectedSeasonEpisdoes() async {
    bool requestFlag = false;
    var data;
    String url = "";
    url = constructUrl();
    url = constructHeader(url);
    url = await fetchDefaultParams(url);
    printIfDebug("Tv Show Get Season");
    printIfDebug(url);
    var response = await http.get(Uri.parse(url));
    printIfDebug(response.body);
    printIfDebug(response.statusCode);
    try {
      if (response.statusCode == 200) {
        String responseBody = response.body;
        data = json.decode(responseBody);
        requestFlag = true;
      } else {
        printIfDebug(
            "Error getting IP address:\nHttp status ${response.statusCode}");
      }
    } catch (exception) {
      printIfDebug('Failed getting IP address');
    }
    if (requestFlag) {
      printIfDebug("Episode Recived");
      printIfDebug(data);
      updateSelectedSeasonEpisodes(data["episodes"], data["streamingSources"]);
    }
    if (!mounted) return;
  }

  Widget setSeasonBanner(int index) {
    return new Expanded(
      child: new Container(
        padding: const EdgeInsets.all(0.0),
        child: new Center(
          child: new ClipRRect(
            borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(
                8.0,
              ),
              topRight: const Radius.circular(
                8.0,
              ),
            ),
            child: Image(
              image: NetworkImage(
                tvShow["seasons"][index]["seasonPoster"],
                // useDiskCache: false,
              ),
              fit: BoxFit.cover,
              width: cardLayout["width"],
              gaplessPlayback: true,
            ),
          ),
        ),
      ),
      flex: 2,
    );
  }

  Widget setSeasonName(int index) {
    Color overallColor = Constants.appColorL3;
    return new Expanded(
      child: new Container(
        child: new ClipRRect(
          borderRadius: new BorderRadius.only(
              bottomLeft: const Radius.circular(8.0),
              bottomRight: const Radius.circular(8.0)),
          child: new Container(
              color: Constants.appColorFont,
              padding: const EdgeInsets.all(0.0),
              child: new Center(
                child: new Text(
                  tvShow["seasons"][index]["label"],
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: overallColor,
                    fontSize: 10.0,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              )),
        ),
      ),
      flex: 1,
    );
  }

  Widget buildAllSeasonCards(BuildContext context, int index) {
    // var sections = <Widget>[];
    // sections.add(setSeasonBanner(index));
    // sections.add(setSeasonName(index));
    // printIfDebug("shakthi" +
    //     widget.tvShow["seasons"][index].keys.toString());
    // String episodesLength = (widget.tvShow["seasons"][index] != null &&
    //         widget.tvShow["seasons"][index]["episodes"] != null)
    //     ? widget.tvShow["seasons"][index]["episodes"].length.toString()
    //     : "0";
    String episodesLength = tvShow["seasons"][index]["episodeCount"].toString();
    return Container(
      width: 160,
      child: AnimationConfiguration.staggeredList(
        position: index,
        duration: const Duration(milliseconds: 375),
        child: SlideAnimation(
          verticalOffset: 50.0,
          child: FadeInAnimation(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    setChosenPage(0);
                    selectedSeason(index);
                    Navigator.of(context).push(
                      new MaterialPageRoute(
                        builder: (BuildContext context) => new MovieTvshowPage(
                          tvShowMin: tvShow,
                          defaultChosenSeason: chosenSeason,
                          authenticationToken: widget.authenticationToken,
                          urlPhase: widget.urlPhase,
                        ),
                      ),
                    );
                  },
                  child:

                      // getBlurredImageContainer(index),
                      getSeasonCard(
                          index, episodesLength, index == chosenSeason),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getSeasonCard(
    int index,
    String episodesLength,
    bool flag,
  ) {
    return Container(
      child:
          // Stack(
          //   children: <Widget>[
          // Container(
          //   height: 350,
          //   width: 200,
          //   margin: EdgeInsets.all(
          //     5,
          //   ),
          //   child: Image.asset(
          //     getRandomImageFilePath(
          //       index,
          //     ),
          //     fit: BoxFit.fill,
          //   ),
          // ),
          Container(
        // color: Constants.appColorFont,
        width: 160,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: flag ? Constants.appColorLogo : Constants.appColorL1,
        ),
        // height: index == chosenSeason ? 100 : 100,
        height: flag ? cardLayout["height"] - 60 : 70,

        margin: EdgeInsets.all(
          5,
        ),
        padding: EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Center(
                child: Row(
                  children: [
                    Text(
                      tvShow["seasons"][index]["title"] ?? "",
                      // textAlign: TextAlign.center,
                      style: TextStyle(
                        color: flag
                            ? Constants.appColorL1
                            : Constants.appColorFont,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.5,
                        fontSize: 15,
                      ),
                    ),
                    Spacer(),
                    flag
                        ? Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Constants.appColorL1,
                            ),
                            height: 25,
                            width: 25,
                            child: Icon(
                              Icons.play_arrow,
                              size: 15,
                              color: Constants.appColorLogo,
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
            flag
                ? Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // margin: EdgeInsets.only(bottom: 10),
                          color: Constants.appColorL1.withOpacity(0.70),
                          height: 1,
                        ),
                        Spacer(),
                        Text(
                          episodesLength + " Episodes",
                          style: TextStyle(
                            color: index == chosenSeason
                                ? Constants.appColorL1
                                : Constants.appColorFont,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            letterSpacing: 1.0,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      //   ],
      // ),
    );
  }

  Widget displayLoader() {
    return Scaffold(
      appBar: appHeaderBar(),
      backgroundColor: Constants.appColorFont,
      body: new Center(
        child: ShimmerTvshows(),
      ),
    );
  }

  AppBar appHeaderBar() {
    return new AppBar(
      backgroundColor: Constants.appColorL2,
      iconTheme: new IconThemeData(color: Constants.appColorDivider),
      leading: new Container(
        width: 40.0,
        child: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Constants.appColorFont),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      title: new Text("Seasons",
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(
              fontWeight: FontWeight.normal, color: Constants.appColorFont),
          textAlign: TextAlign.center,
          maxLines: 1),
    );
  }

  Widget tvShowSeasonsView() {
    return Container(
      height: cardLayout["height"] - 20,
      child: ListView.builder(
        itemCount: tvShow["seasons"].length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        // padding: const EdgeInsets.all(10.0),
        itemBuilder: (BuildContext context, int index) {
          return buildAllSeasonCards(
            context,
            index,
          );
        },
      ),
    );
  }

  Widget displayReviews() {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appHeaderBar(),
      body: new Container(
        width: screenWidth,
        padding: const EdgeInsets.only(bottom: 10.0),
        color: Constants.appColorL1,
        child: new SingleChildScrollView(
          child: new ConstrainedBox(
            constraints: new BoxConstraints(),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                tvShowSeasonsView(),
                new MovieDetailTvShowChosenSeason(
                  season: chosenSeasonData,
                  chosenSeason: chosenSeason,
                  setChosenPage: setChosenPage,
                  chosenPage: chosenPage,
                ),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Constants.appColorL1,
      bottomNavigationBar: new MovieNavigator(),
    );
  }

  Widget checkDataReady() {
    try {
      if (fetchedTvshow) {
        return displayReviews();
      } else {
        return displayLoader();
      }
    } catch (exception) {
      return displayLoader();
    }
  }

  @override
  Widget build(BuildContext context) {
    printIfDebug(
        '\n\nhey there! i am inside build method of ${this.runtimeType}');
    return checkDataReady();
  }
}
